define([], function() {

  	function PayAPerson_deleteRecipient_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_deleteRecipient_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	PayAPerson_deleteRecipient_CommandHandler.prototype.execute = function(command){
		var self = this;
        
        function onCompletionCallback(status,response,error){
          if(status == kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,error);
          }
           else{
               self.sendResponse(command,status,error);
           } 
        }
      
        try {
            var payPersonModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PayPerson");
            payPersonModel.customVerb('deletePayPerson', {"PayPersonId":command.context.recipientID}, onCompletionCallback);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
	
	PayAPerson_deleteRecipient_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_deleteRecipient_CommandHandler;
    
});