define([], function() {

  	function PayAPerson_UpdatePreferencesForP2PUser_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_UpdatePreferencesForP2PUser_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	PayAPerson_UpdatePreferencesForP2PUser_CommandHandler.prototype.execute = function(command){
       var self = this;
      var params = {
        "username": command.context.username,
        "default_account_payments" : command.context.accountNumber
      };
      /**
       * Call back function for updateUserCompletionCallback.
       * @params  : status message, data, error message.
       */
      function completionCallback(status, response, err) {
          if(status==kony.mvc.constants.STATUS_SUCCESS)
            self.sendResponse(command, status, response);
          else
            self.sendResponse(command, status, err);
      } 
      try {
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.updatePreferredP2PAccounts(command.context,completionCallback);
      } catch (error) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
      }   		
		
    };
	
	PayAPerson_UpdatePreferencesForP2PUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_UpdatePreferencesForP2PUser_CommandHandler;
    
});