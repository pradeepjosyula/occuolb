define([], function() {

  	function PayAPerson_editPayAPersonRecipient_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_editPayAPersonRecipient_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	PayAPerson_editPayAPersonRecipient_CommandHandler.prototype.execute = function(command){
		var self = this;
        
        function onCompletionCallback(status,response,error){
          if(status == kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,error);
          }
           else{
               self.sendResponse(command,status,error);
           } 
        }
      
        try {
            var payPersonModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PayPerson");
            payPersonModel.customVerb('editPayPerson', command.context, onCompletionCallback);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
	
	PayAPerson_editPayAPersonRecipient_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_editPayAPersonRecipient_CommandHandler;
    
});