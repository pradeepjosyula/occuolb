define([], function() {

    var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
  
  	function PayAPerson_CheckP2PEligibilityForUser_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_CheckP2PEligibilityForUser_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	PayAPerson_CheckP2PEligibilityForUser_CommandHandler.prototype.execute = function(command){
      var self = this;
      /**
       * Call back function for checkBillPayEligibility.
       * @params  : status message, data, error message.
       */
      function completionCallback(status, response, err) {
          if(status==kony.mvc.constants.STATUS_SUCCESS)
            self.sendResponse(command, status, response);
          else
            self.sendResponse(command, status, err);
      } 
      try {
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.checkP2PEligibilityForUser(command.context,completionCallback);
      } catch (error) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
      }
    };
	
	PayAPerson_CheckP2PEligibilityForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_CheckP2PEligibilityForUser_CommandHandler;
    
});