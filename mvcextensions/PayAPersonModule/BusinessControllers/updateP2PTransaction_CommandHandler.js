define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function PayAPerson_updateP2PTransaction_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_updateP2PTransaction_CommandHandler, CommandHandler);
  
  	PayAPerson_updateP2PTransaction_CommandHandler.prototype.execute = function(command){
		
	var self = this;

    function onCreateTransfer (status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var context = command.context;
    var transactionsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var newTransaction = new transactionsModel({
      'transactionId': context.transactionId,
      'transactionsNotes': context.transactionsNotes,
	  'fromAccountNumber':  context.fromAccountNumber,    
	  'amount':context.amount,
	  'frequencyType': context.frequencyType,
	  'isScheduled': context.isScheduled,
	  'scheduledDate': context.scheduledDate,      
	  'p2pContact' : context.p2pContact,
	  'numberOfRecurrences': context.numberOfRecurrences, 
	  'frequencyStartDate': context.frequencyStartDate,
	  'frequencyEndDate': context.frequencyEndDate,
	  'personId': context.personId
      //Below once are only needed if frequencyType is not 'once'
    });
    newTransaction.partialUpdate(onCreateTransfer);
		
    };
	
	PayAPerson_updateP2PTransaction_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_updateP2PTransaction_CommandHandler;
    
});