define([], function() {

  	function PayAPerson_payPersonActivity_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_payPersonActivity_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	PayAPerson_payPersonActivity_CommandHandler.prototype.execute = function(command){
		var self = this;
        
        function onCompletionCallback(status,response,error){
          if(status == kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,response);
          }
           else{
               self.sendResponse(command,status,error);
           } 
        }
      
        try {
            var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
            TransactionModel.customVerb('getPayPersonHistory', command.context , onCompletionCallback);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
	
	PayAPerson_payPersonActivity_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_payPersonActivity_CommandHandler;
    
});
