define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;

  function PayAPerson_cancelRequestMoney_CommandHandler(commandId) {
    CommandHandler.call(this, commandId);
  }

  inheritsFrom(PayAPerson_cancelRequestMoney_CommandHandler, CommandHandler);

      PayAPerson_cancelRequestMoney_CommandHandler.prototype.execute = function(command) {
        var self = this;
        var context = command.context;

        function completionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
            var transactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
            var transactionObject = new transactionModel({
                "transactionId": context.transactionId
            });
            transactionModel.removeById(context.transactionId,completionCallback);
        } catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };

  PayAPerson_cancelRequestMoney_CommandHandler.prototype.validate = function(){

  };

  return PayAPerson_cancelRequestMoney_CommandHandler;

});