define([], function() {

  	function PayAPerson_ActivateP2PForUser_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_ActivateP2PForUser_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	PayAPerson_ActivateP2PForUser_CommandHandler.prototype.execute = function(command){
		var self = this;
      /**
       * Call back function for activateBillPayForUser.
       * @params  : status message, data, error message.
       */
      function completionCallback(status, response, err) {
          if(status==kony.mvc.constants.STATUS_SUCCESS)
            self.sendResponse(command, status, response);
          else
            self.sendResponse(command, status, err);
      } 
      try {
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.activateP2PForUser(command.context,completionCallback);
      } catch (error) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
      }		
    };
	
	PayAPerson_ActivateP2PForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_ActivateP2PForUser_CommandHandler;
    
});