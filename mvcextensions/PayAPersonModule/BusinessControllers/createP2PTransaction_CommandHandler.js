define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;

  function PayAPerson_createP2PTransaction_CommandHandler(commandId) {
    CommandHandler.call(this, commandId);
  }

  inheritsFrom(PayAPerson_createP2PTransaction_CommandHandler, CommandHandler);

  PayAPerson_createP2PTransaction_CommandHandler.prototype.execute = function(command){

    var self = this;

    function onCreateTransfer (status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var context = command.context;
    var transactionsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var newTransaction = new transactionsModel({
      'fromAccountNumber':  context.fromAccountNumber,
      'amount':context.amount,
      'transactionsNotes': context.transactionsNotes,
      //Need to Change
      'toAccountNumber': context.toAccountNumber,
      'frequencyType': context.frequencyType,
      'transactionType': "P2P",
      'isScheduled': context.isScheduled,
      'scheduledDate': context.scheduledDate,      
      'personId': context.personId,
	  'p2pContact' : context.p2pContact,
      //Below once are only needed if frequencyType is not 'once'
      'numberOfRecurrences': context.numberOfRecurrences, 
      'frequencyStartDate': context.frequencyStartDate,
      'frequencyEndDate': context.frequencyEndDate,
	  'fee' :  context.fee
    });
    newTransaction.save(onCreateTransfer);
  };

  PayAPerson_createP2PTransaction_CommandHandler.prototype.validate = function(){

  };

  return PayAPerson_createP2PTransaction_CommandHandler;

});