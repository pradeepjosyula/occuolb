define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;

  function PayAPerson_getSentP2PTransactions_CommandHandler(commandId) {
    CommandHandler.call(this, commandId);
  }

  inheritsFrom(PayAPerson_getSentP2PTransactions_CommandHandler, CommandHandler);

  PayAPerson_getSentP2PTransactions_CommandHandler.prototype.execute = function(command){

    var self = this;  
    var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var params = {
      "sortBy": command.context.sortBy,
      "order": command.context.order,
      "offset": command.context.offset,
      "limit": command.context.limit
    };

    function completionCallback(status, response, err) {
      if(status==kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, err);
    }
    TransactionsModel.customVerb("getSentP2PTransactions", params, completionCallback);

  };

  PayAPerson_getSentP2PTransactions_CommandHandler.prototype.validate = function(){

  };

  return PayAPerson_getSentP2PTransactions_CommandHandler;

});