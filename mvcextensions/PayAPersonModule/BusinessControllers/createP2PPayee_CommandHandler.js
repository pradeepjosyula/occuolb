define([], function() {

  	function PayAPerson_createP2PPayee_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_createP2PPayee_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	PayAPerson_createP2PPayee_CommandHandler.prototype.execute = function(command){
	var self = this;	
      try{
        function createPayeeCompletionCallBack(status,data,error){
           self.sendResponse(command,status,status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
          var PayPersonModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PayPerson");
          var payPersonObj = new PayPersonModel(command.context);
          payPersonObj.save(createPayeeCompletionCallBack);
        }catch(err){
          self.sendResponse(command,status,err);
        }
    };
	
	PayAPerson_createP2PPayee_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_createP2PPayee_CommandHandler;
    
});