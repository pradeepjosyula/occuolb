define([], function() {
    
          function PayAPerson_getP2PRecipientsList_CommandHandler(commandId) {
            kony.mvc.Business.CommandHandler.call(this, commandId);
        }
        
        inheritsFrom(PayAPerson_getP2PRecipientsList_CommandHandler, kony.mvc.Business.CommandHandler);
      
          PayAPerson_getP2PRecipientsList_CommandHandler.prototype.execute = function(command){
          var self = this;
          
          if (command.context.searchString) {
			var criteria = kony.mvc.Expression.eq("searchString", command.context.searchString);
			} else{
            var criteria = kony.mvc.Expression.and( 
                kony.mvc.Expression.eq("sortBy", command.context.sortBy),
                kony.mvc.Expression.eq("order", command.context.order),
                kony.mvc.Expression.eq("offset", command.context.offset), 
                kony.mvc.Expression.eq("limit", command.context.limit));	
           
		   }
          
          var payAPersonModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PayPerson");
          function completionCallBack(status,data,error){
             self.sendResponse(command,status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
          }
                    
           payAPersonModel.getByCriteria(criteria, completionCallBack);
        };
        
        PayAPerson_getP2PRecipientsList_CommandHandler.prototype.validate = function(){
            
        };
        
        return PayAPerson_getP2PRecipientsList_CommandHandler;
        
    });