define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function PayAPerson_getP2PRequestsForUser_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_getP2PRequestsForUser_CommandHandler, CommandHandler);
  
  	PayAPerson_getP2PRequestsForUser_CommandHandler.prototype.execute = function(command){	
      var self = this;
      var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Transactions');
	  function onCompletion(status, response, error){
        if(status==kony.mvc.constants.STATUS_SUCCESS)
              self.sendResponse(command, status, response);
            else
              self.sendResponse(command, status, error);
      } 
      TransactionsModel.customVerb("getAllP2PRequestMoneyForUser", {}, onCompletion);
    };
	
	PayAPerson_getP2PRequestsForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_getP2PRequestsForUser_CommandHandler;
    
});
