define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;

  function PayAPerson_sendReminder_CommandHandler(commandId) {
    CommandHandler.call(this, commandId);
  }

  inheritsFrom(PayAPerson_sendReminder_CommandHandler, CommandHandler);

  PayAPerson_sendReminder_CommandHandler.prototype.execute = function(command){

    var self = this;

    function onCreateTransfer (status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var context = command.context;
    var transactionsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var newTransaction = new transactionsModel({
      'transactionId': context.transactionId,
      'p2pRequiredDate': context.p2pRequiredDate, //Expected date format is yyyy-mm-dd 
      'transactionsNotes': context.transactionsNotes
      //Below once are only needed if frequencyType is not 'once'
    });
    newTransaction.partialUpdate(onCreateTransfer);

  };

  PayAPerson_sendReminder_CommandHandler.prototype.validate = function(){

  };

  return PayAPerson_sendReminder_CommandHandler;

});