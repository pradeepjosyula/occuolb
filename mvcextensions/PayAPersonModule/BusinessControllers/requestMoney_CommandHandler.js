define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;

  function PayAPerson_requestMoney_CommandHandler(commandId) {
    CommandHandler.call(this, commandId);
  }

  inheritsFrom(PayAPerson_requestMoney_CommandHandler, CommandHandler);

  PayAPerson_requestMoney_CommandHandler.prototype.execute = function(command){
    var self = this;

    function onCreateRequest(status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var context = command.context;
    var transactionsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var newTransaction = new transactionsModel({
      'fromAccountNumber': context.fromAccountNumber,
      'amount': context.amount,
      'p2pRequiredDate': context.p2pRequiredDate, //Expected date format is yyyy-mm-dd 
      'p2pContact': context.p2pContact,
      'personId': context.personId,
      'transactionsNotes': context.notes,
      'transactionType': "Request"
    });
    newTransaction.save(onCreateRequest);
  };

  PayAPerson_requestMoney_CommandHandler.prototype.validate = function(){

  };

  return PayAPerson_requestMoney_CommandHandler;

});