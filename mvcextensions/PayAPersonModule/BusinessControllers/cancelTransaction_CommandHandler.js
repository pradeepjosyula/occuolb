define([], function() {

  	function PayAPerson_cancelTransaction_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PayAPerson_cancelTransaction_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	PayAPerson_cancelTransaction_CommandHandler.prototype.execute = function(command){
		  var self = this;
      var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      function onCompletionCallback(status,response,error){
        if(status === kony.mvc.constants.STATUS_SUCCESS){
            self.sendResponse(command,status,error);
        }
         else{
             self.sendResponse(command,status,error);
         } 
      }
      TransactionModel.removeById(command.context.transactionId, onCompletionCallback);
    };
	
	PayAPerson_cancelTransaction_CommandHandler.prototype.validate = function(){
		
    };
    
    return PayAPerson_cancelTransaction_CommandHandler;
    
});