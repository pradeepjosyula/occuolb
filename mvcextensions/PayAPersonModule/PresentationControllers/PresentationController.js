define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {

    function PayAPerson_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.allowedToPresent = false;
        this.constants = {
            offset: OLBConstants.DEFAULT_OFFSET,
            limit: OLBConstants.PAGING_ROWS_LIMIT,
            paginationRowLimit: OLBConstants.PAGING_ROWS_LIMIT,
            paginationText: "paginationText"
        };
        this.sentOrRequestSortConfig = {
            'sortBy': 'nickName',
            'defaultSortBy': 'nickName',
            'order': OLBConstants.ASCENDING_KEY,
            'defaultOrder': OLBConstants.ASCENDING_KEY,
            'offset': OLBConstants.DEFAULT_OFFSET,
            'limit': OLBConstants.PAGING_ROWS_LIMIT
        };
        this.manageRecipientSortConfig = {
            'sortBy': 'nickName',
            'defaultSortBy': 'nickName',
            'order': OLBConstants.ASCENDING_KEY,
            'defaultOrder': OLBConstants.ASCENDING_KEY,
            'offset': OLBConstants.DEFAULT_OFFSET,
            'limit': OLBConstants.PAGING_ROWS_LIMIT
        };
        this.sentSortConfig = {
            'sortBy': 'transactionDate',
            'defaultSortBy': 'transactionDate',
            'order': OLBConstants.DESCENDING_KEY,
            'defaultOrder': OLBConstants.DESCENDING_KEY,
            'offset': OLBConstants.DEFAULT_OFFSET,
            'limit': OLBConstants.PAGING_ROWS_LIMIT
        };
        this.receivedSortConfig = {
            'sortBy': 'transactionDate',
            'defaultSortBy': 'transactionDate',
            'order': OLBConstants.DESCENDING_KEY,
            'defaultOrder': OLBConstants.DESCENDING_KEY,
            'offset': OLBConstants.DEFAULT_OFFSET,
            'limit': OLBConstants.PAGING_ROWS_LIMIT
        };
    }

    inheritsFrom(PayAPerson_PresentationController, kony.mvc.Presentation.BasePresenter);

    PayAPerson_PresentationController.prototype.initializePresentationController = function() {};

    var frequencies = {
        Once: "i18n.transfers.frequency.once",
        Daily: "i18n.Transfers.Daily",
        Weekly: "i18n.Transfers.Weekly",
        BiWeekly: "i18n.Transfers.EveryTwoWeeks",
        Monthly: "i18n.Transfers.Monthly",
        Quarterly: "i18n.Transfers.Quaterly",
        HALF_YEARLY: "i18n.Transfers.HalfYearly",
        Yearly: "i18n.Transfers.Yearly"
    };

    var forHowLong = {
        ON_SPECIFIC_DATE: "i18n.transfers.lbxOnSpecificDate",
        NO_OF_RECURRENCES: "i18n.transfers.lblNumberOfRecurrences"
        // Will be added in R2
        // CONTINUE_UNTIL_CANCEL: "i18n.transfers.lblContinueUntilCancel"
    };

    var generateFromAccounts = function(fromAccount) {
        return [fromAccount.accountID, getFormattedAccountName(fromAccount)];
    };

    var getFormattedAccountName = function(account) {
        return CommonUtilities.getAccountDisplayName(account) + " " + CommonUtilities.getDisplayBalance(account);
    };


    PayAPerson_PresentationController.prototype.listboxForHowLong = function(dateString) {
        var list = [];
        for (var key in forHowLong) {
            if (forHowLong.hasOwnProperty(key)) {
                list.push([key, kony.i18n.getLocalizedString(forHowLong[key])]);
            }
        }
        return list;
    };

    PayAPerson_PresentationController.prototype.listboxFrequencies = function(dateString) {
        var list = [];
        for (var key in frequencies) {
            if (frequencies.hasOwnProperty(key)) {
                list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
            }
        }
        return list;
    };

    PayAPerson_PresentationController.prototype.getFormattedDateString = function(dateString) {
        return CommonUtilities.getFrontendDateString(dateString, CommonUtilities.getConfiguration("frontendDateFormat"));
    };

    /* 
    To get backend date string
    */
    PayAPerson_PresentationController.prototype.getBackendDate = function(dateString, dateFormat) {
        return CommonUtilities.getBackendDateFormat(dateString, dateFormat);
    };
    /*
    To format amount
    */
    PayAPerson_PresentationController.prototype.formatCurrency = function(amountString, isCurrencySumbolNotRequired) {
        return CommonUtilities.formatCurrencyWithCommas(amountString, isCurrencySumbolNotRequired);
    };

    /**
     * checkP2PEligibilityForUser - checks if pay a person is available for user and sets the iseligible and isactivated flag.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {String}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.checkP2PEligibilityForUser = function() {

        var completionCallback = function(commandResponse) {
            kony.mvc.MDAApplication.getSharedInstance().appContext.iseligible = false;
            kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated = false;
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                if (commandResponse.data.result === "Not Eligible" || commandResponse.data.result === "") {
                    kony.mvc.MDAApplication.getSharedInstance().appContext.iseligible = false;
                }
                if (commandResponse.data.result === "Activated") {
                    kony.mvc.MDAApplication.getSharedInstance().appContext.iseligible = true;
                    kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated = true;
                }
                if (commandResponse.data.result === "Not Activated") {
                    kony.mvc.MDAApplication.getSharedInstance().appContext.iseligible = true;
                    kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated = false;
                }
            } 
        };
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.checkP2PEligibilityForUser", {}, completionCallback));
    };

    /**
     * showPayAPersonActivation - presents Pay a person screen with activation screen.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {String}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showPayAPersonActivation = function() {
        var viewModel = {};
        viewModel.preActivation = true;
        this.loadComponents("frmPayAPerson");
        this.presentUserInterface("frmPayAPerson", viewModel);
    };

    /**
     * loadP2PSettingsScreen - presents Pay a person screen with  settings screen(part of edit settings flow).
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {String}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.loadP2PSettingsScreen = function(viewModel) {
        this.showProgressBar("frmPayAPerson");
        this.fetchAccountsAndShowPayPerson(viewModel);
    };

    /**
     * showPayAPersonNotEligibilityScreen - presents Pay a person screen with Not Eligible screen.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {String}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showPayAPersonNotEligibilityScreen = function() {
        var self = this;
        viewModel.notEligible = true;
        self.loadComponents("frmPayAPerson");
        this.presentUserInterface("frmPayAPerson", viewModel);
    };

    /**
     * updateP2PTranasctionWithPayee - updates a one time transaction with the newly added payee details at backend.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - response from one time payment and a payPersonJSON with recipient details in it.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.updateP2PTranasctionWithPayee = function(res) {
        var self = this;
        var requestObj = {};
        requestObj.transactionId = res.payPersonJSON.transactionId;
        requestObj.personId = res.status.data.PayPersonId;

        function completionCallBack(response) {
            var viewModel = {};
            viewModel.showAddRecipientAck = true;
            viewModel.status = res.status;
            viewModel.payPersonJSON = res.payPersonJSON;
            self.presentUserInterface("frmPayAPerson", viewModel);
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PayAPerson.updateP2PTransaction', requestObj, completionCallBack));
    };

    /**
     * createP2PPayee - creates a pay a person 
     * @member of {PayAPerson_PresentationController}
     * @param {object} - payPersonJSON with details {payeeName,payeeNickname,phone,email,secondaryPhone, secondaryEmail and primaryContactForSending}
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.createP2PPayee = function(payPersonJSON) {
        self = this;

        function completionCallBack(status) {
            var viewModel = {};
            viewModel.showAddRecipientAck = true;
            viewModel.status = status;
            if (payPersonJSON.transactionId != null && payPersonJSON.transactionId != "") {
                viewModel.payPersonJSON = payPersonJSON;
                self.updateP2PTranasctionWithPayee(viewModel);
            } else {
                viewModel.payPersonJSON = payPersonJSON;
                self.presentUserInterface("frmPayAPerson", viewModel);
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.createP2PPayee", payPersonJSON, completionCallBack));
    };

    /**
     * getP2PPayeesList - fetches pay a person recipients list and calls present user interface.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - View(it can be a result of another service call, for example send/request)
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.getP2PPayeesList = function(view, dataInputs) {
        var self = this,
            sortConig;

        function completionCallBack(response) {
            var viewModel = {};
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                viewModel.serverError = false;
                if (response.data.length > 0) {
                    viewModel.pagination = self.constants;
                    viewModel.sortInputs = tmpInputs;
                    self.constants.paginationText = kony.i18n.getLocalizedString("i18n.billpay.payees");
                    viewModel.status = response.status;
                    viewModel[view] = response.data;
                    self.getPaymentAccounts(viewModel);
                    self.constants.limit = response.data.length;
                } else {
                    if (self.constants.offset == 0) {
                        viewModel.noRecords = true;
                        self.getPaymentAccounts(viewModel);
                    } else {
                        self.constants.offset -= self.constants.paginationRowLimit;
                        self.getPaymentAccounts(viewModel);
                    }
                    self.hideProgressBar("frmPayAPerson");
                }
            } else {
                self.hideProgressBar("frmPayAPerson");
                viewModel.serverError = response.data.errmsg;
                self.getPaymentAccounts(viewModel);
            }
        }
        if (!dataInputs) {
            dataInputs = {};
            dataInputs.limit = self.constants.limit;
            dataInputs.offset = self.constants.offset;
        }
        self.constants.offset = dataInputs.offset;
        self.constants.limit = dataInputs.limit;
        if (view === "sendOrRequest") {
            sortConig = self.sentOrRequestSortConfig;
        } else {
            sortConig = self.manageRecipientSortConfig;
        }
        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, sortConig);
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.getP2PRecipientsList", tmpInputs, completionCallBack));
    };

    /**
     * getManagePayeesData - fetches default (0- paginationlimit(10) ) pay a person recipients list and shows manage recipients tab.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.getManagePayeesData = function() {
        self = this;
        self.showProgressBar("frmPayAPerson");
        self.constants.offset = 0;
        self.constants.limit = self.constants.paginationRowLimit;
        self.getP2PPayeesList("managePayeesData");
    };

    /**
     * getNextManagePayeesRecords -  fetches previous page pay a person recipients list and shows manage recipients tab.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.getNextManagePayeesRecords = function() {
        self = this;
        self.showProgressBar("frmPayAPerson");
        self.constants.offset = self.constants.limit + self.constants.offset;
        self.getP2PPayeesList("managePayeesData");
    };

    /**
     * getPreviousManagePayeesRecords - fetches next page pay a person recipients list and shows manage recipients tab.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.getPreviousManagePayeesRecords = function() {
        self = this;
        self.showProgressBar("frmPayAPerson");
        self.constants.limit = self.constants.paginationRowLimit;
        self.constants.offset = self.constants.offset - self.constants.limit;
        if (self.constants.offset < 0) self.constants.offset = 0;
        self.getP2PPayeesList("managePayeesData");
    };

    /**
     * getCurrentPageManagePayeesData - fetches current page  pay a person recipients list and calls present user interface.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.getCurrentPageManagePayeesData = function() {
        self = this;
        self.showProgressBar("frmPayAPerson");
        if (self.constants.limit == 1) {
            self.constants.limit = self.constants.paginationRowLimit;
            if (self.constants.offset > 0)
                self.constants.offset = self.constants.offset - self.constants.paginationRowLimit;
        }
        self.getP2PPayeesList("managePayeesData");
    };

    /**
     * deleteRecipient - deletes a payaperson recipient.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - payeeData
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.deleteRecipient = function(payeeData) {
        self = this;

        function deleteCompletionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                self.getCurrentPageManagePayeesData();
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.deleteRecipient", payeeData, deleteCompletionCallback.bind(this)));
    };

    /**
     * deactivateP2P - deactivates pay a person service for user.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.deactivateP2P = function() {
        var self = this;

        var completionCallback = function(commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                var viewModel = {};
                viewModel.showDeactivateP2PAcknowledgement = true;
                kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated = false;
                self.showPayPerson(viewModel);
            } else {
            	var viewModel = {};
            	viewModel.serverError = commandResponse.data.errmsg;
                self.presentUserInterface("frmPayAPerson", viewModel);
            }
        };
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.deactivateP2PForUser", {}, completionCallback));
    };

    /**
     * NavigateToPayAPerson - Navigates To PayAPerson screen and based on user activaion status. it presents him activation / not eligible/ send or request screen.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.NavigateToPayAPerson = function() {
        this.showProgressBar("frmPayAPerson");
        var self = this;
        var viewModel = {};
        self.loadComponents("frmPayAPerson");
        if (kony.mvc.MDAApplication.getSharedInstance().appContext.iseligible === false) {
            viewModel.notEligible = true;
            self.showPayPerson(viewModel);
        } else if (kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated === false) {
            viewModel.preActivation = true;
            self.showPayPerson(viewModel);
        } else {
            self.showSendOrRequestSegment();
        }
    };

    /**
     * getPayPersonViewActivity - fetches pay a person recipient activity list.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - payPersonId
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.getPayPersonViewActivity = function(payPersonId) {
        self = this;

        function onFetchComplete(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                var viewModel = {};
                viewModel.PayAPersonViewActivity = response.data;
                self.fetchAccountsAndShowPayPerson(viewModel);
            } else {
                self.hideProgressBar("frmPayAPerson");
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.payPersonActivity", {
            "personId": payPersonId
        }, onFetchComplete.bind(this)));
    };

    /**
     * editRecipient - This method is used to edit a pay a person recipient.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - payeeData with details like (id, nickName, firstName, phone, email, secondaryEmail, secondaryPhoneNumber,primaryContactForSending)
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.editRecipient = function(payeeData) {
        self = this;
        var payeeJSON = {
            "PayPersonId": payeeData.id,
            "nickName": payeeData.nickName,
            "firstName": payeeData.firstName,
            "phone": payeeData.phone,
            "email": payeeData.email,
            "secondaryEmail": payeeData.secondaryEmail,
            "secondaryPhoneNumber": payeeData.secondaryPhoneNumber,
            "primaryContactForSending": payeeData.primaryContactForSending
        };

        function editCompletionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                var viewModel = {};
                viewModel.showEditRecipientAck = true;
                viewModel.status = status;
                viewModel.payPersonJSON = payPersonJSON;
                self.presentUserInterface("frmPayAPerson", viewModel);
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.editPayAPersonRecipient", payeeJSON, editCompletionCallback.bind(this)));
    };

    /**
     * getP2PPayeesList - fetches pay a person recipients list and calls present user interface.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - View(it can be a result of another service call, for example send/request)
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.noThanksBtnClicked = function() {
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountsModule.presentationController.showAccountsDashboard();
    };


    /**
     * ActivateP2P - This method is used to activate pay a person Service for a user.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - userSettings preferencesObject.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.ActivateP2P = function(preferencesObject) {
        var self = this;
        var ActivationCompletionCallback = function(commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                if (commandResponse.data.result === "success") {
                    kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated = true;
                    self.updateP2PPreferencesForUser(preferencesObject);
                }
            } else {
                var viewModel = {};
                viewModel.serverError = commandResponse.data.errmsg;
                self.presentUserInterface("frmPayAPerson", viewModel);
            }
        };
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.activateP2PForUser", {}, ActivationCompletionCallback));
    };

    /**
     * updateP2PPreferencesForUser - This method is used to update pay a person settings .
     * @member of {PayAPerson_PresentationController}
     * @param {object} - userSettings preferencesObject.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.updateP2PPreferencesForUser = function(preferencesObject) {
        var self = this;
        var param = {
            "default_to_account_p2p": preferencesObject.defaultToAccount,
            "default_from_account_p2p": preferencesObject.defaultFromAccount
        };
        var completionCallback = function(commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.default_to_account_p2p = preferencesObject.defaultToAccount;
                kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.default_from_account_p2p = preferencesObject.defaultFromAccount;
                self.showProgressBar("frmPayAPerson");
                self.showSendOrRequestSegment();
            } else {
				var viewModel = {};
				viewModel.serverError = commandResponse.data.errmsg;
				self.fetchAccountsAndShowPayPerson(viewModel);
            }
        };
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.updatePreferencesForP2PUser", param, completionCallback));
    };

    /**
     * showView - This method is used to call presentUserInterface with the provided viewModel and data
     * @member of {PayAPerson_PresentationController}
     * @param {object} - formName and data
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showView = function(frm, data) {
        this.presentUserInterface(frm, data);
    };

    /**
     * loadComponents - This method is used to load pay a person components like hamburger and sidemenu
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.loadComponents = function(frm) {
        var scopeObj = this;
        var howToShowHamburgerMenu = function(sideMenuViewModel) {
            scopeObj.showView(frm, {
                "sideMenu": sideMenuViewModel
            });
        };

        scopeObj.SideMenu.init(howToShowHamburgerMenu);

        var presentTopBar = function(topBarViewModel) {
            scopeObj.showView(frm, {
                "topBar": topBarViewModel
            });
        };
        scopeObj.TopBar.init(presentTopBar);
    };

    /**
     * showProgressBar - This method is used to show ProgressBar.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showProgressBar = function(frm) {
        var self = this;
        self.showView(frm, {
            "ProgressBar": {
                show: true
            }
        });
    };

    /**
     * hideProgressBar - This method is used to hide ProgressBar
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.hideProgressBar = function(frm) {
        var self = this;
        self.showView(frm, {
            "ProgressBar": {
                show: false
            }
        });
    };

    /**
     * fetchAccountsAndShowPayPerson - This method is used to fetch payment Accounts and show Pay a person
     * @member of {PayAPerson_PresentationController}
     * @param {object} - viewModel
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.fetchAccountsAndShowPayPerson = function(viewModel) {
        this.getPaymentAccounts(viewModel);
    };

    /**
     * showPayPerson - This method is used to show pay a person along with payment accounts.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - viewModel
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showPayPerson = function(viewModel) {
        this.presentUserInterface("frmPayAPerson", viewModel);
    };

    /**
     * getPaymentAccounts - This method is used to fetch payment accounts of user.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - viewModel
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.getPaymentAccounts = function(viewModel) {
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
        var self = this;

        function getAccountsCompletionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                //Filtering out accounts which do not support transfer
                var accounts = response.data;
                var paymentAccounts = [];
                for (var index in accounts) {
                    if (accounts[index]['supportTransferFrom'] == 1)
                        paymentAccounts.push(accounts[index]);
                }
                viewModel.paymentAccounts = paymentAccounts;
                viewModel.sendPayemntAccounts = (paymentAccounts).map(generateFromAccounts);
                self.showPayPerson(viewModel);
            } else if (response.status == kony.mvc.constants.STATUS_FAILURE) {
                self.showPayPerson(viewModel);
            }
        }
        accountsModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, getAccountsCompletionCallback));
    };

    /**
     * showSendOrRequestSegment - This method is used to fetch pay a person recipients list and show send/ request segment
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showSendOrRequestSegment = function() {
        var self = this;
        self.constants.offset = 0;
        self.constants.limit = self.constants.paginationRowLimit;
        self.getP2PPayeesList("sendOrRequest");
    };

    /**
     * showNextSendOrRequestSegment - This method is used to fetch  next page pay a person recipients list and show send/ request segment
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showNextSendOrRequestSegment = function() {
        var self = this;
        self.constants.offset = self.constants.offset + self.constants.limit;
        self.getP2PPayeesList("sendOrRequest");
    };

    /**
     * showPreviousSendOrRequestSegment - This method is used to fetch previous page pay a person recipients list and show send/ request segment.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showPreviousSendOrRequestSegment = function() {
        var self = this;
        self.constants.limit = self.constants.paginationRowLimit;
        self.constants.offset -= self.constants.limit;
        if (self.constants.offset < 0)
            self.constants.offset = 0;
        self.getP2PPayeesList("sendOrRequest");
    };

    /**
     * showAddRecipient - This method is used to show add recipient screen of pay a person.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showAddRecipient = function() {
        var self = this;
        var viewModel = {};
        viewModel.addRecipient = true;
        self.fetchAccountsAndShowPayPerson(viewModel); 
    };

    /**
     * fetchRequests - This method is used to fetch requests for a pay a person user
     * @member of {PayAPerson_PresentationController}
     * @param {object} - pay a person presentation Controller instance (optional)
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.fetchRequests = function(sender) {
        if (sender)
            var self = sender;
        else
            var self = this;
        self.showProgressBar("frmPayAPerson");
        var viewModel = {};

        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                var mySentRequests = response.data;
                viewModel.myRequests = mySentRequests;
                viewModel.serverError = false;
                self.fetchAccountsAndShowPayPerson(viewModel);
            } else {
                self.hideProgressBar("frmPayAPerson");
                viewModel.serverError = response.data.errmsg;
                self.fetchAccountsAndShowPayPerson(viewModel);
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PayAPerson.getP2PRequestsForUser', {}, completionCallback));
    };

    /**
     * createRequestMoney - This method is used to create a request money transaction.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - requestObj which contains transaction details.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.createRequestMoney = function(requestObj) {
        var self = this;
        self.showProgressBar("frmPayAPerson");

        function completionCallBack(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                var viewModel = {};
                viewModel.showRequestMoneyAck = true;
                viewModel.status = response;
                viewModel.requestObj = requestObj;
                self.presentUserInterface("frmPayAPerson", viewModel);
            } else {
                self.hideProgressBar("frmPayAPerson");
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.requestMoney", requestObj, completionCallBack));
    };
	
  	PayAPerson_PresentationController.prototype.checkMFAP2PSendMoney = function(requestObj) {
     var self =this;
      if((CommonUtilities.getConfiguration('isMFAEnabledForP2P') === "true") && Number(requestObj.amount)>Number(CommonUtilities.getConfiguration('minimumAmountForMFAP2P'))  && !CommonUtilities.isCSRMode())
      {
        var mfaModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('MultiFactorAuthenticationModule');
        mfaModule.presentationController.startCVVFlow({
          hamburgerSelection: {selection1:"PayAPerson", selection2: "SendOrRequest"},
          breadcrumb: kony.i18n.getLocalizedString("i18n.p2p.PayAPerson"),
          termsAndConditions: kony.i18n.getLocalizedString("i18n.PayAPerson.TermsAndConditions"),
          cancelCallback: self.showSendOrRequestSegment.bind(self),
          successCallback: self.createP2PSendMoney.bind(self, requestObj)
        });
      }else
      {
        self.createP2PSendMoney(requestObj);
      } 
    };	
  
    /**
     * createP2PSendMoney - This method is used to create send money transaction.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - requestObj which contains transaction details.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.createP2PSendMoney = function(requestObj) {
        var self = this;
        self.showProgressBar("frmPayAPerson");

        function completionCallBack(responseData) {
			
			if(responseData.status == kony.mvc.constants.STATUS_SUCCESS)
			{
			var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
            accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccountById", requestObj.fromAccountNumber, callback));

            function callback(response) {
                if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                    var viewModel = {};
					viewModel.serverError = false;
                    viewModel.showRequestSendMoneyAck = true;
                    viewModel.status = responseData;
                    viewModel.requestObj = requestObj;
                    viewModel.accountBalance = response.data.availableBalance;
                    viewModel.accountName = response.data.accountName;
                    self.presentUserInterface("frmPayAPerson", viewModel);
                } else {
                    self.hideProgressBar("frmPayAPerson");
                }
            }
			}else{
				var viewModel = {};
				viewModel.serverError = responseData.data.errmsg;
				if (CommonUtilities.getConfiguration('serviceFeeFlag') === "true") {
					requestObj.amount  = String(Number(requestObj.amount) - Number(requestObj.fee));
				}
				viewModel.sendMoneyData = requestObj;
				self.presentUserInterface("frmPayAPerson", viewModel);
				self.hideProgressBar("frmPayAPerson");
			}
        }
        requestObj.scheduledDate = CommonUtilities.sendDateToBackend(requestObj.scheduledDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat'));
        if (requestObj.frequencyType == kony.i18n.getLocalizedString(frequencies.Once)) {
            requestObj.numberOfRecurrences = "";
            requestObj.frequencyStartDate = "";
            requestObj.frequencyEndDate = "";
        } else if (requestObj.hasHowLong == "NO_OF_RECURRENCES") {
            requestObj.frequencyStartDate = "";
            requestObj.frequencyEndDate = "";
        } else if (requestObj.hasHowLong == "ON_SPECIFIC_DATE") {
            requestObj.numberOfRecurrences = "";
            requestObj.frequencyStartDate = CommonUtilities.sendDateToBackend(requestObj.frequencyStartDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat'));
            requestObj.frequencyEndDate = CommonUtilities.sendDateToBackend(requestObj.frequencyEndDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat'));
        }
        if (requestObj.transactionId != null && requestObj.transactionId != "") {
            self.businessController.execute(new kony.mvc.Business.Command('com.kony.PayAPerson.updateP2PTransaction', requestObj, completionCallBack));
        } else {
            if (CommonUtilities.getConfiguration('serviceFeeFlag') === "true") {
                requestObj.fee = CommonUtilities.getConfiguration('p2pServiceFee');
            }
            self.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.createP2PTransaction", requestObj, completionCallBack));
        }
    };

    /**
     * fetchSentTransactions - This method is used to fetch sent transactions for user.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - dataInputs (sort configs and pagination values)
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.fetchSentTransactions = function(dataInputs) {
        var self = this;

        function completionCallBack(response) {
            var viewModel = {};
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                viewModel.serverError = false;
                viewModel.response = response.status;
                self.constants.paginationText = kony.i18n.getLocalizedString("i18n.PFM.TransactionsCapsOn")
                viewModel.pagination = self.constants;
                viewModel.sentTransactions = response.data;
                viewModel.sortInputs = tmpInputs;
                if (response.data.length > 0) {
                    self.fetchAccountsAndShowPayPerson(viewModel);
                    self.constants.limit = response.data.length;
                    viewModel.pagination = self.constants;
                } else {
                    self.hideProgressBar("frmPayAPerson");
                }
            } else {
                self.hideProgressBar("frmPayAPerson");
                viewModel.serverError = response.data.errmsg;
                self.fetchAccountsAndShowPayPerson(viewModel);
            }
        }
        if (!dataInputs) {
            dataInputs = {};
            dataInputs.limit = self.constants.limit;
            dataInputs.offset = self.constants.offset;
        }
        self.constants.offset = dataInputs.offset;
        self.constants.limit = dataInputs.limit;
        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, self.sentSortConfig);
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.getSentP2PTransactions", tmpInputs, completionCallBack));
    };

    /**
     * showSentSegment - This method is used to fetch default page sent transactions.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showSentSegment = function() {
        var self = this;
        self.constants.offset = 0;
        self.constants.limit = self.constants.paginationRowLimit;
        self.fetchSentTransactions();
    };

    /**
     * showNextSentSegment - This method is used to fetch next page sent transactions.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showNextSentSegment = function() {
        var self = this;
        self.constants.offset = self.constants.offset + self.constants.limit;
        self.fetchSentTransactions();
    };

    /**
     * showPreviousSentSegment - This method is used to fetch previous page sent transactions.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - userSettings preferencesObject.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showPreviousSentSegment = function() {
        var self = this;
        self.constants.limit = self.constants.paginationRowLimit;
        self.constants.offset -= self.constants.limit;
        if (self.constants.offset < 0) self.constants.offset = 0;
        self.fetchSentTransactions();
    };

    /**
     * sendRemainder - This method is used to create a remainder.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - remainderObj
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.sendRemainder = function(constructRemainderObj) {
        var self = this;
        self.showProgressBar("frmPayAPerson");
        constructRemainderObj.p2pRequiredDate = CommonUtilities.getBackendDateFormat(constructRemainderObj.p2pRequiredDate, "dd/mm/yyyy");

        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                var viewModel = {};
                viewModel.data = response.data;
                viewModel.requestObj = constructRemainderObj;
                viewModel.showRemainderAck = true;
                self.presentUserInterface("frmPayAPerson", viewModel);
            } else {
                self.hideProgressBar("frmPayAPerson");
            }

        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PayAPerson.sendReminder', constructRemainderObj, completionCallback));
    };

    /**
     * ActivateP2P - This method is used to perform search in pay a person screen.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - data which contains keys like searchKeyword.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.searchPayAPerson = function(data) {
        var self = this;
        if (data && data.searchKeyword.length >= 0) {
            var searchInputs = {
                'searchString': data.searchKeyword
            };
            var viewModel = {};
            var fetchSearchPayAPersonCallback = function(commandResponse) {

                if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                    viewModel.searchPayAPerson = {
                        payAPersonData: commandResponse.data,
                        searchInputs: searchInputs
                    };
                } else {
                    self.hideProgressBar("frmPayAPerson");
                    viewModel.serverError = commandResponse.data.errmsg;
                }
                self.presentUserInterface("frmPayAPerson", viewModel);
            };
            self.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.getP2PRecipientsList", {
                "searchString": searchInputs.searchString
            }, fetchSearchPayAPersonCallback));
        } 
    };

    /**
     * CancelRequestMoney - This method is used to cancel a requestMoney transaction.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - reqObject.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.CancelRequestMoney = function(sender, reqObject) {
        var self = this;
        self.showProgressBar("frmPayAPerson");

        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                self.showProgressBar("frmPayAPerson");
                self.fetchRequests(sender);
            } else {
                self.hideProgressBar("frmPayAPerson");
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PayAPerson.cancelRequestMoney', reqObject, completionCallback));
    };

    /**
     * fetchReceivedTransactions - This method is used to fetch received transactions.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - dataInputs
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.fetchReceivedTransactions = function(dataInputs) {
        var self = this;

        function completionCallBack(response) {
            var viewModel = {};
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                viewModel.serverError = false;
                viewModel.response = response.status;
                self.constants.paginationText = "Transactions";
                viewModel.pagination = self.constants;
                viewModel.receivedTransactions = response.data;
                viewModel.sortInputs = tmpInputs;
                self.fetchAccountsAndShowPayPerson(viewModel);
                self.constants.limit = response.data.length;
            } else {
                self.hideProgressBar("frmPayAPerson");
                viewModel.serverError = response.data.errmsg;
                self.fetchAccountsAndShowPayPerson(viewModel);
            }
        }
        if (!dataInputs) {
            dataInputs = {};
            dataInputs.limit = self.constants.limit;
            dataInputs.offset = self.constants.offset;
        }
        self.constants.offset = dataInputs.offset;
        self.constants.limit = dataInputs.limit;
        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, self.receivedSortConfig);
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.PayAPerson.getReceivedP2PTransactions", tmpInputs, completionCallBack));
    };

    /**
     * showReceivedSegment - This method is used to fetch received requests and show received tab in pay a person.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showReceivedSegment = function() {
        var self = this;
        self.constants.offset = 0;
        self.constants.limit = self.constants.paginationRowLimit;
        self.fetchReceivedTransactions();
    };

    /**
     * showNextReceivedSegment - This method is used to fetch next page received requests and show received tab in pay a person.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showNextReceivedSegment = function() {
        var self = this;
        self.constants.offset = self.constants.offset + self.constants.limit;
        self.fetchReceivedTransactions();
    };

    /**
     * showPreviousReceivedSegment - This method is used to fetch previous page received requests and show received tab in pay a person.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - NONE.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showPreviousReceivedSegment = function() {
        var self = this;
        self.constants.offset -= self.constants.limit;
        self.constants.limit = self.constants.paginationRowLimit;
        if (self.constants.offset < 0) self.constants.offset = 0;
        self.fetchReceivedTransactions();
    };

    /**
     * showPayAPerson - This method acts as an entry point for pay a person.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - data(view).
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.showPayAPerson = function(data) {
        var self = this;
        self.showProgressBar("frmPayAPerson");
        this.loadComponents("frmPayAPerson");
        if (kony.mvc.MDAApplication.getSharedInstance().appContext.iseligible === false) {
            viewModel.notEligible = true;
            self.showPayPerson(viewModel);
            return;
        }
        if (kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated === true) {
            if (data.show == "SendMoney" && data) {
                self.onSendMoney(data);
            } else if (data.show == "ReceivedTransactions" && data) {
                self.fetchReceivedTransactions();
            } else if (data.show == "SentTransactions" && data) {
                self.fetchSentTransactions();
            } else if (data.show == "P2PRequestMoneyForUser" && data) {
                self.fetchRequests(self);
            } else if (data.show == "managePayeesData" && data) {
                self.getP2PPayeesList("managePayeesData");
            } else if (data.show == "RequestMoney" && data) {
                self.onRequestMoney(data);
            } else if (data && data.show == "addRecipient") {
                self.showAddRecipient();
            } else if(data){
                self.getP2PPayeesList("sendOrRequest");
            }
        } else {
            self.showPayAPersonActivation();
        }
    };

    /**
     * onSendMoney - This method is used to show send money.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - data.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.onSendMoney = function(data) {
        var self = this;
        var viewModel = {};
        if (data["amount"])
            data["amount"] = "" + data["amount"];
        self.showProgressBar("frmPayAPerson");
        viewModel.sendMoneyData = data;
        self.getPaymentAccounts(viewModel);
    };

    /**
     * onRequestMoney - This method is used to show request money.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - data.
     * @returns {object}  - VOID
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.onRequestMoney = function(data) {
        var self = this;
        var viewModel = {};
        self.showProgressBar("frmPayAPerson");
        viewModel.requestMoneyData = data;
        self.getPaymentAccounts(viewModel);
    };

    /**
     * validatePayAPersonAmount - This method is used to validate the amount.
     * @member of {PayAPerson_PresentationController}
     * @param {amount} - amount to be validated.
     * @returns {object}  - contains the isAmountValid and errMsg
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.validatePayAPersonAmount = function(amount) {
        var minP2PLimit = parseFloat(CommonUtilities.getConfiguration("minP2PLimit"));
        var maxP2PLimit = parseFloat(CommonUtilities.getConfiguration("maxP2PLimit"));
        var result = {
            isAmountValid : false
        };
        if(amount < minP2PLimit) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.minTransactionError") + " " + CommonUtilities.formatCurrencyWithCommas(minP2PLimit);
        } else if(amount > maxP2PLimit) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.maxTransactionError") + " " + CommonUtilities.formatCurrencyWithCommas(maxP2PLimit);
        } else {
            result.isAmountValid = true;
        }
        return result;
    };

    /**
     * cancelTransaction - This method is used to cancel a transaction.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - transaction object.
     * @returns {VOID}
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.cancelTransaction = function(transaction){
        var self = this;
        function cancelTransactionCallback(response){
            if(response.status === kony.mvc.constants.STATUS_SUCCESS){
                self.showSentSegment();
            }else{
                var viewModel = {};
                viewModel.serverError = response.data.errmsg;
                self.presentUserInterface('frmPayAPerson', viewModel);
            } 
        }
        this.businessController.execute(new kony.mvc.Business.Command('com.kony.PayAPerson.cancelTransaction', transaction, cancelTransactionCallback));
    };
  
    /**
     * cancelTransactionOccurrence - This method is used to cancel a transaction occurrence.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - transaction object.
     * @returns {VOID}
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.cancelTransactionOccurrence = function(transaction){
        var self = this;
        function cancelTransactionOccurrenceCallback(response){
            if(response.status === kony.mvc.constants.STATUS_SUCCESS){
                self.showSentSegment();
            }else{
                var viewModel = {};
                viewModel.serverError = response.data.errmsg;
                self.presentUserInterface('frmPayAPerson', viewModel);
            } 
        }
        this.businessController.execute(new kony.mvc.Business.Command('com.kony.PayAPerson.cancelTransactionOccurrence', transaction, cancelTransactionOccurrenceCallback));
    };
  
    /**
     * cancelTransactionSeries - This method is used to cancel a transaction series.
     * @member of {PayAPerson_PresentationController}
     * @param {object} - transaction object.
     * @returns {VOID}
     * @throws {}
     */
    PayAPerson_PresentationController.prototype.cancelTransactionSeries = function(transaction){
        var self = this;
        function cancelTransactionSeriesCallback(response){
            if(response.status === kony.mvc.constants.STATUS_SUCCESS){
                self.showSentSegment();
            }else{
                var viewModel = {};
                viewModel.serverError = response.data.errmsg;
                self.presentUserInterface('frmPayAPerson', viewModel);
            } 
        }
        this.businessController.execute(new kony.mvc.Business.Command('com.kony.PayAPerson.cancelTransactionSeries', transaction, cancelTransactionSeriesCallback));
    };
    
    return PayAPerson_PresentationController;
});