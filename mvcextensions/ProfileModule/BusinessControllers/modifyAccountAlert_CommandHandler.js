define([], function() {

  function Profile_modifyAccountAlert_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Profile_modifyAccountAlert_CommandHandler, kony.mvc.Business.CommandHandler);

  Profile_modifyAccountAlert_CommandHandler.prototype.execute = function(command){
    var self = this;
    var params = {
      "userName": command.context.userName,
      "accountNumber":command.context.accountNumber,
      "checkNumber":command.context.checkNumber,
      "days":command.context.days,
      "expirationDate":command.context.expirationDate,
      "notifyAmount":command.context.notifyAmount,
      "productId":command.context.productId,
      "accountType":command.context.productType,
      "subtype":command.context.subtype,
      "text":command.context.text,
      "email":command.context.email,
      "push":command.context.push,
      "type":command.context.type
    };
    kony.print("Profile_modifyAccountAlert_CommandHandler input params: "+JSON.stringify(params));
    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }

    try {
      var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("UserAccountAlerts");
      userModel.customVerb("modifyAccountAlert", params, completionCallBack);
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  Profile_modifyAccountAlert_CommandHandler.prototype.validate = function(){
  };

  return Profile_modifyAccountAlert_CommandHandler;

});