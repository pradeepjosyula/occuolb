define([], function() {

    function Profile_verifyAnswerSecurityQuestions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
    
    inheritsFrom(Profile_verifyAnswerSecurityQuestions_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Profile_verifyAnswerSecurityQuestions_CommandHandler.prototype.execute = function(command){
      var self = this;
      var record={};
      record.securityQuestions = command.context;
      record.userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
        function completionCallBack(status, response, err) {
             if (status === kony.mvc.constants.STATUS_SUCCESS) {
                 self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
             } else {
                 self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
             }
         }
         try {
             var saveUserSecurityQuestionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SecurityQuestions");                
             saveUserSecurityQuestionsModel.customVerb("verifyCustomerSecurityQuestions", record, completionCallBack);
         } catch (err) {
             self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
         }        
    };
    
    Profile_verifyAnswerSecurityQuestions_CommandHandler.prototype.validate = function(){
        
    };
    
    return Profile_verifyAnswerSecurityQuestions_CommandHandler;
    
});