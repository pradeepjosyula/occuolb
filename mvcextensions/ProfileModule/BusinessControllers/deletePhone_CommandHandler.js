define([], function() {
    
          function Profile_deletePhone_CommandHandler(commandId) {
            kony.mvc.Business.CommandHandler.call(this, commandId);
        }
        
        inheritsFrom(Profile_deletePhone_CommandHandler, kony.mvc.Business.CommandHandler);
      
          Profile_deletePhone_CommandHandler.prototype.execute = function(command){
        var self = this;
           function completionCallBack(status, response, err) {
                if (status === kony.mvc.constants.STATUS_SUCCESS) {
                    self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
                } else {
                    self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                }
            }
            try {
                var phoneModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Phone");                
                phoneModel.customVerb("deletePhone", {id: command.context}, completionCallBack);
            } catch (err) {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }	
        };
        
        Profile_deletePhone_CommandHandler.prototype.validate = function(){
            
        };
        
        return Profile_deletePhone_CommandHandler;
        
    });