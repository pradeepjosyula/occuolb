define([], function() {

  	function Profile_fetchProfileAlerts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_fetchProfileAlerts_CommandHandler, kony.mvc.Business.CommandHandler);
  
   Profile_fetchProfileAlerts_CommandHandler.prototype.execute = function(command){
    var self=this;      
    function completionCallBack(status, response, error) {
      if (status === kony.mvc.constants.STATUS_SUCCESS) 
        self.sendResponse(command, status, response);
      else 
        self.sendResponse(command, status, error);
    }

    try{
      var profileAlerts = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("UserAlerts");
      profileAlerts.customVerb("getAllAlerts",command.context, completionCallBack);
    }catch(e){
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE,{});
    }
   };
	
	Profile_fetchProfileAlerts_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_fetchProfileAlerts_CommandHandler;
    
});