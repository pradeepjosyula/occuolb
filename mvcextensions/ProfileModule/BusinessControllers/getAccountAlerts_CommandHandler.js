define([], function() {

  function Profile_getAccountAlerts_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Profile_getAccountAlerts_CommandHandler, kony.mvc.Business.CommandHandler);

  Profile_getAccountAlerts_CommandHandler.prototype.execute = function(command){
    var self = this;
	var params = {
      "userName": command.context.userName
    };
    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }
    
    try {
      var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("UserAccountAlerts");
      userModel.customVerb("getAccountAlerts", params, completionCallBack);
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  Profile_getAccountAlerts_CommandHandler.prototype.validate = function(){
  };

  return Profile_getAccountAlerts_CommandHandler;

});