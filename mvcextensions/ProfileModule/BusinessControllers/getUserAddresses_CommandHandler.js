define([], function() {
    
          function Profile_getUserAddresses_CommandHandler(commandId) {
            kony.mvc.Business.CommandHandler.call(this, commandId);
        }
        
        inheritsFrom(Profile_getUserAddresses_CommandHandler, kony.mvc.Business.CommandHandler);
      
          Profile_getUserAddresses_CommandHandler.prototype.execute = function(command){
            var self = this;
          var params = {};
           function completionCallBack(status, response, err) {
                if (status === kony.mvc.constants.STATUS_SUCCESS) {
                    self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
                } else {
                    self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                }
            }
            try {
                var userAddressModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");                
                userAddressModel.customVerb("getAllAddress", params, completionCallBack);
            } catch (err) {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }	
        };
        
        Profile_getUserAddresses_CommandHandler.prototype.validate = function(){
            
        };
        
        return Profile_getUserAddresses_CommandHandler;
        
    });