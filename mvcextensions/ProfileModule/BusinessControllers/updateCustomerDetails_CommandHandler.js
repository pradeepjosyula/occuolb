define([], function() {

  	function Profile_updateCustomerDetails_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_updateCustomerDetails_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_updateCustomerDetails_CommandHandler.prototype.execute = function(command){
		var self = this;
        var params=command.context;

        function completionCallBack(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, err);
            }
        }
        try {
            var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
            userModel.customVerb("updateCustomerDetails", params, completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
    Profile_updateCustomerDetails_CommandHandler.prototype.validate = function() {};
    return Profile_updateCustomerDetails_CommandHandler;
    
});