define([], function() {

  	function Profile_checkSecurityQuestions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_checkSecurityQuestions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_checkSecurityQuestions_CommandHandler.prototype.execute = function(command){
		var self = this;
  
    function completionCallBack(status,response,err){
      if(status===kony.mvc.constants.STATUS_SUCCESS){
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS,response);
      }
      else {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS,err);
      }
    }

    try{
      var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
      userModel.customVerb("checkSecurityQuestionStatus",{},completionCallBack);
    }
    catch(err){
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
    };
	
	Profile_checkSecurityQuestions_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_checkSecurityQuestions_CommandHandler;
    
});