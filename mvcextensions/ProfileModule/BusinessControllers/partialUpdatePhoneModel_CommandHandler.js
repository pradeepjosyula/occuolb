define([], function() {
    
          function Profile_partialUpdatePhoneModel_CommandHandler(commandId) {
            kony.mvc.Business.CommandHandler.call(this, commandId);
        }
        
        inheritsFrom(Profile_partialUpdatePhoneModel_CommandHandler, kony.mvc.Business.CommandHandler);
      
          Profile_partialUpdatePhoneModel_CommandHandler.prototype.execute = function(command){
            var self = this;
            var context = command.context;
            function completionCallback(status, data, error) {
                self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
            }
            try {
                var params = context.partialUpdateObject;
                params.id = context.phoneId;
                var phoneModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Phone');
                phoneModel.customVerb("updatePhone", params,  completionCallback);
            } catch (err) {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        };
        
        Profile_partialUpdatePhoneModel_CommandHandler.prototype.validate = function(){
            
        };
        
        return Profile_partialUpdatePhoneModel_CommandHandler;
        
    });