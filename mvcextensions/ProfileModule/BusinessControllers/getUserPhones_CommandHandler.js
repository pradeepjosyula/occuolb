define([], function () {
    
        function Profile_getUserPhones_CommandHandler(commandId) {
            kony.mvc.Business.CommandHandler.call(this, commandId);
        }
    
        inheritsFrom(Profile_getUserPhones_CommandHandler, kony.mvc.Business.CommandHandler);
    
        Profile_getUserPhones_CommandHandler.prototype.execute = function (command) {
            var self = this;
            function completionCallBack(status, response, err) {
                if (status === kony.mvc.constants.STATUS_SUCCESS) {
                    self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
                } else {
                    self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                }
            }
            try {
                var PhoneModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Phone");
                PhoneModel.customVerb("getAllPhones", {} ,completionCallBack);
            } catch (err) {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        };
    
        Profile_getUserPhones_CommandHandler.prototype.validate = function () {
    
        };
    
        return Profile_getUserPhones_CommandHandler;
    
    });