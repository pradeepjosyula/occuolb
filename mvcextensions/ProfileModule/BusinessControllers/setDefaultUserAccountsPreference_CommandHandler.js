define([], function() {

  	function Profile_setDefaultUserAccountsPreference_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_setDefaultUserAccountsPreference_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_setDefaultUserAccountsPreference_CommandHandler.prototype.execute = function(command){
		var self = this;
		function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
		var accountModel =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
		 accountModel.customVerb('updateUserAccountSettings',command.context,completionCallBack);
		
		
    };
	
	Profile_setDefaultUserAccountsPreference_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_setDefaultUserAccountsPreference_CommandHandler;
    
});