define([], function() {
    function Profile_updatePhone_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
    inheritsFrom(Profile_updatePhone_CommandHandler, kony.mvc.Business.CommandHandler);
    Profile_updatePhone_CommandHandler.prototype.execute = function(command) {
        var self = this;
        var context = command.context;
        var params = {
            id: context.id,
            countryType: context.countryType,
            extension: context.extension,
            isPrimary: context.isPrimary,
            phoneNumber: context.phoneNumber,
            receivePromotions: context.receivePromotions,
            type: context.type
        };

        function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var phonrModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Phone");
            phonrModel.customVerb("updatePhone", params, completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
    Profile_updatePhone_CommandHandler.prototype.validate = function() {};
    return Profile_updatePhone_CommandHandler;
});