define([], function() {
    
          function Profile_updateUserAddress_CommandHandler(commandId) {
            kony.mvc.Business.CommandHandler.call(this, commandId);
        }
        
        inheritsFrom(Profile_updateUserAddress_CommandHandler, kony.mvc.Business.CommandHandler);
      
          Profile_updateUserAddress_CommandHandler.prototype.execute = function(command){
        var self = this;
        var context = command.context;
          var params = {
              addressId: context.addressId,
            addressLine1: context.addressLine1,
            addressLine2: context.addressLine2,
            country: context.country,
            city: context.city,
            zipcode: context.zipcode,
            isPreferredAddress: context.isPreferredAddress,
            addressType: context.addressType
          };
           function completionCallBack(status, response, err) {
                if (status === kony.mvc.constants.STATUS_SUCCESS) {
                    self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
                } else {
                    self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                }
            }
            try {
                var userAddressModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");                
                userAddressModel.customVerb("updateAddress", params, completionCallBack);
            } catch (err) {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }	
        };
        
        Profile_updateUserAddress_CommandHandler.prototype.validate = function(){
            
        };
        
        return Profile_updateUserAddress_CommandHandler;
        
    });