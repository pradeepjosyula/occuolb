define([], function() {

  	function Profile_setDefaultUserAccounts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_setDefaultUserAccounts_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_setDefaultUserAccounts_CommandHandler.prototype.execute = function(command){
		var self = this;

        function completionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
            var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('User');
            var userObj = new userModel();
            for (var keys in command.context) {
                userObj[keys] = command.context[keys]==="undefined"?"null":command.context[keys];
            }
            userObj.partialUpdate(completionCallback);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
	
	Profile_setDefaultUserAccounts_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_setDefaultUserAccounts_CommandHandler;
    
});