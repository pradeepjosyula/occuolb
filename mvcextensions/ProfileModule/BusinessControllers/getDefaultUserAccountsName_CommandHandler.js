define([], function() {

  	function Profile_getDefaultUserAccountsName_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_getDefaultUserAccountsName_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_getDefaultUserAccountsName_CommandHandler.prototype.execute = function(command){
		var self=this;
		var cri = [];
        var count = 0;
        var defaultNames={};
        var accountNumbers=command.context;
        
		var criteria = kony.mvc.Expression.and.apply(this,cri);
		function getAllCompletionCallback(status, data, error){
				if(status==kony.mvc.constants.STATUS_SUCCESS){
					for(var keys in accountNumbers){
						for(var i in data){
							if(accountNumbers[keys]===data[i].accountID){
								defaultNames[keys]=data[i].accountName;
								break;
							}
						}
					}
					self.sendResponse(command,status,defaultNames);
				}else{
					self.sendResponse(command,status,error);
				}
			
		}
		try{
			if(count===0){
				self.sendResponse(command,kony.mvc.constants.STATUS_SUCCESS, defaultNames);
			}else{
				var accountModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
			    accountModel.getByCriteria(criteria ,getAllCompletionCallback);
			}
		}catch(err){
			self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE, err);
		}
    };
	
	Profile_getDefaultUserAccountsName_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_getDefaultUserAccountsName_CommandHandler;
    
});