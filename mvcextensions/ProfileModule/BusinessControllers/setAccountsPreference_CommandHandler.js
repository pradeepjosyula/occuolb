define([], function() {

    function Profile_setAccountsPreference_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Profile_setAccountsPreference_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Profile_setAccountsPreference_CommandHandler.prototype.execute = function(command){
        var self = this;
        var data=command.context;
        data = JSON.stringify(data);
    data = data.replace(/"/g , "'");
        var final={};
        final['accountli']=data;
    function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
    var accountModel =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
    accountModel.customVerb('updateAccountPreference',final,completionCallBack);  
    };
  
  Profile_setAccountsPreference_CommandHandler.prototype.validate = function(){
    
    };
    
    return Profile_setAccountsPreference_CommandHandler;
    
});