define([], function() {

  	function Profile_modifyProfileAlerts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_modifyProfileAlerts_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_modifyProfileAlerts_CommandHandler.prototype.execute = function(command){
		  var self = this;

        function completionCallBack(status, response, error) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, response);
            } else {
                self.sendResponse(command, status, error);
            }
        }
        var alertsDataModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("UserAlerts");
        alertsDataModel.customVerb("updateAlerts", command.context, completionCallBack);
    };
	
	Profile_modifyProfileAlerts_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_modifyProfileAlerts_CommandHandler;
    
});