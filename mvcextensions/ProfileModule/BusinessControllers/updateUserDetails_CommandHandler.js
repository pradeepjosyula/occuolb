define([], function() {

  	function Profile_updateUserDetails_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_updateUserDetails_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_updateUserDetails_CommandHandler.prototype.execute = function(command){
	var self = this;
      var params = {"userName":command.context};
       function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");                
            var userObject=new userModel(params);
            userObject.partialUpdate(completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }	
    };
	
	Profile_updateUserDetails_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_updateUserDetails_CommandHandler;
    
});