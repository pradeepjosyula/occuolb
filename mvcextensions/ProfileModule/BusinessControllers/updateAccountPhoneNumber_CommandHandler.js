define([], function() {
    
        function Profile_updateAccountPhoneNumber_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
    inheritsFrom(Profile_updateAccountPhoneNumber_CommandHandler, kony.mvc.Business.CommandHandler);
    Profile_updateAccountPhoneNumber_CommandHandler.prototype.execute = function(command) {
        var self = this;
        var params = command.context;

        function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var accountModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
            accountModel.customVerb("updateAccountPhoneNumber", command.context, completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
    Profile_updateAccountPhoneNumber_CommandHandler.prototype.validate = function() {};
    return Profile_updateAccountPhoneNumber_CommandHandler;
});
