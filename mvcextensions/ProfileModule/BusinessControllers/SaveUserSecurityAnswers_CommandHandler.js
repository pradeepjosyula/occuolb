define([], function() {

    function Profile_SaveUserSecurityAnswers_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
    
    inheritsFrom(Profile_SaveUserSecurityAnswers_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Profile_SaveUserSecurityAnswers_CommandHandler.prototype.execute = function(command){
        var self = this;
        var record = {};
        record.securityQuestions = command.context;
		record.userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
        function completionCallback(status, response, err) {
             if (status === kony.mvc.constants.STATUS_SUCCESS) {
                 self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
             } else {
                 self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
             }
         }
         try {
              var securityQuestionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SecurityQuestions");                
          securityQuestionsModel.customVerb("createCustomerSecurityQuestions",record,completionCallback);
         } catch (err) {
             self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
         }      
    };
    
    Profile_SaveUserSecurityAnswers_CommandHandler.prototype.validate = function(){
        
    };
    
    return Profile_SaveUserSecurityAnswers_CommandHandler;
    
});