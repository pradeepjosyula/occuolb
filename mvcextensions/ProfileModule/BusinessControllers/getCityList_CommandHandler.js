define([], function () {

    function Profile_getCityList_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Profile_getCityList_CommandHandler, kony.mvc.Business.CommandHandler);

    Profile_getCityList_CommandHandler.prototype.execute = function (command) {
        var self = this;

        function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var StatesModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("States");
            StatesModel.getAllCities({}, completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };

    Profile_getCityList_CommandHandler.prototype.validate = function () {

    };

    return Profile_getCityList_CommandHandler;

});