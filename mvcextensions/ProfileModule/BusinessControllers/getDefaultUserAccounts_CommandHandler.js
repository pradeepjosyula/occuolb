define([], function() {

  	function Profile_getDefaultUserAccounts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_getDefaultUserAccounts_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_getDefaultUserAccounts_CommandHandler.prototype.execute = function(command){
  		var self=this;
  		function  getAllCompletionCallback(status,  data,  error){
  			self.sendResponse(command,status,status===kony.mvc.constants.STATUS_SUCCESS? data:error);
  		}
	  	try{
	        var  userProfile  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
	        userProfile.getAll(getAllCompletionCallback);
	      
	    }catch(err){
	        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
	    }
		
    };
	
	Profile_getDefaultUserAccounts_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_getDefaultUserAccounts_CommandHandler;
    
});