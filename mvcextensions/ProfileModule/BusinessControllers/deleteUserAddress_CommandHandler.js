define([], function() {
    
          function Profile_deleteUserAddress_CommandHandler(commandId) {
            kony.mvc.Business.CommandHandler.call(this, commandId);
        }
        
        inheritsFrom(Profile_deleteUserAddress_CommandHandler, kony.mvc.Business.CommandHandler);
      
          Profile_deleteUserAddress_CommandHandler.prototype.execute = function(command){
        var self = this;
           function completionCallBack(status, response, err) {
                if (status === kony.mvc.constants.STATUS_SUCCESS) {
                    self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
                } else {
                    self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                }
            }
            try {
                var userAddressModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");                
                userAddressModel.customVerb("deleteAddress", {addressId: command.context}, completionCallBack);
            } catch (err) {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }	
        };
        
        Profile_deleteUserAddress_CommandHandler.prototype.validate = function(){
            
        };
        
        return Profile_deleteUserAddress_CommandHandler;
        
    });