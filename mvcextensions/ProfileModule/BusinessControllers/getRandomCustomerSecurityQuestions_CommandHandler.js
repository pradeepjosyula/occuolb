define([], function() {

  	function Profile_getRandomCustomerSecurityQuestions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Profile_getRandomCustomerSecurityQuestions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Profile_getRandomCustomerSecurityQuestions_CommandHandler.prototype.execute = function(command){
		var self = this;
       function completionCallback(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
             var questionsRepo  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('SecurityQuestions');
            questionsRepo.customVerb('getRandomCustomerSecurityQuestions', {userName: command.context.userName}, completionCallback);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }	
    };
	
	Profile_getRandomCustomerSecurityQuestions_CommandHandler.prototype.validate = function(){
		
    };
    
    return Profile_getRandomCustomerSecurityQuestions_CommandHandler;
    
});