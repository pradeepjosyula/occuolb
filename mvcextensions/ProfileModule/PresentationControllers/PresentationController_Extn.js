define([], function () {

  return {
    fetchAccountAlerts : function() {
      kony.print("Inside call of fetchAccountAlerts");
      this.presentUserInterface("frmProfileManagement",{isLoading: true});
      var self = this;
      var params = {
        "userName": kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
      };
      function completionCallback(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          self.presentUserInterface('frmProfileManagement', {
            "AccountAlerts":response.data
          });
        } else {
          self.presentUserInterface('frmProfileManagement', {
            "AccountAlertsError":response.data
          });
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getAccountAlerts", params, completionCallback));  
    },
    
    modifyAccountAlert : function(alertData) {
      kony.print("Inside call of modifyAccountAlert");
      this.presentUserInterface("frmProfileManagement",{isLoading: true});
      this.presentUserInterface("frmProfileManagement",{isLoading: true});
      var self = this;
      var params = {
        "userName": kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
        "accountNumber":alertData.accountNumber,
        "checkNumber":alertData.checkNumber,
        "days":alertData.days,
        "expirationDate":alertData.expirationDate,
        "notifyAmount":alertData.notifyAmount,
        "productId":alertData.productId,
        "productType":alertData.productType,
        "subtype":alertData.subtype,
        "text":alertData.text,
        "email":alertData.email,
        "push":alertData.push,
        "type":alertData.type
      };
      function completionCallback(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          self.presentUserInterface('frmProfileManagement', {
            "ModifyAccountAlert":response.data
          });
        } else {
          self.presentUserInterface('frmProfileManagement', {
            "ModifyAccountAlertError":response.data
          });
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.modifyAccountAlert", params, completionCallback));  
    }
  };
});