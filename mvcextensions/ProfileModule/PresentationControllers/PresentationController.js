define(['ProfileModule/PresentationControllers/PresentaionController_UserProfile','CommonUtilities'], function(userProfile,CommonUtilities) {

  function Profile_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Profile_PresentationController, kony.mvc.Presentation.BasePresenter);

  Profile_PresentationController.prototype.initializeUserProfileClass = function () {
    this.userProfile = userProfile;
    this.userProfile.init(this.showSettingsScreen.bind(this), this.businessController)
  };
  /**
  * Method to fetch alerts and mapping to UI
  * @member of Profile_PresentationController
  * @param {JSON} alertType - type of the alert
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.getSpecifiedCitiesAndStates = function(addressSelection, addressId,states) {
        var self = this;
        var data=[];
        if (addressSelection === "country") {
            var statesList = [];
            statesList.push(["lbl1","Select a State"]);
            for (var i = 0; i < Object.keys(states).length; ++i) {
                if (states[i][2] === addressId) {
                    statesList.push([states[i][0], states[i][1]]);
                }
            }
            data={
                "states":statesList
            };
            
        } else if (addressSelection === "state") {
            var cityList = [];
            cityList.push(["lbl2","Select a City"]);
            for (var j = 0; j < Object.keys(states).length; ++j) {
                if (states[j][2] === addressId) {
                    cityList.push([states[j][0], states[j][1]]);
                }
            }
            data={
                "cities":cityList
            };
           
        }
        return data;
    };


  Profile_PresentationController.prototype.fetchAlerts = function (alertType) {
  this.presentUserInterface("frmProfileManagement",{isLoading: true});
    var self = this;
    var params={userName: kony.mvc.MDAApplication.getSharedInstance().appContext.username,
                alertTypeName: alertType
               };
    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.presentUserInterface('frmProfileManagement', {
          "Alerts":response.data
        });

      } else {
        self.presentUserInterface('frmProfileManagement', {
          "AlertsError":response.data
        });
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.fetchProfileAlerts", params, completionCallback));  
  };

/**
  * updateAlerts- Method to update alerts and mapping to UI
  * @member of Profile_PresentationController
  * @param {Collection} response -  alerts list and alert type
  * @returns {void} - None
  * @throws {void} -None
  */

  Profile_PresentationController.prototype.updateAlerts = function (responseData) {
    var self = this;
    var alertsData=JSON.stringify(responseData.alerts);
    alertsData = alertsData.replace(/"/g , "'");
    var params = {
        userName: kony.mvc.MDAApplication.getSharedInstance().appContext.username,
        alerts: alertsData,
        isSelected: responseData.isSelected
    };
    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        var data={ 
          "alertTypes":[{"alerts":responseData.alerts,
                        "isSelected": responseData.isSelected,
                         "alertType": responseData.alertType}]
        };
        self.presentUserInterface('frmProfileManagement', {
          "Alerts":data
        });

      } else {
          self.presentUserInterface('frmProfileManagement', {
          "AlertsError":response.data
        });
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.modifyProfileAlerts", params, completionCallback));  
  };



  /**
  * Method to fetch user profile info and mapping to UI
  * @member of Profile_PresentationController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.showProfileSettings = function() {
    this.initializeUserProfileClass();
    this.userProfile.showUserProfile();
  };

  /**
  * Method to show secure access code settings
  * @member of Profile_PresentationController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.showSecureAccessSettings = function() {
        this.initializeUserProfileClass();
        //this.userProfile.showUserProfile();
        var viewModel = {};
        viewModel.secureAccessSettings = true;
        this.presentUserInterface('frmProfileManagement', viewModel);
    };

  /**
  * Method to show setting screen
  * @member of Profile_PresentationController
  * @param {JSON} viewModel - Data to be mapped at setting's screen 
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.showSettingsScreen = function (viewModel) {
    this.presentUserInterface("frmProfileManagement", viewModel);
  }


  /**
  * Method to load hamburger and and initialize headers
  * @member of Profile_PresentationController
  * @param {string} frm - name of the form 
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.loadHamburger = function(frm){
    var self = this;
    var howToShowHamburgerMenu = function(sideMenuViewModel){
      self.presentUserInterface(frm,{sideMenu : sideMenuViewModel});
      // this.presentTransfers({sideMenu : sideMenuViewModel});        
    };
    self.SideMenu.init(howToShowHamburgerMenu); 

    var presentTopBar = function(topBarViewModel) {
      self.presentUserInterface(frm,{topBar : topBarViewModel});
    };
    self.TopBar.init(presentTopBar);
  }; 


  /**
  * Method to get default user accounts
  * @member of Profile_PresentationController
  * @param {String} errorSchenario - String which act as falg to handle the flow 
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.getDefaultUserProfile = function(errorSchenerio) {
    var self = this;
    if (errorSchenerio === "errorCase") {
      self.getDefaultAccountNames("error");
    } else {
      function completionCallback(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          var data = {
            defaultTransferAccount: response.data[0]['default_account_transfers'],
            defaultBillPayAccount: response.data[0]['default_account_billPay'],
            defaultP2PAccount: response.data[0]['default_from_account_p2p'],
            defaultCheckDepositAccount: response.data[0]['default_account_deposit']
          };
          self.getDefaultAccountNames(data);
        } else {
          //alert('error fetching data for default Account Number');
          self.getDefaultAccountNames("error");
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getDefaultUserAccounts", {}, completionCallback));
    }
  };


  /**
  * Method to get Default account name for the user
  * @member of Profile_PresentationController
  * @param {Integer} accountNumber - accountNumber of the account whose name is required
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.getDefaultAccountNames=function(accountNumbers){
    var defaultNames=[],self=this;
    this.defaultAccounts=accountNumbers;
    if(accountNumbers==="error"){
      defaultNames['errorCase'] = true;
      self.presentUserInterface('frmProfileManagement', {
        "showDefautUserAccounts": defaultNames
      });
    }else{

      function completionCallback(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          for (var keys in accountNumbers) {
            if (accountNumbers[keys] && accountNumbers[keys] !== "-1") {
              for (var i in response.data) {
                if (accountNumbers[keys] === response.data[i].accountID) {
                  defaultNames[keys] = response.data[i].accountName;
                  break;
                }
              }
            } else {
              defaultNames[keys] = "None";
            }
          }
          self.presentUserInterface('frmProfileManagement', {
            "showDefautUserAccounts": defaultNames
          });
        } else {
          defaultNames['errorCase'] = true;
          self.presentUserInterface('frmProfileManagement', {
            "showDefautUserAccounts": defaultNames
          });
          //alert('Error in Defalut Account Names');
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getDefaultUserAccountsName", accountNumbers, completionCallback));
    }
  };

  /**
  * Method to get List of account
  * @member of Profile_PresentationController
  * @param {void} - None 
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.getAccountsList=function(){
    var self=this;
    var defaultAccounts=this.defaultAccounts;
    var getAccountDisplayName=function(fromAccount){
      return fromAccount.accountName+'-XXXX'+fromAccount.accountID.slice(-4);
    };
    var generateFromAccounts = function(fromAccount, index) {
      return [fromAccount.accountID, getAccountDisplayName(fromAccount)];
    };
    var getDefaultSelectedKey=function(data){
      if(data&&data!=="-1"){
        return data;
      }else{
        return 'undefined';
      }
    };
    var getAccountsListViewModel={};
    var generateViewModel=function(data){
      getAccountsListViewModel['TransfersAccounts']=data.filter(function(acc){
        return acc.supportTransferFrom==='1';
      }).map(generateFromAccounts);
      getAccountsListViewModel['defaultTransfersAccounts']=getDefaultSelectedKey(defaultAccounts.defaultTransferAccount);
      getAccountsListViewModel['BillPayAccounts']=data.filter(function(acc){
        return acc.supportBillPay==='1';
      }).map(generateFromAccounts);
      getAccountsListViewModel['defaultBillPayAccounts']=getDefaultSelectedKey(defaultAccounts.defaultBillPayAccount);
      getAccountsListViewModel['P2PAccounts']=data.filter(function(acc){
        return acc.supportTransferFrom==='1';
      }).map(generateFromAccounts);
      getAccountsListViewModel['defaultP2PAccounts']=getDefaultSelectedKey(defaultAccounts.defaultP2PAccount);
      getAccountsListViewModel['CheckDepositAccounts']=data.filter(function(acc){
        return acc.supportDeposit==='1';
      }).map(generateFromAccounts);
      getAccountsListViewModel['defaultCheckDepositAccounts']=getDefaultSelectedKey(defaultAccounts.defaultCheckDepositAccount);
    };
    function completionCallback(response){
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
        generateViewModel(response.data);
        self.presentUserInterface('frmProfileManagement',{
          "getAccountsList":getAccountsListViewModel
        });
      }else{
        //alert('Error fetching User accounts');
        self.presentUserInterface('frmProfileManagement', {
          "getAccountsList": "errorCase"
        });
        //alert('Error fetching User accounts');
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getDefaultUserAccountsName", {}, completionCallback));
  };

  /**
  * Method to save aletered accounts
  * @member of Profile_PresentationController
  * @param {JSON} defaultAccounts - altered setting of the account
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.saveDefaultAccounts=function(defaultAccounts){
    var self=this;
    function completionCallback(response){
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
         kony.mvc.MDAApplication.getSharedInstance().appContext.billPayDefaultAccount=defaultAccounts.default_account_billPay;
        self.getDefaultUserProfile();
      }else{
        self.getDefaultUserProfile("errorCase");
        //alert("error in saving the data for default accounts");
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.setDefaultUserAccounts", defaultAccounts, completionCallback));
  };

  /**
  * Method to fetch accounts and show prefered accounts
  * @member of Profile_PresentationController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.showPreferredAccounts = function() {
    var self = this;
    this.initializeUserProfileClass();
    this.presentUserInterface("frmProfileManagement", {
      isLoading: true
    });
    var accountsList = {};
    var accountTypeConfig = {
      'Savings': {
        sideImage: 'accounts_sidebar_turquoise.png',
        skin: 'sknflxhex26d0cecode',
        image: 'account_change_turquoise.png'
      },
      'Checking': {
        sideImage: 'accounts_sidebar_purple.png',
        skin: 'sknflxhex9060B7code',
        image: 'account_change_purple.png',
      },
      'CreditCard': {
        sideImage: 'accounts_sidebar_yellow.png',
        skin: 'sknflxhexf4ba22code',
        image: 'account_change_yellow.png',
      },
      'Deposit': {
        sideImage: 'accounts_sidebar_blue.png',
        skin: 'sknflxhex4a90e2code',
        image: 'account_change_turquoise.png'
      },
      'Mortgage': {
        sideImage: 'accounts_sidebar_brown.png',
        skin: 'sknflxhex8D6429code',
        image: 'account_change_yellow.png',
      },
      'Loan': {
        sideImage: 'accounts_sidebar_brown.png',
        skin: 'sknflxhex8D6429code',
        image: 'account_change_yellow.png',
      },
      'Default': {
        sideImage: 'accounts_sidebar_turquoise.png',
        skin: 'sknflxhex26d0cecode',
        image: 'account_change_turquoise.png'

      }
    };
    var getConfigFor = function(accountType) {
      if (accountTypeConfig[accountType]) {
        return accountTypeConfig[accountType];
      } else {
        kony.print(accountType + ' is not configured for display. Using Default configuration.');
        return accountTypeConfig.Default;
      }
    };
    var generateViewModelAccounts = function(accountsData) {
      if (accountsData.isExternalAccount) {
        return {
          "accountIdPK":accountsData.Account_id,
          "flxIdentifier": getConfigFor(accountsData.TypeDescription).skin,
          "AccountName": accountsData.AccountName,
          "AccountHolder": accountsData.AccountHolder,
          "AccountNumber": accountsData.Number,
          "AccountType": accountsData.TypeDescription,
          "imgFavoriteCheckBox": (String(accountsData.FavouriteStatus).trim().toLowerCase() === "true") ? "checked_box.png" : "unchecked_box.png",
          "imgMenu": getConfigFor(accountsData.TypeDescription).image,
          "imgEStatementCheckBox": "unchecked_box.png",
          "fullName": "",
          "nickName": accountsData.NickName,
          "accountPreference": Number.MAX_SAFE_INTEGER,
          "email": "",
          "external": true,
        }
      } else {
        return {
          "flxIdentifier": getConfigFor(accountsData.accountType).skin,
          "AccountName": accountsData.accountName,
          "AccountHolder": accountsData.accountHolder,
          "AccountNumber": accountsData.accountID,
          "AccountType": accountsData.accountType,
          "imgFavoriteCheckBox": accountsData.favouriteStatus == "1" ? "checked_box.png" : "unchecked_box.png",
          "imgEStatementCheckBox": (accountsData.eStatementEnable == "true" || accountsData.eStatementEnable == "1") ? "checked_box.png" : "unchecked_box.png",
          "imgMenu": getConfigFor(accountsData.accountType).image,
          "fullName": accountsData.firstName + " " + accountsData.lastName,
          "nickName": accountsData.nickName,
          "accountPreference": Number(accountsData.accountPreference),
          "email": accountsData.email,
          "external": false
        }
      }
    };
    if (CommonUtilities.getConfiguration("isAggregatedAccountsEnabled")) {
      var accountsViewModel;
      var serviceCounts = 0;
      var allAccounts = {
        internal: null,
        external: null
      };
      var finalAccounts = [];

      function completionCallback(response) {
        if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
          serviceCounts++;
          allAccounts.internal = response.data;
          if (serviceCounts === 2) {
            finalAccounts = allAccounts.internal;
            allAccounts.external.forEach(function(externalAccount) {
              finalAccounts.push(externalAccount);
            });
            accountsViewModel = finalAccounts.map(generateViewModelAccounts);
            var sortedaccountsViewModel = accountsViewModel.sort(function(a, b) {
              return a.accountPreference - b.accountPreference

            });
            self.presentUserInterface('frmProfileManagement', {
              "getPreferredAccountsList": sortedaccountsViewModel
            });
          }

        } else {
          self.presentUserInterface('frmProfileManagement', {
            "getPreferredAccountsList": {
              "errorCase": true
            }
          });
          //alert("Error in Getting the Accounts list");
        }
      }

      function completionCallbackExternal(response) {
        if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
          serviceCounts++;
          response.data.forEach(function(externalAccount) {
            externalAccount.isExternalAccount = true;
          });
          allAccounts.external = response.data;
          if (serviceCounts === 2) {
            finalAccounts = allAccounts.internal;
            allAccounts.external.forEach(function(externalAccount) {
              finalAccounts.push(externalAccount);
            });
            accountsViewModel = finalAccounts.map(generateViewModelAccounts);
            var sortedaccountsViewModel = accountsViewModel.sort(function(a, b) {
              return a.accountPreference - b.accountPreference

            });
            self.presentUserInterface('frmProfileManagement', {
              "getPreferredAccountsList": sortedaccountsViewModel
            });
          }
        } else {
          self.presentUserInterface('frmProfileManagement', {
            "getPreferredAccountsList": {
              "errorCase": true
            }
          });
          //alert("Error in Getting the Accounts list");
        }

      }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getDefaultUserAccountsName", {}, completionCallback));
      var accountModule = new kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getExternalAccountsFromDB", {
        mainUser: kony.mvc.MDAApplication.getSharedInstance().appContext.username
      }, completionCallbackExternal));
    } else {
      function completionCallback(response) {
        var accountsViewModel;
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          accountsViewModel = response.data.map(generateViewModelAccounts);
          var sortedaccountsViewModel = accountsViewModel.sort(function(a, b) {
            return a.accountPreference - b.accountPreference
          });
          self.presentUserInterface('frmProfileManagement', {
            "getPreferredAccountsList": sortedaccountsViewModel
          });
        } else {
          self.presentUserInterface('frmProfileManagement', {
            "getPreferredAccountsList": {
              "errorCase": true
            }
          });
          //alert("Error in Getting the Accounts list");
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getDefaultUserAccountsName", {}, completionCallback));
    }

  };
  /**
  * Method to change the preference of the account
  * @member of Profile_PresentationController
  * @param {JSON} updatedAccounts - Accounts with changed preference
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.setAccountsPreference=function(updatedAccounts){ 
    var self=this;
    function completionCallback(response){
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
        self.showPreferredAccounts();
      }else{
        self.showPreferredAccounts();
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.setAccountsPreference", updatedAccounts, completionCallback));
  };

  /**
  * Method to save preferred account data
  * @member of Profile_PresentationController
  * @param {JSON} data - accounts data whose preference is changed
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.savePreferredAccountsData=function(data){
     var self=this;
    function completionCallback(response){
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
        self.showPreferredAccounts();
      }else{
        self.presentUserInterface('frmProfileManagement',{errorEditPrefferedAccounts:true});
        //alert("Error in saving preffered accounts");
      }
    }
    if(data.external){
      var newdata = {
        "NickName": data.NickName,
        "FavouriteStatus": data.FavouriteStatus,
        "Account_id": data.Account_id,
      }
      var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      accountsModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.updateExternalAccountFavouriteStatus", newdata, completionCallback));
      
    } 
    else{
     
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.setDefaultUserAccountsPreference", data, completionCallback));
 
    }
  };

  /**
  * Method to check exisitng password
  * @member of Profile_PresentationController
  * @param {String} viewModel - Password entered by user
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.checkExistingPassword = function (viewModel) {
    var self = this;
    function completionCallback(response){
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        if (response.data.result === "The user is verified"){
          self.presentUserInterface("frmProfileManagement", {showVerificationByChoice : viewModel});
        }
        else{
          self.presentUserInterface("frmProfileManagement",{passwordExists : "password exists"});
        }
      }
      else
        self.presentUserInterface("frmProfileManagement",{passwordExistsServerError : "password exists"});
    }
    var password = viewModel;
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.checkExistingPassword", password, completionCallback));

  };

  /**
  * Method to request OTP
  * @member of Profile_PresentationController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.requestOtp = function () {
    var scopeObj = this;    
    function completionRequestOTPCallback(response){
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        scopeObj.presentUserInterface("frmProfileManagement", {requestOtp : response.data});
      }
      else{
        scopeObj.presentUserInterface("frmProfileManagement", {requestOtpError : response.data});
      }

    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.requestOTP",{},completionRequestOTPCallback));
  };

  /**
  * Method to verify otp
  * @member of Profile_PresentationController
  * @param {String} viewModel - OTP 
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.verifyOtp = function (viewModel) {
    var scopeObj = this;    
    var otpJSON = {
      "otp": viewModel
    };
    function completionOTPValidateCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS){
        scopeObj.presentUserInterface("frmProfileManagement", {verifyOtp : response.status});
      }
      else{
        scopeObj.presentUserInterface("frmProfileManagement", {verifyOtpError : response.status});
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.validateOTP", otpJSON, completionOTPValidateCallback));
  };

  /**
  * Method to update the password
  * @member of Profile_PresentationController
  * @param {JSON} viewModel - Data required to update password
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.updatePassword = function (viewModel) {
    var scopeObj = this;

    function completionCallback(response){
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        if(response.data.success)
          scopeObj.presentUserInterface("frmProfileManagement", {
            update: "password"
          });
        else
          scopeObj.presentUserInterface("frmProfileManagement", {
            showPasswordServerError: "password"
          });
      } else {
        scopeObj.presentUserInterface("frmProfileManagement", {
          showPasswordServerError: "password"
        });
      }
    }
    var password = viewModel;
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.resetPassword", password, completionCallback));
  };

  /**
  * Method to Check existing secure access option
  * @member of Profile_PresentationController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.checkSecureAccess = function () {
    var scopeObj = this;

    function completionCallback(response){
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        scopeObj.presentUserInterface("frmProfileManagement", {securityAccess : response});
      }
      else{
        scopeObj.presentUserInterface("frmProfileManagement", {checkSecurityAccessError : response});
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getSecurityAccess", {}, completionCallback));
  };

  /**
  * Method to update secure access option
  * @member of Profile_PresentationController
  * @param {JSON} viewModel - Secure access data which needs to be updated
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.updateSecureAccessOptions = function (viewModel) {
    var scopeObj = this;

    function completionCallback(response){
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        scopeObj.presentUserInterface("frmProfileManagement", {secureAccessOption : response});
      }
      else{
        scopeObj.presentUserInterface("frmProfileManagement", {secureAccessOptionError : response});
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.updateSecurityAccess", viewModel, completionCallback));
  };


  /**
  * Method to fetch all the security questions
  * @member of Profile_PresentationController
  * @param {JSON} context - JSON to handle the security questions
  * @param {function} staticSetQuestions - function binded to handle the response
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.fetchSecurityQuestions = function(context,staticSetQuestions) {
    var self= this; 

    function getSecurityQuestionsCompletionCallback(response) {
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        var i=0;
        while (i < response.data.records.length) {
                    context.securityQuestions[i] = response.data.records[i].SecurityQuestion;
                    context.flagToManipulate[i] = "false";
                    i++;
                }
                staticSetQuestions(context, response.data.records);
            } else staticSetQuestions(context, response.data.records);

    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.enroll.getSecurityQuestions", {}, getSecurityQuestionsCompletionCallback));

  };

  /**
  * Method to change the answers of the security questions
  * @member of Profile_PresentationController
  * @param {JSON} data - questions and their answers
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.updateSecurityQuestions = function (data) {
    var scopeObj = this;
    var securityQuestionsCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        scopeObj.presentUserInterface("frmProfileManagement", {update : "security questions"});
      }else {
        scopeObj.presentUserInterface("frmProfileManagement", {updateSecurityQuestionError : "security questions"});
      }
    };

    data = JSON.stringify(data);
    data = data.replace(/"/g , "'");

    scopeObj.businessController.execute(
      new kony.mvc.Business.Command("com.kony.Profile.saveUserSecurityAnswers",data , securityQuestionsCallback)
    );
  };

  /**
  * Method to check existance of security questions
  * @member of Profile_PresentationController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.checkSecurityQuestions = function () {
    var scopeObj = this;

    function completionCallback(response){
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        if(response.data.result)
          scopeObj.presentUserInterface("frmProfileManagement", {SecurityQuestionExists : response});
        else
          scopeObj.presentUserInterface("frmProfileManagement", {SecurityQuestionExistsError : response});
      }
      else{
        scopeObj.presentUserInterface("frmProfileManagement", {SecurityQuestionExistsError : response});
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.checkSecurityQuestions", {}, completionCallback));
  };

  /**
  * Method to show setting screen
  * @member of Profile_PresentationController
  * @param {JSON} viewModel - Data to be mapped at setting's screen 
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.updateUsername = function (viewModel) {
    var scopeObj = this;

    function completionCallback(response){
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        if(response.data.userName)
          scopeObj.presentUserInterface("frmProfileManagement", {update : "username"});
        else
          scopeObj.presentUserInterface("frmProfileManagement", {updateUsernameError : "username already exists"});
      }
      else{
        scopeObj.presentUserInterface("frmProfileManagement", {updateUsernameError : "username"});
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.updateUserDetails", viewModel, completionCallback));
  };

  /**
  * Method to get Security questions to be answeres
  * @member of Profile_PresentationController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.getAnswerSecurityQuestions = function () {
    var scopeObj = this;
    var param={
      "userName": kony.mvc.MDAApplication.getSharedInstance().appContext.username,
    };
    function completionCallback(response){
      if(response.status === kony.mvc.constants.STATUS_SUCCESS){
        scopeObj.presentUserInterface("frmProfileManagement", {answerSecurityQuestion : response.data.records});
      }
      else{
        scopeObj.presentUserInterface("frmProfileManagement", {getAnswerSecurityQuestionError : response});
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getRandomCustomerSecurityQuestions", param, completionCallback));
  };

  /**
  * Method to verify Security Question and answers
  * @member of Profile_PresentationController
  * @param {JSON} data - JSON consisting of question and answers
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.verifyQuestionsAnswer = function (data) {
    var scopeObj = this;
    var completionCallback = function(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                if(response.data.verifyStatus === "true")
                {
                    scopeObj.presentUserInterface("frmProfileManagement", {
                    verifyQuestion: "security questions"
                });
                }else {
                scopeObj.presentUserInterface("frmProfileManagement", {
                    verifyQuestionAnswerError: "security questions"
                });
            }               
            } else {
                scopeObj.presentUserInterface("frmProfileManagement", {
                    verifyQuestionAnswerError: "security questions"
                });
            }
        };

    data = JSON.stringify(data);
    data = data.replace(/"/g ,  "'");
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.verifyAnswerSecurityQuestions", data, completionCallback));
  };
  /**
  * Method to edit external account
  * @member of Profile_PresentationController
  * @param {JSON} data - JSON consisting account data
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.showEditExternalAccount = function (data) {
    this.presentUserInterface("frmProfileManagement", {
      isLoading: true
    });
    this.presentUserInterface('frmProfileManagement', {
      "editExternalAccounts": data
    });
  };
   Profile_PresentationController.prototype.getPasswordRules=function(){
     var scopeObj=this;
    function completionCallBack(response){
       if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
         scopeObj.presentUserInterface("frmProfileManagement", {usernamepasswordRules : response.data.records});
       }
      else{
      CommonUtilities.showServerDownScreen();
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.Profile.getPasswordPolicies", {}, completionCallBack));

  };
  
  /**
  * Method to save external account data
  * @member of Profile_PresentationController
  * @param {JSON} data - accounts data
  * @returns {void} - None
  * @throws {void} -None
  */
  Profile_PresentationController.prototype.saveExternalAccountsData=function(data){
    var self=this;
    function completionCallback(response){
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
        self.presentUserInterface('frmProfileManagement',{errorSaveExternalAccounts:{error:false}});
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountsModule.presentationController.showAccountsDashboard();
      }else{
    self.presentUserInterface('frmProfileManagement',{errorSaveExternalAccounts:true});
      }
    }

    var newdata = {
      "NickName": data.NickName,
      "FavouriteStatus": data.FavouriteStatus,
      "Account_id": data.Account_id,
    }
    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    accountsModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.updateExternalAccountFavouriteStatus", newdata, completionCallback));



  };
  return Profile_PresentationController;
});