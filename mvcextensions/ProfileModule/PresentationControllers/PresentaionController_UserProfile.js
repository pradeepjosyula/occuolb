/**
 * This module Helps Profile Presentation Controller with 
 * user profile settings - Email, Phone Number, Addresses 
 * No state management. Functions get input and produce viewmodel
 * @author Shivam Marwaha
 */

define(['CommonUtilities'], function (commonUtils) {
    var arrayToListBoxValues = function (array) {
        return array.map(function (item, index) {
            return [index, item];
        });
    }

    var presentor = null;
    var commandExecuter = null;

    var PhoneTypes = {
        'Mobile': 'Mobile',
        'Work': 'Work',
        'Home': 'Home',
		'Other': 'Other'
    }

    var AddressTypes = {
        'home': 'Home',
        'office': 'Work'
    }

    //  Can be called in Async/Sync Mode
    var getUserProfileObject = function (successCallback, forceRefresh) {
        if (successCallback === undefined) {
            return kony.mvc.MDAApplication.getSharedInstance().appContext.userProfileObject;
        }
        else {
            if (forceRefresh) {
                function completionCallback(response) {
                      kony.mvc.MDAApplication.getSharedInstance().appContext.userProfileObject = response.data;
                        successCallback(response.data);
                }
        
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getUserProfile", {}, completionCallback));
            }
            else {
                successCallback(kony.mvc.MDAApplication.getSharedInstance().appContext.userProfileObject)
            }
        }
         
    }

    var AddPhoneViewModel = function (accounts) {
        var services = accounts.map(function (account) {
            return {
                id: account.accountID,
                name: commonUtils.getAccountDisplayName(account),
            }
        });
        return {
            phoneTypes: objectToListBoxArray(PhoneTypes),
            phoneTypeSelected: "Mobile",
            countryType: 'domestic',
            phoneNumber: '',
            ext: '',
            isPrimary: false,
            services: services,
            recievePromotions: false
        }
        
    }

    var AddAddressViewModel = function (countries) {
        return {
            serverError: null,
            addressTypes: objectToListBoxArray(AddressTypes),
            addressTypeSelected: "home",
            addressLine1: '',
            addressLine2: '',
            countries: countries.map(function(country) {return [country.Name, country.Name]}),
            countrySelected: countries[0].Name,
            city: '',
            isPreferredAddress: false,
            zipcode: '',
        }
        
    }

    var assignExistingAddress = function (newViewModel, address) {
        newViewModel.addressLine1 = address.addressLine1,
        newViewModel.addressLine2 =  address.addressLine2,
        newViewModel.countrySelected =  address.countrySelected,
        newViewModel.citySelected =  address.citySelected,
        newViewModel.zipcode =  address.zipcode,
        newViewModel.isPreferredAddress =  address.isPreferredAddress ? "1" : "0",
        newViewModel =  address.addressType
    }

    var objectToListBoxArray = function (obj) {
        var list = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                // list.push([key, kony.i18n.getLocalizedString(obj[key])]);
                list.push([key, obj[key]]);
                
            }
        }
        return list;
      }

    var fetchUserProfile = function (onSuccess, onError) {
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(response.data);
            } else {
                onError(response.data);
            }
        }

        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getUserProfile", {}, completionCallback));
    }
    

    var showUserProfile = function (data) {
        var userProfileViewModel = {
            name: data.userfirstname + " " + data.userlastname,
            dob: commonUtils.getFrontendDateString(new Date(data.dateOfBirth), commonUtils.getConfiguration('frontendDateFormat')),
            maskedSSN: '***-**-' + commonUtils.getLastFourDigit(data.ssn),
            userImage: data.userImage
        }
        presentor({
            userProfile: userProfileViewModel,
            isLoading: false
        })
    }

    var userObjectToEmailList = function (userObject) {
        var userEmails = [];
        userEmails.push({
         id:"email",
         email: userObject.email,
         isPrimary: true
        }, 
        {
         id:"secondaryemail",
         email: userObject.secondaryemail,
         isPrimary: false
        },
        {
         id:"secondaryemail2",
         email: userObject.secondaryemail2,
         isPrimary: false  
       })
       userEmails = userEmails.filter(function (emailRecord) {
        return emailRecord.email !== undefined && emailRecord.email !== null && emailRecord.email !== ""
       })
       return userEmails;
    }

    var fetchUserEmails = function (refresh, successCallback, errorCallback) {
        getUserProfileObject(function (userObject) {
            var userEmails = userObjectToEmailList(userObject);
           successCallback(userEmails);
       }, refresh);
       
    }
     var getEmails = function(emailList){
       presentor ({
            emails: emailList,
            isLoading: false
        })
    }
    var showUserEmails = function (emailList) {
        presentor ({
            emailList: emailList,
            isLoading: false
        })
    };
   
    var fetchUserAddresses = function (successCallback) {
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                successCallback(response.data);
            }
           
        }
      commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.getUserAddresses", {}, completionCallback));
    }

    var showUserAddress = function (addresses) {
        // Possible Mapping Required
        presentor ({
            addressList: addresses,
            isLoading: false
        }) 
    } 

    var fetchUserPhones = function (successCallback){
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                successCallback(response.data);
            }
           
        }
      commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.getUserPhones", {}, completionCallback));
    }

    var showUserPhones = function (userPhones) {
        presentor ({
            phoneList: userPhones,
            isLoading: false
        })
    }
    var fetchAccounts = function (onSuccess, onError) {
        var completionCallback = function (commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(commandResponse.data);
            } else {
                onError(commandResponse.data);
            }
        };
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.businessController.execute( new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, completionCallback));
    };

    var showAddPhoneNumberForm = function (accounts) {
        var phoneViewModel = AddPhoneViewModel(accounts);
        presentor({
            isLoading: false,
            addPhoneViewModel: phoneViewModel
        })
    }

    var showEditPhoneNumberForm = function (phoneObj, accounts) {
        var newPhoneModel = AddPhoneViewModel(accounts);
        newPhoneModel.countryType = phoneObj.countryType;
        newPhoneModel.phoneTypeSelected = phoneObj.type;
        newPhoneModel.ext = phoneObj.extension;
        newPhoneModel.phoneNumber = phoneObj.phoneNumber;
        newPhoneModel.recievePromotions = phoneObj.receivePromotions === "1";
        newPhoneModel.isPrimary = phoneObj.isPrimary === "1";
        newPhoneModel.services = accounts.map(function (account) {
            return {
                id: account.accountID,
                name: commonUtils.getAccountDisplayName(account),
                selected: account.phoneId === phoneObj.phoneNumber
            }
        })
        newPhoneModel.id = phoneObj.id;
        newPhoneModel.onBack = showPhoneDetails.bind(undefined, phoneObj,accounts);
        presentor({
            isLoading: false,
            editPhoneViewModel: newPhoneModel
        })
    }

    var onSavePhoneError = function (errorMessage) {
        presentor({
            isLoading: false,
            addPhoneViewModel: {
                serverError: errorMessage
            }
        })
    }

    var savePhoneNumber = function (viewModel, successCallback, errorCallback) {
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                attachPhoneNumberToAccounts(response.data.id, viewModel.services, successCallback);
            }
            else {
                errorCallback(response.data.errmsg);
            }
        }

      var params = {
        countryType: viewModel.countryType,
        extension: viewModel.extension,
        isPrimary: viewModel.isPrimary ? 1 : 0,
        phoneNumber: viewModel.phoneNumber,
        receivePromotions: viewModel.receivePromotions? 1 : 0,
        type: viewModel.type
      }  
      commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.addPhone", params, completionCallback));
        
    };

    var attachPhoneNumberToAccounts = function (phoneId, accountIds, successCallback) {
        function completionCallback () {
            successCallback();
        }

        if(accountIds.length === 0) {
            successCallback();
        }
        else {
            var commands = accountIds.map(function(id) {
                return new kony.mvc.Business.Command("com.kony.Profile.attachPhoneToAccount", {accountId: id, phoneId: phoneId})
            })
            kony.mvc.util.ParallelCommandExecuter.executeCommands(commandExecuter, commands, completionCallback)            
        }
    }

    var updateAccountsForPhoneNumber = function (phoneId, accountIds, successCallback) {
        function completionCallback () {
            successCallback();
        }

        if(accountIds.length === 0) {
            successCallback();
        }
        else {
            var commands = accountIds.map(function(selectionObj) {
                return new kony.mvc.Business.Command("com.kony.Profile.attachPhoneToAccount", {accountId: selectionObj.id, phoneId: selectionObj.selected ? phoneId:"null"})
            })
            kony.mvc.util.ParallelCommandExecuter.executeCommands(commandExecuter, commands, completionCallback)            
        }
    }


    var saveAddress = function (viewmodel, successCallback) {
        var context = {
            addressLine1: viewmodel.addressLine1,
            addressLine2: viewmodel.addressLine2,
            country: viewmodel.countrySelected,
            city: viewmodel.city,
            zipcode: viewmodel.zipcode,
            isPreferredAddress: viewmodel.isPreferredAddress ? 1 : 0,
            addressType: viewmodel.addressTypeSelected
        }
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                successCallback();
            }
            else {
                var newViewModel = AddAddressViewModel();
                assignExistingAddress(newViewModel, viewmodel);
                newViewModel.serverError = response.data.errmsg;
                presentor({
                    addNewAddress: newViewModel
                })
               
            }
        }
      commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.saveUserAddress", context, completionCallback));
    }

    var showAddAddressViewModel = function (countries) {
        var viewModel = AddAddressViewModel(countries);
        presentor({isLoading: false,addNewAddress: viewModel}) ;
    }

    var updateEmail = function (viewmodel, successCallback) {
        function generateCommandContext (emailObject, userObject) {
            var partialUserObject = {};
            if (emailObject.id === "email") {
                partialUserObject.email = emailObject.email
                return partialUserObject;
            }
            // partialUserObject.userId = userObject.userID;
            if (emailObject.isPrimary) {
                partialUserObject.email = emailObject.email
                partialUserObject[emailObject.id] = userObject.email
            }
            else {
                partialUserObject[emailObject.id] = emailObject.email
            }
            return partialUserObject;
        }

        function updateCompleteCompletionCallback(response) {
            if(response.status===kony.mvc.constants.STATUS_SUCCESS){ 
                successCallback()
            }
        }

        commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.setDefaultUserAccounts", generateCommandContext(viewmodel, getUserProfileObject()), updateCompleteCompletionCallback))
    }

    var deleteEmail = function (viewmodel, successCallback) {
        function generateCommandContext (emailObject) {
            var partialUserObject = {};
            partialUserObject[emailObject.id] = "";
            return partialUserObject;
        }

        function updateCompleteCompletionCallback(response) {
            if(response.status===kony.mvc.constants.STATUS_SUCCESS){ 
                successCallback()
            }
        }

        commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.setDefaultUserAccounts", generateCommandContext(viewmodel, getUserProfileObject()), updateCompleteCompletionCallback))
    }

    var deleteAddress = function (viewmodel, successCallback) {
        function deleteEmailCompletionCallback(response) {
            if(response.status===kony.mvc.constants.STATUS_SUCCESS){ 
                successCallback()
            }
        }

        commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.deleteUserAddress", viewmodel.addressId, deleteEmailCompletionCallback))
    }

    var deletePhone = function (phoneObj, successCallback) {
        function deletePhoneCompletionCallback(response) {
            if(response.status===kony.mvc.constants.STATUS_SUCCESS){ 
                successCallback()
            }
        }

        commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.deletePhone", phoneObj.id, deletePhoneCompletionCallback))
    }


    function isEmpty (value) {
        return value === "" || value === undefined || value === null;
    }

    function checkEmptySlot (keys, obj) {
        for (var i = 0 ; i < keys.length; i++) {
            if (isEmpty(obj[keys[i]])) {
                return keys[i];
            }
        }
    }

    var saveEmail = function (viewmodel, successCallback, errorCallback) {
        function sameEmail (userObject, emailAddress) {
            var emailFields = ["email", "secondaryemail", "secondaryemail2"];
            for (var i = 0; i < emailFields.length ; i++) {
                var existingEmail = userObject[emailFields[i]] || "";
                if (existingEmail.toUpperCase() === emailAddress.toUpperCase()) {
                    return true;
                }
            }
            return false;
        }
        function generateCommandContext (emailObject) {
            var partialUserObject = {};
            var userObject = getUserProfileObject();
            var slot = checkEmptySlot(["email", "secondaryemail", "secondaryemail2"], userObject);            
            if (slot) {
                if (emailObject.isPrimary) {
                    partialUserObject.email = emailObject.emailAddress;
                    partialUserObject[slot] = userObject.email
                }
                else {
                    partialUserObject[slot] = emailObject.emailAddress;                    
                }
                return partialUserObject;            
            }
        }
        function updateCompleteCompletionCallback(response) {
            if(response.status===kony.mvc.constants.STATUS_SUCCESS){ 
                successCallback()
            }
            else {
                errorCallback(response.data.errmsg)
            }
        }
        if (sameEmail(getUserProfileObject(), viewmodel.emailAddress)) {
            errorCallback(kony.i18n.getLocalizedString("i18n.profile.emailAlreadyExists"));
        }
        else {
            var context = generateCommandContext(viewmodel, getUserProfileObject());
            if (context) {
                commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.setDefaultUserAccounts", context, updateCompleteCompletionCallback))        
            }
            else {
                errorCallback("We currently do not support adding more than three emails for a user");
            }
        }
    }

    var fetchCountryList = function (successCallback) {
        function completionCallback(response) {
            if(response.status===kony.mvc.constants.STATUS_SUCCESS){ 
                successCallback(response.data);
            }
            else {
                // var newViewModel = Add
            }
        }
      commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.getCountryList", {}, completionCallback));
    };


    var updateAddress = function (viewmodel, successCallback) {
        var context = {
            addressLine1: viewmodel.addressLine1,
            addressId: viewmodel.addressId,
            addressLine2: viewmodel.addressLine2,
            country: viewmodel.countrySelected,
            city: viewmodel.city,
            zipcode: viewmodel.zipcode,
            isPreferredAddress: viewmodel.isPreferredAddress ? 1 : 0,
            addressType: viewmodel.addressTypeSelected
        }
        function completionCallback(response) {
            if(response.status===kony.mvc.constants.STATUS_SUCCESS){ 
                successCallback()
            }
            else {
                // var newViewModel = Add
            }
        }
      commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.updateUserAddress", context, completionCallback));
    }

    var showEditAddressForm = function (address, countries) {
        var viewModel = AddAddressViewModel(countries);
        viewModel.addressId = address.addressId;
        viewModel.addressLine1 = address.addressLine1;
        viewModel.addressLine2 = address.addressLine2;
        viewModel.addressTypeSelected = address.addressType;
        viewModel.city = address.city;
        viewModel.isPreferredAddress = address.isPreferredAddress === "1";
        viewModel.zipcode = address.zipcode;
        viewModel.countrySelected = address.country;
        viewModel.hidePrefferedAddress = address.isPreferredAddress === "1";
        presentor({
            isLoading: false,
            editAddress: viewModel
        })
    }

    var partialUpdatePhoneNumber = function (phoneObject,partialUpdateObject, successCallback) {
        function updateCompleteCompletionCallback(response) {
            if(response.status===kony.mvc.constants.STATUS_SUCCESS){ 
                successCallback()
            }
            else {
            }
        }
            commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.partialUpdatePhoneModel", {partialUpdateObject: partialUpdateObject, phoneId: phoneObject.id}, updateCompleteCompletionCallback))
    }


    var showPhoneDetails = function (phoneObject, accounts) {
        var services = accounts.map(function (account) {
                                    return {
                                        id: account.accountID,
                                        name: commonUtils.getAccountDisplayName(account),
                                        selected: account.phoneId === phoneObject.phoneNumber
                                    }
                                })
        presentor({
            phoneDetails: {
                phone: phoneObject,
                services: services 
            },
            isLoading: false
        })
    }

    var attachPhoneNumberToAccount = function (phoneId, accountId) {
        function completionCallback () {

        }

        commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.attachPhoneToAccount", {accountId: accountId, phoneId: phoneId}, completionCallback))
    }

    var showEditPhoneNumberError = function (errorMessage) {
        presentor({
            isLoading: false,
            editPhoneViewModel: {
                serverError: errorMessage
            }
        })
    }


    var editPhone = function (id, viewModel, successCallback, onError) {
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                updateAccountsForPhoneNumber(id, viewModel.services, successCallback);
            }
            else {
                onError(response.data.errmsg)
            }
        }
      var params = {
        id: id,
        countryType: viewModel.countryType,
        extension: viewModel.extension,
        isPrimary: viewModel.isPrimary ? 1 : 0,
        phoneNumber: viewModel.phoneNumber,
        receivePromotions: viewModel.receivePromotions? 1 : 0,
        type: viewModel.type
      }  
      commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.updatePhone", params, completionCallback));
      
        
    }

    var userImageUpdate = function (base64String) {
        function updateCompleteCompletionCallback () {
            fetchUserProfile(showUserProfile);
        }

        var userObject = {
            userImage: base64String
        }
        
        commandExecuter.execute(new kony.mvc.Business.Command("com.kony.Profile.setDefaultUserAccounts", userObject, updateCompleteCompletionCallback))
    }

    var showEmailAddError = function (errorMessage) {
        presentor({
            isLoading: false,
            emailError: errorMessage
        })
    }
    var showEditEmailError = function (errorMessage) {
        presentor({
            isLoading: false,
            editEmailError: errorMessage
        })
    }




    return {
        init: function (presentUserInterface, businessController) {
            presentor = presentUserInterface;
            commandExecuter =  businessController;
        },
        showUserProfile: function () {
            presentor({isLoading:true});
            fetchUserProfile(showUserProfile);
        },
        showUserEmail: function () {
            presentor({isLoading: true});
            fetchUserEmails(false,showUserEmails);
        },
       getUserEmail: function () {
            presentor({isLoading: true});
            fetchUserEmails(false,getEmails);
        },
        saveEmail: function (viewModel) {
            presentor({isLoading: true})
            saveEmail(viewModel,  function (){fetchUserEmails(true, showUserEmails)}, showEmailAddError)
        },
        showUserAddresses: function () {
            presentor({isLoading: true});
            fetchUserAddresses(showUserAddress);
        },
        showUserPhones: function () {
            presentor({isLoading: true});
            fetchUserPhones(showUserPhones);
        },
        getAddPhoneNumberViewModel: function () {
            presentor({isLoading: true});
            fetchAccounts(showAddPhoneNumberForm);            
        },
        savePhoneNumber: function (viewModel) {
            presentor({isLoading: true})
            savePhoneNumber(viewModel, fetchUserPhones.bind(undefined, showUserPhones), onSavePhoneError);
        },
        saveAddress: function (viewModel) {
            presentor({isLoading: true})
            saveAddress(viewModel, function (){fetchUserAddresses(showUserAddress)});
        },
        deleteEmail: function (viewModel) {
            presentor({isLoading: true})            
            deleteEmail(viewModel,  function (){fetchUserEmails(true, showUserEmails)})
        },
        deleteAddress: function (viewModel) {
            presentor({isLoading: true})            
            deleteAddress(viewModel,  function (){fetchUserAddresses(showUserAddress)})
        },
        editEmail: function (viewmodel) {
            presentor({isLoading: true})
            updateEmail(viewmodel,  function (){fetchUserEmails(true, showUserEmails), showEditEmailError})
        },
        getAddNewAddressViewModel: function () {
            presentor({isLoading: true});
            fetchCountryList(showAddAddressViewModel);
        },
        getEditAddressViewModel: function(address) {
            presentor({isLoading: true});
            fetchCountryList(showEditAddressForm.bind(undefined, address));
        },
        updateAddress: function (viewmodel) {
            presentor({isLoading: true})
            updateAddress(viewmodel,  function (){fetchUserAddresses(showUserAddress)})
        },
        makePhoneNumberPrimary: function (phoneObject) {
            partialUpdatePhoneNumber(phoneObject, {isPrimary: "1"}, function() {});
        },
        changeReceivePromotions: function (phoneObject, value) {
            partialUpdatePhoneNumber(phoneObject, {receivePromotions:value ? 1 : 0, isPrimary: phoneObject.isPrimary === "1" ? 1 : 0} ,function() {});
        },
        getPhoneDetails: function (phoneObject) {
            presentor({isLoading: true})
            fetchAccounts(showPhoneDetails.bind(undefined,phoneObject))
        },
        attachPhoneToAccount: function (accountId, phoneId) {
            attachPhoneNumberToAccount(phoneId, accountId);
        },
        removePhoneFromAccount: function (accountId, phoneId) {
            attachPhoneNumberToAccount("null", accountId);
        },
        deletePhone: function (phoneObject) {
            presentor({isLoading: true});
            deletePhone(phoneObject, fetchUserPhones.bind(undefined, showUserPhones))
        },
        editPhone: function (phone) {
            presentor({isLoading: true});
            fetchAccounts(showEditPhoneNumberForm.bind(undefined, phone));            
        },
        editPhoneNumber: function (id, phoneViewModel) {
            presentor({isLoading: true});
            editPhone(id, phoneViewModel, fetchUserPhones.bind(undefined, showUserPhones), showEditPhoneNumberError);            
        },
        userImageUpdate: function (base64String) {
            presentor({isLoading: true});
           userImageUpdate(base64String);
        }
    }
})