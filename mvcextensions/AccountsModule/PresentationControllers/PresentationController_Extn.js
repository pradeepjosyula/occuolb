define(['formatDate', 'ErrHandler', 'DateUtils', 'CommonUtilities', 'OLBConstants','CommonUtilitiesOCCU'], function (formatDate, ErrHandler, DateUtils, CommonUtilities, OLBConstants,CommonUtilitiesOCCU){
  var _transactions = [];
  var transactionTypes = {
        Both: 'i18n.accounts.allTransactions',
      //for occu
       // Deposit: 'i18n.accounts.deposits',
       // Withdrawal: 'i18n.accounts.withdrawls',
		Deposits: 'i18n.accounts.deposits',
        Withdrawals: 'i18n.accounts.withdrawls',
        Checks: 'i18n.accounts.checks',
        Transfers: 'i18n.accounts.transfers',
        BillPay: 'i18n.accounts.billPay',
        P2PDebits: 'i18n.accounts.p2pDebits'
      //for occu
       // P2PCredits: 'i18n.accounts.p2pCredits'

    };
	var timePeriods = {
        ANY_DATE: 'i18n.accounts.anyDate',
        LAST_SEVEN_DAYS: 'i18n.accounts.lastSevenDays',
        LAST_THIRTY_DAYS: 'i18n.accounts.lastThirtyDays',
        LAST_SIXTY_DAYS: 'i18n.accounts.lastSixtyDays',
        CUSTOM_DATE_RANGE: 'i18n.accounts.customDateRange'
    };
  var objectToListBoxArray = function (obj) {
        var list = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                list.push([key, kony.i18n.getLocalizedString(obj[key])]);
            }
        }
        return list;
    };

  return {
	getFetchInputs: function(){
	return dataInput;
	},
	setFetchInputs(fetchInputs){
	dataInput = fetchInputs;
	},
    getTransactions: function(){
     return _transactions;
   },
   setTransactions:function(trans){
     _transactions = trans;
   },
    getAccountDetailsTransactionsViewModel : function (account) {
        var scopeObj = this;
        var viewModel = {}; //local variable to store transactions view model.
        scopeObj.transactonsConfig = {
            'All': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Checks': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Deposits': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Transfers': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Withdrawals': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Payments': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Purchases': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Interest': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            }
        };
        scopeObj.viewModel.transactionsViewModel = {
            searchViewModel: {
                searchPerformed: false,
                visible: false,
                searchResults: [],
                keyword: '',
                transactionTypes: objectToListBoxArray(transactionTypes),
                transactionTypeSelected: OLBConstants.BOTH,
                timePeriods: objectToListBoxArray(timePeriods),
                timePeriodSelected: OLBConstants.ANY_DATE,
                fromCheckNumber: '',
                toCheckNumber: '',
                fromAmount: '',
                toAmount: '',
                getTransactionTypeFromKey: function (key) {
                    return kony.i18n.getLocalizedString(transactionTypes[key]);
                },
                getTimePeriodFromKey: function (key) {
                    return kony.i18n.getLocalizedString(timePeriods[key]);
                },
                fromDate: DateUtils.getCurrentDateString(),
                toDate: DateUtils.getCurrentDateString(),
                toggleSearchView: function () {
                  //for occu
                    /*if (viewModel.showingType != OLBConstants.ALL) {
                       // viewModel.showTransactionsByType()
                      viewModel.renderTransactionTabs = 2;
                        viewModel.searchViewModel.visible = true;
                    }
                    else if (viewModel.searchViewModel.searchPerformed) {
                        //viewModel.showTransactionsByType()
                      viewModel.renderTransactionTabs = 2;
                    }
                    else {*/
                        viewModel.searchViewModel.visible = !viewModel.searchViewModel.visible;
                        viewModel.renderTransactions = false;
                        viewModel.renderTransactionTabs = 2;
                        scopeObj.presentAccountDetailsTransactions(viewModel);
                   // }
                },
                performSearch: function (updatedViewModel) {
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: ['transactionDetails']
                    });
                    scopeObj.fetchTransactionsBySearch(account, updatedViewModel, scopeObj.showTransactionsSearchResults.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
                },
                modifySearch: function (updatedViewModel) {
                    viewModel.searchViewModel = updatedViewModel;
                    viewModel.searchViewModel.visible = true;
                    viewModel.searchViewModel.searchPerformed = false;
                    viewModel.renderTransactions = false;
                  //for occu
                  	viewModel.renderTransactionTabs = 2;
                    scopeObj.presentAccountDetailsTransactions(viewModel);

                },
                clearSearch: function (updatedViewModel) {
                    scopeObj.initTransactionsViewModel();
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: ['transactionDetails']
                    });
                    viewModel.showTransactionsByType()
                },
            },
            showingType: null,
            pendingTransactions: [],
            recentTransactions: [],
            page: 0,
            paginationString: '',
            renderTransactions: true,
            isLastPageFlag: false,
            nextPage: function (dataInputs) {
                if (viewModel.canGoNext()) {
                    viewModel.page += 1;
                    scopeObj.transactonsConfig[viewModel.showingType].offset = viewModel.page * OLBConstants.PAGING_ROWS_LIMIT;
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: ['transactionDetails']
                    });
                    scopeObj.showTransactions(account, viewModel.showingType, dataInputs);
                }
            },
            prevPage: function (dataInputs) {
                if (viewModel.canGoPrev()) {
                    viewModel.page -= 1;
                    scopeObj.transactonsConfig[viewModel.showingType].offset = viewModel.page * OLBConstants.PAGING_ROWS_LIMIT;
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: ['transactionDetails']
                    });
                    scopeObj.showTransactions(account, viewModel.showingType, dataInputs);
                }
            },
            canGoNext: function () {
                return !scopeObj.isLastPage(viewModel);
            },
            canGoPrev: function () {
                return viewModel.page > 0;
            },
            gotoFirstPage: function (dataInputs) {
                page = 0;
                scopeObj.updateLoadingForCompletePage({
                    isLoading: true,
                    serviceViewModels: ['transactionDetails']
                });
                scopeObj.showTransactions(account, viewModel.showingType, dataInputs);
            },
            gotoLastPage: function (dataInputs) {
                //TODO : requires getting count of all transactions
                scopeObj.updateLoadingForCompletePage({
                    isLoading: true,
                    serviceViewModels: ['transactionDetails']
                });
                scopeObj.showTransactions(account, viewModel.showingType, dataInputs);
            },
            showTransactionsByType: function (dataInputs) {
                dataInputs = dataInputs || {};
                scopeObj.initTransactionsViewModel();
                scopeObj.updateLoadingForCompletePage({
                    isLoading: true,
                    serviceViewModels: ['transactionDetails']
                });
                scopeObj.showTransactions(account, dataInputs.type || viewModel.showingType, dataInputs);
            },
            config: null //store fetched transaction config to maintain sort and pagination

        };
        viewModel = scopeObj.viewModel.transactionsViewModel;
        scopeObj.initTransactionsViewModel();
        viewModel.showTransactionsByType({
            resetSorting: true,
            type: OLBConstants.ALL
        });
        return {
            showTransactionsByType: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs);
            },
            showAll: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.ALL
                });
            },
            showChecks: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.CHECKS
                });
            },
            showDeposits: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.DEPOSITS
                });
            },
            showTransfers: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.TRANSFERS
                });
            },
            showWithdrawals: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.WITHDRAWLS
                });
            },
            showInterest: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.INTEREST
                });
            },
            showPurchase: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.PURCHASES
                });
            },
            showPayment: function (dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.PAYMENTS
                });
            },
            nextPage: function () {
                viewModel.nextPage();
            },
            prevPage: function () {
                viewModel.prevPage();
            }
        };
    },

    createAccountSummaryViewModal : function (account) {
        var scopeObj = this;
        return {
            accountName: account.accountName,
            nickName: account.nickName,
            accountNumber: account.accountID,
            type: account.accountType,
            availableBalance: scopeObj.formatCurrency(account.availableBalance),
            currentBalance: scopeObj.formatCurrency(account.currentBalance),
			outstandingBalance: scopeObj.formatCurrency(account.outstandingBalance),
            isFavourite: scopeObj.isFavourite(account),
           /*added for OCCU project */
            shareId:account.shareId,
            balanceType:account.balanceType,
            opttransfer:account.opttransfer,
            optpayBill:account.optpayBill,
            optdebitCard:account.optdebitCard,
            optviewDetails:account.optviewDetails,
            optisFavorite:account.optisFavorite,
            optdepositCheck:account.optdepositCheck,
            optsendMoney:account.optsendMoney,
            optstopCheck:account.optstopCheck,
            optmakePayment:account.optmakePayment,
          	hexcolor:account.accounthexColor,
            isPrimaryOwner:account.isPrimaryOwner,
            toggleFavourite: function () {
                scopeObj.updateLoadingForCompletePage({
                    isLoading : true,
                    serviceViewModels : ['accountsSummary']
                })
                scopeObj.changeAccountFavouriteStatus(account, scopeObj.refreshData.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
            },
            openQuickActions: function () {
                scopeObj.onQuickActionsMenu(account);
            },
            onAccountSelection: function () {
                return scopeObj.navigateToAccountDetails(account);
            }
        };
    },

   fetchTransactionsBySearch : function (account, searchViewModel, successCallback, errorCallBack) {
        var scopeObj = this;
     	var transactionTypeSelected = "";
			if(searchViewModel.transactionTypeSelected == OLBConstants.BOTH){
				transactionTypeSelected =  "All";
			}else{
				transactionTypeSelected = searchViewModel.transactionTypeSelected;
			}

        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                successCallback(response.data);
            }
            else {
                errorCallBack(response.data);
            }
        }
        var dateRange = this.getDateRangeForTimePeriod(searchViewModel);
        var commandObj = {
            searchTransactionType: transactionTypeSelected,
            searchDescription: searchViewModel.keyword,
            searchMinAmount: searchViewModel.fromAmount,
            searchMaxAmount: searchViewModel.toAmount,
            searchStartDate: this.handleDateFormat(new Date(dateRange.startDate)),
            searchEndDate: this.handleDateFormat(new Date(dateRange.endDate)),
            fromCheckNumber: searchViewModel.fromCheckNumber,
            toCheckNumber: searchViewModel.toCheckNumber,
          	cursor:"",
          //code for occu
            //accountNumber: account.accountID
          accountID: account.accountID,
          	account:account
        }
        scopeObj.businessController.execute(new kony.mvc.Business.Command('com.kony.accounts.advancedSearch', commandObj, completionCallback));
        //scopeObj.businessController.execute(new kony.mvc.Business.Command('com.kony.accounts.searchAccountTransactions', commandObj, completionCallback));
    },
     handleDateFormat : function(date) {
      if(date === undefined || date === null || date === ''){
        return null;
      }
      if(date instanceof Date){
        return ((date.getMonth()+1) < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1) +"/"+(date.getDate() < 10 ? '0' + date.getDate() : date.getDate())+"/"+date.getFullYear();    
      } else {
        var dateObj  = new Date(date); 
        return ((dateObj.getMonth()+1) < 10 ? '0' + (dateObj.getMonth()+1) : dateObj.getMonth()+1)+"/"+(dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate())+"/"+dateObj.getFullYear();         
      }
  },
     getDateRangeForTimePeriod :function(searchviewmodel) {
        var startDate = null;
        var endDate = null;
        if (searchviewmodel.timePeriodSelected === OLBConstants.ANY_DATE) {
        var today = new Date();
         endDate = DateUtils.toDateFormat(today, 'dd-mm-yyyy');
          var customYear = today.getFullYear()-2;
				today.setFullYear(customYear);
                startDate = DateUtils.toDateFormat(today, 'dd-mm-yyyy');
        }
        else if (searchviewmodel.timePeriodSelected === 'CUSTOM_DATE_RANGE') {
            startDate = DateUtils.uiFormatToBackend(searchviewmodel.fromDate);
            endDate = DateUtils.uiFormatToBackend(searchviewmodel.toDate);
        }
        else {
            var dateConfig = {
                LAST_SEVEN_DAYS: 7,
                LAST_THIRTY_DAYS: 30,
                LAST_SIXTY_DAYS: 60
            }
            var today = new Date();
            var endDate = DateUtils.toDateFormat(today, 'dd-mm-yyyy');
            today.setDate(today.getDate() - dateConfig[searchviewmodel.timePeriodSelected])
            var startDate = DateUtils.toDateFormat(today, 'dd-mm-yyyy');
        }
        return {
            startDate: startDate,
            endDate: endDate
        }
    },
	     
	showTransactionsSearchResults : function (transactions) {
        var scopeObj = this;
        var viewModel = scopeObj.viewModel.transactionsViewModel;
      if(transactions.length){
        
      	transactions = transactions.filter(function (_, i) {
            return i < OLBConstants.PAGING_ROWS_LIMIT;
        });

        
        viewModel.showingType = OLBConstants.ALL;
        viewModel.searchViewModel.searchPerformed = true;
        if (Array.isArray(transactions) && transactions.length > 0) {
            viewModel.paginationString = scopeObj.displayedPaginationString(viewModel, transactions.length);
        } else {
            viewModel.paginationString = false;
        }
        var toTransactionViewModel = function (transaction) {
            return scopeObj.getTransactionViewModel(transaction, viewModel.accountType, OLBConstants.ALL);
        };
        viewModel.pendingTransactions = transactions.filter(scopeObj.isPendingTransaction).map(toTransactionViewModel);
        viewModel.recentTransactions = transactions.filter(scopeObj.isSuccessfulTransacion).map(toTransactionViewModel);
        viewModel.renderTransactions = true;
        //viewModel.renderTransactionTabs = false;
        viewModel.renderTransactionTabs = 1;
      }else{
       // viewModel.renderTransactionTabs = true;
        viewModel.renderTransactionTabs = 3;
        viewModel.renderTransactions = false;
      }
        scopeObj.presentAccountDetailsTransactions(viewModel);
    },
      
   fetchQuickActions: function(account, success, error){
     //Quick actions Configuration.
        kony.print("account is "+JSON.stringify(account));
        //modified for OCCU project
        var qckactions = [];
            if(account.opttransfer == "true" || account.opttransfer == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"));  
            }
			if(account.optpayBill == "true" || account.optpayBill == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.PayBill"));   
            }
			if(account.optdebitCard == "true" || account.optdebitCard == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.DebitCard"));  
            }
			if(account.optviewDetails == "true" || account.optviewDetails == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.ViewDetails")); 
            }
			if(account.favouriteStatus && account.favouriteStatus === '1') {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.UntagasFavorite"));
            } else{
			  qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.TagasFavorite"));
			}
			if(account.optdepositCheck == "true" || account.optdepositCheck == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"));  
            }
			if(account.optsendMoney == "true" || account.optsendMoney == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.SendMoney"));   
            }
			if(account.optstopCheck == "true" || account.optstopCheck == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.StopCheck"));  
            }
			if(account.optmakePayment == "true" || account.optmakePayment == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.MakePayment"));  //payDueAmountDisplayName
            }
            /*if(account.isPrimaryOwner == "true" || account.isPrimaryOwner == true) {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.viewStatements"));  //payDueAmountDisplayName
            }*/
            success({
                account: account,
                actions: qckactions
            });
   },
   isValidAction: function(actionName, dataItem) {
            var isValid = false;
            var principalBalance, currentAmountDue, eStatementsFlag;
            switch (actionName) {
                case OLBConstants.ACTION.PAY_DUE_AMOUNT:
                    principalBalance = Number(dataItem.principalBalance) ? Number(dataItem.principalBalance) : 0;
                    currentAmountDue = Number(dataItem.currentAmountDue) ? Number(dataItem.currentAmountDue) : 0;
                    isValid = CommonUtilities.getConfiguration("loanPaymentEnabled") === "true" && (principalBalance > 0 && currentAmountDue > 0);
                    break;
                case OLBConstants.ACTION.PAYOFF_LOAN:
                    principalBalance = Number(dataItem.principalBalance) ? Number(dataItem.principalBalance) : 0;
                    isValid = CommonUtilities.getConfiguration("payOffLoanPaymentEnabled") === "true" && (principalBalance > 0);
                    break;
                default:
                    isValid = true;
            }
            return isValid;
        },
        getValidActions: function(actions, isValidAction, dataInput) {
            var scopeObj = this;
            return actions.filter(function(action) {
                return scopeObj.isValidAction(action, dataInput);
            });
        },
        todoAction: function(data) {
            //alert("Coming Soon!!! Functionality is under progress , Apologies for the inconvenience caused.
        },
        getQuickActions: function(dataInput) {
            var scopeObj = this;
            var onCancel = dataInput.onCancel || function() {
                ErrorHandler.onError("Cancel Event not provided");
            };
            //modified for OCCU project
            var quickActions = [{
                actionName: OLBConstants.ACTION.SCHEDULED_TRANSACTIONS,
                displayName: dataInput.scheduledDisplayName || kony.i18n.getLocalizedString("i18n.accounts.scheduledTransactions"),
                action: function(account) {
                    if (dataInput.showScheduledTransactionsForm) {
                        dataInput.showScheduledTransactionsForm(account);
                    }
                }
            }, {
                actionName: kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"), //OLBConstants.ACTION.MAKE_A_TRANSFER, //MAKE A TRANSFER
                displayName: dataInput.makeATransferDisplayName,
                action: function(account) {
                    //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                    var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    transferModule.presentationController.showTransferScreen({
                        accountObject: account,
                        onCancelCreateTransfer: onCancel
                    });
                }
            }, {
                actionName: OLBConstants.ACTION.TRANSFER_MONEY, //MAKE A TRANSFER
                displayName: dataInput.tranferMoneyDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.TransferMoney"),
                action: function(account) {
                    //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                    var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    transferModule.presentationController.showTransferScreen({
                        accountObject: account,
                        onCancelCreateTransfer: onCancel
                    });
                }
            }, {
                actionName: "Pay Bill", //OLBConstants.ACTION.PAY_A_BILL,
                displayName: dataInput.payABillDisplayName,
                action: function(account) {
                    //Function call to open bill pay screen
                    var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    billPayModule.presentationController.showBillPayData("quickAction", {
                        "fromAccountNumber": account.accountID,
                        "show": 'PayABill',
                        "onCancel": onCancel
                    });
                }
            }, {
                actionName: OLBConstants.ACTION.PAY_A_PERSON_OR_SEND_MONEY,
                displayName: dataInput.sendMoneyDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payAPersonFrom"),
                action: function(account) {
                    /* var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                     var dataItem = account;
                     dataItem.show = "SendOrRequest";
                     dataItem.onCancel = onCancel;
                     p2pModule.presentationController.showPayAPerson(dataItem);*/
                }
            }, {
                actionName: "Make Payment", //OLBConstants.ACTION.PAY_DUE_AMOUNT,
                displayName: dataInput.payDueAmountDisplayName,
                action: function(account) {
                    account.currentAmountDue = CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue);
                    account.currentAmountDue = account.currentAmountDue.slice(1);
                    var data = {
                        "accounts": account
                    };
                    var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                    loanModule.presentationController.navigateToLoanDue(data);
                }
            }, {
                actionName: OLBConstants.ACTION.PAYOFF_LOAN,
                displayName: dataInput.payoffLoanDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan"),
                action: function(account) {
                    account.principalBalance = CommonUtilities.formatCurrencyWithCommas(account.principalBalance);
                    account.payOffCharge = CommonUtilities.formatCurrencyWithCommas(account.payOffCharge);
                    var data = {
                        "accounts": account
                    };
                    var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                    loanModule.presentationController.navigateToLoanPay(data);
                }
            }, {
                actionName: OLBConstants.ACTION.VIEW_BILL,
                displayName: dataInput.viewBillDisplayName || kony.i18n.getLocalizedString("i18n.accounts.viewBill"),
                action: function(account) {
                    scopeObj.todoAction();
                }
            }, {
                actionName: dataInput.viewStatementsDisplayName,
                displayName: dataInput.viewStatementsDisplayName,
                action: function(account) {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.navigateToAccountDetails(account)
                    accountsModule.presentationController.showFormatEstatements(account);
                }
            }, {
                actionName: OLBConstants.ACTION.UPDATE_ACCOUNT_SETTINGS,
                displayName: dataInput.updateSettingsAndPreferenceDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateAccountSettings"),
                action: function(account) {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.showPreferredAccounts();
                }
            }, {
                actionName: dataInput.markasfavourite,
                displayName: dataInput.markasfavourite,
                action: function(account) {
                    var currForm = kony.application.getCurrentForm();
                    var index = currForm.accountList.segAccounts.selectedIndex[1];
                    var segmentData = currForm.accountList.segAccounts.data[index][1][0];
                    segmentData.toggleFavourite();
                }
            }, {
                actionName: dataInput.unmarkasfavourite,
                displayName: dataInput.unmarkasfavourite,
                action: function(account) {
                    var currForm = kony.application.getCurrentForm();
                    var index = currForm.accountList.segAccounts.selectedIndex[1];
                    var segmentData = currForm.accountList.segAccounts.data[index][1][0];
                    segmentData.toggleFavourite();
                }
            }, {
                actionName: OLBConstants.ACTION.ORDER_CHECKS,
                displayName: dataInput.orderChecksDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.OrderChecks"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.REQUEST_OR_REPLACE_CARD,
                displayName: dataInput.requestOrReplaceCardDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.REPORT_LOST_OR_STOLEN,
                displayName: dataInput.reportLostOrStolenDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.reportLostOrStolen"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.SETUP_NEW_PIN,
                displayName: dataInput.setupNewPinDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.setupNewPIN"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.LOCK_OR_DECACTICATE_CARD,
                displayName: dataInput.lockOrDeactivateCardDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.LockCardOrDeactivateCard_Temporary"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.TRAVEL_NOTIFICATION,
                displayName: dataInput.travelNotificationDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.TravelNotification"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.GET_ASSISTANCE,
                displayName: dataInput.getAssitanceDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.getAssistance"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.ECHECK_OR_ROUTING_DETAILS,
                displayName: dataInput.echeckOrRoutingDetailsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.eCheckRoutingDetails"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.REWARDS_POINTS,
                displayName: dataInput.rewardsPointsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.rewardsPoints"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: OLBConstants.ACTION.ACCOUNT_SETTINGS,
                displayName: dataInput.accountSettingsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateSettingAndPreferences"),
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: kony.i18n.getLocalizedString("i18n.accounts.DebitCard"),
                displayName: dataInput.debitcard,
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: kony.i18n.getLocalizedString("i18n.accounts.ViewDetails"),
                displayName: dataInput.viewdetails,
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"),
                displayName: dataInput.depositcheck,
                action: function(account) {
                   scopeObj.todoAction();
                }
            }, {
                actionName: kony.i18n.getLocalizedString("i18n.accounts.StopCheck"),
                displayName: dataInput.stopcheck,
                action: function(account) {
                   scopeObj.todoAction();
                }
            }];
            return quickActions;
        },
        getQuickActionsViewModel: function(account, actions) {
            kony.print("in getQuickActionsViewModel");
            var scopeObj = this,
                accountType = account.accountType,
                validActions,
                finalActionsViewModel = [];
            /**
             * Method to handle Cancel quick action.
             */
            var onCancel = function() {
                scopeObj.showAccountDetails(account);
            };
            if (accountType) {
                if (actions.length) {
                    validActions = this.getValidActions(actions, scopeObj.isValidAction, account);
                    finalActionsViewModel = validActions.map(function(action) { //get action object.
                        //modified for OCCU project
                        var quickActions = scopeObj.getQuickActions({
                            onCancel: onCancel,
                            payABillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.PayBill"),
                            sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.accounts.SendMoney"),
                            makeATransferDisplayName: kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"),
                            viewStatementsDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewStatements"),
                            markasfavourite: kony.i18n.getLocalizedString("i18n.accounts.TagasFavorite"),
                            unmarkasfavourite: kony.i18n.getLocalizedString("i18n.accounts.UntagasFavorite"),
                            debitcard: kony.i18n.getLocalizedString("i18n.accounts.DebitCard"),
                            viewdetails: kony.i18n.getLocalizedString("i18n.accounts.ViewDetails"),
                            depositcheck: kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"),
                            stopcheck: kony.i18n.getLocalizedString("i18n.accounts.StopCheck"),
                            payDueAmountDisplayName: kony.i18n.getLocalizedString("i18n.accounts.MakePayment")
                        })
                        return scopeObj.getAction(quickActions, action, account);
                    });
                }
            }
            return finalActionsViewModel;
        },
    
        getTransactionViewModel : function (transaction, accountType, requestedTransactionType) {
        var scopeObj = this;

        var numberOfRecurrences;

        if(transaction.numberOfRecurrences == undefined ||  transaction.numberOfRecurrences == null || transaction.numberOfRecurrences == "0"){
            numberOfRecurrences =  kony.i18n.getLocalizedString('i18n.common.none');
        } else {
            numberOfRecurrences =  transaction.numberOfRecurrences;
        }
      //for occu
        var amountTrim = ((transaction.amount).replace(/ +/g, ""));
        return {
            date: formatDate(transaction.transactionDate),
            description: transaction.description || kony.i18n.getLocalizedString('i18n.common.none'),
            statusDescription: transaction.statusDescription.toLowerCase(),
            notes: (transaction.transactionsNotes == null ||  transaction.transactionsNotes == undefined) ? kony.i18n.getLocalizedString("i18n.common.none") : transaction.transactionsNotes,
            type: transaction.transactionAction,
            fromAccountName: transaction.fromAccountName,
            externalAccountNumber: transaction.ExternalAccountNumber,
            showTransactionIcons: scopeObj.isTransactionTypeIconsVisible(accountType, requestedTransactionType),
            fromAcc: transaction.fromAccountNumber,
            toAcc: transaction.toAccountNumber,
            reference: transaction.transactionId,
            frequencyType: transaction.frequencyType || kony.i18n.getLocalizedString('i18n.transfers.frequency.once'),
              amount: CommonUtilities.getDisplayCurrencyFormat(amountTrim),
            //amount: CommonUtilities.getDisplayCurrencyFormat(transaction.amount.replace(/ +/g, "")),
            balance: transaction.fromAccountBalance ? CommonUtilities.getDisplayCurrencyFormat(transaction.fromAccountBalance) : kony.i18n.getLocalizedString('i18n.common.none'),
            category: transaction.category,
            accountType: accountType,
            nickName: transaction.toAccountName || transaction.payPersonName || transaction.payeeNickName || transaction.payeeName,
            numberOfRecurrences: numberOfRecurrences,
            transaction: transaction,
            //checks
            frontImage1 : transaction.frontImage1,
            frontImage2 : transaction.frontImage2,
            backImage1 : transaction.backImage1,
            backImage2 : transaction.backImage2,
            memo : transaction.memo ? transaction.memo : "",
            checkNumber1: transaction.checkNumber1,
            checkNumber2: transaction.checkNumber2,
            bankName1: transaction.bankName1,
            bankName2: transaction.bankName2,
            withdrawlAmount1: transaction.withdrawlAmount1 ? CommonUtilities.getDisplayCurrencyFormat(transaction.withdrawlAmount1) : "",
            withdrawlAmount2: transaction.withdrawlAmount2 ? CommonUtilities.getDisplayCurrencyFormat(transaction.withdrawlAmount2) : "",
            totalAmount: transaction.totalCheckAmount ? CommonUtilities.getDisplayCurrencyFormat(transaction.totalCheckAmount) : "",
            cashAmount: transaction.cashAmount ? CommonUtilities.getDisplayCurrencyFormat(transaction.cashAmount) : "",

        };
    },

    outputDataCheck : function (dataString) {
        if (typeof dataString === 'string') {
            return dataString;
        } else {
            return null;
        }
    },
    displayInterest : function (interestString) {
        return this.outputDataCheck(this.toInterestFormat(interestString));
    },
    toInterestFormat : function (interestString) {
        if (interestString) {
            return interestString + '%';
        } else {
            return null;
        }
    },
    displayAccountNumber : function (accountNumberString) {
        if (typeof accountNumberString === 'string' && accountNumberString.length > 0) {
            return this.accountNumberMask(accountNumberString);
        } else {
            return outputDataCheck(null);
        }
    },
     accountNumberMask : function (accountNumber) {
        var stringAccNum = accountNumber;
        var isLast4Digits = function isLast4Digits(index) {
            return index > stringAccNum.length - 5;
        };
        return stringAccNum.split('').map(function (c, i) {
            return isLast4Digits(i) ? c : 'X';
        }).join('');
    },
    
    getAccountDetailsViewModel : function (account) {
        var scopeObj = this; 
        return {
            accountType: account.accountType,
            accountSummary: {
                availableBalance: scopeObj.formatCurrency(account.availableBalance),
                eStatementEnable: account.currentAmountDueeStatementEnable,
                currentBalance: scopeObj.formatCurrency(account.currentBalance),
                pendingDeposit: scopeObj.formatCurrency(account.pendingDeposit),
                pendingWithdrawals: scopeObj.formatCurrency(account.pendingWithdrawal),
                currentDueAmount: scopeObj.formatCurrency(account.currentAmountDue),
                creditLimit: scopeObj.formatCurrency(account.creditLimit),
                principalBalance: scopeObj.formatCurrency(account.principalBalance),
                principalAmount: scopeObj.formatCurrency(account.principalValue),              
                payOffLoan: scopeObj.formatCurrency(account.principalBalance),
              //for OCCU
              	productBalance: scopeObj.formatCurrency(account.productBalance),
              	productBalanceType: account.productBalanceType,
              	balanceType: account.balanceType,
              	hexcolor: account.accounthexColor,
                maturityDate: CommonUtilities.formatDate(account.maturityDate),
                maturityOption: scopeObj.outputDataCheck(account.maturityOption),
                asOfDate: kony.i18n.getLocalizedString("i18n.accounts.AsOf") + CommonUtilities.formatDate(new Date().toISOString()),
                availableCredit: scopeObj.formatCurrency(account.availableCredit),
                interestEarned: scopeObj.formatCurrency(account.interestEarned),
                paymentTerm: scopeObj.outputDataCheck(account.paymentTerm)
            },
            balanceAndOtherDetails: {
                totalCredit: scopeObj.formatCurrency(account.totalCreditMonths),
                totalDebits: scopeObj.formatCurrency(account.totalDebitsMonth),
                bondInterestLastYear: scopeObj.displayInterest(account.bondInterestLastYear),
                dividendPaidLastYear: scopeObj.formatCurrency(account.dividendPaidLastYear),
                dividendRate: scopeObj.displayInterest(account.dividendRate),
                dividendPaidYTD: scopeObj.formatCurrency(account.dividendPaidYTD),
                lastDividendPaid: scopeObj.formatCurrency(account.lastDividendPaidAmount),
                paidOn: CommonUtilities.formatDate(account.lastPaymentDate),
                minimumDueAmount: scopeObj.formatCurrency(account.minimumDue),
                paymentDueDate: CommonUtilities.formatDate(account.dueDate),
                lastStatementBalance: scopeObj.formatCurrency(account.lastStatementBalance),
                lastPaymentDate: CommonUtilities.formatDate(account.lastPaymentDate),
                rewardsBalance: scopeObj.outputDataCheck(account.availableCredit),
                interestRate: scopeObj.displayInterest(account.interestRate),
                interestPaidYTd: scopeObj.formatCurrency(account.interestPaidYTD),
                interestPaidLastYear: scopeObj.formatCurrency(account.interestPaidLastYear),
                lastPaymentAmount: scopeObj.formatCurrency(account.lastPaymentAmount),
                originalAmount: scopeObj.formatCurrency(account.originalAmount),
                originalDate: CommonUtilities.formatDate(account.openingDate),
                payOffCharge: scopeObj.formatCurrency(account.payOffCharge),
                closingDate: CommonUtilities.formatDate(account.closingDate),
                loanClosingDate: account.closingDate,
                outstandingBalance: scopeObj.formatCurrency(account.outstandingBalance),
                dividendLastPaidAmount: scopeObj.formatCurrency(account.dividendLastPaidAmount),
                dividendLastPaidDate: CommonUtilities.formatDate(account.dividendLastPaidDate),
                previousYearsDividends: scopeObj.formatCurrency(account.previousYearsDividends),
                maturityAmount: scopeObj.formatCurrency(account.maturityAmount),
                regularPaymentAmount: scopeObj.formatCurrency(account.regularPaymentAmount)
            },
            accountInfo: {
                //modiufied for OCCU
                accountName: account.accountName,
                accountNumber: scopeObj.displayAccountNumber(account.accountID),
                accountID: account.accountID,
                accountType: scopeObj.outputDataCheck(account.accountType),
                routingNumber: scopeObj.outputDataCheck(account.routingNumber),
                swiftCode: scopeObj.outputDataCheck(account.swiftCode),
                primaryAccountHolder: scopeObj.outputDataCheck(account.accountHolder),
                jointHolder: scopeObj.outputDataCheck(account.jointHolders),
                creditIssueDate: CommonUtilities.formatDate(account.openingDate),
                creditcardNumber: scopeObj.outputDataCheck(account.creditCardNumber),
                accountHolder:account.accountHolder,
                shareId:account.shareId
            },
            rightSideActions: (function (account) {
                var onCancel = function () {
                    scopeObj.showAccountDetails(account);
                };
                //Right side action object array
                var RightSideActions = scopeObj.getQuickActions({
                    onCancel: onCancel,
                    showScheduledTransactionsForm: scopeObj.showScheduledTransactionsForm.bind(scopeObj),
                    payABillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payABill"),
                    makeATransferDisplayName: kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"),
                    payDueAmountDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payDueAmount"),
                    payoffLoanDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan_Caps"),
                    viewStatementsDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewStatements"),
                    viewBillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewBill"),
                    markasfavourite: kony.i18n.getLocalizedString("i18n.accounts.TagasFavorite"),
     				//code added for OCCU PrimaryActions
                  	unmarkasfavourite: kony.i18n.getLocalizedString("i18n.accounts.UntagasFavorite"),
                    sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.accounts.SendMoney"),
                    debitcard: kony.i18n.getLocalizedString("i18n.accounts.DebitCard"),
                    viewdetails: kony.i18n.getLocalizedString("i18n.accounts.ViewDetails"),
                    depositcheck:  kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"),
                    stopcheck: kony.i18n.getLocalizedString("i18n.accounts.StopCheck")
                });
                var accountType = account.accountType,
                    validActions,
                    finalActions = [];
                if (accountType) {
                 // for OCCU Primary Options
                    //var actions = scopeObj.fetchRightSideActions(account);
                 // if (actions.length) {
                //    validActions = _getValidActions(actions, scopeObj.isValidAction, account);
                   // finalActions = validActions.map(function (action) {  //get action object.
                     // return scopeObj.getAction(RightSideActions, action, account);
                   // });
                 // }
                 
                  var actions;
                  
                  scopeObj.fetchQuickActions(account, function success(response){
                        actions = response.actions;
                  },CommonUtilities.ErrorHandler.onError);
                  
                 if (actions.length) {
                    validActions = scopeObj.getValidActions(actions, scopeObj.isValidAction, account);
                    finalActions = validActions.map(function (action) {  //get action object.
                      return scopeObj.getAction(RightSideActions, action, account);
                    });
                  }
                    
                }
                return finalActions;

            })(account),

            secondaryActions: (function (account) { //What else do you want dropdown
                var onCancel = function () {
                    scopeObj.showAccountDetails(account);
                };
                //Secondary action object array
                var SecondaryActions = scopeObj.getQuickActions({
                    onCancel: onCancel,
                  //Code For SecondaryActions
                   	showScheduledTransactionsForm: scopeObj.showScheduledTransactionsForm.bind(scopeObj),
                    payABillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payABill"),
                    makeATransferDisplayName: kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"),
                    payDueAmountDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payDueAmount"),
                    payoffLoanDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan_Caps"),
                    viewStatementsDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewStatements"),
                    viewBillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewBill"),
                    markasfavourite: kony.i18n.getLocalizedString("i18n.accounts.TagasFavorite"),
                    unmarkasfavourite: kony.i18n.getLocalizedString("i18n.accounts.UntagasFavorite"),
                    sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.accounts.SendMoney"),
                    debitcard: kony.i18n.getLocalizedString("i18n.accounts.DebitCard"),
                    viewdetails: kony.i18n.getLocalizedString("i18n.accounts.ViewDetails"),
                    depositcheck:  kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"),
                    stopcheck: kony.i18n.getLocalizedString("i18n.accounts.StopCheck")
                });
                var accountType = account.accountType,
                    validActions,
                    finalActions = [];
                if (accountType) {
                    //var actions = scopeObj.fetchSecondayActions(account);
                  var actions;
                  
                  scopeObj.fetchQuickActions(account, function success(response){
                        actions = response.actions;
                  },CommonUtilities.ErrorHandler.onError);
                  
                    if (actions.length) {
                        validActions = scopeObj.getValidActions(actions, scopeObj.isValidAction, account);
                        finalActions = validActions.map(function (action) {  //get action object.
                            return scopeObj.getAction(SecondaryActions, action, account);
                        });
                    }
                }
                return finalActions;
            })(account)
        };
    },
    
   onSuccessTransactions: function (transactions, fetchInputs) { 
    var scopeObj = this;
        var viewModel = scopeObj.viewModel.transactionsViewModel;
     	viewModel.showingType = fetchInputs.transactionType;
            viewModel.accountType = fetchInputs.account.accountType;

    if (transactions.length>0){
          
      if(fetchInputs.transactionType == "All"){
   		 scopeObj.setTransactions(transactions);
      }
     scopeObj.setFetchInputs(fetchInputs);
 // var typeBasedTransactions = transactions;
  	 transactions = transactions.filter(function (_, i) {
            return i < OLBConstants.PAGING_ROWS_LIMIT;
        });

    if (transactions.length > OLBConstants.PAGING_ROWS_LIMIT) { //TODO: for pagination ,  required refactoring if standard pagination is ready.
            viewModel.isLastPageFlag = false;
        } else {
            viewModel.isLastPageFlag = true;
        }
    
    
    if (Array.isArray(transactions) && transactions.length > 0) {
      
            viewModel.paginationString = scopeObj.displayedPaginationString(viewModel, transactions.length);
      if (fetchInputs!= null){
      		
            viewModel.config = fetchInputs;
        }
        } else {
            viewModel.paginationString = false;
        }
        var toTransactionViewModel = function (transaction) {
            return scopeObj.getTransactionViewModel(transaction, viewModel.accountType, viewModel.showingType);
        };      
        viewModel.pendingTransactions = transactions.filter(scopeObj.isPendingTransaction).map(toTransactionViewModel);
        viewModel.recentTransactions = transactions.filter(scopeObj.isSuccessfulTransacion).map(toTransactionViewModel);
      	viewModel.renderTransactions = true;
      viewModel.renderTransactionTabs = 0;
       // viewModel.renderTransactionTabs = false;
    }else{
      viewModel.renderTransactionTabs = 3;
       // viewModel.renderTransactionTabs = true;
        viewModel.renderTransactions = false;
      }
     
     scopeObj.presentAccountDetailsTransactions(viewModel);    
  },
    showAllExt: function(){
      var trans = this.getTransactions();
      var dep={
      transactionType : "All",
       account : this.viewModel.accountDetailsViewModel 
      }
    this.onSuccessTransactions(trans,dep);
  }, 
  showChecksExt: function(){
    var checkTransactions =[];
    var trans = this.getTransactions();
        for (i = 0; i < trans.length; i++) {
       if (trans[i].transactionAction == "Withdrawal" && trans[i].transactionSource == "check") {
         checkTransactions.push(trans[i]);
          }
        }
    
    var dep={
      transactionType : OLBConstants.TRANSACTION_TYPE.CHECKS,
      account : this.viewModel.accountDetailsViewModel
    }
   this.onSuccessTransactions(checkTransactions,dep);
  },

  showDepositsExt: function(){
   var DepositTransactions =[];
    var trans = this.getTransactions();
    for(i=0;i<_transactions.length;i++){
      if (trans[i].transactionAction == "Deposit" && trans[i].transactionSource == "null") {
                DepositTransactions.push(trans[i]);
            }
    }
	var dep={
    transactionType : OLBConstants.TRANSACTION_TYPE.DEPOSITS,
    account : this.viewModel.accountDetailsViewModel 
    }
    this.onSuccessTransactions(DepositTransactions,dep);
  },
  showTransfersExt: function(){
    var transferTransactions = [];
    var trans = this.getTransactions();
        for (i = 0; i < trans.length; i++) {
     if (trans[i].transactionAction == "Deposit" && trans[i].transactionSource == "transfer") {
                transferTransactions.push(trans[i]);
            }
    if (trans[i].transactionAction == "Withdrawal" && trans[i].transactionSource == "transfer") {
                transferTransactions.push(trans[i]);
    	}
    }
  
	var dep = {
    transactionType : OLBConstants.TRANSACTION_TYPE.TRANSFERS,
      account : this.viewModel.accountDetailsViewModel
    }
    this.onSuccessTransactions(transferTransactions,dep);
  },
  showWithdrawalsExt: function(){
    var withdrawTransactions =[];
    var trans = this.getTransactions();
        for (i = 0; i < trans.length; i++) {
          if (trans[i].transactionAction == "Withdrawal" && trans[i].transactionSource == "null") {
                    withdrawTransactions.push(trans[i]);
         }
    
  }
  var dep={
    transactionType :  OLBConstants.TRANSACTION_TYPE.WITHDRAWLS,
    account : this.viewModel.accountDetailsViewModel
  }
    
    this.onSuccessTransactions(withdrawTransactions,dep);
  },
  fetchCheckDetails: function(dataInputs){
    //var inputData = this.getFetchInputs();
    //var trans = this.getTransactions();
    var self=this;
    
    function checkCompletionCallback (response) {
      if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
        var datares = response.data;
        imgCheck1 = datares[0].bufferFront;
        imgCheck2 = datares[0].bufferBack;

        var checkViewModel={
          "checkDetails":{
            "imgCheck1":imgCheck1,
            "imgCheck2":imgCheck2
          }
        };
        self.presentUserInterface("frmAccountsDetails",checkViewModel);
      }else{
        errorCallBack(response.data);
      }
    }
    function handleDateFormat(date) {
      if(date === undefined || date === null || date === ''){
        return null;
      }
      if(date instanceof Date){
        return ((date.getMonth()+1) < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1) +"/"+(date.getDate() < 10 ? '0' + date.getDate() : date.getDate())+"/"+date.getFullYear();    
      } else {
        var dateObj  = new Date(date); 
        return ((dateObj.getMonth()+1) < 10 ? '0' + (dateObj.getMonth()+1) : dateObj.getMonth()+1)+"/"+(dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate())+"/"+dateObj.getFullYear();         
      }
    }
    var commandObj = {
      accountNumber: dataInputs.accountNumber,
      checkNumber:dataInputs.checkNumber,
      PaymentDate:handleDateFormat(new Date(dataInputs.date))
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getCheck", commandObj, checkCompletionCallback));
  },
    // successCheck: function(checkTrans){
    // }    
  fetchStatementsList: function(onsuccess, accountID, year) {
            this.updateProgressBarState(true);
            var scopeObj = this;
            var context = {
                "accountID": accountID,
                "startYear": year,
                "endYear": year
            }
            var completionCallback = function(commandResponse) {
                if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                    kony.print("in success response is " + commandResponse.data);
                    onsuccess(commandResponse.data);
                } else {
                    kony.print("in error response is " + commandResponse.data);
                }
            };
            scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.showStatementsList", context, completionCallback));
        },
        fetchStatementsDoc: function(onsuccess, docId,docdesc) {
            this.updateProgressBarState(true);
            var scopeObj = this;
            var context = {
                "docId": docId
            }
            var completionCallback = function(commandResponse) {
                if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                    kony.print("in success response is " + commandResponse.data);
                    onsuccess(commandResponse.data,docdesc);
                } else {
                    windowsOpenSt.close();
                    kony.print("in error response is " + commandResponse.data);
                }
            };
            scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getSelectedStatement", context, completionCallback));
        },
       showFormatEstatements : function () {
        var scopeObj = this;
		scopeObj.activeForm = scopeObj.viewFormsList.frmAccountsDetails;
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: scopeObj.serviceViewModels[scopeObj.activeForm]
        });
        scopeObj.loadAccountDetailsStatComponents();   
        var context = {
            "action": "Navigation To eStatements",
            "data": ""
        };
        this.updateProgressBarState(true);
        this.presentAccountDetails(context);
      },
      loadAccountDetailsStatComponents : function (onAccountSelectionAction) {
			var scopeObj = this;
			var howToShowHamburgerMenu = function (sideMenuViewModel) {
				scopeObj.presentAccountDetails({
					sideMenuSt: sideMenuViewModel
				});
			};
			scopeObj.SideMenu.init(howToShowHamburgerMenu);

			var presentTopBar = function (topBarViewModel) {
				scopeObj.presentAccountDetails({
					topBar: topBarViewModel
				});
			};
			scopeObj.TopBar.init(presentTopBar);
         },
		stopCheckDetails: function(dataInputs){
		  var self = this;
		  function stopCheckCompletionCallback (response) {
			kony.print("stopCheckCompletionCallback res-> "+JSON.stringify(response));
			if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
              if(response.data[0].success !== null && response.data[0].success != "null"){
                alert("Stop check request success");
              }else if(response.data[0].error){
                alert(response.data[0].error);
              }
			  //var checkViewModel = {};
			  //self.presentUserInterface("frmAccountsDetails",checkViewModel);
			}else {
			  errorCallBack(response.data);
			}
		  }
		  var commandObj = {
			accountNumber: dataInputs.accountNumber,
			productId: dataInputs.productId,
			checkNumber: dataInputs.checkNumber,
			productType: CommonUtilitiesOCCU.getAccountTypeKey(dataInputs.productType),
			checkNumberHighRange: dataInputs.checkNumberHighRange,
			payee: dataInputs.payee
		  };
		  kony.print("stopCheck inputparams : "+JSON.stringify(commandObj));
		  this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.stopCheck", commandObj, stopCheckCompletionCallback));
		},
      onShowFavouriteAccounts : function (accounts) {
        var scopeObj = this;
        scopeObj.viewModel.accountSummaryViewModel.toggleAccounts = function(){
            scopeObj.updateLoadingForCompletePage({
                isLoading: true,
                serviceViewModels: ['accountsSummary']
            });
            scopeObj.showAllAccounts();
        };
        //modified for OCCU project
        if(accounts.length > 0){
        var favaccounts = accounts.filter(scopeObj.isFavourite);
		if(favaccounts !== null && favaccounts.length >0){
         scopeObj.viewModel.accountSummaryViewModel.showFavouriteText = true;
         scopeObj.viewModel.accountSummaryViewModel.toggleAccountsText = kony.i18n.getLocalizedString("i18n.accounts.showAllAccounts");
         scopeObj.viewModel.accountSummaryViewModel.toggleFavAccountsText =  kony.i18n.getLocalizedString("i18n.accounts.MyFavoriteAccounts");
         scopeObj.refreshData = scopeObj.showFavouriteAccounts.bind(scopeObj);
         scopeObj.showAccounts(accounts.filter(scopeObj.isFavourite));
		} else {
         scopeObj.viewModel.accountSummaryViewModel.showFavouriteText = false;
         scopeObj.viewModel.accountSummaryViewModel.toggleAccountsText = "";
         scopeObj.viewModel.accountSummaryViewModel.toggleFavAccountsText =  kony.i18n.getLocalizedString("i18n.accounts.MyFavoriteAccounts");
         scopeObj.refreshData = scopeObj.showFavouriteAccounts.bind(scopeObj);
         scopeObj.onShowAllAccounts(accounts);
		}
        }else{
          this.updateProgressBarState(false);
        }
    },
    onShowAllAccounts : function (accounts) {
        var scopeObj = this;
        scopeObj.viewModel.accountSummaryViewModel.toggleAccounts = function(){
            scopeObj.updateLoadingForCompletePage({
                isLoading : true,
                serviceViewModels : ['accountsSummary']
            });
            scopeObj.showFavouriteAccounts(accounts);
        };
        //modified for OCCU project
        scopeObj.viewModel.accountSummaryViewModel.toggleAccountsText = kony.i18n.getLocalizedString("i18n.accounts.showFavouriteAccounts");
        scopeObj.viewModel.accountSummaryViewModel.toggleFavAccountsText =  kony.i18n.getLocalizedString("i18n.accounts.AllAccounts");  
        scopeObj.refreshData = scopeObj.showAllAccounts.bind(scopeObj);
        scopeObj.showAccounts(accounts);
    },
	fetchUserProfile: function(onSuccess, onError) {
        var scopeObj = this;
        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(response.data);
            } else {
                onError(response.data);
            }
        }
        //scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getUserProfile", {}, completionCallback));
		completionCallback({"data":{"userfirstname":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,"lastlogintime":"01/01/2018"},"status":"100"});
    },
    getAccountDeatilsAllAccountsViewModel:function (accounts) {
            var scopeObj = this;
            var createAccountViewModal = function (account) {
                return {
                    //construct the view modal for the account 
                    accountName: CommonUtilities.getAccountName(account),
                    accountNumber: scopeObj.displayAccountNumber(account.accountID),
                    type: account.accountType,
                    accountOwner: account.accountHolder +" | "+ account.accountID,    
                    accountshareID : account.accountName+" | "+account.accountType+"-"+account.shareId,
                    onAccountSelection: function () {
                        scopeObj.updateLoadingForCompletePage({
                            isLoading : true,
                            serviceViewModels : scopeObj.serviceViewModels[scopeObj.activeForm]
                        });
                        return scopeObj.showAccountDetails(account);
                    }
                };
            };
            return accounts.map(createAccountViewModal);
    }
  }
  
});