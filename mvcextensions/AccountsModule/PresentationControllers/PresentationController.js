define(['formatDate', 'ErrHandler', 'DateUtils', 'CommonUtilities', 'OLBConstants'], function(formatDate, ErrHandler, DateUtils, CommonUtilities, OLBConstants) {

    function Accounts_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);

        this.initializePresentationController();
    }
    inheritsFrom(Accounts_PresentationController, kony.mvc.Presentation.BasePresenter);

    /* **************************************************************************
     * Private methods for Account module , can be moveed to Common Utilities.
     * **************************************************************************/

    /**
     * Method to handled Functionality. Coming Soon feature.
     * @param {object} data 
     */
    var _todoAction = function(data) {
        //alert("Coming Soon!!! Functionality is under progress , Apologies for the inconvenience caused.
    };

    /********************************************************************************************************** */

    /**
     * initializePresentationController - Method to intialize Presentation controller data , called by constructor
     * @member of {Accounts_PresentationController}
     * @param {Object} dataInputs - dataInputs to configure Presentation controller. (optional) - useful when we need customize.
     * @returns {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.initializePresentationController = function(dataInputs) {
        var scopeObj = this;
        scopeObj.viewModel = {};
        scopeObj.refreshData = null; //Method to toggle between Favorite and All accounts.
        scopeObj.viewModel.welcomeBannerViewModel = null; //Welcome banner view model.
        scopeObj.viewModel.accountSummaryViewModel = { //account summary view model
            toggleAccounts: null,
            toggleAccountsText: null,
            accounts: [],
        };
        scopeObj.viewModel.selectedAccount = null;
        scopeObj.viewModel.accountDetailsViewModel = null;
        scopeObj.viewModel.transactionsViewModel = null;
        scopeObj.viewModel.scheduledTransactionsViewModel = null;
        scopeObj.activeForm = null;
        scopeObj.viewFormsList = {
            frmAccountsLanding: 'frmAccountsLanding',
            frmAccountsDetails: 'frmAccountsDetails',
            frmScheduledTransactions: 'frmScheduledTransactions',
            frmPrintTransaction: 'frmPrintTransaction',
            frmPrintTransfer: 'frmPrintTransfer'
        };
        //Configuration to maintain sort and pagination
        scopeObj.transactonsConfig = null;
        scopeObj.scheduleTransactonsConfig = null;

        //Handling loading progreebar
        scopeObj.totalServiceModels = 0; //flag to wait for models from service
        scopeObj.tmpSerViewModels = []; //array to store models from service
        scopeObj.serverErrorVieModel = 'serverError';
        scopeObj.serviceViewModels = {
            "frmAccountsLanding": ['accountsSummary', 'welcomeBanner'],
            "frmAccountsDetails": ['accountList', 'accountDetails', 'transactionDetails'],
            "frmScheduledTransactions": ['accountList', 'transactionDetails']
        };
    };


    /**
     * isValidAction: Method to validate action w.r.t given data inputs.
     * @member of {Accounts_PresentationController}
     * @param {string} actionName , action name
     * @param {object} dataItem , data inputs to validated action.
     * @return {boolean} isValid, true/false
     * @throws {}
     */
    Accounts_PresentationController.prototype.isValidAction = function(actionName, dataItem) {
        var isValid = false;
        var principalBalance, currentAmountDue, eStatementsFlag;

        switch (actionName) {
            case OLBConstants.ACTION.PAY_A_BILL:
                isValid = CommonUtilities.getConfiguration("isBillPayEnabled") === "true";
                break;
            case OLBConstants.ACTION.MAKE_A_TRANSFER:
            case OLBConstants.ACTION.TRANSFER_MONEY:
                isValid = ((CommonUtilities.getConfiguration("isKonyBankAccountsTransfer")) === "true" || (CommonUtilities.getConfiguration("isOtherKonyAccountsTransfer") === "true") || (CommonUtilities.getConfiguration("isOtherBankAccountsTransfer") === "true") || (CommonUtilities.getConfiguration("isInternationalAccountsTransfer") === "true") || (CommonUtilities.getConfiguration("isInternationalWireTransferEnabled") === "true") || (CommonUtilities.getConfiguration("isDomesticWireTransferEnabled") === "true"));
                break;
            case OLBConstants.ACTION.PAY_A_PERSON_OR_SEND_MONEY:
                isValid = CommonUtilities.getConfiguration("ispayAPersonEnabled") === "true";
                break;
            case OLBConstants.ACTION.PAY_DUE_AMOUNT:
                principalBalance = Number(dataItem.principalBalance) ? Number(dataItem.principalBalance) : 0;
                currentAmountDue = Number(dataItem.currentAmountDue) ? Number(dataItem.currentAmountDue) : 0;
                isValid = CommonUtilities.getConfiguration("loanPaymentEnabled") === "true" && (principalBalance > 0 && currentAmountDue > 0);
                break;
            case OLBConstants.ACTION.PAYOFF_LOAN:
                principalBalance = Number(dataItem.principalBalance) ? Number(dataItem.principalBalance) : 0;
                isValid = CommonUtilities.getConfiguration("payOffLoanPaymentEnabled") === "true" && (principalBalance > 0);
                break;
            case OLBConstants.ACTION.STOPCHECKS_PAYMENT:
                isValid = CommonUtilities.getConfiguration("enableStopPayments") === "true";
                break;
            default:

                isValid = true;
        }
        return isValid;
    };

    /**
     * getValidActions: Get valid action w.r.t isValidation method and dataInput
     * @member of {Accounts_PresentationController}
     * @param {Array} actions , Actions list
     * @param {function} isValidAction , Method to validated Action
     * @param {object} dataInput , data inputs to validated action.
     * @return {Array} actions , valid actins
     * @throws {}
     */
    Accounts_PresentationController.prototype.getValidActions = function(actions, isValidAction, dataInput) {
        return actions.filter(function(action) {
            return isValidAction(action, dataInput);
        });
    };

    /**
     * getQuickActions : Method will return all available actions in Accounts.
     * @member of {Accounts_PresentationController}
     * @param {Object} dataInput , data inputs for actions to configure.
     * @return {Array} , All actions.
     * @throws {}
     */
    Accounts_PresentationController.prototype.getQuickActions = function(dataInput) {
        var onCancel = dataInput.onCancel || function() {
            CommonUtilities.ErrorHandler.onError("Cancel Event not provided");
        };

        var quickActions = [{
                actionName: OLBConstants.ACTION.SCHEDULED_TRANSACTIONS,
                displayName: dataInput.scheduledDisplayName || kony.i18n.getLocalizedString("i18n.accounts.scheduledTransactions"),
                action: function(account) {
                    if (dataInput.showScheduledTransactionsForm) {
                        dataInput.showScheduledTransactionsForm(account);
                    }
                }
            },
            {
                actionName: OLBConstants.ACTION.MAKE_A_TRANSFER, //MAKE A TRANSFER
                displayName: dataInput.makeATransferDisplayName || kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"),
                action: function(account) {
                    //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                    var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    transferModule.presentationController.showTransferScreen({
                        accountObject: account,
                        onCancelCreateTransfer: onCancel
                    });
                }
            },
            {
                actionName: OLBConstants.ACTION.TRANSFER_MONEY, //MAKE A TRANSFER
                displayName: dataInput.tranferMoneyDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.TransferMoney"),
                action: function(account) {
                    //Function call to  open tranfers page with parameter - account obj to be tranferred from.
                    var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    transferModule.presentationController.showTransferScreen({
                        accountObject: account,
                        onCancelCreateTransfer: onCancel
                    });
                }
            },
            {
                actionName: OLBConstants.ACTION.PAY_A_BILL,
                displayName: dataInput.payABillDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payABillFrom"),
                action: function(account) {
                    //Function call to open bill pay screen
                    var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    billPayModule.presentationController.showBillPayData("quickAction", {
                        "fromAccountNumber": account.accountID,
                        "show": 'PayABill',
                        "onCancel": onCancel
                    });
                }
            },
            {
                actionName: OLBConstants.ACTION.PAY_A_PERSON_OR_SEND_MONEY,
                displayName: dataInput.sendMoneyDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payAPersonFrom"),
                action: function(account) {
                    var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    var dataItem = account;
                    dataItem.show = "SendOrRequest";
                    dataItem.onCancel = onCancel;
                    p2pModule.presentationController.showPayAPerson(dataItem);
                }
            },
            {
                actionName: OLBConstants.ACTION.PAY_DUE_AMOUNT,
                displayName: dataInput.payDueAmountDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payDueAmount"),
                action: function(account) {
                    account.currentAmountDue = CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue);
                    account.currentAmountDue = account.currentAmountDue.slice(1);
                    var data = {
                        "accounts": account
                    };
                    var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                    loanModule.presentationController.navigateToLoanDue(data);
                }
            },
            {
                actionName: OLBConstants.ACTION.PAYOFF_LOAN,
                displayName: dataInput.payoffLoanDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan"),
                action: function(account) {
                    account.principalBalance = CommonUtilities.formatCurrencyWithCommas(account.principalBalance);
                    account.payOffCharge = CommonUtilities.formatCurrencyWithCommas(account.payOffCharge);
                    var data = {
                        "accounts": account
                    };
                    var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
                    loanModule.presentationController.navigateToLoanPay(data);
                }
            },
            {
                actionName: OLBConstants.ACTION.STOPCHECKS_PAYMENT,
                displayName: dataInput.stopCheckPaymentDisplayName || kony.i18n.getLocalizedString("i18n.StopPayments.stopCheckPaymentCamelcase"),
                action: function(account) {

                    var stopPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
                    stopPaymentsModule.presentationController.showStopPayments({
                        onCancel: onCancel,
                        accountID: account.accountID,
                        "show": OLBConstants.ACTION.SHOW_STOPCHECKS_FORM
                    });
                }
            },
            {
                actionName: OLBConstants.ACTION.VIEW_BILL,
                displayName: dataInput.viewBillDisplayName || kony.i18n.getLocalizedString("i18n.accounts.viewBill"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.VIEW_STATEMENTS,
                displayName: dataInput.viewStatementsDisplayName || kony.i18n.getLocalizedString("i18n.accounts.viewStatments_Small"),
                action: function(account) {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showFormatEstatements(account.accountID);

                }
            },
            {
                actionName: OLBConstants.ACTION.UPDATE_ACCOUNT_SETTINGS,
                displayName: dataInput.updateAccountSettingsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateAccountSettings"),
                action: function(account) {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.showPreferredAccounts();
                }
            },
            {
                actionName: OLBConstants.ACTION.ORDER_CHECKS,
                displayName: dataInput.orderChecksDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.OrderChecks"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.REQUEST_OR_REPLACE_CARD,
                displayName: dataInput.requestOrReplaceCardDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.REPORT_LOST_OR_STOLEN,
                displayName: dataInput.reportLostOrStolenDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.reportLostOrStolen"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.SETUP_NEW_PIN,
                displayName: dataInput.setupNewPinDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.setupNewPIN"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.LOCK_OR_DECACTICATE_CARD,
                displayName: dataInput.lockOrDeactivateCardDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.LockCardOrDeactivateCard_Temporary"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.TRAVEL_NOTIFICATION,
                displayName: dataInput.travelNotificationDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.TravelNotification"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.GET_ASSISTANCE,
                displayName: dataInput.getAssitanceDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.getAssistance"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.ECHECK_OR_ROUTING_DETAILS,
                displayName: dataInput.echeckOrRoutingDetailsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.eCheckRoutingDetails"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.REWARDS_POINTS,
                displayName: dataInput.rewardsPointsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.rewardsPoints"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.ACCOUNT_SETTINGS,
                displayName: dataInput.accountSettingsDisplayName || kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateSettingAndPreferences"),
                action: function(account) {
                    _todoAction();
                }
            },
            {
                actionName: OLBConstants.ACTION.REMOVE_ACCOUNT,
                displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.removeAccount"),
                action: function(account) {
                    var accountData = account;
                    var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountModule.presentationController.showAccountDeletionPopUp(accountData);

                },
            },
            {
                actionName: OLBConstants.ACTION.ACCOUNT_PREFERENCES,
                displayName: kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountPreferences"),
                action: function(account) {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.showPreferredAccounts();
                  },
            },
            {
                actionName: OLBConstants.ACTION.EDIT_ACCOUNT,
                displayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.editAccount"),
                action: function(account) {
                    var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
                    profileModule.presentationController.initializeUserProfileClass();
                    profileModule.presentationController.showEditExternalAccount(account);
                },
            },
        ];

        return quickActions;
    };

    /**
     * showAccountsDashboard : Entry point for Account(DashBoard) form
     * @member of {Accounts_PresentationController}
     * @param {}
     * @returns {}
     * @throws {} 
     */
    Accounts_PresentationController.prototype.showAccountsDashboard = function() {
        var scopeObj = this;
        scopeObj.activeForm = scopeObj.viewFormsList.frmAccountsLanding;
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: scopeObj.serviceViewModels[scopeObj.activeForm]
        });
        scopeObj.showWelcomeBanner();
        console.log("Temp fix for MF issue with async calls.");
        console.log("Temp fix for MF issue with async calls.");
        scopeObj.showAllAccounts();
        console.log("Temp fix for MF issue with async calls.");
        console.log("Temp fix for MF issue with async calls.");
        scopeObj.loadAccountLandingComponents({}); //load other components.
    };

    /**
     * updateLoadingForCompletePage : Method to handle loading progress bar w.r.t expected view models.
     * @member of {Accounts_PresentationController}
     * @param {Object}  viewModel ,  form view model
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.updateLoadingForCompletePage = function(viewModel) {
        var scopeObj = this;
        if (viewModel.isLoading === true) {
            scopeObj.tmpSerViewModels = viewModel.serviceViewModels;
            scopeObj.totalServiceModels = 0;
            scopeObj.updateProgressBarState(true);
        } else {
            if (scopeObj.isServiceViewModel(viewModel, scopeObj.tmpSerViewModels)) {
                scopeObj.totalServiceModels++;
            }
            if (scopeObj.totalServiceModels === scopeObj.tmpSerViewModels.length) {
                scopeObj.updateProgressBarState(false);
                scopeObj.totalServiceModels = 0;
            }
        }
    };

    /**
     * isServiceViewModel : Method to validate service view models.
     * @member of {Accounts_PresentationController}
     * @param {Object}  viewModel ,  form view model
     * @param {Array}  serviceViewModels ,  expected serice view models.
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.isServiceViewModel = function(viewModel, serviceViewModels) {
        var scopeObj = this;
        return Object.keys(viewModel).filter(function(key) {
            return serviceViewModels.indexOf(key) >= 0 || key === scopeObj.serverErrorVieModel; //include server error as expected
        }).length > 0;
    };

    /**
     * formatCurrency : Format currecy for account module
     * @member of {Accounts_PresentationController}
     * @param {Number} amount, currency value (12.3)
     * @return : formatted currency ($12.30)
     * @throws {}
     */
    Accounts_PresentationController.prototype.formatCurrency = function(amount) {
        return CommonUtilities.formatCurrencyWithCommas(amount);
    };

    /**
     * loadAccountLandingComponents : Sets up the dashboard components.
     * @member of {Accounts_PresentationController}
     * @param {JSON object} config
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.loadAccountLandingComponents = function() {
        var self = this;

        var howToShowHamburgerMenu = function(sideMenuViewModel) {
            self.asyncPresentAccountsDashboard({
                sideMenu: sideMenuViewModel
            });
        };
        self.SideMenu.init(howToShowHamburgerMenu.bind(self));

        var presentTopBar = function(topBarViewModel) {
            self.asyncPresentAccountsDashboard({
                topBar: topBarViewModel
            });
        };
        self.TopBar.init(presentTopBar.bind(self));
        
        self.asyncPresentAccountsDashboard({
            "secondaryDetails":  {
                show : true
            }
        });
        
        //Outage Message.
        self.showOutageMessage();


        var alertsMsgsModule;
        if (CommonUtilities.getConfiguration("enableAlertsIcon") === "true") {
            alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");

            function unreadMsgsOrNotificationsCountCompletionCallback(response) {
                var viewModel = {
                    count: response.totalUnreadCount
                };
                self.asyncPresentAccountsDashboard({
                    "unreadCount": viewModel
                });
            }
            alertsMsgsModule.presentationController.getUnreadMessagesOrNotificationsCount(unreadMsgsOrNotificationsCountCompletionCallback);
        }

        console.log("checking for bill pay eligibility");
        console.log("checking for bill pay eligibility");

        if (CommonUtilities.getConfiguration("isBillPayEnabled") === "true") {
            if (!kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility) {
                var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                billPayModule.presentationController.checkBillPayEligibilityForUser();
            }
        }

        console.log("checking for p2p eligibility");
        console.log("checking for p2p eligibility");

        var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
        p2pModule.presentationController.checkP2PEligibilityForUser();

        console.log("fetching dashboard widget transactions");
        console.log("fetching dashboard widget transactions");

        function fetchScheduledTransactionsSuccessCallBack(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.asyncPresentAccountsDashboard({
                    "UpcomingTransactionsWidget": response.data
                });
            } else {
                self.asyncPresentAccountsDashboard({
                    "UpcomingTransactionsWidget": [],
                    "serverError": response.data && response.data.errmsg ? response.data.errmsg : "Server Error"
                });
            }
        }

        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getUserScheduledTransactions", {}, fetchScheduledTransactionsSuccessCallBack.bind(this)));

        console.log("fetching messages for dashboard");
        console.log("fetching messages for dashboard");

        function fetchMessagesSuccessCallBack(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data && response.data.customerrequests_view) {
                self.asyncPresentAccountsDashboard({
                    "messagesWidget": response.data.customerrequests_view.slice(0, CommonUtilities.getConfiguration("getDashboardMessageCount"))
                });
            } else {
                self.asyncPresentAccountsDashboard({
                    "messagesWidget": [],
                    "serverError": response.data && response.data.errmsg ? response.data.errmsg : "Server Error"
                });
            }
        }

        console.log("fetching pfmMontlyDashBoard for dashboard");
        console.log("fetching pfmMontlyDashBoard for dashboard");

        function fetchPFMSuccessCallBack(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.asyncPresentAccountsDashboard({
                    "PFMMonthlyWidget": self.formatPFMDonutChartData(response.data),
                });
            } else {
                self.asyncPresentAccountsDashboard({
                    "PFMMonthlyWidget": [],
                    "serverError": response.data && response.data.errmsg ? response.data.errmsg : "Server Error"
                });
            }
        }


        alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
        var param = {
            username: kony.mvc.MDAApplication.getSharedInstance().appContext.username,
            softDeleteFlag: "false"
        };
        alertsMsgsModule.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.getRequests", param, fetchMessagesSuccessCallBack.bind(this)));


        if (CommonUtilities.getConfiguration("isPFMWidgetEnabled") === "false") {
            this.asyncPresentAccountsDashboard({
                "PFMDisabled": true
            });
        } else {
            var pfmModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PersonalFinanceManagementModule");
            var date = new Date();
            var requestObj = {
                'monthId': date.getMonth(),
                'year': date.getFullYear()
            };
            pfmModule.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.getMonthlySpending', requestObj, fetchPFMSuccessCallBack.bind(this)));
        }
    };

    /**
     * formatPFMDonutChartData : used to format the chartData and return TotalCash
     * @member of {Accounts_PresentationController}
     * @param {monthlyData} 
     * @return {totalcashSpent,pfmChartData}
     * @throws {}
     */
    Accounts_PresentationController.prototype.formatPFMDonutChartData = function(monthlyData) {
        var monthlyDonutChartData = {};

        function addRequireDonutChartFields(month) {
            month.label = month.categoryName;
            month.Value = Number(month.cashSpent);
            month.colorCode = OLBConstants.CATEGORIES[month.categoryName];
            return month;
        }
        var pfmData = monthlyData.map(addRequireDonutChartFields);
        if (monthlyData.length !== 0)
            monthlyDonutChartData.totalCashSpent = CommonUtilities.formatCurrencyWithCommas(monthlyData[0].totalCashSpent);
        monthlyDonutChartData.pfmChartData = pfmData;
        return monthlyDonutChartData;
    };


    /**
     * presentAccountsDashboard : Update account dashboard
     * @member of {Accounts_PresentationController}
     * @param {JSON  Object} viewModel, view model for account dashboard
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.presentAccountsDashboard = function(viewModel) {
        var scopeObj = this;
        scopeObj.presentUserInterface(this.viewFormsList.frmAccountsLanding, viewModel);
        scopeObj.updateLoadingForCompletePage(viewModel);
    };

    /**
     * presentAccountsDashboard : Update account dashboard
     * @member of {Accounts_PresentationController}
     * @param {JSON  Object} viewModel, view model for account dashboard
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.asyncPresentAccountsDashboard = function(viewModel) {
        var scopeObj = this;
        scopeObj.asyncUpdateUI(this.viewFormsList.frmAccountsLanding, viewModel);
        scopeObj.updateLoadingForCompletePage(viewModel);
    };

    /**
     * navigateToAccountDetails: Navigate to Accounts Module and present the details of given account
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account 
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.navigateToAccountDetails = function(account) {
        this.showAccountDetails(account);
    };

    /**
     * newMessage : Navigate to Messages Module and loads the new Message screen 
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @returns {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.newMessage = function() {
        //Todo Need ti send Cancel function call
        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
        alertsMsgsModule.presentationController.showAlertsPage("AccountsLanding", {
            "show": "CreateNewMessage"
        });
    };

    /**
     * showSelectedMessage : Navigate to Messages Module and show selected message
     * @member of {Accounts_PresentationController}
     * @param {object} message, message object 
     * @returns {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.showSelectedMessage = function(message) {
        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
        alertsMsgsModule.presentationController.showAlertsPage("AccountsLanding", {
            "show": "ShowSelectedMessage",
            "selectedRequestId": message.id
        });
    };

    /**
     * getAction : Method to get Action object from Action Array
     * @member of {Accounts_PresentationController}
     * @param {String} Actions , Action Object Array
     * @param {String} actionName, action name
     * @param {Object}     account , binded data object for action -  RBObjects.Account
     * @return {Object} matchedAction, return matched action
     * @throws {}
     */
    Accounts_PresentationController.prototype.getAction = function(Actions, actionName, account) {
        var actionItem, matchedAction;
        for (var i = 0; i < Actions.length; i++) {
            actionItem = Actions[i];
            if (actionItem.actionName === actionName) {
                matchedAction = {
                    actionName: actionItem.actionName,
                    displayName: actionItem.displayName,
                    action: actionItem.action.bind(null, account)
                };
                break;
            }
        }
        if (!matchedAction) {
            CommonUtilities.ErrorHandler.onError("Action :" + actionName + " is not found, please validate with Contextial actions list.");
            return false;
        }
        return matchedAction;
    };

    /**
     * fetchQuickActions: Fetch Quick actions list
     * @member of {Accounts_PresentationController}
     * @param {object} account , account object - RBObjects.Account
     * @param {function} success , success callback method.
     * @param {function} error , error callback method.
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchQuickActions = function(account, success, error) {
        //Quick actions Configuration.
        if (account.isExternalAccount) {
            var quickActionsConfig = OLBConstants.CONFIG.EXTERNAL_ACCOUNT_QUICK_ACTIONS;
            if (quickActionsConfig) {
                success({
                    account: account,
                    actions: quickActionsConfig
                });
            }
        } else {
            var quickActionsConfig = OLBConstants.CONFIG.ACCOUNTS_QUICK_ACTIONS;
            if (quickActionsConfig[account.accountType]) {
                success({
                    account: account,
                    actions: quickActionsConfig[account.accountType]
                });
            } else {
                error("Actions are not found for account type : " + account.accountType);
            }
        }

    };

    /*********************************************************************************************************************************
     * Quick Actions - Contextual Actions
     *********************************************************************************************************************************/

    /**
     * onQuickActionsMenu: Method to handle show Quick actions menu
     * @member of {Accounts_PresentationController}
     * @param {object} account , account object 
     * @returns {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.onQuickActionsMenu = function(account) {
        var scopeObj = this;
        scopeObj.fetchQuickActions(
            account,
            scopeObj.onFetchQuickActionsSuccess.bind(scopeObj),
            CommonUtilities.ErrorHandler.onError
        );
    };

    /**
     * onQuickActionsMenu: Method to handle fetchQuickActions success 
     * @member of {Accounts_PresentationController}
     * @param {Object, Array} - account object, actions quick actions list
     * @returns {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.onFetchQuickActionsSuccess = function(response) {
        var scopeObj = this;
        var quickActionsViewModel = scopeObj.getQuickActionsViewModel(response.account, response.actions);
        scopeObj.asyncPresentAccountsDashboard({
            accountQuickActions: quickActionsViewModel
        });
    };

    /**
     * getQuickActionsViewModel: Method to create final Quick actions view model 
     * @member of {Accounts_PresentationController}
     * @param {Object} account , account object  - RBObjects.Account.
     * @param {Array} actions , final actions view model.
     * @param {Array} finalActionsViewModel , final actions view model.
     * @throws {}
     */
    Accounts_PresentationController.prototype.getQuickActionsViewModel = function(account, actions) {
        var scopeObj = this,
            accountType = account.accountType,
            validActions,
            finalActionsViewModel = [];

        /**
         * Method to handle Cancel quick action.
         */
        var onCancel = function() {
            scopeObj.showAgainAccountsDetails(account.accountID);
        };

        if (accountType) {
            if (actions.length) {
                validActions = scopeObj.getValidActions(actions, scopeObj.isValidAction, account);
                finalActionsViewModel = validActions.map(function(action) { //get action object.
                    var quickActions = scopeObj.getQuickActions({
                        onCancel: onCancel,
                        tranferMoneyDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.makeTransferFrom"),
                        payABillDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payABillFrom"),
                        sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payAPersonFrom")
                    });
                    return scopeObj.getAction(quickActions, action, account);
                });
            }
        }
        return finalActionsViewModel;
    };



    /*********************************************************************************************************************************
     * Account Summary 
     *********************************************************************************************************************************/

    /**
     * isFavourite: Method to return whether account is favourite or not 
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account , account object  - RBObjects.Account.
     * @returns {boolean} is this Account marked as Favourite?
     * @throws {}
     */
    Accounts_PresentationController.prototype.isFavourite = function(account) {
        return account.favouriteStatus && account.favouriteStatus === '1';
    };

    /**
     * showAccounts: Method  to create Account dashboard view model.
     * @member of {Accounts_PresentationController}
     * @param {Array} accounts - RBObjects.Account
     * @returns {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.showAccounts = function(accounts) {
        var scopeObj = this;
        scopeObj.viewModel.accountSummaryViewModel.accounts = accounts.map(scopeObj.createAccountSummaryViewModal.bind(scopeObj));
        scopeObj.presentAccountsDashboard({
            accountsSummary: scopeObj.viewModel.accountSummaryViewModel
        });
    };

    /**
     * showAllAccounts : Method to show all accounts 
     * @member of {Accounts_PresentationController}
     * @param {Array}   RBObjects.Account
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.showAllAccounts = function() {
        var scopeObj = this;
        scopeObj.fetchAccounts(scopeObj.onShowAllAccounts.bind(scopeObj), CommonUtilities.showServerDownScreen);
    };

    /**
     * onShowAllAccounts : Method to handle on successful fetch accounts from MF w.r.t. All accounts
     * @member of {Accounts_PresentationController}
     * @param {Array} accounts - RBObjects.Account
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.onShowAllAccounts = function(accounts) {
        var scopeObj = this;
		scopeObj.refreshData = scopeObj.showAllAccounts.bind(scopeObj);
        scopeObj.showAccounts(accounts);
    };

   

    /**
     * createAccountSummaryViewModal: converts domain object to viewModel
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account 
     * @returns {JSON} viewModel for Account
     * @throws {}
     */

    Accounts_PresentationController.prototype.createAccountSummaryViewModal = function(account) {
        var scopeObj = this;
        return {
            accountName: account.accountName,
            nickName: account.nickName,
            accountNumber: account.accountID,
            type: account.accountType,
            availableBalance: scopeObj.formatCurrency(account.availableBalance),
            currentBalance: scopeObj.formatCurrency(account.currentBalance),
            outstandingBalance: scopeObj.formatCurrency(account.outstandingBalance),
            isFavourite: scopeObj.isFavourite(account),
            bankLogo: account.bankLogo,
            availableBalanceUpdatedAt: account.availableBalanceUpdatedAt,
            isError: account.isError,
            bankName: account.bankName,
            isExternalAccount: account.isExternalAccount,
            userName: account.userName,
            bankId: account.bankId,
            toggleFavourite: function() {
                scopeObj.updateLoadingForCompletePage({
                    isLoading: true,
                    serviceViewModels: ['accountsSummary']
                });
                scopeObj.changeAccountFavouriteStatus(account, scopeObj.refreshData.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
            },
            openQuickActions: function() {
                scopeObj.onQuickActionsMenu(account);
            },
            onAccountSelection: function() {
                return scopeObj.navigateToAccountDetails(account);                               
            }
        };
    };

    /**
     * onServerError : Method to handle server errors.
     * @member of {Accounts_PresentationController}
     * @param {object} data - Service error object
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.onServerError = function(data) {
        var scopeObj = this;
        if (scopeObj.activeForm) {
            scopeObj.presentUserInterface(scopeObj.activeForm, {
                serverError: data
            });
            scopeObj.updateLoadingForCompletePage({
                serverError: data
            });
        }
    };

    /**
     * changeAccountFavouriteStatus:  toggles favourite status of given account
     * @member of {Accounts_PresentationController}
     * @param {Object} account , RBObjects.Account
     * @param {function} success , on success call back
     * @param {function} error , on error call back
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.changeAccountFavouriteStatus = function(account, success, error) {
        var scopeObj = this;
        var completionCallback = function(commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                success(commandResponse);
            } else {
                error('Could not change favourite status', commandResponse.data);
            }
        };
        if (account.isExternalAccount === true) {
            var favoriteStatus;
            if (account.favouriteStatus === "1") {
                favoriteStatus = "false";
            } else {
                favoriteStatus = "true";
            }

            var accountModel = {
                "Account_id": account.externalAccountId,
                "FavouriteStatus": favoriteStatus
            };
            scopeObj.businessController.execute(
                new kony.mvc.Business.Command("com.kony.accounts.updateExternalAccountFavouriteStatus", accountModel, completionCallback)
            );
        } else {
            scopeObj.businessController.execute(
                new kony.mvc.Business.Command("com.kony.accounts.updateFavouriteStatus", {
                    accountID: account.accountID
                }, completionCallback)
            );
        }
    };

    /**
     * fetchAccounts: fetches accounts using Command - 'com.kony.accounts.getAccounts'
     * @member of {Accounts_PresentationController}
     * @param {function} onSuccess , success callback
     * @param {function} onError , error callback
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchAccounts = function(onSuccess, onError) {
        var scopeObj = this;
        if (CommonUtilities.getConfiguration("isAggregatedAccountsEnabled")) {
			var allAccounts = {internal:null,external:null};
			var finalAccounts =[];
			var serviceCounts = 0;
			var completionCallbackInternal = function(commandResponse) {
				if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
					serviceCounts++;
					allAccounts.internal = commandResponse.data;
					if(serviceCounts === 2){
						finalAccounts = allAccounts.internal;
						allAccounts.external.forEach(function(externalAccount) {
							finalAccounts.push(externalAccount);
						});
						onSuccess(finalAccounts);
					}
                
				} else {
					onError(commandResponse.data);
            }
        };
		
		var completionCallbackExternal = function(commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
				serviceCounts++;
				var externalAccounts = scopeObj.processExternalAccountsData(commandResponse.data);
				allAccounts.external = externalAccounts;
				if(serviceCounts === 2){
					finalAccounts = allAccounts.internal;
						allAccounts.external.forEach(function(externalAccount) {
							finalAccounts.push(externalAccount);
						});
					onSuccess(finalAccounts);
				}
                
            } else {
                onError(commandResponse.data);
            }
        };
        scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, completionCallbackInternal));
		scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getExternalAccountsFromDB", {
                mainUser: kony.mvc.MDAApplication.getSharedInstance().appContext.username
            }, completionCallbackExternal));
		}
		else{
			var completionCallback = function(commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(commandResponse.data);
            } else {
                onError(commandResponse.data);
            }
        };
        scopeObj.businessController.execute(
            new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, completionCallback)
        );
		}
    };


    /*********************************************************************************************************************************
     * Account Welcome Banner 
     *********************************************************************************************************************************/


    /**
     * fetchUserProfile: Uses Module 'AuthModule' and command 'com.kony.auth.getUserProfile' to get user profile data
     * @param {Function<JSON>} onSuccess , success callback
     * @param {Function<JSON>} onError , error callback
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchUserProfile = function(onSuccess, onError) {
        var scopeObj = this;

        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(response.data);
            } else {
                onError(response.data);
            }
        }
        scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getUserProfile", {}, completionCallback));
    };

    /**
     * onFetchUserProfileSuccess:  Method to handle on successful fetch user profile from MF.
     * @member of {Accounts_PresentationController}
     * @param {JSON Object} data , MF response.
     * @return {}
     * @throws {}
     */

    Accounts_PresentationController.prototype.onFetchUserProfileSuccess = function(data) {
        var scopeObj = this;
        kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata = {
            "userName": data.userfirstname,
            "userPhoneNumber": data.phone,
            "userEmail": data.email,
            //For P2P Settings
            "userFirstName": data.userfirstname,
            "userLastName": data.userlastname,
            "default_to_account_p2p": data.default_to_account_p2p,
            "default_from_account_p2p": data.default_from_account_p2p
        }; //TODO : Hope it's not required for accounts, but need to confirm with team.
        kony.mvc.MDAApplication.getSharedInstance().appContext.userProfileObject = data; //TODO : Hope it's not required for accounts, but need to confirm with team.

        scopeObj.viewModel.welcomeBannerViewModel = scopeObj.createWelcomeBannerViewModel(data);
        scopeObj.presentAccountsDashboard({
            welcomeBanner: scopeObj.viewModel.welcomeBannerViewModel
        });
    };

    /**
     * showWelcomeBanner: Method to show Welcome Banner
     * @member of {Accounts_PresentationController}
     * @param {JSON Object} data , MF response.
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.showWelcomeBanner = function(response) {
        var scopeObj = this;
        if(kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData){
            scopeObj.onFetchUserProfileSuccess(kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData);
        }else{
            scopeObj.fetchUserProfile(scopeObj.onFetchUserProfileSuccess.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
        }
    };

    /**
     * createWelcomeBannerViewModel: Method to create Welcome Banner view model
     * @member of {Accounts_PresentationController}
     * @param {JSON Object} data, MF response.
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.createWelcomeBannerViewModel = function(data) {
        return {
            firstName: data.userfirstname,
            lastName: data.userlastname,
            lastLoggedInTime: new Date(Date.parse(data.lastlogintime)),
            userImage: data.userImageURL
        };
    };

    /*********************************************************************************************************************************
     * Account Details 
     *********************************************************************************************************************************/

    /**
     * Some Private methods specific to Account details and Schedule Transactions.
     */
    var outputDataCheck = function(dataString) {
        if (typeof dataString === 'string') {
            return dataString;
        } else {
            return null;
        }
    };
    var toInterestFormat = function(interestString) {
        if (interestString) {
            return interestString + '%';
        } else {
            return null;
        }
    };
    var displayInterest = function(interestString) {
        return outputDataCheck(toInterestFormat(interestString));
    };
    var accountNumberMask = function(accountNumber) {
        var stringAccNum = accountNumber;
        var isLast4Digits = function isLast4Digits(index) {
            return index > stringAccNum.length - 5;
        };
        return stringAccNum.split('').map(function(c, i) {
            return isLast4Digits(i) ? c : 'X';
        }).join('');
    };
    var displayAccountNumber = function(accountNumberString) {
        if (typeof accountNumberString === 'string' && accountNumberString.length > 0) {
            return accountNumberMask(accountNumberString);
        } else {
            return outputDataCheck(null);
        }
    };

    /**
     * presentAccountsDetails : show/update Account Details screen witn given view model
     * @member of {Accounts_PresentationController}
     * @param {JSON Object} viewModel, view model object for Account details
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.presentAccountDetails = function(viewModel) {
        this.presentUserInterface(this.viewFormsList.frmAccountsDetails, viewModel);
        this.updateLoadingForCompletePage(viewModel);
    };

    /**
     * Method to handle Progress bar
     * @member of {Accounts_PresentationController}
     * @param {boolean} isLoading , loading flag true.false
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.updateProgressBarState = function(isLoading) {
        var scopeObj = this;
        scopeObj.presentUserInterface(scopeObj.activeForm, {
            "progressBar": isLoading
        });
    };
    /**
     * showFormatEstatements - navigation to e-Statements 
     * @member of {AccountDetails_PresentationController}
     * @param {String} accountID - accountID of account clicked
     * @param {}
     * @returns {}
     * @throws {} 
     **/
    Accounts_PresentationController.prototype.showFormatEstatements = function(accountID) {
        var context = {
            "action": "Navigation To eStatements",
            "data": accountID
        };
        this.updateProgressBarState(true);
        this.presentAccountDetails(context);
    };
    Accounts_PresentationController.prototype.showAccountDeletionPopUp = function(account) {
      //give name to object instead of context
      var context = {
        "ExternalAccountToRemove":{
          "action": OLBConstants.REMOVE_ACCOUNT,
          "data": account              
        }

        };
        this.presentAccountsDashboard(context);
    };

    /**
     * showDownloadStatementScreen : Method to show download screen. fetch showDownloadStatement and update Account details
     * @member of {Accounts_PresentationController}
     * @param {Number} accountID - RBObjects.Account.accountID 
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.showDownloadStatementScreen = function(accountID, year, month, format) {
        var self = this;
        var context = {
            "accountID": accountID,
            "year": year,
            "format": format,
            "StatementMonth": month
        };

        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                var data = response.data[0].StatementLink;
                self.presentAccountDetails({
                    "showDownloadStatement": data
                });
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.showDownloadStatementScreen", context, completionCallback));

    };

    /**
     * showAccountDetails : Entry Point for Account Details module - load Account detail components
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account 
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.showAccountDetails = function(account) {
        var scopeObj = this;

        scopeObj.activeForm = scopeObj.viewFormsList.frmAccountsDetails;
        scopeObj.viewModel.selectedAccount = account;
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: scopeObj.serviceViewModels[scopeObj.activeForm]
        });
        scopeObj.loadAccountDetailsComponents();
        console.log("Temp fix for MF issue with async calls.");
        console.log("Temp fix for MF issue with async calls.");
        scopeObj.loadAccountDeatilsAllAccounts();
        scopeObj.Transactions = scopeObj.getAccountDetailsTransactionsViewModel(account);
        scopeObj.viewModel.accountDetailsViewModel = scopeObj.getAccountDetailsViewModel(account);
        scopeObj.presentAccountDetails({
            accountDetails: scopeObj.viewModel.accountDetailsViewModel
        });
    };

    /**
     * loadAccountDetailsComponents : load Account details components topbar, sidemenu etc.
     * @member of {Accounts_PresentationController}
     * @param {}
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.loadAccountDetailsComponents = function() {
        var scopeObj = this;
        var howToShowHamburgerMenu = function(sideMenuViewModel) {
            scopeObj.presentAccountDetails({
                sideMenu: sideMenuViewModel
            });
        };
        scopeObj.SideMenu.init(howToShowHamburgerMenu);

        var presentTopBar = function(topBarViewModel) {
            scopeObj.presentAccountDetails({
                topBar: topBarViewModel
            });
        };
        scopeObj.TopBar.init(presentTopBar);
    };

    /**
     * loadAccountDeatilsAllAccounts : Method load all accounts for Account details.
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.loadAccountDeatilsAllAccounts = function() {
        var scopeObj = this;
        scopeObj.fetchAccounts(scopeObj.onAccountDeatilsAllAccounts.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
    };

    /**
     * onAccountDeatilsAllAccounts : Method to handle on successful fetch accounts from MF w.r.t Accounts details.
     * @member of {Accounts_PresentationController}
     * @param {Array} accounts - RBObjects.Account , all accounts
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.onAccountDeatilsAllAccounts = function(accounts) {
        var scopeObj = this;
        var accountList = scopeObj.getAccountDeatilsAllAccountsViewModel(accounts);

        scopeObj.presentAccountDetails({
            accountList: accountList
        });
    };

    /**
     * getAccountDeatilsAllAccountsViewModel : Method to return All accounts view model for Account details.
     * @member of {Accounts_PresentationController}
     * @param {Array} accounts - RBObjects.Account , all accounts
     * @returns {Array} accounts -  All accounts view model for Account list in Account details.
     * @throws {}
     */
    Accounts_PresentationController.prototype.getAccountDeatilsAllAccountsViewModel = function(accounts) {
        var scopeObj = this;
        var createAccountViewModal = function(account) {
            return {
                //construct the view modal for the account  
                accountName: CommonUtilities.getAccountName(account),
                accountNumber: displayAccountNumber(account.accountID),
                type: account.accountType,
                onAccountSelection: function() {
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: scopeObj.serviceViewModels[scopeObj.activeForm]
                    });
                    return scopeObj.showAccountDetails(account);
                }
            };
        };
        return scopeObj.getInteralAccounts(accounts).map(createAccountViewModal);
    };

    /**
     * getInteralAccounts : Method to return All Internal accounts   - remove external accounts.
     * @member of {Accounts_PresentationController}
     * @param {Array} accounts - RBObjects.Account , all accounts
     * @returns {Array} accounts -  All Internal accounts   - remove external accounts.
     * @throws {}
     */
    Accounts_PresentationController.prototype.getInteralAccounts = function(accounts) {
        var internalAccounts = [];
        if(Array.isArray(accounts)) {
            internalAccounts = accounts.filter(function(account){
                return String(account.isExternalAccount) !== "true";
            });
        }
        return internalAccounts;
    };

    /**
     * getAccountDetailsViewModel : Method to return viewModel for displaying Account Details
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account
     * @returns {JSON} accountViewModel
     * @throws {}
     */
    Accounts_PresentationController.prototype.getAccountDetailsViewModel = function(account) {
        this.loadedAccountID = account.accountID
        var scopeObj = this;
        return {
            accountType: account.accountType,
            accountSummary: {
                availableBalance: scopeObj.formatCurrency(account.availableBalance),
                eStatementEnable: account.currentAmountDueeStatementEnable,
                currentBalance: scopeObj.formatCurrency(account.currentBalance),
                pendingDeposit: scopeObj.formatCurrency(account.pendingDeposit),
                pendingWithdrawals: scopeObj.formatCurrency(account.pendingWithdrawal),
                currentDueAmount: scopeObj.formatCurrency(account.currentAmountDue),
                creditLimit: scopeObj.formatCurrency(account.creditLimit),
                principalBalance: scopeObj.formatCurrency(account.principalBalance),
                principalAmount: scopeObj.formatCurrency(account.principalValue),
                payOffLoan: scopeObj.formatCurrency(account.principalBalance),
                maturityDate: CommonUtilities.formatDate(account.maturityDate),
                maturityOption: outputDataCheck(account.maturityOption),
                asOfDate: kony.i18n.getLocalizedString("i18n.accounts.AsOf") + CommonUtilities.formatDate(new Date().toISOString()),
                availableCredit: scopeObj.formatCurrency(account.availableCredit),
                interestEarned: scopeObj.formatCurrency(account.interestEarned),
                paymentTerm: outputDataCheck(account.paymentTerm)
            },
            balanceAndOtherDetails: {
                totalCredit: scopeObj.formatCurrency(account.totalCreditMonths),
                totalDebits: scopeObj.formatCurrency(account.totalDebitsMonth),
                bondInterestLastYear: displayInterest(account.bondInterestLastYear),
                dividendPaidLastYear: scopeObj.formatCurrency(account.dividendPaidLastYear),
                dividendRate: displayInterest(account.dividendRate),
                dividendPaidYTD: scopeObj.formatCurrency(account.dividendPaidYTD),
                lastDividendPaid: scopeObj.formatCurrency(account.lastDividendPaidAmount),
                paidOn: CommonUtilities.formatDate(account.lastPaymentDate),
                minimumDueAmount: scopeObj.formatCurrency(account.minimumDue),
                paymentDueDate: CommonUtilities.formatDate(account.dueDate),
                lastStatementBalance: scopeObj.formatCurrency(account.lastStatementBalance),
                lastPaymentDate: CommonUtilities.formatDate(account.lastPaymentDate),
                rewardsBalance: outputDataCheck(account.availableCredit),
                interestRate: displayInterest(account.interestRate),
                interestPaidYTd: scopeObj.formatCurrency(account.interestPaidYTD),
                interestPaidLastYear: scopeObj.formatCurrency(account.interestPaidLastYear),
                lastPaymentAmount: scopeObj.formatCurrency(account.lastPaymentAmount),
                originalAmount: scopeObj.formatCurrency(account.originalAmount),
                originalDate: CommonUtilities.formatDate(account.openingDate),
                payOffCharge: scopeObj.formatCurrency(account.payOffCharge),
                closingDate: CommonUtilities.formatDate(account.closingDate),
                loanClosingDate: account.closingDate,
                outstandingBalance: scopeObj.formatCurrency(account.outstandingBalance),
                dividendLastPaidAmount: scopeObj.formatCurrency(account.dividendLastPaidAmount),
                dividendLastPaidDate: CommonUtilities.formatDate(account.dividendLastPaidDate),
                previousYearsDividends: scopeObj.formatCurrency(account.previousYearsDividends),
                maturityAmount: scopeObj.formatCurrency(account.maturityAmount),
                regularPaymentAmount: scopeObj.formatCurrency(account.regularPaymentAmount)
            },
            accountInfo: {
                accountName: outputDataCheck(CommonUtilities.getAccountName(account)),
                accountNumber: displayAccountNumber(account.accountID),
                accountID: account.accountID,
                accountType: outputDataCheck(account.accountType),
                routingNumber: outputDataCheck(account.routingNumber),
                swiftCode: outputDataCheck(account.swiftCode),
                primaryAccountHolder: outputDataCheck(account.accountHolder),
                jointHolder: outputDataCheck(account.jointHolders),
                creditIssueDate: CommonUtilities.formatDate(account.openingDate),
                creditcardNumber: outputDataCheck(account.creditCardNumber),
            },
            rightSideActions: (function(account) {
                var onCancel = function() {
                    scopeObj.showAgainAccountsDetails(account.accountID);
                };
                //Right side action object array
                var RightSideActions = scopeObj.getQuickActions({
                    onCancel: onCancel,
                    showScheduledTransactionsForm: scopeObj.showScheduledTransactionsForm.bind(scopeObj),
                    payABillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payABill"),
                    makeATransferDisplayName: kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"),
                    payDueAmountDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payDueAmount"),
                    payoffLoanDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan_Caps"),
                    viewStatementsDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewStatements"),
                    viewBillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewBill"),
                    updateAccountSettingsDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.updateAccountSettings"),
                    stopCheckPaymentDisplayName: kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS")
                });
                var accountType = account.accountType,
                    validActions,
                    finalActions = [];
                if (accountType) {
                    var actions = scopeObj.fetchRightSideActions(account);
                    if (actions.length) {
                        validActions = scopeObj.getValidActions(actions, scopeObj.isValidAction, account);
                        finalActions = validActions.map(function(action) { //get action object.
                            return scopeObj.getAction(RightSideActions, action, account);
                        });
                    }
                }
                return finalActions;

            })(account),

            secondaryActions: (function(account) { //What else do you want dropdown
                var onCancel = function() {
                    scopeObj.showAgainAccountsDetails(account.accountID);
                };
                //Secondary action object array
                var SecondaryActions = scopeObj.getQuickActions({
                    onCancel: onCancel
                });
                var accountType = account.accountType,
                    validActions,
                    finalActions = [];
                if (accountType) {
                    var actions = scopeObj.fetchSecondayActions(account);
                    if (actions.length) {
                        validActions = scopeObj.getValidActions(actions, scopeObj.isValidAction, account);
                        finalActions = validActions.map(function(action) { //get action object.
                            return scopeObj.getAction(SecondaryActions, action, account);
                        });
                    }
                }
                return finalActions;
            })(account)
        };
    };


    /**
     * fetchRightSideActions : Method to fetch Rightside actions configurations.
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account
     * @returns {Array}  actions object array
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchRightSideActions = function(account) {
        //Right side actions Configuration.
        var RightSideActionsConfig = OLBConstants.CONFIG.ACCOUNTS_RIGHTSIDE_ACTIONS;

        return RightSideActionsConfig[account.accountType];
    };

    /**
     * fetchSecondayActions : Method to fetch Seconday actions configurations.
     * @member of {Accounts_PresentationController}
     * @param {Object} account , account object
     * @return {Array} seconady actions object array
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchSecondayActions = function(account) {
        //Secondary actions Configuration - What else do dropdown.
        var SecondaryActionsConfig = OLBConstants.CONFIG.ACCOUNTS_SECONDARY_ACTIONS;

        return SecondaryActionsConfig[account.accountType];
    };
    /**
     * showPrintPage - Method to navigate Print Transactions form and upadate active form.
     * @member of {Accounts_PresentationController}
     * @param {object} data , view model for Print form
     * @return {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.showPrintPage = function(data) {
        var scopeObj = this;
        //scopeObj.activeForm = scopeObj.viewFormsList.frmPrintTransaction;
        scopeObj.presentUserInterface(scopeObj.viewFormsList.frmPrintTransaction, data);
    };

    /**
     * showAgainAccountsDetails - Method to navigate Accoint details form with saved selected account.
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Account.accountID} accountID, RBObject.Account.accountID
     * @return {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.showAgainAccountsDetails = function(accountID) {
        var scopeObj = this;
        if (!accountID && scopeObj.viewModel && scopeObj.viewModel.selectedAccount && scopeObj.viewModel.selectedAccount.accountID) {
            accountID = scopeObj.viewModel.selectedAccount.accountID;
        }
        if (accountID) {
            scopeObj.fetchUpdatedAccountDetails(accountID);
        } else {
            CommonUtilities.ErrorHandler.onError("showAgainAccountsDetails : No account found." + accountID);
        }
    };

    /**
     * showTransferPrintPage - Method to navigate Transfers Print form , upadate active form
     * @member of {Accounts_PresentationController}
     * @param {object} data , view model for Print form
     * @return {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.showTransferPrintPage = function(data) {
        var scopeObj = this;
        //scopeObj.activeForm = scopeObj.viewFormsList.frmPrintTransfer;
        scopeObj.presentUserInterface(scopeObj.viewFormsList.frmPrintTransfer, data);
    };

    /**
     * onBtnRepeatAccountDetailsTransaction - Method to handle repeat action in Transactions.
     * @member of {Accounts_PresentationController}
     * @param {object} selectedData , transaction data
     * @return {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.onBtnRepeatAccountDetailsTransaction = function(selectedData) {
        var scopeObj = this;
        var dataItem = selectedData.transaction;

        if (dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER || dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.INTERNALTRANSFER) {
            var transactionObject = {
                "amount": Math.abs(dataItem.amount),
                "frequencyEndDate": dataItem.frequencyEndDate,
                "frequencyStartDate": dataItem.frequencyStartDate,
                "frequencyType": dataItem.frequencyType,
                "fromAccountNumber": dataItem.fromAccountNumber,
                "isScheduled": "1",
                "numberOfRecurrences": dataItem.numberOfRecurrences,
                "scheduledDate": dataItem.scheduledDate,
                "toAccountNumber": dataItem.toAccountNumber,
                "transactionDate": dataItem.transactionDate,
                "ExternalAccountNumber": dataItem.ExternalAccountNumber,
                "transactionId": dataItem.transactionId,
                "notes": dataItem.transactionsNotes,
                "transactionType": dataItem.transactionType,
                "category": dataItem.category
            };
            var onCancelCreateTransfer = function() {
                scopeObj.presentUserInterface(scopeObj.viewFormsList.frmAccountsDetails);
            };

            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('TransferModule');
            transferModule.presentationController.showTransferScreen({
                transactionObject: transactionObject,
                onCancelCreateTransfer: onCancelCreateTransfer
            });
        } else if (dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.BILLPAY) {

            var billpayData = {
                "payeeNickname": dataItem.payeeNickName || dataItem.payeeName,
                "dueAmount": dataItem.billDueAmount,
                "payeeId": dataItem.payeeId,
                "billid": dataItem.billid,
                "notes": dataItem.transactionsNotes,
                "amount":  CommonUtilities.formatCurrencyWithCommas(String(Math.abs(dataItem.amount)), true),
                "fromAccountName": dataItem.fromAccountName,
                "fromAccountNumber": dataItem.fromAccountNumber,
                "referenceNumber": dataItem.transactionId,
                "lastPaidAmount": dataItem.billPaidAmount || dataItem.lastPaidAmount,
                "lastPaidDate": CommonUtilities.getFrontendDateString(dataItem.billPaidDate || dataItem.lastPaidDate),
                "nameOnBill": dataItem.nameOnBill,
                "eBillSupport": dataItem.eBillSupport,
                "eBillStatus": dataItem.eBillEnable,
                "billDueDate": CommonUtilities.getFrontendDateString(dataItem.billDueDate),
                "billCategory": dataItem.billCategoryId,
                "billCategoryName": dataItem.billCategory,
                "billGeneratedDate": CommonUtilities.getFrontendDateString(dataItem.billGeneratedDate),
                "ebillURL": dataItem.ebillURL,
				"frequencyEndDate": dataItem.frequencyEndDate,
                "frequencyStartDate": dataItem.frequencyStartDate,
                "frequencyType": dataItem.frequencyType,
              	"isScheduled": dataItem.isScheduled,
                onCancel: function() {
                    scopeObj.showAgainAccountsDetails();
                }
            };

            var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('BillPayModule');
            billPayModule.presentationController.showBillPayData(null, {
                show: 'PayABill',
                data: billpayData
            });
        } else if (dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.P2P) {
            var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
            dataItem.show = "SendMoney";
			dataItem.amount = CommonUtilities.formatCurrencyWithCommas(String(Math.abs(dataItem.amount)), true);
            dataItem.onCancel = function() {
                scopeObj.showAgainAccountsDetails();
            };
            p2pModule.presentationController.showPayAPerson(dataItem);
        } else if (dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.WIRE) {
            var viewModel = {
                payeeNickName: dataItem.payeeNickName,
                payeeName: dataItem.payeeName,
                payeeCurrency: dataItem.payeeCurrency,
                type: dataItem.payeeType,
                wireAccountType: dataItem.wireAccountType,
                accountNumber: dataItem.payeeAccountNumber,
                routingCode: dataItem.routingNumber,
                country: dataItem.country,
                IBAN: dataItem.IBAN,
                fromAccountNumber: dataItem.fromAccountNumber,
                swiftCode: dataItem.swiftCode,
                internationalRoutingCode: dataItem.internationalRoutingCode,
                payeeId: dataItem.payeeId,
                bankAddressLine1: dataItem.bankAddressLine1,
                bankAddressLine2: dataItem.bankAddressLine2,
                bankCity: dataItem.bankCity,
                bankState: dataItem.bankState,
                bankZip: dataItem.bankZip,
                bankName: dataItem.bankName,
                addressLine1: dataItem.payeeAddressLine1,
                addressLine2: dataItem.payeeAddressLine2,
                cityName: dataItem.cityName,
                state: dataItem.state,
                transactionsNotes: dataItem.transactionsNotes,
                amount: Math.abs(dataItem.amount),
                amountRecieved: Math.abs(dataItem.amountRecieved),
                zipCode: dataItem.zipCode
            };
            var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('WireTransferModule');
            wireTransferModule.presentationController.showWireTransfer({
                transactionObject: viewModel
            });
        } else {}
    };

    /**
     * fetchUpdatedAccountDetails - Method to show account details w.r.t accountID. fetch account details by id and navigate to account details form
     * @member of {Accounts_PresentationController}
     * @param {Number} accountID -RBObjects.Account.accountID
     * @return {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchUpdatedAccountDetails = function(accountID) {
        var scopeObj = this;

        function getAccountCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                var context = {
                    "action": "Navigation To AccountDetails",
                    "data": response.data
                };
                scopeObj.presentAccountDetails(context);
            }
        }
        scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccountById", accountID, getAccountCallback));
    };
    /**
     * fetchAccountDetailsById - Method to show account details w.r.t accountID. fetch account details by id and navigate to account details form
     * @member of {Accounts_PresentationController}
     * @param {Number} accountID -RBObjects.Account.accountID
     * @return {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchAccountDetailsById = function(accountID, fromAccount) {
        var scopeObj = this;

        function getAccountCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                var context = {
                    "action": "Account Details Success",
                    "data": [response.data, fromAccount]
                };
                scopeObj.presentAccountDetails(context);
            }
        }
        scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccountById", accountID, getAccountCallback));
    };

    /**
     * backToAccounts - Method to navigate to account dash board from Account detauls
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.backToAccounts = function() {
        var scopeObj = this;
        scopeObj.showAccountsDashboard();
    };

    /************************************************************************************** *******************************************
     * Account Details - Transactions
     *********************************************************************************************************************************/



    //Private Methods and objects used in Transactions.
    function getDateRangeForTimePeriod(searchviewmodel) {
        var startDate = null;
        var endDate = null;
        if (searchviewmodel.timePeriodSelected === OLBConstants.ANY_DATE) {
            startDate = "";
            endDate = "";
        } else if (searchviewmodel.timePeriodSelected === 'CUSTOM_DATE_RANGE') {
            startDate = DateUtils.uiFormatToBackend(searchviewmodel.fromDate);
            endDate = DateUtils.uiFormatToBackend(searchviewmodel.toDate);
        } else {
            var dateConfig = {
                LAST_SEVEN_DAYS: 7,
                LAST_THIRTY_DAYS: 30,
                LAST_SIXTY_DAYS: 60
            };
            var today = new Date();
            endDate = DateUtils.toDateFormat(today, 'dd-mm-yyyy');
            today.setDate(today.getDate() - dateConfig[searchviewmodel.timePeriodSelected]);
            startDate = DateUtils.toDateFormat(today, 'dd-mm-yyyy');
        }
        return {
            startDate: startDate,
            endDate: endDate
        };
    }

    var transactionTypes = {
        Both: 'i18n.accounts.allTransactions',
        Deposit: 'i18n.accounts.deposits',
        Withdrawal: 'i18n.accounts.withdrawls',

        Checks: 'i18n.accounts.checks',
        Transfers: 'i18n.accounts.transfers',
        BillPay: 'i18n.accounts.billPay',
        P2PDebits: 'i18n.accounts.p2pDebits',
        P2PCredits: 'i18n.accounts.p2pCredits'

    };

    var timePeriods = {
        ANY_DATE: 'i18n.accounts.anyDate',
        LAST_SEVEN_DAYS: 'i18n.accounts.lastSevenDays',
        LAST_THIRTY_DAYS: 'i18n.accounts.lastThirtyDays',
        LAST_SIXTY_DAYS: 'i18n.accounts.lastSixtyDays',
        CUSTOM_DATE_RANGE: 'i18n.accounts.customDateRange'
    };


    var objectToListBoxArray = function(obj) {
        var list = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                list.push([key, kony.i18n.getLocalizedString(obj[key])]);
            }
        }
        return list;
    };

    /**
     * displayedPaginationString : Method to create pagination string..
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Transaction} transaction, transaction object
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.displayedPaginationString = function(viewModel, numberOfTransactionsInCurrentPage) {
        return "" + (OLBConstants.PAGING_ROWS_LIMIT * viewModel.page + 1) +
            " - " +
            ((OLBConstants.PAGING_ROWS_LIMIT * viewModel.page) + numberOfTransactionsInCurrentPage);
    };


    /**
     * showTransactionsSearchResults : Method to show Transactons search result.
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Transaction} transaction, transaction object
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.showTransactionsSearchResults = function(transactions) {
        var scopeObj = this;
        var viewModel = scopeObj.viewModel.transactionsViewModel;

        viewModel.showingType = OLBConstants.ALL;
        viewModel.searchViewModel.searchPerformed = true;
        if (Array.isArray(transactions) && transactions.length > 0) {
            viewModel.paginationString = scopeObj.displayedPaginationString(viewModel, transactions.length);
        } else {
            viewModel.paginationString = false;
        }
        var toTransactionViewModel = function(transaction) {
            return scopeObj.getTransactionViewModel(transaction, viewModel.accountType, OLBConstants.ALL);
        };
        viewModel.pendingTransactions = transactions.filter(scopeObj.isPendingTransaction).map(toTransactionViewModel);
        viewModel.recentTransactions = transactions.filter(scopeObj.isSuccessfulTransacion).map(toTransactionViewModel);
        viewModel.renderTransactions = true;
        scopeObj.presentAccountDetailsTransactions(viewModel);
    };

    /**
     * getTransactionViewModel : Method to return Transactons view model
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Transaction} transaction, transaction object
     * @param {RBObject.Account.accountType} accountType, account type 
     * @param {string} requestedTransactionType, requsted account type All/Saving specific to tab
     * @return {Object} , Transaction view model. 
     * @throws {}
     */
    Accounts_PresentationController.prototype.getTransactionViewModel = function(transaction, accountType, requestedTransactionType) {
        var scopeObj = this;

        var numberOfRecurrences;

        if (transaction.numberOfRecurrences === undefined || transaction.numberOfRecurrences === null || transaction.numberOfRecurrences === "0") {
            numberOfRecurrences = kony.i18n.getLocalizedString('i18n.common.none');
        } else {
            numberOfRecurrences = transaction.numberOfRecurrences;
        }

        return {
            date: formatDate(transaction.transactionDate),
            description: transaction.description || kony.i18n.getLocalizedString('i18n.common.none'),
            statusDescription: transaction.statusDescription.toLowerCase(),
            notes: (transaction.transactionsNotes === null || transaction.transactionsNotes === undefined) ? kony.i18n.getLocalizedString("i18n.common.none") : transaction.transactionsNotes,
            type: transaction.transactionType,
            fromAccountName: transaction.fromAccountName,
            externalAccountNumber: transaction.ExternalAccountNumber,
            showTransactionIcons: scopeObj.isTransactionTypeIconsVisible(accountType, requestedTransactionType),
            fromAcc: transaction.fromAccountNumber,
            toAcc: transaction.toAccountNumber,
            reference: transaction.transactionId,
            frequencyType: transaction.frequencyType || kony.i18n.getLocalizedString('i18n.transfers.frequency.once'),
            amount: CommonUtilities.getDisplayCurrencyFormat(transaction.amount),
            balance: transaction.fromAccountBalance ? CommonUtilities.getDisplayCurrencyFormat(transaction.fromAccountBalance) : kony.i18n.getLocalizedString('i18n.common.none'),
            category: transaction.category,
            accountType: accountType,
            nickName: transaction.toAccountName || transaction.payPersonName || transaction.payeeNickName || transaction.payeeName,
            numberOfRecurrences: numberOfRecurrences,
            transaction: transaction,
            //checks
            frontImage1: transaction.frontImage1,
            frontImage2: transaction.frontImage2,
            backImage1: transaction.backImage1,
            backImage2: transaction.backImage2,
            memo: transaction.memo ? transaction.memo : "",
            checkNumber1: transaction.checkNumber1,
            checkNumber2: transaction.checkNumber2,
            bankName1: transaction.bankName1,
            bankName2: transaction.bankName2,
            withdrawlAmount1: transaction.withdrawlAmount1 ? CommonUtilities.getDisplayCurrencyFormat(transaction.withdrawlAmount1) : "",
            withdrawlAmount2: transaction.withdrawlAmount2 ? CommonUtilities.getDisplayCurrencyFormat(transaction.withdrawlAmount2) : "",
            totalAmount: transaction.totalCheckAmount ? CommonUtilities.getDisplayCurrencyFormat(transaction.totalCheckAmount) : "",
            cashAmount: transaction.cashAmount ? CommonUtilities.getDisplayCurrencyFormat(transaction.cashAmount) : "",
            //wire
            isPayeeExists: transaction.isPayeeDeleted === "true" ? false : true

        };
    };

    /**
     * isTransactionTypeIconsVisible : Method to check whether the transaction icons should be displayed
     * @member of {Accounts_PresentationController}
     * @param {String} accountType, account type 
     * @param {String} requestedTransactionType ,requsted account type All/Saving specific to tab
     * @returns {boolean} true/false whether the transaction icons should be displayed
     * @throws {}
     */
    Accounts_PresentationController.prototype.isTransactionTypeIconsVisible = function(accountType, requestedTransactionType) {
        return requestedTransactionType === OLBConstants.ALL && (accountType === OLBConstants.ACCOUNT_TYPE.SAVING || accountType === OLBConstants.ACCOUNT_TYPE.CHECKING);
    };


    /**
     * isPendingTransaction : Method to return wheter Transacton is pending or not.
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Transaction} transaction, transaction object
     * @return {boolean} , true/false wheter Transacton is pending or not.
     * @throws {}
     */
    Accounts_PresentationController.prototype.isPendingTransaction = function(transaction) {
        return typeof transaction.statusDescription === 'string' && transaction.statusDescription.toLowerCase() === OLBConstants.PENDING;
    };

    /**
     * isSuccessfulTransacion : Method to return wheter Transacton is sucessful or not.
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Transaction} transaction, transaction object
     * @return {boolean} , true/false wheter Transacton is sucessful or not.
     * @throws {}
     */
    Accounts_PresentationController.prototype.isSuccessfulTransacion = function(transaction) {
        return typeof transaction.statusDescription === 'string' && transaction.statusDescription.toLowerCase() === OLBConstants.SUCCESSFUL;
    };


    /**
     * presentAccountDetailsTransactions : Method to call Account details for Transactions view model.
     * @member of {Accounts_PresentationController}
     * @param {Object} transaction, transactions view model object
     * @param {boolean} renderTransactions, true/false wheter render Transactons data or not. Default true
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.presentAccountDetailsTransactions = function(viewmodel) {
        var scopeObj = this;
        scopeObj.presentAccountDetails({
            transactionDetails: viewmodel
        });
    };


    /**
     * fetchTransactionsBySearch : Method to fetch Transactions by given searchView
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Account} account, account object
     * @param {Object} searchViewModel, view model for expected search 
     * @param {successCallback} successCallback, success callback 
     * @param {errorCallBack} errorCallBack, failure callback
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchTransactionsBySearch = function(account, searchViewModel, successCallback, errorCallBack) {
        var scopeObj = this;

        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                successCallback(response.data);
            } else {
                errorCallBack(response.data);
            }
        }
        var dateRange = getDateRangeForTimePeriod(searchViewModel);
        var commandObj = {
            searchTransactionType: searchViewModel.transactionTypeSelected,
            searchDescription: searchViewModel.keyword,
            searchMinAmount: searchViewModel.fromAmount,
            searchMaxAmount: searchViewModel.toAmount,
            searchStartDate: dateRange.startDate,
            searchEndDate: dateRange.endDate,
            fromCheckNumber: searchViewModel.fromCheckNumber,
            toCheckNumber: searchViewModel.toCheckNumber,
            accountNumber: account.accountID
        };
        this.searchParams = commandObj; // used for download
        scopeObj.businessController.execute(new kony.mvc.Business.Command('com.kony.accounts.searchAccountTransactions', commandObj, completionCallback));
    };


    /**
     * fetchTransactions : Method to fetch Transactions by given type and dataInputs for pagination , sorting etc and "isScheduled": "false".
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Account} account, account object
     * @param {String} transactionType , transction type All, checks, depost  etc.
     * @param {Object} dataInputs , extra data inputs for pagination, sorting etc.
     * @param {successCallback} successCallback, success callback 
     * @param {errorCallBack} errorCallBack, failure callback
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchTransactions = function(account, transactionType, dataInputs, successCallback, errorCallBack) {
        var scopeObj = this;
        var viewModel = scopeObj.viewModel.transactionsViewModel;
        var commandObj = {
            "account": account,
            "accountID": account.accountID,
            "transactionType": transactionType,
            "offset": OLBConstants.PAGING_ROWS_LIMIT * viewModel.page,
            "limit": OLBConstants.PAGING_ROWS_LIMIT + 1,
            "isScheduled": "false",
            "sortBy": dataInputs.sortBy,
            "order": dataInputs.order
        };
        scopeObj.fetchTransactionsByType(commandObj, successCallback, errorCallBack);
    };


    /**
     * showTransactions : Method to show Transactions by given type and dataInputs for pagination , sorting etc.
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Account} account, account object
     * @param {String} transactionType , transction type All, checks, depost  etc.
     * @param {Object} dataInputs , extra data inputs for pagination, sorting etc.
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.showTransactions = function(account, transactionType, dataInputs) {
        var scopeObj = this;
        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, scopeObj.transactonsConfig[transactionType]);
        scopeObj.fetchTransactions(
            account,
            transactionType,
            tmpInputs,
            scopeObj.onSuccessTransactions.bind(scopeObj),
            scopeObj.onServerError.bind(scopeObj)
        );
    };

    /**
     * onSuccessTransactions : Method to handle on succsefull fetchTransactions.
     * @member of {Accounts_PresentationController}
     * @param {Array} transactions,  RBObject.Transaction - transactions object array
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.onSuccessTransactions = function(transactions, fetchInputs) {
        var scopeObj = this;
        var viewModel = scopeObj.viewModel.transactionsViewModel;
        if (transactions.length > OLBConstants.PAGING_ROWS_LIMIT) { //TODO: for pagination ,  required refactoring if standard pagination is ready.
            viewModel.isLastPageFlag = false;
        } else {
            viewModel.isLastPageFlag = true;
        }
        transactions = transactions.filter(function(_, i) {
            return i < OLBConstants.PAGING_ROWS_LIMIT;
        });
        if (Array.isArray(transactions) && transactions.length > 0) {
            viewModel.paginationString = scopeObj.displayedPaginationString(viewModel, transactions.length);
            viewModel.config = fetchInputs;
        } else {
            viewModel.paginationString = false;
        }
        viewModel.showingType = fetchInputs.transactionType;
        viewModel.accountType = fetchInputs.account.accountType;
        var toTransactionViewModel = function(transaction) {
            return scopeObj.getTransactionViewModel(transaction, viewModel.accountType, viewModel.showingType);
        };
        viewModel.pendingTransactions = transactions.filter(scopeObj.isPendingTransaction).map(toTransactionViewModel);
        viewModel.recentTransactions = transactions.filter(scopeObj.isSuccessfulTransacion).map(toTransactionViewModel);
        viewModel.renderTransactions = true;
        scopeObj.presentAccountDetailsTransactions(viewModel);
    };

    /**
     * resetSearchViewModel : Method to reset transactions search view model.
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.resetSearchViewModel = function() {
        var scopeObj = this;
        var viewModel = scopeObj.viewModel.transactionsViewModel;
        viewModel.searchViewModel.visible = false;
        viewModel.searchViewModel.searchPerformed = false;
        viewModel.transactionTypeSelected = OLBConstants.BOTH;
        viewModel.timePeriodSelected = OLBConstants.ANY_DATE;
        viewModel.keyword = "";
        viewModel.fromAmount = "";
        viewModel.toAmount = "";
        viewModel.fromCheckNumber = "";
        viewModel.toCheckNumber = "";
        viewModel.startDate = DateUtils.getCurrentDateString();
        viewModel.endDate = DateUtils.getCurrentDateString();
    };

    /**
     * initTransactionsViewModel  : Initializes the state of the transactions view  model, 
     * call before switching to showing different type of transactions
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.initTransactionsViewModel = function() {
        var scopeObj = this;
        var viewModel = scopeObj.viewModel.transactionsViewModel;
        viewModel.page = 0;
        viewModel.isLastPageFlag = false;
        viewModel.pendingTransactions = [];
        viewModel.recentTransactions = [];
        scopeObj.resetSearchViewModel();
    };

    /**
     * isLastPage  : Initializes the state of the transactions view  model, 
     * call before switching to showing different type of transactions
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.isLastPage = function(viewModel) {
        return viewModel.isLastPageFlag || (viewModel.pendingTransactions.length + viewModel.recentTransactions.length) < OLBConstants.PAGING_ROWS_LIMIT;
    };

    /**
     * getAccountDetailsTransactionsViewModel : Method to return Account details Transactons view model
     * @member of {Accounts_PresentationController}
     * @param {Object} account, 
     * @return {Object} , Transaction view model. 
     * @throws {}
     */
    Accounts_PresentationController.prototype.getAccountDetailsTransactionsViewModel = function(account) {
        var scopeObj = this;
        var viewModel = {}; //local variable to store transactions view model.
        scopeObj.transactonsConfig = {
            'All': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Checks': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Deposits': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Transfers': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Withdrawals': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Payments': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Purchases': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            },
            'Interest': {
                'sortBy': 'transactionDate',
                'defaultSortBy': 'transactionDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            }
        };
        scopeObj.viewModel.transactionsViewModel = {
            searchViewModel: {
                searchPerformed: false,
                visible: false,
                searchResults: [],
                keyword: '',
                transactionTypes: objectToListBoxArray(transactionTypes),
                transactionTypeSelected: OLBConstants.BOTH,
                timePeriods: objectToListBoxArray(timePeriods),
                timePeriodSelected: OLBConstants.ANY_DATE,
                fromCheckNumber: '',
                toCheckNumber: '',
                fromAmount: '',
                toAmount: '',
                getTransactionTypeFromKey: function(key) {
                    return kony.i18n.getLocalizedString(transactionTypes[key]);
                },
                getTimePeriodFromKey: function(key) {
                    return kony.i18n.getLocalizedString(timePeriods[key]);
                },
                fromDate: DateUtils.getCurrentDateString(),
                toDate: DateUtils.getCurrentDateString(),
                toggleSearchView: function() {
                    if (viewModel.showingType != OLBConstants.ALL) {
                        viewModel.showTransactionsByType();
                        viewModel.searchViewModel.visible = true;
                    } else if (viewModel.searchViewModel.searchPerformed) {
                        viewModel.showTransactionsByType();
                    } else {
                        viewModel.searchViewModel.visible = !viewModel.searchViewModel.visible;
                        viewModel.renderTransactions = false;
                        scopeObj.presentAccountDetailsTransactions(viewModel);
                    }
                },
                performSearch: function(updatedViewModel) {
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: ['transactionDetails']
                    });
                    scopeObj.fetchTransactionsBySearch(account, updatedViewModel, scopeObj.showTransactionsSearchResults.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
                },
                modifySearch: function(updatedViewModel) {
                    viewModel.searchViewModel = updatedViewModel;
                    viewModel.searchViewModel.visible = true;
                    viewModel.searchViewModel.searchPerformed = false;
                    viewModel.renderTransactions = false;
                    scopeObj.presentAccountDetailsTransactions(viewModel);

                },
                clearSearch: function(updatedViewModel) {
                    scopeObj.initTransactionsViewModel();
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: ['transactionDetails']
                    });
                    viewModel.showTransactionsByType();
                },
            },
            showingType: null,
            pendingTransactions: [],
            recentTransactions: [],
            page: 0,
            paginationString: '',
            renderTransactions: true,
            isLastPageFlag: false,
            nextPage: function(dataInputs) {
                if (viewModel.canGoNext()) {
                    viewModel.page += 1;
                    scopeObj.transactonsConfig[viewModel.showingType].offset = viewModel.page * OLBConstants.PAGING_ROWS_LIMIT;
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: ['transactionDetails']
                    });
                    scopeObj.showTransactions(account, viewModel.showingType, dataInputs);
                }
            },
            prevPage: function(dataInputs) {
                if (viewModel.canGoPrev()) {
                    viewModel.page -= 1;
                    scopeObj.transactonsConfig[viewModel.showingType].offset = viewModel.page * OLBConstants.PAGING_ROWS_LIMIT;
                    scopeObj.updateLoadingForCompletePage({
                        isLoading: true,
                        serviceViewModels: ['transactionDetails']
                    });
                    scopeObj.showTransactions(account, viewModel.showingType, dataInputs);
                }
            },
            canGoNext: function() {
                return !scopeObj.isLastPage(viewModel);
            },
            canGoPrev: function() {
                return viewModel.page > 0;
            },
            gotoFirstPage: function(dataInputs) {
                viewModel.page = 0;
                scopeObj.updateLoadingForCompletePage({
                    isLoading: true,
                    serviceViewModels: ['transactionDetails']
                });
                scopeObj.showTransactions(account, viewModel.showingType, dataInputs);
            },
            gotoLastPage: function(dataInputs) {
                //TODO : requires getting count of all transactions
                scopeObj.updateLoadingForCompletePage({
                    isLoading: true,
                    serviceViewModels: ['transactionDetails']
                });
                scopeObj.showTransactions(account, viewModel.showingType, dataInputs);
            },
            showTransactionsByType: function(dataInputs) {
                dataInputs = dataInputs || {};
                scopeObj.initTransactionsViewModel();
                scopeObj.updateLoadingForCompletePage({
                    isLoading: true,
                    serviceViewModels: ['transactionDetails']
                });
                scopeObj.showTransactions(account, dataInputs.type || viewModel.showingType, dataInputs);
            },
            config: null //store fetched transaction config to maintain sort and pagination

        };
        viewModel = scopeObj.viewModel.transactionsViewModel;
        scopeObj.initTransactionsViewModel();
        viewModel.showTransactionsByType({
            resetSorting: true,
            type: OLBConstants.ALL
        });
        return {
            showTransactionsByType: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs);
            },
            showAll: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.ALL
                });
            },
            showChecks: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.CHECKS
                });
            },
            showDeposits: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.DEPOSITS
                });
            },
            showTransfers: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.TRANSFERS
                });
            },
            showWithdrawals: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.WITHDRAWLS
                });
            },
            showInterest: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.INTEREST
                });
            },
            showPurchase: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.PURCHASES
                });
            },
            showPayment: function(dataInputs) {
                viewModel.showTransactionsByType(dataInputs || {
                    resetSorting: true,
                    type: OLBConstants.TRANSACTION_TYPE.PAYMENTS
                });
            },
            nextPage: function() {
                viewModel.nextPage();
            },
            prevPage: function() {
                viewModel.prevPage();
            }
        };
    };

    /*********************************************************************************************************************************
     * Account Details - Scheduled Transactions
     *********************************************************************************************************************************/



    /**
     * presentScheuledTrasansctions : show/update Account Details screen witn given view model
     * @member of {Accounts_PresentationController}
     * @param {JSON Object} viewModel, view model object for Scheduled Transactions.
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.presentScheuledTrasansctions = function(viewModel) {
        this.presentUserInterface(this.viewFormsList.frmScheduledTransactions, viewModel);
    };

    /**
     * loadScheuledTransactionsComponents : load scheduled transactions components topbar, sidemenu etc.
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.loadScheuledTransactionsComponents = function() {
        var scopeObj = this;
        var howToShowHamburgerMenu = function(sideMenuViewModel) {
            scopeObj.presentScheuledTrasansctions({
                sideMenu: sideMenuViewModel
            });
        };
        scopeObj.SideMenu.init(howToShowHamburgerMenu);

        var presentTopBar = function(topBarViewModel) {
            scopeObj.presentScheuledTrasansctions({
                topBar: topBarViewModel
            });
        };
        scopeObj.TopBar.init(presentTopBar);
    };

    /**
     * showScheduledTransactionsForm - Show Scheduled Transactions form
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account 
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.showScheduledTransactionsForm = function(account) {
        var scopeObj = this;
        scopeObj.activeForm = scopeObj.viewFormsList.frmScheduledTransactions;
        scopeObj.viewModel.selectedAccount = account;
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: scopeObj.serviceViewModels[scopeObj.activeForm]
        });
        scopeObj.loadScheuledTransactionsComponents();
        scopeObj.ScheduledTransactions = scopeObj.getAccountDetailsScheduledTransactionsViewModel(account);
        console.log("Temp fix for MF issue with async calls.");
        console.log("Temp fix for MF issue with async calls.");
        scopeObj.loadScheduledTransctionsAllAccounts();
    };

    /**
     * showScheduledTransactionAccountInfo - Show Scheduled Transactions Account Info
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account 
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.showScheduledTransactionAccountInfo = function(account) {
        var scopeObj = this;
        var accountInfo = {
            accountName: outputDataCheck(CommonUtilities.getAccountName(account)),
            accountNumber: displayAccountNumber(account.accountID),
            accountID: account.accountID,
            accountType: outputDataCheck(account.accountType),
            routingNumber: outputDataCheck(account.routingNumber),
            swiftCode: outputDataCheck(account.swiftCode),
            primaryAccountHolder: outputDataCheck(account.accountHolder),
            jointHolder: outputDataCheck(account.jointHolders),
            creditIssueDate: CommonUtilities.formatDate(account.openingDate),
            creditcardNumber: outputDataCheck(account.creditCardNumber),
        };
        scopeObj.presentScheuledTrasansctions({
            accountInfo: accountInfo
        });
    };

    /**
     * loadScheduledTransctionsAllAccounts : Method load all accounts for Schduled transactions.
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.loadScheduledTransctionsAllAccounts = function() {
        var scopeObj = this;
        scopeObj.fetchAccounts(scopeObj.onScheduledTransctionsAllAccounts.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
    };

    /**
     * getScheduledTransactionsAllAccountsViewModel : Method to return All accounts view model for Scheduled transactions.
     * @member of {Accounts_PresentationController}
     * @param {Array} accounts - RBObjects.Account , all accounts
     * @returns {Array} accounts -  All accounts view model for Account list in Scheduled transactions.
     * @throws {}
     */
    Accounts_PresentationController.prototype.getScheduledTransactionsAllAccountsViewModel = function(accounts) {
        var scopeObj = this;
        var createAccountViewModal = function(account) {
            return {
                //construct the view modal for the account  
                accountName: CommonUtilities.getAccountName(account),
                accountNumber: displayAccountNumber(account.accountID),
                type: account.accountType,
                onAccountSelection: function() {
                    return scopeObj.showScheduledTransactions(account);
                }
            };
        };
        return scopeObj.getInteralAccounts(accounts).map(createAccountViewModal);
    };

    /**
     * onScheduledTransctionsAllAccounts : Method to handle on successful fetch accounts from MF w.r.t Scheduled.
     * @member of {Accounts_PresentationController}
     * @param {Array} accounts - RBObjects.Account , all accounts
     * @returns {} 
     * @throws {}
     */
    Accounts_PresentationController.prototype.onScheduledTransctionsAllAccounts = function(accounts) {
        var scopeObj = this;
        var accountList = scopeObj.getScheduledTransactionsAllAccountsViewModel(accounts);
        scopeObj.presentScheuledTrasansctions({
            accountList: accountList
        });
    };

    /**
     * fetchScheduledTransactions : Method to fetch Transactions by given type and dataInputs for pagination , sorting etc and "isScheduled": "true".
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Account} account, account object
     * @param {Object} dataInputs , extra data inputs for pagination, sorting etc.
     * @param {successCallback} successCallback, success callback 
     * @param {errorCallBack} errorCallBack, failure callback
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.fetchScheduledTransactions = function(account, dataInputs, successCallBack, errorCallBack) {
        var scopeObj = this;
        var commandObj = {
            "account": account,
            "accountID": account.accountID,
            "transactionType": dataInputs.transactionType,
            "offset": dataInputs.offset,
            "limit": dataInputs.limit,
            "isScheduled": "true", //true to get scheduled transactions
            "sortBy": dataInputs.sortBy,
            "order": dataInputs.order,
        };
        scopeObj.fetchTransactionsByType(commandObj, successCallBack, errorCallBack);
    };

    /**
    .* fetchTransactionsByType : Method to fetch Transactions by given type and dataInputs for isSchedule, pagination , sorting etc and "isScheduled": "false".
    * @member of {Accounts_PresentationController}
    * @param {Object} dataInputs , extra data inputs for pagination, sorting etc.
    * @param {successCallback} successCallback, success callback 
    * @param {errorCallBack} errorCallBack, failure callback
    * @return {}
    * @throws {}
    */
    Accounts_PresentationController.prototype.fetchTransactionsByType = function(dataInputs, successCallback, errorCallBack) {
        var scopeObj = this;
        var completionCallback = function(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                successCallback(response.data, dataInputs);
            } else {
                errorCallBack(response.data);
            }
        };
        scopeObj.businessController.execute(new kony.mvc.Business.Command('com.kony.accounts.getAccountTransactionsByType', dataInputs, completionCallback));
    };



    /**
     * showScheduledTransactions : Method to show Scheduled Transactions, Default ALL transactions .
     * @member of {Accounts_PresentationController}
     * @param {RBObjects.Account} account 
     * @param {Object} dataInputs , extra data inputs for pagination, sorting etc.
     * @param {successCallback} successCallback, success callback 
     * @param {errorCallBack} errorCallBack, failure callback
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.showScheduledTransactions = function(account, dataInputs) {
        var scopeObj = this;
        scopeObj.viewModel.selectedAccount = account;
        scopeObj.showScheduledTransactionAccountInfo(account);
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['transactionDetails']
        });
        dataInputs = dataInputs || {};
        dataInputs.transactionType = OLBConstants.ALL;
        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, scopeObj.scheduleTransactonsConfig[dataInputs.transactionType]);
        tmpInputs.transactionType = OLBConstants.ALL;
        scopeObj.fetchScheduledTransactions(
            account,
            tmpInputs,
            scopeObj.onSuccessScheduledTransactions.bind(scopeObj),
            scopeObj.onServerError.bind(scopeObj)
        );
    };

    /**
     * onSuccessScheduledTransactions : Method to handle on succsefull fetchScheduled Transactions.
     * @member of {Accounts_PresentationController}
     * @param {Array} transactions,  RBObject.Transaction - transactions object array
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.onSuccessScheduledTransactions = function(transactions, fetchInputs) {
        var scopeObj = this;
        var viewModel = scopeObj.viewModel.scheduledTransactionsViewModel;

        var toTransactionViewModel = function(transaction) {
            return scopeObj.createScheduledTransactionViewModel(transaction, viewModel.accountType, viewModel.showingType);
        };
        viewModel.showingType = fetchInputs.transactionType;
        viewModel.accountType = fetchInputs.account.accountType;
        viewModel.showingForAccount = fetchInputs.account;
        viewModel.config = fetchInputs;
        viewModel.scheduledTransactions = transactions.map(toTransactionViewModel);
        scopeObj.presentScheuledTrasansctions({
            transactionDetails: viewModel
        });
    };

    /**
     * createScheduledTransactionViewModel : Method to return Scheduled Transactons view model
     * @member of {Accounts_PresentationController}
     * @param {RBObject.Transaction} transaction, transaction object
     * @param {RBObject.Account.accountType} accountType, account type 
     * @param {string} requestedTransactionType, requsted account type All/Saving specific to tab
     * @return {Object} , scheduled Transaction view model. 
     * @throws {}
     */
    Accounts_PresentationController.prototype.createScheduledTransactionViewModel = function(transaction, accountType, requestedTransactionType) {
        var scopeObj = this;
        return {
            date: formatDate(transaction.scheduledDate),
            description: transaction.description,
            notes: (transaction.transactionsNotes === null || transaction.transactionsNotes === undefined) ? kony.i18n.getLocalizedString("i18n.common.none") : transaction.transactionsNotes,
            type: transaction.transactionType,
            showTransactionIcons: scopeObj.isTransactionTypeIconsVisible(accountType, requestedTransactionType),
            toAcc: transaction.toAccountNumber,
            reference: transaction.transactionId,
            amount: CommonUtilities.getDisplayCurrencyFormat(transaction.amount),
            balance: CommonUtilities.getDisplayCurrencyFormat(transaction.fromAccountBalance),
            category: transaction.category,
            accountType: accountType,
            nickName: transaction.toAccountName || transaction.payPersonName || transaction.payeeNickName || transaction.payeeName,
          	recurrenceDesc: transaction.recurrenceDesc,
            frequencyType: transaction.frequencyType,
            frequencyStartDate: transaction.frequencyStartDate,
            frequencyEndDate: transaction.frequencyEndDate,
            transaction: transaction
        };
    };


    /**
     * initScheduledTransactionsViewModel  : Initializes the state of the scheduled transactions view  model, 
     * call before switching to showing different type of transactions
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {}
     * @throws {}
     */
    Accounts_PresentationController.prototype.initScheduledTransactionsViewModel = function() {
        var scopeObj = this;
        var viewModel = scopeObj.viewModel.scheduledTransactionsViewModel;
        viewModel.showingForAccount = null;
        viewModel.showingType = null;
        viewModel.scheduledTransactions = [];
    };

    /**
     * getAccountDetailsScheduledTransactionsViewModel : Method to return Account details  Scheduled Transactons view model
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {Object} , Scheduled Transactions view model. 
     * @throws {}
     */
    Accounts_PresentationController.prototype.getAccountDetailsScheduledTransactionsViewModel = function(account) {
        var scopeObj = this;

        /**
         * viewModel for Transactions component
         * Ideally the entire state of component is concentrated here
         */
        scopeObj.scheduleTransactonsConfig = {
            'All': {
                'sortBy': 'scheduledDate',
                'defaultSortBy': 'scheduledDate',
                'order': OLBConstants.DESCENDING_KEY,
                'defaultOrder': OLBConstants.DESCENDING_KEY,
                'offset': OLBConstants.DEFAULT_OFFSET,
                'limit': OLBConstants.PAGING_ROWS_LIMIT
            }
        };
        scopeObj.viewModel.scheduledTransactionsViewModel = {
            showingForAccount: null,
            showingType: null,
            scheduledTransactions: [],
            showTransactionType: null,
            config: null //store fetched transactions config.
        };
        scopeObj.initScheduledTransactionsViewModel();

        var viewModel = scopeObj.viewModel.scheduledTransactionsViewModel;

        viewModel.events = {
            showAccountDetails: function() {
                scopeObj.showAccountDetails(scopeObj.viewModel.selectedAccount);
            }
        };
        scopeObj.presentScheuledTrasansctions({
            events: viewModel.events
        });

        scopeObj.showScheduledTransactionAccountInfo(account);
        scopeObj.showScheduledTransactions(account, {
            transactionType: OLBConstants.ALL,
            resetSorting: true
        });

        return {
            showTransactionsByType: function(dataInputs) {
                scopeObj.showScheduledTransactions(scopeObj.viewModel.selectedAccount, dataInputs);
            },
            onModifyTransaction: function(dataItem) {

                if (dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.EXTERNALTRANSFER || dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.INTERNALTRANSFER) {
                    var record = {
                        "amount": Math.abs(dataItem.amount),
                        "frequencyEndDate": dataItem.frequencyEndDate,
                        "frequencyStartDate": dataItem.frequencyStartDate,
                        "frequencyType": dataItem.frequencyType,
                        "fromAccountNumber": dataItem.fromAccountNumber,
                        "isScheduled": "1",
                        "numberOfRecurrences": dataItem.numberOfRecurrences,
                        "scheduledDate": dataItem.scheduledDate,
                        "toAccountNumber": dataItem.toAccountNumber,
                        "transactionDate": dataItem.transactionDate,
                        "ExternalAccountNumber": dataItem.ExternalAccountNumber,
                        "transactionId": dataItem.transactionId,
                        "notes": dataItem.transactionsNotes,
                        "transactionType": dataItem.transactionType,
                        "category": dataItem.category
                    };
                    var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('TransferModule');
                    var onCancel = function() {
                        scopeObj.showScheduledTransactionsForm(scopeObj.viewModel.selectedAccount);
                    };
                    var context = {};
                    context.editTransactionObject = record;
                    context.onCancelCreateTransfer = onCancel;
                    transfersModule.presentationController.showTransferScreen(context);

                } else if (dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.BILLPAY) {
                    var billpayData = {
                        "payeeNickname": dataItem.payeeNickName || dataItem.payeeName,
                        "dueAmount": dataItem.billDueAmount,
                        "payeeId": dataItem.payeeId,
                        "billid": dataItem.billid,
                        "sendOn": CommonUtilities.getFrontendDateString(dataItem.scheduledDate),
                        "notes": dataItem.transactionsNotes,
                        "amount": CommonUtilities.formatCurrencyWithCommas(String(Math.abs(dataItem.amount)), true),
                        "fromAccountName": dataItem.fromAccountName,
                        "fromAccountNumber": dataItem.fromAccountNumber,
                        "referenceNumber": dataItem.transactionId,
                        "lastPaidAmount": dataItem.billPaidAmount || dataItem.lastPaidAmount,
                        "lastPaidDate": CommonUtilities.getFrontendDateString(dataItem.billPaidDate || dataItem.lastPaidDate),
                        "nameOnBill": dataItem.nameOnBill,
                        "eBillSupport": dataItem.eBillSupport,
                        "eBillStatus": dataItem.eBillEnable,
                        "billDueDate": CommonUtilities.getFrontendDateString(dataItem.billDueDate),
                        "billCategory": dataItem.billCategoryId,
                        "billCategoryName": dataItem.billCategory,
                        "billGeneratedDate": CommonUtilities.getFrontendDateString(dataItem.billGeneratedDate),
                        "ebillURL": dataItem.ebillURL,
                        onCancel: function() {
                            scopeObj.showScheduledTransactionsForm(scopeObj.viewModel.selectedAccount);
                        }
                    };
                    var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('BillPayModule');
                    billPayModule.presentationController.showBillPayData(null, {
                        show: 'PayABill',
                        data: billpayData
                    });
                } else if (dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.P2P) {
                    var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    dataItem.show = "SendMoney";
					dataItem.amount = CommonUtilities.formatCurrencyWithCommas(String(Math.abs(dataItem.amount)), true);
                    dataItem.onCancel = function() {
                        scopeObj.showScheduledTransactionsForm(scopeObj.viewModel.selectedAccount);
                    };
                    p2pModule.presentationController.showPayAPerson(dataItem);
                } else if (dataItem.transactionType === OLBConstants.TRANSACTION_TYPE.LOAN) {
                    scopeObj.fetchAccountDetailsById(dataItem.toAccountNumber, account.accountID);
                } else {

                }
            }
        };
    };
  	
  	/**
     * cancelScheduledTransactionOccurrence : Method to cancel an occurrence of a transaction.
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {Object} - Transaction object. 
     * @throws {}
     */
  	Accounts_PresentationController.prototype.cancelScheduledTransactionOccurrence = function(transaction){
      var self = this;
      function cancelTransactionOccurrenceCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          self.ScheduledTransactions.showTransactionsByType({});
        }else{
          self.onServerError(true);
          self.presentScheuledTrasansctions({});
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.accounts.cancelTransactionOccurrence', transaction, cancelTransactionOccurrenceCallback));
    };
  
  	/**
     * cancelScheduledTransaction : Method to cancel a transaction.
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {Object} - Transaction object. 
     * @throws {}
     */
  	Accounts_PresentationController.prototype.cancelScheduledTransaction = function(transaction){
      var self = this;
      function cancelTransactionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          self.ScheduledTransactions.showTransactionsByType({});
        }else{
          self.onServerError(true);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.accounts.cancelTransaction', transaction, cancelTransactionCallback));
    };
	
  	/**
     * cancelScheduledTransactionSeries : Method to cancel a series of transaction.
     * @member of {Accounts_PresentationController}
     * @param {} 
     * @return {Object} - Transaction object. 
     * @throws {}
     */
  	Accounts_PresentationController.prototype.cancelScheduledTransactionSeries = function(transaction){
      var self = this;
      function cancelTransactionSeriesCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          self.ScheduledTransactions.showTransactionsByType({});
        }else{
          self.onServerError(true);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.accounts.cancelTransactionSeries', transaction, cancelTransactionSeriesCallback));
    };
   /*
   ** Process external Account Data
   */
    Accounts_PresentationController.prototype.processExternalAccountsData = function(rawData) {
        try {
            var externalAccounts = [];
            rawData.forEach(function(externalAccount) {
                var account = {};
                account.accountName = externalAccount.AccountName;
                account.nickName = externalAccount.NickName;
                account.accountID = externalAccount.Number;
                account.accountType = externalAccount.TypeDescription;
                account.availableBalance = externalAccount.AvailableBalance;
                account.currentBalance = externalAccount.AvailableBalance;
                account.outstandingBalance = externalAccount.AvailableBalance;
                account.availableBalanceUpdatedAt = (externalAccount.LastUpdated)?(CommonUtilities.getTimeDiferenceOfDate(externalAccount.LastUpdated)):kony.i18n.getLocalizedString('i18n.AcountsAggregation.timeDifference.justNow');
                if (String(externalAccount.FavouriteStatus).trim().toLowerCase() === "true") {
                    account.favouriteStatus = "1";
                } else {
                    account.favouriteStatus = "0";
                }
                account.bankLogo = externalAccount.BankLogo;
                account.isError = isError(externalAccount.error);
                account.isExternalAccount = true;
                account.bankName = externalAccount.BankName;
                account.userName = (externalAccount.Username)?externalAccount.Username:externalAccount.username;
                account.bankId = externalAccount.Bank_id;
                account.externalAccountId = externalAccount.Account_id;
                account.accountHolder = externalAccount.AccountHolder;
                externalAccounts.push(account);
            });
            return externalAccounts;
        } catch (error) {}
    };

    function isError(error) {
        try {
            if (error && error.trim() !== "") {
                return true;
            }
            return false;
        } catch (error) {}
    }
    



    Accounts_PresentationController.prototype.deleteExternalAccount = function(accountObject) {
        function deleteExternalAccountCompletionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
//              will  uncomment below lines if we find a way to delete the removed account from the list.for now refreshing the page.
//                 self.presentAccountsDashboard({
//                   'accountDeleteSuccess' : {
//                     success : response.data
//                   }
//                 });
                   self.showAccountsDashboard();
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
              self.onServerError(response.data);
            }
        }
        try {
            var self = this;
            self.updateLoadingForCompletePage({
              isLoading : true,
              serviceViewModels: ['accountDeleteSuccess']
            });
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.deleteExternalAccount", accountObject, deleteExternalAccountCompletionCallback.bind(this)));
        } catch (error) {}
    };

    Accounts_PresentationController.prototype.showExternalBankList = function() {
        CommonUtilities.showProgressBar(this.view);
		kony.print("----Start: Accounts_PresentationController.prototype.showExternalBankList----");
        var self = this;
        function completionCallback(response) {
            if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
                response.data.map(function(x) {
                    x.BankName = {
                        text: x.BankName,
                        tooltip: x.BankName
                    };
                });
                self.presentUserInterface("frmAccountsLanding", {"externalBankList": response.data});
            } else if(response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.presentUserInterface("frmAccountsLanding", {"externalBankList": []});
            }
        }
        try {
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getExternalBankList", {}, completionCallback.bind(this)));
        } catch(err) {
            kony.print("----Accounts_PresentationController.prototype.showExternalBankList: In catch- " + JSON.stringify(err) + "----");
        }
        kony.print("----End: Accounts_PresentationController.prototype.showExternalBankList----");
    };
    /**
     * showAddExternalBankAccount : Entry point for Account(Add External Bank Accounts) form
     * @member of {Accounts_PresentationController}
     * @param {}
     * @returns {}
     * @throws {} 
     */
    Accounts_PresentationController.prototype.fetchExternalBankAccounts = function(bankObj) {
      var self = this;
      var viewModel = {};
      
      function fetchExternalBankAccountsSuccessCallBack(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          kony.print(JSON.stringify(response.data));
          viewModel.externalBankAccountsModel = self.processOtherBankAccountsData(response.data,bankObj.userName);
          self.presentAccountsDashboard(viewModel);
        } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
          self.onServerError(response.data);
        }        
      }
      try{
        var bankCredentials = {
                mainUser : bankObj.mainUser,
                userName : bankObj.userName,
                bankId : bankObj.bankId
        };
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getExternalBankAccounts", bankCredentials, fetchExternalBankAccountsSuccessCallBack));
      } catch (error) {
        
      }
    };
     
     Accounts_PresentationController.prototype.addExternalBankAccounts = function(selectedData){
       var self = this;
       var viewModel = {};
       function addExternalBankAccountsSuccessCallBack(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          kony.print("---------Added accounts successfully----------\n"+JSON.stringify(response.data)+"\n---------Added accounts successfully----------\n");
          viewModel.savedExteranlAccountsModel = response.data;
          self.presentAccountsDashboard(viewModel);
        } else if (response.status == kony.mvc.constants.STATUS_FAILURE) {
            viewModel.addExternalBankAccountsFailure = true;
            self.presentUserInterface("frmAccountsLanding",viewModel);
        }  
       }
        try {
            var bankParameters = this.prepareInputConfigForSavingEXternalBankAccounts(selectedData);
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.addExternalBankAccounts", bankParameters, addExternalBankAccountsSuccessCallBack));
        } catch (err) {
        }       
     };
      Accounts_PresentationController.prototype.prepareInputConfigForSavingEXternalBankAccounts = function(selectedData){
            var accData = {};
            accData.AccountName = "";
            accData.main_user = "";
            accData.bank_id = "";
            accData.username = "";
            accData.CurrencyCode = "";
            accData.AvailableBalance = "";
            accData.Number = "";
            accData.Type_id = "";
            accData.AccountHolder = "";
            var mainUser = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
            for (var i in selectedData) {
                accData.AccountName = accData.AccountName + selectedData[i].AccountName + ",";
                accData.main_user = accData.main_user + mainUser + ",";
                accData.bank_id = accData.bank_id + selectedData[i].BankId + ",";
                accData.CurrencyCode = accData.CurrencyCode + selectedData[i].CurrencyCode + ",";
                accData.AvailableBalance = accData.AvailableBalance + selectedData[i].AvailableBalance + ",";
                accData.Number = accData.Number + selectedData[i].AccountNumber + ",";
                accData.Type_id = accData.Type_id + selectedData[i].TypeId + ",";
                accData.AccountHolder = accData.AccountHolder + selectedData[i].AccountHolder + ",";
                accData.username = accData.username + selectedData[i].UserName + ",";
            }
            accData.loop_count = (++i).toString();
            accData.AccountName = accData.AccountName.slice(0, -1);
            accData.main_user = accData.main_user.slice(0, -1);
            accData.bank_id = accData.bank_id.slice(0, -1);
            accData.username = accData.username.slice(0, -1);
            accData.CurrencyCode = accData.CurrencyCode.slice(0, -1);
            accData.AvailableBalance = accData.AvailableBalance.slice(0, -1);
            accData.Number = accData.Number.slice(0, -1);
            accData.Type_id = accData.Type_id.slice(0, -1);
            accData.AccountHolder = accData.AccountHolder.slice(0, -1);
        
            return accData;
      }; 
  
    Accounts_PresentationController.prototype.processOtherBankAccountsData = function(data,userName) {
        var secData = [];
        var scopeObject = this;
        for (var i = 0; i < data.length; i++) {
            secData[i] = {};
            secData[i].AccountName = data[i].AccountName;
            //secData[i].checkImage= "checkboxtick.png";
            secData[i].AccountType = data[i].TypeDescription;

            secData[i].AvailableBalanceLabel = kony.i18n.getLocalizedString("i18n.accountDetail.availableBalance");        
            var availableBalance = parseFloat(data[i].AvailableBalance).toFixed(2);
            secData[i].AvailableBalanceWithCurrency = scopeObject.formatCurrency(availableBalance);
            secData[i].AvailableBalance = availableBalance;
            secData[i].CurrencyCode = data[i].CurrencyCode;
            secData[i].Number = data[i].Number;
            secData[i].bank_id = data[i].Bank_id;
            secData[i].Type_id = data[i].Type_id;
            secData[i].AccountHolder = data[i].AccountHolder;
            secData[i].UserName = userName;
        }
        return secData;
    };

    /**
	 * fetchSingleAccountDetails : Method to fetch details of a single external account
     * @member of {Accounts_PresentationController}
     * @param {}
     * @returns {}
     * @throws {}  
     */
    Accounts_PresentationController.prototype.fetchSingleAccountDetails = function(mainUser, userName,bankId, accountName) {
       var self = this;
       var viewModel = {};
       function getSingleAccountDetailsCallBack(response) {
         if (response.status === kony.mvc.constants.STATUS_SUCCESS) {         
           self.showAccountsDashboard();
         } else if (response.status === kony.mvc.constants.STASTATUS_FAILURETUS_FAILURE) {
           self.onServerError(response.data);
         }  
       }
        try {
            var accountParameters = {
              mainUser : mainUser,
              userName : userName,
              bankId : bankId,
              accountName : accountName
            };
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getSingleAccountDetails", accountParameters, getSingleAccountDetailsCallBack));
        } catch (err) {
        }
    };		
    
    /**
     * getOutageMessage : Method to get outage message
     * @member of {Accounts_PresentationController}
     * @param {}
     * @returns {}
     * @throws {}  
     */
    Accounts_PresentationController.prototype.getOutageMessage = function() {
        var outageMessages = kony.mvc.MDAApplication.getSharedInstance().appContext.outageMessages;
        if(outageMessages && outageMessages.length > 0) {
            return outageMessages.join("\n");
        }
        return "";
    };

    /**
     * showOutageMessage : Method Show outage message
     * @member of {Accounts_PresentationController}
     * @param {}
     * @returns {}
     * @throws {}  
     */
    Accounts_PresentationController.prototype.showOutageMessage = function() {
        var scopeObj = this;
        var outageMessage = scopeObj.getOutageMessage();
        scopeObj.asyncPresentAccountsDashboard({
            "outageMessage" : {
                "show" : false
            }
        });
        if(!CommonUtilities.isEmptyString(outageMessage)) {
            scopeObj.asyncPresentAccountsDashboard({
                "outageMessage" : {
                    "show" : true,
                    "message" : outageMessage
                }
            });
        }
    };

    Accounts_PresentationController.prototype.downloadTransactionFile = function (param) {
        var requestParam = {};
        if(param.isSearchParam && this.searchParams){
            requestParam = this.searchParams;
        }
        requestParam.fromDate = param.fromDate;
        requestParam.toDate = param.toDate;
        requestParam.format = param.format;    
        requestParam.accountID = this.loadedAccountID;
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.showDownloadStatementScreen", requestParam, this.completionCallback.bind(this)));
    }

    Accounts_PresentationController.prototype.completionCallback = function (commandResponse) {
        if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
            if(commandResponse.data[0] && commandResponse.data[0] && commandResponse.data[0].StatementLink){
                var fileUrl = commandResponse.data[0].StatementLink;
                this.presentAccountDetails({transactionDownloadFile: fileUrl});
            }else{
                this.onServerError(commandResponse.data);
            }
        } else {
            this.onServerError(true);
        }
    }
    
    return Accounts_PresentationController;
    
});