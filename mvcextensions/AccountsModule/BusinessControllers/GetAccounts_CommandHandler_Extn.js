define({

    addBefore:function(){

    },

    addAfter:function(){
        
    },
  	
   execute: function(command){
        var  self  =  this;

        /**
         * Call back function for getAll service.
         * @params  : status message, data, error message.
         */
        function  getAllCompletionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try  {
          
          var loggedinUser = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
          if (loggedinUser === "jane.doe"){
            kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU = "cwarde46";
          }else if (loggedinUser === "john.doe"){
            kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU = "ADIVERDB";
          }else if (loggedinUser === "john.bailey"){
            kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU = "rgraver1";
          }else if (loggedinUser === "konyolbuser"){
            kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU = "bcudby3";
          }else if (loggedinUser === "SFFCU002"){
            kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU = "agoadbie5";
          }
          var  accountModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
          accountModel.customVerb('getAccountsPostLogin',{"username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU},getAllCompletionCallback)
        }  catch  (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    }

});