define([], function() {

    function Accounts_getSingleAccountDetails_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Accounts_getSingleAccountDetails_CommandHandler, kony.mvc.Business.CommandHandler);

    Accounts_getSingleAccountDetails_CommandHandler.prototype.execute = function(command) {
        var  self  =  this;
        /**
         * Call back function for getSingleAccountDetails service.
         * @params  : status message, data, error message.
         */
        function getSingleAccontDetailsCallback(status,  data,  error) {
            self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try {
            var params;
            var singleAccountDetailsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SingleAccountDetails");
            var criteria = kony.mvc.Expression.and(
                kony.mvc.Expression.eq("main_user", command.context.mainUser),
                kony.mvc.Expression.eq("username", command.context.userName),
                kony.mvc.Expression.eq("bank_id", command.context.bankId),
                kony.mvc.Expression.eq("account", command.context.accountName)
            );
            singleAccountDetailsModel.getByCriteria(criteria, getSingleAccontDetailsCallback);
        } catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }

    };

    Accounts_getSingleAccountDetails_CommandHandler.prototype.validate = function() {

    };

    return Accounts_getSingleAccountDetails_CommandHandler;

});