define([], function () {

  function Accounts_fetchBankDetails_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Accounts_fetchBankDetails_CommandHandler, kony.mvc.Business.CommandHandler);

  Accounts_fetchBankDetails_CommandHandler.prototype.execute = function (command) {
    var  self  =  this;
    /**
     * Call back function for getAll service.
     * @params  : status message, data, error message.
     */
    function  getAllCompletionCallback(status,  data,  error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
    }

    try  {
      var  accountModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
      accountModel.customVerb('fetchBankDetails', command.context, getAllCompletionCallback);
    } 
    catch  (error) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  Accounts_fetchBankDetails_CommandHandler.prototype.validate = function () {

  };

  return Accounts_fetchBankDetails_CommandHandler;

});