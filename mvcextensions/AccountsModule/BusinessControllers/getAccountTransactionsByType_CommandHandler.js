define(['CommonUtilitiesOCCU'], function(CommonUtilitiesOCCU) {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;
  
    function Accounts_getAccountTransactionsByType_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Accounts_getAccountTransactionsByType_CommandHandler, CommandHandler);
  
    Accounts_getAccountTransactionsByType_CommandHandler.prototype.execute = function(command){
    /*This command expects four parameters. 
        accountID - Integer.
        transactionType - One among {"All", "Transfers", "Deposits", "Checks", "Withdrawals",
         "Purchases", "Payments", "Interest"} **Case-sensitive**
         isScheduled - String - "true", "false"
        offset, limit - Integers.
    */
    var self = this;  
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var params = {
            "accountNumber": command.context.accountID,
          	"productId": command.context.account.shareId,
          	"productType": CommonUtilitiesOCCU.getAccountTypeKey(command.context.account.accountType),
          	"cursor": "",
            //"transactionType": command.context.transactionType,
            //"isScheduled": command.context.isScheduled,
            //"offset": command.context.offset,
            //"limit": command.context.limit,
            //"sortBy":  command.context.sortBy,
            //"order":  command.context.order
        };

        function completionCallback(status, response, err) {
            if(status==kony.mvc.constants.STATUS_SUCCESS)
              self.sendResponse(command, status, response);
            else
              self.sendResponse(command, status, err);
        }
        TransactionsModel.customVerb("getAccountTransactionsByType", params, completionCallback);
    };
  
  Accounts_getAccountTransactionsByType_CommandHandler.prototype.validate = function(){
    
    };
    
    return Accounts_getAccountTransactionsByType_CommandHandler;
    
});