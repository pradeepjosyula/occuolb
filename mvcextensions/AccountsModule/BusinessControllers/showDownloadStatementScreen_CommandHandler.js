define([], function() {

  	function Accounts_showDownloadStatementScreen_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_showDownloadStatementScreen_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_showDownloadStatementScreen_CommandHandler.prototype.execute = function(command){
		var self = this;

       function completionCallBack(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var showDownloadStatement = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("AccountStatement");       
           showDownloadStatement.customVerb("showDownloadStatements",command.context,completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }	
		
		
		
    };
	
	Accounts_showDownloadStatementScreen_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_showDownloadStatementScreen_CommandHandler;
    
});