define(['CommonUtilitiesOCCU'], function(CommonUtilitiesOCCU) {
   
    var CommandHandler = kony.mvc.Business.CommandHandler;
   var Command = kony.mvc.Business.Command;
   var CommandResponse = kony.mvc.Business.CommandResponse;
  
    function Accounts_advancedSearch_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Accounts_advancedSearch_CommandHandler, CommandHandler);
  
    Accounts_advancedSearch_CommandHandler.prototype.execute = function(command){
   
    	var self = this;  
        var searchModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var params = {
          	
            "accountNumber": command.context.accountID,
          	"transactionType":command.context.searchTransactionType,
          	"productId":command.context.account.shareId,
          "productType": CommonUtilitiesOCCU.getAccountTypeKey(command.context.account.accountType),
           // "":command.context.searchDescription,
           "lowAmount" :command.context.searchMinAmount,
           "highAmount" :command.context.searchMaxAmount,
           "startDate" :command.context.searchStartDate,
            "endDate":command.context.searchEndDate,
          "lowCheck" :command.context.fromCheckNumber,
           "highCheck":command.context.toCheckNumber,
          "cursor":command.context.cursor

        };

        function completionCallback(status, response, err) {
            if(status==kony.mvc.constants.STATUS_SUCCESS)
              self.sendResponse(command, status, response);
            else
              self.sendResponse(command, status, err);
        }
        searchModel.customVerb("advancedSearch", params, completionCallback);
    };
  
  Accounts_advancedSearch_CommandHandler.prototype.validate = function(){
    
    };
    
    return Accounts_advancedSearch_CommandHandler;

      
   
});