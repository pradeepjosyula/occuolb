define([], function() {

  	function Accounts_cancelTransaction_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_cancelTransaction_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_cancelTransaction_CommandHandler.prototype.execute = function(command){
		  var self = this;
      var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      function onCompletionCallback(status,response,error){
        if(status === kony.mvc.constants.STATUS_SUCCESS){
            self.sendResponse(command,status,error);
        }
         else{
             self.sendResponse(command,status,error);
         } 
      }
      TransactionModel.removeById(command.context.transactionId, onCompletionCallback);
    };
	
  	Accounts_cancelTransaction_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_cancelTransaction_CommandHandler;
    
});