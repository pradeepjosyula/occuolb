define([], function() {

  	function Accounts_getScheduledAccountTransactions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_getScheduledAccountTransactions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_getScheduledAccountTransactions_CommandHandler.prototype.execute = function(command) {
        var self = this;
        var accountNumber = command.context.accountNumber;
        var offset = command.context.offset;
        var limit = command.context.limit;
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var criteria = kony.mvc.Expression.and(
            kony.mvc.Expression.eq("accountNumber", accountNumber),
            kony.mvc.Expression.eq("searchType", "Search"),
            kony.mvc.Expression.eq("searchTransactionType", "Both"),
            kony.mvc.Expression.eq("firstRecordNumber", offset),
            kony.mvc.Expression.eq("lastRecordNumber", limit)
        
        ); 

        function filterTransactions(records) {
            var scheduledTransactions = [];
            records.forEach(function(record) {
                if (record['isScheduled'] == 'true') scheduledTransactions.push(record);
            });
            return scheduledTransactions;
        }

        function onScheduledTransactionsCompletionCallBack(status, response, error) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                var ScheduledTransactionsResponse = filterTransactions(response);
                self.sendResponse(command, status, ScheduledTransactionsResponse);
            } else {
                self.sendResponse(command, status, error);
            }
        }
        TransactionsModel.getByCriteria(criteria, onScheduledTransactionsCompletionCallBack);
    };
	
	Accounts_getScheduledAccountTransactions_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_getScheduledAccountTransactions_CommandHandler;
    
});