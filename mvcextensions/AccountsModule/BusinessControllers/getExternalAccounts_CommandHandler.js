define([], function() {

  	function Accounts_getExternalAccounts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_getExternalAccounts_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_getExternalAccounts_CommandHandler.prototype.execute = function(command){
      var self = this;

      function getExternalCompletionCallback(status, data, error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
      }

      try {
        var extAccountModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccounts");
        extAccountModel.getAll(getExternalCompletionCallback);
      } catch (error) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
      }
    };
	
	Accounts_getExternalAccounts_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_getExternalAccounts_CommandHandler;
    
});