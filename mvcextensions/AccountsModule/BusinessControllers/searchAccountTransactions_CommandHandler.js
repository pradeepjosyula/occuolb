define([], function () {

    var CommandHandler = kony.mvc.Business.CommandHandler;
    var Command = kony.mvc.Business.Command;
    var CommandResponse = kony.mvc.Business.CommandResponse;

    /**
     * Command Context Params
     * 1.searchTransactionType
     * 2.searchDescription
     * 3.searchMinAmount
     * 4.searchMaxAmount
     * 5.searchStartDate
     * 6.searchEndDate
     * 7.fromCheckNumber
     * 8.toCheckNumber
     * 9.accountNumber
     */

    function Accounts_searchAccountTransactions_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }


    function sanitizeContext(context) {
        for (var key in context) {
            if (context.hasOwnProperty(key)) {
                if (context[key] === null  || context[key] === undefined) {
                    context[key] === "";
                }
            }
        }
    }

    function getSearchExpressionFromContext (context) {

        sanitizeContext(context);

        var expr= kony.mvc.Expression.and(kony.mvc.Expression.eq("searchTransactionType",context.searchTransactionType),
                                        kony.mvc.Expression.eq("searchDescription",context.searchDescription),
                                        kony.mvc.Expression.eq("searchMinAmount",context.searchMinAmount),
                                        kony.mvc.Expression.eq("searchMaxAmount",context.searchMaxAmount),
                                        kony.mvc.Expression.eq("searchStartDate",context.searchStartDate),
                                        kony.mvc.Expression.eq("searchEndDate",context.searchEndDate),
                                        kony.mvc.Expression.eq("fromCheckNumber",context.fromCheckNumber),
                                        kony.mvc.Expression.eq("toCheckNumber",context.toCheckNumber),
                                        kony.mvc.Expression.eq("accountNumber",context.accountNumber),                                                                              
                                        kony.mvc.Expression.eq("searchType","Search"),
                                        kony.mvc.Expression.eq("firstRecordNumber","0"),    
                                        kony.mvc.Expression.eq("lastRecordNumber","10")                                                                            
                                        ); 

        return expr;

    }

    inheritsFrom(Accounts_searchAccountTransactions_CommandHandler, CommandHandler);

    Accounts_searchAccountTransactions_CommandHandler.prototype.execute = function (command) {
        var self = this;
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");

        function completionCallback(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS)
                self.sendResponse(command, status, response);
            else
                self.sendResponse(command, status, err);
        }
        var expr = getSearchExpressionFromContext(command.context);
        TransactionsModel.getByCriteria(expr, completionCallback);
    };

    Accounts_searchAccountTransactions_CommandHandler.prototype.validate = function () {

    };

    return Accounts_searchAccountTransactions_CommandHandler;

});