define([], function() {

    function Accounts_updateExternalAccountFavouriteStatus_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Accounts_updateExternalAccountFavouriteStatus_CommandHandler, kony.mvc.Business.CommandHandler);

    Accounts_updateExternalAccountFavouriteStatus_CommandHandler.prototype.execute = function(command) {
        function partialUpdateCompletionCallback(status, response, error) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, response);
            } else {
                self.sendResponse(command, status, error);
            }
        }

        try {
            kony.print("#### start Accounts_updateFavouriteStatus_CommandHandler : execute ####");
            var self = this;
            var selectedAccountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SelectedAccounts");
            var selectedAccountsObject;
            if (!(command.context && command.context !== undefined && typeof command.context === 'object' && command.context.Account_id && command.context.FavouriteStatus)) {
                throw "Invalid data: record is null, undefined or not an object or composite keys are missing";
            }
            selectedAccountsObject = new selectedAccountsModel(command.context);
            selectedAccountsObject.partialUpdate(partialUpdateCompletionCallback);
        } catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };

    Accounts_updateExternalAccountFavouriteStatus_CommandHandler.prototype.validate = function() {

    };

    return Accounts_updateExternalAccountFavouriteStatus_CommandHandler;

});