define([], function() {

  	function Accounts_getExternalAccountTransactions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_getExternalAccountTransactions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_getExternalAccountTransactions_CommandHandler.prototype.execute = function(command){
		
        var self = this;
        var accountNumber = command.context.accountNumber;
        var offset = command.context.offset;
        var limit = command.context.limit;
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var params = {
            "accountID": accountNumber,
            "firstRecordNumber": offset,
            "lastRecordNumber": limit
        };

        function completionCallback(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) 
                self.sendResponse(command, status, response);
            else 
                self.sendResponse(command, status, err);
        }
        TransactionsModel.customVerb("getExternalAccountTransactions", params, completionCallback);
    };
	
	Accounts_getExternalAccountTransactions_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_getExternalAccountTransactions_CommandHandler;
    
});