define([], function() {

    function ExternalAccounts_getRefreshAccountsFromDB_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(ExternalAccounts_getRefreshAccountsFromDB_CommandHandler, kony.mvc.Business.CommandHandler);

    ExternalAccounts_getRefreshAccountsFromDB_CommandHandler.prototype.execute = function(command) {
        var  self  =  this;
        var mainUser = command.context.mainUser;
        var bankId = command.context.bankId;

        /**
         * Call back function for getByMainUser service.
         * @params  : status message, data, error message.
         */
        function completionCallback(status,  data,  error) {
            self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try  {
            var  refreshAccountsFromDBModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("RefreshAccountsFromDB");
            var criteria;
            if(bankId && String(bankId).trim() !== "") {
                criteria = kony.mvc.Expression.and(
                    kony.mvc.Expression.eq("main_user", mainUser),
                    kony.mvc.Expression.eq("bank_id", bankId)
                );
            } else {
                criteria = kony.mvc.Expression.eq("main_user", mainUser);
            }
            
            refreshAccountsFromDBModel.getByCriteria(criteria, completionCallback);
        } 
        catch  (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }

    };

    ExternalAccounts_getRefreshAccountsFromDB_CommandHandler.prototype.validate = function() {

    };

    return ExternalAccounts_getRefreshAccountsFromDB_CommandHandler;

});