define([], function () {
  
  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;

  function Accounts_stopCheck_CommandHandler(commandId) {
    CommandHandler.call(this, commandId);
  }

  inheritsFrom(Accounts_stopCheck_CommandHandler, CommandHandler);

  Accounts_stopCheck_CommandHandler.prototype.execute = function(command){
    
    var self = this;
    var CheckModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Check");
    var params = {
      "accountNumber": command.context.accountNumber,
      "productId": command.context.productId,
      "checkNumber": command.context.checkNumber,
      "productType": command.context.productType,
      "checkNumberHighRange":command.context.checkNumberHighRange, 
      "payee": command.context.payee
    };

    function completionCallback(status, response, err) {
      if(status==kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, err);
    }
    CheckModel.customVerb("stopCheck", params, completionCallback);
  };

  Accounts_stopCheck_CommandHandler.prototype.validate = function(){

  };

  return Accounts_stopCheck_CommandHandler;

});