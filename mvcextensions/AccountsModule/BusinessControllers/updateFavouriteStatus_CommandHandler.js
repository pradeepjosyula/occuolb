define([], function() {

  	function Accounts_updateFavouriteStatus_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_updateFavouriteStatus_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_updateFavouriteStatus_CommandHandler.prototype.execute = function(command){
		var self = this;

        function completionCallBack(status, response, error) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, response);
            } else {
                self.sendResponse(command, status, error);
            }
        }
        var accountModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
        accountModel.customVerb("updateFavouriteStatus", {"accountID": command.context.accountID}, completionCallBack);
    };
	
	Accounts_updateFavouriteStatus_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_updateFavouriteStatus_CommandHandler;
    
});