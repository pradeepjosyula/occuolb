define([], function() {

    function Accounts_addExternalBankAccounts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Accounts_addExternalBankAccounts_CommandHandler, kony.mvc.Business.CommandHandler);

    Accounts_addExternalBankAccounts_CommandHandler.prototype.execute = function(command) {
        var  self  =  this;
        /**
         * Call back function for addExternalBankAccounts service.
         * @params  : status message, data, error message.
         */
        function saveCompletionCallback(status,  data,  error) {
            self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try {
            var externalAccountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SelectedAccounts");
            var newExternalAccount = new externalAccountsModel(command.context);
            newExternalAccount.save(saveCompletionCallback, "online");
        } catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
      
    };

    Accounts_addExternalBankAccounts_CommandHandler.prototype.validate = function() {

    };

    return Accounts_addExternalBankAccounts_CommandHandler;

});