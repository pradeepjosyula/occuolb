define([], function () {

  function Accounts_GetAccounts_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Accounts_GetAccounts_CommandHandler, kony.mvc.Business.CommandHandler);

  Accounts_GetAccounts_CommandHandler.prototype.execute = function (command) {
    var  self  =  this;

    /**
     * Call back function for getAll service.
     * @params  : status message, data, error message.
     */
    function  getAllCompletionCallback(status,  data,  error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
    }

    try  {
      var  accountModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
      accountModel.customVerb('getAccountsPostLogin', {}, getAllCompletionCallback)
    } 
    catch  (error) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  Accounts_GetAccounts_CommandHandler.prototype.validate = function () {

  };

  return Accounts_GetAccounts_CommandHandler;

});