define([], function() {

  	function Accounts_getRecentAccountTransactions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_getRecentAccountTransactions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_getRecentAccountTransactions_CommandHandler.prototype.execute = function(command) {
        var self = this;
        var accountNumber = command.context.accountNumber;
        var offset = command.context.offset;
        var limit = command.context.limit;
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var criteria = kony.mvc.Expression.and(
            kony.mvc.Expression.eq("accountNumber", accountNumber),
            kony.mvc.Expression.eq("searchType", "Search"),
            kony.mvc.Expression.eq("searchTransactionType", "Both"),
            kony.mvc.Expression.eq("firstRecordNumber", offset),
            kony.mvc.Expression.eq("lastRecordNumber", limit)
        
        ); 

        function filterTransactions(records) {
            var recentTransactions = [];
            records.forEach(function(record) {
                if (record['isScheduled'] == 'false') recentTransactions.push(record);
            });
            return recentTransactions;
        }

        function onRecentTransactionsCompletionCallBack(status, response, error) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                var recentTransactions = filterTransactions(response);
                self.sendResponse(command, status, recentTransactions);
            } else {
                self.sendResponse(command, status, error);
            }
        }
        TransactionsModel.getByCriteria(criteria, onRecentTransactionsCompletionCallBack);
    };
	
	Accounts_getRecentAccountTransactions_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_getRecentAccountTransactions_CommandHandler;
    
});