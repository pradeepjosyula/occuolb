define([], function() {

  	function Accounts_getSelectedStatement_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_getSelectedStatement_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_getSelectedStatement_CommandHandler.prototype.execute = function(command){
		var self = this;

       function completionCallBack(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var showDownloadStatement = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("AccountStatement");       
           showDownloadStatement.customVerb("getDocument",command.context,completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }	
		
		
		
    };
	
	Accounts_getSelectedStatement_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_getSelectedStatement_CommandHandler;
    
});