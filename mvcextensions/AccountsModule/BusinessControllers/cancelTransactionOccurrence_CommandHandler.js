define([], function() {

  	function Accounts_cancelTransactionOccurrence_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Accounts_cancelTransactionOccurrence_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Accounts_cancelTransactionOccurrence_CommandHandler.prototype.execute = function(command){
		var self = this;
		var transactionRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('Transactions');
      	function onCompletion(status, response, error){
        	self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? response: error);
        }
      	transactionRepo.customVerb('cancelScheduledTransactionOccurrence', {transactionId: command.context.transactionId}, onCompletion);
    };
	
	Accounts_cancelTransactionOccurrence_CommandHandler.prototype.validate = function(){
		
    };
    
    return Accounts_cancelTransactionOccurrence_CommandHandler;
    
});