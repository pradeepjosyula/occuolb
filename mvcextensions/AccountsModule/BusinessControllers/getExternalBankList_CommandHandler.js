define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;
  
    function Accounts_getExternalBankList_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Accounts_getExternalBankList_CommandHandler, CommandHandler);
  
    Accounts_getExternalBankList_CommandHandler.prototype.execute = function(command){
        kony.print("----Start: Accounts_getExternalBankList_CommandHandler.prototype.execute----");
		var self = this;
        function completionCallback(status,  data,  error) {
            self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
            var externalBanksModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalBanks");
            externalBanksModel.getAll(completionCallback);
        } catch(err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
        kony.print("----End: Accounts_getExternalBankList_CommandHandler.prototype.execute----");
    };
  
  Accounts_getExternalBankList_CommandHandler.prototype.validate = function(){
    
    };
    
    return Accounts_getExternalBankList_CommandHandler;
    
});