define([], function () {
  
   var CommandHandler = kony.mvc.Business.CommandHandler;
   var Command = kony.mvc.Business.Command;
   var CommandResponse = kony.mvc.Business.CommandResponse;
  
    function Accounts_getCheck_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Accounts_getCheck_CommandHandler, CommandHandler);
  
    Accounts_getCheck_CommandHandler.prototype.execute = function(command){
   
    	var self = this;  
        var CheckModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Check");
        var params = {
            "accountNumber": command.context.accountNumber,
          	"checkNumber": command.context.checkNumber,
          	"processDate": command.context.PaymentDate,
          	
        };

        function completionCallback(status, response, err) {
            if(status==kony.mvc.constants.STATUS_SUCCESS)
              self.sendResponse(command, status, response);
            else
              self.sendResponse(command, status, err);
        }
        CheckModel.customVerb("getCheck", params, completionCallback);
    };
  
  Accounts_getCheck_CommandHandler.prototype.validate = function(){
    
    };
    
    return Accounts_getCheck_CommandHandler;

      
    
});