define([], function() {

    function Accounts_getExternalBankAccounts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Accounts_getExternalBankAccounts_CommandHandler, kony.mvc.Business.CommandHandler);

    Accounts_getExternalBankAccounts_CommandHandler.prototype.execute = function(command) {
        var  self  =  this;
        var mainUser = command.context.mainUser;
        var bankId = command.context.bankId;
        var userName = command.context.userName;
        /**
         * Call back function for getExternalBankAccounts service.
         * @params  : status message, data, error message.
         */
        function getExternalBankAccountsCompletionCallback(status,  data,  error) {
            self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try {
            var ExternalAccountsAggregationModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccountsAggregation");
            var criteria = kony.mvc.Expression.and(
                kony.mvc.Expression.eq("main_user", mainUser),
                kony.mvc.Expression.eq("username", userName),
                kony.mvc.Expression.eq("bank_id", bankId)
            );

            ExternalAccountsAggregationModel.getByCriteria(criteria, getExternalBankAccountsCompletionCallback);
        } catch (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }

    };

    Accounts_getExternalBankAccounts_CommandHandler.prototype.validate = function() {

    };

    return Accounts_getExternalBankAccounts_CommandHandler;

});