define([], function() {

    function Accounts_deleteAccount_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Accounts_deleteAccount_CommandHandler, kony.mvc.Business.CommandHandler);

    Accounts_deleteAccount_CommandHandler.prototype.execute = function(command) {
        var self = this;
        function deleteExternalAccountCompletionCallback(status, response, error) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, response);
            } else {
                self.sendResponse(command, status, error);
            }
        }      
        try {
            var params;
            var primaryKeys;
            var selectedAccountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SelectedAccounts");
            if (command.context && (command.context !== undefined && typeof command.context === 'object')) {
                primaryKeys = {
                    "main_user": command.context.mainUser,
                    "username": command.context.userName,
                    "bank_id": command.context.bankId,
                    "AccountName": command.context.accountName,
                    "loop_count": command.context.loopCount
                };
            }

            selectedAccountsModel.removeById(primaryKeys, deleteExternalAccountCompletionCallback);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };

    Accounts_deleteAccount_CommandHandler.prototype.validate = function() {

    };

    return Accounts_deleteAccount_CommandHandler;

});