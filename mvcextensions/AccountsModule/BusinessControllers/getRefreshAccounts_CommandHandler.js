define([], function() {

    function Accounts_getRefreshAccounts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Accounts_getRefreshAccounts_CommandHandler, kony.mvc.Business.CommandHandler);

    Accounts_getRefreshAccounts_CommandHandler.prototype.execute = function(command) {
        var  self  =  this;
        var mainUser = command.context.mainUser;
        var bankId = command.context.bankId;

        /**
         * Call back function for getByMainUser service.
         * @params  : status message, data, error message.
         */
        function completionCallback(status,  data,  error) {
            self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try  {
            var  refreshAccountsFromDBModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("RefreshAccountsFromDB");
            var criteria;
            if(bankId && String(bankId).trim() !== "") {
                criteria = kony.mvc.Expression.and(
                    kony.mvc.Expression.eq("main_user", mainUser),
                    kony.mvc.Expression.eq("bank_id", bankId)
                );
            } else {
                criteria = kony.mvc.Expression.eq("main_user", mainUser);
            }
            
            refreshAccountsFromDBModel.getByCriteria(criteria, completionCallback);
        } 
        catch  (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }

    };

    Accounts_getRefreshAccounts_CommandHandler.prototype.validate = function() {

    };

    return Accounts_getRefreshAccounts_CommandHandler;

});