define([], function() {
    
          function Accounts_GetAccountById_CommandHandler(commandId) {
            kony.mvc.Business.CommandHandler.call(this, commandId);
        }
        
        inheritsFrom(Accounts_GetAccountById_CommandHandler, kony.mvc.Business.CommandHandler);
      
        Accounts_GetAccountById_CommandHandler.prototype.execute = function(command){
            var  self  =  this;
            var id = command.context;
    
            /**
             * Call back function for getById service.
             * @params  : status message, data, error message.
             */
            function  getByIdCompletionCallback(status,  data,  error) {
              self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data[0] : error);
            }
    
            try  {
              var  accountModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
              var criteria = kony.mvc.Expression.eq("accountID", id)
              accountModel.customVerb('getAccountsPostLogin',{'accountID':id},getByIdCompletionCallback)
            }  catch  (error) {
              self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
            }
        };
        
        Accounts_GetAccountById_CommandHandler.prototype.validate = function(){
            
        };
        
        return Accounts_GetAccountById_CommandHandler;
        
    });