define([], function() {
	
    function handleDateFormat (date) {
    if(date == undefined || date == null || date === ''){
      return null;
    }
    if(date instanceof Date){
      return date.getFullYear() + '-' + ((date.getMonth()+1) < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1) + '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());    
    } else {
      var dateObj  = new Date(date); 
      return dateObj.getFullYear() + '-' + ((dateObj.getMonth()+1) < 10 ? '0' + (dateObj.getMonth()+1) : dateObj.getMonth()+1) + '-' + (dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate());         
    }
  }
  	function LoanPay_payDueAmount_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(LoanPay_payDueAmount_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	LoanPay_payDueAmount_CommandHandler.prototype.execute = function(command){
		    var self = this;

    function onCreateTransfer (status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var context = command.context;
    var transactionsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var newTransaction = new transactionsModel({
      'fromAccountNumber':  context.fromAccountNumber,
      'amount':context.amount,
      'transactionsNotes': context.transactionsNotes,
      'toAccountNumber': context.toAccountNumber,
      'transactionType': context.transactionType,
      'isScheduled': context.isScheduled,
      'scheduledDate': handleDateFormat(context.scheduledDate),
      'penaltyFlag':context.penaltyFlag,
      'payoffFlag':context.payoffFlag,
     });
    newTransaction.save(onCreateTransfer);

    };
	
	LoanPay_payDueAmount_CommandHandler.prototype.validate = function(){
		
    };
    
    return LoanPay_payDueAmount_CommandHandler;
    
});