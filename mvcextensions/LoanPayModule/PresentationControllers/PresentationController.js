define(['CommonUtilities'], function (CommonUtilities) {

  function LoanPay_PresentationController() {

    kony.mvc.Presentation.BasePresenter.call(this);
    var viewModel = [];
  }

  inheritsFrom(LoanPay_PresentationController, kony.mvc.Presentation.BasePresenter);

  LoanPay_PresentationController.prototype.initializePresentationController = function () {

  };

  LoanPay_PresentationController.prototype.presentLoanPay = function (data) {
    this.presentUserInterface("frmPayDueAmount", data);

  };
  /**
  * To get frontend date string
  * @member LoanPay_PresentationController
  * @param {Date} dateString
  * @returns {Date} frontend date
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.getFormattedDateString = function (dateString) {
    return CommonUtilities.getFrontendDateString(dateString, CommonUtilities.getConfiguration("frontendDateFormat"));
  };
  /**
  * To get backend date string
  * @member LoanPay_PresentationController
  * @param {Date} dateString
  * @param {String} dateFormat
  * @returns {Date} backend date (yyyy-mm-dd) format
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.getBackendDate = function (dateString, dateFormat) {
    return CommonUtilities.getBackendDateFormat(dateString, dateFormat);
  };
  /**
 * Entry Function for Loan Due
 * @member LoanPay_PresentationController
 * @param {Object} response contains account details
 * @returns {void} - None
 * @throws {void} - None
 */
  LoanPay_PresentationController.prototype.navigateToLoanDue = function (response) {
    this.loadLoanPayComponents();
    if (response)
      viewModel = response;
    this.presentLoanPay({loanDue:viewModel});
  };
  /**
 * Entry Function for Loan Pay Off
 * @member LoanPay_PresentationController
 * @param {Object} response contains account details
 * @returns {void} - None
 * @throws {void} - None
 */
  LoanPay_PresentationController.prototype.navigateToLoanPay = function (response) {
    this.loadLoanPayComponents();
    if (response)
      viewModel = response;
    this.presentLoanPay({loanPayoff:viewModel});
  };
  LoanPay_PresentationController.prototype.loadLoanPayComponents = function () {
    this.loadComponents('frmPayDueAmount');
  };
  LoanPay_PresentationController.prototype.showView = function (frm, data) {
    this.presentUserInterface(frm, data);
  };
  /**
   * Function to load components
   * @member LoanPay_PresentationController
   * @param {object} frm
   * @returns {void} - None
   * @throws {void} - None
   */
  LoanPay_PresentationController.prototype.loadComponents = function (frm) {
    var scopeObj = this;
    var howToShowHamburgerMenu = function (sideMenuViewModel) {
      scopeObj.showView(frm, { "sideMenu": sideMenuViewModel });
    };
    scopeObj.SideMenu.init(howToShowHamburgerMenu);

    var presentTopBar = function (topBarViewModel) {
      scopeObj.showView(frm, { "topBar": topBarViewModel });
    };
    scopeObj.TopBar.init(presentTopBar);
  };
  /**
  * Function to Show Progress Bar
  * @member LoanPay_PresentationController
  * @param {void} - None 
  * @returns {void} - None
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.showProgressBar = function () {
    var self = this;
    self.presentLoanPay({
      "ProgressBar": {
        show: true
      }
    });
  };
  /**
  * Function to Hide Progress Bar
  * @member LoanPay_PresentationController
  * @param {void} - None 
  * @returns {void} - None
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.hideProgressBar = function () {
    var self = this;
    self.presentLoanPay({
      "ProgressBar": {
        show: false
      }
    });
  };
  /**
  * Function to create transfer for Loan Pay Off and Loan Pay Due Amount
  * @member LoanPay_PresentationController
  * @param {Object} data 
  * @param {String} context stores context
  * @returns {void} - None
  * @throws {void} - None
  */
  LoanPay_PresentationController.prototype.payLoanOff = function (data, context) {
    var self = this;
    self.showProgressBar();
    var info = {};
    var commandData = {
      fromAccountNumber: data.fromAccountID,
      amount: data.amount,
      transactionsNotes: data.notes,
      isScheduled: data.isScheduled ? data.isScheduled : "false",
      transactionType: "Loan",
      toAccountNumber: data.toAccountID,
      scheduledDate: data.date,
      penaltyFlag: data.penaltyFlag ? data.penaltyFlag : "false",
      payoffFlag: data.payoffFlag ? data.payoffFlag : "false"

    };
    function paymentCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.hideProgressBar();
        var responseData = {
          "data": data,
          "referenceId": response.data.referenceId
        };
        if(context=== "payOtherAmount"){
          self.presentLoanPay({ payOtherAmount : responseData});
        } else if(context=== "payCompleteDue"){
          self.presentLoanPay({ payCompleteDue : responseData});
        } else if(context=== "payCompleteMonthlyDue"){
          self.presentLoanPay({ payCompleteMonthlyDue : responseData});
        }
      } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
        self.presentLoanPay({"serverError":response.data.errmsg}); 
        self.hideProgressBar();
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createTransfer", commandData, paymentCallback));
  };
  /**
    * Function to Navigate to Accounts module
    * @member LoanPay_PresentationController
    * @param {void} - None
    * @returns {void} - None
    * @throws {void} - None
    */
  LoanPay_PresentationController.prototype.backToAccount = function () {
    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    accountsModule.presentationController.showAccountsDashboard();
  };
  /**
    * Function to navigate to account details module
    * @member LoanPay_PresentationController
    * @param {Object} account
    * @returns {void} - None
    * @throws {void} - None
    */
  LoanPay_PresentationController.prototype.backToAccountDetails = function (account) {
    var accountDetails = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    accountDetails.presentationController.showAccountDetails(account);
  };
  /**
    * Function to fetch updated account object
    * @member LoanPay_PresentationController
    * @param {Object} accountID
    * @param {String} context stores context
    * @returns {void} - None
    * @throws {void} - None
    */
  LoanPay_PresentationController.prototype.fetchUpdatedAccountDetails = function (accountID, context) {
    var self = this;
    function getAccountCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        if(context=== "newAccountSelection"){
          self.presentLoanPay({ newAccountSelection : response.data});
        } else if(context=== "navigationToAccountDetails"){
          self.presentLoanPay({ navigationToAccountDetails : response.data});
        } else if(context=== "validateData"){
          self.presentLoanPay({ validateData : response.data});
        } else if(context=== "updateFromAccount"){
          self.presentLoanPay({ updateFromAccount : response.data});
        } else if(context=== "updateToAccount"){
          self.presentLoanPay({ updateToAccount : response.data});
        } else if(context=== "populateAccountData"){
          self.presentLoanPay({ populateAccountData : response.data});
        }
      } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
        self.hideProgressBar();
        CommonUtilities.showServerDownScreen();        
      }
    }
    var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccountById", accountID, getAccountCallback));
  };

  LoanPay_PresentationController.prototype.fetchCheckingAccounts = function () {
    var self = this;

    function getAccountsCompletionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {        
        self.presentLoanPay({"loadAccounts":response.data});
        self.hideProgressBar();
      } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
        self.hideProgressBar();
        CommonUtilities.showServerDownScreen();        
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.loans.getAccounts", {}, getAccountsCompletionCallback));

  };
  return LoanPay_PresentationController;
});