define([], function() {

  	function LocateUs_getBranchOrATMDetails_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(LocateUs_getBranchOrATMDetails_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	LocateUs_getBranchOrATMDetails_CommandHandler.prototype.execute = function(command){
        var self = this;
        var params = {
            "type": command.context.type,
            "placeID": command.context.placeID
        };

        function getLocationDetailsCompletionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
            var locationsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Locations");
            locationsModel.getLocationDetails(params, getLocationDetailsCompletionCallback);
        } catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
	
	  LocateUs_getBranchOrATMDetails_CommandHandler.prototype.validate = function(){
		
    };
    
    return LocateUs_getBranchOrATMDetails_CommandHandler;
    
});