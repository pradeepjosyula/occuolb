define([], function() {
    function LocateUs_getBranchOrATMLocations_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
    inheritsFrom(LocateUs_getBranchOrATMLocations_CommandHandler, kony.mvc.Business.CommandHandler);
    LocateUs_getBranchOrATMLocations_CommandHandler.prototype.execute = function(command) {
        var self = this;

        var params = {
            "currLatitude": command.context.latitude,
            "currLongitude": command.context.longitude
        };

        function getLocationsCompletionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try {
            var locationsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Locations");
            locationsModel.customVerb("getLocationList", params, getLocationsCompletionCallback);


        }
        catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
    LocateUs_getBranchOrATMLocations_CommandHandler.prototype.validate = function() {

    };
    return LocateUs_getBranchOrATMLocations_CommandHandler;
});