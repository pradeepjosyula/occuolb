define({
   
    addBefore:function(){

    },

    addAfter:function(){
        
    },
    
    execute:function(command){
      var self=this;
      var params = {
      "currLatitude": command.context.latitude,
      "currLongitude": command.context.longitude
    };
  		function getLocationsCompletionCallback(status,data,error){
     		self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    	}
      try{
      var TransfersModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Locations");
        TransfersModel.customVerb("getLocations", params, getLocationsCompletionCallback);  
      }catch(err){
        kony.print(err);
      }
    }
});