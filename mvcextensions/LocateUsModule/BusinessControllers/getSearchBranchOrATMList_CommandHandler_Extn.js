define({

  addBefore:function(){
    
  },

  addAfter:function(){
    
  },

  execute: function(command){
    var self = this;

    var params = {
      "addressLine1": command.context.query
    };
    function getLocationsCompletionCallback(status, data, error) {
      self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
    }
    try {
      var locationsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Locations");
      locationsModel.customVerb("getLocations", params, getLocationsCompletionCallback);
    }catch (error) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  }

});