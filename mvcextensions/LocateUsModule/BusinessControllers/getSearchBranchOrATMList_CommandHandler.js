define([], function() {
    function LocateUs_getSearchBranchOrATMList_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
    inheritsFrom(LocateUs_getSearchBranchOrATMList_CommandHandler, kony.mvc.Business.CommandHandler);
    LocateUs_getSearchBranchOrATMList_CommandHandler.prototype.execute = function(command) {
        var self = this;

        var params = {
            "query": command.context.query
        };

        function getLocationsCompletionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try {
            var locationsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Locations");
            locationsModel.customVerb("getLocationsQuery", params, getLocationsCompletionCallback);


        }
        catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
    LocateUs_getSearchBranchOrATMList_CommandHandler.prototype.validate = function() {

    };
    return LocateUs_getSearchBranchOrATMList_CommandHandler;
});