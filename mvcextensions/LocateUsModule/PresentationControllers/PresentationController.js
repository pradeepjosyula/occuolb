define([], function () {
    var MDABasePresenter = kony.mvc.Presentation.BasePresenter;
    var BusinessController = kony.mvc.Business.Controller;
    var MDAFormController = kony.mvc.MDAFormController;
    function LocateUs_PresentationController() {
        MDABasePresenter.call(this);
        this.locateUsSearchModel = {
            "searchResult": null,
        };
    }

    inheritsFrom(LocateUs_PresentationController, MDABasePresenter);
    LocateUs_PresentationController.prototype.initializePresentationController = function () { };

    /**
     * presentLocateUs : Method for navigate to LocateUs form
     * @member LocateUs_PresentationController
     * @param {object} - viewModel to be sent to the form
     * @returns {}
     * @throws {}
     */
    LocateUs_PresentationController.prototype.presentLocateUs = function (data) {
        this.presentUserInterface("frmLocateUs", data);
    };

    LocateUs_PresentationController.prototype.loadLocateUsComponents = function () {
        this.showProgressBar();
        this.loadComponents('frmLocateUs');
    };

    /**
     * loadComponents : Load the Side Menu and Top Menu
     * @member LocateUs_PresentationController
     * @param {String} - Form name
     * @returns {}
     * @throws {}
     */
    LocateUs_PresentationController.prototype.loadComponents = function (frm) {
        var scopeObj = this;
        var howToShowHamburgerMenu = function (sideMenuViewModel) {
            scopeObj.presentLocateUs({ "sideMenu": sideMenuViewModel });
        };

        scopeObj.SideMenu.init(howToShowHamburgerMenu);

        var presentTopBar = function (topBarViewModel) {
            scopeObj.presentLocateUs({ "topBar": topBarViewModel });
        };
        scopeObj.TopBar.init(presentTopBar);
    };

    /**
     * showProgressBar : Method to show the progress bar
     * @member LocateUs_PresentationController
     * @param {}
     * @returns {}
     * @throws {}
     */
    LocateUs_PresentationController.prototype.showProgressBar = function () {
        var self = this;
        self.presentLocateUs({
            "ProgressBar": {
                show: true
            }
        });
    };

    /**
     * hideProgressBar : Method to hide the progress bar
     * @member LocateUs_PresentationController
     * @param {}
     * @returns {}
     * @throws {}
     */
    LocateUs_PresentationController.prototype.hideProgressBar = function () {
        var self = this;
        self.presentLocateUs({
            "ProgressBar": {
                show: false
            }
        });
    };

    /**
     * showLocateUsPage : Entry Function for Locate Us page
     * @member LocateUs_PresentationController
     * @param {}
     * @returns {}
     * @throws {}
     */
    LocateUs_PresentationController.prototype.showLocateUsPage = function () {
        var self = this;
        if (!navigator.geolocation) {
            self.presentLocateUs({ "geoLocationError": "geoLocationError" });
            return;
        } else {
            if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
                this.presentLocateUs({ "preLoginView": "preLoginView" });
            } else {
                this.loadLocateUsComponents();
                this.presentLocateUs({ "postLoginView": "postLoginView" });
            }
        }
    };

    /**
     * getBranchOrATMList : Get the list of Atms and Branches based on the current location
     * @member LocateUs_PresentationController
     * @param {}
     * @returns {}
     * @throws {}
     */
    LocateUs_PresentationController.prototype.getBranchOrATMList = function () {
        var self = this;
        self.showProgressBar();
        var param = null;

        if (!navigator.geolocation) {
            self.presentLocateUs({ "geoLocationError": "geoLocationError" });
            return;
        } else {
            /*
            var coords = {
                latitude: "17.447931",
                longitude: "78.371316"
            };
            var position = {
                coords
            };
            geoSuccess(position);
            */
            navigator.geolocation.getCurrentPosition(geoSuccess, geoFailure);
        }

        function geoSuccess(position) {
            self.globalLat = position.coords.latitude;
            self.globalLon = position.coords.longitude;
            param = {
                "latitude": position.coords.latitude,
                "longitude": position.coords.longitude
            };
            function GetBranchOrATMListCompletionCallback(response) {
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                    if (Array.isArray(response.data)) {
                        var viewModal = response.data;
                        self.presentLocateUs({ "getBranchOrATMListSuccess": viewModal });
                    }
                    else {
                        self.presentLocateUs({ "getBranchOrATMListFailure": "getBranchOrATMListFailure" });
                    }
                } else {
                    self.presentLocateUs({ "getBranchOrATMListFailure": "getBranchOrATMListFailure" });
                }
            }

            self.businessController.execute(new kony.mvc.Business.Command("com.kony.locateUs.getBranchOrATMLocations", param, GetBranchOrATMListCompletionCallback));
        }
        function geoFailure() {
            self.presentLocateUs({ "geoLocationError": "geoLocationError" });
            return;
        }
    };

    /**
     * getSearchBranchOrATMList : Method to get the list of ATMs and Branches based on the search criteria
     * @member LocateUs_PresentationController
     * @param {String} - queryParams contains the search criteria
     * @returns {}
     * @throws {}
     */
    LocateUs_PresentationController.prototype.getSearchBranchOrATMList = function (queryParams) {
        var self = this;
        self.showProgressBar();
        function completionSearchCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                if (Array.isArray(response.data)) {
                    var viewModal = response.data;
                    self.presentLocateUs({ "getSearchBranchOrATMListSuccess": viewModal });
                }
                else {
                    self.presentLocateUs({ "getSearchBranchOrATMListFailure": "getSearchBranchOrATMListFailure" });
                }
            } else {
                self.presentLocateUs({ "getSearchBranchOrATMListFailure": "getSearchBranchOrATMListFailure" });
            }
        }
        var param = {
            "query": queryParams.query
        };
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.locateUs.getSearchBranchOrATMList", param, completionSearchCallback));
    };

    /**
     * getAtmorBranchDetails : Method to get the Details of the ATM or Branch based on the Location Id
     * @member LocateUs_PresentationController
     * @param {Object} - contains the Type and Location Id 
     * @returns {}
     * @throws {}
     */
    LocateUs_PresentationController.prototype.getAtmorBranchDetails = function (detailsJSON) {
        var self = this;
        var params = {
            "type": detailsJSON.type,
            "placeID": detailsJSON.placeID
        };
        function GetBranchOrATMDetailsCompletionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                if (response.data.PlaceDetails && response.data.PlaceDetails.length > 0) {
                    var viewModal = response.data.PlaceDetails[0];
                    self.presentLocateUs({ "getAtmorBranchDetailsSuccess": viewModal });
                } else {
                    self.presentLocateUs({ "getAtmorBranchDetailsFailure": "getAtmorBranchDetailsFailure" });
                }
            } else {
                self.presentLocateUs({ "getAtmorBranchDetailsFailure": "getAtmorBranchDetailsFailure" });
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.locateUs.getBranchOrATMDetails", params, GetBranchOrATMDetailsCompletionCallback));

    };

    return LocateUs_PresentationController;
});