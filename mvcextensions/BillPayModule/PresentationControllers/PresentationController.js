define(['CommonUtilities','OLBConstants'], function (CommonUtilities, OLBConstants) {

    var MDABasePresenter = kony.mvc.Presentation.BasePresenter;
    var BusinessController = kony.mvc.Business.Controller;
    var MDAFormController = kony.mvc.MDAFormController;
    /**
     * BillPay viewModel
     *  
     */
    function BillPay_PresentationController() {
        MDABasePresenter.call(this);
        this.billPayModel = {
            "managePayee": null,
            "history": null,
            "scheduledTab": null,
            "paymentDue": null,
            "noOfRecords": null,
            "accountCategories": null,

        };
        this.confirmModel = {
            "payABill": null,
            "payABillData": null
        };
        this.acknowledgementModel = {
            "ackPayABill": {
                "savedData": null,
                "successfulData": null,
                "accountData": null
            }
        };
    }

    inheritsFrom(BillPay_PresentationController, MDABasePresenter);

    /**
     * BillPay Module with all supported methods.
     *  
     */
    var BillPayModule = function (PresentationControllerName) {
        var self = this;

        /**
         *  Method to initialize BillPay Module with default inputs
         */
        var _init = function () {
            self.module = new kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
            self.commandExecuter = self.module.businessController;
        }();

        /**
         * Method to fetch all bill pay accounts from AccountsModule.
         * @params callback : callback method 
         */
        var _fetchAccountsList = function (callback) {
            try {
                var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                accountsModule.businessController.execute(
                    new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, callback)
                );
            }
            catch (error) {
                logMessage(error);
            }
        };

        /**
        * Method to delete the scheduled Transaction from TransferModule.
        * @params callback : callback method 
        */
        var _deleteScheduledTransaction = function (params, callback) {
            try {
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.businessController.execute(
                    new kony.mvc.Business.Command("com.kony.transfer.deleteTransfer", params, callback)
                );
            }
            catch (error) {
                logMessage(error);
            }
        };

        /**
         * Method to fetch all due bills.
         * @param callback : callback method
         */
        var _fetchDueBills = function (params, callback) {
            try {
                self.commandExecuter.execute(
                    new kony.mvc.Business.Command("com.kony.billpay.getBillsDueForUser", params, callback)
                );
            }
            catch (error) {
                logMessage(error);
            }
        };

        /**
         * Method to fetch all Scheduled bills.
         * @param callback : callback method
         */
        var _fetchScheduledBills = function (dataInputs, callback) {
            try {
                self.commandExecuter.execute(
                    new kony.mvc.Business.Command("com.kony.billpay.getBillsScheduledForUser", dataInputs, callback)
                );
            }
            catch (error) {
                logMessage(error);
            }
        };

        /**
           * Method to creat bill.
           *@params params : inputs data for creaat method 
           @params callback : callback method
           */
        var _createBill = function (params, callback) {
            try {
                if (params.data) {
                    self.commandExecuter.execute(
                        new kony.mvc.Business.Command("com.kony.billpay.createBill", params.data, callback)
                    );
                }
            }
            catch (error) {
                logMessage(error);
            }
        };

        var _fetchUserbillPayHistory = function (noOfRecords, callback) {
            try {
                self.commandExecuter.execute(
                    new kony.mvc.Business.Command("com.kony.billpay.getUserBillHistory", noOfRecords, callback)
                );
            }
            catch (error) {
                logMessage(error);
            }
        };

        /**
         * Method to fetch all e-Bills.
         * @param callback : callback method
         */
        var _fetchPreviousEBills = function (params, callback) {
            try {
                self.commandExecuter.execute(
                    new kony.mvc.Business.Command("com.kony.billpay.getPreviousBillsForBiller", params, callback)
                );
            }
            catch (error) {
                logMessage(error);
            }
        };

        /**
         * Method to fetch all payee Bills.
         * @param callback : callback method
         */
        var _fetchPayeeBills = function (params, callback) {
            try {
                self.commandExecuter.execute(
                    new kony.mvc.Business.Command("com.kony.billpay.getPayeeBills", params, callback)
                );
            }
            catch (error) {
                logMessage(error);
            }
        };
        /**
          * Method to deactivate ebill  
          * @param callback : callback method
          */
        var _deactivateEBill = function (params,callback) {
           try {
                self.commandExecuter.execute(
                    new kony.mvc.Business.Command("com.kony.billpay.modifyEbillStatus", params, callback)
                );
            }
            catch (error) {
                logMessage(error);
            }
        };
        /**
          * Method to Get Selceted  Payees  
          * @param callback : callback method
          */
    var _searchBillPayPayees = function (params,callback) {
      try {
        self.commandExecuter.execute(
          new kony.mvc.Business.Command("com.kony.billpay.getBillPayees", params, callback)
        );
      }
      catch (error) {
        logMessage(error);
      }
    };
        /**
         * Method to log messages to console with PresentationControllerName.
         * @params : msg , message to log.
         */
        function logMessage(msg) {
            kony.print(PresentationControllerName + msg);
        }

        return {
            fetchAccountsList: _fetchAccountsList,
            fetchDueBills: _fetchDueBills,
            fetchScheduledBills: _fetchScheduledBills,
            createBill: _createBill,
            fetchUserbillPayHistory: _fetchUserbillPayHistory,
            deleteScheduledTransaction: _deleteScheduledTransaction,
            fetchPreviousEBills : _fetchPreviousEBills,
            fetchPayeeBills : _fetchPayeeBills,
            deactivateEBill : _deactivateEBill,
            searchBillPayPayees : _searchBillPayPayees
        };
    };
    BillPay_PresentationController.prototype.initializePresentationController = function () {
        var self = this;
        self.billPayModule = BillPayModule("BillPay_PresentationController");
        self.viewModel = {};
        self.Constants = {
            PAYEE_ACTIVITY_LIMIT : 12,
            PAGING_ROWS_LIMIT : OLBConstants.PAGING_ROWS_LIMIT
        };
        
         //ManagePayee configuration.
        self.managePayeeConfig = {
            'sortBy' : 'payeeNickName',
            'defaultSortBy' :  'payeeNickName',
            'order' : OLBConstants.ASCENDING_KEY,            
            'defaultOrder' : OLBConstants.ASCENDING_KEY,
            'offset': OLBConstants.DEFAULT_OFFSET,
            'limit' : OLBConstants.PAGING_ROWS_LIMIT
        };
        //Scheduled configuration
        self.scheduledConfig = {
            'sortBy' : 'scheduledDate',
            'defaultSortBy' :  'scheduledDate',
            'order' : OLBConstants.DESCENDING_KEY,            
            'defaultOrder' : OLBConstants.DESCENDING_KEY
        };
        //History configuration
        self.historyConfig = {
            'sortBy' : 'transactionDate',
            'defaultSortBy' :  'transactionDate',
            'order' : OLBConstants.DESCENDING_KEY,            
            'defaultOrder' : OLBConstants.DESCENDING_KEY,
            'offset': OLBConstants.DEFAULT_OFFSET,
            'limit' : OLBConstants.PAGING_ROWS_LIMIT
        };
        //Payment Due configuration
        self.paymentDueConfig = {
            'sortBy' : 'billDueDate',
            'defaultSortBy' :  'billDueDate',
            'order' : OLBConstants.DESCENDING_KEY,            
            'defaultOrder' : OLBConstants.DESCENDING_KEY
        };
        // All payees configuration
        self.allPayeesConfig = {
            'sortBy' : 'payeeNickName',
            'defaultSortBy' :  'payeeNickName',
            'order' : OLBConstants.ASCENDING_KEY,            
            'defaultOrder' : OLBConstants.ASCENDING_KEY
        };
        
    };
    /** 
     * listboxFrequencies:   Method for getting frequncies
     * @member of {frmBillPayController}
     * @param {}
     * @return {}
     * @throws {} 
     */
    BillPay_PresentationController.prototype.listboxFrequencies = function() {
        var self = this;
        self.frequencies = {
            Once: "i18n.transfers.frequency.once",
            Daily: "i18n.Transfers.Daily",
            Weekly: "i18n.Transfers.Weekly",
            BiWeekly: "i18n.Transfers.EveryTwoWeeks",
            Monthly: "i18n.Transfers.Monthly",
            Quarterly: "i18n.Transfers.Quaterly",
            HALF_YEARLY: "i18n.Transfers.HalfYearly",
            Yearly: "i18n.Transfers.Yearly"
        };
        self.listboxFrequencies = function() {
            var frequencies = self.frequencies;
            var list = [];
            for (var key in frequencies) {
                if (frequencies.hasOwnProperty(key)) {
                    list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
                }
            }
            return list;
        };
    
    };
    
     /** 
     * listboxForHowLong:   Method for getting list box howLong 
     * @member of {frmBillPayController}
     * @param {}
     * @return {}
     * @throws {} 
     */
    BillPay_PresentationController.prototype.listboxForHowLong = function() {
      var self = this;
      self.forHowLong = {
            ON_SPECIFIC_DATE: "i18n.transfers.lbxOnSpecificDate",
            NO_OF_RECURRENCES: "i18n.transfers.lblNumberOfRecurrences"
        };
        self.listboxForHowLong = function() {
            var forHowLong = self.forHowLong;
            var list = [];
            for (var key in forHowLong) {
                if (forHowLong.hasOwnProperty(key)) {
                    list.push([key, kony.i18n.getLocalizedString(forHowLong[key])]);
                }
            }
            return list;
        };
    };
    
    BillPay_PresentationController.prototype.loadHamburgerBillPay = function(frm){
        var self = this;
        var howToShowHamburgerMenu = function(sideMenuViewModel){
          self.presentUserInterface(frm,{sideMenu : sideMenuViewModel});    
        };
        self.SideMenu.init(howToShowHamburgerMenu); 

        var presentTopBar = function(topBarViewModel) {
          self.presentUserInterface(frm,{topBar : topBarViewModel});
        };
        self.TopBar.init(presentTopBar);
    };
    BillPay_PresentationController.prototype.fetchAccountsList = function (callback) {
        var scopeObj = this;
        var onCompleteCallback = function (commandResponse) {

            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                var data = commandResponse.data;

                var paymentAccounts = data.filter(function (account) { //filter accounts for only pay bill support 
                    return account.supportBillPay === "1";
                });
                if (paymentAccounts.length > 0) {
                    var accountConfig = null;
                    var viewModel = paymentAccounts.map(function (account) {
                        return {
                            accountName: CommonUtilities.getAccountDisplayName(account) + " " + CommonUtilities.getDisplayBalance(account),
                            accountID: account.accountID,
                            accountType: account.accountType,
                            availableBalance: account.availableBalance,
                            currentBalance: account.currentBalance
                        };
                    });
                    scopeObj.viewModel.myPaymentAccounts = viewModel;
                    //scopeObj.frmController.bindMyPaymentAccountsData(viewModel);
                    scopeObj.presentBillPay({ "myPaymentAccounts": viewModel });
                }
                else {
                    kony.print("No Payment accounts.");
                }
            }
            else {
                kony.print(commandResponse.status + " : " + commandResponse.data);
                scopeObj.showServerError(commandResponse.data);
            }
             
            if(callback){
                callback.call();
            }
        };
        scopeObj.billPayModule.fetchAccountsList(onCompleteCallback);
    };

    BillPay_PresentationController.prototype.fetchPaymentDueBills = function (dataInputs) {
        var scopeObj = this;
        var fetchDueBillsCallback = function (commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                var data = commandResponse.data;
                var viewModel = {
                    totalActivatedEbills: {
                        count: data.length,
                        totalDueAmount: 0
                    },
                    dueBills: []
                };
                var totalDueAmount = 0;
                viewModel.dueBills = data.map(function (dataItem) {
                    totalDueAmount += parseFloat(dataItem.dueAmount);
                    return {
                        payeeName: dataItem.payeeName,
                        paidAmount: scopeObj.formatCurrency(dataItem.paidAmount),
                        paidDate: scopeObj.getDateFromDateString(dataItem.paidDate),
                        ebillStatus: dataItem.ebillStatus,
                        billDueDate: scopeObj.getDateFromDateString(dataItem.billDueDate),
                        dueAmount: scopeObj.formatCurrency(dataItem.dueAmount),
                        fromAccountName: dataItem.fromAccountName,
                        billerCategoryName: dataItem.billerCategoryName,
                        billid: dataItem.id,
                        payeeid: dataItem.payeeid,
                        billGeneratedDate : scopeObj.getDateFromDateString(dataItem.billGeneratedDate),
                        ebillURL : dataItem.ebillURL
                    };
                });
                viewModel.totalActivatedEbills.totalDueAmount = scopeObj.formatCurrency(totalDueAmount);
                viewModel.config = tmpInputs;                
                viewModel.ignorePaymentDue = dataInputs.ignorePaymentDue;
                scopeObj.viewModel.paymentDueBills = viewModel;
                scopeObj.presentBillPay(viewModel);
                //successCallback(viewModel);
            } else {
                //errorCallback(commandResponse.data);
                scopeObj.showServerError(commandResponse.data);
            }
        };
        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, scopeObj.paymentDueConfig);
        scopeObj.billPayModule.fetchDueBills(tmpInputs, fetchDueBillsCallback);
    };

    BillPay_PresentationController.prototype.fetchScheduledBills = function (dataInputs) {
        var scopeObj = this;
        var fetchScheduledBillsCallback = function (commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                var data = commandResponse.data;
                var viewModel = data.map(function (dataItem) {
                    return {
                        scheduledDate:  scopeObj.getDateFromDateString(dataItem.scheduledDate),
                        payeeName:  dataItem.payeeNickName  ||  dataItem.payeeName,
                        payeeId:  dataItem.payeeId,
                        billid:  dataItem.billid,
                        billDueAmount:  scopeObj.formatCurrency(dataItem.billDueAmount),
                        paidAmount:  scopeObj.formatCurrency(dataItem.amount),
                        fromAccountName:  dataItem.fromAccountName,
                        fromAccountNumber:  dataItem.fromAccountNumber,
                        notes:  dataItem.transactionsNotes || "",
                        referenceNumber: dataItem.referenceId,
                        lastPaidAmount:  scopeObj.formatCurrency(dataItem.billPaidAmount),
                        lastPaidDate:  scopeObj.getDateFromDateString(dataItem.billPaidDate),
                        eBillStatus:  dataItem.eBillEnable,
                        eBillSupport:  dataItem.eBillSupport,
                        dueDate:  scopeObj.getDateFromDateString(dataItem.billDueDate),
                        billerCategoryName:  dataItem.billerCategoryName,
                        billGeneratedDate : scopeObj.getDateFromDateString(dataItem.billGeneratedDate),
                        ebillURL : dataItem.ebillURL,
                        frequencyType :  dataItem.frequencyType,
                        numberOfRecurrences : dataItem.numberOfRecurrences,
                        frequencyStartDate:  dataItem.frequencyStartDate,
                        frequencyEndDate:   dataItem.frequencyEndDate,
                        amount : dataItem.amount,
                      	recurrenceDesc: dataItem.recurrenceDesc
                    };
                });
                scopeObj.viewModel.scheduledBills = viewModel;
                scopeObj.presentBillPay({ 'scheduledBills': viewModel , config :  tmpInputs });
            } else {
                kony.print(commandResponse.status + " : " + commandResponse.data);
                scopeObj.showServerError(commandResponse.data);
            }
        };
        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, scopeObj.scheduledConfig);
        scopeObj.billPayModule.fetchScheduledBills(tmpInputs, fetchScheduledBillsCallback);
    };

    BillPay_PresentationController.prototype.paySingleBill = function (data) {
        var scopeObj = this;
        var addPayABillData = null;
        var payABillCallback = function (commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                scopeObj.navigateToAcknowledgementForm(commandResponse.data);
            } else {
                kony.print(commandResponse.status + " : " + commandResponse.data);
            }
        };

        addPayABillData = {
            amount: data.payABillNavObj.txtSearch.text,
            billid: data.selectedBillData.lblBillId,
            fromAccountNumber: data.selectedBillData.lblfromAccountNumber,
            notes: data.payABillNavObj.txtNotes.text,
            scheduledDate: data.payABillNavObj.calSendOn.datecomponents[2] + "-" + data.payABillNavObj.calSendOn.datecomponents[1] + "-" + data.payABillNavObj.calSendOn.datecomponents[0],
            transactionType: "BillPay"
        };
        scopeObj.billPayModule.createBill({ data: addPayABillData }, payABillCallback);
    };

    BillPay_PresentationController.prototype.deleteScheduledTransaction = function (params) {
        var scopeObj = this;
        var deleteTransactionCallback = function (commandResponse) {
            //TODO what to do after deletion goes here
            var deleteSchedule = {};
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                deleteSchedule.successData = "success"; //TODO : we need Object for success   
            } else {
                //deleteSchedule.failureData =  commandResponse.data;
                scopeObj.showServerError(commandResponse.data);
            }
            //scopeObj.presentUserInterface("frmBillPay",  {"deleteSchedule" : deleteSchedule});
            scopeObj.presentBillPay({ "deleteSchedule": deleteSchedule });
        };
        scopeObj.billPayModule.deleteScheduledTransaction(params, deleteTransactionCallback);
    };
    BillPay_PresentationController.prototype.showServerError = function (data) {
        var scopeObj = this;
        scopeObj.presentBillPay({ "serverError": data });
    };

    BillPay_PresentationController.prototype.showOneTimePayment= function(data,sender){
     var data={
       "data":data,
       "sender":sender,
     }
     this.presentBillPay({"showOneTimePayment" :  data});
    };
  
    BillPay_PresentationController.prototype.showBillPayHistory = function (context, numberOfRecords) {
        var scopeObj = this;
        // var noOfRecords = {
        //     "offset": 0,
        //     "limit": scopeObj.Constants.PAGING_ROWS_LIMIT
        // };
        // if(numberOfRecords &&  numberOfRecords.offset) {
        //      noOfRecords.offset = numberOfRecords.offset;
        // }
        // if(numberOfRecords &&  numberOfRecords.limit) {
        //      noOfRecords.limit = numberOfRecords.limit;
        // }
        // if (numberOfRecords !== undefined) {
        //     noOfRecords.offset = numberOfRecords.offset;
        // }

        //Bill Pay History Starts
        var ViewModel = {
            billpayHistory: [],
            noOfRecords: null
        };

        var bindbillPayHistory = function (HistoryDataList, noOfRecords,sender) {
            ViewModel.billpayHistory = HistoryDataList.map(function (HistoryData) {
                return {
                    lastPaidDate: scopeObj.getDateFromDateString(HistoryData.transactionDate),
                    RefrenceNumber: HistoryData.referenceId,
                    SentFrom: HistoryData.fromAccountName,
                    payeeNickname: HistoryData.payeeNickName,
                    lastPaidAmount: scopeObj.formatCurrency(HistoryData.amount),
                    Status: HistoryData.statusDescription,
                    Notes: (HistoryData.transactionsNotes || ''),
                    dueAmount: scopeObj.formatCurrency(HistoryData.billDueAmount),
                    billDueDate: scopeObj.getDateFromDateString(HistoryData.billDueDate),
                    categoriesKey: HistoryData.category,
                    billid: HistoryData.billid,
                    billGeneratedDate : scopeObj.getDateFromDateString(HistoryData.billGeneratedDate),
                    ebillURL :HistoryData.ebillURL,
                    eBillStatus: HistoryData.eBillEnable,
                    eBillSupport:  HistoryData.eBillSupport,
                    frequencyType : HistoryData.frequencyType,
                    numberOfRecurrences :HistoryData.numberOfRecurrences,
                    frequencyStartDate: HistoryData.frequencyStartDate,
                    frequencyEndDate:  HistoryData.frequencyEndDate,
                    amount : HistoryData.amount,
                    payeeId : HistoryData.payeeId
                };
            });
            ViewModel.noOfRecords = noOfRecords;
            ViewModel.config = tmpInputs;
            ViewModel.billpayHistory.sender = sender;
            scopeObj.viewModel.history = ViewModel;
            scopeObj.presentBillPay(ViewModel);
        };

        if (context === "acknowledgement") {
            ViewModel.billpayHistory.sender = context;
        }
        
        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(numberOfRecords, scopeObj.historyConfig);
        
        var fetchUserbillPayHistoryCallback = function (commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                bindbillPayHistory(commandResponse.data, tmpInputs,context);
            } else {
                kony.print(commandResponse.status + " : " + commandResponse.data);
                scopeObj.showServerError(commandResponse.data);
            }
        };
        scopeObj.billPayModule.fetchUserbillPayHistory(tmpInputs, fetchUserbillPayHistoryCallback);
    };
    /**
     * Method for navigate to BillPay form
     * @ param : {object} data for form
     */
    BillPay_PresentationController.prototype.presentBillPay = function (data) {
        this.presentUserInterface("frmBillPay", data);
    };

    /**
     * Method for navigate to Cofirm form
     * @ param : {object} data for form
     */
    BillPay_PresentationController.prototype.navigateToAcknowledgementForm = function (data) {
        this.presentUserInterface("frmAcknowledgement", data);
    };

    BillPay_PresentationController.prototype.showPrintPage = function (data) {
        this.presentUserInterface("frmPrintTransfer", data);
    };    

    /**
     * Method for show Pay a Bill
     * @ param : {object} data for BillPay form and widgets
     */
    BillPay_PresentationController.prototype.showPayABill = function (data, context) {
        var scopeObj = this;
        //TODO: update with data from other forms.
        //    if(kony.application.getCurrentForm().id !== "frmBillPay") {
        //      scopeObj.presentBillPay(data);
        //    }
        //   if(context)
        //      scopeObj.frmController.setDataForPayABill(data,context);
        //   else
        //      scopeObj.frmController.setDataForPayABill(data);
        //this.getBillerCategories();
        if (context == "quickAction") {
            this.getAllPayees(this.showAllPayeesinForm.bind(this));
        }
        //      scopeObj.frmController.setDataForPayABill(data);        

        scopeObj.presentBillPay({
            "payABillWithContext": {
                data: data,
                context: context
            }
        });
    };

    /**
     * show all payees in create a bill form (Quick Actions flow)
     * @param : {Array} Payees models list
     */

    BillPay_PresentationController.prototype.showAllPayeesinForm = function (payees) {
        

        this.presentBillPay({allPayees: payees})
    }

    /**
     * Utility method for Date formatter
     * @param : {string} dateStr , date in string format
     * @return : {string} date in dd/mm/yyyy format
     */
    BillPay_PresentationController.prototype.getDateFromDateString = function (dateStr, format) {
        if (dateStr) {
            return  CommonUtilities.getFrontendDateString(dateStr,CommonUtilities.getConfiguration('frontendDateFormat'));            
        }
        else {
            return "";
        }
    }
    /**
     * Format currecy
     * @amount : currency value (12.3)
     * @return : formatted currency ($12.3)
     */
    BillPay_PresentationController.prototype.formatCurrency = function (amount,currencySymbolNotRequired) {
        return CommonUtilities.formatCurrencyWithCommas(amount,currencySymbolNotRequired);   
    }

    /**
     * Download file
     */
    BillPay_PresentationController.prototype.downloadFile = function (data) {
        return data ? CommonUtilities.downloadFile(data) : 'Invalid data';
    }

    /**
     * Method to set offset and limit.
     * @params : offset
     */
    BillPay_PresentationController.prototype.showBillPayData = function (sender, data) {
        var scopeObj = this;
        scopeObj.loadBillPayComponents();
        if(kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility === "Not Eligible") {
            scopeObj.showBillPayNotEligibleView();
        } else if(kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility === "Not Activated" &&
                    CommonUtilities.getConfiguration("isBillPayActivationAllowed") === "true") {
            scopeObj.showProgressBar();
            scopeObj.fetchAccountsList();
            scopeObj.showBillPayDeactivatedView();  
        } else {
            if(data && (data.show === "History" || data.show === "PayABill" || data.show === "AllPayees" || data.show === "ManagePayees")){
                scopeObj.presentBillPay({
                    "intialView" : data
                });
            }
            scopeObj.showProgressBar();
            scopeObj.getBillerCategories( function(){
                scopeObj.fetchAccountsList( function(){
                    if (data === undefined) {
                        data = {};
                    }
            
                    if (sender === "acknowledgement") {
                        scopeObj.showBillPayHistory(sender);            
                    }
                    else {
                            if(data.show === "PayABill"){                    
                                scopeObj.showPayABill(data.data ? data.data : data, data.sender?data.sender:sender);
                            } else if(data.show === "History"){
                                scopeObj.presentBillPay({
                                    "showHistoryUI" : true
                                });
                                scopeObj.showBillPayHistory(null, data);
                               
                           } else if(data.show === "ManagePayees"){
                                var noOfRecords = {
                                       "offset": data.offset || 0,
                                       "limit": data.limit || scopeObj.Constants.PAGING_ROWS_LIMIT,
                                       'resetSorting': true
                                   };
                                   scopeObj.showProgressBar();
                                   scopeObj.managePayeeData(noOfRecords);
                               
                           }
                            else {
                                scopeObj.showProgressBar();
                                scopeObj.allPayeeData({
                                    'resetSorting': true
                                });
                            }
                    }
                });
            });
            
        }
    };
    BillPay_PresentationController.prototype.createBulkPayments = function(bulkPayRecords, bulkPayRecordsModify) {
        var self = this;
        
                function completionCallback(response) {
                    if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                        self.billPayModel.bulkPay = response.data;
                        self.presentUserInterface("frmAcknowledgement", self.billPayModel);
                    } else {
                        self.modifyBulkPay(bulkPayRecordsModify);
                        response.data.bulkBillPay=true;
                        self.showServerError(response.data);
                    }
                }
                var transactions = [];
                for (var index in bulkPayRecords) {
                    var record = bulkPayRecords[index];
                    transactions.push({
                        'accountNumber': record.accountNumber,
                        'scheduledDate': record.lblSendOn,
                        'billid': record.billid,
                        'payeeId': record.payeeId,
                        'paidAmount': (record.lblAmount).slice(1).replace(/,/g,""),
                        'deliverBy': record.lblDeliverBy
                    });
                }
                self.businessController.execute(new kony.mvc.Business.Command('com.kony.billpay.createBulkBillPay', {
                    'transactionArray': transactions
                }, completionCallback));
    };
    BillPay_PresentationController.prototype.setDataForBulkBillPayConfirm = function(BulkPayRecords) {
        this.viewModel.bulkPayRecords = BulkPayRecords;
        this.presentUserInterface("frmConfirm",this.viewModel);
    };
    BillPay_PresentationController.prototype.modifyBulkPay = function(bulkPayRecords) {
        var self = this;
        self.billPayModel.BulkPay = null;
        self.billPayModel.managePayee = null;
        self.billPayModel.modifyBulkPay = bulkPayRecords;
        self.presentUserInterface("frmBillPay",self.billPayModel);
    };
    BillPay_PresentationController.prototype.getBillPayees = function(dataInputs, loadAllPayees){
        var self = this;
        if (loadAllPayees) {
            function completionAllPayeesCallback(response) {
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                    self.billPayModel.modifyBulkPay = null;
                    self.billPayModel.BulkPay = response.data;
                    self.billPayModel.managePayee = null;
                    self.billPayModel.config = tmpInputs;
                    self.presentUserInterface("frmBillPay", self.billPayModel);
                }
                else{
                    self.showServerError(response.data);
                }
            }
            var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, self.allPayeesConfig);
            delete tmpInputs.limit; //Remove default limit.
            delete tmpInputs.offset;
            self.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.getBillPayees", tmpInputs, completionAllPayeesCallback));
        }
    };
    BillPay_PresentationController.prototype.allPayeeData = function(dataInputs) {
        var self = this;
      	var loadAllPayees=true;
        self.getBillPayees(dataInputs,loadAllPayees);
    };
    /**
         * Method to fetch all bill pay data from payeeModule.
         * @params : successCallback function which will be called with 
         */
        BillPay_PresentationController.prototype.getAllPayees = function (successCallback) {
            var self = this;
            function completionManagePayeesCallback(response) {
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                    successCallback(response.data);            
                }
            }
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.getBillPayees", {}, completionManagePayeesCallback));
        };
    /**
         * Method to fetch all bill pay data from payeeModule.
         * @params : noOfRecords Json which contains offset and limit
         */
    BillPay_PresentationController.prototype.managePayeeData = function (dataInputs,callback) {
        var self = this;
        var tmpInputs =  CommonUtilities.Sorting.getSortConfigObject(dataInputs, self.managePayeeConfig);
        this.billPayModel.noOfRecords = tmpInputs;
        this.billPayModel.sortInputs = tmpInputs;

        function completionManagePayeesCallback(response) {
            self.managePayeeResponse(response);
            if(callback)
                callback.call();
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.getBillPayees", tmpInputs, completionManagePayeesCallback));
    };
    /**
     * Method to send response to frmBillpay when manage tab is clicked.
     * @params: response from getBillPayees command handler
     */
    BillPay_PresentationController.prototype.managePayeeResponse = function (response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
            this.billPayModel.managePayee = response.data;
            this.presentUserInterface("frmBillPay", this.billPayModel);
        }
        else
            kony.print(" " + response.err);
    };
    /**
     * Method to activate EBill in manage payee.
     * @params: payeeId, offset
     */
    BillPay_PresentationController.prototype.modifyEbillStatus = function (data, offset) {
        this.offset = offset;
        var EbillChangeJSON = {
            "payeeId": data["payeeId"],
            "eBillStatus": 1,
        };
        var self = this;
        function completionManagePayeesCallback(response) {
            if(data["allPayees"]){
                data["allPayee"] = false;
                self.showBillPayData();
            }
            else
                self.modifyEbillStatusResponse(response);
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.modifyEbillStatus", EbillChangeJSON, completionManagePayeesCallback));
    };
    /**
     * Method to fetch data after ebill is activated.
     * @params : response from  modifyEbillStatus command handler
     */
    BillPay_PresentationController.prototype.modifyEbillStatusResponse = function (response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
            this.managePayeeData({offset : this.offset});
        }
        else
            kony.print(" " + response.err);
    };
    /**
     * Method to fetch categories of biller.
     */
    BillPay_PresentationController.prototype.getBillerCategories = function (callback) {
        var self = this;
        function completionBillerCategoriesCallback(response) {
            self.getBillerCategoriesResponse(response);
            if(callback){
                callback.call();
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.getBillerCategories", {}, completionBillerCategoriesCallback));
    };

    /**
     * Method to fetch categories of biller.
     * @params : response from  getBillerCategories command handler
     */
    BillPay_PresentationController.prototype.getBillerCategoriesResponse = function (response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
            //this.billPayModel.accountCategories = response.data;
            this.presentBillPay( {"billerCategories" : response.data } );
        }
        else{
            kony.print(" " + response.err);
            this.showServerError(response.data);
        }
    };

    /**
     *Method to call formcontroller to set the viewModel
    */
    BillPay_PresentationController.prototype.setDataToPayABill = function (data) {
        var scopeObj = this;
        //    if(kony.application.getCurrentForm().id !== "frmBillPay") {
        //      scopeObj.presentBillPay(data);
        //    }
        //    scopeObj.frmController.setDataForPayABill(data);
        scopeObj.showPayABill(data);

    };
    /**
       *Method to filter the data to be  displayed on Pay A Bill 
      */
    BillPay_PresentationController.prototype.filterDataFromViewModelForPayABill = function (viewModel, searchData) {
        return viewModel.billpayHistory.filter(function (e) {
            return e.RefrenceNumber === searchData;
        })[0];
    };
    /**
       * Method to REPEAT a transaction from bill pay history tab
    */
    BillPay_PresentationController.prototype.onloadPayABill = function () {
        scopeObj = this;
        var currForm = kony.application.getCurrentForm();
        var data = currForm.tableView.segmentBillpay.selectedItems[0];
        kony.print("Data of the segment clicked is " + data);
        var filteredObj = scopeObj.filterDataFromViewModelForPayABill(scopeObj.viewModel.history, data.lblRefrenceNumberValue);
        if (filteredObj === null)
            kony.print("No such data with given Id found");
        else
            scopeObj.setDataToPayABill(filteredObj);
    };
    BillPay_PresentationController.prototype.setDataForConfirm = function (payABill) {
        // this.confirmModel.payABill = payABill;
        // this.presentUserInterface("frmConfirm", this.confirmModel);
        this.showView("frmConfirm",  { "payABill" : payABill});
    };
    /**
    * Method to set data for pay a bill
    */
    BillPay_PresentationController.prototype.showPayABillModify = function (payABillData) {
        // this.confirmModel.payABillData = payABillData;
        // this.presentUserInterface("frmBillPay", this.confirmModel);
        this.presentBillPay({"payABillData" :  payABillData});
    };
    BillPay_PresentationController.prototype.getAccountDataByAccountId = function (accountNumber) {
        var self = this;
        function completionAccountDetailsCallback(response) {
            self.acknowledgementModel.ackPayABill.accountData = response.data;
            self.presentUserInterface("frmAcknowledgement", self.acknowledgementModel);
        }
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccountById", accountNumber, completionAccountDetailsCallback));
    };

    /**
        * checkMFASingleBillPay:   Method for handling MFA for SingleBillPay 
        * @member of {frmBillPayController}
        * @param {}
        * @return {executes resepective view}
        * @throws {} 
    */
     BillPay_PresentationController.prototype.checkMFASingleBillPay = function(ackPayABill) {
      var self =this;
      if(CommonUtilities.getConfiguration("isMFAEnabledForBillPay") === "true" && Number(String(ackPayABill.amount).replace(/,/g, "")) > CommonUtilities.getConfiguration("minimumAmountForMFABillPay") && !CommonUtilities.isCSRMode())
      {
        var mfaModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('MultiFactorAuthenticationModule');
          mfaModule.presentationController.startSecurityQuestionsFlow({
          hamburgerSelection: {selection1:"BILL PAY", selection2: "Pay a Bill"},
          breadcrumb: kony.i18n.getLocalizedString("i18n.billPay.BillPay"),
          termsAndConditions: kony.i18n.getLocalizedString("i18n.BillPay.TermsAndConditions"),
          cancelCallback: self.showBillPayData.bind(self,{ show: "PayABill" }),
          successCallback: self.singlePayAcknowlegement.bind(self,ackPayABill)
        });
      }else
      {
        self.singlePayAcknowlegement(ackPayABill);  
      } 
    };  
    
    BillPay_PresentationController.prototype.singlePayAcknowlegement = function (ackPayABill) {
        
       var self = this;
        this.acknowledgementModel.ackPayABill.savedData = ackPayABill;
        var  date  =  ackPayABill.sendOn;
        var  newdate  =  CommonUtilities.sendDateToBackend(date, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat'));
        var addPayABillData = {
            amount: String(ackPayABill.amount).replace(/,/g, ""),
            payeeId: ackPayABill.payeeId,
            billid: ackPayABill.billid,
            fromAccountNumber: ackPayABill.fromAccountNumber,
            notes: ackPayABill.notes,
            scheduledDate: newdate,
            transactionType: "BillPay",
            billCategory: ackPayABill.categories
        };
        if (ackPayABill.frequencyType == kony.i18n.getLocalizedString(self.frequencies.Once)) {
            addPayABillData.numberOfRecurrences = "";
            addPayABillData.frequencyStartDate = "";
            addPayABillData.frequencyEndDate = "";
            addPayABillData.frequencyType = ackPayABill.frequencyType;
        } else if (ackPayABill.hasHowLong == "NO_OF_RECURRENCES") {
            addPayABillData.frequencyStartDate = "";
            addPayABillData.frequencyEndDate = "";
            addPayABillData.frequencyType = ackPayABill.frequencyType;
            addPayABillData.numberOfRecurrences = ackPayABill.numberOfRecurrences;
        } else if (ackPayABill.hasHowLong == "ON_SPECIFIC_DATE") {
            addPayABillData.numberOfRecurrences = "";
            addPayABillData.frequencyType = ackPayABill.frequencyType;
            addPayABillData.frequencyStartDate = CommonUtilities.sendDateToBackend(ackPayABill.frequencyStartDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat'));
            addPayABillData.frequencyEndDate = CommonUtilities.sendDateToBackend(ackPayABill.frequencyEndDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat'));
        }
        if(ackPayABill.gettingFromOneTimePayment)
        {
            addPayABillData.toAccountNumber = ackPayABill.accountNumber;
        }
        if (ackPayABill.isScheduled === "false" || !ackPayABill.referenceNumber) {
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.createBill", addPayABillData, completionBillerAcknowlegementCallback));
        } else {
            addPayABillData.transactionId = ackPayABill.referenceNumber;
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.updateBill", addPayABillData, completionBillerAcknowlegementCallback));
        }

        function completionBillerAcknowlegementCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                if (response.data.transactionId) {
                    response.data.referenceId = response.data.transactionId;
                }
                self.acknowledgementModel.ackPayABill.successfulData = response.data.referenceId;
                self.getAccountDataByAccountId(addPayABillData.fromAccountNumber);
            } else {
                if(ackPayABill.gettingFromOneTimePayment)
                    response.data.gettingFromOneTimePayment = ackPayABill.gettingFromOneTimePayment;
                  else
                    response.data.gettingFromSingleBillPay = true;
                self.showServerError(response.data);
            }
        }
    };

    /**
     * Method to fetch all previous ebills sent by Payee
     * @param : object {payeeid}
     */
    BillPay_PresentationController.prototype.fetchPreviousEBills = function ( params ) {
        var scopeObj = this;
        var fetchPreviousEBillsCallBack = function (commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                //TODO: parse response.
               kony.print(commandResponse);
            } else {
                kony.print(commandResponse.status + " : " + commandResponse.data);
                //scopeObj.showServerError(commandResponse.data);
            }
        };
        scopeObj.billPayModule.fetchPreviousEBills(params, fetchPreviousEBillsCallBack);
    };
    
    /**
     * Method to cancel transaction occurrence
     * @param : object {transaction object}
     */
    BillPay_PresentationController.prototype.cancelScheduledTransactionOccurrence = function(transaction) {
        var self = this;
        function cancelOccurrenceCallback(response) {
            var deleteSchedule = {};
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                deleteSchedule.successData = "success";
            } else {
                self.showServerError(response.data);
            }
            self.presentBillPay({
                "deleteSchedule": deleteSchedule
            });
        }
        this.businessController.execute(new kony.mvc.Business.Command('com.kony.billpay.cancelScheduledTransactionOccurrence', transaction, cancelOccurrenceCallback));
    };
    
    /**
     * Method to fetch all transaction by Payee
     * @param : object {payeeid, limit}
     */
    BillPay_PresentationController.prototype.fetchPayeeBills = function ( params ) {
        var scopeObj = this;
        var fetchPayeEBillsCallBack = function (commandResponse) {

            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                var data = commandResponse.data;
                var viewModel = data.map(function (dataItem) {
                    return {
                        scheduledDate:  scopeObj.getDateFromDateString(dataItem.scheduledDate),
                        amount: scopeObj.formatCurrency(dataItem.amount),
                        payeeName: dataItem.payeeName,
                        payeeNickName: dataItem.payeeNickName,
                        payeeId:  dataItem.payeeId,
                        billid:  dataItem.billid,
                        billDueAmount:  scopeObj.formatCurrency(dataItem.billDueAmount),
                        billDueDate: scopeObj.getDateFromDateString(dataItem.billDueDate),
                        billGeneratedDate: scopeObj.getDateFromDateString(dataItem.billGeneratedDate),
                        fromAccountName:  dataItem.fromAccountName,
                        fromAccountNumber:  dataItem.fromAccountNumber,
                        notes:  dataItem.transactionsNotes || "",
                        referenceNumber: dataItem.referenceId,
                        lastPaidAmount:  scopeObj.formatCurrency(dataItem.billPaidAmount),
                        lastPaidDate:  scopeObj.getDateFromDateString(dataItem.billPaidDate),
                        eBillStatus:  dataItem.eBillEnable,
                        eBillSupport:  dataItem.eBillSupport,
                        dueDate:  scopeObj.getDateFromDateString(dataItem.billDueDate),
                        billerCategoryName:  dataItem.billerCategoryName,
                        ebillURL : dataItem.ebillURL,
                        statusDescription: dataItem.statusDescription
                    };
                });
                scopeObj.viewModel.payeeActivities = viewModel;
                scopeObj.presentBillPay({'payeeActivities': viewModel});
            } else {
                kony.print(commandResponse.status + " : " + commandResponse.data);
                scopeObj.showServerError(commandResponse.data);
            }
        };

        params.limit = scopeObj.Constants.PAYEE_ACTIVITY_LIMIT;

        scopeObj.billPayModule.fetchPayeeBills(params, fetchPayeEBillsCallBack);
    };



    BillPay_PresentationController.prototype.loadBillPayComponents = function () {
        this.loadComponents('frmBillPay');
    };

    BillPay_PresentationController.prototype.showView = function (frm, data) {
        this.presentUserInterface(frm, data);
    };

    BillPay_PresentationController.prototype.loadComponents = function (frm) {
        var scopeObj = this;
        var howToShowHamburgerMenu = function (sideMenuViewModel) {
            scopeObj.showView(frm, { "sideMenu": sideMenuViewModel });
        };

        scopeObj.SideMenu.init(howToShowHamburgerMenu);

        var presentTopBar = function (topBarViewModel) {
            scopeObj.showView(frm, { "topBar": topBarViewModel });
        };
        scopeObj.TopBar.init(presentTopBar);
    };
    /**     
         * Method to call Update Command Handler to update edited details in manage payee edit.     
         * @params: response from updateBillPayee command handler, offset value     
         */     
        BillPay_PresentationController.prototype.updateManagePayee = function(response, offsetVal) {        
            var self = this;        
            
            function completionCallback(response) {     
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                    kony.print("--- success updating ----" + JSON.stringify(response));     
                    self.managePayeeData({offset : offsetVal});     
                    self.hideProgressBar();
                } else {        
                    kony.print("---- ERROR updating ----");     
                }       
            }
            self.showProgressBar();
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.updateBillPayee", response, completionCallback));       
        };      
        /**     
         * Method to call delete Command Handler to delete biller in manage payee edit.     
         * @params: response from deleteBillPayee command handler, offset value     
         */     
        BillPay_PresentationController.prototype.deleteManagePayee = function(delResponse, offsetVal) {     
            var self = this;        
            
            function completionCallback(response) {     
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {        
                    kony.print("------- success deleting ----" + JSON.stringify(response));     
                    self.managePayeeData({offset : offsetVal});
                    self.hideProgressBar();
                } else {        
                    kony.print("---- ERROR deleting ----" + JSON.stringify(response));      
                }       
            }       
            self.showProgressBar();
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.deleteBillPayee", delResponse, completionCallback));        
        };      
        /**     
         * Method to deactivate EBill in manage payee.      
         * @params: payeeId, offset     
         */     
        BillPay_PresentationController.prototype.deactivateEbill = function(payeeId, offset) {      
            this.offset = offset;       
            var eBillJson = {       
                "payeeId": payeeId,     
                "eBillStatus": 0,       
            };      
            var self = this;        
            
            function completionCallbackDeactivate(response) {       
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {        
                    self.modifyEbillStatusResponse(response);
                } else {        
                    kony.print("---- ERROR deactivating ----" + JSON.stringify(response));      
                }       
            }       
            this.billPayModule.deactivateEBill(eBillJson, completionCallbackDeactivate);
            //this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.modifyEbillStatus", eBillJson, completionCallbackDeactivate));        
        };
        BillPay_PresentationController.prototype.checkBillPayEligibilityForUser = function () {
            var self = this;
            var username = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
            var param = {
                "userName" : username
            };
            function billPayEligibilityCompletionCallback(response) {
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                    if(response.data.result === "Activated" || response.data.result === "Not Activated" || response.data.result === "Not Eligible" ) {
                        kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility = response.data.result;
                    }
                } else {
                    kony.print("server error");
                }
            }
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.checkBillPayEligibilityForUser", param, billPayEligibilityCompletionCallback));
        };
        BillPay_PresentationController.prototype.activateBillPayForUser = function (param) {
            var self = this;
            function activateBillPaymentCompletionCallback(response) {
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                    if(response.data.result === "success") {
                        self.updateUserPreferredBillPayAccountNumber(param);
                        self.hideProgressBar();
                    }
                } else {
                    self.hideProgressBar();
                }
            }
            self.showProgressBar();
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.activateBillPaymentForUser", param, activateBillPaymentCompletionCallback));
        };
        BillPay_PresentationController.prototype.updateUserPreferredBillPayAccountNumber = function (param) {
            var self = this;
            function updateUserCompletionCallback(response) {
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                   kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility = "Activated";
                } else {
                    kony.print("server error billpay not activated");
                }
            }
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.updateUsersPreferredBillPayAccountNumber", param, updateUserCompletionCallback));
        };
        BillPay_PresentationController.prototype.showBillPayDeactivatedView = function () {
            var self = this;
            self.presentBillPay({ "showDeactivatedView": "showDeactivatedView" });
        };
        BillPay_PresentationController.prototype.showBillPayNotEligibleView = function () {
            var self = this;
            self.presentBillPay({ "showNotEligibleView": "showNotEligibleView" });
        };

        BillPay_PresentationController.prototype.showProgressBar = function (data) {
            var self = this;
            self.presentBillPay({ "ProgressBar": {
                show : true
            }  });
        };

        BillPay_PresentationController.prototype.hideProgressBar = function (data) {
            var self = this;
            self.presentBillPay({ "ProgressBar": {
                show : false
            } });
        };
        
        /**
   * Search BillPay Payees.
   * 
   */
  BillPay_PresentationController.prototype.searchBillPayPayees = function (data) {
    var scopeObj = this;

    if(data && data.searchKeyword.length >= 0) {
      var searchInputs = {
        'searchString': data.searchKeyword
      };

      var fetchSearchBillPayPayeesCallback = function (commandResponse) {
        var viewModel = {};
        if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
           viewModel.searchBillPayPayees = {
            managePayee : commandResponse.data,
            searchInputs : searchInputs
          }; 
        }else {
          //viewModel.searchBillPayPayees = { "error": commandResponse };
           scopeObj.showServerError(commandResponse.data);
          kony.print("Search BillPay : " + commandResponse.status + " :" + commandResponse.data);
        }
          scopeObj.presentBillPay(viewModel);
        };
       scopeObj.billPayModule.searchBillPayPayees(searchInputs, fetchSearchBillPayPayeesCallback);
      }
      else {
      kony.print('Invalid input.');
    }
  };
  
     /**
       *To hide UI related to bill pay when navigating from 
       * acknowledgement form
      */      
     BillPay_PresentationController.prototype.hideAllFlx=function(){
        var self=this;
        self.presentBillPay({"hideAllFlx":{
            show:true
        }
              });
      };

      /**
      * validateBillPayAmount - To Validate minimum and maximum limit for the amount
      * @member of {BillPay_PresentationController}
      * @param {amount} - amount
      * @returns {Object} - object contains isAmountValid and errMsg
      * @throws {}
      */
      BillPay_PresentationController.prototype.validateBillPayAmount = function (amount) {
        var minBillPayLimit = parseFloat(CommonUtilities.getConfiguration("minBillPayLimit"));
        var maxBillPayLimit = parseFloat(CommonUtilities.getConfiguration("maxBillPayLimit"));
        var result = {
            isAmountValid : false
        };
        if(amount < minBillPayLimit) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.minTransactionError") + " " + CommonUtilities.formatCurrencyWithCommas(minBillPayLimit);
        } else if(amount > maxBillPayLimit) {
            result.errMsg = kony.i18n.getLocalizedString("i18n.common.maxTransactionError") + " " + CommonUtilities.formatCurrencyWithCommas(maxBillPayLimit);
        } else {
            result.isAmountValid = true;
        }
        return result;
      };
      BillPay_PresentationController.prototype.saveDefaultAccountBillPay=function(defaultAccountBillPay){
        var self=this;
        var param ={
          default_account_billPay : defaultAccountBillPay
        
        };
        function completionCallBack(response){
          if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
           kony.mvc.MDAApplication.getSharedInstance().appContext.billPayDefaultAccount = defaultAccountBillPay;
          }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.setDefaultUserAccounts", param, completionCallBack));
      };
  
    BillPay_PresentationController.prototype. updateShowBillPayFromAccPop = function(){
      var self=this;
      var param = {
        showBillPayFromAccPopup : false
      };
      function completionCallBack(response){
         if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
             kony.mvc.MDAApplication.getSharedInstance().appContext.defaultBillPayNeverShow = false;
          }
      }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.setShowBillPayFromAccPopUp", param, completionCallBack));
    };
    return BillPay_PresentationController;
});