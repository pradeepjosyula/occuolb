define(['formatDate', 'ErrHandler', 'DateUtils', 'CommonUtilities', 'OLBConstants','CommonUtilitiesOCCU'], function (formatDate, ErrHandler, DateUtils, CommonUtilities, OLBConstants,CommonUtilitiesOCCU){
 
  return {
       showBillPayData : function (sender, data) {
        var scopeObj = this;
        
        //if(kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility === "Activated"){ //BillPay Activated
            if(data && (data.show === "History" || data.show === "PayABill" || data.show === "AllPayees" || data.show === "ManagePayees")){
                scopeObj.presentBillPay({
                    "intialView" : data
                });
            }
            scopeObj.showProgressBar();
           // scopeObj.getBillerCategories( function(){
               // scopeObj.fetchAccountsList( function(){
                    if (data === undefined) {
                        data = {};
                    }
            
                    if (sender === "acknowledgement") {
                        scopeObj.showBillPayHistory(sender);            
                    }
                    else {
                            if(data.show === "PayABill"){                    
                                scopeObj.showPayABill(data.data ? data.data : data, data.sender?data.sender:sender);
                            } else if(data.show === "History"){
                                scopeObj.presentBillPay({
                                    "showHistoryUI" : true
                                });
                                scopeObj.showBillPayHistory(null, data);
                               
                           } else if(data.show === "ManagePayees"){
                                var noOfRecords = {
                                       "offset": data.offset || 0,
                                       "limit": data.limit || scopeObj.Constants.PAGING_ROWS_LIMIT,
                                       'resetSorting': true
                                   };
                                   scopeObj.showProgressBar();
                                   scopeObj.managePayeeData(noOfRecords);
                               
                           }
                            else {
                                var noOfRecords = {
                                       "offset": data.offset || 0,
                                       "limit": data.limit || scopeObj.Constants.PAGING_ROWS_LIMIT,
                                       'resetSorting': true
                                };
                                scopeObj.showProgressBar();
                                scopeObj.allPayeeData(noOfRecords);
                            }
                    }
           //     });
         //   });
            
        /*}
        else {
            if(kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility === "Not Activated") {
				scopeObj.showProgressBar();
                scopeObj.fetchAccountsList();
                scopeObj.showBillPayDeactivatedView();  
            } else {
                scopeObj.showBillPayNotEligibleView();
            }
            kony.print( "BillPay : "  +  kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility);
        }*/
        scopeObj.loadBillPayComponents();
      },
      allPayeeData : function(dataInputs) {
        var self = this;
      	var loadAllPayees=true;
        self.getPreferredAccount(dataInputs,loadAllPayees);
     },
     getPreferredAccount : function(dataInputs,loadAllPayees) {
        var self = this;
                function accountFetchCompletionCallback(secondResponse) {
                    if (secondResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                        var accounts =  secondResponse.data;
                        var payeeaccountList = [];
                        if(accounts !== null && accounts.length >0){
                            for(var i=0; i <accounts.length; i++){
                                var billpayflag = accounts[i]["optpayBill"];
                                if(billpayflag === true || billpayflag === "true"){
                                    payeeaccountList.push(accounts[i]);
                                }
                            }
                            self.viewModel.payeeaccountList = payeeaccountList;
                            self.presentUserInterface("frmBillPay", self.viewModel);
                        }
    					if(loadAllPayees){
                        function completionAllPayeesCallback(thirdResponse) {
                            if (thirdResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                                self.billPayModel.modifyBulkPay = null;
                                self.billPayModel.EntireBulkPay = thirdResponse.data;
                                var paybilllist = []; //CommonUtilitiesOCCU.getpayeeList();
                                var limit = self.billPayModel.noOfRecords.limit;
								var offset = self.billPayModel.noOfRecords.offset;
								limit = limit + offset;
								thirdResponse.data = thirdResponse.data.filter(function (_, count) {
									return (count >= offset && count < limit);
								});
                                self.billPayModel.BulkPay = thirdResponse.data;
                                
                                if (thirdResponse.data !== null && thirdResponse.data !== "" && thirdResponse.data !== undefined) {
                                    if (thirdResponse.data[0].payeeId !== null && thirdResponse.data[0].payeeId !== "" && thirdResponse.data[0].payeeId !== undefined) {
                                        kony.print("Value exists");
                                    } else {
                                        self.billPayModel.BulkPay = paybilllist;
                                        self.billPayModel.EntireBulkPay = paybilllist;
                                    }
                                }                                
                                self.billPayModel.managePayee = null;
                                self.billPayModel.config = tmpInputs;
                                self.presentUserInterface("frmBillPay", self.billPayModel);
                            }
                        }
                        var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, self.allPayeesConfig);
                        self.billPayModel.noOfRecords = tmpInputs;
                        self.billPayModel.sortInputs = tmpInputs;
                        self.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.getBillPayees", tmpInputs, completionAllPayeesCallback));
                        }
                    }
                }
                var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccounts",{}, accountFetchCompletionCallback));
     },
     getAllPayeeDataForPagination: function(dataInputs){
       var self = this;
       var BulkPay = self.billPayModel.EntireBulkPay;
           var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, self.allPayeesConfig);
           self.billPayModel.noOfRecords = tmpInputs;
           self.billPayModel.sortInputs = tmpInputs;
           self.billPayModel.managePayee = null;
           self.billPayModel.config = tmpInputs;
       if(BulkPay !== null && BulkPay !== undefined && BulkPay.length >0){
           var paybilllist = []; 
           var limit = self.billPayModel.noOfRecords.limit;
           var offset = self.billPayModel.noOfRecords.offset;
           limit = limit + offset;
           BulkPay = BulkPay.filter(function (_, count) {
             return (count >= offset && count < limit);
           });
           self.billPayModel.BulkPay = BulkPay;
             if (BulkPay!== null && BulkPay !== "" && BulkPay !== undefined) {             
               kony.print("Value exists");
             } else {
               self.billPayModel.BulkPay = paybilllist;
             }              
          
           self.presentUserInterface("frmBillPay", self.billPayModel);
       }
     },
     createBulkPayments : function(bulkPayRecords) {
                var self = this;   
                //this.showProgressBar();
                function completionCallback(response) {
                    //this.hideProgressBar();
                    var responseBilldata = [];
                    if (response.status === kony.mvc.constants.STATUS_SUCCESS && (response.data.success === true || response.data.success === "true")) {
                        responseBilldata = response.data.data;
                        self.billPayModel.bulkPay = bulkPayRecords; //response.data;      
                        self.billPayModel.responseBilldata = responseBilldata;
                        self.presentUserInterface("frmAcknowledgement", self.billPayModel);
                    } else {
                        kony.print(" " + response.err);
                        self.billPayModel.bulkPay = bulkPayRecords; //response.data;                      
                        self.presentUserInterface("frmAcknowledgement", self.billPayModel, responseBilldata);
                    }
                }
                var transactions = [];
                for (var index in bulkPayRecords) {
                    var record = bulkPayRecords[index];
                    transactions.push({                       
                        'startDate': record.lblSendOn,
                        'payeeId': record.payeeId,
                        'payeeName':record.lblPayee,
                        'frequency':record.lblFrequency,
                        'endDate':record.lblDeliverBy,
                        'occurrence': 1,
                        'memo':'',
                        'amount': (record.lblAmount).slice(1).replace(/,/g,"")
                    });
                }
                  var transactionstringval = JSON.stringify(transactions);
                  var newtraansstr = transactionstringval.replace(/\"/g, "'");
                  var maintransactions = {
                      'username': kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
                      'account': bulkPayRecords[0].accountNumber,
                      'paymentDetails': newtraansstr
                  }
                      self.businessController.execute(new kony.mvc.Business.Command('com.kony.billpay.createBulkBillPay', {
                    'transactionArray': maintransactions
                }, completionCallback));
    },    
	}
  
});