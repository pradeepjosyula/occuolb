define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_getBillsScheduledForUser_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_getBillsScheduledForUser_CommandHandler, CommandHandler);
  
  	BillPay_getBillsScheduledForUser_CommandHandler.prototype.execute = function(command){
		var self = this;
        var transactionType = command.context.transactionType;
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var params = {
            "transactionType": transactionType,
            "sortBy" : command.context.sortBy,
            "order" : command.context.order
        };

        function completionCallback(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) 
                self.sendResponse(command, status, response);
            else 
                self.sendResponse(command, status, err);
        }
        TransactionsModel.customVerb("getUsersScheduledBill", params, completionCallback);
    };
	
	BillPay_getBillsScheduledForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_getBillsScheduledForUser_CommandHandler;
    
});