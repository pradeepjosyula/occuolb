define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_validateBillerAccount_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_validateBillerAccount_CommandHandler, CommandHandler);
  
  	BillPay_validateBillerAccount_CommandHandler.prototype.execute = function(command){
	  var self = this;	
      var BillPayValidateModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("BillerMaster");
      
      function onCompletion(status, response, err){
        if (status == kony.mvc.constants.STATUS_SUCCESS) 
                self.sendResponse(command, status, response);
            else 
                self.sendResponse(command, status, err);
      }
      var params = {
        'id':command.context.billerId,
        'paymentAccount':command.context.paymentAccount
        //'limit':command.context.limit
      };
      BillPayValidateModel.customVerb('validateBillerByAccount', params, onCompletion);
    };
	
	BillPay_validateBillerAccount_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_validateBillerAccount_CommandHandler;
    
});