define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_getUserBillHistory_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_getUserBillHistory_CommandHandler, CommandHandler);
  
  	BillPay_getUserBillHistory_CommandHandler.prototype.execute = function(command){
		var self = this;
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var params = {
          "offset":command.context.offset,
          "limit":command.context.limit, 
          "sortBy": command.context.sortBy,
          "order" : command.context.order
        };

        function completionCallback(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) 
                self.sendResponse(command, status, response);
            else 
                self.sendResponse(command, status, err);
        }
        TransactionsModel.customVerb("getUserCompletedBillHistory", params, completionCallback);

    };
	
	BillPay_getUserBillHistory_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_getUserBillHistory_CommandHandler;
    
});