define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_updateBill_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_updateBill_CommandHandler, CommandHandler);
  
  	BillPay_updateBill_CommandHandler.prototype.execute = function(command){
		
      	var self=this;
      	function completionCallback(status,response,error){
            if(status==kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,response);
            }else{
              self.sendResponse(command,status,error);
            }
        }
      
      	try{
          
          var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
          var transactionObject=new transactionsModel({"transactionId": command.context.transactionId});
          transactionObject.amount = command.context.amount;
            transactionObject.scheduledDate = command.context.scheduledDate;
            transactionObject.fromAccountNumber = command.context.fromAccountNumber;
            transactionObject.transactionsNotes = command.context.notes;
            transactionObject.payeeId = command.context.payeeId;
            transactionObject.frequencyType =  command.context.frequencyType,
            transactionObject.numberOfRecurrences = command.context.numberOfRecurrences,
            transactionObject.frequencyStartDate = command.context.frequencyStartDate,
            transactionObject.frequencyEndDate = command.context.frequencyEndDate
          transactionObject.partialUpdate(completionCallback);
      	}catch(err){
        	self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
      	}
      
    };
	
	BillPay_updateBill_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_updateBill_CommandHandler;
    
});