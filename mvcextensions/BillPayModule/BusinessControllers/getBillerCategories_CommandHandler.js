define([], function() {
  	var CommandHandler = kony.mvc.Business.CommandHandler;
    var Command = kony.mvc.Business.Command;
    var CommandResponse = kony.mvc.Business.CommandResponse;
  
  	function BillPay_getBillerCategories_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_getBillerCategories_CommandHandler, CommandHandler);
  
  	BillPay_getBillerCategories_CommandHandler.prototype.execute = function(command){
       var  self  =  this;

        /**
         * Call back function for getAll service.
         * @params  : status message, data, error message.
         */
        function  getAllCompletionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try  {
          var billerCategoryModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("BillerCategory");
          billerCategoryModel.getAll(getAllCompletionCallback);
        }  catch  (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
	
	BillPay_getBillerCategories_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_getBillerCategories_CommandHandler;
    
});