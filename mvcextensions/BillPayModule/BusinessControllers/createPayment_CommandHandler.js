define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_createPayment_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_createPayment_CommandHandler, CommandHandler);
  
  	BillPay_createPayment_CommandHandler.prototype.execute = function(command){
		
    };
	
	BillPay_createPayment_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_createPayment_CommandHandler;
    
});