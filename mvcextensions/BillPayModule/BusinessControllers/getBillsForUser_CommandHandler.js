define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_getBillsForUser_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_getBillsForUser_CommandHandler, CommandHandler);
  
  	BillPay_getBillsForUser_CommandHandler.prototype.execute = function(command){
		
    };
	
	BillPay_getBillsForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_getBillsForUser_CommandHandler;
    
});