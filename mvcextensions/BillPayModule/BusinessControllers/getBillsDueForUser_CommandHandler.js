define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_getBillsDueForUser_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_getBillsDueForUser_CommandHandler, CommandHandler);
  
  	BillPay_getBillsDueForUser_CommandHandler.prototype.execute = function(command){
	
      var  self  =  this;

        /**
         * Call back function for getAll service.
         * @params  : status message, data, error message.
         */
        function  getAllCompletionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try {

          var sortBy = command.context.sortBy;
          var order = command.context.order;

          var billsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Bills");
          var criteria = kony.mvc.Expression.and(
            kony.mvc.Expression.eq("sortBy", sortBy),
            kony.mvc.Expression.eq("order", order)
          );
          billsModel.getByCriteria(criteria, getAllCompletionCallback);

        } catch (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
      
    };
	
	BillPay_getBillsDueForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_getBillsDueForUser_CommandHandler;
    
});