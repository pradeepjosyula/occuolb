define([], function() {

  	function BillPay_activateBillPaymentForUser_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_activateBillPaymentForUser_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	BillPay_activateBillPaymentForUser_CommandHandler.prototype.execute = function(command){
      var self = this;
      /**
       * Call back function for activateBillPayForUser.
       * @params  : status message, data, error message.
       */
      function completionCallback(status, response, err) {
          if(status==kony.mvc.constants.STATUS_SUCCESS)
            self.sendResponse(command, status, response);
          else
            self.sendResponse(command, status, err);
      } 
      try {
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.activateBillPaymentForUser(command.context,completionCallback);
      } catch (error) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
      }		
    };
	
	  BillPay_activateBillPaymentForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_activateBillPaymentForUser_CommandHandler;
    
});