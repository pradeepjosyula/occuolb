define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_getBillerByAccountNumber_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_getBillerByAccountNumber_CommandHandler, CommandHandler);
  
  	BillPay_getBillerByAccountNumber_CommandHandler.prototype.execute = function(command){
		var self = this;
        var accountNumber = command.context.accountNumber;
        var BillerMasterModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("BillerMaster");
        var params = {
          "accountNumber":accountNumber
        };

        function completionCallback(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) 
                self.sendResponse(command, status, response);
            else 
                self.sendResponse(command, status, err);
        }
        BillerMasterModel.customVerb("getBillerByAccountNumber", params, completionCallback);
    
    };
	
	BillPay_getBillerByAccountNumber_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_getBillerByAccountNumber_CommandHandler;
    
});