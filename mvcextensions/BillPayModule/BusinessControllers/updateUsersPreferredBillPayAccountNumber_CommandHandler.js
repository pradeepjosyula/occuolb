define([], function() {

  	function BillPay_updateUsersPreferredBillPayAccountNumber_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_updateUsersPreferredBillPayAccountNumber_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	BillPay_updateUsersPreferredBillPayAccountNumber_CommandHandler.prototype.execute = function(command){
      var self = this;
      var params = {
        "username": command.context.username,
        "default_account_payments" : command.context.accountNumber
      };
      /**
       * Call back function for updateUserCompletionCallback.
       * @params  : status message, data, error message.
       */
      function completionCallback(status, response, err) {
          if(status==kony.mvc.constants.STATUS_SUCCESS)
            self.sendResponse(command, status, response);
          else
            self.sendResponse(command, status, err);
      } 
      try {
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.updatePreferredBillPayAccount(command.context,completionCallback);
      } catch (error) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
      }   		
    };
	
	  BillPay_updateUsersPreferredBillPayAccountNumber_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_updateUsersPreferredBillPayAccountNumber_CommandHandler;
    
});