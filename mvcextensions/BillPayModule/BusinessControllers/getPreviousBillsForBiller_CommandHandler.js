define([], function () {

    function BillPay_getPreviousBillsForBiller_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(BillPay_getPreviousBillsForBiller_CommandHandler, kony.mvc.Business.CommandHandler);

    BillPay_getPreviousBillsForBiller_CommandHandler.prototype.execute = function (command) {
            var scopeObj = this;

            /**
             * Call back function for service.
             * @params  : status message, data, error message.
             */
            function completionCallback(status, data, error) {
                scopeObj.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
            }
            

            var params = {
                "PayeeId":command.context.payeeId,
            };

            try {
                var billsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Bills");
                billsModel.customVerb("getPreviousBillsForBiller", params, completionCallback);
            } catch (error) {
                scopeObj.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
            }
    };

    BillPay_getPreviousBillsForBiller_CommandHandler.prototype.validate = function () {

    };

    return BillPay_getPreviousBillsForBiller_CommandHandler;

});