define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_deleteBillPayee_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_deleteBillPayee_CommandHandler, CommandHandler);
  
  	BillPay_deleteBillPayee_CommandHandler.prototype.execute = function(command){
		var self = this;
		function completionCallbackDelete(status, response, error) {
			if (status == kony.mvc.constants.STATUS_SUCCESS) {
				self.sendResponse(command, status, response);
				kony.print("---success response--" + JSON.stringify(response));
			} else {
				self.sendResponse(command, status, error);
			}
		}
		try {
			
			var payeeModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
			var payeeObject = new payeeModel({
					"payeeId": command.context.payeeId
				});
			payeeModel.removeById(command.context.payeeId, completionCallbackDelete);
		} catch (err) {
			self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
		}
		
    };
	
	BillPay_deleteBillPayee_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_deleteBillPayee_CommandHandler;
    
});