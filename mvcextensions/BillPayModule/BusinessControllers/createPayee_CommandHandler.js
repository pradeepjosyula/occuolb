define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_createPayee_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_createPayee_CommandHandler, CommandHandler);
  
  	BillPay_createPayee_CommandHandler.prototype.execute = function(command){
		var self=this;
      	
      	try{
          function completionCallback(status,response,error){
            if(status==kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,response);
            }else{
              self.sendResponse(command,status,error);
            }
        }
      
          var payeeModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
          var payeeObject={};
          payeeObject.accountNumber=command.context.accountNumber;
          payeeObject.username=kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU;
          var billerAddress = command.context.billerAddress;
          var state= "", cityName= "", zipCode= "", addressLine1="";
          if(billerAddress !== null && billerAddress !== "" && billerAddress !== undefined){
            var addressarr = billerAddress.split("@");
            payeeObject.addressLine1 = addressarr[0];
            payeeObject.cityName = addressarr[1];
            payeeObject.state = addressarr[2];
            payeeObject.zipCode = addressarr[3];
          }
         // payeeObject.eBillStatus=command.context.eBillStatus;
          //payeeObject.addressLine2=command.context.addressLine2;
          //payeeObject.addressLine1=command.context.addressLine1;
          //payeeObject.cityName=command.context.cityName;
          payeeObject.payeeName=command.context.companyName;
          //payeeObject.email=command.context.email;
         // payeeObject.payeeName=command.context.payeeName;
          payeeObject.payeeNickName=command.context.payeeNickName;
         // payeeObject.phone=command.context.phone;
         // payeeObject.state=command.context.state;
         // payeeObject.street=command.context.street;
         // payeeObject.zipCode=command.context.zipCode;
          //payeeObject.notes=command.context.notes;
         // payeeObject.nameOnBill=command.context.nameOnBill;
          //payeeObject.billerId=command.context.billerId;
          if(command.context.billerId){
            payeeObject.custom = false;
          }else{
            payeeObject.custom = true;
          }
          payeeModel.customVerb('addPayee', payeeObject, completionCallback);
         // payeeObject.save(completionCallback);
         // BillPayValidateModel.customVerb('validateBillerByAccount', params, onCompletion);
      	}catch(err){
        	self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
      	}
    };
	
	BillPay_createPayee_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_createPayee_CommandHandler;
    
});