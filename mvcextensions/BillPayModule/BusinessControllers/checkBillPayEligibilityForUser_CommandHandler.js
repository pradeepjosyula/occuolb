define([], function() {

  	function BillPay_checkBillPayEligibilityForUser_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_checkBillPayEligibilityForUser_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	BillPay_checkBillPayEligibilityForUser_CommandHandler.prototype.execute = function(command){
      var self = this;
      /**
       * Call back function for checkBillPayEligibility.
       * @params  : status message, data, error message.
       */
      function completionCallback(status, response, err) {
          if(status==kony.mvc.constants.STATUS_SUCCESS)
            self.sendResponse(command, status, response);
          else
            self.sendResponse(command, status, err);
      } 
      try {
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.checkBillPayEligibilityForUser(command.context,completionCallback);
      } catch (error) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
      }
    };
	
	BillPay_checkBillPayEligibilityForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_checkBillPayEligibilityForUser_CommandHandler;
    
});