define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_updateBillPayee_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_updateBillPayee_CommandHandler, CommandHandler);
  
  	BillPay_updateBillPayee_CommandHandler.prototype.execute = function(command){
		var self = this;
		function completionCallback(status, response, error) {
			if (status == kony.mvc.constants.STATUS_SUCCESS) {
				self.sendResponse(command, status, response);
			} else {
				self.sendResponse(command, status, error);
			}
		}
		try {
			
			var payeeModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
			var payeeObject = new payeeModel({
					"payeeId": command.context.payeeId
				});
			payeeObject.payeeNickName = command.context.payeeNickName;
          	payeeObject.addressLine2 = command.context.addressLine2;
            payeeObject.state = command.context.state;
            payeeObject.zipCode = command.context.zipCode;
            payeeObject.cityName = command.context.cityName;
		    payeeObject.addressLine1=command.context.addressLine1;  
			payeeObject.partialUpdate(completionCallback);
		} catch (err) {
			self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
		}
		
		
    };
	
	BillPay_updateBillPayee_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_updateBillPayee_CommandHandler;
    
});