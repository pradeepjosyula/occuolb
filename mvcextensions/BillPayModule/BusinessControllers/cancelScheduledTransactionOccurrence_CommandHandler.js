define([], function() {

  	function BillPay_cancelScheduledTransactionOccurrence_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_cancelScheduledTransactionOccurrence_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	BillPay_cancelScheduledTransactionOccurrence_CommandHandler.prototype.execute = function(command){
		var self = this;
		var transactionRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('Transactions');
      	function onCompletion(status, response, error){
        	self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? response: error);
        }
      	transactionRepo.customVerb('cancelScheduledTransactionOccurrence', {transactionId: command.context.transactionId}, onCompletion);
    };
	
	BillPay_cancelScheduledTransactionOccurrence_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_cancelScheduledTransactionOccurrence_CommandHandler;
    
});