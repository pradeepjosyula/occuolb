define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_getPayeeBills_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_getPayeeBills_CommandHandler, CommandHandler);
  
  	BillPay_getPayeeBills_CommandHandler.prototype.execute = function(command){
		var self = this;
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var params = {
          "payeeId":command.context.payeeId,
          "limit":command.context.limit
        };

        function completionCallback(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) 
                self.sendResponse(command, status, response);
            else 
                self.sendResponse(command, status, err);
        }
        TransactionsModel.customVerb("getPayeeBills", params, completionCallback);

    };
	
	BillPay_getPayeeBills_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_getPayeeBills_CommandHandler;
    
});