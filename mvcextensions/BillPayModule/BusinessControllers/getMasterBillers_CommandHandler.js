define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_getMasterBillers_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_getMasterBillers_CommandHandler, CommandHandler);
  
  	BillPay_getMasterBillers_CommandHandler.prototype.execute = function(command){
      var self = this;
       function  getMasterBillersCompletionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
      try  {
          var  masterBillersModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("BillerMaster");
          masterBillersModel.getAll(getAllCompletionCallback);
        }  catch  (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
	
	BillPay_getMasterBillers_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_getMasterBillers_CommandHandler;
    
});