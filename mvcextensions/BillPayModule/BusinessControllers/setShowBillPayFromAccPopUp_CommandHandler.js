define([], function() {

    var CommandHandler = kony.mvc.Business.CommandHandler;
    var Command = kony.mvc.Business.Command;
    var CommandResponse = kony.mvc.Business.CommandResponse;
    
    function setShowBillPayFromAccPopUp_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
    
    inheritsFrom(setShowBillPayFromAccPopUp_CommandHandler, CommandHandler);
  
    setShowBillPayFromAccPopUp_CommandHandler.prototype.execute = function(command){
        var self = this;
        function completionCallback(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) 
                self.sendResponse(command, status, response);
            else 
                self.sendResponse(command, status, err);
        }
       try {
            var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('User');
            var userObj = new userModel();
            for (var keys in command.context) {
                userObj[keys] = command.context[keys]==="undefined"?"null":command.context[keys];
            }
            userObj.partialUpdate(completionCallback);
        }catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
    
    setShowBillPayFromAccPopUp_CommandHandler.prototype.validate = function(){
        
    };
    
    return setShowBillPayFromAccPopUp_CommandHandler;
    
});