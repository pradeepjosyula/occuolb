define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;

  function BillPay_getBillPayees_CommandHandler(commandId) {
    CommandHandler.call(this, commandId);
  }

  inheritsFrom(BillPay_getBillPayees_CommandHandler, CommandHandler);

  BillPay_getBillPayees_CommandHandler.prototype.execute = function(command){
    var self = this;
    
    /**
     * Call back function for getByCriteria service.
     * @params  : status message, data, error message.
     */
    function getPayeeListCompletionCallback(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, err);
    }
    try {
      var payeeModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
      var params = {
        "searchString": command.context.searchString,
        "limit": command.context.limit,
        "offset": command.context.offset,
        "sortBy": command.context.sortBy || "payeeName",
        "order": command.context.order || "asc"
      };
      var criteria;
      if (params.searchString) {
        criteria = kony.mvc.Expression.eq("searchString", params.searchString);
      } else {
        criteria = kony.mvc.Expression.and(
          kony.mvc.Expression.eq("sortBy", params.sortBy),
          kony.mvc.Expression.eq("order", params.order)
        )
        if(params.limit) {
          criteria = kony.mvc.Expression.and(
            criteria,
            kony.mvc.Expression.eq("limit", params.limit),
            kony.mvc.Expression.eq("offset", params.offset)
          )
        }
      }
      payeeModel.getByCriteria(criteria, getPayeeListCompletionCallback);
    } catch (error) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }	 
  };

  BillPay_getBillPayees_CommandHandler.prototype.validate = function(){

  };

  return BillPay_getBillPayees_CommandHandler;

});