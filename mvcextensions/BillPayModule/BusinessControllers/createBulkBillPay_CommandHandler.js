define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_createBulkBillPay_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_createBulkBillPay_CommandHandler, CommandHandler);
  
  	BillPay_createBulkBillPay_CommandHandler.prototype.execute = function(command){
		var transactions  = command.context.transactionArray;
      	var transactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Transactions');
        transactions = JSON.stringify(transactions);
        transactions.replace("\"", "'");
      	var self = this;
      	function onCompletion(status, data, error){
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
      	transactionModel.customVerb('createBulkBillPay', {'bulkPayString': transactions}, onCompletion);
    };
	
	BillPay_createBulkBillPay_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_createBulkBillPay_CommandHandler;
    
});