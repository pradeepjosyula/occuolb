define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function BillPay_searchBillerByName_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(BillPay_searchBillerByName_CommandHandler, CommandHandler);
  
  	BillPay_searchBillerByName_CommandHandler.prototype.execute = function(command){
	  var self = this;	
      var BillPayModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("BillerMaster");
      
      function onCompletion(status, response, err){
        if (status == kony.mvc.constants.STATUS_SUCCESS) 
                self.sendResponse(command, status, response);
            else 
                self.sendResponse(command, status, err);
      }
      var params = {
        'searchString':command.context.searchString,
        'limit':command.context.limit
      };
      BillPayModel.customVerb('searchBillerByName', params, onCompletion);
    };
	
	BillPay_searchBillerByName_CommandHandler.prototype.validate = function(){
		
    };
    
    return BillPay_searchBillerByName_CommandHandler;
    
});