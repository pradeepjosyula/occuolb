define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;
  
    function BillPay_createBill_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(BillPay_createBill_CommandHandler, CommandHandler);
  
    BillPay_createBill_CommandHandler.prototype.execute = function(command){
    var self=this;
        function completionCallback(status,response,error){
            if(status==kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,response);
            }else{
              self.sendResponse(command,status,error);
            }
        }
      
        try{
          //billCategory should one of the following values: 'Credit Card','Phone','Utilities','Insurance' "
          var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
          var transactionObject=new transactionsModel(
          {
                'amount': command.context.amount,
                'billid': command.context.billid,
                'scheduledDate': command.context.scheduledDate,
                'payeeId': command.context.payeeId,
                'fromAccountNumber': command.context.fromAccountNumber,
                'transactionsNotes': command.context.notes,
                'transactionType': command.context.transactionType,
                'billCategory': command.context.billCategory,
                'toAccountNumber': command.context.toAccountNumber,
                'frequencyType' : command.context.frequencyType,
                'numberOfRecurrences' : command.context.numberOfRecurrences,
                'frequencyStartDate' : command.context.frequencyStartDate,
                'frequencyEndDate' :command.context.frequencyEndDate
          });
          transactionObject.save(completionCallback);
        }catch(err){
          self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
        }
    };
  
  BillPay_createBill_CommandHandler.prototype.validate = function(){
    
    };
    
    return BillPay_createBill_CommandHandler;
    
});