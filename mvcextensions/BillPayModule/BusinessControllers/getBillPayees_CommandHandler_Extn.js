define({

  addBefore:function(){

  },

  addAfter:function(){

  },
  execute:function (command) {
    var self = this;    
    /**
     * Call back function for getByCriteria service.
     * @params  : status message, data, error message.
     */
    function getPayeeListCompletionCallback(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, err);
    }
    try {
      var payeeModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
      var params = {
        "username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU
      }
      var criteria;
	  criteria = kony.mvc.Expression.eq("username", params.username);
      payeeModel.getByCriteria(criteria, getPayeeListCompletionCallback);
    } catch (error) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  }
});