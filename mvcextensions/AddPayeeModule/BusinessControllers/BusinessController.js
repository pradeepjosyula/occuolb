define([],function () {
	var BusinessController = kony.mvc.Business.Controller;
	var CommandExecutionEngine = kony.mvc.Business.CommandExecutionEngine;
	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	
  	function AddPayee_BusinessController(){
      	BusinessController.call(this);
    }

  	inheritsFrom(AddPayee_BusinessController,BusinessController);
	

		
	AddPayee_BusinessController.prototype.initializeBusinessController = function(){

    };

    AddPayee_BusinessController.prototype.execute = function(command)
    {
      BusinessController.prototype.execute.call(this,command);
    };

    return AddPayee_BusinessController;
});