define([], function () {

    var MDABasePresenter = kony.mvc.Presentation.BasePresenter;
    var BusinessController = kony.mvc.Business.Controller;
    var MDAFormController = kony.mvc.MDAFormController;
  
    function AddPayee_PresentationController() {
      MDABasePresenter.call(this);
    }
  
    inheritsFrom(AddPayee_PresentationController, kony.mvc.Presentation.BasePresenter);
  
    var payeeDetails ={
      init: function(){
        this.selectedBillerDetails = {
          billerId:null,
          billerName:null,
          billerCategoryName: null,
          billerAddress: null
        };
        //Biller Details if Biller not present
        this.billerId = null;
        this.billerName = null;//  PayeeName
        this.addressLine1 = null// (Street)
        this.addressLine2 = null;
        this.address= null;
        this.billerCategoryName= null;
        this.cityName = null;
        //this.state = null;
  
        //Payee Details
        this.zipCode = null;
        this.accountNumber = null;
        this.policyNumber = null;
        this.mobileNumber = null;
        this.payeeNickName = null;
        this.nameOnBill = null;
        this.note = null;
      },
  
      updateSelectedBiller: function(biller){
        this.selectedBillerDetails = biller;
      },
      isBillerDetailValid : function(payeeInfo){
        if(payeeInfo.isManualUpdate || (this.selectedBillerDetails.billerName != null 
                                        && this.selectedBillerDetails.billerName.trim() !== "" && this.selectedBillerDetails.billerName == payeeInfo.billerName)){
          return true;
        }
        return false;
      },
      isValidInput : function(payeeInfo){
        if(!this.selectedBillerDetails || !this.selectedBillerDetails.billerId){
          return "INVALID_BILLER";
        }
        if(this.selectedBillerDetails.billerName.trim() !== "" && this.selectedBillerDetails.billerName !== payeeInfo.billerName){
          return "INVALID_BILLER"
        }
        if(payeeInfo.accountNumber !== payeeInfo.accountNumberAgain){
          return this.billerCategoryName == 'Phone' ? "MISMATCH_RELATIONSHIPNUMBER" : "MISMATCH_ACCOUNTNUMBER";
        }
        return true;
      },
      updatePayeeDetails : function(payeeInfo){
        if(payeeInfo.isManualUpdate){
          this.selectedBillerDetails = null;
          this.billerId = null;
          this.billerName = payeeInfo.billerName;
          this.addressLine1 = payeeInfo.addressLine1;
          this.addressLine2 = payeeInfo.addressLine2;
          this.cityName = payeeInfo.cityName;
          //this.state = payeeInfo.state;
          this.billerAddress= this.addressLine1 +" " +this.addressLine2 +" "+ this.cityName;
  
          if(payeeInfo.noAccountNumber){
            this.note = payeeInfo.note;
            this.accountNumber= "";
          }else{
            this.note = null;
            this.accountNumber= payeeInfo.accountNumber;
          }
          this.mobileNumber = null;
  
        }else{
          //update selected biller
          this.billerId= this.selectedBillerDetails.billerId;
          this.billerName = this.selectedBillerDetails.billerName;
          this.billerAddress = this.selectedBillerDetails.billerAddress;
          this.billerCategoryName = this.selectedBillerDetails.billerCategoryName;
          this.addressLine1 = null;
          this.addressLine2 = null;
          this.cityName = null;                    
          //update account number
          if (this.billerCategoryName == 'Credit Card' || (this.billerCategoryName == 'Utilities')) {
            this.accountNumber = payeeInfo.accountNumber;
            this.mobileNumber = null;
          } else if (this.billerCategoryName == 'Phone') {
            this.accountNumber = payeeInfo.relationShipNumber || payeeInfo.accountNumber;
            this.mobileNumber = payeeInfo.mobileNumber;
          } else if (this.billerCategoryName == 'Insurence') {
            this.accountNumber = payeeInfo.accountNumber;
            this.mobileNumber = payeeInfo.policyNumber || payeeInfo.mobileNumber;
          }
        }
        this.nameOnBill = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userFirstName + " " + kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userLastName;
        this.payeeNickName = this.billerName;
        this.zipCode = payeeInfo.zipCode;
      },
      updateTransationId : function(transaction){
        this.transactionId = transaction.transactionId;
      },
      updatePayeeId: function(payee){
            this.payeeId = payee.payeeId;
      },
      updateConfirmPayeeDetails : function(payeeInfo){
        // If the user input empty retain the default value.
        if(payeeInfo.payeeNickName.trim() !==""){
          this.payeeNickName = payeeInfo.payeeNickName;
        }
        if(payeeInfo.nameOnBill.trim() !==""){
          this.nameOnBill = payeeInfo.nameOnBill;
        }
      },
      getTransationId : function(){
        return this.transactionId;
      },
      getOneTimePayeeInfo : function(){
        return {
          billerId : this.billerId,
          billerCategoryName : this.billerCategoryName,
          zipCode : this.zipCode,
          mobileNumber : this.mobileNumber,
          billerName : this.billerName,
          billerAddress : this.billerAddress,
          accountNumber : this.accountNumber,
        };
      },
      getRequestObject : function(){
        return {
          accountNumber: this.accountNumber,
          street : this.addressLine1,
          addressLine2 : this.addressLine2,
          cityName : this.cityName,
          payeeNickName: this.payeeNickName,
          zipCode: this.zipCode,
          //state : this.state,
          companyName : this.billerName,
          notes : this.note,
          nameOnBill : this.nameOnBill,
          billerId : this.billerId,
          phone : this.mobileNumber
        };
      },
      getPayeeDetailsToUpdatePage : function(){
        return {
          billerName : this.billerName,
          billerAddress : this.billerAddress,
          accountNumber : this.accountNumber,
          payeeNickName : this.payeeNickName,
          nameOnBill : this.nameOnBill
        };
      },
      getPayeeDetailsConfirmPage: function(){
        return {
          billerName : this.billerName,
          billerAddress : this.billerAddress,
          accountNumber : this.accountNumber,
          payeeNickName : this.payeeNickName,
          nameOnBill : this.nameOnBill
        };
      }, 
      getPayeeDetailsSuccessPage : function(){
        return {
          billerName : this.billerName,
          billerAddress : this.billerAddress,
          accountNumber : this.accountNumber,
          payeeNickName : this.payeeNickName,
          nameOnBill : this.nameOnBill
        };
      },
      getPayeeDetailsForPayment: function() {
        return {
          billerName: this.billerName,
          billerAddress: this.billerAddress,
          accountNumber: this.accountNumber,
          payeeNickname: this.payeeNickName,
          nameOnBill: this.nameOnBill,
          payeeId: this.payeeId,
          show: "PayABill"
        };
      }      
    };
    var lastSearchValue;
    AddPayee_PresentationController.prototype.fetchBillerList = function (searchValue) {
      var self=this;
      if(searchValue == null || searchValue.trim() == ""){
        return;
      }
      self.showProgressBar();
      lastSearchValue = searchValue;
      var completionCallback = function (commandResponse) {
        if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
          if(lastSearchValue === searchValue){ // because response is async.
            self.hideProgressBar();
            self.getBillerList(commandResponse.data);
          }
        } else {
          kony.print(commandResponse.data);
        }
      };
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.searchBillerByName", {
        searchString: searchValue,
        limit: 5
      }, completionCallback));
    };
  
    AddPayee_PresentationController.prototype.getBillerList = function(billersList){
      var self= this;
      var createBillerList = function (biller) {
        var billerObj=  {
          billerName: biller.billerName,
          billerId: biller.id,
          billerCategoryId: biller.billerCategoryId,
          billerCategoryName: biller.billerCategoryName,
          billerAddress: biller.address,
          onBillerSelection: function () {
            return self.updateBillerDetails(billerObj);
          }
        };
        return billerObj;
      };
      this.updateUI({billersList : billersList.map(createBillerList)});
    };
  
    AddPayee_PresentationController.prototype.updateBillerDetails = function(biller){
      this.updateUI({billerDetails : biller});
      payeeDetails.updateSelectedBiller(biller);        
    };
  
    AddPayee_PresentationController.prototype.navigateToAddPayee= function(){
      var initialConfig = {'firstLoad':true};
      payeeDetails.init();
      this.updateUI(initialConfig);
      this.getRegisteredPayeeList();  
      fetchHamburgerConfig(this.loadComponents.bind(this));
    };
    AddPayee_PresentationController.prototype.navigateToOneTimePayement= function(isCancelCallBack){
      if (isCancelCallBack !== true) {
        var initialConfig = {'initOneTimePayment':true};
        payeeDetails.init();
        this.updateUI(initialConfig);
        this.getRegisteredPayeeList();  
        fetchHamburgerConfig(this.loadComponents.bind(this));
      }else{
        this.updateUI({});
      }
    };  
    AddPayee_PresentationController.prototype.updateOneTimePayeeInfo = function(payeeInfo){
      var errorMessage;
      if((errorMessage = payeeDetails.isValidInput(payeeInfo)) === true){
        payeeDetails.updatePayeeDetails(payeeInfo);
        this.navigateToPayBills(payeeDetails.getOneTimePayeeInfo());
      }else{
        this.updateUI({validationError : errorMessage});
      }
    };
    AddPayee_PresentationController.prototype.showUpdateBillerPageWithTransaction= function(transaction){
        payeeDetails.updateTransationId(transaction);
        this.updateUI({payeeUpdateDetails : payeeDetails.getPayeeDetailsToUpdatePage()});
    };
    AddPayee_PresentationController.prototype.showUpdateBillerPage= function(payeeInfo){
      if(payeeInfo && !payeeDetails.isBillerDetailValid(payeeInfo)){
        this.updateUI({isInvalidPayee : true});
      }else{
        if(payeeInfo)payeeDetails.updatePayeeDetails(payeeInfo);
        this.updateUI({payeeUpdateDetails : payeeDetails.getPayeeDetailsToUpdatePage()});
      }
    };
    AddPayee_PresentationController.prototype.showAddPayeeConfirmPage= function(payeeInfo){
      payeeDetails.updateConfirmPayeeDetails(payeeInfo);
      this.updateUI({payeeConfirmDetails : payeeDetails.getPayeeDetailsConfirmPage()});
    };
    AddPayee_PresentationController.prototype.showAddPayeeSucess= function(response){
      if (response === undefined || response === null){
        this.addPayeeDetails();
      }else if(response.payeeId !== undefined && response.payeeId!==""){
		   payeeDetails.updatePayeeId({
                payeeId: response.payeeId
          });  
        if(payeeDetails.getTransationId()){
          this.updateOneTimePayment({
            payeeId : response.payeeId,
            transactionId : payeeDetails.getTransationId()
          });
        }
        this.updateUI({payeeSuccessDetails : payeeDetails.getPayeeDetailsSuccessPage()});  
      }else{
        this.showAddPayeeError(response);
      }
    };
    AddPayee_PresentationController.prototype.showAddPayeeError = function(response) {
      //show update Biller page with error message
      var errmsg = {
        payeeUpdateDetails: payeeDetails.getPayeeDetailsToUpdatePage()
      }
      if(response == undefined || !response.errmsg){
        errmsg.errorInAddingPayee = "Server error";
      }else{
        errmsg.errorInAddingPayee = response.errmsg;
      }
      this.updateUI(errmsg);
    }
  
    AddPayee_PresentationController.prototype.addPayeeDetails= function(){
      var self= this;
      var completionCallback = function (commandResponse) {
        if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
          self.showAddPayeeSucess(commandResponse.data);
        } else {
          self.showAddPayeeError(commandResponse.data);
        }
      };
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.createPayee", payeeDetails.getRequestObject(), completionCallback));
    };
  
    AddPayee_PresentationController.prototype.updateOneTimePayment= function(data){
      var completionCallback = function (commandResponse) {
        if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
          kony.print("one time payment updated successfully"+ data);
        } else {
          kony.print("one time payment fail"+ data);
        }
      };
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.updateOneTimePayment", data, completionCallback));
    };
  
    AddPayee_PresentationController.prototype.getRegisteredPayeeList = function(){
      var self= this;
      var completionCallback = function (commandResponse) {
        if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
          self.showRegisteredPayeeList(commandResponse.data);
        } else {
          kony.print(commandResponse.data);
        }
      };
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.billpay.getRegisteredPayees", {
        sortBy:"billDueDate",
        order:"desc"    
      }, completionCallback));
    }
  
    AddPayee_PresentationController.prototype.showRegisteredPayeeList = function(payeeList){
      var self= this;
      var createPayeeList = function (payee) {
        var payeeObj= 	{
          payeeName:  payee.payeeNickName || payee.companyName,
          lastPaidDate: payee.lastPaidDate,
          lastPaidAmount: payee.lastPaidAmount,
          payeeId: payee.payeeId,
          accountNumber: payee.accountNumber,
          billDueDate: payee.billDueDate,
          billid : payee.billid,
          dueAmount : payee.dueAmount,
          payeeNickname : payee.payeeNickName || payee.companyName,
          eBillStatus : payee.eBillStatus,
          show: "PayABill",
          onViewDetailsClick : function(){
            //self.navigateToBillPayee(payeeObj);
		  self.showManagePayee();
          },
          onPayBillsClick : function(){
            self.makePayment(payeeObj);
          }
  
        };
        return payeeObj;
      };
      this.updateUI({registeredPayeeList : payeeList.map(createPayeeList)});
    }; 
	
	AddPayee_PresentationController.prototype.showManagePayee = function() {
        var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        BillPayModule.presentationController.showBillPayData(null, {
            show: "ManagePayees"
        })
    };
	
 	AddPayee_PresentationController.prototype.makePayment= function(payee){
      var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      if(payee){
        BillPayModule.presentationController.showBillPayData(BillPayModule.presentationController, payee);
        }
      else{
        BillPayModule.presentationController.showBillPayData(BillPayModule.presentationController, payeeDetails.getPayeeDetailsForPayment());
      }
    };
  
    AddPayee_PresentationController.prototype.navigateToPayBills= function(payee){
      var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      BillPayModule.presentationController.showOneTimePayment(payee);
    };

  AddPayee_PresentationController.prototype.updateUI= function(data){
      this.presentUserInterface('frmAddPayee', data);
    };
    var fetchHamburgerConfig = function (onSuccess) {
      onSuccess();
    };  
    AddPayee_PresentationController.prototype.loadComponents = function (config) {  
      var howToShowHamburgerMenu = function(sideMenuViewModel){
        this.updateUI({sideMenu : sideMenuViewModel});        
      };
      this.SideMenu.init(howToShowHamburgerMenu.bind(this)); 
  
      var presentTopBar = function(topBarViewModel) {
        this.updateUI({topBar : topBarViewModel});
      };
      this.TopBar.init(presentTopBar.bind(this)); 

    };   
    AddPayee_PresentationController.prototype.showProgressBar = function () {
      var self = this;
      self.updateUI({ "ShowProgressBar": true});
    };

    AddPayee_PresentationController.prototype.hideProgressBar = function () {
      var self = this;
      self.updateUI({ "HideProgressBar": true});
    };
    return AddPayee_PresentationController;
  });