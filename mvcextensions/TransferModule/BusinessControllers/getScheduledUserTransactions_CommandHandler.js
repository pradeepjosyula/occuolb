define([], function() {

  	function Transfer_getScheduledUserTransactions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_getScheduledUserTransactions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Transfer_getScheduledUserTransactions_CommandHandler.prototype.execute = function(command){
		var self = this;

        function completionCallBack(status, response, error) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) 
              	self.sendResponse(command, status, response);
            else 
              	self.sendResponse(command, status, error);
        }

        var inputParams = {
            "firstRecordNumber": command.context.offset,
            "lastRecordNumber": command.context.limit,
            "isScheduled": 1,
        };
        if(command.context.sortBy)
            inputParams["sortBy"] = command.context.sortBy;
        if(command.context.order)
            inputParams["order"] = command.context.order;

        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        TransactionsModel.customVerb("getScheduledUserTransactions", inputParams, completionCallBack);
    };
	
	Transfer_getScheduledUserTransactions_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_getScheduledUserTransactions_CommandHandler;
    
});