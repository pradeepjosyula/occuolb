define([], function() {

  	function Transfer_makeTrialDeposit_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_makeTrialDeposit_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Transfer_makeTrialDeposit_CommandHandler.prototype.execute = function(command){
		 var self = this;
		var accNo = {"ExternalAccountNumber":command.context};
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");

        function completionCallback(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) self.sendResponse(command, status, response);
            else self.sendResponse(command, status, err);
        }
        TransactionsModel.customVerb("makeTrialDeposit", accNo, completionCallback);
    };
	
	Transfer_makeTrialDeposit_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_makeTrialDeposit_CommandHandler;
    
});