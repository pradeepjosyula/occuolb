define([], function () {

  function handleDateFormat (date) {
    if(date == undefined || date == null || date === ''){
      return "";
    }
    if(date instanceof Date){
      return ((date.getMonth()+1) < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1) +"/"+(date.getDate() < 10 ? '0' + date.getDate() : date.getDate())+"/"+date.getFullYear();    
    } else {
      var dateObj  = new Date(date); 
      return ((dateObj.getMonth()+1) < 10 ? '0' + (dateObj.getMonth()+1) : dateObj.getMonth()+1)+"/"+(dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate())+"/"+dateObj.getFullYear();         
    }
  }


  function Transfer_scheduleTransfer_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Transfer_scheduleTransfer_CommandHandler, kony.mvc.Business.CommandHandler);

  Transfer_scheduleTransfer_CommandHandler.prototype.execute = function (command) {
    var self = this;

    function onScheduleTransfer (status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var context = command.context;
    var transfersModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transfer");
    if(context.frequencyType === "BiWeekly"){
      context.frequencyType = "Every 2 Weeks";
    } else if(context.frequencyType === "QUARTERLY"){   
      context.frequencyType = "Quarterly";
    } else if(context.frequencyType === "HALF_YEARLY"){  
      context.frequencyType = "Semi Annually";
    } else if(context.frequencyType === "YEARLY"){  
      context.frequencyType = "Annually";
    }
    var newTransaction = {
      "username": context.username,
      "fromAccountNumber":  context.fromAccountNumber,
      "amount":context.amount,
      "toAccountNumber": context.toAccountNumber,
      "frequency": context.frequencyType,
      "day1": "",
	  "day2": "",
      "startDate": handleDateFormat(context.scheduledDate),  //handleDateFormat(context.frequencyStartDate)
      "endDate": handleDateFormat(context.frequencyEndDate),
      "fromProductType": context.fromProductType,
      "fromProductId": context.fromProductId,
      "toProductType": context.toProductType,
      "toProductId": context.toProductId,
      "comment": "" 
    };
    //newTransaction.save(onScheduleTransfer);
    transfersModel.customVerb('scheduleTransfer',newTransaction,onScheduleTransfer);
  };
  

  Transfer_scheduleTransfer_CommandHandler.prototype.validate = function () {

  };

  return Transfer_scheduleTransfer_CommandHandler;

});