define({

    addBefore:function(){

    },

    addAfter:function(){
        
    },
  	
   execute: function(command){
        var  self  =  this;

        /**
         * Call back function for getAll service.
         * @params  : status message, data, error message.
         */
        function completionCallback(status,data,error){
			self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
		}
		try{
			//var deleteAccountModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccounts");
            var deleteAccountModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Recipient");
	    	//deleteAccountModel.customVerb("deleteExternalAccount",{accountNumber:command.context.accountNumber},completionCallback); 
            deleteAccountModel.customVerb("removeRecipient",{"username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,"accountNumber": command.context.accountNumber,"productType": command.context.accountType,"productId": command.context.shareId,"accountLastName": command.context.lastName,"description":""},completionCallback);
          
    	}catch(err){
    		kony.print(err);
    	}
    }

});