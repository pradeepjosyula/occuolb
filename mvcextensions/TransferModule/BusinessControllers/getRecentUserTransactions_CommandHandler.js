define([], function() {

  	function Transfer_getRecentUserTransactions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_getRecentUserTransactions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Transfer_getRecentUserTransactions_CommandHandler.prototype.execute = function(command){
      var self = this;
  		
      function completionCallBack(status, response, error){
        if(status === kony.mvc.constants.STATUS_SUCCESS)
        	self.sendResponse(command,status,response);
        else
          	self.sendResponse(command,status,error);
      }
		var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        TransactionsModel.customVerb("getRecentUserTransactions", { 
            "firstRecordNumber": command.context.offset, 
            "lastRecordNumber": command.context.limit, 
            "isScheduled": 0,
            "sortBy" : command.context.sortBy,
            "order" : command.context.order
        }, completionCallBack);
            
    };
	
	Transfer_getRecentUserTransactions_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_getRecentUserTransactions_CommandHandler;
    
});