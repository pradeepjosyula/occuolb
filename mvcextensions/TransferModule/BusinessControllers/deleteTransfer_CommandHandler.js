define([], function() {

    function Transfer_deleteTransfer_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Transfer_deleteTransfer_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Transfer_deleteTransfer_CommandHandler.prototype.execute = function(command){
    var self = this;
        var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        function onCompletionCallback(status,response,error){
          if(status === kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,error);
          }
           else{
               self.sendResponse(command,status,error);
           } 
        }
      
        TransactionModel.removeById(command.context.transactionId, onCompletionCallback);
    };
  
  Transfer_deleteTransfer_CommandHandler.prototype.validate = function(){
    
    };
    
    return Transfer_deleteTransfer_CommandHandler;
    
});