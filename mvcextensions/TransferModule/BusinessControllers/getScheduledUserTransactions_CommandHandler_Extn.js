define({

    addBefore:function(){

    },

    addAfter:function(){
        
    },
  	
   execute: function(command){
       var self = this;

        function completionCallBack(status, response, error) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) 
              	self.sendResponse(command, status, response);
            else 
              	self.sendResponse(command, status, error);
        }

        var inputParams = {
//             "firstRecordNumber": command.context.offset,
//             "lastRecordNumber": command.context.limit,
//             "isScheduled": 1,
            "username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU 
          
          
        };
        if(command.context.sortBy)
            inputParams["sortBy"] = command.context.sortBy;
        if(command.context.order)
            inputParams["order"] = command.context.order;

        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transfer");
        TransactionsModel.customVerb("getScheduledTransfers", inputParams, completionCallBack);
    }

});