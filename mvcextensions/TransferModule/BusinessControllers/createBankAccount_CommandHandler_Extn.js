define(['commonUtilitiesOCCU'], function (commonUtilitiesOCCU) {
return{
  addBefore: function(){},
  addAfter: function(){},
  execute :function(command){
    
    var self=this;
   if(command.context.isSameBankAccount === "true"){
    var recipientModel=kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Recipient"); //Retrieve the model definition of ExternalAccounts
    //var internalAccountData=new ExternalAccountsModel({accountNumber : command.context.reAccountNumber}); // Fill in details for the new instance of retrieved Model
    //internalAccountData.productType = commonUtilitiesOCCU.getAccountTypeKey(command.context.accountType);
    //internalAccountData.accountLastName = command.context.nickName;
    //internalAccountData.username = kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU;
    //internalAccountData.productId = command.context.beneficiaryName;
    //internalAccountData.accountId = command.context.reAccountNumber;
    //internalAccountData.description = "";
    // alert(JSON.stringify(internalAccountData));

    var params = {
            "accountNumber": command.context.reAccountNumber,
            "productType": commonUtilitiesOCCU.getAccountTypeKey(command.context.accountType),
            "accountLastName": command.context.nickName,
            "username": kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
            "productId": command.context.beneficiaryName,
            "description": ""  
    };
    function completionCallback(status,response,error){
      if(status==kony.mvc.constants.STATUS_SUCCESS){ //check for the success and failure response from service
        // alert("successcallback in commandHandler"+status);
        self.sendResponse(command,kony.mvc.constants.STATUS_SUCCESS,response);
      }else{
        // alert("errorcallback in commandHandler"+status);
        self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,error);
      }
    }
      recipientModel.customVerb('createRecipient',params,completionCallback);
    //internalAccountData.save(completionCallback); //call the save(MDA command) to fill information into the model
}
    else{
       var accountcat = command.context.accountType;
       if(command.context.accountType ==="Checking"){
          accountcat = "0";
       } else {
          accountcat = "1";
       }
       var externalModel=kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts"); //Retrieve the model definition of ExternalAccounts
       var params = {
            "accountID": command.context.accountNumber,
			"routingNumber": command.context.routingNumber,
            "nickName": command.context.nickName,
			"accountType": accountcat,
            "userName": kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
       };
      
    function completionCallback(status,response,error){
      if(status==kony.mvc.constants.STATUS_SUCCESS){ //check for the success and failure response from service
        // alert("successcallback in commandHandler"+status);
        self.sendResponse(command,kony.mvc.constants.STATUS_SUCCESS,response);
      }else{
        // alert("errorcallback in commandHandler"+status);
        self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,error);
      }

    }
    externalModel.customVerb('addExternalAccount',params,completionCallback);
    }
    
    
  }
   };
});