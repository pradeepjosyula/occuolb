define([], function() {

  	function Transfer_verifyCredentials_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_verifyCredentials_CommandHandler, kony.mvc.Business.CommandHandler);

  Transfer_verifyCredentials_CommandHandler.prototype.execute = function(command){
    var self = this;	
    var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");

    function completionCallback(status, response, err) {
      if(status===kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, err);
    }
    TransactionsModel.customVerb("verifyExternalBankAccount", command.context.data, completionCallback);
  };

	Transfer_verifyCredentials_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_verifyCredentials_CommandHandler;
    
});