define([], function() {

    function Transfer_viewTransactionAccountActivity_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Transfer_viewTransactionAccountActivity_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Transfer_viewTransactionAccountActivity_CommandHandler.prototype.execute = function(command){
      var self=this;
  function completionCallback(status,data,error){
     self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
      try{
      var TransfersModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        TransfersModel.customVerb("getToExternalAccountTransactions",{
              "accountNumber":command.context.accountNumber,
              "firstRecordNumber":0,
              "lastRecordNumber":1000
            },completionCallback);  
      }catch(err){
      }
    };
  
  Transfer_viewTransactionAccountActivity_CommandHandler.prototype.validate = function(){
    
    };
    
    return Transfer_viewTransactionAccountActivity_CommandHandler;
    
});