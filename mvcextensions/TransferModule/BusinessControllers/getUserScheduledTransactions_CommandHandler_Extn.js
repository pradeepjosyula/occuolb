define({
   
    addBefore:function(){

    },

    addAfter:function(){
        
    },
    
    execute:function(command){
      var self=this;
      
  		function completionCallback(status,data,error){
     		self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    	}
      try{
      var TransfersModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        TransfersModel.customVerb("getUserScheduledTransactions", {"username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU}, completionCallback);  
      }catch(err){
        kony.print(err);
      }
    }
});