define([], function() {

    function Transfer_verifyByTrialDeposit_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Transfer_verifyByTrialDeposit_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Transfer_verifyByTrialDeposit_CommandHandler.prototype.execute = function(command){
      var self=this;
    function completionCallback(status,data,error){
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
        }
      var params={
          "accountNumber":command.context.accountNumber,
          "firstDeposit":command.context.firstDeposit,
          "secondDeposit":command.context.secondDeposit
        };
      try{
          var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        TransactionModel.customVerb("verifyTrialDeposit",params,completionCallback);  
        }catch(err){
        }
    };
  
  Transfer_verifyByTrialDeposit_CommandHandler.prototype.validate = function(){
    
    };
    
    return Transfer_verifyByTrialDeposit_CommandHandler;
    
});