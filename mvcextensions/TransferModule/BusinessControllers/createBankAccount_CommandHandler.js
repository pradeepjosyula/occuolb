define([], function() {

    function Transfer_createBankAccount_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Transfer_createBankAccount_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Transfer_createBankAccount_CommandHandler.prototype.execute = function(command){
      var self=this;
    var ExternalAccountsModel=kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccounts"); //Retrieve the model definition of ExternalAccounts
        var internalAccountData=new ExternalAccountsModel({accountNumber : command.context.accountNumber}); // Fill in details for the new instance of retrieved Model
        internalAccountData.productType=command.context.accountType;
        //internalAccountData.bankName=command.context.bankName;
        internalAccountData.accountLastName=command.context.nickName;
     	internalAccountData.username = "cwarde46";
      	internalAccountData.productId = command.context.beneficiaryName;
      	//internalAccountData.description = command.context.description;
        internalAccountData.accountId = command.context.reAccountNumber;
        //internalAccountData.nickName=command.context.nickName;
        //internalAccountData.isSameBankAccount=command.context.isSameBankAccount;
        //internalAccountData.routingNumber=command.context.routingNumber;
        //internalAccountData.swiftCode=command.context.swiftCode;
        //internalAccountData.isInternationalAccount=command.context.isInternationalAccount;
        //internalAccountData.isVerified=command.context.isVerified;
       // alert(JSON.stringify(internalAccountData));
        
        function completionCallback(status,response,error){
          if(status==kony.mvc.constants.STATUS_SUCCESS){ //check for the success and failure response from service
           // alert("successcallback in commandHandler"+status);
            self.sendResponse(command,kony.mvc.constants.STATUS_SUCCESS,response);
          }else{
           // alert("errorcallback in commandHandler"+status);
            self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,error);
          }
          
        }
        internalAccountData.save(completionCallback); //call the save(MDA command) to fill information into the model
    };
  
  Transfer_createBankAccount_CommandHandler.prototype.validate = function(){
    
    };
    
    return Transfer_createBankAccount_CommandHandler;
    
});