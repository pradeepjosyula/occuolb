define({
   
    addBefore:function(){

    },

    addAfter:function(){
        
    },
    
    execute:function(command){
      var self=this;
      
  		function completionCallback(status,data,error){
     		self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    	}
		var params={
          "accountNumber":command.context.accountNumber.text,
          "firstDeposit":command.context.firstDeposit,
          "secondDeposit":command.context.secondDeposit,
          "username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU
        };
      try{
      var TransfersModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Recipient");
        TransfersModel.customVerb("activateExternalRecipient", params, completionCallback);  
      }catch(err){
        kony.print(err);
      }
    }
});