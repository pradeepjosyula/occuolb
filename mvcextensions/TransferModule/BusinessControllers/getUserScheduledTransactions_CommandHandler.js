define([], function() {

    function Transfer_getUserScheduledTransactions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Transfer_getUserScheduledTransactions_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Transfer_getUserScheduledTransactions_CommandHandler.prototype.execute = function(command){
      var self=this;
      
  		function completionCallback(status,data,error){
     		self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    	}
      try{
      var TransfersModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        TransfersModel.customVerb("getUserScheduledTransactions", {}, completionCallback);  
      }catch(err){
        kony.print(err);
      }
    };
  
  Transfer_getUserScheduledTransactions_CommandHandler.prototype.validate = function(){
    
    };
    
    return Transfer_getUserScheduledTransactions_CommandHandler;
    
});