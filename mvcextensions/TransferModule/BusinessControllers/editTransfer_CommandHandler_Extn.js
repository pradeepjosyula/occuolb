define(['CommonUtilities'], function (CommonUtilities) {
return{

  	 addBefore:function(){

    },

    addAfter:function(){
        
    },
  	
	
    handleDateFormat: function(date) {
        if (date === undefined || date ===null || date === '') {
            return null;
        }
        if (date instanceof Date) {
            return date.getFullYear() + '-' + (date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth()) + '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
        } else {
            var dateObj = new Date(date);
            return dateObj.getFullYear() + '-' + (dateObj.getMonth() < 10 ? '0' + dateObj.getMonth() : dateObj.getMonth()) + '-' + (dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate());
        }
    },
    
  
  execute : function(command) {
    var self = this;

    function onEditTransfer(status, data, error) {
      if (status === kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, status, data);
      } else self.sendResponse(command, status, error);
    }
    var context = command.context;
    var accCode=command.context.toAccountNumber.split("-")[0];
    var x="X";
    var code=accCode.slice(6);
    var accountCode=x+code;
    var date = CommonUtilities.getFrontendDateString(context.scheduledDate,kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'));
    var modifiedDate = CommonUtilities.sendDateToBackend(date,kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'), kony.onlineBanking.configurations.getConfiguration('backendDateFormat'));
    var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transfer");
    var transactionOBJ = {
      'scheduledTransferId': context.transactionId,
//       'isScheduled': context.isScheduled,
      'externalAccountId': context.fromAccountNumber,
      'amount': context.amount,
      'description': context.transactionsNotes,
      'toAccount': accountCode,
      'accountCode':context.toAccountNumber,
      'frequency': context.frequencyType,
      'transactionType': context.transactionType,
      'startDate':context.transactionDate,
      'occurrences': context.numberOfRecurrences,
//       'frequencyStartDate': this.handleDateFormat(context.frequencyStartDate),
//       'frequencyEndDate': this.handleDateFormat(context.frequencyEndDate),
      'endDate':"",
//       'category': context.Category,
      'username':kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU
    };
    transactionsModel.customVerb("editExternalScheduledTransfer",transactionOBJ,onEditTransfer);

  }
	
}   
});