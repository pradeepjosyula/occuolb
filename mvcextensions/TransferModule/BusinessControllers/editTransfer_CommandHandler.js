define(['CommonUtilities'], function(CommonUtilities) {

  	function Transfer_editTransfer_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    function handleDateFormat(date) {
        if (date === undefined || date === null || date === '') {
            return null;
        }
        if (date instanceof Date) {
            return date.getFullYear() + '-' + (date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth()) + '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
        } else {
            var dateObj = new Date(date);
            return dateObj.getFullYear() + '-' + (dateObj.getMonth() < 10 ? '0' + dateObj.getMonth() : dateObj.getMonth()) + '-' + (dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate());
        }
    }
    inheritsFrom(Transfer_editTransfer_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Transfer_editTransfer_CommandHandler.prototype.execute = function(command) {
        var self = this;

        function onEditTransfer(status, data, error) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, data);
            } else self.sendResponse(command, status, error);
        }
        var context = command.context;
        var date = CommonUtilities.getFrontendDateString(context.scheduledDate,kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'));
        var modifiedDate = CommonUtilities.sendDateToBackend(date,kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'), kony.onlineBanking.configurations.getConfiguration('backendDateFormat'));
        var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        var transactionOBJ = new transactionsModel({
            'transactionId': context.transactionId,
            'isScheduled': context.isScheduled,
            'fromAccountNumber': context.fromAccountNumber,
            'amount': context.amount,
            'transactionsNotes': context.transactionsNotes,
            'toAccountNumber': context.toAccountNumber,
            'frequencyType': context.frequencyType,
            'transactionType': context.transactionType,
            'scheduledDate':modifiedDate,
            'numberOfRecurrences': context.numberOfRecurrences,
            'frequencyStartDate': handleDateFormat(context.frequencyStartDate),
            'frequencyEndDate': handleDateFormat(context.frequencyEndDate),
            'category': context.Category
        });
        transactionOBJ.partialUpdate(onEditTransfer);
    };
	Transfer_editTransfer_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_editTransfer_CommandHandler;
    
});