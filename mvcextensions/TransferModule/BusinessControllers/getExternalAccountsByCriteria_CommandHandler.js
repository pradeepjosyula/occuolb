define([], function() {

  	function Transfer_getExternalAccountsByCriteria_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_getExternalAccountsByCriteria_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Transfer_getExternalAccountsByCriteria_CommandHandler.prototype.execute = function(command){
        var  self  =  this;
        var searchString = command.context.searchString;

        /**
         * Call back function for getById service.
         * @params  : status message, data, error message.
         */
        function completionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try  {
          var  accountModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccounts");
          var criteria = kony.mvc.Expression.eq("searchString", searchString );
            accountModel.getByCriteria(criteria, completionCallback)
        }  catch  (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
		
    };
	
	Transfer_getExternalAccountsByCriteria_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_getExternalAccountsByCriteria_CommandHandler;
    
});