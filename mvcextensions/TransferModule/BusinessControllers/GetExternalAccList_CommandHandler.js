define([], function() {

    function Transfer_GetExternalAccList_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Transfer_GetExternalAccList_CommandHandler, kony.mvc.Business.CommandHandler);
  
    Transfer_GetExternalAccList_CommandHandler.prototype.execute = function(command){
        var  self  =  this;

        /**
         * Call back function for getAll service.
         * @params  : status message, data, error message.
         */
        function  getAllCompletionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try  {
          var transfersModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
          transfersModel.customVerb('getExternalAccountsOptions',{"userName":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU},getAllCompletionCallback);
        }  catch  (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
  
  Transfer_GetExternalAccList_CommandHandler.prototype.validate = function(){
    
    };
    
    return Transfer_GetExternalAccList_CommandHandler;
    
});