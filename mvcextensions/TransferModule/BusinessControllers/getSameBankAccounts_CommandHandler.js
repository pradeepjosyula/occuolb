define([], function() {

  	function Transfer_getSameBankAccounts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_getSameBankAccounts_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Transfer_getSameBankAccounts_CommandHandler.prototype.execute = function(command){
		var self = this;	
      	var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccounts");
        
        function completionCallback(status, response, err) {
            if(status===kony.mvc.constants.STATUS_SUCCESS)
              self.sendResponse(command, status, response);
          	else
              self.sendResponse(command, status, err);
        }
        TransactionsModel.customVerb("getSameBankAccount", {}, completionCallback);
    };
	
	Transfer_getSameBankAccounts_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_getSameBankAccounts_CommandHandler;
    
});