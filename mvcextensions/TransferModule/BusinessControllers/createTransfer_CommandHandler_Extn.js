define({

  addBefore:function(){

  },

  addAfter:function(){

  },
  handleDateFormat:function (date) {
    if(date === undefined || date === null || date === ''){
      return null;
    }
    if(date instanceof Date){
      return ((date.getMonth()+1) < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1) +"/"+(date.getDate() < 10 ? '0' + date.getDate() : date.getDate())+"/"+date.getFullYear();    
    } else {
      var dateObj  = new Date(date); 
      return ((dateObj.getMonth()+1) < 10 ? '0' + (dateObj.getMonth()+1) : dateObj.getMonth()+1)+"/"+(dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate())+"/"+dateObj.getFullYear();         
    }
  },
  execute:function (command) {
    var self = this;

    function onCreateTransfer (status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var context = command.context;
    var transfersModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transfer");
    var newTransaction = {
            "fromAccountNumber": context.fromAccountNumber,
            "amount": context.amount,
            "toAccountNumber": context.toAccountNumber,
            "username": context.username,
            "fromProductType": context.fromProductType,
            "fromProductId": context.fromProductId,
            "toProductType": context.toProductType,
            "toProductId": context.toProductId,
            "toAccountLastName": context.toAccountLastName,
            "comment": ""      
    };
    //newTransaction.save(onCreateTransfer);
    transfersModel.customVerb('createTransfer',newTransaction,onCreateTransfer);
  }

});