define([], function() {

  	function Transfer_deleteAccount_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_deleteAccount_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Transfer_deleteAccount_CommandHandler.prototype.execute = function(command){
		var self=this;
		function completionCallback(status,data,error){
			self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
		}
		try{
			var deleteAccountModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccounts");
	    	deleteAccountModel.customVerb("deleteExternalAccount",{accountNumber:command.context.accountNumber},completionCallback);  
    	}catch(err){
    	}
    };
	
	Transfer_deleteAccount_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_deleteAccount_CommandHandler;
    
});