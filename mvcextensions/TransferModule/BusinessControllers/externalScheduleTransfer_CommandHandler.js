define([], function () {

  function handleDateFormat (date) {
    if(date == undefined || date == null || date === ''){
      return "";
    }
    if(date instanceof Date){
      return ((date.getMonth()+1) < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1) +"/"+(date.getDate() < 10 ? '0' + date.getDate() : date.getDate())+"/"+date.getFullYear();    
    } else {
      var dateObj  = new Date(date); 
      return ((dateObj.getMonth()+1) < 10 ? '0' + (dateObj.getMonth()+1) : dateObj.getMonth()+1)+"/"+(dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate())+"/"+dateObj.getFullYear();         
    }
  }
  
  function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;   //calculates months between two years
    months -= d1.getMonth() + 1; 
    months += d2.getMonth();  //calculates number of complete months between two months
    day1 = 30-d1.getDate();  
    day2 = day1 + d2.getDate();
    months += parseInt(day2/30);  //calculates no of complete months lie between two dates
    return months <= 0 ? 0 : months;
 }
   
  function Transfer_externalScheduleTransfer_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Transfer_externalScheduleTransfer_CommandHandler, kony.mvc.Business.CommandHandler);

  Transfer_externalScheduleTransfer_CommandHandler.prototype.execute = function (command) {
    var self = this;

    function onScheduleTransfer (status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var context = command.context;
    var transferType = 0;
    var externalAccountId = "";
    var externalAccountCode = "";
    var numberofrec = 1;
    if (context.numberOfRecurrences !== null && context.numberOfRecurrences !== "" && context.numberOfRecurrences !== "null") {
              numberofrec = context.numberOfRecurrences;
			  if(context.frequencyType === "BiWeekly"){
				context.frequencyType = "Every 2 Weeks";
			  } else if(context.frequencyType === "QUARTERLY"){   
				context.frequencyType = "Quarterly";
			  } else if(context.frequencyType === "HALF_YEARLY"){  
				context.frequencyType = "Semi Annually";
			  } else if(context.frequencyType === "YEARLY"){  
				context.frequencyType = "Annually";
			  }
        } else {
              var startDate =  new Date(handleDateFormat(context.scheduledDate));
			  var endDate = new Date(handleDateFormat(context.frequencyEndDate));
			  if(context.frequencyType === "Weekly"){        
				numberofrec = Math.round((endDate - startDate) / (7 * 24 * 60 * 60 * 1000));   
			  } else if(context.frequencyType === "BiWeekly"){
				numberofrec = Math.round(((endDate - startDate) / (7 * 24 * 60 * 60 * 1000))/2);  
				context.frequencyType = "Every 2 Weeks";
			  } else if(context.frequencyType === "Monthly"){
				numberofrec = monthDiff(startDate,endDate);   
			  } else if(context.frequencyType === "QUARTERLY"){
				numberofrec = Math.round(monthDiff(startDate,endDate)/4);   
				context.frequencyType = "Quarterly";
			  } else if(context.frequencyType === "HALF_YEARLY"){
				numberofrec = Math.round(monthDiff(startDate,endDate)/6);   
				context.frequencyType = "Semi Annually";
			  }  else if(context.frequencyType === "YEARLY"){
				numberofrec = Math.round(monthDiff(startDate,endDate)/12);   
				context.frequencyType = "Annually";
			  }
        }
   if(context.fromIsExternal !== null && context.fromIsExternal !== "" && context.fromIsExternal !== "null"){
     if (context.fromIsExternal === true || context.fromIsExternal === "true") {
       transferType = 1;
       externalAccountId = context.FromExternalAccountId;
       externalAccountCode = context.ToExternalAccountId;
     } else {
       externalAccountId = context.ToExternalAccountId;
       externalAccountCode = context.FromExternalAccountId;
     }
   } else {
     externalAccountId = context.ToExternalAccountId;
     externalAccountCode = context.FromExternalAccountId;
   }
    var transfersModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transfer");
    var newTransaction = {
      "username": context.username,
      "accountCode":  externalAccountCode,
      "amount":context.amount,
      "externalAccountId": externalAccountId,
      "frequency": context.frequencyType,
      "tansferType": transferType,
      "numberOfTransfers": numberofrec,
      "processingDate": handleDateFormat(context.scheduledDate),
      "endDate": handleDateFormat(context.frequencyEndDate),
      "comment": "" 
    };

    transfersModel.customVerb('scheduleExternalTransfer',newTransaction,onScheduleTransfer);
  };
  

  Transfer_externalScheduleTransfer_CommandHandler.prototype.validate = function () {

  };

  return Transfer_externalScheduleTransfer_CommandHandler;

});