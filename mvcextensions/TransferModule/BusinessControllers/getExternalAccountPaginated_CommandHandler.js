define([], function() {

  	function Transfer_getExternalAccountPaginated_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_getExternalAccountPaginated_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Transfer_getExternalAccountPaginated_CommandHandler.prototype.execute = function(command){
		
  		var self=this;
		function completionCallback(status,data,error){
			self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
		}
		try{
			var TransfersModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccounts");
	    	TransfersModel.customVerb("getAllExternalAccountsWithPagination",{offset:command.context.offset,limit:command.context.limit, sortBy:command.context.sortBy, order:command.context.order,"username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU},completionCallback);  
    	}catch(err){
    		kony.print(err);
    	}
    };
	
	Transfer_getExternalAccountPaginated_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_getExternalAccountPaginated_CommandHandler;
    
});