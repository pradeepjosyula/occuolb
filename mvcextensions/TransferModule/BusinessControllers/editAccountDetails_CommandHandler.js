define([], function() {

  function Transfer_editAccountDetails_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Transfer_editAccountDetails_CommandHandler, kony.mvc.Business.CommandHandler);

  Transfer_editAccountDetails_CommandHandler.prototype.execute = function(command){

    var self=this;
    function completionCallback(status,data,error){
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);      
    }
    var params={
      "accountNumber":command.context.accountNumber,
      "accountType":command.context.accountType,
      "nickName":command.context.nickName,
      "isVerified":command.context.isVerified
    };
    try{
      var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalAccounts");
      TransactionsModel.customVerb('editExternalAccount',params,completionCallback);
    }catch(err){
    }
  };

  Transfer_editAccountDetails_CommandHandler.prototype.validate = function(){

  };

  return Transfer_editAccountDetails_CommandHandler;

});