define([], function() {

  	function Transfer_getAllPayee_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Transfer_getAllPayee_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Transfer_getAllPayee_CommandHandler.prototype.execute = function(command){
		var self = this;

      function getPayeeCompletionCallback(status, data, error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
      }

      try {
        var payeeModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
        payeeModel.getAll(getPayeeCompletionCallback);
      } catch (error) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
      }
    };
	
	Transfer_getAllPayee_CommandHandler.prototype.validate = function(){
		
    };
    
    return Transfer_getAllPayee_CommandHandler;
    
});