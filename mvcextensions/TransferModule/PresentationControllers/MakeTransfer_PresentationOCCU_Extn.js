define(['CommonUtilities'],function (CommonUtilities) {
  var presentWith = null;
  var navigateToConfirmTransfer = null;
  var navigateToTransferAcknowledge = null;
  var accountsFrom = [];
  var accountsTo = [];
  var userAccounts = [];
  var externalAccounts = [];
  var externalOtherAccounts = [];
  var transferToTypeSelected = null;
  var initialContext = {};
  var userObject = null;

  var businessController = null;


  var cancelTransaction = function (viewModel) {
    viewModel.selectedTransferToType = null;
    presentWith(viewModel);
  }

  var newTransfer = function  (viewModel) {
    viewModel.selectedTransferToType = null;
    presentWith(viewModel);
  }

  var onTransferToTypeSelected = function (type) {
    transferToTypeSelected = type;
    showTransfersForm(type);
  };

  function getDateObject (dateString) {
    if (CommonUtilities.getConfiguration('frontendDateFormat') === "mm/dd/yyyy") {
      return new Date(dateString);
    }
    else if (CommonUtilities.getConfiguration('frontendDateFormat') === "dd/mm/yyyy")
    {
      var dateParts = dateString.split("/");
      var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
      return dateObject;
    }
    else {
      return new Date();
    }
  }



  function getCurrDateString () {
    var currDate = new Date();
    var month = currDate.getMonth() + 1;
    if (CommonUtilities.getConfiguration("frontendDateFormat") === "mm/dd/yyyy") {
      return  (month < 10 ? '0' + month : month) + "/" +  (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/"  + currDate.getFullYear()
    }
    else {
      return (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/" + (month < 10 ? '0' + month : month) + "/" + currDate.getFullYear();        
    }
  }

  var listboxFrequencies = function() {
    var list = [];
    for (var key in frequencies) {
      if (frequencies.hasOwnProperty(key)) {
        list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
      }
    }
    return list;
  }

  var  listboxForHowLong = function() {
    var list = []
    for (var key in forHowLong) {
      if (forHowLong.hasOwnProperty(key)) {
        list.push([key, kony.i18n.getLocalizedString(forHowLong[key])]);
      }
    }
    return list;
  }


  var getAccountDisplayName = function (account) {
    return getAccountName(account) + " ...." + getLastFourDigit(account.accountID) + " " +  getDisplayBalance(account);
  }

  var getAccountName = function (account) {
    if (account.nickName && account.nickName.length > 0) {
      return account.nickName;
    } else {
      return account.accountName;
    }
  };


  var getLastFourDigit = function (numberStr) {
    if (numberStr.length < 4) return numberStr;
    return numberStr.slice(-4);
  };

  var getDisplayBalance = function (account) {
    return "(" + kony.i18n.getLocalizedString('i18n.common.available') + ":" +CommonUtilities.formatCurrencyWithCommas(account.availableBalance) + ")";
  }



  var viewModel = {
    selectedTransferToType: null,
    accountsFrom: [],
    transferGatewayConfig: [],
    accountsTo: [],
    frequencies: listboxFrequencies(),
    forHowLong: listboxForHowLong(),
    accountFromKey: 0,
    accountToKey: 0,
    amount: "",
    serverError: null,
    endOnDate: getCurrDateString(),
    toFieldDisabled:  false,
    frequencyKey: "Now",
    noOfRecurrences: "",
    notes: "",
    howLongKey: "DATE_RANGE",
    sendOnDate: getCurrDateString(),
    otherAmount: "",
    paymentAmountkey : "Payment Amount",
    RbOneTimeLoanKey : "OneTime",
    RbNowLoan : "Now",
    transferSubmitButtonListener: submitTransfer,
    transferToTypeListener: onTransferToTypeSelected,
    cancelTransaction: cancelTransaction,
    getAccountName: getAccountName,
    getLastFourDigit: getLastFourDigit,
    error: null,
    showProgressBar: false,
    newTransfer: newTransfer,
    indefinetly: false
  }

  var frequencies = {
    Now: "i18n.Transfers.Now",
    Once: "i18n.transfers.frequency.once",
    // Daily: "i18n.Transfers.Daily",
    Weekly: "i18n.Transfers.Weekly",
    BiWeekly: "i18n.Transfers.EveryTwoWeeks",
    Twice_a_Month:"i18n.Transfers.TwiceaMonth",
    Monthly: "i18n.Transfers.Monthly",    
    QUARTERLY: "i18n.Transfers.Quaterly",
    HALF_YEARLY: "i18n.Transfers.HalfYearly",
    YEARLY: "i18n.Transfers.Yearly"
    
  }

  var forHowLong = {
    //ON_SPECIFIC_DATE: "i18n.transfers.lbxOnSpecificDate",
    DATE_RANGE :"i18n.transfers.lbxOnSpecificDateOCCU",
    NO_OF_RECURRENCES: "i18n.transfers.lblNumberOfRecurrences",
    // Will be added in R2
    //CONTINUE_UNTIL_CANCEL: "i18n.transfers.lblContinueUntilCancel"
    UNTILL_CANCELLED :"i18n.transfers.lblContinueUntilCancelOCCU"
  }

  var transferToTypes =  {
    OWN_INTERNAL_ACCOUNTS: {
      info:["i18n.transfers.MemToMemTrans","i18n.transfers.transferLimit"], 
      title:"i18n.transfers.toMyKonyBankAccounts",
      image: "to_konybank_acc.png",
      external: false,
      filterFunction: function (userAccount) {
        return userAccount.supportTransferTo === "1"
      }
    },
    OTHER_INTERNAL_MEMBER: {
      info:["i18n.transfers.OrangeToOthers","i18n.transfers.transferLimit"], 
      title:"i18n.transfers.toOtherKonyBankAccounts",
      image: "to_otherkonybank_acc.png",
      external: true,
      filterFunction: function (externalAccount) {
        return externalAccount.isSameBankAccount === 'true';
      }
    },
    OTHER_EXTERNAL_ACCOUNT: {
      info:["i18n.transfers.OtherDomesticBank","i18n.transfers.transferLimit"], 
      title:"i18n.transfers.toOtherBankAccounts",
      image: "to_otherbank_acc.png",
      external: true,
      filterFunction: function (externalAccount) {
        return externalAccount.isSameBankAccount === 'false' || externalAccount.isSameBankAccount === null;
      }
    },  
    INTERNATIONAL_ACCOUNT: {
      info:["i18n.transfers.365days","i18n.transfers.transferLimit"], 
      title:"i18n.transfers.toInternationalAccounts",
      image: "to_internationalbank_acc.png",
      external: true,
      filterFunction: function (externalAccount) {
        return externalAccount.isInternationalAccount === 'true';
      }
    },
    WIRE_TRANSFER:  {
      info:["i18n.transfers.365days","i18n.transfers.transferLimitWireTransfer"], 
      title:"i18n.transfers.wireTransfer",
      image: "wire_transfer.png",
      external: true,
      filterFunction: function (externalAccount) {
        return false;
      }
    }
  }

  var modifyTransaction = function (viewModel) {
    presentWith(viewModel);
  }



  var generateFromAccounts = function(fromAccount, index) {
    return [index, getAccountDisplayName(fromAccount)];
  }

  var generateToAccounts = function(toAccount, index) {
    return toAccount.accountID ? [index, toAccount.nickName] : [index, toAccount.nickName]
  }

  var validateTransferData  = function (makeTransferViewModel) {
    var currTime = new Date();
    currTime.setHours(0,0,0,0); // Sets to midnight.
    /*if (accountsFrom[makeTransferViewModel.accountFromKey] === accountsTo[makeTransferViewModel.accountToKey]) {
            makeTransferViewModel.error = "i18n.transfers.error.cannotTransferToSame";
            return false;
        }
        else */
    if (makeTransferViewModel.frequencyKey !== 'Once' && makeTransferViewModel.frequencyKey !== 'Now') {
      makeTransferViewModel.sendOnDate= makeTransferViewModel.sendOnDateone; 
      makeTransferViewModel.sendOnDateComponents = makeTransferViewModel.sendOnDateComponentsone;
    }

    if(makeTransferViewModel.accToKey !== null && makeTransferViewModel.accToKey !== undefined && makeTransferViewModel.accToKey === kony.i18n.getLocalizedString('i18n.transfers.newrecipient')){
      makeTransferViewModel.amount = makeTransferViewModel.recamount;
    } 
    if(makeTransferViewModel.ToProductType !== null && makeTransferViewModel.ToProductType !== undefined && (makeTransferViewModel.ToProductType === "L" || makeTransferViewModel.ToProductType === "M")){
      if(makeTransferViewModel.otherPaymentAmount)
        makeTransferViewModel.amount = makeTransferViewModel.ToPaymentDue;
      else
        makeTransferViewModel.amount = makeTransferViewModel.otherAmount;  
    } 
    if(makeTransferViewModel.ToProductType !== null && makeTransferViewModel.ToProductType !== undefined && makeTransferViewModel.ToProductType === "CC" ){
      if(makeTransferViewModel.enteredccAmount) {
        makeTransferViewModel.amount = makeTransferViewModel.ccOtherAmount;  
      }
    } 

    if(makeTransferViewModel.accFromKey === "Select" || makeTransferViewModel.accFromKey === ""){
      makeTransferViewModel.error = "i18n.transfers.error.selectFrom";
      return false;   
    } else if(makeTransferViewModel.accToKey === "Select" || makeTransferViewModel.accToKey === ""){
      makeTransferViewModel.error = "i18n.transfers.error.selectTo";
      return false;   
    } else if(makeTransferViewModel.accToKey === kony.i18n.getLocalizedString('i18n.transfers.newrecipient') && makeTransferViewModel.recLastName === ""){
      makeTransferViewModel.error = "i18n.transfers.error.lastname";
      return false;   
    } else if(makeTransferViewModel.accToKey === kony.i18n.getLocalizedString('i18n.transfers.newrecipient') && makeTransferViewModel.recMemshpID === ""){
      makeTransferViewModel.error = "i18n.transfers.error.membershipid";
      return false;   
    } else if(makeTransferViewModel.accToKey === kony.i18n.getLocalizedString('i18n.transfers.newrecipient') && makeTransferViewModel.recShareID === ""){
      makeTransferViewModel.error = "i18n.transfers.error.shareid";
      return false;   
    }
    else if (makeTransferViewModel.amount === null || parseFloat(makeTransferViewModel.amount) <= 0 || !(/^\d*(\.\d+)?$/.test(makeTransferViewModel.amount)) || makeTransferViewModel.amount === "" || isNaN(makeTransferViewModel.amount)){
      makeTransferViewModel.error = "i18n.transfers.error.enterAmount";
      return false;            
    }        
    /*if (kony.application.getCurrentForm().transfermain.maketransfer.RbNowLoan.selectedKey == "Payment Date") {
            if (getDateObject(makeTransferViewModel.ToPaymentDate).getTime() < currTime.getTime()){
				makeTransferViewModel.error = "i18n.transfers.error.invalidSendOnDate";
				return false;
			}else{
				return true;
			}
        } else*/
    // if (kony.application.getCurrentForm().transfermain.maketransfer.RbNowLoan.selectedKey == "Future Payment") {
    if (getDateObj(makeTransferViewModel.sendOnDateComponents).getTime() < currTime.getTime()) {
      makeTransferViewModel.error = "i18n.transfers.error.invalidSendOnDate";
      return false;
    } else {
      if (makeTransferViewModel.frequencyKey !== 'Once' && makeTransferViewModel.frequencyKey !== 'Now') {
        if (makeTransferViewModel.indefinetly === false || makeTransferViewModel.indefinetly === "false") {
          if (getDateObj(makeTransferViewModel.endOnDateComponents).getTime() < currTime.getTime()) {
            makeTransferViewModel.error = "i18n.transfers.errors.invalidEndOnDate";
            return false;
          }
          if (getDateObj(makeTransferViewModel.endOnDateComponents).getTime() === getDateObj(makeTransferViewModel.sendOnDateComponents).getTime()) {
            makeTransferViewModel.error = "i18n.transfers.errors.sameEndDate";
            return false;
          }
          if (getDateObj(makeTransferViewModel.endOnDateComponents).getTime() < getDateObj(makeTransferViewModel.sendOnDateComponents).getTime()) {
            makeTransferViewModel.error = "i18n.transfers.errors.beforeEndDate";
            return false;
          }
        }
      } else {
        // Removed the check from 
        if (Number.parseFloat(makeTransferViewModel.amount) > Number.parseFloat(makeTransferViewModel.FromExistingBalance)) {
          makeTransferViewModel.error = "i18n.transfers.error.notEnoughBalance";
          return false;
        }
      }
    }
    // }

    return true;
  }

  var showTransferFormForFixedToAccount = function () {
    function accountsFromFilter (userAccount) {
      return userAccount.supportTransferFrom === "1"
    }
    var toAccountNumber = initialContext.accountTo;
    generateViewModelNewTransaction(userAccounts.filter(accountsFromFilter), 
                                    externalAccounts.filter(function(externalAccount) {
      return externalAccount.accountNumber === toAccountNumber;   
    }));
  }


  var showTransfersFormForExistingTransaction = function () {
    var transaction = initialContext.transactionObject;
    if(initialContext.transactionObject.transactionType === 'InternalTransfer') {
      generateViewModelFromExistingTransaction(userAccounts,userAccounts);
    }
    else {
      generateViewModelFromExistingTransaction(externalAccounts, externalAccounts);
    }
  }

  var showTransfersFormForEditExistingTranaction = function () {
    var transaction = initialContext.editTransactionObject;
    /*if(transaction.transactionType === 'InternalTransfer') {
            generateViewModelForEditTransaction(userAccounts,userAccounts);
        }
        else {
            generateViewModelForEditTransaction(externalAccounts, externalAccounts);
        }*/
    var type = initialContext.editTransactionObject.selectedTransferToType;
    if (type === "OWN_INTERNAL_ACCOUNTS") {
      generateViewModelForEditTransaction(userAccounts, userAccounts);
    } else if (type === "OTHER_INTERNAL_MEMBER") {
      generateViewModelForEditTransaction(externalAccounts, externalAccounts);
    } else {
      generateViewModelForEditTransaction(externalOtherAccounts, externalOtherAccounts);
    }
  }

  //modified OCCU
  var showTransfersForm = function (type) {
    function accountsFromFilter (userAccount) {
      return userAccount; //.supportTransferFrom === "1"
    }
    var typeConfig = transferToTypes[type]
    if(type === "OWN_INTERNAL_ACCOUNTS"){
      generateViewModelNewTransaction(userAccounts,userAccounts);
    } else if(type === "OTHER_INTERNAL_MEMBER"){
      generateViewModelNewTransaction(externalAccounts, externalAccounts);
    }
    else {
      generateViewModelNewTransaction(externalOtherAccounts, externalOtherAccounts);
    }
  }

  var generateViewModelForEditTransaction = function (userAccounts, externalAccounts) {
    viewModel.toFieldDisabled= true;
    var transaction = initialContext.editTransactionObject;
    viewModel.selectedTransferToType = initialContext.editTransactionObject.selectedTransferToType;
    viewModel.accountToKey = 0;
    viewModel.accountFromKey = 0;
    viewModel.accountToKey = initialContext.editTransactionObject.toAccountNumber.split("-")[0];
    viewModel.accountFromKey = initialContext.editTransactionObject.fromAccountNumber;
    viewModel.amount = transaction.amount || "";
    viewModel.endOnDate = getCurrDateString();
    viewModel.error = null;
    viewModel.frequencyKey = transaction.frequencyType || frequencies.Now;
    viewModel.noOfRecurrences = transaction.noOfRecurrences || "";
    viewModel.notes = transaction.transactionsNotes || "";
    viewModel.indefinetly = transaction.indefinetly;
    viewModel.howLongKey = "DATE_RANGE" ; //"DATE_RANGE";transaction.howLongKey
    if (getDateObject(transaction.scheduledDate).getTime() < new Date().getTime()) {
      viewModel.sendOnDate = getCurrDateString();
    }
    else {
      viewModel.sendOnDate = CommonUtilities.getFrontendDateString(transaction.scheduledDate, CommonUtilities.getConfiguration('frontendDateFormat'));
    }
    if (getDateObject(transaction.frequencyEndDate).getTime() < new Date().getTime()) {
      viewModel.endOnDate = getCurrDateString();
    }
    else {
      viewModel.endOnDate =  CommonUtilities.getFrontendDateString(transaction.frequencyEndDate, CommonUtilities.getConfiguration('frontendDateFormat'));
    }
    generateViewModel(userAccounts, externalAccounts, transaction.fromAccountNumber);
  };

  var generateViewModelNewTransaction = function (userAccounts, externalAccounts) {
    resetViewModel();
    generateViewModel(userAccounts, externalAccounts);
  };

  var generateViewModelFromExistingTransaction = function (userAccounts, externalAccounts) {
    var transaction = initialContext.transactionObject;
    viewModel.selectedTransferToType = transaction.selectedTransferToType;
    viewModel.accountToKey = 0;
    viewModel.accountFromKey = 0;
    viewModel.amount = transaction.amount || "";
    viewModel.endOnDate = getCurrDateString();
    viewModel.error = null;
    viewModel.frequencyKey = transaction.frequencyType || frequencies.Now;
    viewModel.noOfRecurrences = transaction.noOfRecurrences || "";
    viewModel.notes = transaction.notes || "";
    viewModel.indefinetly = transaction.indefinetly;
    viewModel.howLongKey = "DATE_RANGE" ; //"DATE_RANGE";transaction.howLongKey
    if (getDateObject(transaction.scheduledDate).getTime() < new Date().getTime()) {
      viewModel.sendOnDate = getCurrDateString();
    }
    else {
      viewModel.sendOnDate = transaction.scheduledDate;
    }
    generateViewModel(userAccounts, externalAccounts);
  }
function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;   //calculates months between two years
    months -= d1.getMonth() + 1; 
    months += d2.getMonth();  //calculates number of complete months between two months
    day1 = 30-d1.getDate();  
    day2 = day1 + d2.getDate();
    months += parseInt(day2/30);  //calculates no of complete months lie between two dates
    return months <= 0 ? 0 : months;
 }

  var callEditTransaction = function (viewModel) {
    var existingTransactionObject = initialContext.editTransactionObject;
     var numberofrec = 1;
    if (viewModel.noOfRecurrences !== null && viewModel.noOfRecurrences !== "" && viewModel.noOfRecurrences !== "null") {
              numberofrec = viewModel.noOfRecurrences;
			  if(viewModel.frequencyKey === "BiWeekly"){
				viewModel.frequencyKey = "Every 2 Weeks";
			  } else if(viewModel.frequencyKey === "QUARTERLY"){   
				viewModel.frequencyKey = "Quarterly";
			  } else if(viewModel.frequencyKey === "HALF_YEARLY"){  
				viewModel.frequencyKey = "Semi Annually";
			  } else if(viewModel.frequencyKey === "YEARLY"){  
				viewModel.frequencyKey = "Annually";
			  }
        } else {
              var startDate =  getDateObj(viewModel.sendOnDateComponents);
			  var endDate = getDateObj(viewModel.endOnDateComponents);
			  if(viewModel.frequencyKey === "Weekly"){        
				numberofrec = Math.round((endDate - startDate) / (7 * 24 * 60 * 60 * 1000));   
			  } else if(viewModel.frequencyKey === "BiWeekly"){
				numberofrec = Math.round(((endDate - startDate) / (7 * 24 * 60 * 60 * 1000))/2);  
				viewModel.frequencyKey = "Every 2 Weeks";
			  } else if(viewModel.frequencyKey === "Monthly"){
				numberofrec = monthDiff(startDate,endDate);   
			  } else if(viewModel.frequencyKey === "QUARTERLY"){
				numberofrec = Math.round(monthDiff(startDate,endDate)/4);   
				viewModel.frequencyKey = "Quarterly";
			  } else if(viewModel.frequencyKey=== "HALF_YEARLY"){
				numberofrec = Math.round(monthDiff(startDate,endDate)/6);   
				viewModel.frequencyKey = "Semi Annually";
			  }  else if(viewModel.frequencyKey === "YEARLY"){
				numberofrec = Math.round(monthDiff(startDate,endDate)/12);   
				viewModel.frequencyKey = "Annually";
			  }
        }
    existingTransactionObject.fromAccountNumber = viewModel.FromaccountID;
    existingTransactionObject.amount = viewModel.amount;
    existingTransactionObject.transactionsNotes = viewModel.notes;
    existingTransactionObject.frequencyType = viewModel.frequencyKey;
    existingTransactionObject.scheduledDate = getDateObj(viewModel.sendOnDateComponents);
    existingTransactionObject.numberOfRecurrences = numberofrec;
    existingTransactionObject.frequencyStartDate =  getDateObj(viewModel.sendOnDateComponents);
    existingTransactionObject.frequencyEndDate = getDateObj(viewModel.endOnDateComponents);
    initialContext.editTransfer(null,existingTransactionObject);
  };



  var submitTransfer = function (makeTransferViewModel) {
    if (validateTransferData(makeTransferViewModel)) {
      makeTransferViewModel.error = null;
      if(initialContext.editTransactionObject) {
        makeTransferViewModel.onTransactionSubmit = callEditTransaction.bind(this); //callEditTransaction;
      }
      else {
        makeTransferViewModel.onTransactionSubmit = saveTransfer.bind(this);            
      }
      makeTransferViewModel.accountFrom = makeTransferViewModel.accFromKey; //accountsFrom[makeTransferViewModel.accountFromKey];
      makeTransferViewModel.accountTo = makeTransferViewModel.accToKey; //accountsTo[makeTransferViewModel.accountToKey];
      makeTransferViewModel.frequency = frequencies[makeTransferViewModel.frequencyKey];
      makeTransferViewModel.modifyTransaction = modifyTransaction;
      makeTransferViewModel.selectedDate = kony.application.getCurrentForm().transfermain.maketransfer.RbNowLoan.selectedKey;
      navigateToConfirmTransfer(makeTransferViewModel);
    }
    else {
      presentWith(makeTransferViewModel);
    }
  }
  // varÂ transactionsModelÂ =Â kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
  // transactionsModel.


  var getDateObj = function (dateComponents) {
    var date =  new Date();
    date.setDate(dateComponents[0]);
    date.setMonth(parseInt(dateComponents[1])-1);
    date.setFullYear(dateComponents[2]);
    date.setHours(0,0,0,0)
    return date;
  }

  var isFutureDate = function (date) {
    var endTimeToday = new Date();
    endTimeToday.setHours(23,59,59,59);
    if(date.getTime() > endTimeToday.getTime()) {
      return true;
    }
    return false;
  }

  var fetchUserAccountAndNavigate = function (acknowledgeViewModel) {
    function getAccountCallback (response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        acknowledgeViewModel.transferData.accountFrom = response.data;
        navigateToTransferAcknowledge(acknowledgeViewModel);            
      }
    }
    // var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    // accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccountById", acknowledgeViewModel.transferData.accountFrom.accountID, getAccountCallback))
    navigateToTransferAcknowledge(acknowledgeViewModel); 
  }

  var saveTransfer = function (transferData) {

    function createTransferCallback (response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        transferData.referenceId = ""; // response.data.referenceId;
        if (transferData.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT"){
          transferData.successFlag = response.data[0].success;
          transferData.errorFlag = response.data[0].error;
          if(response.data[0].availabelbalance !== null && response.data[0].availabelbalance !== undefined && response.data[0].availabelbalance !== 'undefined'){
            transferData.servAvaibal = response.data.availabelbalance;
          } else {
            transferData.servAvaibal = ""; 
          }
        } else {
          transferData.successFlag = response.data.success;
          transferData.errorFlag = response.data.error;
          if(response.data.availabelbalance !== null && response.data.availabelbalance !== undefined && response.data.availabelbalance !== 'undefined'){
            transferData.servAvaibal = response.data.availabelbalance;
          } else {
            transferData.servAvaibal = ""; 
          }
        }                
        var acknowledgeViewModel = {};
        acknowledgeViewModel.transferData = transferData;              
        fetchUserAccountAndNavigate(acknowledgeViewModel);
      } else {
        transferData.serverError = response.data.errmsg;
        presentWith(transferData);
      }
    }
    var accountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
    var transactionType = transferData.accountTo instanceof accountsModel ? 'InternalTransfer' : 'ExternalTransfer';
    var toAccountNumber = transferData.accountTo instanceof accountsModel ? transferData.accountTo.accountID : transferData.accountTo.accountNumber; 
    var frequencyenddateval = (transferData.indefinetly === 'false' || transferData.indefinetly === false) ? getDateObj(transferData.endOnDateComponents) : null;
    if(transferData.frequencyKey === "Once"){
      frequencyenddateval = "";
    } 
    var accountlastname = "";
    if(transferData.selectedTransferToType === "OTHER_INTERNAL_MEMBER")
    {
      accountlastname = transferData.ToAccountName;
    }
    var sendOnDate = getDateObj(transferData.sendOnDateComponents);
    if ((transferData.ToProductType === "L" || transferData.ToProductType === "M") && transferData.selectedDate == "Payment Date"){
      sendOnDate = transferData.ToPaymentDate;
      frequencyenddateval = "";
    }

    if (transferData.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT" && transferData.frequencyKey === "Now") {
      transferData.frequencyKey = "Once";
      transferData.scheduledDate = CommonUtilities.formatDate(new Date().toString(), 'mm/dd/yyyy');
    }

    if (transferData.selectedTransferToType === "OWN_INTERNAL_ACCOUNTS" && transferData.howLongKey === 'NO_OF_RECURRENCES') {

      var frequecny = transferData.frequencyKey;
      var recurrence = transferData.noOfRecurrences;
      frequencyenddateval= onCalculateScheduleDate(frequecny,recurrence,sendOnDate);
    }

    var commandData = {  
      notes: transferData.notes,
      isScheduled: isFutureDate(getDateObj(transferData.sendOnDateComponents)) || (transferData.frequencyKey !== 'Once' && transferData.frequencyKey !== 'Now') ? "1" : "0",
      transactionType: transactionType,
      frequencyType: transferData.frequencyKey,
      numberOfRecurrences: transferData.howLongKey === 'NO_OF_RECURRENCES' ? transferData.noOfRecurrences : null,
      frequencyEndDate: frequencyenddateval,
      scheduledDate: sendOnDate,
      username: kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
      fromAccountNumber: transferData.FromaccountID, //accountFrom.accountID,
      fromProductType: transferData.FromProductType,
      fromProductId: transferData.FromProductID,
      amount: transferData.amount,
      toAccountNumber: transferData.ToaccountID, //toAccountNumber,
      toProductType: transferData.ToProductType,
      toProductId: transferData.ToProductID,
      toAccountLastName: accountlastname,
      fromIsExternal: transferData.FromIsExternal,
      FromExternalAccountId : transferData.FromExternalAccountId,
      ToExternalAccountId : transferData.ToExternalAccountId
    }

    if(transferData.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT"){
      if(transferData.frequencyKey === "Once"){
        transferData.frequencyEndDate = "";
      }
      businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.externalScheduleTransfer", commandData, createTransferCallback));
    } else 
    {
      if(transferData.accountTo !== null && transferData.accountTo !== undefined && transferData.accountTo === kony.i18n.getLocalizedString('i18n.transfers.newrecipient')){
        businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createTransfer", commandData, createTransferCallback));
      } else {          
        if(transferData.frequencyKey === "Now"){
          businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createTransfer", commandData, createTransferCallback));
          if ((transferData.ToProductType === "L"|| transferData.ToProductType === "M") && transferData.selectedDate == "Payment Date"){
            businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.scheduleTransfer", commandData, createTransferCallback)); 
          }
        } else {
          if(transferData.frequencyKey === "Once"){
            transferData.frequencyEndDate = "";
          }
          businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.scheduleTransfer", commandData, createTransferCallback));
        }
      }
    }

  }

  var resetViewModel = function () {
    viewModel.accountToKey = 0;
    viewModel.indefinetly = false;
    viewModel.selectedTransferToType = transferToTypeSelected;         
    viewModel.accountFromKey = 0;
    viewModel.amount = "";
    viewModel.toFieldDisabled = false;
    viewModel.endOnDate = getCurrDateString();
    viewModel.frequencyKey = "Now";
    viewModel.noOfRecurrences = "";
    viewModel.notes = "";
    viewModel.error = null;
    viewModel.serverError = null;
    viewModel.recLastName = "";
    viewModel.recMemshpID = "";
    viewModel.recShareID = "";
    //viewModel.howLongKey = "ON_SPECIFIC_DATE";
    viewModel.howLongKey = "DATE_RANGE";
    viewModel.sendOnDate = getCurrDateString();
    viewModel.otherAmount= ""; 
    viewModel.paymentAmountkey = "Payment Amount";
    viewModel.RbOneTimeLoanKey = "OneTime";
    viewModel.RbNowLoan = "Now";
    viewModel.cancelTransaction = newTransfer;
    viewModel.accFromKey = "";
    viewModel.FromaccountID = "";
    viewModel.FromProductID = "";
    viewModel.FromProductType = "";
    viewModel.FromExistingBalance = "";
    viewModel.accToKey = "";
    viewModel.ToaccountID = "";
    viewModel.ToProductID = "";
    viewModel.ToProductType = "";
    viewModel.secHeader = "";
    viewModel.FromIsExternal= "";
    viewModel.ToIsExternal = "";
    viewModel.processingDate = "";
    viewModel.FromExternalAccountId = "";
    viewModel.ToExternalAccountId = "";
    viewModel.indefinetly = false;
  }



  var  generateViewModel = function(fromAccounts, toAccounts, fromAccountNumber) {
    accountsFrom = fromAccounts;
    accountsTo = toAccounts;
    viewModel.accountsFrom = fromAccounts; //.map(generateFromAccounts);
    viewModel.accountsTo = fromAccounts; //.map(generateToAccounts);
    viewModel.frequencies = listboxFrequencies();
    viewModel.forHowLong = listboxForHowLong();
    /*if (initialContext.accountObject) {
            viewModel.accountFromKey = fromAccounts.findIndex(function (account) {
                return account.accountID === initialContext.accountObject.accountID
            })
        }
        else if (fromAccountNumber) {
            viewModel.accountFromKey = fromAccounts.findIndex(function (account) {
                return account.accountID === fromAccountNumber
            })
        }*/
    if (initialContext.onCancelCreateTransfer && typeof(initialContext.onCancelCreateTransfer) === 'function' ) {
      viewModel.cancelTransaction = initialContext.onCancelCreateTransfer
    }
    viewModel.transferSubmitButtonListener = submitTransfer.bind(this);
    viewModel.showProgressBar = false;
    presentWith(viewModel);
  }

  var loadUserAccounts  = function (onSuccess, onError) {
    function completionCallback(responseList) {
      if (responseList[0].status === kony.mvc.constants.STATUS_SUCCESS && responseList[1].status === kony.mvc.constants.STATUS_SUCCESS) {
        externalAccounts = responseList[0].data;
        userAccounts = responseList[1].data;
        externalOtherAccounts = responseList[2].data;
        onSuccess(responseList[1].data, responseList[0].data, responseList[2].data);
      }
      else{
        onError();
      }
    }
    var commands = [new kony.mvc.Business.Command("com.kony.transfer.getRecipient", {}),new kony.mvc.Business.Command("com.kony.transfer.getAccountsToTransfer", {}),  new kony.mvc.Business.Command("com.kony.transfer.getExternalTrasferAccountList", {})]
    var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
    // accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, completionCallback));
    kony.mvc.util.ParallelCommandExecuter.executeCommands(businessController, commands, completionCallback)
  };

  var generateGatewayConfig = function (userAccounts,externalAccounts) {
    var list = [];        
    function generateConfigForTransferType (key) {
      var config = {
        transferType: key,
        info: transferToTypes[key].info,
        title: transferToTypes[key].title,
        image: transferToTypes[key].image
      };
      if(transferToTypes[key].external) {
        config.visible = externalAccounts; //.filter(transferToTypes[key].filterFunction).length > 0;
      }
      else {
        config.visible = userAccounts; //.filter(transferToTypes[key].filterFunction).length > 0;
      }
      return config;
    }

    for (var key in transferToTypes) {
      if (transferToTypes.hasOwnProperty(key)) {
        list.push(generateConfigForTransferType(key))
      }
    }

    return list;
  }

  var showTransferGateway = function (userAccounts, externalAccounts) {
    viewModel.transferGatewayConfig = generateGatewayConfig(userAccounts, externalAccounts);
    viewModel.selectedTransferToType = null;
    viewModel.showProgressBar = false;
    presentWith(viewModel);
  }

  // Calculating the Enddate for the Schedule Payemnt
  var onCalculateScheduleDate = function(frequencyKey,recurrences,sendOnDate)
  {
    try
    {
      switch(frequencyKey)
      {
        case "Weekly":
          var NoOfDays = 7;
          var NOOFRecrr = recurrences;
          var totalNoOfDays = parseInt(NOOFRecrr) * NoOfDays;         
          var targetDate = new Date(sendOnDate);
          var endDate = targetDate.setDate(targetDate.getDate() + totalNoOfDays);      
          var dateCal = new Date(endDate);
          return dateCal;
        case "Every Two Weeks":
          NoOfDays = 7;
          NOOFRecrr = recurrences*2;
          totalNoOfDays = parseInt(NOOFRecrr) * NoOfDays;         
          targetDate = new Date(sendOnDate);
          endDate = targetDate.setDate(targetDate.getDate() + totalNoOfDays);      
          dateCal = new Date(endDate);
          return dateCal;                   
        case "Monthly":
          NoOfDays =30;
          NOOFRecrr = recurrences;
          totalNoOfDays = parseInt(NOOFRecrr) * NoOfDays;         
          targetDate = new Date(sendOnDate);
          endDate = targetDate.setDate(targetDate.getDate() + totalNoOfDays);      
          dateCal = new Date(endDate);
          return dateCal;   
        case "Twice a Month":
          NoOfDays =30;
          NOOFRecrr = recurrences*2;
          totalNoOfDays = parseInt(NOOFRecrr) * NoOfDays;         
          targetDate = new Date(sendOnDate);
          endDate = targetDate.setDate(targetDate.getDate() + totalNoOfDays);      
          dateCal = new Date(endDate);
          return dateCal;
        case "Quaterly":
          NoOfDays =90;
          NOOFRecrr = recurrences;
          totalNoOfDays = parseInt(NOOFRecrr) * NoOfDays;          
          targetDate = new Date(sendOnDate);
          endDate = targetDate.setDate(targetDate.getDate() + totalNoOfDays);      
          dateCal = new Date(endDate);
          return dateCal;                  
        case "Semi Annually":
          NoOfDays =180;
          NOOFRecrr = recurrences;
          totalNoOfDays = parseInt(NOOFRecrr) * NoOfDays;         
          targetDate = new Date(sendOnDate);
          endDate = targetDate.setDate(targetDate.getDate() + totalNoOfDays);      
          dateCal = new Date(endDate);
          return dateCal;                  
        case "Annually":
          NoOfDays =365;
          NOOFRecrr = recurrences;
          totalNoOfDays = parseInt(NOOFRecrr) * NoOfDays;          
          targetDate = new Date(sendOnDate);
          endDate = targetDate.setDate(targetDate.getDate() + totalNoOfDays);      
          dateCal = new Date(endDate);
          return dateCal;                   
      }
    }
    catch(err)
    {
      kony.print("onCalculateScheduleDate Exception----------> "+err);
    }
  };


  return {
    MakeTransfer : {
      init: function (transferBusinessController, presenter, onNavigateToConfirmTransfer, transferAcknowledgePresenter, context) {
        presentWith = presenter;
        businessController = transferBusinessController;
        navigateToConfirmTransfer = onNavigateToConfirmTransfer;
        navigateToTransferAcknowledge = transferAcknowledgePresenter;
        initialContext = context || {};
        if (initialContext.initialView !== "makeTransfer") {
          return;
        }
        if(initialContext.transactionObject) {
          transferToTypeSelected = "any";                
          resetViewModel();
          viewModel.showProgressBar = true;
          presentWith(viewModel);
          loadUserAccounts(showTransfersFormForExistingTransaction,kony.print);
          return;
        }
        if (initialContext.editTransactionObject) {
          transferToTypeSelected = initialContext.editTransactionObject.category; //"OWN_INTERNAL_ACCOUNTS";                
          resetViewModel();
          viewModel.showProgressBar = true;
          presentWith(viewModel);
          loadUserAccounts(showTransfersFormForEditExistingTranaction,kony.print);
          return;
        }
        if(initialContext.accountTo) {
          transferToTypeSelected = "any";                
          resetViewModel();
          viewModel.showProgressBar = true;
          presentWith(viewModel);
          loadUserAccounts(showTransferFormForFixedToAccount,kony.print);
          return;
        }
        transferToTypeSelected = null;                
        resetViewModel();
        viewModel.showProgressBar = true;
        presentWith(viewModel);
        loadUserAccounts(showTransferGateway,kony.print);                
      },
      isSameAccount: function (indexFrom, indexTo) {
        return  accountsFrom[indexFrom] === accountsTo[indexTo];
      },
      isFutureDate: function (dateComponents) {
        return isFutureDate(getDateObj(dateComponents));
      },
      getTransferType: function (account) {
        var accountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts")
        return account instanceof accountsModel ? "InternalTransfer" : "ExternalTransfer";
      }
    },
    transferToTypeListener: onTransferToTypeSelected,
  };


});