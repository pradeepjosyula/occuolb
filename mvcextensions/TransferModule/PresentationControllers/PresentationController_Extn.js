define(['CommonUtilities','OLBConstants'], function(CommonUtilities, OLBConstants){
  var frequencies = {
    'Once': "i18n.transfers.frequency.once",
    'Daily': "i18n.Transfers.Daily",
    'Weekly': "i18n.Transfers.Weekly",
    'BiWeekly': "i18n.Transfers.EveryTwoWeeks",
    'Monthly': "i18n.Transfers.Monthly",
    'Quarterly': "i18n.Transfers.Quaterly",
    'Half Yearly': "i18n.Transfers.HalfYearly",
    'Yearly': "i18n.Transfers.Yearly"
}
  return {
    deleteExternalAccount : function (accountNumber,lastName,accType,shareId,description) {
      var self=this;
      function completionCallback(response){
        if(response.status==kony.mvc.constants.STATUS_SUCCESS){
          self.getExternalAccounts();
        }else{
          self.getExternalAccounts();
        }
      }
      self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.deleteAccount",{"accountNumber":accountNumber,"lastName":lastName,"accountType":accType,"shareId":shareId,"description":description}, completionCallback));
    },
    getFrequencies: function () {
      var list = [];
      for (var key in frequencies) {
          if (frequencies.hasOwnProperty(key)) {
              list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
          }
      }
      return list;
    },
    showTransferScreen : function (context) {
      //this.loadHamburger("frmTransfers");
      var initialContext  = context || {};
      if (initialContext.initialView === undefined) {
        initialContext.initialView = "makeTransfer";
      }
      // if

      if (initialContext.initialView === "recent") {
        this.presentTransfers({showRecentTransfers: true});
      }
      // this.loadMakeTransferComponents(initialContext);
      //this.getAllAccounts();
      this.presentTransfers({gateway: {}})
    },
    
    //overiding for Transfer scheuled pagination 
    
    
    fetchScheduledUserTransactions:function(fController, recordNum) {
    var self = this;
      var scheduleConfig  = {
    'sortBy' : 'transactionDate',
    'defaultSortBy' :  'transactionDate',
    'order' : OLBConstants.DESCENDING_KEY,            
    'defaultOrder' : OLBConstants.DESCENDING_KEY,
    'offset': OLBConstants.DEFAULT_OFFSET,
    'limit' : OLBConstants.PAGING_ROWS_LIMIT
  };
  

    function userTransCompletionCallback(response) {

        if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
          
          
            var data = {};
          if(response.data.length!==undefined){
            
            var limit = recordNum.limit;
            var offset = recordNum.offset;
            limit = limit + offset;
            response.data = response.data.filter(function (_, count) {
                return (count >= offset && count < limit);
            }); 
            data["scheduledTransfers"] = response.data;
            data.config = tmpInputs;
            self.presentUserInterface("frmTransfers", data);
          }else{
            data["scheduledTransfers"] = response.data;
            self.presentUserInterface("frmTransfers", data);
          }
            
        } else if (response.status == kony.mvc.constants.STATUS_FAILURE) {
          self.presentServerError(response.data);
         // alert("error" + JSON.stringify(recordNum));
        }
    }
    var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(recordNum, scheduleConfig);
    
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getScheduledUserTransactions", tmpInputs, userTransCompletionCallback.bind(this)));
},
    

    //////........> override for confirm verify


    confirmVerifyAndAdd :function(selectedRow){
      kony.print("inside confirmverify and add");
      var self=this;

      function completionCallback(response){

        function editCompletionCallback(response){
          if(response.status==kony.mvc.constants.STATUS_SUCCESS){
            self.getExternalAccounts();    
          }else{
            kony.print("in failure callback of editCompletionCallback inside ");
          }

        }

        

        if(response.status==kony.mvc.constants.STATUS_SUCCESS){
          var editedInfo = {
            "accountNumber": selectedRow.accountNumber,
            "accountType": selectedRow.accountType,
            "nickName": selectedRow.nickName,
            "isVerified": 1
          };

          // self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.editAccountDetails",editedInfo, editCompletionCallback)); 
          self.getExternalAccounts();
          // self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getRecipientList",editedInfo, completionCallback)); 

        }else{
          kony.print("inside error of confirm verify and add");
          self.getExternalAccounts();     
        }

      }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.verifyByTrialDeposit", {"accountNumber":selectedRow.accountNumber,"firstDeposit":selectedRow.firstDeposit,"secondDeposit":selectedRow.secondDeposit}, completionCallback));  
    },

    //// overiding for delete Internel/External scheduleTransfer

    deleteTransfer : function(fController, transactionObject) {
      var self = this;
      if(transactionObject.type=="external"){
        inputParams={
          "username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
          "scheduledTransferId":transactionObject.scheduledTransferId

        }

        this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.deleteTransfer", inputParams, deleteTransCompletionCallback.bind(this)));
      }
      else{

        inputParams = {
          "fromAccountNumber": transactionObject.fromAccountNumber,
          "toAccountNumber": transactionObject.toAccountNumber,
          "frequency": transactionObject.frequency,
          "nextTransfer": transactionObject.nextTransfer,
          "toProductType": transactionObject.toProductType,
          "toProductId": transactionObject.toProductId,
          "amount": transactionObject.amount,
          "fromProductType": transactionObject.fromProductType,
          "fromProductId": transactionObject.fromProductId,
          "username": kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
          "startDate":transactionObject.startDate,
          "endDate":transactionObject.endDate,
          "edit":""
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.deleteTransferInternal", inputParams, deleteTransCompletionCallback.bind(this)));
      }
      function deleteTransCompletionCallback(response) {
        if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
          this.fetchScheduledUserTransactions(fController, {
            offset: 0,
            limit: 10
          });
        } else if (response.status == kony.mvc.constants.STATUS_FAILURE) kony.print("Error in deleting the transaction");
      }
      
    },
    fetchUserAccountAndNavigate : function (acknowledgeViewModel) {
      function getAccountCallback (response) {
          if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
              acknowledgeViewModel.transferData.accountFrom = response.data;
              navigateToTransferAcknowledge(acknowledgeViewModel);            
          }
      }
     // var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
     // accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccountById", acknowledgeViewModel.transferData.accountFrom.accountID, getAccountCallback))
     navigateToTransferAcknowledge(acknowledgeViewModel); 
  },

    createTransfer: function (transferData) {

      function createTransferCallback (response) {
          if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
              transferData.referenceId = ""; // response.data.referenceId;
              if (transferData.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT"){
                transferData.successFlag = response.data[0].success;
                transferData.errorFlag = response.data[0].error;
                if(response.data[0].availabelbalance !== null && response.data[0].availabelbalance !== undefined && response.data[0].availabelbalance !== 'undefined'){
                 transferData.servAvaibal = response.data.availabelbalance;
                } else {
                 transferData.servAvaibal = ""; 
                }
              } else {
                transferData.successFlag = response.data.success;
                transferData.errorFlag = response.data.error;
                if(response.data.availabelbalance !== null && response.data.availabelbalance !== undefined && response.data.availabelbalance !== 'undefined'){
                 transferData.servAvaibal = response.data.availabelbalance;
                } else {
                 transferData.servAvaibal = ""; 
                }
              }                
              var acknowledgeViewModel = {};
              acknowledgeViewModel.transferData = transferData;              
              this.fetchUserAccountAndNavigate(acknowledgeViewModel);
          } else {
              transferData.serverError = response.data.errmsg;
              presentWith(transferData);
          }
      }
      var accountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
      var transactionType = transferData.accountTo instanceof accountsModel ? 'InternalTransfer' : 'ExternalTransfer';
      var toAccountNumber = transferData.accountTo instanceof accountsModel ? transferData.accountTo.accountID : transferData.accountTo.accountNumber; 
      var frequencyenddateval = transferData.howLongKey === 'DATE_RANGE' ? getDateObj(transferData.endOnDateComponents) : null;
  if(transferData.frequencyKey === "Once"){
    frequencyenddateval = "";
  } 
      var accountlastname = "";
      if(transferData.selectedTransferToType === "OTHER_INTERNAL_MEMBER")
      {
        accountlastname = transferData.ToAccountName;
      }
      var sendOnDate = getDateObj(transferData.sendOnDateComponents);
      if ((transferData.ToProductType === "L" || transferData.ToProductType === "M") && transferData.selectedDate == "Payment Date"){
    sendOnDate = transferData.ToPaymentDate;
          frequencyenddateval = "";
      }
    
    if (transferData.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT" && transferData.frequencyKey === "Now") {
              transferData.frequencyKey = "Once";
              transferData.scheduledDate = CommonUtilities.formatDate(new Date().toString(), 'mm/dd/yyyy');
    }
    var commandData = {  
          notes: transferData.notes,
          isScheduled: isFutureDate(getDateObj(transferData.sendOnDateComponents)) || (transferData.frequencyKey !== 'Once' && transferData.frequencyKey !== 'Now') ? "1" : "0",
          transactionType: transactionType,
          frequencyType: transferData.frequencyKey,
          numberOfRecurrences: transferData.howLongKey === 'NO_OF_RECURRENCES' ? transferData.noOfRecurrences : null,
          frequencyEndDate: frequencyenddateval,
          scheduledDate: sendOnDate,
          username: kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
          fromAccountNumber: transferData.FromaccountID, //accountFrom.accountID,
          fromProductType: transferData.FromProductType,
          fromProductId: transferData.FromProductID,
          amount: transferData.amount,
          toAccountNumber: transferData.ToaccountID, //toAccountNumber,
          toProductType: transferData.ToProductType,
          toProductId: transferData.ToProductID,
    toAccountLastName: accountlastname,
          fromIsExternal: transferData.FromIsExternal,
          FromExternalAccountId : transferData.FromExternalAccountId,
          ToExternalAccountId : transferData.ToExternalAccountId
      }
    
      if(transferData.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT"){
         if(transferData.frequencyKey === "Once"){
            transferData.frequencyEndDate = "";
          }
         businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.externalScheduleTransfer", commandData, createTransferCallback));
      } else 
      {
          if(transferData.accountTo !== null && transferData.accountTo !== undefined && transferData.accountTo === kony.i18n.getLocalizedString('i18n.transfers.newrecipient')){
            businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createTransfer", commandData, createTransferCallback));
          } else {          
              if(transferData.frequencyKey === "Now"){
                businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createTransfer", commandData, createTransferCallback));
                if ((transferData.ToProductType === "L"|| transferData.ToProductType === "M") && transferData.selectedDate == "Payment Date"){
                 businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.scheduleTransfer", commandData, createTransferCallback)); 
                }
              } else {
                if(transferData.frequencyKey === "Once"){
                  transferData.frequencyEndDate = "";
                }
                businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.scheduleTransfer", commandData, createTransferCallback));
              }
           }
      }
    
  },


    getExternalAccounts: function(value){
      var data=[];
      var self=this;
      var tmpInputs=[];
      var externalAccountsConfig  = {
        'sortBy' : 'nickName',
        'defaultSortBy' :  'nickName',
        'order' : OLBConstants.ASCENDING_KEY,            
        'defaultOrder' : OLBConstants.ASCENDING_KEY,
        'offset': OLBConstants.DEFAULT_OFFSET,
        'limit' : OLBConstants.PAGING_ROWS_LIMIT
      };

      //get the data frombackend for viewModel
      function completionCallback(response){
        if(response.status==kony.mvc.constants.STATUS_SUCCESS){

          if(response.data.length!==undefined){
            var limit = value.limit;
            var offset = value.offset;
            limit = limit + offset;
            response.data = response.data.filter(function (_, count) {
              return (count >= offset && count < limit);
            }); 
            data=response.data;
            data.push(tmpInputs);
          } 
          else{
            data=response.data;
          }


          self.presentUserInterface("frmTransfers",{"externalAccounts":data});
        }else{
          kony.print("Failed to fetch Accounts list");
          self.presentUserInterface("frmTransfers",{"externalAccounts":"errorExternalAccounts"});
        }
      }


      tmpInputs = CommonUtilities.Sorting.getSortConfigObject(value || {}, externalAccountsConfig);

      self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getRecipientList",tmpInputs, completionCallback)); 
    },
    fetchAccounts: function (businessController,success, error) {
      function completionCallback(responseList) {
        if (responseList[0].status === kony.mvc.constants.STATUS_SUCCESS && responseList[1].status === kony.mvc.constants.STATUS_SUCCESS) {
            // userAccounts = responseList[0].data;
            // externalAccounts = responseList[1].data;
            // userObject = responseList[2].data;
            success(responseList[0].data, responseList[1].data, responseList[2].data);
        }
        else{
            error();
        }
    }
    var commands = [new kony.mvc.Business.Command("com.kony.transfer.getRecipient", {}),new kony.mvc.Business.Command("com.kony.transfer.getAccountsToTransfer", {}),  new kony.mvc.Business.Command("com.kony.transfer.getExternalTrasferAccountList", {})]
    // accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, completionCallback));
    kony.mvc.util.ParallelCommandExecuter.executeCommands(businessController, commands, completionCallback)

    },
    loadAccountsByTransferType:function (type) {
      var self = this;
       var minTransferLimit="";
      var maxTransfersLimit="";
      function onDataFetchComplete(recipients, accountsFrom, externalTransferAccountList) {
        self.presentTransfers({
          makeTransfer: {
            recipients: recipients,
            accountsFrom: accountsFrom,
            externalTransferAccountList: externalTransferAccountList,
            selectedTransferToType: type
          },
          isLoading: false
        })
      }
  
      self.presentTransfers({
        isLoading: true
      });
  
      this.fetchAccounts(this.businessController, onDataFetchComplete);
    
    }

  }
});