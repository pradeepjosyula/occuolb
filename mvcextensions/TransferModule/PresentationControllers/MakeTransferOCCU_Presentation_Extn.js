define(['CommonUtilities'],function (CommonUtilities) {
    var presentWith = null;
    var navigateToConfirmTransfer = null;
    var navigateToTransferAcknowledge = null;
    var accountsFrom = [];
    var accountsTo = [];
    var userAccounts = [];
    var externalAccounts = [];
    var transferToTypeSelected = null;
    var initialContext = {};
    var userObject = null;
	loadUserAccounts  = function (onSuccess, onError) {
        function completionCallback(responseList) {
            if (responseList[0].status === kony.mvc.constants.STATUS_SUCCESS && responseList[1].status === kony.mvc.constants.STATUS_SUCCESS) {
                userAccounts = responseList[0].data;
                externalAccounts = responseList[1].data;
                userObject = responseList[2].data;
                onSuccess(responseList[0].data, responseList[1].data, responseList[2].data);
            }
            else{
                onError();
            }
        }
        var commands = [new kony.mvc.Business.Command("com.kony.transfer.getRecipient", {}),new kony.mvc.Business.Command("com.kony.transfer.getExternalAccounts", {}),  new kony.mvc.Business.Command("com.kony.transfer.getUserProfile", {})]
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        // accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, completionCallback));
        kony.mvc.util.ParallelCommandExecuter.executeCommands(businessController, commands, completionCallback)
    }
	generateGatewayConfig = function (userAccounts,externalAccounts) {
        var list = [];        
        function generateConfigForTransferType (key) {
            var config = {
                transferType: key,
                info: transferToTypes[key].info,
                title: transferToTypes[key].title,
                image: transferToTypes[key].image
            };
            if(transferToTypes[key].external) {
                config.visible = externalAccounts.filter(transferToTypes[key].filterFunction).length > 0;
            }
            else {
                config.visible = userAccounts; //.filter(transferToTypes[key].filterFunction).length > 0;
            }
            return config;
        }

        for (var key in transferToTypes) {
            if (transferToTypes.hasOwnProperty(key)) {
                list.push(generateConfigForTransferType(key))
            }
        }

        return list;
    }
	var  generateViewModel = function(fromAccounts, toAccounts, fromAccountNumber) {
        accountsFrom = fromAccounts;
        accountsTo = toAccounts;
        viewModel.accountsFrom = fromAccounts; //.map(generateFromAccounts);
        viewModel.accountsTo = fromAccounts; //.map(generateToAccounts);
        viewModel.frequencies = listboxFrequencies();
        viewModel.forHowLong = listboxForHowLong();
        if (initialContext.onCancelCreateTransfer && typeof(initialContext.onCancelCreateTransfer) === 'function' ) {
            viewModel.cancelTransaction = initialContext.onCancelCreateTransfer
        }
        viewModel.transferSubmitButtonListener = submitTransfer.bind(this);
        viewModel.showProgressBar = false;
        presentWith(viewModel);
    }
});