define(['CommonUtilities','OLBConstants'], function(CommonUtilities, OLBConstants) {

  function Transfer_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Transfer_PresentationController, kony.mvc.Presentation.BasePresenter);

  function getDateObject (dateString) {
    if (CommonUtilities.getConfiguration('frontendDateFormat') === "mm/dd/yyyy") {
        return new Date(dateString);
    }
    else if (CommonUtilities.getConfiguration('frontendDateFormat') === "dd/mm/yyyy")
    {
        var dateParts = dateString.split("/");
        var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        return dateObject;
    }
    else {
        return new Date();
    }
}

  Transfer_PresentationController.prototype.initializePresentationController = function() {

  };

  var getTypeByTransactionObject = function (transactionObject, externalAccounts) {
    if (transactionObject.transactionType === "InternalTransfer") {
      return OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS;
    }
    else {
      var externalAccount = externalAccounts.filter(function(account) {
        return account.accountNumber === transactionObject.ExternalAccountNumber;
      })[0]

      if (externalAccount.isSameBankAccount === "true") {
        return OLBConstants.TRANSFER_TYPES.OTHER_INTERNAL_MEMBER;
      }
      if (externalAccount.isSameBankAccount === "false" || externalAccount.isSameBankAccount === null) {
        return OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT;
      }
      if (externalAccount.isInternationalAccount === "true" ) {
        return OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT;
      }
    }

  }

   /** Transfers Form Show - Calls Present User Interface 
     * @member  Transfer_PresentationController
     * @param  {object} context object
     * @returns {void} None
     * @throws {void} None
     */
  
  Transfer_PresentationController.prototype.showFrmTransfers = function(context) {
  this.presentUserInterface('frmTransfers',context);
  };

   /** Compares Date with todays and tell is its future or not
     * @member  Transfer_PresentationController
     * @param  {object} date object
     * @returns {boolean} True for future else false
     * @throws {void} None
     */

  Transfer_PresentationController.prototype.isFutureDate = function (dateComponents) {
    var dateObj = getDateObj(dateComponents)
    var endTimeToday = new Date();
    endTimeToday.setHours(23,59,59,59);
    if(dateObj.getTime() > endTimeToday.getTime()) {
        return true;
    }
    return false;
}

 /** Refresh user account from which transfer is made and shows Acknowledge
     * @member  Transfer_PresentationController
     * @param  {object} acknowledgeViewModel JSON containing transfer data and reference number
     * @returns {void} None
     * @throws {void} None
     */

Transfer_PresentationController.prototype.fetchUserAccountAndNavigate = function (acknowledgeViewModel) {
  var self = this;
  function getAccountCallback (response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          acknowledgeViewModel.transferData.accountFrom = response.data;
          self.presentTransferAcknowledge({
            transferAcknowledge: acknowledgeViewModel
          });            
      }
  }
  var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
  accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccountById", acknowledgeViewModel.transferData.accountFrom.accountID, getAccountCallback))
}

/** Converts Date Components to Date Object
     * @member  Transfer_PresentationController
     * @returns {void} None
     * @throws {void} None
     */

var getDateObj = function (dateComponents) {
  var date =  new Date();
  date.setDate(dateComponents[0]);
  date.setMonth(parseInt(dateComponents[1])-1);
  date.setFullYear(dateComponents[2]);
  date.setHours(0,0,0,0)
  return date;
}

  var forHowLong = {
    ON_SPECIFIC_DATE: "i18n.transfers.lbxOnSpecificDate",
    NO_OF_RECURRENCES: "i18n.transfers.lblNumberOfRecurrences"
    // Will be added in R2
    // CONTINUE_UNTIL_CANCEL: "i18n.transfers.lblContinueUntilCancel"
}


  var frequencies = {
    'Once': "i18n.transfers.frequency.once",
    'Daily': "i18n.Transfers.Daily",
    'Weekly': "i18n.Transfers.Weekly",
    'BiWeekly': "i18n.Transfers.EveryTwoWeeks",
    'Monthly': "i18n.Transfers.Monthly",
    'Quarterly': "i18n.Transfers.Quaterly",
    'Half Yearly': "i18n.Transfers.HalfYearly",
    'Yearly': "i18n.Transfers.Yearly"
}

/** Tell if transfer is allowed from account
     * @member  Transfer_PresentationController
     * @param  {object} userAccount 
     * @returns {boolean} if allowed the true is returned
     * @throws {void} None
     */

var userAccountsFilter = function (userAccount) {
  return userAccount.supportTransferFrom === "1";
}

/** Filter To Accounts based on type string
     * @member  Transfer_PresentationController
     * @param  {object} type Type constant  
     * @param  {array} userAccounts User Accounts
     * @param  {array} externalAccounts External Accounts
     * @returns {boolean} Array of Accounts or External Accounts
     * @throws {void} None
     */

var getToAccountsByType = function (type, userAccounts, externalAccounts) {
  var filterConfig = transferToTypes[type];
  return filterConfig.external ? externalAccounts.filter(filterConfig.filterFunction) : userAccounts.filter(filterConfig.filterFunction);
}

/** Convert Frequency Constant to List Box Values
     * @member  Transfer_PresentationController
     * @returns {array} Array of Listbox Values
     * @throws {void} None
     */

Transfer_PresentationController.prototype.getFrequencies = function () {
  var list = [];
  for (var key in frequencies) {
      if (frequencies.hasOwnProperty(key)) {
          list.push([key, kony.i18n.getLocalizedString(frequencies[key])]);
      }
  }
  return list;
}

/** For How Long Values for listbox
     * @member  Transfer_PresentationController
     * @returns {array} Array of ListBox Values
     * @throws {void} None
     */

Transfer_PresentationController.prototype.getForHowLong = function () {
    var list = []
    for (var key in forHowLong) {
        if (forHowLong.hasOwnProperty(key)) {
            list.push([key, kony.i18n.getLocalizedString(forHowLong[key])]);
        }
    }
    return list;
}

  

  var transferToTypes =  {
    OWN_INTERNAL_ACCOUNTS: {
        external: false,
        filterFunction: function (userAccount) {
            return userAccount.supportTransferTo === "1"
        }
    },
    OTHER_INTERNAL_MEMBER: {
        external: true,
        filterFunction: function (externalAccount) {
            return externalAccount.isSameBankAccount === 'true';
        }
    },
    OTHER_EXTERNAL_ACCOUNT: {
        external: true,
        filterFunction: function (externalAccount) {
            return externalAccount.isSameBankAccount === 'false' || externalAccount.isSameBankAccount === null;
        }
    },  
    INTERNATIONAL_ACCOUNT: {
        external: true,
        filterFunction: function (externalAccount) {
            return externalAccount.isInternationalAccount === 'true';
        }
    },
    WIRE_TRANSFER:  {
        external: true,
        filterFunction: function (externalAccount) {
            return false;
        }
      }
    }

    /** parallely fetches the initial data - User Accounts and User Profile
     * @member  Transfer_PresentationController
     * @param  {object} businessController 
     * @param  {function} success callback when successfull 
     * @param  {function} error callback when unsuccessfull 
     * @throws {void} None
     */

  var fetchUserAccountsAndProfile = function (businessController,success, error) {
    function completionCallback(responseList) {
      if (responseList[0].status === kony.mvc.constants.STATUS_SUCCESS && responseList[1].status === kony.mvc.constants.STATUS_SUCCESS) {
          // userAccounts = responseList[0].data;
          // externalAccounts = responseList[1].data;
          // userObject = responseList[2].data;
          success(responseList[0].data, responseList[1].data, responseList[2].data);
      }
      else{
          error();
      }
  }
  var commands = [new kony.mvc.Business.Command("com.kony.transfer.getAccounts", {}), new kony.mvc.Business.Command("com.kony.transfer.getExternalAccounts", {}),  new kony.mvc.Business.Command("com.kony.transfer.getUserProfile", {})]
  // accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getAccounts", {}, completionCallback));
  kony.mvc.util.ParallelCommandExecuter.executeCommands(businessController, commands, completionCallback)

  }

  var recentConfig  = {
    'sortBy' : 'transactionDate',
    'defaultSortBy' :  'transactionDate',
    'order' : OLBConstants.DESCENDING_KEY,            
    'defaultOrder' : OLBConstants.DESCENDING_KEY,
    'offset': OLBConstants.DEFAULT_OFFSET,
    'limit' : OLBConstants.PAGING_ROWS_LIMIT
  };

  /** Shows Server error
     * @member  Transfer_PresentationController
     * @param  {object} data Data from server
     * @throws {void} None
     */

  Transfer_PresentationController.prototype.presentServerError = function (data) {
    this.presentUserInterface("frmTransfers", {
      'serverError' : data
    });
  }

  /** Fetch recent User Transactions
     * @member  Transfer_PresentationController
     * @param  {object} FormController 
     * @param  {number} recordNumber - For pagination
     * @throws {void} None
     */

  Transfer_PresentationController.prototype.fetchRecentUserTransactions = function(fController, recordNum) {
    var self = this;

    function userTransCompletionCallback(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
            var data = {};
            data["recentTransfers"] = response.data;
            data.config = tmpInputs;
            self.presentUserInterface("frmTransfers", data);
        } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
          self.presentServerError(response.data);
          //alert("error" + JSON.stringify(recordNum));
        }
            
    }
    var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(recordNum, recentConfig);
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getRecentUserTransactions", tmpInputs, userTransCompletionCallback.bind(this)));
};

/** Formats Currency
     * @member  Transfer_PresentationController
     * @param  {number} amount Amount  
     * @param  {boolean} currencySymbolNotRequired If currency symbol needs to appended  
     * @returns {string} Formatted Amount
     * @throws {void} None
     */

Transfer_PresentationController.prototype.formatCurrency = function (amount, currencySymbolNotRequired) {
  return CommonUtilities.formatCurrencyWithCommas(amount, currencySymbolNotRequired);   
};

/** Delete Transfer - 
     * @member  Transfer_PresentationController
     * @param  {object} fController  
     * @param  {object} transaction Model  Object
     * @throws {void} None
     */

Transfer_PresentationController.prototype.deleteTransfer = function(fController, transactionObject) {
        var self = this;

        function deleteTransCompletionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                this.fetchScheduledUserTransactions(fController, {
                    offset: 0,
                    limit: 10
                });
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {};
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.deleteTransfer", transactionObject, deleteTransCompletionCallback.bind(this)));
    };
/** Edit Transfer - 
     * @member  Transfer_PresentationController
     * @param  {object} fController  
     * @param  {object} transaction Model  Object
     * @throws {void} None
     * @returns {void} None
     */

Transfer_PresentationController.prototype.editTransfer = function(fController, transactionObject) {
        var self = this;

        function editTransCompletionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                this.fetchScheduledUserTransactions(fController, {
                    offset: 0,
                    limit: 10
                });
            } else {
                var transferData = {};
                transferData.transferError = response.data.errmsg; 
                self.presentTransfers(transferData);
              }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.editTransfer", transactionObject, editTransCompletionCallback.bind(this)));
    };

  var scheduleConfig  = {
    'sortBy' : 'transactionDate',
    'defaultSortBy' :  'transactionDate',
    'order' : OLBConstants.DESCENDING_KEY,            
    'defaultOrder' : OLBConstants.DESCENDING_KEY,
    'offset': OLBConstants.DEFAULT_OFFSET,
    'limit' : OLBConstants.PAGING_ROWS_LIMIT
  };


  /** fetch Scheduled user Transactions 
     * @member  Transfer_PresentationController
     * @param  {object} fController  
     * @param  {object} recordNum Pagination Number
     * @throws {void} None
     * @returns {void} None
     */

Transfer_PresentationController.prototype.fetchScheduledUserTransactions = function(fController, recordNum) {
    var self = this;

    function userTransCompletionCallback(response) {

        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
            var data = {};
            data["scheduledTransfers"] = response.data;
            data.config = tmpInputs;
            self.presentUserInterface("frmTransfers", data);
        } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
          self.presentServerError(response.data);
         // alert("error" + JSON.stringify(recordNum));
        }
    }
    var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(recordNum, scheduleConfig);
    
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getScheduledUserTransactions", tmpInputs, userTransCompletionCallback.bind(this)));
};

/** Show Domestic Accounts 
     * @member  Transfer_PresentationController
     * @param  {object} fController  
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.showDomesticAccounts = function(fController) {
    var self=this;
    function completionCallback(response){
      var data={};
      data["domesticAccounts"]=response.data;
      if(kony.application.getCurrentForm().id!=="frmAddExternalAccount"){ 
        //self.presentUserInterface("frmAddExternalAccount",data);
        self.loadHamburger("frmAddExternalAccount",data);
      }else{
        fController.setDomesticAccounts(data.domesticAccounts);
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getOtherBankAccounts", {}, completionCallback));
  };

   /** Create Bank Payee
     * @member  Transfer_PresentationController
     * @param {JSON} newPayeeJSON New Payee Data
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.createBankPayee = function (fController,newPayeeJSON) {
    var self=this;
    function completionCallback(response){

      alert(JSON.stringify(response));
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
        newPayeeJSON["referenceNo"]=response.data.Id; //Fetching the reference Id form the service 
        self.presentfrmVerifyAccount(fController,newPayeeJSON); //Calling the verify Account form 
      }

    }
    newPayeeJSON["isSameBankAccount"]="1"; //Adding an additional field to identify an external account within same bank
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", newPayeeJSON, completionCallback)); //call to the commandHandler
  };

   /** Presents Add Internal Account Form
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */
  Transfer_PresentationController.prototype.navigateToTransfer = function (fController,newPayeeJSON) {
    this.presentUserInterface("frmAddInternalAccount",{}); //To load the frmAddInternalAccount
  };

   /** Presents Add Confirm Account Form
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.presentfrmConfirmAccount = function (fController,data) {
    this.presentUserInterface("frmConfirmAccount",{'InternalAccount':data}); //Loading the ConfirmAccount form
  };

  /** Presents Form Verify Account
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.presentfrmVerifyAccount = function(fController,Verifydata) {
    this.presentUserInterface("frmVerifyAccount",{'InternalAccount':Verifydata}); //Loading the verifyAccount form
  };

   /** Shows International Account
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.showInternationalAccounts = function(fController) {
    var self=this;
    function completionCallback(response){
      var data={};
      data["internationalAccounts"]=response.data;
      if(kony.application.getCurrentForm().id!=="frmAddExternalAccount"){ 
        //self.presentUserInterface("frmAddExternalAccount",data);
        self.loadHamburger("frmAddExternalAccount",data);
      }else{
        fController.setInternationalAccounts(data.internationalAccounts);
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getOtherBankAccounts", {}, completionCallback));
  };

     /** Shows Same Bank Accounts
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.showSameBankAccounts = function(fController) {
    var self=this;
    function completionCallback(response){
      var data={};
      data["sameAccounts"]=response.data;
      //self.presentUserInterface("frmAddInternalAccount",data);
      self.loadHamburger("frmAddInternalAccount",data);
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getSameBankAccounts", {}, completionCallback));
  };

  /**
     * converts domain object to viewModel
     * @param {RBObjects.Account} account 
     * @returns {JSON} viewModel for Account
     */
  var createAccountViewModal = function (account) {
    return {
      accountName: account.accountName,
      nickName: account.nickName,
      accountNumber: Number(account.accountID),
      type: account.accountType,
      availableBalance: CommonUtilities.formatCurrencyWithCommas(account.availableBalance),
      currentBalance: CommonUtilities.formatCurrencyWithCommas(account.currentBalance)

    };
  };

     /** Entry Point Method of Transfer Module
     * @member  Transfer_PresentationController
     * @param {object} context
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.showTransferScreen = function (context) {
    //this.loadHamburger("frmTransfers");
    var initialContext  = context || {};
    if (initialContext.initialView === undefined) {
      initialContext.initialView = "makeTransfer";
    }
    if (initialContext.initialView === "recent") {
      this.presentTransfers({showRecentTransfers: true});
      return;
    }
    // this.loadMakeTransferComponents(initialContext);
    // this.resetViewAndShowLoader();
    if (initialContext.transactionObject) {
      this.repeatTransfer(initialContext.transactionObject, initialContext.onCancelCreateTransfer);
      return;
    }
    if (initialContext.editTransactionObject) {
      this.showMakeTransferForEditTransaction(context.editTransactionObject, context.onCancelCreateTransfer);
      return;
    }
    if (initialContext.accountTo) {
      this.loadAccountsByTransferType(null, initialContext.accountTo);
      return;
    }
    this.presentTransfers({
      gateway: {overrideFromAccount: initialContext.accountObject ? initialContext.accountObject.accountID: null}
    })

  }

  /**Fetches the list of pending accounts
   * @member  Transfer_PresentationController
   * @param {void} None
   * @throws {void} None
   * @returns {void} None
   */
  Transfer_PresentationController.prototype.showPendingAccountsCount=function(){
    var self=this;
    function completionCallback(response){
      var data={};
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
        self.presentUserInterface('frmTransfers',{'pendingAccounts':response.data});
      }else{
        self.presentUserInterface('frmTransfers',{'pendingAccounts':{error:true}});
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getOtherBankAccounts", {}, completionCallback));
  };

       /**Load Accounts bt Transfer Tyoe
     * @member  Transfer_PresentationController
     * @param {string} type Type of Transfer
     * @param {object} accountTo Account to object
     * @param {object} accountFrom Account From Object
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.loadAccountsByTransferType = function (type, accountTo, accountFrom) {
    var self = this;
     var minTransferLimit="";
    var maxTransfersLimit="";
    function onDataFetchComplete(userAccounts, externalAccounts, userProfile) {
      self.presentTransfers({
        makeTransfer: {
          fromAccounts: userAccounts.filter(userAccountsFilter),
          toAccounts: type ? getToAccountsByType(type, userAccounts, externalAccounts) : externalAccounts.filter(function (externalAccount) { return externalAccount.accountNumber === accountTo}),
          defaultFromAccountNumber: accountFrom? accountFrom: userProfile.default_account_transfers,
          type: type,
          limit:self.getMinMaxTransfersLimit(type)
        },
        isLoading: false
      })
    }

    self.presentTransfers({
      isLoading: true
    });

    fetchUserAccountsAndProfile(this.businessController, onDataFetchComplete);
  
  }

   /** getMinMaxTransfersLimit  - Shows Confirmation for transfer
     * @member  Transfer_PresentationController
     * @param {object} makeTransferViewModel data of transfer
     * @param {object} formData new data
     * @throws {void} None
     * @returns {void} None
     */
  Transfer_PresentationController.prototype.getMinMaxTransfersLimit = function(type){
        var limit = {
            minimum : "",
            maximum : ""
        };
        switch(type){
           case  "OWN_INTERNAL_ACCOUNTS"  : 
                limit.minimum = CommonUtilities.getConfiguration("minKonyBankAccountsTransferLimit");
                limit.maximum = CommonUtilities.getConfiguration("maxKonyBankAccountsTransferLimit");
                break;
           case  "OTHER_INTERNAL_MEMBER" :
                 limit.minimum = CommonUtilities.getConfiguration("minOtherKonyAccountsTransferLimit");
                 limit.maximum = CommonUtilities.getConfiguration("maxOtherKonyAccountsTransferLimit");
                 break;
           case  "OTHER_EXTERNAL_ACCOUNT" :
                 limit.minimum = CommonUtilities.getConfiguration("minOtherBankAccountsTransferLimit");
                 limit.maximum = CommonUtilities.getConfiguration("maxOtherBankAccountsTransferLimit");
                 break;
           case  "INTERNATIONAL_ACCOUNT" :
                 limit.minimum = CommonUtilities.getConfiguration("minInternationalAccountsTransferLimit");
                 limit.maximum = CommonUtilities.getConfiguration("maxInternationalAccountsTransferLimit");
                 break;
           default:
                limit.minimum = CommonUtilities.getConfiguration("minInternationalAccountsTransferLimit");
                limit.maximum = CommonUtilities.getConfiguration("maxInternationalAccountsTransferLimit");
       }
        return limit;
      };

       /** Confirm Transfer  - Shows Confirmation for transfer
     * @member  Transfer_PresentationController
     * @param {object} makeTransferViewModel data of transfer
     * @param {object} formData new data
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.confirmTransfer = function (makeTransferViewModel, formData) {
    formData.frequency = frequencies[formData.frequencyKey];
    this.presentTransferConfirm({
      transferConfirm: {
        makeTransferViewModel: makeTransferViewModel,
        transferData: formData
      }
    })
  }

       /** Shows Transfer form with existing transferData
     * @member  Transfer_PresentationController
     * @param {object} transferConfirmViewModel
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.modifyTransaction = function (transferConfirmViewModel) {
    var makeTransferViewModel = transferConfirmViewModel.makeTransferViewModel;
    makeTransferViewModel.transferData = transferConfirmViewModel.transferData;
    this.presentTransfers({})
  }


  Transfer_PresentationController.prototype.loadInitialData = function () {

  }

  
       /** Shows Transfer for Editing a Transaction
     * @member  Transfer_PresentationController
     * @param {object} editTransactionObject Objct of Transaction Model
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.showMakeTransferForEditTransaction = function (editTransactionObject, onCancelCreateTransfer) {
    var self = this;

    function filterToAccounts (userAccounts, externalAccounts) {
      if(editTransactionObject.transactionType === 'InternalTransfer') {
        return userAccounts.filter(function (userAccount) {
            return userAccount.accountID === editTransactionObject.toAccountNumber;
        })
    }
    else {
        return externalAccounts.filter(function (externalAccount) {
            return externalAccount.accountNumber === editTransactionObject.ExternalAccountNumber;
        })
    }
    }

    function onDataFetchComplete(userAccounts, externalAccounts) {
      self.presentTransfers({
        makeTransfer: {
          fromAccounts: userAccounts.filter(userAccountsFilter),
          toAccounts: filterToAccounts(userAccounts, externalAccounts),
          defaultFromAccountNumber: editTransactionObject.fromAccountNumber,
          editTransactionObject: editTransactionObject,
          onCancelCreateTransfer: onCancelCreateTransfer,
          isEdit:true,
          limit:self.getMinMaxTransfersLimit(getTypeByTransactionObject(editTransactionObject, externalAccounts))
        },
        isLoading: false
      })
    }

    self.presentTransfers({
      isLoading: true
    });

    fetchUserAccountsAndProfile(this.businessController, onDataFetchComplete);
  }

  
       /** Shows Transfer form with existing transaction
     * @member  Transfer_PresentationController
     * @param {object} transactionObject Transaction Object
     * @param {function} onBackPressed when cancel is clicked 
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.repeatTransfer = function (transactionObject,onBackPressed) {
    var self = this;

    function filterToAccounts (userAccounts, externalAccounts) {
      if(transactionObject.transactionType === 'InternalTransfer') {
        return userAccounts.filter(function (userAccount) {
            return userAccount.accountID === transactionObject.toAccountNumber;
        })
    }
    else {
        return externalAccounts.filter(function (externalAccount) {
            return externalAccount.accountNumber === transactionObject.ExternalAccountNumber;
        })
    }
    }

    function onDataFetchComplete(userAccounts, externalAccounts) {
      self.presentTransfers({
        makeTransfer: {
          fromAccounts: userAccounts.filter(function (userAccount) {return userAccount.accountID === transactionObject.fromAccountNumber}),
          toAccounts: filterToAccounts(userAccounts, externalAccounts),
          defaultFromAccountNumber: transactionObject.fromAccountNumber,
          repeatTransactionObject: transactionObject,
          onCancelCreateTransfer: onBackPressed,
          limit:self.getMinMaxTransfersLimit(getTypeByTransactionObject(transactionObject, externalAccounts))
        },
        isLoading: false
      })
    }

    self.presentTransfers({
      isLoading: true
    });

    fetchUserAccountsAndProfile(this.businessController, onDataFetchComplete);
  }

  
       /** Present Form Transfer
     * @member  Transfer_PresentationController
     * @param {object} viewModel
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.presentTransfers = function (viewModel) {
    this.presentUserInterface("frmTransfers", viewModel)
  }

    /** Present frmConfirm
     * @member  Transfer_PresentationController
     * @param {object} viewModel
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.presentTransferConfirm = function (viewModel) {
    this.presentUserInterface("frmConfirm", viewModel)
  }
  
    /** Present frmAcknowledgement
     * @member  Transfer_PresentationController
     * @param {object} viewModel
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.presentTransferAcknowledge = function (viewModel) {
    this.presentUserInterface("frmAcknowledgement", viewModel)
  }

  
    /** Present frmPrintTransfer
     * @member  Transfer_PresentationController
     * @param {object} viewModel
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.showPrintPage = function (viewModel) {
    this.presentUserInterface("frmPrintTransfer", viewModel);
  };

  
    /** Present frmConfirmAccount
     * @member  Transfer_PresentationController
     * @param {object} viewModel
     * @throws {void} None
     * @returns {void} None
     */

  
  Transfer_PresentationController.prototype.addInternalAccount = function (fController,viewModel) {
    //this.presentUserInterface("frmConfirmAccount",{"internalAccount":viewModel});
    this.loadHamburger("frmConfirmAccount",{"internalAccount":viewModel});
  };

    
    /** Present frmConfirmAccount for international
     * @member  Transfer_PresentationController
     * @param {object} viewModel
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.addInternationalAccount = function (fController,viewModel) {
    //this.presentUserInterface("frmConfirmAccount",{"internationalAccount":viewModel});
    this.loadHamburger("frmConfirmAccount",{"internationalAccount":viewModel});
  };

    
    /** Present frmConfirmAccount for domestic
     * @member  Transfer_PresentationController
     * @param {object} viewModel
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.addDomesticAccount = function (fController,viewModel) {
    //this.presentUserInterface("frmConfirmAccount",{"domesticAccount":viewModel});
     this.loadHamburger("frmConfirmAccount",{"domesticAccount":viewModel});
  };

  Transfer_PresentationController.prototype.dataForNextForm = function (viewModel) {
    this.viewModelData=viewModel;
  }; 

   /** Shows verify Account 
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */

 Transfer_PresentationController.prototype.navigateToVerifyAccount = function (fController) {
    var data=this.viewModelData;
    var self=this;
    for(var i=0;i<data.length;i++){
      var result=data[i];
      if(data[i].internalAccount){
        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            result.internalAccount["referenceNo"]=response.data.Id; //Fetching the reference Id form the service 
            self.loadHamburger("frmVerifyAccount",result);
          }else{
            self.loadHamburger("frmAddInternalAccount",{"serverError":response.data.errmsg});
          }

        }
        data[i].internalAccount["isSameBankAccount"]="true"; //Adding an additional field to identify an external account within same bank
        data[i].internalAccount["isInternationalAccount"]="false";
        data[i].internalAccount["isVerified"] = "true";
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].internalAccount, completionCallback)); 
      }
      
      if(data[i].domesticAccount){
        
        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            result.domesticAccount["referenceNo"]=response.data.Id; //Fetching the reference Id form the service 
            self.loadHamburger("frmVerifyAccount",result);
          }else{
            self.loadHamburger("frmAddExternalAccount",{"serverDomesticError":response.data.errmsg});
          }

        }
        data[i].domesticAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
        data[i].domesticAccount["isInternationalAccount"]="false";
        data[i].domesticAccount.isVerified = "true";
        if(data[i].domesticAccount.ownerImage === "unchecked_box.png"){
          self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].domesticAccount, completionCallback));
        }
        else{
          self.loadHamburger("frmVerifyAccount",data[i]);
        }
         
      }
      if(data[i].internationalAccount){
        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            result.internationalAccount["referenceNo"]=response.data.Id; //Fetching the reference Id form the service 
            self.loadHamburger("frmVerifyAccount",result);
          }else{
            self.loadHamburger("frmAddExternalAccount",{"serverInternationalError":response.data.errmsg});
          }

        }
        data[i].internationalAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
        data[i].internationalAccount["isInternationalAccount"]="true";
        data[i].internationalAccount.isVerified = "true";
        if(data[i].internationalAccount.ownerImage === "unchecked_box.png"){
          self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].internationalAccount, completionCallback));
        }
        else{
          self.loadHamburger("frmVerifyAccount",data[i]);
        }
          
      }
    }
  };

      /** Saves Transfer Data
     * @member  Transfer_PresentationController
     * @param {object} transferData Create Transfer from form Data
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.createTransfer = function (transferData) {
    var self = this;
            function createTransferCallback (response) {
                if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                    transferData.referenceId = response.data.referenceId;
                    var acknowledgeViewModel = {};
                    acknowledgeViewModel.transferData = transferData;
                    self.fetchUserAccountAndNavigate(acknowledgeViewModel);
                } else {
                  var viewmodel = {};
                  viewmodel.transferError = response.data.errmsg; 
                  self.presentTransfers(viewmodel);
                }
            }
            var accountsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
            var transactionType = transferData.accountTo instanceof accountsModel ? 'InternalTransfer' : 'ExternalTransfer';
            var toAccountNumber = transferData.accountTo instanceof accountsModel ? transferData.accountTo.accountID : transferData.accountTo.accountNumber;
            var externalAccountNumber = transactionType === "ExternalTransfer" ? transferData.accountTo.accountNumber : null;
            var commandData = {
                fromAccountNumber:  transferData.accountFrom.accountID,
                amount:transferData.amount,
                notes: transferData.notes,
                ExternalAccountNumber: externalAccountNumber,
                isScheduled: self.isFutureDate(transferData.sendOnDateComponents) || transferData.frequencyKey !== 'Once' ? "1" : "0",
                transactionType: transactionType,
                toAccountNumber: toAccountNumber,
                frequencyType: transferData.frequencyKey,
                numberOfRecurrences: transferData.howLongKey === 'NO_OF_RECURRENCES' ? transferData.noOfRecurrences : null,
                frequencyEndDate: transferData.howLongKey === 'ON_SPECIFIC_DATE' ? getDateObj(transferData.endOnDateComponents) : null,
                scheduledDate: getDateObj(transferData.sendOnDateComponents)
            }
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createTransfer", commandData, createTransferCallback));        
          
        }

    Transfer_PresentationController.prototype.getTransferType = function (account) {
      var accountsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts")
      return account instanceof accountsModel ? "InternalTransfer" : "ExternalTransfer";
  }

  Transfer_PresentationController.prototype.getSelectedExternalAccounts = function(value){
    var data=[];
    var count=0;    
    var self=this;
    function completionCallback(response){
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
        var viewModel=response.data;
        for(count=0;count<viewModel.length;count++){
          if(viewModel[count].accountNumber===value.accountNumber){
            break;
          }
        }
        var temp=Math.floor(count/10)*10;
        for(var i=temp;i<temp+value.limit;i++){
          if(viewModel[i]!==undefined&&viewModel[i]!==null){
            data.push(viewModel[i]);
          }
        }
        data.push({offset:temp,limit:value.limit,index:(count%10)});
        self.presentUserInterface("frmTransfers",{"viewSelectedExternalAccount":data});
      }else{
        //show Downtime warning
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getExternalAccounts", {}, completionCallback));  
  };

  Transfer_PresentationController.prototype.showVerifyAccounts= function(selectedRow){   
   this.presentUserInterface("frmVerifyAccount",{"verifyAccounts":selectedRow});  
  }; 


    
      /** Verify and Add Extrnal Account
     * @member  Transfer_PresentationController
     * @param {object} selectedRow Selected Row Data 
     * @throws {void} None
     * @returns {void} None
     */



  Transfer_PresentationController.prototype.confirmVerifyAndAdd = function(selectedRow){
     var self=this;
     
     function completionCallback(response){ 
       function editCompletionCallback(response){
         if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            self.getExternalAccounts();    
         }else{
          self.presentUserInterface('frmTransfers',{"serverError":true});
         }
        }
       if(response.status===kony.mvc.constants.STATUS_SUCCESS){
         var editedInfo = {
                    "accountNumber": selectedRow.accountNumber,
                    "accountType": selectedRow.accountType,
                    "nickName": selectedRow.nickName,
                    "isVerified": 1
                };  
         self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.editAccountDetails",editedInfo, editCompletionCallback)); 
       }else{
        self.presentUserInterface('frmVerifyAccount',{"errorVerifyAccount":response.data.errmsg});    
      }

    }
     this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.verifyByTrialDeposit", {"accountNumber":selectedRow.accountNumber,"firstDeposit":selectedRow.firstDeposit,"secondDeposit":selectedRow.secondDeposit}, completionCallback));  
  };

  
      /** Deletes External Account
     * @member  Transfer_PresentationController
     * @param {string} accountNumber Account Number
     * @throws {void} None
     * @returns {void} None
     */




  Transfer_PresentationController.prototype.deleteExternalAccount  = function (accountNumber) {
      var self=this;
      function completionCallback(response){
        if(response.status===kony.mvc.constants.STATUS_SUCCESS){
          self.getExternalAccounts();
        }else{
          self.presentUserInterface('frmTransfers',{"serverError":true});
        }
      }
      self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.deleteAccount",{"accountNumber":accountNumber}, completionCallback)); 
    };

    
      /** Save Changed External Account
     * @member  Transfer_PresentationController
     * @param {object} editedInfo Save Changed External Account to backend
     * @throws {void} None
     * @returns {void} None
     */


  
   Transfer_PresentationController.prototype.saveChangedExternalAccount  = function (editedInfo) {
      var self=this;
      function completionCallback(response){
        if(response.status===kony.mvc.constants.STATUS_SUCCESS){
          self.getExternalAccounts();
        }else{
           self.presentUserInterface('frmTransfers',{"serverError":true});
        }
      }
      self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.editAccountDetails",editedInfo, completionCallback)); 
    };

 
      /** Shows Selected Account Transactions
     * @member  Transfer_PresentationController
     * @param {object} selectedRow Selected Row data
     * @throws {void} None
     * @returns {void} None
     */


    
   
 Transfer_PresentationController.prototype.showSelectedAccountTransactions= function(selectedRow){
    var self=this;
     var data=[]
     function completionCallback(response){
       if(response.status===kony.mvc.constants.STATUS_SUCCESS){
         data=response.data;
         data.push({
           "accountNumber":selectedRow.lblAccountTypeValue,
           "nickName":selectedRow.lblAccountName
         })
         self.presentUserInterface("frmTransfers",{"viewExternalAccountTransactionActivity":data});
       }else{
        self.presentUserInterface('frmTransfers',{"serverError":true});
       }
     }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.viewTransactionAccountActivity", {"accountNumber":selectedRow.txtAccountNumber.text}, completionCallback));  
  }; 
  
  var externalAccountsConfig  = {
      'sortBy' : 'nickName',
      'defaultSortBy' :  'nickName',
      'order' : OLBConstants.ASCENDING_KEY,            
      'defaultOrder' : OLBConstants.ASCENDING_KEY,
      'offset': OLBConstants.DEFAULT_OFFSET,
      'limit' : OLBConstants.PAGING_ROWS_LIMIT
    };

  /** Get External accounts and sends to form
     * @member  Transfer_PresentationController
     * @param {object} value - Sorting and pagination parameters 
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.getExternalAccounts = function(value){
    var data=[];
    var self=this;
    //get the data frombackend for viewModel
    function completionCallback(response){
      if(response.status===kony.mvc.constants.STATUS_SUCCESS){
        data=response.data;
        data.push(tmpInputs);
        self.presentUserInterface("frmTransfers",{"externalAccounts":data});
      }else{
        self.presentUserInterface("frmTransfers",{"externalAccounts":"errorExternalAccounts"});
      }
    }
   
    
    var tmpInputs = CommonUtilities.Sorting.getSortConfigObject(value || {}, externalAccountsConfig);
    
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getExternalAccountPaginated",tmpInputs, completionCallback)); 
  }; 

  /** Shows External Accounts Based on flow
     * @member  Transfer_PresentationController
     * @param {object} context
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.showExternalAccounts= function(navFlow){
    //alert("Inside showExternalAccounts");
    var offset=0;
    var limit=10;
    if(navFlow==undefined){
      this.getExternalAccounts({
        "offset":offset,
        "limit":limit,
        'resetSorting': true
      });
    }else if(navFlow.getSelectedExternalAccount){
        this.getSelectedExternalAccounts({
          "accountNumber":navFlow.getSelectedExternalAccount,
          "offset":offset,
          "limit":limit
        });
      } 
    };

    /** Modify External Account
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */
  


    Transfer_PresentationController.prototype.modifyAccountInfo = function (fController) {
    var data=this.viewModelData;
    var self=this;
    for(var i=0;i<data.length;i++){
      if(data[i].internalAccount){
        //this.presentUserInterface('frmAddInternalAccount', data[i]);
        this.loadHamburger('frmAddInternalAccount', data[i]);
      }
      else{
        //this.presentUserInterface('frmAddExternalAccount', data[i]);
        this.loadHamburger('frmAddExternalAccount', data[i]);
      }
    }
  };

  /** Add Another Account 
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.addAnotherAccount = function (fController) {
    var data=this.viewModelData;
    var self=this;
    for(var i=0;i<data.length;i++){
      if(data[i].internalAccount){
        this.showSameBankAccounts();
      }
      if(data[i].domesticAccount){
        this.showDomesticAccounts();
      }
      if(data[i].internationalAccount){
        this.showInternationalAccounts();
      }      
    }
  };

   /** Cancel The Transaction
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.cancelTransaction=function(fController){
    var data=this.viewModelData;
    var self=this;
    if(kony.application.getCurrentForm().id!=="frmAddExternalAccount" || kony.application.getCurrentForm().id!=="frmAddInternalAccount"){
      this.presentUserInterface('frmTransfers');
    }
    else{
      for(var i=o;i<data.lengrth;i++){
        if(data[i].internalAccount){
          this.showSameBankAccounts();
        }
        else if(data[i].domesticAccount){
          this.showDomesticAccounts();
        }else{
          this.showInternationalAccounts();
        }
      }
    }
  };

   /** Shows Make Transfer Form
     * @member  Transfer_PresentationController
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.makeTransfer=function(fController){
    this.presentUserInterface("frmTransfers");
  };

   /** Initialize Hamburger Extension for a form
     * @member  Transfer_PresentationController
     * @param  {string} frm Name of the page
     * @throws {void} None
     * @returns {void} None
     */


  Transfer_PresentationController.prototype.loadHamburger = function(frm,viewModel){
    var self = this;
    var howToShowHamburgerMenu = function(sideMenuViewModel){
      var data=[{sideMenu : sideMenuViewModel}];
      data.push(viewModel);
      self.presentUserInterface(frm,data);
      // this.presentTransfers({sideMenu : sideMenuViewModel});        
    };
    self.SideMenu.init(howToShowHamburgerMenu); 

    var presentTopBar = function(topBarViewModel) {
      var data=[{topBar : topBarViewModel}];
      data.push(viewModel);
      self.presentUserInterface(frm,data);
      // this.presentTransfers({topBar : topBarViewModel});
    };
    self.TopBar.init(presentTopBar);
  }; 

   /** Initialize Hamburger Extension for a form
     * @member  Transfer_PresentationController
     * @param  {string} frm Name of the page
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.loadHamburgerTransfers = function(frm){
    var self = this;
    var howToShowHamburgerMenu = function(sideMenuViewModel){
      self.presentUserInterface(frm,{sideMenu : sideMenuViewModel});
      // this.presentTransfers({sideMenu : sideMenuViewModel});        
    };
    self.SideMenu.init(howToShowHamburgerMenu); 

    var presentTopBar = function(topBarViewModel) {
      self.presentUserInterface(frm,{topBar : topBarViewModel});
      // this.presentTransfers({topBar : topBarViewModel});
    };
    self.TopBar.init(presentTopBar);
  }; 

   /** Verify Account By Credentials
     * @member  Transfer_PresentationController
     * @param  {object} viewModel Data Of Verification Page 
     * @throws {void} None
     * @returns {void} None
     */
  
  Transfer_PresentationController.prototype.verifyCredentials = function(data){
    var self=this;

    function completionCallback(response){

      if(response.status === kony.mvc.constants.STATUS_SUCCESS){ 
        if(response.data.result === "Successful")
          self.toAddVerifyAccount();
        else{
        self.presentUserInterface("frmVerifyAccount",{"invalidCredential":"true"});
      }

      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.verifyCredentials", {data:data}, completionCallback));

  };

Transfer_PresentationController.prototype.addToVerifyAccount = function (viewModel) {
    var data=this.viewModelData;
    var self=this;
    for(var i=0;i<data.length;i++){
      var result=data[i];

      if(data[i].domesticAccount){
        var k =i;
        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            if(viewModel.ExternalAccountNumber){

              function completionCallback(response){

                if(response.status === kony.mvc.constants.STATUS_SUCCESS){ 
                  self.presentUserInterface("frmVerifyAccount",{"validateByTrialDeposit":"successfull"});
                  //self.loadHamburger("frmVerifyAccount",data);
                }
              }
              self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.makeTrialDeposit", data[k].domesticAccount.accountNumber, completionCallback));
            }

          }else{
            self.loadHamburger("frmAddExternalAccount",{"serverDomesticError":response.data.errmsg});
          }
        }
        data[i].domesticAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
        data[i].domesticAccount["isInternationalAccount"]="false";
        data[i].domesticAccount.isVerified = "false";
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].domesticAccount, completionCallback)); 
      }
      if(data[i].internationalAccount){
        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){

            if(viewModel.ExternalAccountNumber){

              function completionCallback(response){

                if(response.status === kony.mvc.constants.STATUS_SUCCESS){ 
                  self.presentUserInterface("frmVerifyAccount",{"validateByTrialDeposit":"successfull"});
                  //self.loadHamburger("frmVerifyAccount",data);
                }else{

                }
              }
              self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.makeTrialDeposit", data[1].internationalAccount.accountNumber, completionCallback));
            }

          }else{
            self.loadHamburger("frmAddExternalAccount",{"serverInternationalError":response.data.errmsg});
          }

        }
        data[i].internationalAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
        data[i].internationalAccount["isInternationalAccount"]="true";
        data[i].internationalAccount.isVerified = "false";
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].internationalAccount, completionCallback)); 
      }
    }
  };

   /** Creates  External Account and Show Verification Page
     * @member  Transfer_PresentationController
     * @param  {object} viewModel Data Of External Accounts 
     * @throws {void} None
     * @returns {void} None
     */
  
 Transfer_PresentationController.prototype.toAddVerifyAccount = function (viewModel) {
    var data=this.viewModelData;
    var self=this;
    for(var i=0;i<data.length;i++){
      var result=data[i];

      if(data[i].domesticAccount){

        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            self.presentUserInterface("frmVerifyAccount",{"validateByCredential":"successfull"});
          }else{
            self.loadHamburger("frmAddExternalAccount",{"serverDomesticError":response.data.errmsg});
          }
        }
        data[i].domesticAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
        data[i].domesticAccount["isInternationalAccount"]="false";
        data[i].domesticAccount.isVerified = "true";
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].domesticAccount, completionCallback)); 
      }
      if(data[i].internationalAccount){
        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            self.presentUserInterface("frmVerifyAccount",{"validateByCredential":"successfull"});

          }else{
            self.loadHamburger("frmAddExternalAccount",{"serverInternationalError":response.data.errmsg});
          }

        }
        data[i].internationalAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
        data[i].internationalAccount["isInternationalAccount"]="true";
        data[i].internationalAccount.isVerified = "true";
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].internationalAccount, completionCallback)); 
      }
    }
  };

  /** Creates  External Account and Show Verification Page
     * @member  Transfer_PresentationController
     * @param  {object} viewModel Data Of External Accounts 
     * @throws {void} None
     * @returns {void} None
     */

  Transfer_PresentationController.prototype.toAddVerifyAccount = function (viewModel) {
    var data=this.viewModelData;
    var self=this;
    for(var i=0;i<data.length;i++){
      var result=data[i];

      if(data[i].domesticAccount){

        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            self.presentUserInterface("frmVerifyAccount",{"validateByCredential":"successfull"});
          }else{
            self.loadHamburger("frmAddExternalAccount",{"serverDomesticError":response.data.errmsg});
          }
        }
        data[i].domesticAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
        data[i].domesticAccount["isInternationalAccount"]="false";
        if(data[i].domesticAccount.ownerImage === "unchecked_box.png"){
          data[i].domesticAccount.isVerified = "false";

        }
        else{
          data[i].domesticAccount.isVerified = "true";
        }
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].domesticAccount, completionCallback)); 
      }
      if(data[i].internationalAccount){
        function completionCallback(response){
          if(response.status===kony.mvc.constants.STATUS_SUCCESS){
            self.presentUserInterface("frmVerifyAccount",{"validateByCredential":"successfull"});

          }else{
            self.loadHamburger("frmAddExternalAccount",{"serverInternationalError":response.data.errmsg});
          }

        }
        data[i].internationalAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
        data[i].internationalAccount["isInternationalAccount"]="true";
        if(data[i].internationalAccount.ownerImage === "unchecked_box.png"){
          data[i].internationalAccount.isVerified = "false";
        }
        else{
          data[i].internationalAccount.isVerified = "true";
        }
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].internationalAccount, completionCallback)); 
      }
    }
  };
  /** Search Payees 
     * @member  Transfer_PresentationController
     * @param  {object} data Search Inputs
     * @throws {void} None
     * @returns {void} None
     */
  Transfer_PresentationController.prototype.searchTransferPayees = function (data) {
    var scopeObj = this;

    if(data && data.searchKeyword.length >= 0) {
      var searchInputs = {
        'searchString': data.searchKeyword
      };
  
      function completionCallback(response) {
        var viewModel = {};
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          viewModel.searchTransferPayees = {
            externalAccounts : response.data,
            searchInputs : searchInputs
          };
        } else {
          viewModel.searchTransferPayees = { "error": response };
        }
        scopeObj.presentTransfers(viewModel)
      }
      
      scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.getExternalAccountsByCriteria", searchInputs, completionCallback));
    } else {
    }
    
  }; 
  /** fetchBankDetails
     * @member  Transfer_PresentationController
     * @param  {routingNumber,serviceName} 
     * @throws {void} None
     * @returns {void} None
     */ 
  Transfer_PresentationController.prototype.fetchBankDetails=function(routingNumber,serviceName){
    var self=this;
    var params={
      "routingNumber":routingNumber,
      "serviceName":serviceName
    }
     function completionCallBack(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                 self.presentUserInterface("frmAddExternalAccount",{"context":"updateBankName", "data":response.data.bankName});
            }else{
              CommonUtilities.showServerDownScreen();
            }
      }

    var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
     accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.fetchBankDetails", params, completionCallBack));  
  };
   /** fetchBankDetailsForInternationalTransfer
     * @member  Transfer_PresentationController
     * @param  {swiftCode,serviceName} 
     * @throws {void} None
     * @returns {void} None
     */ 
  Transfer_PresentationController.prototype.fetchBankDetailsForInternationalTransfer=function(swiftCode,serviceName){
     var self=this;
    var params={
      "swiftCode":swiftCode,
      "serviceName":serviceName
    }
     function completionCallBack(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                 self.presentUserInterface("frmAddExternalAccount",{"context":"updateInternationalBankName", "data":response.data.bankName});
            }else{
              CommonUtilities.showServerDownScreen();
            }
      }

    var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
     accountModule.businessController.execute(new kony.mvc.Business.Command("com.kony.accounts.fetchBankDetails", params, completionCallBack));  
  };
  Transfer_PresentationController.prototype.cancelTransactionOccurrence = function(transaction){
    var self = this;
    function cancelTransactionOccurrenceCallback(response){
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.fetchScheduledUserTransactions("frmTransfers", {
          offset: 0,
          limit: 10
        });
      } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {};
    }
    this.businessController.execute(new kony.mvc.Business.Command('com.kony.transfer.cancelTransactionOccurrence', transaction, cancelTransactionOccurrenceCallback));
  };
  return Transfer_PresentationController;
});