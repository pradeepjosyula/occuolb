define(function () {
   
    return {
        navigateToVerifyAccount : function (fController,segData) {
          
          if(this.viewModelData){
            var data=this.viewModelData;
          }
          else{
            data=segData;
          }
          
          
          
          
          var self=this;
          if(data.length==0||data.length==undefined){
				if (data.lblIdentifier) {
                    self.loadHamburger("frmVerifyAccount", data);
                    //self.showVerifyAccountRecipient(data);
                }
			}
          for(var i=0;i<data.length;i++){
            var result=data[i];
            if(data[i].internalAccount){
              function completionCallback(response){
                if(response.status==kony.mvc.constants.STATUS_SUCCESS){
                  result.internalAccount["referenceNo"]=response.data.Id; //Fetching the reference Id form the service
                  result.internalAccount["successFlag"]=response.data[0].success;
            	  result.internalAccount["errMsg"]=response.data[0].error;
                  self.loadHamburger("frmVerifyAccount",result);
                }else{
                  self.loadHamburger("frmAddInternalAccount",{"serverError":response.data.errmsg});
                }

              }
              data[i].internalAccount["isSameBankAccount"]="true"; //Adding an additional field to identify an external account within same bank
              data[i].internalAccount["isInternationalAccount"]="false";
              data[i].internalAccount["isVerified"] = "true";
              self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].internalAccount, completionCallback)); 
            }

            if(data[i].domesticAccount){

              function completionCallback(response){
                if(response.status==kony.mvc.constants.STATUS_SUCCESS){
                  result.domesticAccount["success"]=response.data[0].success;
            	  result.domesticAccount["errMsg"]=response.data[0].errmsg;
                  result.domesticAccount["referenceNo"]=response.data[0].Id; //Fetching the reference Id form the service 
                  if(response.data[0].success == "true" || response.data[0].success == true){
                            result.domesticAccount["bankName"] = response.data[1].bankName;
							} else{ 
							result.domesticAccount["bankName"] = response.data[0].bankName;
							}
                 // result.domesticAccount["bankName"]=response.data[0].bankName;
                  self.loadHamburger("frmVerifyAccount",result);
                }else{
                  self.loadHamburger("frmAddExternalAccount",{"serverDomesticError":response.data[0].errmsg});
                }

              }
              data[i].domesticAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
              data[i].domesticAccount["isExternalAccount"]="true";
             // data[i].domesticAccount.isVerified = "true";
             // if(data[i].domesticAccount.ownerImage === "unchecked_box.png"){
                self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].domesticAccount, completionCallback));
             /* }
              else{
                self.loadHamburger("frmVerifyAccount",data[i]);
              }*/

            }
            if(data[i].internationalAccount){
              function completionCallback(response){
                if(response.status==kony.mvc.constants.STATUS_SUCCESS){
                  result.internationalAccount["referenceNo"]=response.data.Id; //Fetching the reference Id form the service 
                  self.loadHamburger("frmVerifyAccount",result);
                }else{
                  self.loadHamburger("frmAddExternalAccount",{"serverInternationalError":response.data.errmsg});
                }

              }
              data[i].internationalAccount["isSameBankAccount"]="false"; //Adding an additional field to identify an external account within same bank
              data[i].internationalAccount["isInternationalAccount"]="true";
              data[i].internationalAccount.isVerified = "true";
              if(data[i].internationalAccount.ownerImage === "unchecked_box.png"){
                self.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.createBankAccount", data[i].internationalAccount, completionCallback));
              }
              else{
                self.loadHamburger("frmVerifyAccount",data[i]);
              }

            }
//             if(data[i].lblIdentifier){
              
              
//                   self.loadHamburger("frmVerifyAccount",data);
                
            

            
              
//               this.showVerifyAccountScreen(data);
//             }
          }
      },     
      
      
      //// for navigating to verify screen
      
      
//       showVerifyAccountRecipient: function(selectedData) {
//         kony.application.getCurrentForm().acknowledgmentVer.setVisibility(false);
//         kony.application.getCurrentForm().verifyByCredential.setVisibility(true);

//         if(kony.application.getCurrentForm().CheckBoxGroup0cb331bee901341.selectedKeyValues){
//           kony.application.getCurrentForm().CheckBoxGroup0cb331bee901341.selectedKeyValues.length=0;
//         }


//         kony.application.getCurrentForm().tbxUsername.onKeyUp = function() {
//           self.validateDepositInput();
//         };
//         kony.application.getCurrentForm().tbxPassword.onKeyUp = function() {
//           self.validateDepositInput();
//         };
//         kony.application.getCurrentForm().CheckBoxGroup0cb331bee901341.onSelection = function() {
//           self.validateDepositInput();
//         };


//         kony.application.getCurrentForm().lblBillerValue.text=selectedData.lblBankName;
//         kony.application.getCurrentForm().lblAccountTypeValue.text=selectedData.lblAccountTypeValue;
//         kony.application.getCurrentForm().lblAccountNumberValue.text=selectedData.lblRoutingNumberValue;
//         kony.application.getCurrentForm().lblBeneficiaryNameValue.text=selectedData.lblSeparatorActions;
//         kony.application.getCurrentForm().lblAccountNickNameValue.text=selectedData.lblAccountNumberTitleUpper;

//         kony.application.getCurrentForm().lblUsername.text = kony.i18n.getLocalizedString("i18n.transfers.depositAmount1");
//         kony.application.getCurrentForm().lblPassword.text = kony.i18n.getLocalizedString("i18n.transfers.depositAmount2");
//         kony.application.getCurrentForm().tbxUsername.placeholder = kony.i18n.getLocalizedString("i18n.transfers.EnterAmountHere");
//         kony.application.getCurrentForm().tbxPassword.placeholder = kony.i18n.getLocalizedString("i18n.transfers.EnterAmountHere");
//         kony.application.getCurrentForm().lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.transfers.verifyAccountLC");
//         kony.application.getCurrentForm().lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.verifyByProvidingDeposits");
//         kony.application.getCurrentForm().Label0f96e5b8496f040.text = selectedRow.lblBankName;
//       },
     
      
      
      
      showSameRecBankAccounts : function(recdata) {
        var self = this;	
        function completionCallback(response) {
            var data = {};
            data["sameAccounts"] = response.data;
			data["newrecipient"] = recdata;
            //self.presentUserInterface("frmAddInternalAccount",data);
            self.loadHamburger("frmAddInternalAccount", data);
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.transfer.getSameBankAccounts", {}, completionCallback));
    }
    };
});