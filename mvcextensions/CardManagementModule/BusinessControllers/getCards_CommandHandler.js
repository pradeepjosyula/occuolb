define([], function() {

  	function CardManagement_getCards_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_getCards_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_getCards_CommandHandler.prototype.execute = function(command){
		var self = this;
      	function  completionCallback(status,  data,  error) {
      		self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
    	}
      	var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Cards");
      	cardsModel.getAll(completionCallback);
    };
	
	CardManagement_getCards_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_getCards_CommandHandler;
    
});