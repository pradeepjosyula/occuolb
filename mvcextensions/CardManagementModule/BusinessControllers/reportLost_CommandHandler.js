define([], function() {

  	function CardManagement_reportLost_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_reportLost_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_reportLost_CommandHandler.prototype.execute = function(command){
		var self = this;
      	var params = {
          cardId: command.context.cardId,
          Reason: command.context.Reason,
          notes: command.context.notes,
          Action: 'Report Lost'
        };
      	function onCompletion(status, data, error){
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
      	var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Cards');
      	cardsModel.customVerb('reportLost', params, onCompletion);
    };
	
	CardManagement_reportLost_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_reportLost_CommandHandler;
    
});