define([], function() {

  	function CardManagement_unlockCard_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_unlockCard_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_unlockCard_CommandHandler.prototype.execute = function(command){
		var self = this;
      	var params = {
          cardId: command.context.cardId,
          'Action': 'Activate'
        };
      	function onCompletion(status, data, error){
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data: error);
        }
      	var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Cards');
      	cardsModel.customVerb('unlockCard', params, onCompletion);
    };
	
	CardManagement_unlockCard_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_unlockCard_CommandHandler;
    
});