define([], function() {

  	function CardManagement_sendSecureAccessCode_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_sendSecureAccessCode_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_sendSecureAccessCode_CommandHandler.prototype.execute = function(command){
     	var self = this;
        function completionCallBack(status, response, err) {
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS? response: err);
        }
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.customVerb("requestOTP", {}, completionCallBack);
    };
	
	CardManagement_sendSecureAccessCode_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_sendSecureAccessCode_CommandHandler;
    
});