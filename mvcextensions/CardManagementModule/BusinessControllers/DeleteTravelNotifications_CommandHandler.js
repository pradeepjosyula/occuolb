define([], function() {

  	function CardManagement_DeleteTravelNotifications_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_DeleteTravelNotifications_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_DeleteTravelNotifications_CommandHandler.prototype.execute = function(command){
        var self = this;
        function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Cards");                
            cardsModel.customVerb("deleteTravelNotification", command.context, completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }	
    };
	
	CardManagement_DeleteTravelNotifications_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_DeleteTravelNotifications_CommandHandler;
    
});