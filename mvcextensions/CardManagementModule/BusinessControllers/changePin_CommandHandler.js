define([], function() {

  	function CardManagement_changePin_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_changePin_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_changePin_CommandHandler.prototype.execute = function(command){
      var self = this;
      var params = {
        'cardId': command.context.cardId,
        'Reason': command.context.Reason,
        'Action': 'PinChange'
      };
      function onCompletion(status, data, error){
        self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS?data:error);
      }
      var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Cards');
	  cardsModel.customVerb('changePIN', params, onCompletion);
    };
	
	CardManagement_changePin_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_changePin_CommandHandler;
    
});