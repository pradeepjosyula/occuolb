define([], function() {

  	function CardManagement_GetTravelNotifications_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_GetTravelNotifications_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_GetTravelNotifications_CommandHandler.prototype.execute = function(command){
		var self = this;
      	function  completionCallback(status,  data,  error) {
      		self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
		}
		function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Cards");                
            cardsModel.customVerb("getTravelNotification", command.context, completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }	
    };
	
	CardManagement_GetTravelNotifications_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_GetTravelNotifications_CommandHandler;
    
});