define([], function() {

  	function CardManagement_verifySecureAccessCode_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_verifySecureAccessCode_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_verifySecureAccessCode_CommandHandler.prototype.execute = function(command){
		var self = this;
        function completionCallBack(status, data, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, data);
            } else {
                self.sendResponse(command, status, err);
            }
        }
        var params = {
            otp: command.context.otp
        };
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.customVerb("verifyOTP", params, completionCallBack);
    };
	
	CardManagement_verifySecureAccessCode_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_verifySecureAccessCode_CommandHandler;
    
});