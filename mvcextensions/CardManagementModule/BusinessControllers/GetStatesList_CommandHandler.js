define([], function() {

  	function CardManagement_getStatesList_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_getStatesList_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_getStatesList_CommandHandler.prototype.execute = function(command){
        var self = this;
        
                function completionCallBack(status, response, err) {
                    if (status === kony.mvc.constants.STATUS_SUCCESS) {
                        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
                    } else {
                        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                    }
                }
                try {
                    var StatesModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("States");
                    StatesModel.getAllRegions({},completionCallBack);
                } catch (err) {
                    self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                }
    };
	
	CardManagement_getStatesList_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_getStatesList_CommandHandler;
    
});