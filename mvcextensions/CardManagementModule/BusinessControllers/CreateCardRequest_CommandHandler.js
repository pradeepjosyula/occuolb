define([], function () {
    function CardManagement_CreateCardRequest_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
    inheritsFrom(CardManagement_CreateCardRequest_CommandHandler, kony.mvc.Business.CommandHandler);
    CardManagement_CreateCardRequest_CommandHandler.prototype.execute = function (command) {
        var self = this;
        function onCompletion(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Cards');
        cardsModel.customVerb('createCardRequest', command.context, onCompletion);
    };
    CardManagement_CreateCardRequest_CommandHandler.prototype.validate = function () {
    };
    return CardManagement_CreateCardRequest_CommandHandler;
});