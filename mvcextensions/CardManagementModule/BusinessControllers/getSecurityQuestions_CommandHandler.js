define([], function() {

  	function CardManagement_getSecurityQuestions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_getSecurityQuestions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_getSecurityQuestions_CommandHandler.prototype.execute = function(command){
		var self = this;
      	function completionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try  {
          var questionsRepo  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('SecurityQuestions');
          questionsRepo.customVerb('getRandomCustomerSecurityQuestions', {userName: command.context.userName}, completionCallback);
        }  catch  (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
	
	CardManagement_getSecurityQuestions_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_getSecurityQuestions_CommandHandler;
    
});