define([], function() {

  	function CardManagement_GetActiveCards_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_GetActiveCards_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_GetActiveCards_CommandHandler.prototype.execute = function(command){
		var self = this;
      	function  completionCallback(status,  data,  error) {
      		self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
    	}
      	var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Cards");
      	cardsModel.customVerb("getActiveCards",{},completionCallback);
    };
	
	CardManagement_GetActiveCards_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_GetActiveCards_CommandHandler;
    
});