define([], function() {

  	function CardManagement_lockCard_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_lockCard_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_lockCard_CommandHandler.prototype.execute = function(command){
		var self = this;
      	var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Cards');
      	var params = {
          'cardId': command.context.cardId,
          'Action': 'Lock'
        };
      	function completionCallback(status, data, err){
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : err);
        }
      	cardsModel.customVerb('lockCard', params, completionCallback);
    };
	
	CardManagement_lockCard_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_lockCard_CommandHandler;
    
});