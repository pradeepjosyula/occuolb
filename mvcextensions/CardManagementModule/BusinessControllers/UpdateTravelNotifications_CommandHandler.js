define([], function() {

  	function CardManagement_UpdateTravelNotifications_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_UpdateTravelNotifications_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_UpdateTravelNotifications_CommandHandler.prototype.execute = function(command){
		var self = this;
        var tavelObject ={
            "request_id":command.context.request_id,  
            "Destinations": command.context.destinations,
            "Channel_id": command.context.channelId,
            "StartDate": command.context.StartDate,
            "userName": command.context.username,
            "description": command.context.description,
            "additionNotes":command.context.additionNotes,
            "EndDate": command.context.EndDate,
            "phonenumber": command.context.phoneNumber,
            "Cards": command.context.cards
          };
        function completionCallBack(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
          }
        }
            try {
              var travelModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Cards");
              travelModel.customVerb('updateTravelNotification',tavelObject,completionCallBack);
            } catch (err) {
              self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
            }
    };
	
	CardManagement_UpdateTravelNotifications_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_UpdateTravelNotifications_CommandHandler;
    
});