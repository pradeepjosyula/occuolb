define([], function() {

  	function CardManagement_replaceCard_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_replaceCard_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_replaceCard_CommandHandler.prototype.execute = function(command){
		var self = this;
      	var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Cards');
      	var params = {
          'cardId': command.context.cardId,
          'Action': 'Replace Request'
        };
      	function completionCallback(status, data, err){
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : err);
        }
      	cardsModel.customVerb('replaceCard', params, completionCallback);
    };
	
	CardManagement_replaceCard_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_replaceCard_CommandHandler;
    
});