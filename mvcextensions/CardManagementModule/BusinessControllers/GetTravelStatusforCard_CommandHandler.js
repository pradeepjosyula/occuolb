define([], function() {

  	function CardManagement_GetTravelStatusforCard_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_GetTravelStatusforCard_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_GetTravelStatusforCard_CommandHandler.prototype.execute = function(command){
		var self = this;

        function onCompletion(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        var cardsStatusModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('Cards');
        cardsStatusModel.customVerb('getTravelNotificationStatus', command.context, onCompletion);
    };
	
	CardManagement_GetTravelStatusforCard_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_GetTravelStatusforCard_CommandHandler;
    
});