define([], function() {

  	function CardManagement_verifySecurityQuestions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_verifySecurityQuestions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_verifySecurityQuestions_CommandHandler.prototype.execute = function(command){
		var self = this;
      	function onCompletion(status, data, error){
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data: error);
        }
      	var securityQuestionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('SecurityQuestions');
      	securityQuestionsModel.customVerb('verifyCustomerSecurityQuestions', command.context, onCompletion);
    };
	
	CardManagement_verifySecurityQuestions_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_verifySecurityQuestions_CommandHandler;
    
});