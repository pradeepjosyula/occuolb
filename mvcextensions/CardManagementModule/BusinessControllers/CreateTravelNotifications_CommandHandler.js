define([], function() {

  	function CardManagement_CreateTravelNotifications_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_CreateTravelNotifications_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_CreateTravelNotifications_CommandHandler.prototype.execute = function(command){
		var self = this;
        var tavelObject = {
            "Destinations": command.context.destinations,
            "Channel_id": command.context.channelId,
            "StartDate": command.context.StartDate,
            "userName": command.context.username,
            "description": command.context.description,
            "additionNotes":command.context.additionNotes,
            "EndDate": command.context.EndDate,
            "phonenumber": command.context.phoneNumber,
            "Cards": command.context.cards
          };
        function completionCallBack(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
          }
        }
            try {
              var travelModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Cards");          
              travelModel.customVerb('createTravelNotification',tavelObject,completionCallBack);
            } catch (err) {
              self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
            }
    };
	
	CardManagement_CreateTravelNotifications_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_CreateTravelNotifications_CommandHandler;
    
});