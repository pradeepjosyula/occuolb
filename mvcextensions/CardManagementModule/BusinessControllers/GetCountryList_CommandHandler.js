define([], function() {

  	function CardManagement_getCountryList_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(CardManagement_getCountryList_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	CardManagement_getCountryList_CommandHandler.prototype.execute = function(command){
		var self = this;
        
                function completionCallBack(status, response, err) {
                    if (status === kony.mvc.constants.STATUS_SUCCESS) {
                        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
                    } else {
                        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                    }
                }
                try {
                    var CountryModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Country");
                    CountryModel.getAllCountries({},completionCallBack);
                } catch (err) {
                    self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
                }
    };
	
	CardManagement_getCountryList_CommandHandler.prototype.validate = function(){
		
    };
    
    return CardManagement_getCountryList_CommandHandler;
    
});