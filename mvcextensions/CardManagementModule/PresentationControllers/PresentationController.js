define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants){

	var MDABasePresenter = kony.mvc.Presentation.BasePresenter;	
		
    function CardManagement_PresentationController() {
        MDABasePresenter.call(this);
        this.country = null;
        this.states = null;
        this.city = null;
    }

    inheritsFrom(CardManagement_PresentationController, MDABasePresenter);

    CardManagement_PresentationController.prototype.initializePresentationController = function() {
        
    };
  
	CardManagement_PresentationController.prototype.loadCardManagementComponents = function () {
        this.loadComponents('frmCardManagement');
    };
  
    CardManagement_PresentationController.prototype.loadComponents = function (frm) {
        var scopeObj = this;
        var howToShowHamburgerMenu = function (sideMenuViewModel) {
            scopeObj.presentUserInterface(frm, { "sideMenu": sideMenuViewModel });
        };

        scopeObj.SideMenu.init(howToShowHamburgerMenu);

        var presentTopBar = function (topBarViewModel) {
            scopeObj.presentUserInterface(frm, { "topBar": topBarViewModel });
        };
        scopeObj.TopBar.init(presentTopBar);
    };
  
  	/**
     * navigateToManageCards - Entry point to Cards Management.
     * @member of {CardManagement_PresentationController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.navigateToManageCards = function(){
        var cardsViewModel = {};
        cardsViewModel.progressBar = true;
        this.presentUserInterface('frmCardManagement', cardsViewModel);
        this.loadCardManagementComponents();
        this.fetchCardsList();
    };
  	
    /**
     * constructCardsViewModel - Generates a viewModel for cards based on card type.
     * @member of {CardManagement_PresentationController}
     * @param {Array} - Array of cards.
     * @returns {object}  - constructed view model for cards.
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.constructCardsViewModel = function(cards){
        var self = this;
      	var cardsViewModel = [];
      	cards.forEach(function(card){
          if(card.cardType === OLBConstants.CARD_TYPE.Debit){
            cardsViewModel.push(self.getDebitCardViewModel(card));
          }
          else if(card.cardType === OLBConstants.CARD_TYPE.Credit){
            cardsViewModel.push(self.getCreditCardViewModel(card));
          }
        });
      	return cardsViewModel;
    };
  	
    /**
     * getDebitCardViewModel - Generates a viewModel for the given debit card.
     * @member of {CardManagement_PresentationController}
     * @param {Object} - Debit card object.
     * @returns {Object}  - constructed view model for debit card.
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.getDebitCardViewModel = function(debitCard){
        var debitCardViewModel = [];
        var debitCardActions = [];
      	debitCardViewModel.cardId = debitCard.cardId;
        debitCardViewModel.cardType = debitCard.cardType;
        debitCardViewModel.cardStatus = debitCard.cardStatus;
        debitCardViewModel.cardNumber = debitCard.cardNumber;
        debitCardViewModel.maskedCardNumber = debitCard.maskedCardNumber;
        debitCardViewModel.productName = debitCard.cardProductName;
        debitCardViewModel.validThrough = this.getValidThroughForCard(debitCard.expiryDate);
        debitCardViewModel.dailyWithdrawalLimit = CommonUtilities.formatCurrencyWithCommas(debitCard.withdrawlLimit);
        debitCardViewModel.accountName = debitCard.accountName;
        debitCardViewModel.maskedAccountNumber = debitCard.maskedAccountNumber;
        debitCardViewModel.serviceProvider = debitCard.serviceProvider;
        debitCardViewModel.cardHolder = debitCard.cardHolderName;
        debitCardViewModel.secondaryCardHolder = debitCard.secondaryCardHolder;
      
        switch(debitCard.cardStatus){
          case OLBConstants.CARD_STATUS.Active: {
            debitCardActions.push(OLBConstants.CARD_ACTION.Lock);
            debitCardActions.push(OLBConstants.CARD_ACTION.Replace);
            debitCardActions.push(OLBConstants.CARD_ACTION.Report_Lost);
            debitCardActions.push(OLBConstants.CARD_ACTION.Change_Pin);
            break;
          }
          case OLBConstants.CARD_STATUS.Locked: {
            debitCardActions.push(OLBConstants.CARD_ACTION.Unlock);
            debitCardActions.push(OLBConstants.CARD_ACTION.Replace);
            debitCardActions.push(OLBConstants.CARD_ACTION.Report_Lost);
            break;
          }
          case OLBConstants.CARD_STATUS.ReportedLost: {
            debitCardActions.push(OLBConstants.CARD_ACTION.Replace);
            break;
          }
          default: {
            break;
          }
        }
        debitCardViewModel.actions = debitCardActions;
        return debitCardViewModel;
    };
  	
    /**
     * getValidThroughForCard - Generates a validThrough/expiry date for a given timestamp.
     * @member of {CardManagement_PresentationController}
     * @param {String} - Expiry Date Timestamp.
     * @returns {String}  - Expiry Date in mm/yy format.
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.getValidThroughForCard = function(expiryDate){
      	if(expiryDate === null || expiryDate === undefined || expiryDate === "")
          return "";
      	expiryDate = expiryDate.split('T')[0];
      	expiryDate = expiryDate.split('-');
      	return expiryDate[1]+'/'+expiryDate[0].slice(2);
    };

    /**
     * getCreditCardViewModel - Generates a viewModel for the given credit card.
     * @member of {CardManagement_PresentationController}
     * @param {Object} - credit card object.
     * @returns {Object}  - constructed view model for credit card.
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.getCreditCardViewModel = function(creditCard){
        var creditCardViewModel = [];
        var creditCardActions = [];
      	creditCardViewModel.cardId = creditCard.cardId;
        creditCardViewModel.cardType = creditCard.cardType;
        creditCardViewModel.cardStatus = creditCard.cardStatus;
        creditCardViewModel.cardNumber = creditCard.cardNumber;
        creditCardViewModel.maskedCardNumber = creditCard.maskedCardNumber;
        creditCardViewModel.productName = creditCard.cardProductName;
        creditCardViewModel.validThrough = this.getValidThroughForCard(creditCard.expiryDate);
        creditCardViewModel.creditLimit = CommonUtilities.formatCurrencyWithCommas(creditCard.creditLimit);
        creditCardViewModel.availableCredit = CommonUtilities.formatCurrencyWithCommas(creditCard.availableCredit);
        creditCardViewModel.serviceProvider = creditCard.serviceProvider;
        creditCardViewModel.cardHolder = creditCard.cardHolderName;
        creditCardViewModel.secondaryCardHolder = creditCard.secondaryCardHolderName;
        creditCardViewModel.billingAddress = creditCard.billingAddress;
        
        switch(creditCard.cardStatus){
          case OLBConstants.CARD_STATUS.Active: {
            creditCardActions.push(OLBConstants.CARD_ACTION.Lock);
            creditCardActions.push(OLBConstants.CARD_ACTION.Replace);
            //creditCardActions.push(OLBConstants.CARD_ACTION.Cancel);
            creditCardActions.push(OLBConstants.CARD_ACTION.Report_Lost);
            creditCardActions.push(OLBConstants.CARD_ACTION.Change_Pin);
            break;
          }
          case OLBConstants.CARD_STATUS.Locked: {
            creditCardActions.push(OLBConstants.CARD_ACTION.Unlock);
            //creditCardActions.push(OLBConstants.CARD_ACTION.Cancel);
            creditCardActions.push(OLBConstants.CARD_ACTION.Replace);
            creditCardActions.push(OLBConstants.CARD_ACTION.Report_Lost);
            break;
          }
          case OLBConstants.CARD_STATUS.ReportedLost: {
            creditCardActions.push(OLBConstants.CARD_ACTION.Replace);
            //creditCardActions.push(OLBConstants.CARD_ACTION.Cancel);
            break;
          }
          default: {
            break;
          }
        }
        creditCardViewModel.actions = creditCardActions;
        return creditCardViewModel;
    };

    /**
     * fetchCardsStatus - Issues a command to fetch travel status  associated with the current user's cards and then presents the user interface.
     * @member of {CardManagement_PresentationController}
     * @param {cards} - array of card ids
     * @returns {VOID} 
     * @throws {}
     */
    CardManagement_PresentationController.prototype.fetchCardsStatus = function(cards) {
        var self = this;
        var cardsViewModel = {};
        var cardsViewArray = [];
        cards.forEach(function(card) {
            cardsViewArray.push(card.maskedCardNumber);
        });
		cardsViewArray=JSON.stringify(cardsViewArray);
        cardsViewArray = cardsViewArray.replace(/"/g , "'");
        var context = {
            "CardNumbers": cardsViewArray,
            "userName": kony.mvc.MDAApplication.getSharedInstance().appContext.username
        };
        
        function fetchStatusCompletionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                cardsViewModel.progressBar = true;
                cardsViewModel.travelStatus = {
                    "status": response.data.CardStatus,
                    "data": cards
                };
                self.presentUserInterface('frmCardManagement', cardsViewModel);
            } else {
                cardsViewModel.serverDown = true;
                self.presentUserInterface('frmCardManagement', cardsViewModel);
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.getTravelStatusforCard', context, fetchStatusCompletionCallback));
    };
    /**
     * fetchCardsList - Issues a command to fetch all the cards associated with the current user and then presents the user interface.
     * @member of {CardManagement_PresentationController}
     * @param {} - NONE
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.fetchCardsList = function(){
      var self = this;
      function fetchCardsCompletionCallback(response){
        var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          cardsViewModel.progressBar = true;
          cardsViewModel.cards = self.constructCardsViewModel(response.data);
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverDown = true;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      self.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.getCards', {}, fetchCardsCompletionCallback));
    };
  	
  	/**
     * lockCard - Issues a command to lock the given card and then presents the user interface.
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Card object, action name (To show appropriate acknowledgement screen after service call.)
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.lockCard = function(card, action){
      var self = this;
      function lockCardCompletionCallback(response){
      var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  cardsViewModel.card = card;
          cardsViewModel.actionAcknowledgement = action;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.lockCard', {cardId: card.cardId}, lockCardCompletionCallback));
    };
  
  	/**
     * unlockCard - Issues a command to unlock the given card and then presents the user interface.
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Card object, action name (To show appropriate acknowledgement screen after service call.)
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.unlockCard = function(card, action){
      var self = this;
      function unlockCardCompletionCallback(response){
      var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  cardsViewModel.card = card;
          cardsViewModel.actionAcknowledgement = action;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.unlockCard', {cardId: card.cardId}, unlockCardCompletionCallback));
    };
  
  	/**
     * changePin - Issues a command to Change the PIN for given card and then presents the user interface.
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Params object, action name (To show appropriate acknowledgement screen after service call.)
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.changePin = function(params, action){
      var self = this;
      function changePinCompletionCallback(response){
      var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  cardsViewModel.card = params.card;
          cardsViewModel.actionAcknowledgement = action;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.changePin', {cardId: params.card.cardId, Reason: params.reason, notes: params.notes}, changePinCompletionCallback));
    };
  
  	/**
     * reportLost - Issues a command to report a lost card and then presents the user interface.
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Params object, action name (To show appropriate acknowledgement screen after service call.)
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.reportLost = function(params, action){
      var self = this;
      function changePinCompletionCallback(response){
      var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  cardsViewModel.card = params.card;
          cardsViewModel.actionAcknowledgement = action;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.reportLost', {cardId: params.card.cardId, Reason: params.Reason, notes: params.notes}, changePinCompletionCallback));
    };
  	
  	/**
     * sendSecureAccessCode - Issues a command to send Secure Access Code and then presents the user interface
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Params object, action name (Based on which UI will be presented.)
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.sendSecureAccessCode = function(params, action){
      var self = this;
      function sendSecureAccessCodeCompletionCallback(response){
        var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          cardsViewModel.params = params;
          cardsViewModel.action = action;
          cardsViewModel.secureAccessCode = true;
          cardsViewModel.progressBar = false;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.progressBar = false;
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.sendSecureAccessCode', {}, sendSecureAccessCodeCompletionCallback));
    };
  
  	/**
     * verifySecureAccessCode - Issues a command to verify the given secure access code and then calls the passed action.
     * @member of {CardManagement_PresentationController}
     * @param {String, Object, String} - Secure Access code, Card object, action name (To call after verifying access code)
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.verifySecureAccessCode = function(params, action){
      var self = this;
      function verifySecureAccessCodeCompletionCallback(response){
        var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          if(response.data.result === "Successful"){
          	if(action === OLBConstants.CARD_ACTION.Lock){
          	  self.lockCard(params.card, action);
            }else if(action === OLBConstants.CARD_ACTION.Change_Pin){
              self.changePin(params, action);
            }else if(action === OLBConstants.CARD_ACTION.Unlock){
          	  self.unlockCard(params.card, action);
            }else if(action === OLBConstants.CARD_ACTION.Report_Lost){
              self.reportLost(params, action);
            }else if(action===OLBConstants.CARD_ACTION.Replace || action===OLBConstants.CARD_ACTION.Offline_Change_Pin){
              self.createCardRequest(params, action);
            }
          }else if(response.data.result === "Failure" || response.data.result === "Failed"){
            cardsViewModel.card = params.card;
            cardsViewModel.action = action;
            cardsViewModel.incorrectSecureAccessCode = true;
          	self.presentUserInterface('frmCardManagement', cardsViewModel);  
          }
        }else{
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.verifySecureAccessCode', {otp: params.enteredAccessCode}, verifySecureAccessCodeCompletionCallback));
    };
  	
  	/**
     * fetchSecurityQuestions - Issues a command to fetch Security Questions and then presents the user interface
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Card object, action name (Based on which UI will be presented.)
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.fetchSecurityQuestions = function(card, action){
      var self = this;
      function fetchSecurityQuestionsCompletionCallback(response){
        var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          cardsViewModel.card = card;
          cardsViewModel.action = action;
          cardsViewModel.securityQuestions = response.data.records;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverDown = true;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.getSecurityQuestions', {userName: kony.mvc.MDAApplication.getSharedInstance().appContext.username}, fetchSecurityQuestionsCompletionCallback));
    };
  	
  	/**
     * verifySecurityQuestionAnswers - Issues a command to verify Security Questions and then presents the user interface
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Params object, action name (Which will be called after verifying security question answers)
     * @returns {VOID} 
     * @throws {}
     */
  	CardManagement_PresentationController.prototype.verifySecurityQuestionAnswers = function(params, action){
      var self = this;
      var questionAnswerParams = {};
      questionAnswerParams.userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      questionAnswerParams.securityQuestions = JSON.stringify(params.questionAnswers);
      function verifySecurityQuestionAnswersCompletionCallback(response){
        var cardsViewModel={};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          if(response.data.verifyStatus === "true"){
            if(action === OLBConstants.CARD_ACTION.Lock){
          		self.lockCard(params.card, action);
            }else if(action === OLBConstants.CARD_ACTION.Change_Pin){
              	self.changePin(params, action);
            }else if(action === OLBConstants.CARD_ACTION.Unlock){
          		self.unlockCard(params.card, action);
            }else if(action === OLBConstants.CARD_ACTION.Report_Lost){
                self.reportLost(params, action);
            }else if(action===OLBConstants.CARD_ACTION.Replace || action===OLBConstants.CARD_ACTION.Offline_Change_Pin){
              self.createCardRequest(params, action);
            }
          }else if(response.data.verifyStatus === "false"){
            cardsViewModel.card = params.card;
            cardsViewModel.action = action;
            cardsViewModel.incorrectSecurityAnswers = true;
          	self.presentUserInterface('frmCardManagement', cardsViewModel);  
          }
        }else{
          cardsViewModel.serverDown = true;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.verifySecurityQuestions', questionAnswerParams, verifySecurityQuestionAnswersCompletionCallback));
    };
	/**
     * replaceCard - Issues a command to replace card and then presents the user interface.
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Params object, action name (To show appropriate acknowledgement screen after service call.)
     * @returns {VOID} 
     * @throws {}
     */
    CardManagement_PresentationController.prototype.replaceCard = function(card, action){
      var self = this;
      function replaceCardCompletionCallback(response){
      var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  cardsViewModel.card = card;
          cardsViewModel.actionAcknowledgement = action;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.replaceCard', {cardId:card.cardId}, replaceCardCompletionCallback));
    };

    /**
     * createCardRequest - Issues a command to create card request(replace card/offline pin change)
     * @member of {CardManagement_PresentationController}
     * @param {Object, String} - Params object, action name (To show appropriate acknowledgement screen after service call.)
     * @returns 
     * @throws
     */
    CardManagement_PresentationController.prototype.createCardRequest = function(params, action) {
      var self = this;
      function createCardRequestCompletionCallback(response) {
      var cardsViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
         if(action === OLBConstants.CARD_ACTION.Offline_Change_Pin) {
              cardsViewModel.card = params.card;
              cardsViewModel.actionAcknowledgement = action;
              self.presentUserInterface('frmCardManagement', cardsViewModel);
          } else if(action===OLBConstants.CARD_ACTION.Replace) {
            self.replaceCard(params.card, action);
          }
        } else {
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.createCardRequest', params, createCardRequestCompletionCallback));
    };
    /**
     * Fetch already created Travel Notifications for user
     * @param {} 
     * @returns {VOID} 
     * @throws {}
     */
    CardManagement_PresentationController.prototype.fetchTravelNotifications = function(){
      var self = this;
      var params={
        "userName":kony.mvc.MDAApplication.getSharedInstance().appContext.username,
        "isOLB": 1
      };
      function completionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  self.presentUserInterface('frmCardManagement', {"travelNotificationsList":response.data})
        }else{          
          self.presentUserInterface('frmCardManagement', {"serverDown":"serverDown"});
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.getTravelNotifications', params, completionCallback));
    };   
    /**
     * Deletes notification
     * @param {} 
     * @returns {VOID} 
     * @throws {}
     */
    CardManagement_PresentationController.prototype.deleteNotification = function(requestID){
      var self = this;
      function completionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  self.presentUserInterface('frmCardManagement',{"notificationDeleted":"notificationDeleted"});
        }else{
          self.presentUserInterface('frmCardManagement', {"serverError":response.data.errmsg});
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.deleteTravelNotifications', {request_id:requestID}, completionCallback));
    }; 

    CardManagement_PresentationController.prototype.getEligibleCards = function(){
      var self = this;
      var cardsViewModel = {};
      function getEligibleCardsCompletionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          cardsViewModel.eligibleCards = response.data;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverDown = true;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      self.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.getActiveCards', {}, getEligibleCardsCompletionCallback));
    };

    CardManagement_PresentationController.prototype.AddNewTravelPlan = function(){
      var self = this;
      var data = {};
      data.AddNewTravelPlan = 'AddNewTravelPlan';	
      if(self.country && self.states && self.city) {
        data.country = self.country;
        data.states = self.states;
        data.city = self.city;
        self.presentUserInterface('frmCardManagement', data); 
      } else {
        function completionCallback(responseList) {
          if (responseList[0].status === kony.mvc.constants.STATUS_SUCCESS && responseList[1].status === kony.mvc.constants.STATUS_SUCCESS && responseList[2].status === kony.mvc.constants.STATUS_SUCCESS) {
              data.country = responseList[0].data.records;
              data.states =  responseList[1].data.records;
              data.city = responseList[2].data.records;
              self.presentUserInterface('frmCardManagement', data); 
          } else {
            CommonUtilities.showServerDownScreen();
          }
      }
        var commands = [new kony.mvc.Business.Command("com.kony.cardmanagement.getCountryList", {}), new kony.mvc.Business.Command("com.kony.cardmanagement.getStatesList", {}), new kony.mvc.Business.Command("com.kony.cardmanagement.getCityList", {})]
        kony.mvc.util.ParallelCommandExecuter.executeCommands(this.businessController, commands, completionCallback);
      }     
    };

    CardManagement_PresentationController.prototype.createTravelNotification=function(notificationObj){
      var self = this;
      var cardsViewModel = {};
      function createTravelNotificationCompletionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          cardsViewModel.notificationAcknowledgement = response.data;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      var notificationData = {
        channelId : OLBConstants.Channel,
        destinations : JSON.stringify(notificationObj.locations),
        username : kony.mvc.MDAApplication.getSharedInstance().appContext.username,
        additionNotes: notificationObj.notes,
        phoneNumber : notificationObj.phone,
        cards : JSON.stringify(notificationObj.selectedcards),
        StartDate : CommonUtilities.sendDateToBackend(notificationObj.fromDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat')),
        EndDate : CommonUtilities.sendDateToBackend(notificationObj.toDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat')),        
      }
      notificationData.destinations = notificationData.destinations.replace(/"/g, "'");
      notificationData.cards = notificationData.cards.replace(/"/g, "'");
      self.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.createTravelNotifications', notificationData, createTravelNotificationCompletionCallback)); 
    };

    CardManagement_PresentationController.prototype.updateTravelNotification=function(notificationObj){
      var self = this;
      var cardsViewModel = {};
      function updateTravelNotificationCompletionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          cardsViewModel.notificationAcknowledgement = response.data;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }else{
          cardsViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmCardManagement', cardsViewModel);
        }
      }
      var notificationData = {
        channelId : OLBConstants.Channel,
        request_id:notificationObj.requestId,
        destinations : JSON.stringify(notificationObj.locations),
        username : kony.mvc.MDAApplication.getSharedInstance().appContext.username,
        additionNotes: notificationObj.notes,
        phoneNumber : notificationObj.phone,
        cards : JSON.stringify(notificationObj.selectedcards),
        StartDate : CommonUtilities.sendDateToBackend(notificationObj.fromDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat')),
        EndDate : CommonUtilities.sendDateToBackend(notificationObj.toDate, CommonUtilities.getConfiguration('frontendDateFormat'), CommonUtilities.getConfiguration('backendDateFormat')),        
      }
      notificationData.destinations = notificationData.destinations.replace(/"/g, "'");
      notificationData.cards = notificationData.cards.replace(/"/g, "'");
      self.businessController.execute(new kony.mvc.Business.Command('com.kony.cardmanagement.updateTravelNotifications', notificationData, updateTravelNotificationCompletionCallback)); 
    };
    return CardManagement_PresentationController;
});
