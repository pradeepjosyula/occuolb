define([], function () {

	function Survey_getSurveyQuestions_CommandHandler(commandId) {
		kony.mvc.Business.CommandHandler.call(this, commandId);
	}

	inheritsFrom(Survey_getSurveyQuestions_CommandHandler, kony.mvc.Business.CommandHandler);

	Survey_getSurveyQuestions_CommandHandler.prototype.execute = function (command) {
		var self = this;
		var context = command.context;
		function completionCallback(status, response, error) {
			if (status === kony.mvc.constants.STATUS_SUCCESS) {
				self.sendResponse(command, status, response);
			} else {
				self.sendResponse(command, status, error);
			}
		}
		try {
			//TO DO - Replace this when service is provided
			var response = {
				"questions": [{
					questionid : 101,
					"inputType": "rating",
					"question": "Have you faced any problem today while using Kony Bank Internet Banking Site ?",
					"questionInput": ""
				},
				{
					questionid : 102,
					"inputType": "mcq",
					"question": "Where do you think we can improve our online experience?",
					"questionInput": ["Transfers", "Bill Pay","Security Questions Setting", "Messages"]
				},
				{
					questionid : 103,
					"inputType": "rating",
					"question": "Please specify how likely are you to recommend Kony Bank to friends/relatives/colleagues, on a scale of 1-5",
					"questionInput": ""
				},
				{
					questionid : 104,
					"inputType": "rating",
					"question": "Please rate your experience of using Kony Bank Internet banking today - Ease of Logging-in ",
					"questionInput": ""
				},
				{
					questionid : 105,
					"inputType": "rating",
					"question": "Please rate your experience of using Kony Bank Internet banking today – Level of security offered",
					"questionInput": ""
				},
				{
					questionid : 106,
					"inputType": "rating",
					"question": "Please rate your experience of using Kony Bank Internet banking today-  Ease of navigation/transacting",
					"questionInput": ""
				},
				{
					questionid : 107,
					"inputType": "rating",
					"question": "Please rate your experience of using Kony Bank Internet banking today – Attractiveness",
					"questionInput": ""
				},
				{
					questionid : 108,
					"inputType": "text",
					"question": "Please suggest any improvement area(s) to make your experience better with Kony Bank Internet Banking? ",
					"questionInput": ""
				}
				]
			};
			self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
		}
		catch (err) {
			self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
		}
	};

	Survey_getSurveyQuestions_CommandHandler.prototype.validate = function () {

	};

	return Survey_getSurveyQuestions_CommandHandler;

});