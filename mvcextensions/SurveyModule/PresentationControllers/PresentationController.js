define([], function () {

  function Survey_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Survey_PresentationController, kony.mvc.Presentation.BasePresenter);

  Survey_PresentationController.prototype.initializePresentationController = function () {

  };
     /**
     * presentSurvey : Method to present data to form
     * @member of {Survey_PresentationController}
     * @param {Json object} data- viewmodel to present
     * @return {}
     * @throws {}
     */  
  Survey_PresentationController.prototype.presentSurvey = function (data) {
    this.presentUserInterface('frmCustomerFeedbackSurvey', data);
  };

     /**
     * getDisplayRatingString : Method to get display rating string
     * @member of {Survey_PresentationController}
     * @param {Number} rattingValue- rating number
     * @return {String} rating string value
     * @throws {}
     */  
  var getDisplayRatingString = function (ratingValue) {
    switch (ratingValue) {
      case 1:
        return "Very Hard";
      case 2:
        return "Hard";
      case 3:
        return "Normal";
      case 4:
        return "Easy"
      case 5:
        return "Very Easy"
    }
  }
     /**
     * getSelectedCheckBoxValue : Method to get selected checkbox values 
     * @member of {Survey_PresentationController}
     * @param {Array, Array} sourceArray- all checkbox, filterArray- selected array value
     * @return {String} reslutArray
     * @throws {}
     */
  var getSelectedCheckBoxValue = function (sourceArray, filterArray) {
    var resArray = sourceArray.filter(function (val, index) {
      for (var i = 0; i < filterArray.length; i++) {
        if (filterArray[i] === index) {
          return val;
        }
      }
    });
    return resArray.toString();
  }   

  var surveyModel = {
    questions: null,
    updateQuestions: function (questions) {
      this.questions = questions;
    },
    updateAnswers: function (answers) {
      this.answers = answers;
    },
    getQuetionsWithAnswers: function () {
      var result = this.questions.map(function (question) {
        var answerString;
        if (question.inputType === "rating") {
          answerString = getDisplayRatingString(surveyModel.answers[question.questionid]);
        } else if (question.inputType === "mcq") {
          answerString = getSelectedCheckBoxValue(question.questionInput,surveyModel.answers[question.questionid])
        } else if (question.inputType === "text") {
          answerString = surveyModel.answers[question.questionid];
        }
        if(answerString === null || answerString ===  undefined || answerString.trim() === ""){
          answerString = kony.i18n.getLocalizedString("i18n.Survey.NotAnswered");
        }
        question.answerString = answerString;
        return question;
      });
      return result;
    }
  };
     /**
     * showSurvey : Method to handle load survey init
     * @member of {Survey_PresentationController}
     * @param {}
     * @return {}
     * @throws {}
     */
  Survey_PresentationController.prototype.showSurvey = function () {
    var self = this;
    if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
      this.presentSurvey({ "preLoginView": "preLoginView" });
    } else {
      this.loadSurveyComponents();
      this.presentSurvey({ "postLoginView": "postLoginView" });
    }
    //TO DO fetch Questions
    this.getSurveyQuestions();
  };

     /**
     * loadSurveyComponents : Method to load survey components
     * @member of {Survey_PresentationController}
     * @param {}
     * @return {}
     * @throws {}
     */
  Survey_PresentationController.prototype.loadSurveyComponents = function () {
    this.loadComponents('frmCustomerFeedbackSurvey');
  };

     /**
     * loadComponents : Method to handle load initial components
     * @member of {Survey_PresentationController}
     * @param {Json object} account- to account json 
     * @return {}
     * @throws {}
     */  
  Survey_PresentationController.prototype.loadComponents = function (frm, data) {
    var self = this;
    var howToShowHamburgerMenu = function (sideMenuViewModel) {
      self.showView(frm, { "sideMenu": sideMenuViewModel });
    };
    self.SideMenu.init(howToShowHamburgerMenu);

    var presentTopBar = function (topBarViewModel) {
      self.showView(frm, { "topBar": topBarViewModel });
    };
    self.TopBar.init(presentTopBar);
  };
     /**
     * showView : Method to handle present data to the form
     * @member of {Survey_PresentationController}
     * @param {String, Object} frm- form name, data- data to send form
     * @return {}
     * @throws {}
     */
  Survey_PresentationController.prototype.showView = function (frm, data) {
    this.presentUserInterface(frm, data);
  };
     /**
     * showProgressBar : Method to handle show progress bar
     * @member of {Survey_PresentationController}
     * @param {}
     * @return {}
     * @throws {}
     */
  Survey_PresentationController.prototype.showProgressBar = function () {
    var self = this;
    self.presentSurvey({ "showProgressBar": "showProgressBar" });
  };
     /**
     * hideProgressBar : Method to handle hide progress bar
     * @member of {Survey_PresentationController}
     * @param {}
     * @return {}
     * @throws {}
     */
  Survey_PresentationController.prototype.hideProgressBar = function () {
    var self = this;
    self.presentSurvey({ "hideProgressBar": "hideProgressBar" });
  };
     /**
     * getSurveyQuestions : Method to get survey questions from service and invoke UI
     * @member of {Survey_PresentationController}
     * @param {}
     * @return {}
     * @throws {}
     */
  Survey_PresentationController.prototype.getSurveyQuestions = function () {
    var self = this;
    function onCompletionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        surveyModel.updateQuestions(response.data.questions);
        self.hideProgressBar();
        self.presentSurvey({ "surveyQuestion": response }); //TO Do if no survey question
      } else {
        self.hideProgressBar();
        //self.presentSurvey({ "" : "" }); To DO - Confirm UI
      }
    }
    self.businessController.execute(
      new kony.mvc.Business.Command("com.kony.survey.getSurveyQuestions", {}, onCompletionCallback)
    );
  };
     /**
     * showSurveyAnswer : Method to create view model with answers and update the form
     * @member of {Survey_PresentationController}
     * @param {Json object} answers- selected answers
     * @return {}
     * @throws {}
     */
  Survey_PresentationController.prototype.showSurveyAnswer = function (answers) {
    surveyModel.updateAnswers(answers);
    this.presentSurvey({ "quetionsWithAnswers": surveyModel.getQuetionsWithAnswers() });
  }
     /**
     * surveyDone : Method to navigate to other module once the survey done.
     * @member of {Survey_PresentationController}
     * @param {}
     * @return {}
     * @throws {}
     */
  Survey_PresentationController.prototype.surveyDone = function () {
    var self = this;
    if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.showLoginScreen();
    } else {
      var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      accountModule.presentationController.showAccountsDashboard();
    }
  };

  return Survey_PresentationController;
});