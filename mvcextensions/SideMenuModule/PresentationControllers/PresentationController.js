define(['OLBConstants'], function (OLBConstants) {

  var viewModel = {
        isMainMenuVisible: false,
        contents: {
            statements: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showFormatEstatements();
                    //accountsModule.presentationController.showAccountDetails();
                }
            },
            myAccounts: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showAccountsDashboard();
                }
            },
            addExternalBankAccounts: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showExternalBankList();
                }
            },
            myCards: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var cardsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModule");
                    cardsModule.presentationController.navigateToManageCards();
                }
            },
            transferMoney: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    transfersModule.presentationController.showTransferScreen();
                }
            },
            transferHistory: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var transfermod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    transfermod.presentationController.showTransferScreen({
                        initialView: 'recent'
                    })
                }
            },
            externalAccounts: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var transfermod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    transfermod.presentationController.showExternalAccounts();
                    //            var navObj = new kony.mvc.Navigation("frmTransfers");
                    //           navObj.navigate();
                }
            },
            payABill: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    BillPayModule.presentationController.showBillPayData(null, {
                        show: "AllPayees"
                    });
                }
            },
            addPayee: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    if (kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility === "Activated") {
                        var addPayeeModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AddPayeeModule");
                        addPayeeModule.presentationController.navigateToAddPayee();
                    } else {
                        var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                        BillPayModule.presentationController.showBillPayData();
                    }
                }
            },
            billPayHistory: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    BillPayModule.presentationController.showBillPayData(null, {
                        show: "History"
                    });
                }
            },
            myPayeeList: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    BillPayModule.presentationController.showBillPayData(null, {
                        show: "ManagePayees"
                    });
                }
            },
            addKonyAccounts: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    trasferModule.presentationController.showSameBankAccounts();
                }
            },
            addNonKonyAccounts: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    trasferModule.presentationController.showDomesticAccounts();
                }
            },
            locateUs: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
                    locateUsModule.presentationController.showLocateUsPage();
                }
            },
            alerts: {
                onSelect: function() {
                    var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                    alertsMsgsModule.presentationController.showAlertsPage();
                }
            },
            messages: {
                onSelect: function() {
                    var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                    alertsMsgsModule.presentationController.showAlertsPage("hamburgerMenu", {
                        show: "Messages"
                    });
                }
            },
            newMesssage: {
                onSelect: function() {
                    var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                    alertsMsgsModule.presentationController.showAlertsPage("hamburgerMenu", {
                        show: "CreateNewMessage"
                    });
                }
            },
            termsAndConditions: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                    informationContentModule.presentationController.showTermsAndConditions();
                }
            },
            privacyPolicy: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                    informationContentModule.presentationController.showPrivacyPolicyPage();
                }
            },
            FAQs: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                    informationContentModule.presentationController.showFAQs();
                }
            },
            contactUs: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                    informationContentModule.presentationController.showContactUsPage();
                }
            },
            SendRequest: {
                onSelect: function() {
                    var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule"); //avinash edit 3
                    p2pModule.presentationController.showPayAPerson({
                        "show": "SendOrRequest"
                    });
                }
            },
            MyRequests: {
                onSelect: function() {
                    var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    p2pModule.presentationController.showPayAPerson({
                        "show": "P2PRequestMoneyForUser"
                    });
                }
            },
            P2pHistory: {
                onSelect: function() {
                    var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    p2pModule.presentationController.showPayAPerson({
                        "show": "SentTransactions"
                    });
                }
            },
            MyRecipients: {
                onSelect: function() {
                    var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    p2pModule.presentationController.showPayAPerson({
                        "show": "managePayeesData"
                    });
                }
            },
            AddRecipient: {
                onSelect: function() {
                    var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                    p2pModule.presentationController.showPayAPerson({
                        "show": "addRecipient"
                    });
                }
            },
            onStopPaymentRequests: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StopPaymentsModule");
                    accountsModule.presentationController.showStopPayments();
                }
            },
            AddExternalBankAccounts: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showExternalBankList();
                }
            },
            onPFMFlow: {
                onSelect: function() {
                    viewModel.closeHamburgerAction();
                    var pfmModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PersonalFinanceManagementModule");
                    pfmModule.presentationController.initPFMForm();
                }
            },
            wireTransfers: {
                makeTransfer: {
                    onSelect: function() {
                        viewModel.closeHamburgerAction();
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "makeTransfer"
                        })
                    }
                },
                wireTransferHistory: {
                    onSelect: function() {
                        viewModel.closeHamburgerAction();
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "wireTransferHistory"
                        })
                    }
                },
                myRecipients: {
                    onSelect: function() {
                        viewModel.closeHamburgerAction();
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "myRecipients"
                        })
                    }
                },
                addRecipient: {
                    onSelect: function() {
                        viewModel.closeHamburgerAction();
                        var wireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                        wireTransferModule.presentationController.showWireTransfer({
                            landingPageView: "addRecipient"
                        })
                    }
                }
            }
        },
        closeHamburgerAction: function() {
            viewModel.isMainMenuVisible = false;
            presentWith(viewModel);
        }
    };
    var presentWith = null;
    return {
        SideMenu: {
            init: function(presenter) {
                presentWith = presenter;
                viewModel.isMainMenuVisible = false;
                presentWith(viewModel);
            },
            presentSideMenu: function() {
                viewModel.isMainMenuVisible = true;
                presentWith(viewModel);
            }
        }
    };
});