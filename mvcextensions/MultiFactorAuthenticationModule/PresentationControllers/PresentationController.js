define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants){
  
    function MultiFactorAuthentication_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }

    inheritsFrom(MultiFactorAuthentication_PresentationController, kony.mvc.Presentation.BasePresenter);

    MultiFactorAuthentication_PresentationController.prototype.initializePresentationController = function() {
        
    };
	
  	MultiFactorAuthentication_PresentationController.prototype.loadMFAComponents = function () {
        this.loadComponents('frmMultiFactorAuthentication');
    };
  
    MultiFactorAuthentication_PresentationController.prototype.loadComponents = function (frm) {
        var scopeObj = this;
        var howToShowHamburgerMenu = function (sideMenuViewModel) {
            scopeObj.presentUserInterface(frm, { "sideMenu": sideMenuViewModel });
        };
        scopeObj.SideMenu.init(howToShowHamburgerMenu);
        var presentTopBar = function (topBarViewModel) {
            scopeObj.presentUserInterface(frm, { "topBar": topBarViewModel });
        };
        scopeObj.TopBar.init(presentTopBar);
    };
  
  	/**
     * cancelCallback - Dummy function. Whenever an MFA flow is started, cancelCallback will contain the actual cancel on click for that operation.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.cancelCallback = function(){};
  
  	/**
     * successCallback - Dummy function. Whenever an MFA flow is started, successCallback will contain the actual success callback for that operation.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.successCallback = function(){};
  	
  	/**
     * startSecureAccessCodeFlow - Entry point for Secure Access Code flow. This method also initiates the cancel and success callbacks.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {Object} - context which contains callbacks and breadcrumb, menu context etc.,
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.startSecureAccessCodeFlow = function(context){
      //Presenting UI to show Loading indicator first.
      var self = this;
      this.presentUserInterface('frmMultiFactorAuthentication', {progressBar: true});
      this.loadMFAComponents();
      var mfaViewModel = {};
      mfaViewModel.breadCrumbData = context.breadcrumb;
      mfaViewModel.hamburgerSelection = context.hamburgerSelection;
      mfaViewModel.termsAndConditions = context.termsAndConditions;
      this.cancelCallback = context.cancelCallback;
      this.successCallback = context.successCallback;
      function getSecureAccessCodeOptionsCompletion(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS && response.data[0] !== null && response.data[0] !== undefined){
          mfaViewModel.secureAccessCodeOptions =  {
            'isPhoneEnabled': response.data[0].isPhoneEnabled,
            'isEmailEnabled': response.data[0].isEmailEnabled
          };
       	  self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);  
        }else{
       	  CommonUtilities.showServerDownScreen();   
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.multifactorauthentication.getSecureAccessCodeOptions', {}, getSecureAccessCodeOptionsCompletion));
    };
  	
  	/**
     * sendSecureAccessCode - Invokes a command handler to send the user a secure access code and then presents the UI.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {Object} - options where the secure access code should be sent. Phone/Email or both.
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.sendSecureAccessCode = function(options){
      var self = this;
      var mfaViewModel = {};
      function sendSecureAccessCodeCompletionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  mfaViewModel.secureAccessCodeSent = true;
          self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
        }else{
          mfaViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.multifactorauthentication.generateSecureAccessCode', options, sendSecureAccessCodeCompletionCallback));
    };
  
  	/**
     * verifySecureAccessCode - Invokes a command handler to verify the secure access code entered by the user and then presents the UI.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {Object} - The secure access code entered by the user.
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.verifySecureAccessCode = function(params){
      var self = this;
      var mfaViewModel = {};
      function verifySecureAccessCodeCompletionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  if(response.data.result === "Successful"){
          	self.successCallback();
          }else if(response.data.result === "Failure" || response.data.result === "Failed"){
            mfaViewModel.incorrectSecureAccessCode = true;
          	self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);  
          }
        }else{
          mfaViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.multifactorauthentication.verifySecureAccessCode', {otp: params.enteredAccessCode}, verifySecureAccessCodeCompletionCallback));
    };

  	/**
     * startCVVFlow - Entry point for CVV flow. This method initiates the cancel and success callbacks.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {Object} - context which contains callbacks and breadcrumb, menu context etc.,
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.startCVVFlow = function(context){
      //Presenting UI to show Loading indicator first.
      this.presentUserInterface('frmMultiFactorAuthentication', {progressBar: true});
      this.loadMFAComponents();
      this.cancelCallback = context.cancelCallback;
      this.successCallback = context.successCallback;
      var mfaViewModel = {};
      mfaViewModel.breadCrumbData = context.breadcrumb;
      mfaViewModel.hamburgerSelection = context.hamburgerSelection;
      mfaViewModel.cvvCardSelection = true;
      mfaViewModel.termsAndConditions = context.termsAndConditions;
      mfaViewModel.progressBar = true;
      this.fetchCardsAndShowCVVScreen(mfaViewModel);
    };
  	
  	
  	/**
     * fetchCardsAndShowCVVScreen - This method fetches the cards to allow the user to select from.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {Object} - context which contains callbacks and breadcrumb, menu context etc.,
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.fetchCardsAndShowCVVScreen = function(mfaViewModel){
      var self = this;
      function fetchCardsCompletionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          mfaViewModel.cards = response.data;
          self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
        }else{
          CommonUtilities.showServerDownScreen();
        }
      }
      self.businessController.execute(new kony.mvc.Business.Command('com.kony.multifactorauthentication.getCards', {}, fetchCardsCompletionCallback));
    };
  	
  
  	/**
     * verifyCVV - Invokes a command handler to verify the CVV entered by the user and then presents the UI.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {Object} - CVV and cardId.
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.verifyCVV = function(params){
      var self = this;
      var mfaViewModel = {};
      function verifyCVVCompletionCallback(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
      	  if(response.data.result === "Successful"){
          	self.successCallback();
          }else if(response.data.result === "Failure" || response.data.result === "Failed"){
            mfaViewModel.incorrectCVV = true;
          	self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);  
          }
        }else{
          mfaViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.multifactorauthentication.verifyCVV', params, verifyCVVCompletionCallback));
    };

  	/**
     * startSecurityQuestionsFlow - Entry point for Security Questions flow. This method also initiates the cancel and success callbacks.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {Object} - context which contains callbacks and breadcrumb, menu context etc.,
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.startSecurityQuestionsFlow = function(context){
      var self = this;
      //Presenting UI to show Loading indicator first.
      this.presentUserInterface('frmMultiFactorAuthentication', {progressBar: true});
      if (CommonUtilities.getConfiguration('isSecurityQuestionConfigured') === "true") {
        this.loadMFAComponents();
        var mfaViewModel = {};
        mfaViewModel.breadCrumbData = context.breadcrumb;
        mfaViewModel.hamburgerSelection = context.hamburgerSelection;
        mfaViewModel.termsAndConditions = context.termsAndConditions;
        self.cancelCallback = context.cancelCallback;
        self.successCallback = context.successCallback;
        self.fetchSecurityQuestionsAndPresentUI(mfaViewModel);
      } else{
        self.startSecureAccessCodeFlow(context);
      }  
    };
  	
  	/**
     * fetchSecurityQuestionsAndPresentUI - This method fetches the security questions and presents the UI.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {} - None
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.fetchSecurityQuestionsAndPresentUI = function(mfaViewModel){
  	  var self = this;
      function fetchSecurityQuestionsCompletion(response){
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          mfaViewModel.securityQuestions = response.data.records;
          self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
        }else{
          mfaViewModel.serverError = response.data.errmsg;
          self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
        }
      }
      this.businessController.execute(new kony.mvc.Business.Command('com.kony.multifactorauthentication.getSecurityQuestions', {userName: kony.mvc.MDAApplication.getSharedInstance().appContext.username}, fetchSecurityQuestionsCompletion));
    };
  	
  	/**
     * verifySecurityQuestionAnswers - Invokes a command handler to verify the security answers entered by the user and then presents the UI.
     * @member of {MultiFactorAuthentication_PresentationController}
     * @param {Object} - The security answers entered by the user.
     * @returns {VOID}
     * @throws {}
     */
  	MultiFactorAuthentication_PresentationController.prototype.verifySecurityQuestionAnswers = function(questionAnswers){
      var self = this;
      var questionAnswerParams = {};
      questionAnswerParams.userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      questionAnswerParams.securityQuestions = JSON.stringify(questionAnswers);
      function verifySecurityQuestionAnswersCompletion(response){
        var mfaViewModel = {};
        if(response.status === kony.mvc.constants.STATUS_SUCCESS){
          if(response.data.verifyStatus === "true"){
            self.successCallback();
          }else if(response.data.verifyStatus === "false"){
            mfaViewModel.incorrectSecurityAnswers = true;
            self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
          }
        }else{
			mfaViewModel.serverError = response.data.errmsg;
          	self.presentUserInterface('frmMultiFactorAuthentication', mfaViewModel);
        }
      }
	  self.businessController.execute(new kony.mvc.Business.Command('com.kony.multifactorauthentication.verifySecurityQuestions', questionAnswerParams, verifySecurityQuestionAnswersCompletion));
    };
  	
    return MultiFactorAuthentication_PresentationController;
});