define([], function() {

  	function MultiFactorAuthentication_getSecureAccessCodeOptions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(MultiFactorAuthentication_getSecureAccessCodeOptions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	MultiFactorAuthentication_getSecureAccessCodeOptions_CommandHandler.prototype.execute = function(command){
		  var self = this;
      function completionCallBack(status, response, err) {
        self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? response :err);
      }
      var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
      userModel.customVerb("checkSecureAccessCode", {}, completionCallBack);
    };
	
	MultiFactorAuthentication_getSecureAccessCodeOptions_CommandHandler.prototype.validate = function(){
		
    };
    
    return MultiFactorAuthentication_getSecureAccessCodeOptions_CommandHandler;
    
});