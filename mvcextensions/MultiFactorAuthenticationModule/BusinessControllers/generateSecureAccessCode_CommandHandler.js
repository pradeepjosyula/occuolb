define([], function() {

  	function MultiFactorAuthentication_generateSecureAccessCode_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(MultiFactorAuthentication_generateSecureAccessCode_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	MultiFactorAuthentication_generateSecureAccessCode_CommandHandler.prototype.execute = function(command){
		var self = this;
        function completionCallBack(status, response, err) {
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS? response: err);
        }
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.customVerb("requestOTP", {}, completionCallBack);
    };
	
	MultiFactorAuthentication_generateSecureAccessCode_CommandHandler.prototype.validate = function(){
		
    };
    
    return MultiFactorAuthentication_generateSecureAccessCode_CommandHandler;
    
});