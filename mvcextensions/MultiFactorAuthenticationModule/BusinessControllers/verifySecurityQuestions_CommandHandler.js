define([], function() {

  	function MultiFactorAuthentication_verifySecurityQuestions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(MultiFactorAuthentication_verifySecurityQuestions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	MultiFactorAuthentication_verifySecurityQuestions_CommandHandler.prototype.execute = function(command){
		var self = this;
      	function onCompletion(status, data, error){
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data: error);
        }
      	var securityQuestionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition('SecurityQuestions');
      	securityQuestionsModel.customVerb('verifyCustomerSecurityQuestions', command.context, onCompletion);
    };
	
	MultiFactorAuthentication_verifySecurityQuestions_CommandHandler.prototype.validate = function(){
		
    };
    
    return MultiFactorAuthentication_verifySecurityQuestions_CommandHandler;
    
});