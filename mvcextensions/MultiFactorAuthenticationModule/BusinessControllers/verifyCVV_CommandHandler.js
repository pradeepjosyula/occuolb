define([], function() {

  	function MultiFactorAuthentication_verifyCVV_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(MultiFactorAuthentication_verifyCVV_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	MultiFactorAuthentication_verifyCVV_CommandHandler.prototype.execute = function(command){
		var self = this;
        function completionCallBack(status, response, error) {
          if (status == kony.mvc.constants.STATUS_SUCCESS) {
            self.sendResponse(command, status, response);
          } else {
            self.sendResponse(command, status, error);
          }
        }
        var params = {
          cvv:command.context.cvv,
          cardId:command.context.cardId
        };
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.customVerb("verifyCVV",params, completionCallBack);
        };
	
	MultiFactorAuthentication_verifyCVV_CommandHandler.prototype.validate = function(){
		
    };
    
    return MultiFactorAuthentication_verifyCVV_CommandHandler;
    
});