define([], function() {

  	function MultiFactorAuthentication_getCards_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(MultiFactorAuthentication_getCards_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	MultiFactorAuthentication_getCards_CommandHandler.prototype.execute = function(command){
		  var self = this;
        function  completionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        var cardsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Cards");
        cardsModel.getAll(completionCallback);
    };
	
	MultiFactorAuthentication_getCards_CommandHandler.prototype.validate = function(){
		
    };
    
    return MultiFactorAuthentication_getCards_CommandHandler;
    
});