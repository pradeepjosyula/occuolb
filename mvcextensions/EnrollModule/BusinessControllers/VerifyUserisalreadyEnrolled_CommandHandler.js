define([], function() {

    function Enroll_VerifyUserisalreadyEnrolled_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Enroll_VerifyUserisalreadyEnrolled_CommandHandler, kony.mvc.Business.CommandHandler);

    Enroll_VerifyUserisalreadyEnrolled_CommandHandler.prototype.execute = function(command) {
       var self=this;
      function completionCallBack(status, response, error) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, status, response);
      } else {
        self.sendResponse(command, status, error);
      }
    }
       var params = {
            userlastname:command.context.userlastname,
            dateOfBirth:command.context.dateOfBirth,
            ssn:command.context.ssn,
        };
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
         userModel.customVerb("checkUserEnrolled",params, completionCallBack);

  };
  Enroll_VerifyUserisalreadyEnrolled_CommandHandler.prototype.validate = function(command) {
            };

    return Enroll_VerifyUserisalreadyEnrolled_CommandHandler;

});