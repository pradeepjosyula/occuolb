define([], function() {

    function Enroll_createNewUser_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Enroll_createNewUser_CommandHandler, kony.mvc.Business.CommandHandler);

    Enroll_createNewUser_CommandHandler.prototype.execute = function(command) {
     var self=this;
    function completionCallBack(status,response,err){
      if(status==kony.mvc.constants.STATUS_SUCCESS){
        self.sendResponse(command,status,response);
      }
      else {
        self.sendResponse(command,status,response);
      }
    }
    var params = {
      ssn:command.context.ssn,
      userlastname:command.context.userlastname,
      dateOfBirth:command.context.dateOfBirth,
      password:command.context.password,
      userName:command.context.userName
      
    };
    var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
    userModel.customVerb("createOLBUser",params,completionCallBack);
    };

    Enroll_createNewUser_CommandHandler.prototype.validate = function() {

    };

    return Enroll_createNewUser_CommandHandler;

});