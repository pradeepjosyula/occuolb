define([], function() {

  	function Enroll_SaveUserSecurityAnswers_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Enroll_SaveUserSecurityAnswers_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Enroll_SaveUserSecurityAnswers_CommandHandler.prototype.execute = function(command){
		var self=this;
        function completionCallback(status,response,error){
            if(status==kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,response);
            }else{
              self.sendResponse(command,status,error);
            }
        }
      
        try{
          var securityQuestionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SecurityQuestions");                
          securityQuestionsModel.customVerb("createCustomerSecurityQuestions",command.context,completionCallback);
       } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
	
	  Enroll_SaveUserSecurityAnswers_CommandHandler.prototype.validate = function(){
		
    };
    
    return Enroll_SaveUserSecurityAnswers_CommandHandler;
    
});