define([], function() {

  	function Enroll_GetSecurityQuestions_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Enroll_GetSecurityQuestions_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Enroll_GetSecurityQuestions_CommandHandler.prototype.execute = function(command){
		   var  self  =  this;

        /**
         * Call back function for getAll service.
         * @params  : status message, data, error message.
         */
        function  getAllCompletionCallback(status,  data,  error) {
          self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }

        try  {
          var  questionModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SecurityQuestions");
           questionModel.customVerb("getSecurityQuestions",{},getAllCompletionCallback);
        }  catch  (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
	
	  Enroll_GetSecurityQuestions_CommandHandler.prototype.validate = function(){
		
    };
    
    return Enroll_GetSecurityQuestions_CommandHandler;
    
});