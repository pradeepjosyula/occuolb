define([], function() {

    function Enroll_GetCardListForEnrollment_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Enroll_GetCardListForEnrollment_CommandHandler, kony.mvc.Business.CommandHandler);

    Enroll_GetCardListForEnrollment_CommandHandler.prototype.execute = function(command) {
         var self=this;
      function completionCallBack(status, response, error) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, status, response);
      } else {
        self.sendResponse(command, status, error);
      }
    }
     
       var params = {
            userlastname:command.context.userlastname,
            dateOfBirth:command.context.dateOfBirth,
            ssn:command.context.ssn
        };
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Cards");
         userModel.customVerb("getCardListForEnrolment",params, completionCallBack);

  };


    Enroll_GetCardListForEnrollment_CommandHandler.prototype.validate = function() {

    };

    return Enroll_GetCardListForEnrollment_CommandHandler;

});