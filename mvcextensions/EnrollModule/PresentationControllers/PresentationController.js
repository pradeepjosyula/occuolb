define(['CommonUtilities'], function (CommonUtilities) {
    var cardsJSON = [];
    var userDetailsJSON = [];
    var userId = "";
    var userName="";

    function Enroll_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }

    inheritsFrom(Enroll_PresentationController, kony.mvc.Presentation.BasePresenter);

    Enroll_PresentationController.prototype.initializePresentationController = function () {

    };
    /**
     * showEnrollPage :This function is called to render UI basing on data
     * @member of {Enroll_PresentationController}
     * @param {data} depending on the context the appropriate function is executed
     * @return {} 
     * @throws {}
     */
    Enroll_PresentationController.prototype.showEnrollPage = function (data) {
        this.presentUserInterface("frmEnrollNow", data);
    };
    /**
   * verifyUser :This function verifies the user basing on the details provided
   * @member of {Enroll_PresentationController}
   * @param {detailsJSON} -user details
   * @return {} 
   * @throws {}
   */
    Enroll_PresentationController.prototype.verifyUser = function (detailsJSON) {
        userDetailsJSON = detailsJSON;
        var self = this;
        var context = {};
        function verifyUserCompletionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS){
                response.userDetails = detailsJSON;
                context = {
                "action": "Verify User Success",
                "data": response
            };
            self.showEnrollPage(context);
            }
            if (response.status === kony.mvc.constants.STATUS_FAILURE){
                context = {
                    "action": "Verify User failure",
                    "data": response
                };
            self.showEnrollPage(context);
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.enroll.VerifyUserisalreadyEnrolled", detailsJSON, verifyUserCompletionCallback.bind(this)));

    };
    /**
     * goToPasswordResetOptionsPage :This function is used to navigate to password reset page 
     * @member of {Enroll_PresentationController}
     * @param {detailsJSON} user details
     * @return {} 
     * @throws {}
     */
    Enroll_PresentationController.prototype.goToPasswordResetOptionsPage = function (detailsJSON) {
        var self = this;
        var context = {};
        self.showEnrollPage(context);
        function resetPasswordCompletionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.getCards(response);
                context = {
                    "action": "Password Reset Success",
                    "data": cardsJSON
                };
                self.showEnrollPage(context);
            }
            if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                context = {
                    "action": "Password Reset Failure",
                    "data": response
                };
                self.showEnrollPage(context);
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.enroll.getCardListForEnrollment", detailsJSON, resetPasswordCompletionCallback));

    };
    /**
     * getCards :Function to get the CardsJSON and format the CardNumber and unmask it and store in the new JSON
     * @member of {Enroll_PresentationController}
     * @param {response} - list of cards and its details
     * @return {} 
     * @throws {}
     */
    Enroll_PresentationController.prototype.getCards = function (response) {
        var cardNumberJSON = response.data;
        cardsJSON = [];
        if (cardNumberJSON.length !== 0) {
            for (var index in cardNumberJSON) {
                var cardNumber = cardNumberJSON[index]["cardNumber"];
                var maskedcardNumber = this.maskCreditCardNumber(cardNumber);
                var tmpIndex = cardsJSON.length;
                cardsJSON[tmpIndex] = {};
                cardsJSON[tmpIndex][cardNumber] = maskedcardNumber;

            }
        }
    };


    /**
      * maskCreditCardNumber :Function to mask the cardnumber 
      * @member of {Enroll_PresentationController}
      * @param {cardNumber} card unique number
      * @return {} 
      * @throws {}
      */
    Enroll_PresentationController.prototype.maskCreditCardNumber = function (cardNumber) {
        var maskedCreditNumber;
        var firstfour = cardNumber.substring(0, 4);
        var lastfour = cardNumber.substring(cardNumber.length - 4, cardNumber.length);
        maskedCreditNumber = firstfour + "XXXXXXXX" + lastfour;
        return maskedCreditNumber;
    };
    /**
  * cvvValidate : function to validate CVV
  * @member of {Enroll_PresentationController}
  * @param {cvv} - cvv of the card 
  * @param {maskedCardNumber} - card number in masked format
  * @return {} 
  * @throws {}
  */

    Enroll_PresentationController.prototype.cvvValidate = function (maskedCardNumber, cvv) {
        var self = this;
        var context = {};
        var cvvJSON = userDetailsJSON;
        cvvJSON.cvv = cvv;
        var unmaskedCardNumber = self.getUnMaskedCardNumber(maskedCardNumber);
        cvvJSON.cardNumber = unmaskedCardNumber;
        function completionCVVValidateCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                context = {
                    "action": "CVV Validate Success",
                    "data": response
                };
                self.showEnrollPage(context);
            }
            else {
                context = {
                    "action": "CVV Validate Failure",
                    "data": response
                };
                self.showEnrollPage(context);
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.verifyCVV", cvvJSON, completionCVVValidateCallback));
    };
    /**
      * getUnMaskedCardNumber : function to get the unmasked card number from the CardsJSON
      * @member of {Enroll_PresentationController}
      * @param {maskedCardNumber}card number in masked format
      * @return {} 
      * @throws {}
      */
    Enroll_PresentationController.prototype.getUnMaskedCardNumber = function (maskedCardNumber) {
        for (var key in cardsJSON) {
            if (cardsJSON.hasOwnProperty(key)) {
                var val = cardsJSON[key];
                if (CommonUtilities.substituteforIncludeMethod(JSON.stringify(val),maskedCardNumber)) {
                    var pos = JSON.stringify(val).indexOf(':', 1);
                    return JSON.stringify(val).substring(2, pos - 1);
                }
            }
        }
        return null;

    };

    /**
    * otpValidate : Function to validate OTP
    * @member of {Enroll_PresentationController}
    * @param {otp} otp
    * @return {} 
    * @throws {}
    */
    Enroll_PresentationController.prototype.otpValidate = function (otp) {
        var otpJSON = userDetailsJSON;
        var context = {};
        var self = this;
        otpJSON.otp = otp;
        function completionOTPValidateCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                context = {
                    "action": "OTP Validate Success",
                    "data": response
                };
                self.showEnrollPage(context);
            }
            else {
                context = {
                    "action": "OTP Validate Failure",
                    "data": response
                };
                self.showEnrollPage(context);
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.validateOTP", otpJSON, completionOTPValidateCallback));
    };
    /**
   * requestOTP :Function to request OTP
   * @member of {Enroll_PresentationController}
   * @param {}
   * @return {} 
   * @throws {}
   */
    Enroll_PresentationController.prototype.requestOTP = function () {
        var context = {};
        var self = this;
        function completionRequestOTPCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                context = {
                    "action": "OTP Response Success",
                    "data": response
                };
                self.showEnrollPage(context);
            }
            else {
                context = {
                    "action": "OTP Response Failure",
                    "data": response
                };
                self.showEnrollPage(context);
            }

        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.requestOTP", {}, completionRequestOTPCallback));

    };
    /**
     * resendOTP :This function is called on click of resend otp button
     * @member of {Enroll_PresentationController}
     * @param {}
     * @return {} 
     * @throws {}
     */
    Enroll_PresentationController.prototype.resendOTP = function () {
        var context = {};
        var self = this;
        function completionRequestOTPCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                context = {
                    "action": "OTP Resend Success",
                    "data": response
                };
                self.showEnrollPage(context);
            }
            else {
                context = {
                    "action": "OTP Resend Failure",
                    "data": response
                };
                self.showEnrollPage(context);
            }

        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.requestOTP", {}, completionRequestOTPCallback));

    };

    /**
     * createUser :Function that creates a new user
     * @member of {Enroll_PresentationController}
     * @param {userName} username provided
     * @param {password} password provided
     * @return {} 
     * @throws {}
     */
    Enroll_PresentationController.prototype.createUser = function (uName, password) {
        var self = this;
        var context = {};
        var userDetails = [];
        userDetails = userDetailsJSON;
        userDetails.userName = uName;
        userDetails.password = password;
        function completionResetPasswordCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                userId = response.data.userId;
                userName=uName;
                context = {
                    "action": "Create User Success",
                    "data": response
                };
                self.showEnrollPage(context);
            }
            else {
                context = {
                    "action": "Create User Failure",
                    "data": response
                };
                self.showEnrollPage(context);
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.enroll.createNewUser", userDetails, completionResetPasswordCallback));
    };
    /**
       * fetchSecurityQuestions : Function to fetch securityQuestions
       * @member of {Enroll_PresentationController}
       * @param {context} context
       * @return {} 
       * @throws {}
       */
    Enroll_PresentationController.prototype.fetchSecurityQuestions = function (context) {
        var self = this;
        var viewModel = {};

        function getSecurityQuestionsCompletionCallback(response) {
            var i = 0;
            while (i < response.data.records.length) {
                context.securityQuestions[i] = response.data.records[i].SecurityQuestion;
                context.flagToManipulate[i] = "false";
                i++;

            }
            viewModel = {
                "action": "Fetch Questions",
                "data": { "context": context, "response": response.data.records }
            };
            self.showEnrollPage(viewModel);
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.enroll.getSecurityQuestions", {}, getSecurityQuestionsCompletionCallback));

    };
    /**
      * saveSecurityQuestions : function to save security questions and given answers
      * @member of {Enroll_PresentationController}
      * @param {data} questions and answers JSON
      * @return {} 
      * @throws {}
      */
    Enroll_PresentationController.prototype.saveSecurityQuestions = function (data) {
        var scopeObj = this;
        var context = {};
        var securityQuestionsData = null;
        var securityQuestionsCallback = function (commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                context = {
                    "action": "Save Questions Success",
                    "data": commandResponse
                };
                scopeObj.showEnrollPage(context);
            } else {
                context = {
                    "action": "Save Questions Failure",
                    "data": commandResponse
                };
                scopeObj.showEnrollPage(context);
            }
        };

        data = JSON.stringify(data);
        data = data.replace(/"/g , "'");
         securityQuestionsData = {
            userName:userName,
            securityQuestions: data
        };

        scopeObj.businessController.execute(
            new kony.mvc.Business.Command("com.kony.enroll.saveUserSecurityAnswers", securityQuestionsData, securityQuestionsCallback)
        );
    };
    Enroll_PresentationController.prototype.getUserNamePolicies = function(){
      var self=this;
    function completionCallBack(response){
       if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
         kony.mvc.MDAApplication.getSharedInstance().appContext.usernamepasswordrules=response.data.records;
       }
      else{
        self.navigateToServerDownScreen();
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.enroll.getPasswordPolicies", {}, completionCallBack));
  };
    /**
   * navigateToServerDownScreen :Function to navigate to server down screen
   * @member of {Enroll_PresentationController}
   * @param {}
   * @return {} 
   * @throws {}
   */
    Enroll_PresentationController.prototype.navigateToServerDownScreen = function () {
        var context = {
            "action": "ServerDown"
        };
        this.presentUserInterface("frmEnrollNow", context);
    };
    return Enroll_PresentationController;
});
