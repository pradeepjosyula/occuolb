define([], function () {

  function WireTransfer_createWireTransfer_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(WireTransfer_createWireTransfer_CommandHandler, kony.mvc.Business.CommandHandler);

  WireTransfer_createWireTransfer_CommandHandler.prototype.execute = function (command) {
    var self = this;

    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }
    try {
      var transactionsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      var newTransaction = new transactionsModel({
        "amount": command.context.amount,
        "payeeCurrency": command.context.payeeCurrency,
        "payeeId": command.context.payeeId,
        "transactionsNotes": command.context.transactionsNotes,
        "fromAccountNumber": command.context.fromAccountNumber,
        "fee":command.context.fee,
        "transactionType": 'Wire',
      });
      newTransaction.save(completionCallBack);
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  WireTransfer_createWireTransfer_CommandHandler.prototype.validate = function () {

  };

  return WireTransfer_createWireTransfer_CommandHandler;

});