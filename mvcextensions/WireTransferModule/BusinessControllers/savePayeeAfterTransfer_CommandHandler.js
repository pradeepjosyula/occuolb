define([], function () {

    function WireTransfer_savePayeeAfterTransfer_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(WireTransfer_savePayeeAfterTransfer_CommandHandler, kony.mvc.Business.CommandHandler);

    WireTransfer_savePayeeAfterTransfer_CommandHandler.prototype.execute = function (command) {
        var self = this;
        var context = command.context;
        var params = {
            "transactionId": context.transactionId,

        }

        function completionCallBack(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }



        try {
            var payeeModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
            payeeModel.customVerb("saveRecipientAfterWireTransfer", params, completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };

    WireTransfer_savePayeeAfterTransfer_CommandHandler.prototype.validate = function () {

    };

    return WireTransfer_savePayeeAfterTransfer_CommandHandler;

});