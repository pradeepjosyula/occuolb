define([], function () {

  function WireTransfer_getWireRecipient_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(WireTransfer_getWireRecipient_CommandHandler, kony.mvc.Business.CommandHandler);

  WireTransfer_getWireRecipient_CommandHandler.prototype.execute = function (command) {
    var self = this;

    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }
    try {
      var  PayeeModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
      PayeeModel.customVerb('getWireTransferRecipient', {
          'offset': command.context.offset,
          'sortBy': command.context.sortBy,
          'limit': command.context.limit,
          'order': command.context.order,
          'searchString': command.context.searchString
        },
        completionCallBack)
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  WireTransfer_getWireRecipient_CommandHandler.prototype.validate = function () {

  };

  return WireTransfer_getWireRecipient_CommandHandler;

});