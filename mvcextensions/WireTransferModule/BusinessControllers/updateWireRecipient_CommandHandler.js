define([], function () {

  function WireTransfer_updateWireRecipient_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(WireTransfer_updateWireRecipient_CommandHandler, kony.mvc.Business.CommandHandler);

  WireTransfer_updateWireRecipient_CommandHandler.prototype.execute = function (command) {
    var self = this;
    var context = command.context;
    var updateRecepientData = {
      "payeeName": context.payeeName,
      "zipCode": context.zipCode,
      "cityName": context.cityName,
      "state": context.state,
      "addressLine1": context.addressLine1,
      "addressLine2": context.addressLine2,
      "type": context.type,
      "country": context.country,
      "payeeId": context.payeeId,
    };

    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }
    try {
      var  PayeeModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
      PayeeModel.customVerb('updateRecipient', updateRecepientData, completionCallBack)
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  WireTransfer_updateWireRecipient_CommandHandler.prototype.validate = function () {

  };

  return WireTransfer_updateWireRecipient_CommandHandler;

});