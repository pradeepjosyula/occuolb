define([], function () {

    function WireTransfer_saveRecipient_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(WireTransfer_saveRecipient_CommandHandler, kony.mvc.Business.CommandHandler);

    WireTransfer_saveRecipient_CommandHandler.prototype.execute = function (command) {
        var self = this;
        var context = command.context;
        var params = {
            "payeeNickName": context.payeeNickName,
            "payeeAccountNumber": context.payeeAccountNumber,
            "payeeName": context.payeeName,
            "zipCode": context.zipCode,
            "cityName": context.cityName,
            "state": context.state,
            "addressLine1": context.addressLine1,
            "addressLine2": context.addressLine2,
            "type": context.type,
            "country": context.country,
            "swiftCode": context.swiftCode,
            "routingCode": context.routingCode,
            "bankName": context.bankName,
            "bankAddressLine1": context.bankAddressLine1,
            "bankAddressLine2": context.bankAddressLine2,
            "bankCity": context.bankCity,
            "bankState": context.bankState,
            "bankZip": context.bankZip,
            "IBAN": context.IBAN,
            "wireAccountType": context.wireAccountType,
            "internationalRoutingCode": context.internationalRoutingCode
        }

        function completionCallBack(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }



        try {
            var payeeModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Payee");
            payeeModel.customVerb("addRecipient", params, completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };

    WireTransfer_saveRecipient_CommandHandler.prototype.validate = function () {

    };

    return WireTransfer_saveRecipient_CommandHandler;

});