define([], function () {

  function handleDateFormat(date) {
    if (date == undefined || date == null || date === '') {
      return null;
    }
    if (date instanceof Date) {
      return date.getFullYear() + '-' + ((date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    } else {
      var dateObj = new Date(date);
      return dateObj.getFullYear() + '-' + ((dateObj.getMonth() + 1) < 10 ? '0' + (dateObj.getMonth() + 1) : dateObj.getMonth() + 1) + '-' + (dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate());
    }
  }


  function WireTransfer_createOneTimeWireTransfer_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(WireTransfer_createOneTimeWireTransfer_CommandHandler, kony.mvc.Business.CommandHandler);

  WireTransfer_createOneTimeWireTransfer_CommandHandler.prototype.execute = function (command) {
    var self = this;

    function onCreateTransfer(status, data, error) {
      self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
    }
    var context = command.context;
    var transactionsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var newTransaction = new transactionsModel({
      'wireAccountType': context.wireAccountType,
      'payeeName': context.payeeName,
      'payeeAddressLine1': context.payeeAddressLine1,
      'payeeAddressLine1': context.payeeAddressLine1,
      'cityName': context.cityName,
      'state': context.state,
      'zipCode': context.zipCode,
      'payeeType': context.payeeType,
      'swiftCode': context.swiftCode,
      'routingNumber': context.routingNumber,
      'IBAN': context.IBAN,
      'internationalRoutingCode': context.internationalRoutingCode,
      'payeeAccountNumber': context.payeeAccountNumber,
      // 'reasonForTransfer': context.reasonForTransfer,
      'payeeNickName': context.payeeNickName,
      'bankName': context.bankName,
      'bankAddressLine1': context.bankAddressLine1,
      'bankAddressLine2': context.bankAddressLine2,
      'bankCity': context.bankCity,
      'bankState': context.bankState,
      'bankZip': context.bankZip,
      'fromAccountNumber': context.fromAccountNumber,
      'payeeCurrency': context.payeeCurrency,
      'country': context.country,
      'amount': context.amount,
      'transactionType': 'Wire',
      'transactionsNotes': context.reasonForTransfer,
      'fee':context.fee,

    });
    newTransaction.save(onCreateTransfer);
  };

  WireTransfer_createOneTimeWireTransfer_CommandHandler.prototype.validate = function () {

  };

  return WireTransfer_createOneTimeWireTransfer_CommandHandler;

});