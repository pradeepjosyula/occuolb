define([], function () {

    function WireTransfer_getCountries_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(WireTransfer_getCountries_CommandHandler, kony.mvc.Business.CommandHandler);

    WireTransfer_getCountries_CommandHandler.prototype.execute = function (command) {
         var self = this;

        function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var CountryModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Country");
            CountryModel.getAll(completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };


    WireTransfer_getCountries_CommandHandler.prototype.validate = function () {

    };

    return WireTransfer_getCountries_CommandHandler;

});