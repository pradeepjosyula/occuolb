define([], function () {

  function WireTransfer_activateWireTransfer_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(WireTransfer_activateWireTransfer_CommandHandler, kony.mvc.Business.CommandHandler);

  WireTransfer_activateWireTransfer_CommandHandler.prototype.execute = function (command) {
    var self = this;
    var params = {
      "default_account_wire": command.context
    };

    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }
    try {
      var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
      var userObject = new userModel(params);
      userObject.partialUpdate(completionCallBack);
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  WireTransfer_activateWireTransfer_CommandHandler.prototype.validate = function () {

  };

  return WireTransfer_activateWireTransfer_CommandHandler;

});