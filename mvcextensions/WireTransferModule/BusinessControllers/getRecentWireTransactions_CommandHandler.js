define([], function () {

  function WireTransfer_getRecentWireTransactions_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(WireTransfer_getRecentWireTransactions_CommandHandler, kony.mvc.Business.CommandHandler);

  WireTransfer_getRecentWireTransactions_CommandHandler.prototype.execute = function (command) {
    var self = this;

    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }
    try {

      var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      TransactionsModel.customVerb('getUserWiredTransactions', {
        'sortBy': command.context.sortBy,
        'order': command.context.order,
        'lastRecordNumber': command.context.limit,
        'firstRecordNumber': command.context.offset
      }, completionCallBack)
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  WireTransfer_getRecentWireTransactions_CommandHandler.prototype.validate = function () {

  };

  return WireTransfer_getRecentWireTransactions_CommandHandler;

});