define([], function () {

  function WireTransfer_getRecipientWireTransaction_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(WireTransfer_getRecipientWireTransaction_CommandHandler, kony.mvc.Business.CommandHandler);

  WireTransfer_getRecipientWireTransaction_CommandHandler.prototype.execute = function (command) {
    var self = this;
    var params=command.context;
   
    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }
    try {

      var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      TransactionsModel.customVerb('getRecipientWireTransaction', params, completionCallBack)
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  WireTransfer_getRecipientWireTransaction_CommandHandler.prototype.validate = function () {

  };

  return WireTransfer_getRecipientWireTransaction_CommandHandler;

});