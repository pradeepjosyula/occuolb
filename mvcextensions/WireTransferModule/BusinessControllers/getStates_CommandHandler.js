define([], function () {

    function WireTransfer_getStates_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(WireTransfer_getStates_CommandHandler, kony.mvc.Business.CommandHandler);

    WireTransfer_getStates_CommandHandler.prototype.execute = function (command) {
        var self = this;
        function completionCallBack(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        try {
            var stateModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("States");
            var criteria = kony.mvc.Expression.eq("countryId", command.context);
            stateModel.getByCriteria(criteria,completionCallBack);
        } catch (err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }


    };

    WireTransfer_getStates_CommandHandler.prototype.validate = function () {

    };

    return WireTransfer_getStates_CommandHandler;

});