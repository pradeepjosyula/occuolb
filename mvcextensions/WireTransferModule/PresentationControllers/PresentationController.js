define(['CommonUtilities', 'IBANUtils', 'OLBConstants'], function (CommonUtilities, IBANUtils, OLBConstants) {

  var isErrorState = false;

  function WireTransfer_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(WireTransfer_PresentationController, kony.mvc.Presentation.BasePresenter);

  /**
   * Creates view model for payment accounts
   * @member WireTransfer_PresentationController
   * @param {object} account - Object of Account Model Class 
   * @returns {JSON} -  view model for payment account
   * @throws {void} - None
   */
  var createAccountViewModel = function (account) {
    return {
      accountName: account.accountName,
      nickName: account.nickName,
      accountNumber: Number(account.accountID),
      type: account.accountType,
      availableBalance: CommonUtilities.formatCurrencyWithCommas(account.availableBalance),
      currentBalance: CommonUtilities.formatCurrencyWithCommas(account.currentBalance),

    };
  }

  var recipientSortConfig = {
    'sortBy': 'nickName',
    'defaultSortBy': 'nickName',
    'order': OLBConstants.ASCENDING_KEY,
    'defaultOrder': OLBConstants.ASCENDING_KEY,
    'offset': OLBConstants.DEFAULT_OFFSET,
    'limit': OLBConstants.PAGING_ROWS_LIMIT
  };

  var recentTransactionSortConfig = {
    'sortBy': 'transactionDate',
    'defaultSortBy': 'transactionDate',
    'order': OLBConstants.DESCENDING_KEY,
    'defaultOrder': OLBConstants.DESCENDING_KEY,
    'offset': OLBConstants.DEFAULT_OFFSET,
    'limit': OLBConstants.PAGING_ROWS_LIMIT
  }

  WireTransfer_PresentationController.prototype.initializePresentationController = function () {

  };

  var showCount = 0;

  /**
   * Fetch accounts and filter checking accounts
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */


  WireTransfer_PresentationController.prototype.fetchCheckingAccounts = function (onSuccess, onError) {
    var self = this;

    function getAccountsCompletionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        var checkingAccounts = response.data.filter(function (account) {
          return account.accountType === "Checking";
        })
        onSuccess(checkingAccounts);
        self.hideProgressBar();
      } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
        onError();
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.getAccounts", {}, getAccountsCompletionCallback));

  };


  /**
   * 
   * Entry function for Wire Transfer
   * @member WireTransfer_PresentationController
   * @param {context} - Initial COntext
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.showWireTransfer = function (context) {
    var self = this;
    showCount = 0;
    isErrorState = false;
    this.presentWireTransfer({
      resetForm: {}
    })
    if (this.checkActivation()) {
      this.showWireTransfersBasedOnContext(context);
    }
    else {
      this.showActivationForm();
    }
    this.loadWireTransferComponents();    
  };

   /**
   * 
   * Uses Initial context to show Wire Transfer View
   * @member WireTransfer_PresentationController
   * @param {context} - Initial COntext
   * @returns {void} - None
   * @throws {void} - None
   */


  WireTransfer_PresentationController.prototype.showWireTransfersBasedOnContext = function (context) {
    context = context || {};
    if (context.transactionObject) {
      this.showRepeat(context.transactionObject);
    }
    else{
      this.showLandingPage(context.landingPageView);
    }
  }

   /**
   * Show Wire Transfer Landing Page
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */

  
  WireTransfer_PresentationController.prototype.showLandingPage = function (view) {
    switch (view) {
      case "makeTransfer":
        this.fetchWireTransferAccounts("wireTransferRecipients");
        break;
      case "wireTransferHistory":
        this.fetchRecentWireTransferTransactions();
        break;
      case "myRecipients":
        this.fetchWireTransferAccounts("manageRecipients");
        break;
      case "addRecipient":
        this.navigateToAddRecipient();
        break;
      default: 
        this.fetchWireTransferAccounts();
    }
  }  

  WireTransfer_PresentationController.prototype.navigateToAddRecipient = function () {
    this.presentWireTransfer({
      addRecipient: {}
    })
  }

  /**
   * Show Activation Form
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */


  WireTransfer_PresentationController.prototype.showActivationForm = function () {
    var self = this;
    function onCheckingAccountsSuccessful (checkingAccounts) {
      self.hideProgressBar();
      self.presentWireTransfer({
        "wireTransferDeactivated": checkingAccounts
      });
    }
    self.showProgressBar();
    self.fetchCheckingAccounts(onCheckingAccountsSuccessful, self.showServerError)
  } 

   /**
   * Show Repeat
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */
  
  WireTransfer_PresentationController.prototype.showRepeat = function (transactionObject) {
    this.getCheckingAccounts();
    this.presentWireTransfer({
      repeatTransaction: transactionObject
    })
  }

  /**
   * Checks if wire transfer is activated
   * @member WireTransfer_PresentationController
   * @returns {boolean} - If wire transfer activated returns true otherwise false
   * @throws {void} - None
   */


  WireTransfer_PresentationController.prototype.checkActivation = function () {
    var wireTransferEligible = kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData.isWireTransferEligible;
    var wireTransferActivated = kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData.isWireTransferActivated;
    //Known MF issue - boolean and string both checks needed    
    return (wireTransferEligible === "true" || wireTransferEligible === true) && (wireTransferActivated === "true" || wireTransferActivated === true);
  };

   /**
   * Checks if wire transfer is repeatable
   * @member WireTransfer_PresentationController
   * @returns {boolean} - If wire transfer repeatable returns true otherwise false
   * @throws {void} - None
   */


  WireTransfer_PresentationController.prototype.isWireTransferRepeatable = function (transactionObj) {
    return transactionObj.isPayeeDeleted==="false";
  }


  WireTransfer_PresentationController.prototype.presentWireTransfer = function (data) {
    if (!isErrorState) {
      this.presentUserInterface('frmWireTransfer', data);    
    }
  };

  WireTransfer_PresentationController.prototype.loadWireTransferComponents = function () {
    this.loadComponents();
  };

  /*
   * Function to load components
   * @param : form
   */
  WireTransfer_PresentationController.prototype.loadComponents = function (data) {
    var self = this;
    var howToShowHamburgerMenu = function (sideMenuViewModel) {
      self.presentWireTransfer({
        "sideMenu": sideMenuViewModel
      });
    };
    self.SideMenu.init(howToShowHamburgerMenu);

    var presentTopBar = function (topBarViewModel) {
      self.presentWireTransfer({
        "topBar": topBarViewModel
      });
    };
    self.TopBar.init(presentTopBar);
  };

  
  WireTransfer_PresentationController.prototype.showProgressBar = function (data) {
    var self = this;
    if (showCount === 0) {
      self.presentWireTransfer({
        "showProgressBar": "showProgressBar"
      });
    }
    showCount++;

  };

  WireTransfer_PresentationController.prototype.hideProgressBar = function (data) {
    var self = this;
    if (showCount > 0) {
      showCount--;
    }
    if (showCount === 0) {
      self.presentWireTransfer({
        "hideProgressBar": "hideProgressBar"
      });
    }
  };

  /**
   * Activate wire transfer and update wire transfer account
   * @member WireTransfer_PresentationController
   * @param {string} account ID
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.updateWireTransferForUser = function (default_account_wire) {
    var self = this;
    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData.default_account_wire = default_account_wire;
        kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData.isWireTransferActivated = true;
        self.showWireTransfer();
      } else {
        self.showServerError();
      }
    };
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.activateWireTransfer", default_account_wire, completionCallback));
  };
  /**
   * Creates a new Wire Transfer Recipient for a User
   * @member WireTransfer_PresentationController
   * @param {string} type type of recipient - domestic/international
   * @param {object} data full details of recipient
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.saveRecipient = function (data, type) {
    var self = this;
    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.hideProgressBar();
        self.showAddAccountAcknowledgement(data, type, commandResponse.data.payeeId);
      } else {
        self.hideProgressBar();
        self.presentWireTransfer({
          saveRecipientServerError:{
            errorMessage:  commandResponse.data.errmsg,
            type: type
          }
        })
      }
    };

    var commandParams = {
      "payeeNickName": data.recipientAccountDetails.recipientAccountNickName,
      "payeeAccountNumber": data.recipientAccountDetails.recipientAccountNumber,
      "payeeName": data.recipientDetails.recipientName,
      "zipCode": data.recipientDetails.recipientZipcode,
      "cityName": data.recipientDetails.recipientCity,
      "state": data.recipientDetails.recipientState,
      "addressLine1": data.recipientDetails.recipientAddressLine1,
      "addressLine2": data.recipientDetails.recipientAddressLine2,
      "type": data.recipientDetails.recipientType,
      "bankName": data.recipientAccountDetails.recipientBankName,
      "bankAddressLine1": data.recipientAccountDetails.recipientBankAddressLine1,
      "bankAddressLine2": data.recipientAccountDetails.recipientBankAddressLine2,
      "bankCity": data.recipientAccountDetails.recipientBankCity,
      "bankState": data.recipientAccountDetails.recipientBankState,
      "bankZip": data.recipientAccountDetails.recipientBankZipcode,
      "wireAccountType": type
    }

    if (type === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
      commandParams.routingCode = data.recipientAccountDetails.recipientAccountRoutingCode;
      commandParams.country = OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY;
    } else {
      commandParams.swiftCode = data.recipientAccountDetails.recipientAccountSwiftCode;
      commandParams.country = data.recipientDetails.recipientCountry;
      if (IBANUtils.isCountrySupportsIBAN(data.recipientDetails.recipientCountry)) {
        commandParams.IBAN = data.recipientAccountDetails.recipientAccountIBAN;
      } else {
        commandParams.internationalRoutingCode = data.recipientAccountDetails.recipientAccountIRC;
      }
    }
    this.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.saveRecipient", commandParams, completionCallback));
  };

  /**
   * Show Wire Transfer form with viemodel containing acknowldegement data
   * @member WireTransfer_PresentationController
   * @param {JSON} data - Data of recipient
   * @param {string} type - type of recipient 'domestic'/'international'
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.showAddAccountAcknowledgement = function (data, type, payeeId) {
    data.wireAccountType = type;
    data.payeeId = payeeId;
    this.presentWireTransfer({
      addAccountAcknowledgement: data
    })
  }

  /**
   * Fetch states based on the country id.
   * @member WireTransfer_PresentationController
   * @param {string} countryId Id of the country
   * @param {string} isEditFlow Id of the country
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.fetchStates = function (countryId,isEditFlow) {
    var self = this;
    countryId = countryId || OLBConstants.WireTransferConstants.DEFAULT_COUNTRY;
    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        if(isEditFlow){
          self.presentWireTransfer({
            statesForEdit: commandResponse.data
          })
        }
        else{
          self.presentWireTransfer({
            states: commandResponse.data
          })
        }
        self.hideProgressBar();
      } else {
        self.showServerError();
      }
    };
    // Progress bar for country is already there
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.getStates", countryId, completionCallback));
  };

  /**
   * Fetch countries
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.fetchCountries = function () {
    var self = this;
    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        // Will hide progress bar after fetching states
        self.presentWireTransfer({
          countries: commandResponse.data
        })
      } else {
        self.showServerError();
      }
    };
    self.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.getCountries", {}, completionCallback));
  };

  /**
   * Fetch Wire Transfer Recipient for a User
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */
  WireTransfer_PresentationController.prototype.fetchWireTransferAccounts = function (context, sortAndPaginationParams, searchText, expandTab) {
    var self = this;
    sortAndPaginationParams = sortAndPaginationParams || {
      resetSorting: true
    };
    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.getCheckingAccounts();
        if (context == "manageRecipients") {
          self.presentWireTransfer({
            "manageWireRecipients": {
              recipients: commandResponse.data,
              config: recipientSortConfig,
              expandTab: expandTab?expandTab:false,
              searchText: searchText
            }
          });
        } else {
          self.presentWireTransfer({
            "wireTransferRecipients": {
              recipients: commandResponse.data,
              config: recipientSortConfig,
              searchText: searchText
            }
          });
        }
        self.fetchCountries(); //to pre-set master data for Country listboxes
        self.hideProgressBar();
      } else {
        self.showServerError();
      }
    };
    this.showProgressBar();
    var params = {};
    if (typeof searchText === "string" && searchText.length > 0) {
      params.searchString = searchText;
    } else {
       params = CommonUtilities.Sorting.getSortConfigObject(sortAndPaginationParams, recipientSortConfig);
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.getWireRecipient", params, completionCallback));
  };

  /**
   * Fetch recent wire transfer transactions for a User
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.fetchRecentWireTransferTransactions = function (sortAndPaginationParams) {
    var self = this;

    sortAndPaginationParams = sortAndPaginationParams || {
      resetSorting: true
    }

    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.presentWireTransfer({
          "wireTransferTransactions":{
            transactions: commandResponse.data,
            config: recentTransactionSortConfig
          }
        });
        self.hideProgressBar();
      } else {
        self.showServerError();
      }
    };
    self.showProgressBar();
    var params = CommonUtilities.Sorting.getSortConfigObject(sortAndPaginationParams, recentTransactionSortConfig);
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.getRecentWireTransactions", params, completionCallback));
  };

  /**
   * Update wire transfer recipient payee
   * @member WireTransfer_PresentationController
   * @param {JSON} - recepient updated data
   * @returns {void} - None
   * @throws {void} - None
   */
  WireTransfer_PresentationController.prototype.updateRecipient = function (updateRecipientData, config) {
    var self = this;
    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.fetchWireTransferAccounts("manageRecipients",config,null,true);
      } else {
        self.showServerError();
      }
    };
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.updateWireRecipient", updateRecipientData, completionCallback));
  };

  /**
   * Create wire transfer 
   * @member WireTransfer_PresentationController
   * @param {JSON} - transaction object 
   * @returns {void} - None
   * @throws {void} - None
   */
  WireTransfer_PresentationController.prototype.createWireTransfer = function (transactionDetails) {
    this.multiFactorAuthentication(this.doWireTransfer.bind(this, transactionDetails), transactionDetails.amount, "Make Transfer");
  };
  /**
   * Create wire transfer 
   * @member WireTransfer_PresentationController
   * @param {JSON} - transaction object 
   * @returns {void} - None
   * @throws {void} - None
   */
  WireTransfer_PresentationController.prototype.doWireTransfer = function (transactionDetails) {
    var self = this;
    var completionCallback = function (commandResponse) {
      self.hideProgressBar();
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.presentWireTransfer({
          "createWireTransfer": commandResponse.data.referenceId,
        });
      } else {
        self.presentWireTransfer({
          "createWireTransferError": commandResponse.data.errmsg,
        });        
      }
    };
    self.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.createWireTransfer", transactionDetails, completionCallback));
  };

  /**
   * Get checking accounts and pass it to form
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */



  WireTransfer_PresentationController.prototype.getCheckingAccounts = function () {
    var self = this;

    function onfetchComplete(checkingAccountsModels) {
      self.hideProgressBar();
      self.presentWireTransfer({
        checkingAccounts: checkingAccountsModels
      })
      self.hideProgressBar();
    }
    self.showProgressBar();
    this.fetchCheckingAccounts(onfetchComplete, this.showServerError)
  };


  /**
   * Create wire transfer 
   * @member WireTransfer_PresentationController
   * @param {JSON} data -  transaction object 
   * @returns {void} - None
   * @throws {void} - None
   */
  WireTransfer_PresentationController.prototype.createOneTimeTransfer = function (data) {
    this.multiFactorAuthentication(this.doOneTimeWIreTransfer.bind(this, data),data.transactionDetails.amount);
  };


  /**
   * Create wire transfer 
   * @member WireTransfer_PresentationController
   * @param {JSON} data -  transaction object 
   * @returns {void} - None
   * @throws {void} - None
   */
  WireTransfer_PresentationController.prototype.doOneTimeWIreTransfer = function (data) {
    var self = this;
    var completionCallback = function (commandResponse) {
      self.hideProgressBar();
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        data.referenceNumber = commandResponse.data.referenceId;
        self.presentWireTransfer({
          oneTimeTransferAcknowledgement: data
        })
      } else {
        self.presentWireTransfer({
          oneTimeTransferServerError: commandResponse.data.errmsg
        });
      }
    };

    var params = {
      'wireAccountType': data.recipientDetails.recipientAccountType,
      'payeeName': data.recipientDetails.recipientName,
      'payeeAddressLine1': data.recipientDetails.recipientAddressLine1,
      'payeeAddressLine2': data.recipientDetails.recipientAddressLine2,
      'cityName': data.recipientDetails.recipientCity,
      'state': data.recipientDetails.recipientState,
      'zipCode': data.recipientDetails.recipientZipcode,
      'payeeType': data.recipientDetails.recipientType,
      // 'swiftCode': data.recipientAccountDetails.recipientAccountSwiftCode, 
      // 'routingNumber': data.recipientAccountDetails.recipientAccountRoutingCode,
      // 'IBAN': data.recipientAccountDetails.recipientAccountIBAN,
      // 'internationalRoutingCode': data.recipientAccountDetails.recipientAccountIRC,
      'payeeAccountNumber': data.recipientAccountDetails.recipientAccountNumber,
      // 'reasonForTransfer': context.reasonForTransfer,
      'payeeNickName': data.recipientAccountDetails.recipientAccountNickName,
      'bankName': data.recipientAccountDetails.recipientBankName,
      'bankAddressLine1': data.recipientAccountDetails.recipientBankAddressLine1,
      'bankAddressLine2': data.recipientAccountDetails.recipientBankAddressLine2,
      'bankCity': data.recipientAccountDetails.recipientBankCity,
      'bankState': data.recipientAccountDetails.recipientBankState,
      'bankZip': data.recipientAccountDetails.recipientBankZipcode,
      'fromAccountNumber': data.transactionDetails.fromAccountNumber,
      'payeeCurrency': data.transactionDetails.currency,
      'amount': data.transactionDetails.amount,
      'transactionsNotes': data.transactionDetails.notes,
      'fee':CommonUtilities.getConfiguration("wireTranferFees"),
      'reasonForTransfer': data.transactionDetails.reasonForTransfer

    };

    if (data.recipientDetails.recipientAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
      params.routingNumber = data.recipientAccountDetails.recipientAccountRoutingCode;
      params.country = OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY;
    } else {
      params.swiftCode = data.recipientAccountDetails.recipientAccountSwiftCode;
      params.country = data.recipientDetails.recipientCountry;
      if (IBANUtils.isCountrySupportsIBAN(data.recipientDetails.recipientCountry)) {
        params.IBAN = data.recipientAccountDetails.recipientAccountIBAN;
      } else {
        params.internationalRoutingCode = data.recipientAccountDetails.recipientAccountIRC;
      }
    }


    this.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.createOneTimeWireTransfer", params, completionCallback));
  };


  WireTransfer_PresentationController.prototype.multiFactorAuthentication = function (transferCallback, amount, hamburgerItem) {
	if((CommonUtilities.getConfiguration('isMFAEnabledForWireTransfer') === "true") && Number(amount)>Number(CommonUtilities.getConfiguration('minimumAmountForMFAWireTransfer'))  && !CommonUtilities.isCSRMode())
  {
    var mfaModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('MultiFactorAuthenticationModule');
            mfaModule.presentationController.startSecureAccessCodeFlow({
                hamburgerSelection: {
                    selection1: "WIRE TRANSFER",
                    selection2: hamburgerItem
                },
                breadcrumb: kony.i18n.getLocalizedString("i18n.Transfers.WireTransfer"),
                cancelCallback: this.showWireTransfer.bind(this),
                termsAndConditions: kony.i18n.getLocalizedString("i18n.WireTransfers.TandC"),
                successCallback: transferCallback
            });
	}else
       transferCallback();
  }

  /**
   * Save Payee after one time  Wire Transfer 
   * @member WireTransfer_PresentationController
   * @param {JSON} - transaction object 
   * @returns {void} - None
   * @throws {void} - None
   */
  WireTransfer_PresentationController.prototype.savePayeeAfterTransfer = function (transactionId, data) {
    var self = this;
    var completionCallback = function (commandResponse) {
      self.hideProgressBar();
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        data.referenceNumber = commandResponse.data.referenceId;
        self.presentWireTransfer({
          addAccountAcknowledgement: data
        })
      } else {
        self.showServerError();
      }
    };

    var params = {
      'transactionId': transactionId,

    };
    this.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.savePayeeAfterTransfer", params, completionCallback));
  };
  /**
   * Delete wire transfer recipient 
   * @member WireTransfer_PresentationController
   * @param {String} - Payee Id 
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.deleteWireTransferRecipient = function (payeeId,config) {
    var self = this;
    var params={
      "payeeId":payeeId
    };
    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        if(config.offset%OLBConstants.PAGING_ROWS_LIMIT===1){
          config.offset= config.offset-OLBConstants.PAGING_ROWS_LIMIT;         
        }
        self.fetchWireTransferAccounts("manageRecipients",config);
        self.hideProgressBar();
      } else {
        self.showServerError();
      }
    };
    self.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.deleteWireRecipient", params, completionCallback));
  };
  /**
   * Get Wire Transfer transactions for a particular recipient
   * @member WireTransfer_PresentationController
   * @param {String} - Payee Id 
   * @returns {void} - None
   * @throws {void} - None
   */
  WireTransfer_PresentationController.prototype.getRecipientWireTransaction = function (payeeId) {
    var self = this;
    var params={
      "payeeId":payeeId,
      "limit":OLBConstants.WIRE_ACTIVITY_LIMIT
    };
    var completionCallback = function (commandResponse) {
      if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.presentWireTransfer({
          "viewActivityRecipientTransactions": commandResponse.data,
        });
        self.hideProgressBar();
      } else {
        self.showServerError();
      }
    };
    self.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.wireTransfer.getRecipientWireTransaction", params, completionCallback));
  };
  


  /**
   * Get Checking accounts to show details for Inbound Transfer
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */
WireTransfer_PresentationController.prototype.inboundCheckingAccounts = function () {
  var self = this;
  function onCheckingAccountsSuccessful (checkingAccounts) {
    self.hideProgressBar();
    self.presentWireTransfer({
      inboundCheckingAccounts: checkingAccounts
    })
  }
  this.showProgressBar();
  this.fetchCheckingAccounts(onCheckingAccountsSuccessful, this.showServerError)
}

/**
   * Show Server error 
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */

WireTransfer_PresentationController.prototype.showServerError = function () {
  isErrorState = true;
  CommonUtilities.showServerDownScreen();
}

/**
   * Show Server Flex on UI 
   * @member WireTransfer_PresentationController
   * @returns {void} - None
   * @throws {void} - None
   */

  WireTransfer_PresentationController.prototype.showServerErrorFlex = function (errorMessage) {
    this.hideProgressBar();
    this.presentWireTransfer({
      serverError: errorMessage
    });
  }
  
  return WireTransfer_PresentationController;
});
