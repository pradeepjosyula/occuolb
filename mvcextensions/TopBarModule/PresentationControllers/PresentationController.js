define(['SideMenuModule/PresentationControllers/PresentationController'],function(SideMenuPresenter){

    var presentWith = null;
  	var viewModel;
    var generateViewModel = function (commandData) {
       	viewModel= {
          	  userImage: commandData.userImage,
          	  email: commandData.email,
              menuAction: topBarModule.onMenu,
              accountsAction: topBarModule.onAccounts,
              transfersAction: topBarModule.onTransferAndPay,
          	  trasferMoneyAction: topBarModule.TransferSubMenu.onTransferMoney,
          	  payBillAction: topBarModule.TransferSubMenu.onPayBills,
			  payAPersonAction:topBarModule.TransferSubMenu.onSendMoney,
              toggle: null,// {accounts/transfers}
          	  feedbackAction:topBarModule.onFeedback,
			  wireTransferAction:topBarModule.TransferSubMenu.onWireMoney,
          };
      
        presentWith(viewModel);
    }
  
	var fetchUserProfile = function (onSuccess,onError) {
      	function completionCallback(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(response.data);
            } else {
                onError(response.data);
            }
        }
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getUserProfile", {}, completionCallback))
    }
	
    var topBarModule = {
      	init : function(presentTopBar){   
                //setup top bar
            presentWith = presentTopBar;
            //fetchUserProfile(generateViewModel,kony.print);
          generateViewModel({"userImage":"default_username.png","email":"a@b.com"});
            },
            onLogout : function(sender, context){
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                context = {
                    "action": "Logout"
                };
                authModule.presentationController.doLogout(context);
            },
            onMessages : function(sender, context){
                //navigate to messages page
            },
            onMenu : function(sender, context){
                //or get presenter from context to show side menu
				SideMenuPresenter.SideMenu.presentSideMenu();
            },
            onAccounts : function(sender, context){
                //navigate to accounts page
              	if(kony.application.getCurrentForm().id==="frmAccountsLanding")
                {
                  //so that it pretends to be non clickable
                }
                else
                {
                var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
				accountModule.presentationController.showAccountsDashboard();
				viewModel.toggle= "accounts";
                }
            },
            onTransferAndPay : function(sender, context){
                //show Transfer & Pay - drop down menu (toggle)
              	viewModel.toggle= "contextualMenu";
              	presentWith(viewModel);
				},  
            TransferSubMenu : {
                onTransferMoney : function(sender, context){
                    //navigate to transfer page
                    var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                    transfersModule.presentationController.showTransferScreen();
                  	viewModel.toggle= "transfers";
                },
                onPayBills : function(sender, context){
                    //navigate to pay bills screen
                    var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                    billPayModule.presentationController.showBillPayData(null, {
                        show: "AllPayees"
                    });
                    viewModel.toggle= "billPay";
                },
                onSendMoney : function(sender, context){
					 var PayAPersonModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                  	 PayAPersonModule.presentationController.NavigateToPayAPerson();

                  viewModel.toggle= "payAPerson";
				},
                onWireMoney : function(sender, context){
					var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
                  	WireTransferModule.presentationController.showWireTransfer();

					viewModel.toggle= "wireTransfer";
				},
            },
            onHelp : function(){},
      		onFeedback: function(){
              //navigate to feedback form
			  if(kony.application.getCurrentForm().id==="frmCustomerFeedback")
                {
                  //so that it pretends to be non clickable
                }
                else
                {
					var feedbackModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeedbackModule");
                    feedbackModule.presentationController.showFeedback();
                }
            },
    };
  	return {
        TopBar : topBarModule
    };
});