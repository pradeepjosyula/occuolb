define([],function(){
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
	return {
	setIdleTimeout : function() {
        var self = this;

        function idleTimeoutCompletionCallback(response) {
            self.onSessionExpire();
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.registerIdleTimeout", {}, idleTimeoutCompletionCallback));
        //this.getUserProfile();
      this.navigateToAccounts();
    },
  }
});