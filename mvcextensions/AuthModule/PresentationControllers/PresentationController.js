define(['CommonUtilities'], function(CommonUtilities) {
  var cardsJSON = [];
  var servicesList=[];
    function Auth_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(Auth_PresentationController, kony.mvc.Presentation.BasePresenter);
    Auth_PresentationController.prototype.initializePresentationController = function() {};
    Auth_PresentationController.prototype.showLoginScreen = function() {
        this.presentUserInterface("frmLogin");
    };
    /**
    * onLogin : executed when the user clicks on Login button after entering the details. If the user logs in from a new browser, an MFA flow starts.
    * @member of {Auth_PresentationController}
    * @param {JSON,loginSuccessCallBack,loginSuccessCallBack}  UserNamePasswordJSON which contains userName,Password,rememberMe Status,callBack Methods executed depending on the success or failure
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.onLogin = function(UsernamePasswordJSON,loginSuccessCallBack,loginFailureCallBack) {
        var self = this;

        function loginCompletionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                /**
                * Function is called when the user enters valid credentials 
                * Parameters: response from the command and rememberme status.
                */
              	loginSuccessCallBack(response, UsernamePasswordJSON);
              	
              	//The following code takes care of invoking an MFA flow. It has been commented out as there is little clarity on when it should be invoked(Prelogin/Postlogin?)
              	
              	/*if(kony.sdk.getCurrentInstance().tokens.CustomLogin.provider_token.params.user_attributes.isNewBrowser === "true"&& !CommonUtilities.isCSRMode()){
                  var context = {
                    successCallback: loginSuccessCallBack.bind(this, response, UsernamePasswordJSON)
                  };
                  self.startSecureAccessCodeFlow(context);
                }else{
                	loginSuccessCallBack(response, UsernamePasswordJSON);
                }*/
            } else if (response.status == kony.mvc.constants.STATUS_FAILURE) {
                /**
                * Function called when the user enters the invalid credentials.
                * Parameters: response from the command
                */
                  loginFailureCallBack(response);
            }
        }
       this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.doLogin", UsernamePasswordJSON, loginCompletionCallback));
    };
   	/**
     * startSecureAccessCodeFlow : Starts the secure access code flow. Also fetches email and phone of the user.
     * @member of {Auth_PresentationController}
     * @param {Object} - An object which a success callback for mfa flow.  
     * @return {}
     * @throws {}
   	 */
  	Auth_PresentationController.prototype.startSecureAccessCodeFlow = function(context){
      	var self = this;
      	context.mfa = true;
      	function completionCallback(response){
          if(response.status === kony.mvc.constants.STATUS_SUCCESS){
            context.email = CommonUtilities.getPrimaryContact(response.data.EmailIds);
            context.phone = CommonUtilities.getPrimaryContact(response.data.ContactNumbers);
            self.presentUserInterface("frmLogin", context);
          }else{
            CommonUtilities.showServerDownScreen();
          }       
        }
        this.businessController.execute(new kony.mvc.Business.Command('com.kony.auth.getCustomerContact', {userName: kony.mvc.MDAApplication.getSharedInstance().appContext.username}, completionCallback));
    };
     /**
    * saveUserName : used to save the userName when the user clicks on rememberMe option
    * @member of {Auth_PresentationController}
    * @param {String,JSONObject} maskedUserName and UserNamePasswordJSON which contains userName,Password,rememberMe Status
    * @return {}
    * @throws {}
   */
   Auth_PresentationController.prototype.saveUserName = function(maskedUserName,UsernamePasswordJSON) {
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.saveUserName", {
            "username":UsernamePasswordJSON.username,
            "maskedUserName":maskedUserName,
            "rememberme": UsernamePasswordJSON.rememberme,
        }));
       };
    /**
     * Function to set Idle Timeout and it internally calls onSessionExpire function
      **/
    Auth_PresentationController.prototype.setIdleTimeout = function() {
        var self = this;
        kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged = true;
        function idleTimeoutCompletionCallback(response) {
            self.onSessionExpire();
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.registerIdleTimeout", {}, idleTimeoutCompletionCallback));
      
        this.getUserProfile();
    };
    /**
    * getUserProfile : used to get the profile of the user
    * @member of {Auth_PresentationController}
    * @param {}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.getUserProfile = function() {
        var self = this;

        function completionGetUserProfileCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData = response.data;
                 kony.mvc.MDAApplication.getSharedInstance().appContext.billPayDefaultAccount = response.data.default_account_billPay;
                 kony.mvc.MDAApplication.getSharedInstance().appContext.defaultBillPayNeverShow=response.data.showBillPayFromAccPopup;
                 self.getOutageMessage();
                 console.log("Temp Fix for Async Calls");
                 console.log("Temp Fix for Async Calls");
                 self.getEntitlementsForUser();
            }
            else {
                self.navigateToLoginErrorPage({
                    errorMessage: response.data.errmsg,
                    action: "hideProgressBar"
                });
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getUserProfile", {}, completionGetUserProfileCallback));
    };
    /**
    * navigateToAccounts : naviating to Accounts DashBoard when the login is Success
    * @member of {Auth_PresentationController}
    * @param {}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.navigateToAccounts = function() {
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.showAccountsDashboard();
    };
    /**
    * onSessionExpire : The Operation to be performed when the session Expires
    * @member of {Auth_PresentationController}
    * @param {}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.onSessionExpire = function() {
        var context = {
            "action": "SessionExpired"
        };
        this.doLogout(context);
    };
     /**
    * doLogout : used to perform the logout operation 
    * @member of {Auth_PresentationController}
    * @param {context} Session Expired
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.doLogout = function(context) {
        var self = this;

        function logoutCompletionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.logoutSuccessCallback(context);
            } else {
                self.logoutErrorCallback();
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.doLogout", {}, logoutCompletionCallback));
    };
    /**
    * logoutSuccessCallback : used to show the logout successful message to the user
    * @member of {Auth_PresentationController}
    * @param {context}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.logoutSuccessCallback = function(context) {
        this.deRegisterIdleTimeout(context);

    };
     /**
    * logoutErrorCallback : used to show the error downtime screen when the logout failss 
    * @member of {Auth_PresentationController}
    * @param {context} Session Expired
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.logoutErrorCallback = function(context) {
        var context = {
            "action": "ServerDown"
        };
        this.presentUserInterface("frmLogin", context);
    };
     /**
    * deRegisterIdleTimeout : used to deregister the idle TimeOut 
    * @member of {Auth_PresentationController}
    * @param {context}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.deRegisterIdleTimeout = function(context) {
        var self = this;

        function deregisterCompletionCallback(response) {
            //deregister doesnt need any response.
            self.deregisterSuccessCallback(context);
        }
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.deregisterIdleTimeout", {}, deregisterCompletionCallback));
    };
    /**
    * deregisterSuccessCallback : used to deregister when idle TimeOut occurs
    * @member of {Auth_PresentationController}
    * @param {context}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.deregisterSuccessCallback = function(context) {
        this.navigateToLogoutScreen(context);
    };
    /**
    * navigateToLogoutScreen : used to navigate to LogOut Page depending on the status
    * @member of {Auth_PresentationController}
    * @param {context}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.navigateToLogoutScreen = function(context) {
        context.username = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
        kony.store.setItem('OLBLogoutStatus', context);
        window.location.reload();
    };
    /**
    * navigateToLoginErrorPage : used to navigate to Error Page from the Login 
    * @member of {Auth_PresentationController}
    * @param {context}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.navigateToLoginErrorPage = function(context) {
        this.presentUserInterface("frmLogin", context);
    };
    /**
    * navigateToServerDownScreen : used to navigate to Server Down Time Screen when any server Error Occurs
    * @member of {Auth_PresentationController}
    * @param {}
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.navigateToServerDownScreen = function() {
        var context = {
            "action": "ServerDown"
        };
        this.doLogout(context);

    };
    /**
    * fetchUserName : used to fetch the userName based on the details LastName,DOB,SSN
    * @member of {Auth_PresentationController}
    * @param {JSON Object , userDetailsSuccessCallBack,userDetailsSuccessCallBack}  detailsJSON which consists of the LastName,DOB,SSn and the callBack number which are executed based on the result fetched
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.fetchUserName = function(detailsJSON,userDetailsSuccessCallBack,userDetailsErrorCallBack) {
        function detailsCompletionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) 
                response.userDetails = detailsJSON;
              userDetailsSuccessCallBack(response);
            if (response.status == kony.mvc.constants.STATUS_FAILURE)
              userDetailsErrorCallBack(response);
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getUsername", detailsJSON, detailsCompletionCallback.bind(this)));
    };
   /**
    * navigateToEnrollPage :used to navigate to Enroll form from the login page  
    * @member of {Auth_PresentationController}
    * @param {data} 
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.navigateToEnrollPage = function(data) {
        this.presentUserInterface("frmEnrollNow", data);
    };
   /**
    * toResetPassword :used to reset the password 
    * @member of {Auth_PresentationController}
    * @param {String,Strng,toResetPasswordResponse} username , passowrd and the callback method which describes what to be done next once the password is reset
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.toResetPassword = function(userName,password,toResetPasswordResponse) {
        var self = this;
        var userDetails = {"userName":userName,"password":password};
        function completionResetPasswordCallback(response) {
            toResetPasswordResponse(response);
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.resetPassword", userDetails, completionResetPasswordCallback));
    };
   /**
    * resendOTP :used to resend the OTP as requested by the user 
    * @member of {Auth_PresentationController}
    * @param {resendOTPResponseSuccess,resendOTPResponseFailure} callbacks methods are called based on OTP received
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.resendOTP = function(resendOTPResponseSuccess,resendOTPResponseFailure, params){
      if(params === undefined || params === null || params === ""){    
      	params = {};
      }
      var context = {};
       var self=this;
        function completionRequestOTPCallback(response){
        if(response.status == kony.mvc.constants.STATUS_SUCCESS){
     context={
      "action":"OTP Resend Success",
      "data": response
    };
    resendOTPResponseSuccess();
      }
        else{
        context={
      "action":"OTP Resend Failure",
      "data": response
    };
      resendOTPResponseFailure();
        }
        
    }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.requestOTP", params, completionRequestOTPCallback));
      
    };
    /**
    * cvvValidate :used to validate the CVV entered by the user for the selected card 
    * @member of {Auth_PresentationController}
    * @param {maskedCardNumber,CVV entered by the user,userName,CVVValidateResponseSuccess,CVVValidateResponseFailure} callbacks methods are called based on whether entered CVV is correc or not
    * @return {}
    * @throws {}
   */
   
     Auth_PresentationController.prototype.cvvValidate = function(maskedCardNumber,cvv, userName,CVVValidateResponseSuccess,CVVValidateResponseFailure) {
        var self = this;
        var unmaskedCardNumber=self.getUnMaskedCardNumber(maskedCardNumber);
        var cvvJSON = {
            "cvv": cvv,
            "userName": userName,
            "cardNumber" : unmaskedCardNumber
        };

        function completionCVVValidateCallback(response) {
          if(response.status=== kony.mvc.constants.STATUS_SUCCESS){
               CVVValidateResponseSuccess();
          }
          else{
             CVVValidateResponseFailure();
          }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.verifyCVV", cvvJSON, completionCVVValidateCallback));
    };
    /**
    * getUnMaskedCardNumber :used to get the unmasked card number parsing through the cardsJSON 
    * @member of {Auth_PresentationController}
    * @param {maskedCardNumber} masked card Number
    * @return {unMasked Card Number}
    * @throws {}
   */
    Auth_PresentationController.prototype.getUnMaskedCardNumber = function(maskedCardNumber ) {
      for (var key in cardsJSON) {
        if (cardsJSON.hasOwnProperty(key)) {
          var val = cardsJSON[key];
          if(CommonUtilities.substituteforIncludeMethod(JSON.stringify(val),maskedCardNumber))
          {  
            var pos = JSON.stringify(val).indexOf(':',1);
            return JSON.stringify(val).substring(2,pos-1);
          }
        }
      }
    return null;

    };
   /**
    * otpValidate :used to validate the OTP entered by the user
    * @member of {Auth_PresentationController}
    * @param {String,String,OTPValidateResponseSuccess,OTPValidateResponseFailure},otp entered by the user,userName  Callback methods called based on whether entered is valid or not
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.otpValidate = function(otp, userName,OTPValidateResponseSuccess,OTPValidateResponseFailure) {
        var otpJSON = {
            "otp": otp,
            "userName": userName
        };
     

        function completionOTPValidateCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS){
                 OTPValidateResponseSuccess();
            }
          else{
               OTPValidateResponseFailure();
          }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.validateOTP", otpJSON, completionOTPValidateCallback));
    };
   
    /**
    * requestOTP :used to  request OTP for the user
    * @member of {Auth_PresentationController}
    * @param {requestOTPResponseSuccess,requestOTPResponseFailure} Callback methods called based on the requested OTP received or not
    * @return {}
    * @throws {}
   */
    Auth_PresentationController.prototype.requestOTP = function(requestOTPResponseSuccess,requestOTPResponseFailure) {
        function completionRequestOTPCallback(response){
        if(response.status == kony.mvc.constants.STATUS_SUCCESS){
          requestOTPResponseSuccess();
      }
        else{
        requestOTPResponseFailure();
         }
        
    }
      this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.requestOTP",{},completionRequestOTPCallback));
      
    };
   /**
    * goToPasswordResetOptionsPage :used to  display the password Reset Options CVV or OTP or both depending on the cards available or not
    * @member of {Auth_PresentationController}
    * @param {getCardsSuccessCallBack,getCardsErrorCallBack} Callbacks method called based on the success or failure of the response 
    * @return {}
    * @throws {}
   */ 
  Auth_PresentationController.prototype.goToPasswordResetOptionsPage = function(getCardsSuccessCallBack,getCardsErrorCallBack){
        var self=this;
        var username=kony.mvc.MDAApplication.getSharedInstance().appContext.username ;
        function resetPasswordCompletionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS)
            {
             self.getCards(response);
             getCardsSuccessCallBack(cardsJSON);
            }
            if (response.status == kony.mvc.constants.STATUS_FAILURE) 
            {
              getCardsErrorCallBack(response);
              
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getCardsByUserName", username, resetPasswordCompletionCallback));
  };
  
  /**
    * getCards :used to form the cards JSON which is a key value pair where key is the unmasked card number and the value is the masked card number
    * @member of {Auth_PresentationController}
    * @param {JSON Object} response which consists of cards of the user 
    * @return {}
    * @throws {}
   */ 
   Auth_PresentationController.prototype.getCards = function(response){
    var cardNumberJSON=response.data;
    cardsJSON=[];
    if (cardNumberJSON.length !== 0) {
            for (var index in cardNumberJSON) {
                var cardNumber = cardNumberJSON[index]["cardNumber"];
                var maskedcardNumber = this.maskCreditCardNumber(cardNumber);
                var tmpIndex = cardsJSON.length;
                cardsJSON[tmpIndex] = {};
                cardsJSON[tmpIndex][cardNumber] = maskedcardNumber;
               
            }
     }
   };
/**
    * maskCreditCardNumber :used to mask the credit card number leaving thefirst and last four digits
    * @member of {Auth_PresentationController}
    * @param {String} cardNumber 
    * @return {}
    * @throws {}
   */ 
   Auth_PresentationController.prototype.maskCreditCardNumber = function(cardNumber){
  var maskedCreditNumber;
   var firstfour = cardNumber.substring(0, 4);
   var lastfour = cardNumber.substring(cardNumber.length-4, cardNumber.length);
   maskedCreditNumber=firstfour + "XXXXXXXX" + lastfour;
   return maskedCreditNumber;
   };
   /**
    * getEntitlementsForUser :get all the entitlements available for the user
    * @member of {Auth_PresentationController}
    * @param {} 
    * @return {}
    * @throws {}
   */ 
 Auth_PresentationController.prototype.getEntitlementsForUser = function() {
        var self = this;
         var param = {
            "userName": kony.mvc.MDAApplication.getSharedInstance().appContext.username
        };
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                var servicesLists = [];
              if(response.data.services.length >= 0){
                servicesLists = response.data.services;
                kony.mvc.MDAApplication.getSharedInstance().appContext.servicesList = servicesLists;

              }
               if(response.data.Addresses.length >= 0){
                 kony.mvc.MDAApplication.getSharedInstance().appContext.address = response.data.Addresses;
               } 
              if(response.data.EmailIds.length >= 0){
                kony.mvc.MDAApplication.getSharedInstance().appContext.emailids = response.data.EmailIds;
              }
              if(response.data.ContactNumbers,length >= 0){
                 kony.mvc.MDAApplication.getSharedInstance().appContext.contactNumbers = response.data.ContactNumbers;
              }
               if(response.data.isSecurityQuestionConfigured){
               var isSecurityQuestionsEnabled = response.data.isSecurityQuestionConfigured;
               }
               self.updateServicesForUser(kony.mvc.MDAApplication.getSharedInstance().appContext.servicesList, isSecurityQuestionsEnabled);
                if (kony.mvc.MDAApplication.getSharedInstance().appContext.servicesList){
                    self.navigateToAccounts();
                }
            } else {
                self.navigateToServerDownScreen();
            }
        }
       this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getEntitlementsForUser", param, completionCallback));
   };
  
  
     /**
    * updateServicesForUser :updating the configuration keys based on the entitlements of the user
    * @member of {Auth_PresentationController}
    * @param {Object[]} It contains array of services of the user 
    * @return {}
    * @throws {}
   */ 

  Auth_PresentationController.prototype.updateServicesForUser = function(servicesListForUser,isSecurityQuestionsEnabled){
    var self=this;
    if(isSecurityQuestionsEnabled)
       self.updateSecurityQuestions(isSecurityQuestionsEnabled);
    if(servicesListForUser){
      for(var i=0;i<servicesListForUser.length;i++)
      {
         switch(servicesListForUser[i].displayName){
           case "KonyBankAccountsTransfer": 
             self.updateKonyBankAccountsTransfer(servicesListForUser[i].displayName, servicesListForUser[i].minTransferLimit, servicesListForUser[i].maxTransferLimit);
             break;
           case "OtherKonyAccountsTransfer":
             self.updateOtherKonyAccountsTransfer(servicesListForUser[i].displayName, servicesListForUser[i].minTransferLimit, servicesListForUser[i].maxTransferLimit);
             break;
           case "OtherBankAccountsTransfer":
             self.updateOtherBankAccountsTransfer(servicesListForUser[i].displayName, servicesListForUser[i].minTransferLimit, servicesListForUser[i].maxTransferLimit);
             break;
           case "InternationalAccountsTransfer":
             self.updateInternationalAccountsTransfer(servicesListForUser[i].displayName, servicesListForUser[i].minTransferLimit, servicesListForUser[i].maxTransferLimit);
             break;
           case "DomesticWireTransfer":
             self.updateDomesticWireTransfer(servicesListForUser[i].displayName, servicesListForUser[i].minTransferLimit, servicesListForUser[i].maxTransferLimit);
             break;
           case "InternationalWireTransfer":
             self.updateInternationalWireTransfer(servicesListForUser[i].displayName, servicesListForUser[i].minTransferLimit, servicesListForUser[i].maxTransferLimit);
             break;
           case "BillPay":
             self.updateBillPay(servicesListForUser[i].displayName, servicesListForUser[i].minTransferLimit, servicesListForUser[i].maxTransferLimit);
             break;
           case "PayAPerson":
              self.updatePayAPerson(servicesListForUser[i].displayName, servicesListForUser[i].minTransferLimit, servicesListForUser[i].maxTransferLimit);
         }
      }
    }
    
  };
    /**
    * updateKonyBankAccountsTransfer :updating the configuration keys "isKonyBankAccountsTransfer", "minTrabsferLimit","maxTransferLimit" based on the availability of the user services
    * @member of {Auth_PresentationController}
    * @param {String,String,String} It contains three values displayName,minTransferLimit,maxTransferLimit of the keys to be updated 
    * @return {}
    * @throws {}
   */ 
  Auth_PresentationController.prototype.updateKonyBankAccountsTransfer = function(displayName,minTransferLimit,maxTransferLimit){
     this.updateUserLevelConfiguration( "isKonyBankAccountsTransfer","true");
     this.updateUserLevelConfiguration("minKonyBankAccountsTransferLimit",minTransferLimit);
     this.updateUserLevelConfiguration("maxKonyBankAccountsTransferLimit",maxTransferLimit);
    };
    /**
    * updateKonyBankAccountsTransfer :updating the configuration keys "updateOtherKonyAccountsTransfer", "minTrabsferLimit","maxTransferLimit" based on the availability of the user services
    * @member of {Auth_PresentationController}
    * @param {String,String,String} It contains three values displayName,minTransferLimit,maxTransferLimit of the keys to be updated 
    * @return {}
    * @throws {}
   */ 
    Auth_PresentationController.prototype.updateOtherKonyAccountsTransfer = function(displayName,minTransferLimit,maxTransferLimit){
      this.updateUserLevelConfiguration("isOtherKonyAccountsTransfer","true");
     this.updateUserLevelConfiguration("minOtherKonyAccountsTransferLimit",minTransferLimit);
     this.updateUserLevelConfiguration("maxOtherKonyAccountsTransferLimit",maxTransferLimit);
    };
   /**
    * updateOtherBankAccountsTransfer :updating the configuration keys "updateOtherBankAccountsTransfer", "minTrabsferLimit","maxTransferLimit" based on the availability of the user services
    * @member of {Auth_PresentationController}
    * @param {String,String,String} It contains three values displayName,minTransferLimit,maxTransferLimit of the keys to be updated 
    * @return {}
    * @throws {}
   */ 
    Auth_PresentationController.prototype.updateOtherBankAccountsTransfer= function(displayName,minTransferLimit,maxTransferLimit){
      this.updateUserLevelConfiguration("isOtherBankAccountsTransfer","true");
      this.updateUserLevelConfiguration("minOtherBankAccountsTransferLimit",minTransferLimit);
      this.updateUserLevelConfiguration("maxOtherBankAccountsTransferLimit",maxTransferLimit);
    };
   /**
    * updateInternationalAccountsTransfer :updating the configuration keys "updateInternationalAccountsTransfer", "minTrabsferLimit","maxTransferLimit" based on the availability of the user services
    * @member of {Auth_PresentationController}
    * @param {String,String,String} It contains three values displayName,minTransferLimit,maxTransferLimit of the keys to be updated 
    * @return {}
    * @throws {}
   */ 
    Auth_PresentationController.prototype.updateInternationalAccountsTransfer= function(displayName,minTransferLimit,maxTransferLimit){
     this.updateUserLevelConfiguration("isInternationalAccountsTransfer","true");
     this.updateUserLevelConfiguration("minInternationalAccountsTransferLimit",minTransferLimit);
     this.updateUserLevelConfiguration("maxInternationalAccountsTransferLimit",maxTransferLimit);
    };
  /**
    * updatePayAPerson :updating the configuration keys "updatePayAPerson", "minTrabsferLimit","maxTransferLimit" based on the availability of the user services
    * @member of {Auth_PresentationController}
    * @param {String,String,String} It contains three values displayName,minTransferLimit,maxTransferLimit of the keys to be updated 
    * @return {}
    * @throws {}
   */ 
    Auth_PresentationController.prototype.updatePayAPerson= function(displayName,minTransferLimit,maxTransferLimit){
      this.updateUserLevelConfiguration("ispayAPersonEnabled","true");
      this.updateUserLevelConfiguration("minP2PLimit",minTransferLimit);
      this.updateUserLevelConfiguration("maxP2PLimit",maxTransferLimit);
    };
   /**
    * updateBillPay :updating the configuration keys "updateBillPay", "minTrabsferLimit","maxTransferLimit" based on the availability of the user services
    * @member of {Auth_PresentationController}
    * @param {String,String,String} It contains three values displayName,minTransferLimit,maxTransferLimit of the keys to be updated 
    * @return {}
    * @throws {}
   */ 
     Auth_PresentationController.prototype.updateBillPay= function(displayName,minTransferLimit,maxTransferLimit){
       this.updateUserLevelConfiguration("isBillPayEnabled","true");
      this.updateUserLevelConfiguration("minBillPayLimit",minTransferLimit);
      this.updateUserLevelConfiguration("maxBillPayLimit",maxTransferLimit);
    };
   /**
    * updateInternationalWireTransfer :updating the configuration keys "updateInternationalWireTransfer", "minTrabsferLimit","maxTransferLimit" based on the availability of the user services
    * @member of {Auth_PresentationController}
    * @param {String,String,String} It contains three values displayName,minTransferLimit,maxTransferLimit of the keys to be updated 
    * @return {}
    * @throws {}
   */ 
     Auth_PresentationController.prototype.updateInternationalWireTransfer= function(displayName,minTransferLimit,maxTransferLimit){
     this.updateUserLevelConfiguration("isInternationalWireTransferEnabled","true");
     this.updateUserLevelConfiguration("minInternationalWireTransferLimit",minTransferLimit);
     this.updateUserLevelConfiguration("maxInternationalWireTransferLimit",maxTransferLimit);
    };
   /**
    * updateDomesticWireTransfer :updating the configuration keys "updateDomesticWireTransfer",      "minTrabsferLimit","maxTransferLimit" based on the availability of the user services
    * @member of {Auth_PresentationController}
    * @param {String,String,String} It contains three values displayName,minTransferLimit,maxTransferLimit of the keys to be updated 
    * @return {}
    * @throws {}
   */ 
     Auth_PresentationController.prototype.updateDomesticWireTransfer= function(displayName,minTransferLimit,maxTransferLimit){
      this.updateUserLevelConfiguration("isDomesticWireTransferEnabled","true");
      this.updateUserLevelConfiguration("minDomesticWireTransferLimit",minTransferLimit);
      this.updateUserLevelConfiguration("maxDomesticWireTransferLimit",maxTransferLimit);
    };
    /**
     * updateSecurityQuestions :updating the configuration keys "isSecurityQuestionConfigured" based on the availability of the securityQuestions
     * @member of {Auth_PresentationController}
     * @param {String} It contains the value to be updated either true or false
     * @return {}
     * @throws {}
     */
    Auth_PresentationController.prototype.updateSecurityQuestions = function(isSecurityQuestionsEnabled) {
        this.updateUserLevelConfiguration("isSecurityQuestionConfigured", isSecurityQuestionsEnabled);
    };
   /**
    * updateUserLevelConfiguration :Method for updating the  configuration key by the given value
    * @member of {Auth_PresentationController}
    * @param {String,String} It contains key and value
    * @return {}
    * @throws {}
   */ 
   Auth_PresentationController.prototype.updateUserLevelConfiguration = function(key,value){
     CommonUtilities.updateUserLevelConfiguration(key,value);
   };
  
   Auth_PresentationController.prototype.launchExternalBankLogin = function(contextData) {
       var self = this;
        try {
            self.presentUserInterface("frmAccountsLanding", {"externalBankLoginContext": contextData});
        } catch(err) {
        }
   };
   
   /**
    * getOutageMessage :Method for getting the outage message from admin console
    * @member of {Auth_PresentationController}
    * @param {} 
    * @return {}
    * @throws {}
   */ 
    Auth_PresentationController.prototype.getOutageMessage = function() {
        var self = this;

        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                var outageMessages = [];
                if(response.data && response.data.records && response.data.records.length) {
                    outageMessages = response.data.records.map(function(msgObj){
                        return msgObj.MessageText
                    });
                }
                kony.mvc.MDAApplication.getSharedInstance().appContext.outageMessages = outageMessages;
            } 
            }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getOutageMessage", {}, completionCallback));
    };
    Auth_PresentationController.prototype.authenticateUserInExternalBank = function(username, password, identityProvider) {
        var self = this;
        function completionCallback(response) {
            if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.presentUserInterface("frmAccountsLanding", {
                    "externalBankLogin": response
                });
            } else if(response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.presentUserInterface("frmAccountsLanding", {
                    "externalBankLogin": response
                });
            }
        }
        try {
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.doExternalBankLogin", {
                "username": username,
                "password": password,
                "identityProvider": identityProvider
            }, completionCallback.bind(this)));
        } catch(err) {
            errorCallback(err);
        }
    };
    

    Auth_PresentationController.prototype.getPasswordRules = function(getPasswordRulesSuccess,getPasswordRulesFailure){
    var self=this;
    function completionCallBack(response){
       if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
         getPasswordRulesSuccess(response.data.records);
       }
      else{
        getPasswordRulesFailure(response);
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.getPasswordPolicies", {}, completionCallBack));
  };
    Auth_PresentationController.prototype.saveExternalBankCredentials = function(username, password, SessionToken, mainUser, bankId) {
        var self = this;
        function completionCallback(response) {
            if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.presentUserInterface("frmAccountsLanding", {
                    "saveExternalBankCredentials": response
                });
            } else if(response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.presentUserInterface("frmAccountsLanding", {
                    "saveExternalBankCredentials": response
                });
            }
        }
        try {
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.saveExternalBankCredentials", {
                "username": username,
                "password": password,
                "SessionToken": SessionToken,
                "main_user": mainUser,
                "bank_id": bankId
            }, completionCallback.bind(this)));
        } catch(err) {
            errorCallback(err);
        }
    };
	
    return Auth_PresentationController;
});
