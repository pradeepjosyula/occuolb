define([], function() {

  	function Auth_getUserProfile_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_getUserProfile_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_getUserProfile_CommandHandler.prototype.execute = function(command){

      var  self  =  this;

        /**
         * Call back function for getAll service.
         * @params  : status message, data, error message.
         */
        function  getAllCompletionCallback(status,  data,  error) {
          if(status === kony.mvc.constants.STATUS_SUCCESS){
            kony.mvc.MDAApplication.getSharedInstance().appContext.bankName = data[0].bankName;
            self.sendResponse(command,status,data[0]);
          }
          else{
            self.sendResponse(command,status,error);
          }
          //kony.mvc.MDAApplication.getSharedInstance().appContext.userProfile = data[0];
          //self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
      
      try{
        var  userProfile  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
          userProfile.getAll(getAllCompletionCallback);
      
      }catch(err){
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    };
	
	Auth_getUserProfile_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_getUserProfile_CommandHandler;
    
});