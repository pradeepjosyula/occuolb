define([], function() {

    function Auth_doLogout_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(Auth_doLogout_CommandHandler, kony.mvc.Business.CommandHandler);

    Auth_doLogout_CommandHandler.prototype.execute = function(command) {
        var self = this;
        var authParams = {
            "loginOptions": {
                "isOfflineEnabled": false
            }
        };
        function successCallback(response) {
            kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged = false;
            self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, {});
        }
        function errorCallback(response) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, response);
        }
        try {
            authClient.logout(successCallback, errorCallback, authParams);
        } catch (err) {
            kony.print(err);
        }
        /** 
        var self = this;
        var integrationClient = new kony.sdk();
        var serviceName = "RetailBankingBEServices";
        var operationName = "logout";
        var params = {};
        var headers = {
         "Content-Type":"application/json"
        };//If there are no headers,pass null
        // options is an optional parameter helps in configuring the network layer. 
        // To configure for a thin layer, use xmlHttpRequestOptions instead of httpRequestOptions.
        // Values for timeoutIntervalForRequest and timeoutIntervalForResource are in seconds.
        var options={"httpRequestOptions":{"timeoutIntervalForRequest":60,
           "timeoutIntervalForResource":600}};
        try{
           integrationClient = KNYMobileFabric.getIntegrationService(serviceName);
        }
        catch(exception){
           kony.print("Exception" + exception.message);
        }
        integrationClient.invokeOperation(operationName, headers, params, 
           function(result) {
                 kony.print("Integration Service Response is :" + JSON.stringify(result));
                 kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged = false; 
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, "Logout is success");
           }, 
           function(error) {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, "Logout is failed");
           }, options
        );
        */
    };

    Auth_doLogout_CommandHandler.prototype.validate = function() {

    };

    return Auth_doLogout_CommandHandler;

});