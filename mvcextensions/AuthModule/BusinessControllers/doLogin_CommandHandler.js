define([], function() {

  	function Auth_doLogin_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_doLogin_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_doLogin_CommandHandler.prototype.execute = function(command){
      var self=this;
        var authParams;
        if (command.context.sessiontoken) {
            authParams = {
                "session_token": command.context.sessiontoken
            };
        } else {
            authParams = {
                "username": command.context.username,
                "password": command.context.password,
                "loginOptions": {
                    "isOfflineEnabled": false
                }
            };
        }

        function successCallback(resSuccess){
		 
          //Getting username from sdk ,so that same can be passed to other services in case of csr login      		   
          var userName=kony.sdk.getCurrentInstance().tokens.CustomLogin.provider_token.params.user_attributes.userName;
          kony.mvc.MDAApplication.getSharedInstance().appContext.username = userName;
          self.sendResponse(command,kony.mvc.constants.STATUS_SUCCESS,"");
        }
        function errorCallback(resError){
           self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,resError);
       }
        try{
         authClient = KNYMobileFabric.getIdentityService("CustomLogin");
         authClient.login(authParams,successCallback,errorCallback);
        }catch(err){
           kony.print(err);
           self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
        }
    };
	
	Auth_doLogin_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_doLogin_CommandHandler;
    
});