define([], function() {

  function Auth_validateOTP_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Auth_validateOTP_CommandHandler, kony.mvc.Business.CommandHandler);

  Auth_validateOTP_CommandHandler.prototype.execute = function(command){
    var self=this;
    function completionCallBack(status,response,err){
      if(status==kony.mvc.constants.STATUS_SUCCESS){
        self.sendResponse(command,status,{});
      }
      else {
        self.sendResponse(command,status,{});
      }
    }
    var params = {
      ssn:command.context.ssn,
      userlastname:command.context.userlastname,
      dateOfBirth:command.context.dateOfBirth,
      otp:command.context.otp
      
    };
    var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
    userModel.customVerb("verifyOTP",params,completionCallBack);

  };

  Auth_validateOTP_CommandHandler.prototype.validate = function(){

  };

  return Auth_validateOTP_CommandHandler;

});