define([], function() {

  	function Auth_registerIdleTimeOut_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_registerIdleTimeOut_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_registerIdleTimeOut_CommandHandler.prototype.execute = function(command){
		 kony.mvc.MDAApplication.getSharedInstance().appContext.IDLE_TIMEOUT = kony.onlineBanking.configurations.getConfiguration("idleTimeOut");
      	 function timeOutCallback(){
           command.completionCallback.call(this ,{});
         }
      	 kony.application.registerForIdleTimeout(kony.mvc.MDAApplication.getSharedInstance().appContext.IDLE_TIMEOUT , timeOutCallback);
    };
	
	Auth_registerIdleTimeOut_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_registerIdleTimeOut_CommandHandler;
    
});