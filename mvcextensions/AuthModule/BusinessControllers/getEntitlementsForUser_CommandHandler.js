define([], function() {

  	function Auth_getEntitlementsForUser_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_getEntitlementsForUser_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_getEntitlementsForUser_CommandHandler.prototype.execute = function(command){
      var self = this;
      var param={
        "userName" : command.context.userName
    };
      function completionCallback(status,response, error) {
      if(status==kony.mvc.constants.STATUS_SUCCESS){
          self.sendResponse(command,status,response);
        }else{
          self.sendResponse(command,status,error);
        }
      }

      try {
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
         userModel.getAllEntitlements(param,completionCallback);
      } catch (error) {
       kony.print("Something went wrong");
      }
    };
	
	Auth_getEntitlementsForUser_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_getEntitlementsForUser_CommandHandler;
    
});