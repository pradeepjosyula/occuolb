define([], function() {

  	function Auth_getCustomerContact_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_getCustomerContact_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_getCustomerContact_CommandHandler.prototype.execute = function(command){
		var self = this;
      	var params = {
          userName: command.context.userName
        };
      	function onCompletion(status, response, err){
          self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS? response: err);
        }
      	var userRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('User');
      	userRepo.customVerb('getCustomerContact', params, onCompletion);
    };
	
	Auth_getCustomerContact_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_getCustomerContact_CommandHandler;
    
});