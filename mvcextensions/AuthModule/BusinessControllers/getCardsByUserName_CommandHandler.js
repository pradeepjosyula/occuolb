define([], function() {

  	function Auth_getCardsByUserName_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_getCardsByUserName_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_getCardsByUserName_CommandHandler.prototype.execute = function(command){
      var self = this;
      
      /*
      Here we have bypassed MDA and MVC methods to call customverbs as it required 
      intial one time session token to get the data.
      We have directly called integration service the format of which could be seen 
      below.
      */
      
// 		function completionCallBack(status,response,err){
           
//            if(status==kony.mvc.constants.STATUS_SUCCESS){
//              self.sendResponse(command,status,{"status":"success"});
//            }
//            else {
//              self.sendResponse(command,status,{"status":"failure"});
//             }
//          }
//           var params = {
//                         userName:command.context.userName
//                     };
//           var userModel = kony.mvc.MDAApplication.getSharedInstance().ModelStore.getModelDefinition("User");
//           userModel.customVerb("verifyCVV",params,completionCallBack);
       var integrationClient = new kony.sdk();
        var serviceName = "RetailBankingBEServices";
        var operationName = "getCardsByUsername";
        var params = { 
   					userName:command.context
        };
        var headers = {};//If there are no headers,pass null
        // options is an optional parameter helps in configuring the network layer. 
        // To configure for a thin layer, use xmlHttpRequestOptions instead of httpRequestOptions.
        // Values for timeoutIntervalForRequest and timeoutIntervalForResource are in seconds.
        var options={"httpRequestOptions":{"timeoutIntervalForRequest":60,
            "timeoutIntervalForResource":600}};
        try{
            integrationClient = KNYMobileFabric.getIntegrationService(serviceName);
        }
        catch(exception){
            kony.print("Exception" + exception.message);
        }
        integrationClient.invokeOperation(operationName, headers, params, 
            function(result) {
          	if(result!== null){
              self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, result.Cards);
            }else{
              self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, {});
            }
            }, 
            function(error) {
                 self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
            }, options
        );
    };
	
	Auth_getCardsByUserName_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_getCardsByUserName_CommandHandler;
    
});