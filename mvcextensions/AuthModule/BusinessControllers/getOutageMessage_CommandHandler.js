define([], function() {

  	function Auth_getOutageMessage_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_getOutageMessage_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_getOutageMessage_CommandHandler.prototype.execute = function(command){
      var self = this;
      function completionCallback(status,response, error) {
          if(status===kony.mvc.constants.STATUS_SUCCESS){
          self.sendResponse(command,status,response);
        }else{
          self.sendResponse(command,status,error);
        }
      }

      try {
        var OutageMessageModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("OutageMessage");
        OutageMessageModel.getOutageMessage({},completionCallback);
      } catch (error) {
       kony.print("Something went wrong");
      }
    };
	
	Auth_getOutageMessage_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_getOutageMessage_CommandHandler;
    
});