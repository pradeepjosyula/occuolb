define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;
  
    function Auth_doExternalBankLogin_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Auth_doExternalBankLogin_CommandHandler, CommandHandler);
  
    Auth_doExternalBankLogin_CommandHandler.prototype.execute = function(command) {
		var self = this;
        function successCallback(res) {
            self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, res);
        }
        function errorCallback(err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
        function loginExternalBankSuccessCallback() {
            authClient.getBackendToken(true, {
                "IdentityServiceName": command.context.identityProvider,
                "AuthParams": {
                    "loginOptions": {
                        "isOfflineEnabled": false
                    }
                }
            }, successCallback, errorCallback);
        }
        try {
            var username = command.context.username;
            var password = command.context.password;
            var identityProvider = command.context.identityProvider;
            if(!username || !password || !identityProvider || String(username).trim() === "" || String(password).trim() === "" || String(identityProvider).trim() === "") {
                throw "params are missing or passed invalid";
            }
			var authParams = {
                "username": username,
                "password": password,
                "loginOptions": {
                    "isOfflineEnabled": false
                }
            };
            authClient = KNYMobileFabric.getIdentityService(identityProvider);
            authClient.login(authParams,loginExternalBankSuccessCallback, errorCallback);
            
        } catch(err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
  
  Auth_doExternalBankLogin_CommandHandler.prototype.validate = function(){
    
    };
    
    return Auth_doExternalBankLogin_CommandHandler;
    
});