define([], function() {

	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
	var CommandResponse = kony.mvc.Business.CommandResponse;
	
  	function Auth_getUsername_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_getUsername_CommandHandler, CommandHandler);
  
  	Auth_getUsername_CommandHandler.prototype.execute = function(command){
		
      var self=this;
      /*
      Here we have bypassed MDA and MVC methods to call customverbs as it required 
      intial one time session token to get the data.
      We have directly called integration service the format of which could be seen 
      below.
      */
//          function completionCallBack(status,response,err){
           
//           self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
      	  
//          }
//           var params = {
//                         ssn:command.context.ssn,
//                         dob:command.context.dob,
//             			lastname:command.context.lastname
//                     };
//           try{
// 	          var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
//     	      userModel.customVerb("getUsername",params,completionCallBack);
//           }
//           catch(err){
//             self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
//           }

       var integrationClient = new kony.sdk();
        var serviceName = "RetailBankingBEServices";
        var operationName = "fetchUserName";
       var params = {
            ssn:command.context.ssn,
            dob:command.context.dob,
            lastname:command.context.lastname
        };
        var headers = {};//If there are no headers,pass null
        // options is an optional parameter helps in configuring the network layer. 
        // To configure for a thin layer, use xmlHttpRequestOptions instead of httpRequestOptions.
        // Values for timeoutIntervalForRequest and timeoutIntervalForResource are in seconds.
        var options={"httpRequestOptions":{"timeoutIntervalForRequest":60,
            "timeoutIntervalForResource":600}};
        try{
            integrationClient = KNYMobileFabric.getIntegrationService(serviceName);
        }
        catch(exception){
            kony.print("Exception" + exception.message);
        }
        integrationClient.invokeOperation(operationName, headers, params, 
            function(result) {
                  kony.print("Integration Service Response is :" + JSON.stringify(result));
          		   self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, result.User[0]);
            }, 
            function(error) {
                 self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
            }, options
        );
      
    };
	
	Auth_getUsername_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_getUsername_CommandHandler;
    
});