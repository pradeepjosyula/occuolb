define([], function() {

  	function Auth_deregisterIdleTimeOut_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_deregisterIdleTimeOut_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_deregisterIdleTimeOut_CommandHandler.prototype.execute = function(command){
		kony.application.unregisterForIdleTimeout();
        command.completionCallback.call(this ,{});
    };
	
	Auth_deregisterIdleTimeOut_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_deregisterIdleTimeOut_CommandHandler;
    
});