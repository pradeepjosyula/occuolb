define([], function() {

  var CommandHandler = kony.mvc.Business.CommandHandler;
  var Command = kony.mvc.Business.Command;
  var CommandResponse = kony.mvc.Business.CommandResponse;
  
    function Auth_saveExternalBankCredentials_CommandHandler(commandId) {
        CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(Auth_saveExternalBankCredentials_CommandHandler, CommandHandler);
  
    Auth_saveExternalBankCredentials_CommandHandler.prototype.execute = function(command) {
		var self = this;
        function completionCallback(status,  data,  error) {
            self.sendResponse(command,  status,  status  ===  kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
            var model = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ExternalBankIdentity");
      	    var obj = new model({
                "username": command.context.username?command.context.username : "",
                "password": command.context.password?command.context.password : "",
                "SessionToken": command.context.SessionToken?command.context.SessionToken : "",
                "main_user": command.context.main_user?command.context.main_user : "",
                "bank_id": command.context.bank_id?command.context.bank_id : ""
        });
        obj.save(completionCallback.bind(this));
        } catch(err) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
    };
  
  Auth_saveExternalBankCredentials_CommandHandler.prototype.validate = function(){
    
    };
    
    return Auth_saveExternalBankCredentials_CommandHandler;
    
});