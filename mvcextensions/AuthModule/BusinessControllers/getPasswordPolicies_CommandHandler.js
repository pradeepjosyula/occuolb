define([], function() {

  	function Auth_getPasswordPolicies_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Auth_getPasswordPolicies_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Auth_getPasswordPolicies_CommandHandler.prototype.execute = function(command){
      var self = this;
      
      function completionCallback(status,response, error) {
      if(status==kony.mvc.constants.STATUS_SUCCESS){
          self.sendResponse(command,status,response);
        }else{
          self.sendResponse(command,status,error);
        }
      }

      try {
        var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
        userModel.customVerb("getPasswordPolicies",{},completionCallback);
      } catch (error) {
       kony.print("Something went wrong");
      }
    };
	
	Auth_getPasswordPolicies_CommandHandler.prototype.validate = function(){
		
    };
    
    return Auth_getPasswordPolicies_CommandHandler;
    
});