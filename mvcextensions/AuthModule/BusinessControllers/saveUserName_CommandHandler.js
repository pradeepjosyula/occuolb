define([], function() {
    function Auth_saveUserName_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
    inheritsFrom(Auth_saveUserName_CommandHandler, kony.mvc.Business.CommandHandler);
    Auth_saveUserName_CommandHandler.prototype.execute = function(command) {
        var username = command.context.username;
        var rememberme = command.context.rememberme;
        var maskedusername=command.context.maskedUserName;
        try {
            var names = [];
            //var flag = 0;
            if (rememberme === true) {
                names = JSON.parse(localStorage.getItem("olbNames"));
                if (names === null || names === undefined) names = [];
                var matchingUserNames = names.filter(function(obj){ 
                    return obj[username];
                });
                if(matchingUserNames.length === 0) {
                    var tmpIndex = names.length;
                    names[tmpIndex] = {};
                    names[tmpIndex][username] = maskedusername;
                    localStorage.setItem("olbNames", JSON.stringify(names));
                }
                /*
                if (names === null || names === undefined) names = [];
               if (names.length === 0) flag = 1;
                for (var inst in names) {
                    if (names[inst][username] === null || names[inst][username] === undefined) 
                    {
                      flag = 1;
                    }
                  else {
						flag = 0;
					}
                }
                                
                if (flag === 1) {
					var tmpIndex = names.length;
					names[tmpIndex] = {};
                    names[tmpIndex][username] = maskedusername;
                }
          localStorage.setItem("olbNames", JSON.stringify(names));
          */
            } else {
                if ((names = localStorage.getItem('olbNames')) !== null) {
                    names = JSON.parse(names);
                    for (var index in names) {
                        if (names[index][username] !== undefined) {
                            names.splice(index, 1);
                            break;
                        }
                    }
                    //                     var pos = names.indexOf(username);
                    //                     names.splice(pos, 1);
                    localStorage.setItem("olbNames", JSON.stringify(names));
                }
            }
        } catch (err) {
            kony.print(err);
        }
    };
    Auth_saveUserName_CommandHandler.prototype.validate = function() {};
    return Auth_saveUserName_CommandHandler;
});