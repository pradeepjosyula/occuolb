define([], function() {

  function Auth_verifyCVV_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Auth_verifyCVV_CommandHandler, kony.mvc.Business.CommandHandler);

  Auth_verifyCVV_CommandHandler.prototype.execute = function(command){
    var self = this;
    function completionCallBack(status, response, error) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, status, response);
      } else {
        self.sendResponse(command, status, error);
      }
    }
    var params = {
      ssn:command.context.ssn,
      userlastname:command.context.userlastname,
      dateOfBirth:command.context.dateOfBirth,
      cvv:command.context.cvv,
      cardNumber:command.context.cardNumber
    };
    var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
    userModel.customVerb("verifyCVV",params, completionCallBack);

  };

  Auth_verifyCVV_CommandHandler.prototype.validate = function(){

  };

  return Auth_verifyCVV_CommandHandler;

});