define([], function() {

  function Auth_requestOTP_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(Auth_requestOTP_CommandHandler, kony.mvc.Business.CommandHandler);

  Auth_requestOTP_CommandHandler.prototype.execute = function(command){
    var self = this;

    function completionCallBack(status,response,err){

      if(status==kony.mvc.constants.STATUS_SUCCESS){
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS,response.otp);
      }
      else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE,err);
      }
    }

    var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
    userModel.customVerb("requestOTP",{},completionCallBack);


  };

  Auth_requestOTP_CommandHandler.prototype.validate = function(){

  };

  return Auth_requestOTP_CommandHandler;

});