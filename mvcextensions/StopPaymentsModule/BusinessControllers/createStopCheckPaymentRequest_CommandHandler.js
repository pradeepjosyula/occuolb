define([], function() {

  	function StopPayments_createStopCheckPaymentRequest_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(StopPayments_createStopCheckPaymentRequest_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	StopPayments_createStopCheckPaymentRequest_CommandHandler.prototype.execute = function(command){

            var scopeObj = this;

            function onCreateTransfer(status, data, error) {
                scopeObj.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
            }
            var context = command.context;
            var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
            var stopCheckRequest = new transactionsModel({
                'fromAccountNumber': context.fromAccountNumber,
                'transactionType': context.transactionType,
                'payeeName': context.payeeName,
                'checkNumber1': context.checkNumber1,
                'checkReason': context.checkReason,
                'transactionsNotes': context.transactionsNotes
            });

            if( context.checkNumber2 ) {
                stopCheckRequest.checkNumber2= context.checkNumber2;
            }
            if( context.amount ) {
                stopCheckRequest.amount= context.amount;
            }
            if( context.checkDateOfIssue ) {
                stopCheckRequest.checkDateOfIssue= context.checkDateOfIssue;
            }
            if(context.requestValidityInMonths) {
                stopCheckRequest.requestValidityInMonths= context.requestValidityInMonths;
            }
            stopCheckRequest.save(onCreateTransfer);
    };
	
	StopPayments_createStopCheckPaymentRequest_CommandHandler.prototype.validate = function(){
		
    };
    
    return StopPayments_createStopCheckPaymentRequest_CommandHandler;
    
});