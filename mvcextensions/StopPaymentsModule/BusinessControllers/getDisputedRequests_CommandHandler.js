define([], function() {

  	function StopPayments_getDisputedRequests_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(StopPayments_getDisputedRequests_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	StopPayments_getDisputedRequests_CommandHandler.prototype.execute = function(command){
        var self = this;

    function completionCallback(status, response, error) {
      if (status === kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, error);
    }

    try {  
      var params = {
        limit : command.context.limit,
        offset : command.context.offset,
        order : command.context.order,
        sortBy : command.context.sortBy, 
      };
      
      var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      TransactionModel.customVerb("getDisputedTransactions",params,completionCallback);  
   
      
          
    } catch (e) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, {});
    }


		
    };
	
	StopPayments_getDisputedRequests_CommandHandler.prototype.validate = function(){
		
    };
    
    return StopPayments_getDisputedRequests_CommandHandler;
    
});