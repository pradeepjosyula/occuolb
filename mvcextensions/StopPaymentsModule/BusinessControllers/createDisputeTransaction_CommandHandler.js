define([], function () {

  function StopPayments_createDisputeTransaction_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(StopPayments_createDisputeTransaction_CommandHandler, kony.mvc.Business.CommandHandler);

  StopPayments_createDisputeTransaction_CommandHandler.prototype.execute = function (command) {
    var self = this;

    function completionCallback(status, response, error) {
      if (status === kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, error);
    }

    try {
      
      var params = {
        "transactionId": command.context.transactionId,
        "disputeReason": encodeURI(command.context.disputeReason), // Platform defect for special character(ticket number 102172)
        "disputeDescription": encodeURI(command.context.disputeDescription), // Platform defect for special character(ticket number 102172)

      };
      
      
      var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
      TransactionModel.customVerb("createDisputedTransaction",params,completionCallback);  
   
      
          
    } catch (e) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, {});
    }
  };

  StopPayments_createDisputeTransaction_CommandHandler.prototype.validate = function () {

  };

  return StopPayments_createDisputeTransaction_CommandHandler;

});