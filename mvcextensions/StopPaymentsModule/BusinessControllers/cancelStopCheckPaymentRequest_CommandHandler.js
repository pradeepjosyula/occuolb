define([], function() {

  	function StopPayments_cancelStopCheckPaymentRequest_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(StopPayments_cancelStopCheckPaymentRequest_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	StopPayments_cancelStopCheckPaymentRequest_CommandHandler.prototype.execute = function(command){
        var scopeObj = this;
        var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        function onCompletionCallback(status,response,error){
          if(status === kony.mvc.constants.STATUS_SUCCESS){
            scopeObj.sendResponse(command,status,error);
          }
           else{
            scopeObj.sendResponse(command,status,error);
           } 
        }
      
        TransactionModel.removeById(command.context.transactionId, onCompletionCallback);
    };
	
	StopPayments_cancelStopCheckPaymentRequest_CommandHandler.prototype.validate = function(){
		
    };
    
    return StopPayments_cancelStopCheckPaymentRequest_CommandHandler;
    
});