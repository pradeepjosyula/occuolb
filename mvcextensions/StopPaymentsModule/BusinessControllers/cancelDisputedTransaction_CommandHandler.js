define([], function() {

  	function StopPayments_cancelDisputedTransaction_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(StopPayments_cancelDisputedTransaction_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	StopPayments_cancelDisputedTransaction_CommandHandler.prototype.execute = function(command){
		var self = this;
        var TransactionModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
        function onCompletionCallback(status,response,error){
          if(status === kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,error);
          }
           else{
               self.sendResponse(command,status,error);
           } 
        }
      
        TransactionModel.removeById(command.context.transactionId, onCompletionCallback);
    };
	
	StopPayments_cancelDisputedTransaction_CommandHandler.prototype.validate = function(){
		
    };
    
    return StopPayments_cancelDisputedTransaction_CommandHandler;
    
});