define([], function() {

  	function StopPayments_getStopCheckPaymentRequests_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(StopPayments_getStopCheckPaymentRequests_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	StopPayments_getStopCheckPaymentRequests_CommandHandler.prototype.execute = function(command){
        var self = this;  
        var params = {
            "offset": command.context.offset,
            "limit": command.context.limit,
            "sortBy":  command.context.sortBy,
            "order":  command.context.order
        };

        function completionCallback(status, response, err) {
            if(status==kony.mvc.constants.STATUS_SUCCESS)
              self.sendResponse(command, status, response);
            else
              self.sendResponse(command, status, err);
        }
        var TransactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");

        TransactionsModel.customVerb("getStopCheckPaymentRequestTransactions", params, completionCallback);
    };
	
	StopPayments_getStopCheckPaymentRequests_CommandHandler.prototype.validate = function(){
		
    };
    
    return StopPayments_getStopCheckPaymentRequests_CommandHandler;
    
});