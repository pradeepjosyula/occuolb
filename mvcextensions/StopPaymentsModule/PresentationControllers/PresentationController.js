define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {

    function StopPayments_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.initializePresentationController();
    }

    inheritsFrom(StopPayments_PresentationController, kony.mvc.Presentation.BasePresenter);

    /**
    * initializePresentationController - Method to intialize Presentation controller data , called by constructor
    * @member of {StopPayments_PresentationController}
    * @param {Object} dataInputs - dataInputs to configure Presentation controller. (optional) - useful when we need to customize.
    * @returns {}
    * @throws {}
    */
    StopPayments_PresentationController.prototype.initializePresentationController = function(dataInputs) {
        var scopeObj = this;

        scopeObj.viewFormsList = {
            frmStopPayments : 'frmStopPayments'
        };

        //Handling loading progreebar
        scopeObj.totalServiceModels = 0; //flag to wait for models from service
        scopeObj.tmpSerViewModels = []; //array to store models from service
        scopeObj.serverErrorVieModel = 'serverError';
        scopeObj.serviceViewModels = {
          frmStopPayments: []
        };
        scopeObj.activeForm =  scopeObj.viewFormsList.frmStopPayments;

        scopeObj.checkRequestReasons = [
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.Duplicate"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.LostorStolenCheck"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.DefectiveGoods"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.InsufficientFunds"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.Others")
        ];
        scopeObj.disputeTransactionRequestReasons = [
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.dontRecognise"),
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.GoodsAndService"),
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.duplicate"),
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.recurringTransaction"),
            kony.i18n.getLocalizedString("i18n.StopPayments.disputeTransaction.BillingError"),
            kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.Others")
        ];
        //Stop check request sort config
        scopeObj.stopChekRequestsConfig = {
            'sortBy' : 'transactionDate',
            'defaultSortBy' :  'transactionDate',
            'order' : OLBConstants.DESCENDING_KEY,            
            'defaultOrder' : OLBConstants.DESCENDING_KEY
        };
        scopeObj.disputedTransactionRequestsConfig = {
            'sortBy' : 'disputeDate',
            'defaultSortBy' :  'disputeDate',
            'order' : OLBConstants.DESCENDING_KEY,            
            'defaultOrder' : OLBConstants.DESCENDING_KEY
        };
    };

    /**
     * showStopPayments : Entry point for Stop payment form 
     * @member of {StopPayments_PresentationController}
     * @param {object} dataInputs, required object to show specific view
     * @returns {}
     * @throws {} 
     */
    StopPayments_PresentationController.prototype.showStopPayments = function (dataInputs) {
        var scopeObj = this;
        scopeObj.activeForm = scopeObj.viewFormsList.frmStopPayments; 
        scopeObj.loadComponents();
        if(dataInputs && dataInputs.show) {
            switch(dataInputs.show) {
                case OLBConstants.ACTION.SHOW_STOPCHECKS_FORM:
                    scopeObj.showStopChecksForm(dataInputs);
                break;
                case OLBConstants.ACTION.SHOW_DISPUTE_TRANSACTION_FORM:
                    scopeObj.showDisputeTransaction(dataInputs.data);
                break;
                case OLBConstants.DISPUTED_CHECKS:
                    scopeObj.showMyRequests({
                        selectTab: OLBConstants.DISPUTED_CHECKS
                    });
                break;
                case OLBConstants.DISPUTED_TRANSACTIONS:
                    scopeObj.showMyRequests({
                        selectTab: OLBConstants.DISPUTED_TRANSACTIONS
                    });
                break;
                default: //Default form view
                    scopeObj.showMyRequests({
                        selectTab: OLBConstants.DISPUTED_TRANSACTIONS
                    });
            }
        }
        else {
            scopeObj.showMyRequests({
                selectTab: OLBConstants.DISPUTED_TRANSACTIONS
            });
        }
    };

    /**
     * loadComponents : load Stop check form components topbar, sidemenu etc.
     * @member of {StopPayments_PresentationController}
     * @param {}
     * @returns {} 
     * @throws {}
     */
    StopPayments_PresentationController.prototype.loadComponents = function () {
        var scopeObj = this;
        var howToShowHamburgerMenu = function (sideMenuViewModel) {
            scopeObj.presentStopPayments({
                sideMenu: sideMenuViewModel
            });
        };
        scopeObj.SideMenu.init(howToShowHamburgerMenu);

        var presentTopBar = function (topBarViewModel) {
            scopeObj.presentStopPayments({
                topBar: topBarViewModel
            });
        };
        scopeObj.TopBar.init(presentTopBar);
    };

     /**
     * presentStopPayments : Update stop payments form
     * @member of {StopPayments_PresentationController}
     * @param {JSON  Object} viewModel, view model for stop payments
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.presentStopPayments = function (viewModel) {
        var scopeObj = this;
        scopeObj.presentUserInterface(scopeObj.viewFormsList.frmStopPayments, viewModel);
        scopeObj.updateLoadingForCompletePage(viewModel);
    };

    /**
     * Method to handle Progress bar
     * @member of {StopPayments_PresentationController}
     * @param {boolean} isLoading , loading flag true.false
     * @returns {} 
     * @throws {}
     */
    StopPayments_PresentationController.prototype.updateProgressBarState = function (isLoading) {
        var scopeObj = this;
        scopeObj.presentUserInterface(scopeObj.activeForm, {
            "progressBar" : isLoading
        });
    };

    /**
     * updateLoadingForCompletePage : Method to handle loading progress bar w.r.t expected view models.
     * @member of {StopPayments_PresentationController}
     * @param {Object}  viewModel ,  form view model
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.  updateLoadingForCompletePage = function (viewModel) {
        var scopeObj = this;
        if(viewModel.isLoading === true){
            scopeObj.tmpSerViewModels = viewModel.serviceViewModels;
            scopeObj.totalServiceModels = 0;
            scopeObj.updateProgressBarState(true);
        } else {
            if (scopeObj.isServiceViewModel(viewModel, scopeObj.tmpSerViewModels)) {
                scopeObj.totalServiceModels++;
            }
            if (scopeObj.totalServiceModels === scopeObj.tmpSerViewModels.length) {
                scopeObj.updateProgressBarState(false);
                scopeObj.totalServiceModels = 0;
            }
        }
    };

    /**
     * isServiceViewModel : Method to validate service view models.
     * @member of {StopPayments_PresentationController}
     * @param {Object}  viewModel ,  form view model
     * @param {Array}  serviceViewModels ,  expected serice view models.
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.isServiceViewModel = function (viewModel, serviceViewModels) {
        var scopeObj = this;
        return Object.keys(viewModel).filter(function (key) {
            return serviceViewModels.indexOf(key) >= 0 || key === scopeObj.serverErrorVieModel; //include server error as expected
        }).length > 0;
    };


    /**
     * onServerError : Method to handle server errors.
     * @member of {StopPayments_PresentationController}
     * @param {object} data - Service error object
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.onServerError = function (data) {
        var scopeObj = this;
        if(scopeObj.activeForm) {
            scopeObj.presentUserInterface(scopeObj.activeForm, {
                serverError: data
            });
            scopeObj.updateLoadingForCompletePage({
                serverError: data
            });
        }
    };

    /**
     * fetchAccounts: fetches accounts using Command - 'com.kony.StopPayments.getAccounts'
     * @member of {StopPayments_PresentationController}
     * @param {function} onSuccess , success callback
     * @param {function} onError , error callback
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.fetchAccounts = function (onSuccess, onError) {
        var scopeObj = this;
        var completionCallback = function (commandResponse) {
            if (commandResponse.status === kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(commandResponse.data);
            } else {
                onError(commandResponse.data);
            }
        };
        scopeObj.businessController.execute(
            new kony.mvc.Business.Command("com.kony.StopPayments.getAccounts", {}, completionCallback)
        );
    };


   
    /****************************************************************************************************************************
        * Stop Payments : Dispute transaction details
        ****************************************************************************************************************************/
        /**
     * showDisputeTransactionRequests: Method to show My Requests Disputed transaction request
     * @member of {StopPayments_PresentationController}
     * @param {object} sort config
     * @return none
     * @throws none
     */
    StopPayments_PresentationController.prototype.showDisputeTransactionRequests = function (dataInputs) {
        var scopeObj = this;
        dataInputs = dataInputs || {};
        var requestDatInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, scopeObj.disputedTransactionRequestsConfig);
        requestDatInputs.transactionType = OLBConstants.TRANSACTION_TYPE.DISPUTEDTRANSACTIONSREQUEST;
        scopeObj.getViewRequestsDisputedTransactions(requestDatInputs, scopeObj.onDisputeTransactionRequestsSuccess.bind(scopeObj), scopeObj.onServerError.bind(scopeObj) );
    };
    
    
    /**
     * getViewRequestsData: Method  return dispute transaction reasons
     * @member of {StopPayments_PresentationController}
     * @param {} none
     * @return {} none
     * @throws {}
     */
    StopPayments_PresentationController.prototype.getViewRequestsDisputedTransactions = function (dataInputs, onSuccess, onError) {
        
        var scopeObj = this;
        

        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                response.config = dataInputs;
                onSuccess(response);
            } else {
                onError(response);
            }
        }
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['viewDisputedRequestsResponse']
            });
            this.businessController.execute(new kony.mvc.Business.Command("com.kony.StopPayments.getDisputedRequests", dataInputs, completionCallback));
    };

    /**
     * onDisputeTransactionRequestsSuccess: Methods that excutes as success callback for getViewRequestsDisputedTransactions
     * @member of {StopPayments_PresentationController}
     * @param {inputs, success callback, error callback} 
     * @return {} none
     * @throws {} none
     */
    StopPayments_PresentationController.prototype.onDisputeTransactionRequestsSuccess = function (responseData) {
    var scopeObj = this;
    if(responseData && responseData.data) {
         scopeObj.presentStopPayments({
             'viewDisputedRequestsResponse' :  {
                 "stopDisputedRequests" : scopeObj.getDisputeRequestsViewModel(responseData.data),
                 "config" : responseData.config
             }
         });
    } else {
        scopeObj.onServerError(responseData);
    }
    };
        /**
     * getDisputeRequestsViewModel: Methods that gets view model for dispute transactions view requests
     * @member of {StopPayments_PresentationController}
     * @param {object} response data from service
     * @return viewModel for get view requests for dispute
     * @throws {} none
     */

    StopPayments_PresentationController.prototype.getDisputeRequestsViewModel = function (disputeViewRequests) {
        var scopeObj = this;
        var disputeTransactionRequestsViewmodel = disputeViewRequests || [];
        disputeTransactionRequestsViewmodel = disputeViewRequests.map(function(requestObject){
		var finalRequestObject = {


				disputeDate:  requestObject.disputeDate === undefined || requestObject.disputeDate === null ? kony.i18n.getLocalizedString("i18n.common.none") : CommonUtilities.getFrontendDateString((new Date(requestObject.disputeDate)), CommonUtilities.getConfiguration("frontendDateFormat")),
				
				transactionDesc: requestObject.description === undefined || requestObject.description === null ?  kony.i18n.getLocalizedString("i18n.common.none") : requestObject.description,
				
                transactionId: requestObject.transactionId,
				
				amount: requestObject.amount === undefined || requestObject.amount === null  ? kony.i18n.getLocalizedString("i18n.common.none") : CommonUtilities.formatCurrencyWithCommas(requestObject.amount),
				
				disputeStatus:requestObject.disputeStatus,
				
                fromAccountNumber: requestObject.fromAccountNumber,
                fromAccountNickName: requestObject.fromNickName,
                fromAccountName: requestObject.fromAccountName,
                fromAccount: CommonUtilities.mergeAccountNameNumber(requestObject.fromNickName || requestObject.fromAccountName, requestObject.fromAccountNumber),
				
				toAccountName:  requestObject.toAccountName || requestObject.payPersonName || requestObject.payeeNickName|| requestObject.payeeName,
				
                transactionDate : CommonUtilities.getFrontendDateString(requestObject.transactionDate),
                transactionType: requestObject.transactionType,
				
				disputeReason:requestObject.disputeReason,
				
				disputeDescription: requestObject.disputeDescription === undefined || requestObject.disputeDescription === null ?  kony.i18n.getLocalizedString("i18n.common.none") : requestObject.disputeDescription,
				
				onSendMessageAction : function(){
                    var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                    alertsMsgsModule.presentationController.showAlertsPage(null, {
                        show: "CreateNewMessage"
                    });
                }
            };
            if (requestObject.disputeStatus === OLBConstants.TRANSACTION_STATUS.INPROGRESS) {
                finalRequestObject.onCancelRequest = function () {
                    scopeObj.onCancelDisputeTransactionRequest(requestObject.transactionId);
                };
            }

           

         return finalRequestObject;
        });
        return disputeTransactionRequestsViewmodel;
    };

    
    /**
     * getdisputeTransactionReasonsListViewModel: Method  return dispute transaction reasons
     * @member of {StopPayments_PresentationController}
     * @param {} none
     * @return {Array}, dispute transaction reasons list
     * @throws {}
     */
    StopPayments_PresentationController.prototype.getdisputeTransactionReasonsListViewModel = function () {
        var  scopeObj = this;
        return scopeObj.disputeTransactionRequestReasons.map(function(reason){
            return {
                id : reason,
                name : reason
            };
        });
    };
    
    
    /**
   * createDisputeTransaction : Method for calling create transaction service for dispute a transaction
   * @member of {StopPayments_PresentationController}
   * @param {object} params 
   * @return {} none 
   * @throws {} none 
   */
    StopPayments_PresentationController.prototype.createDisputeTransaction = function (params, input) {
        var self = this;

        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.presentStopPayments({
                    "disputeTransactionResponse": {
                        data: response.data,
                        values: input
                    }
                });
 
            } else {
                self.onServerError(response.data);
            }
        }
        this.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['disputeTransactionResponse']
            });
        
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.StopPayments.createDisputeTransaction", params, completionCallback));
    };
    /**
     * showDisputeTransaction : Method to show dispute transaction pageX
     * @member of {StopPayments_PresentationController}
     * @param {object} transaction 
     * @return {} none 
     * @throws {} none 
     */
    StopPayments_PresentationController.prototype.showDisputeTransaction = function (data) {
        var transaction = data.disputeTransactionObject.transaction;
        var From = CommonUtilities.mergeAccountNameNumber(transaction.fromNickName || transaction.fromAccountName  , transaction.fromAccountNumber);
        var transactionDate = CommonUtilities.getFrontendDateString(transaction.transactionDate, CommonUtilities.getConfiguration('frontendDateFormat'));
        var viewModel = {
            fromAccountNumber: From,
            toAccount: transaction.toAccountName || transaction.payPersonName || transaction.payeeNickName|| transaction.payeeName,
            amount: CommonUtilities.formatCurrencyWithCommas(Math.abs(transaction.amount), true),
            date: transactionDate,
            types: transaction.transactionType,
            referenceNumber: data.disputeTransactionObject.reference,
            notes: transaction.transactionsNotes,
        };
        this.presentStopPayments({
            disputeTransactionObject: {
                data: viewModel,
                onCancel: data.onCancel,
                onBacktoAccountDetails: function () {
                    var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                    accountsModule.presentationController.showAgainAccountsDetails(transaction.fromAccountNumber);
                }
            }
        });
    };

    /****************************************************************************************************************************
     * Stop Payments : Checks
     ****************************************************************************************************************************/

    
    /**
     * isCheckAccount: Method to return whether account support checks or not 
     * @member of {StopPayments_PresentationController}
     * @param {RBObjects.Account} account , account object  - RBObjects.Account.
     * @returns {boolean} is this Account support check?
     * @throws {}
     */
    StopPayments_PresentationController.prototype.isCheckAccount = function(account){
        return account.supportChecks && account.supportChecks === '1';
    };

    /**
     * showStopChecksForm: Method to show Stop Check request form
     * @member of {StopPayments_PresentationController}
     * @param {object} dataInputs ,input values for pre popultated data
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.showStopChecksForm = function (dataInputs) {
        var scopeObj = this;
            scopeObj.updateLoadingForCompletePage({
                isLoading: true,
                serviceViewModels: ['stopChecksFormAccounts']
            });
        scopeObj.fetchAccounts(scopeObj.onStopChecksFormAccountsSuccess.bind(scopeObj), CommonUtilities.showServerDownScreen);
        scopeObj.presentStopPayments({
            'stopChecksFormData' :  scopeObj.getStopChecksFormViewModel(dataInputs)
        });
    };

    /**
     * onStopChecksFormAccountsSuccess: Method to handle check accounts list
     * @member of {StopPayments_PresentationController}
     * @param {}
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.onStopChecksFormAccountsSuccess = function (accounts) {
        var scopeObj = this;
        scopeObj.presentStopPayments({
            'stopChecksFormAccounts': scopeObj.getStopChecksFormAccountsViewModel(accounts.filter(scopeObj.isCheckAccount))
        });
    };

    /**
     * getStopChecksFormAccountsViewModel: Method to create and return Stop check form accounts view model
     * @member of {StopPayments_PresentationController}
     * @param {Array} accounts, RBObjects.Account array
     * @return {Array} Stop check form accounts view model
     * @throws {}
     */
    StopPayments_PresentationController.prototype.getStopChecksFormAccountsViewModel = function (accounts) {
        var createAccountsViewModal = function (account) {
            return {
                accountName: CommonUtilities.getAccountDisplayName(account),
                accountID: account.accountID,
                type: account.accountType
            };
        };
        return accounts.map(createAccountsViewModal);
    };

    /**
     * getStopChecksFormViewModel: Method to create and return Stop check form
     * @member of {StopPayments_PresentationController}
     * @param {object} data, pre populated form data
     * @return {object} Stop check form view model
     * @throws {}
     */
    StopPayments_PresentationController.prototype.getStopChecksFormViewModel = function (data) {
        var scopeObj = this;
        data = data || {};
        var isSeriesChecks = data.requestType === OLBConstants.CHECK_REQUEST_TYPES.SERIES;
        scopeObj.onCancel = data.onCancel ||  function () {
            scopeObj.presentStopPayments({
                "myRequests": {
                    selectTab: OLBConstants.DISPUTED_CHECKS
                }
            });
        };

        return {
            accountID: data.accountID ||  data.fromAccountNumber|| null,
            payeeName: data.payeeName || "",
            isSeriesChecks: isSeriesChecks,
            checkNumber1: data.checkNumber1,
            checkNumber2: data.checkNumber2,
            checkDateOfIssue: data.checkDateOfIssue ? CommonUtilities.getFrontendDateString((new Date(data.checkDateOfIssue)), CommonUtilities.getConfiguration("frontendDateFormat")) : CommonUtilities.getFrontendDateString((new Date()), CommonUtilities.getConfiguration("frontendDateFormat")),
            checkReason: data.checkReason,
            checkAmount: Math.abs(data.amount),
            description: data.transactionsNotes || "",
            maxDesriptionLength: OLBConstants.NOTES_MAX_LENGTH,
            showStopPaymentServiceFeesAndValidity : CommonUtilities.getConfiguration("enalbeStopPaymentServiceFeesAndValidity") === "true",
            checkServiceFee :  CommonUtilities.getConfiguration("checkServiceFee"),
            checkServiceVality :  CommonUtilities.getConfiguration("checkServiceVality"),
            serviceChargableText :  kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable11") + " " + kony.i18n.getLocalizedString("i18n.common.currencySymbol") + CommonUtilities.getConfiguration("checkServiceFee") + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable12") + " " + CommonUtilities.getConfiguration("checkServiceVality") + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable13"),
            onCancel: scopeObj.onCancel
        };
        
    };

    /**
     * stopCheckRequest: Method to create stop check request 
     * @member of {StopPayments_PresentationController}
     * @param {object} transactionModel, check request transaction object
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.stopCheckRequest = function (transactionModel) {
        var scopeObj = this;

        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['successStopCheckRequest']
        });
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                response.data.fromAccountNumber = transactionModel.fromAccountNumber;
                scopeObj.onCreateStopCheckRequestSuccess(response.data);
            } else {
                scopeObj.onServerError(response.data);
            }
        }
        var checkRequestModel = scopeObj.createCheckRequestModel(transactionModel);
        scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.StopPayments.createStopCheckPaymentRequest", checkRequestModel, completionCallback));
    };

    /**
     * createCheckRequestModel: Method to create Check request model object for Create check request
     * @member of {StopPayments_PresentationController}
     * @param {object} transactionModel, check request transaction object
     * @return {object} formatted transasction model for Create check request
     * @throws {}
     */
    StopPayments_PresentationController.prototype.createCheckRequestModel = function (transactionModel) {
        var checkRequestModel = {};
        if (transactionModel) {
            checkRequestModel = {
                transactionType : transactionModel.transactionType || OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST,
                fromAccountNumber: transactionModel.fromAccountNumber,
                payeeName: transactionModel.payeeName.trim(),
                checkNumber1: transactionModel.checkNumber1,
                amount: transactionModel.amount,
                requestValidityInMonths : CommonUtilities.getConfiguration("checkServiceVality"),
                checkReason: transactionModel.checkReason,
                transactionsNotes: transactionModel.description.trim(),
            };
            if( transactionModel.checkNumber2 ) { //for series checks
                checkRequestModel.checkNumber2= transactionModel.checkNumber2;
            }
            if( transactionModel.amount ) { //for single check
                checkRequestModel.amount= transactionModel.amount;
            }
            if( transactionModel.checkDateOfIssue ) { //for single check
                checkRequestModel.checkDateOfIssue= CommonUtilities.sendDateToBackend(CommonUtilities.getFrontendDateString(transactionModel.checkDateOfIssue,CommonUtilities.getConfiguration('frontendDateFormat')));
            }
        }
        else {
            CommonUtilities.ErrorHandler.onError("Invalid transaction Model");
        }
        return checkRequestModel;
    };

    /**
     * stopCheckRequest: Method to create stop check request 
     * @member of {StopPayments_PresentationController}
     * @param {object} successData, stop check request success resposne
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.onCreateStopCheckRequestSuccess = function (successData) {
        var scopeObj = this;
        if (successData && successData.referenceId) {
            scopeObj.presentStopPayments({
                "successStopCheckRequest" : {
                    referenceNumber : successData.referenceId,
                    onMyRequestAction : function(){
                        scopeObj.showMyRequests({
                            selectTab : OLBConstants.DISPUTED_CHECKS
                        });
                    },
                    onBacktoAccountDetails: function(){
                        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                        accountsModule.presentationController.showAgainAccountsDetails(successData.fromAccountNumber);
                    }
                }
            });
        }
        else {
            scopeObj.onServerError(successData);
        }
    };

    /**
     * getCheckReasonsListViewModel: Method  return check reasons
     * @member of {StopPayments_PresentationController}
     * @param {}
     * @return {Array}, check request reasons list
     * @throws {}
     */
    StopPayments_PresentationController.prototype.getCheckReasonsListViewModel = function () {
        var  scopeObj = this;
        return scopeObj.checkRequestReasons.map(function(reason){
            return {
                id : reason,
                name : reason
            };
        });
    };

    /******************************************************************************************************
     * Stop Payments : My Requests
     ******************************************************************************************************/
    
    /**
     * showMyRequests: Method to show My Requests
     * @member of {StopPayments_PresentationController}
     * @param {object} account, account object
     * @return {Array}, check request reasons list
     * @throws {}
     */
    StopPayments_PresentationController.prototype.showMyRequests = function (dataInputs) {

        var scopeObj = this;
        dataInputs = dataInputs || {};
        var selectTab = dataInputs.selectTab || OLBConstants.DISPUTED_TRANSACTIONS;

        scopeObj.presentStopPayments({
            "myRequests" : {
                selectTab : selectTab,
                addNewStopCheckRequestAction:  {
                    displayName: kony.i18n.getLocalizedString("i18n.StopCheckPayments.AddNewStopCheckRequest"),
                    action: function () {
                        scopeObj.showStopChecksForm({
                            onCancel: function () {
                                scopeObj.presentStopPayments({
                                    "myRequests": {
                                        selectTab: OLBConstants.DISPUTED_CHECKS
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });

        switch(selectTab) {
            case OLBConstants.DISPUTED_TRANSACTIONS: 
            scopeObj.showDisputeTransactionRequests({
                resetSorting: true
            });
                break;
            case OLBConstants.DISPUTED_CHECKS:
                scopeObj.showDisputeCheckRequests({
                    resetSorting: true
                });
            break;
        }
    };

    /**
     * showDisputeCheckRequests: Method to show My Requests Disputed stop check request
     * @member of {StopPayments_PresentationController}
     * @param {object} account, account object
     * @return {Array}, check request reasons list
     * @throws {}
     */
    StopPayments_PresentationController.prototype.showDisputeCheckRequests = function (dataInputs) {
        var scopeObj = this;
        dataInputs = dataInputs || {};
        var requestDatInputs = CommonUtilities.Sorting.getSortConfigObject(dataInputs, scopeObj.stopChekRequestsConfig);
        requestDatInputs.transactionType = OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST;
        scopeObj.getStopCheckRequests(requestDatInputs, scopeObj.onStopCheckRequestsSuccess.bind(scopeObj), scopeObj.onServerError.bind(scopeObj) );
    };

    /**
     * getStopCheckRequests: Method to fetch stop check request from MF/Command handler - com.kony.StopPayments.getStopCheckPaymentRequests.
     * @member of {StopPayments_PresentationController}
     * @param {object} dataInputs, data Inputs to get stop Check requests
     * @param {function} onSuccess, success call back
     * @param {function} onError, error call back
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.getStopCheckRequests = function (dataInputs, onSuccess, onError) {
        
        var scopeObj = this;
        
        scopeObj.updateLoadingForCompletePage({
            isLoading: true,
            serviceViewModels: ['stopCheckRequestsViewModel'] //expected view model object
        });
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                response.config = dataInputs;
                onSuccess(response);
            } else {
                onError(response);
            }
        }
        scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.StopPayments.getStopCheckPaymentRequests", dataInputs, completionCallback));
    };

    /**
     * onStopCheckRequestsSuccess: Method to handle successful stop check requests
     * @member of {StopPayments_PresentationController}
     * @param {object} responseData, response data
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.onStopCheckRequestsSuccess = function (responseData) {
        var scopeObj = this;
       if(responseData && responseData.data) {
            scopeObj.presentStopPayments({
                'stopCheckRequestsViewModel' :  {
                    "stopchecksRequests" : scopeObj.getStopCheckRequestsViewModel(responseData.data),
                    "config" : responseData.config
                }
            });
       } else {
           scopeObj.onServerError(responseData);
       }
    };

    /*
     * getStopCheckRequestsViewModel: Method to return Stop Check Requests view model
     * @member of {StopPayments_PresentationController}
     * @param {Array} stopChecksRequests, stop check requests data array 
     * @return {Array} stopCheckRequestsViewModel, stop check requests view model array 
     * @throws {}
     */
    StopPayments_PresentationController.prototype.getStopCheckRequestsViewModel = function (stopChecksRequests) {
        var scopeObj = this;
        var stopCheckRequestsViewModel = stopChecksRequests || [];
        stopCheckRequestsViewModel = stopChecksRequests.map(function(requestObject){
            var finalRequestObject = {
                transactionId: requestObject.transactionId,
                transactionDate : CommonUtilities.getFrontendDateString(requestObject.transactionDate),
                transactionType: requestObject.transactionType,
                payeeName: requestObject.payeeName,
                statusDescription: requestObject.statusDescription,
                fromAccountNumber: requestObject.fromAccountNumber,
                fromAccountName: requestObject.fromAccountName,
                fromAccountNickName: requestObject.fromNickName,
                fromAccount: CommonUtilities.mergeAccountNameNumber(requestObject.fromNickName || requestObject.fromAccountName, requestObject.fromAccountNumber),
                requestValidity: requestObject.requestValidity ? CommonUtilities.getFrontendDateString(new Date(requestObject.requestValidity),CommonUtilities.getConfiguration('frontendDateFormat')) : kony.i18n.getLocalizedString("i18n.common.none"),
                checkReason: requestObject.checkReason,
                transactionsNotes: requestObject.transactionsNotes === undefined || requestObject.transactionsNotes === null ?  kony.i18n.getLocalizedString("i18n.common.none") : requestObject.transactionsNotes,
                requestType: requestObject.requestType,
                amount: requestObject.amount === undefined || requestObject.amount === null  ? kony.i18n.getLocalizedString("i18n.common.NA") : CommonUtilities.formatCurrencyWithCommas(requestObject.amount),
                checkDateOfIssue:  requestObject.checkDateOfIssue ? CommonUtilities.getFrontendDateString(new Date(requestObject.checkDateOfIssue),CommonUtilities.getConfiguration('frontendDateFormat')) : kony.i18n.getLocalizedString("i18n.common.NA"),
                onSendMessageAction : function(){
                    var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
                    alertsMsgsModule.presentationController.showAlertsPage(null, {
                        show: "CreateNewMessage"
                    });
                }
            };

            if(requestObject.requestType === OLBConstants.CHECK_REQUEST_TYPES.SINGLE) {
                finalRequestObject.checkNumber = requestObject.checkNumber1;
            } else if(requestObject.requestType === OLBConstants.CHECK_REQUEST_TYPES.SERIES){
                finalRequestObject.checkNumber = requestObject.checkNumber1 + " " + OLBConstants.CHECK_SERIES_SEPARATOR + " " +  requestObject.checkNumber2;
            } else {
                CommonUtilities.ErrorHandler.onError("getStopCheckRequestsViewModel : Invalid request type: " + requestObject.requestType);
            }

            if (requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.STOPPED || requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.INPROGRESS) {
                finalRequestObject.onCancelRequest = function(){
                    scopeObj.onCancelStopCheckRequest(requestObject.transactionId);
                };
            } else if (requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.REQUESTEXPIRED ) {
                finalRequestObject.onReNewRequest = function(){
                    requestObject.onCancel = scopeObj.presentStopPayments.bind(scopeObj, {
                        "myRequests": {
                            selectTab: OLBConstants.DISPUTED_CHECKS
                        }
                    });
                    scopeObj.showStopChecksForm(requestObject);

                };
            } else if (requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.CLEARED) {
                //Nothing
            } else if (requestObject.statusDescription === OLBConstants.TRANSACTION_STATUS.FAILED) {
                //Nothing
            }

            return finalRequestObject;
        });
        return stopCheckRequestsViewModel;
    };

    /**
     * onCancelDisputeTransactionRequest: Method to handle cancel dispute transactions request action,
     * @member of {StopPayments_PresentationController}
     * @param {String} transactionId, requested disputed transaction, transaction id.
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.onCancelDisputeTransactionRequest = function (transactionId) {
        var scopeObj = this;
        scopeObj.presentStopPayments({
            "cancelStopCheckAction" : {
                headerText: kony.i18n.getLocalizedString("i18n.StopCheckPayments.CancelDisputeTransaction"),
                message : kony.i18n.getLocalizedString("i18n.StopCheckPayments.AreYouSureToCancelTheDisputeRequest"),
                confirmCancelAction : function(){
                    scopeObj.cancelDisputeTransactionRequest(transactionId, scopeObj.showDisputeTransactionRequests.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
                }
            }
        });
    };


    
    /**
     * cancelDisputeTransactionRequest: Method to cancel/delete dispute a transaction
     * @member of {StopPayments_PresentationController}
     * @param {String} transactionId, requested disputed transaction
     * @param {function} onSuccess, success call back
     * @param {function} onError, error call back
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.cancelDisputeTransactionRequest = function (transactionId, onSuccess, onError) {
        var scopeObj = this;
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(response);
            } else {
                onError(response);
            }
        }
        if(transactionId) {
            scopeObj.updateLoadingForCompletePage({
                isLoading: true,
                serviceViewModels: ['cancelRequestSuccess'] //expected view model object
            });    
            var params = {
                transactionId: transactionId
            };
            scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.StopPayments.cancelDisputedTransaction", params, completionCallback));
        } else {
            CommonUtilities.ErrorHandler.onError("cancelDisputeTransactionRequest : Invalid transaction Id" + transactionId);
        }
        
    };

    /**
     * onCancelStopCheckRequest: Method to handle cancel stop check request action,
     * @member of {StopPayments_PresentationController}
     * @param {String} transactionId, requested stop check transaction id.
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.onCancelStopCheckRequest = function (transactionId) {
        var scopeObj = this;
        scopeObj.presentStopPayments({
            "cancelStopCheckAction" : {
                headerText: kony.i18n.getLocalizedString("i18n.StopCheckPayments.CancelStopCheckPayment"),
                message : kony.i18n.getLocalizedString("i18n.StopCheckPayments.AreYouSureToCancelTheRequest"),
                showStopPaymentServiceFeesAndValidity : CommonUtilities.getConfiguration("enalbeStopPaymentServiceFeesAndValidity") === "true",
                checkServiceFee :  CommonUtilities.getConfiguration("checkServiceFee"),
                checkServiceVality :  CommonUtilities.getConfiguration("checkServiceVality"),
                serviceChargableText :  kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable11") + kony.i18n.getLocalizedString("i18n.common.currencySymbol") + CommonUtilities.getConfiguration("checkServiceFee") + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable12") + CommonUtilities.getConfiguration("checkServiceVality") + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ThisServiceIsChargeable13") +" "+ kony.i18n.getLocalizedString("i18n.StopcheckPayments.ThisRequestWillbeProcessedWithin12hrs"),
                confirmCancelAction : function(){
                    scopeObj.canelStopCheckRequest(transactionId, scopeObj.showDisputeCheckRequests.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
                }
            }
        });
    };

    /**
     * canelStopCheckRequest: Method to cancel/delete stop check request transaction.
     * @member of {StopPayments_PresentationController}
     * @param {String} transactionId, requested stop check transaction id.
     * @param {function} onSuccess, success call back
     * @param {function} onError, error call back
     * @return {}
     * @throws {}
     */
    StopPayments_PresentationController.prototype.canelStopCheckRequest = function (transactionId, onSuccess, onError) {
        var scopeObj = this;
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                onSuccess(response);
            } else {
                onError(response);
            }
        }
        if(transactionId) {
            scopeObj.updateLoadingForCompletePage({
                isLoading: true,
                serviceViewModels: ['cancelRequestSuccess'] //expected view model object
            });    
            var params = {
                transactionId: transactionId
            };
            scopeObj.businessController.execute(new kony.mvc.Business.Command("com.kony.StopPayments.cancelStopCheckPaymentRequest", params, completionCallback));
        } else {
            CommonUtilities.ErrorHandler.onError("canelStopCheckRequest : Invalid transaction Id" + transactionId);
        }
        
    };

    return StopPayments_PresentationController;
});