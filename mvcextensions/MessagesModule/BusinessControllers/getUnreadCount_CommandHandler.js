define([], function() {

  	function Messages_getUnreadCount_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Messages_getUnreadCount_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Messages_getUnreadCount_CommandHandler.prototype.execute = function(command){
      	var self=this;
		function completionCallback(status,response,error){
			if(status==kony.mvc.constants.STATUS_SUCCESS){
            	self.sendResponse(command,status,response[0].count);  
            }else{
              self.sendResponse(command,status,error);  
            }
		}
      
      	try{
        	var MessageModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Messages");
			MessageModel.customVerb('MessageCount',{},completionCallback);
      	}catch(err){
        	self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);  
      	}
    };
	
	Messages_getUnreadCount_CommandHandler.prototype.validate = function(){
		
    };
    
    return Messages_getUnreadCount_CommandHandler;
    
});