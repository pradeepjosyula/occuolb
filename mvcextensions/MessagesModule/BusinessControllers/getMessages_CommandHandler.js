define([], function() {

  	function Messages_getMessages_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Messages_getMessages_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	Messages_getMessages_CommandHandler.prototype.execute = function(command){
      	var self=this; 
        function completionCallback(status,response,error){
            if(status==kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,response);
            }else{
              self.sendResponse(command,status,error);
            }
        }
      
      	try{
         	var MessageModel=kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Messages"); 
         	var expr=kony.mvc.Expression;
      		var criteria=expr.and(expr.eq("messageType", "Inbox"),expr.eq("pageSize", "10"),expr.eq("recordNumber", "0"));
        	MessageModel.getByCriteria(criteria,completionCallback);
      	}catch(err){
        	 self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
      	}
     };
	
	Messages_getMessages_CommandHandler.prototype.validate = function(){
		
    };
    
    return Messages_getMessages_CommandHandler;
    
});