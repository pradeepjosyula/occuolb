define(['CommonUtilities'], function(CommonUtilities) {

    function Feedback_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }

    inheritsFrom(Feedback_PresentationController, kony.mvc.Presentation.BasePresenter);

    Feedback_PresentationController.prototype.initializePresentationController = function() {
        
    };
    /**
     * Navigate to Survey
     * @member Feedback_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
  	Feedback_PresentationController.prototype.showSurveyForm = function() {
      var self=this;
	   var surveyModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SurveyModule");
	     surveyModule.presentationController.showSurvey();
    };
    /**
     * Calls present user interface for frmCustomerFeedback form with data
     * @member Feedback_PresentationController
     * @param {JSON} data
     * @returns {void} - None
     * @throws {void} - None
     */
    Feedback_PresentationController.prototype.presentFeedback = function (data) {
        this.presentUserInterface('frmCustomerFeedback', data);
    };
    /**
     * Show feedback based on login status 
     * @member Feedback_PresentationController
     * @param {JSON} data
     * @returns {void} - None
     * @throws {void} - None
     */
  	Feedback_PresentationController.prototype.showFeedback = function(sender) {
      var self=this;
        if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
           this.presentFeedback({"preLoginView":"preLoginView"});
        } else {
           this.loadFeedbackComponents();
           this.presentFeedback({"postLoginView":"postLoginView"});
        } 
    };

  	Feedback_PresentationController.prototype.loadFeedbackComponents = function () {
    this.loadComponents('frmCustomerFeedback');
  	};
    /**
     * Function to load components
     * @member Feedback_PresentationController
     * @param {object} frm
     * @param {JSON} data
     * @returns {void} - None
     * @throws {void} - None
     */
    Feedback_PresentationController.prototype.loadComponents = function (frm,data) {
      var self = this;
      var howToShowHamburgerMenu = function (sideMenuViewModel) {
        self.showView(frm, { "sideMenu": sideMenuViewModel });
      };
      self.SideMenu.init(howToShowHamburgerMenu);

      var presentTopBar = function (topBarViewModel) {
        self.showView(frm, { "topBar": topBarViewModel });
      };
      self.TopBar.init(presentTopBar);
    };

  	Feedback_PresentationController.prototype.showView = function (frm, data) {
    	this.presentUserInterface(frm, data);
  	};
  	/**
     * Show Progress Bar
     * @member Feedback_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
  	Feedback_PresentationController.prototype.showProgressBar = function () {
            var self = this;
            self.presentFeedback({ "showProgressBar": "showProgressBar"});
    };
    /**
     * Hide Progress Bar
     * @member Feedback_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    Feedback_PresentationController.prototype.hideProgressBar = function (data) {
            var self = this;
            self.presentFeedback({ "hideProgressBar": "hideProgressBar" });
    };
		/**
     * Create Feedback
     * @member Feedback_PresentationController
     * @param {JSON} feedbackParams
     * @returns {void} - None
     * @throws {void} - None
     */
    Feedback_PresentationController.prototype.createFeedback = function(feedbackParams) {
      var self=this;
      function onCompletionCallback(response){
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          self.hideProgressBar();
		  self.presentFeedback({ "submitFeedbackSuccess": "submitFeedbackSuccess" });
        } else {
          self.hideProgressBar();
          CommonUtilities.showServerDownScreen();
        }
      }
      self.businessController.execute(
        new kony.mvc.Business.Command("com.kony.feedback.createUserFeedback", feedbackParams, onCompletionCallback)
      );
    };
    return Feedback_PresentationController;
});