define([], function() {

  	var CommandHandler = kony.mvc.Business.CommandHandler;
    var Command = kony.mvc.Business.Command;
    var CommandResponse = kony.mvc.Business.CommandResponse;

  	function Feedback_createFeedback_CommandHandler(commandId) {
       CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(Feedback_createFeedback_CommandHandler, CommandHandler);
  
  	Feedback_createFeedback_CommandHandler.prototype.execute = function(command){
		var self=this;
      	var context=command.context;
      	function completionCallback(status,response,error){
            if(status===kony.mvc.constants.STATUS_SUCCESS){
              self.sendResponse(command,status,response);
            }else{
              self.sendResponse(command,status,error);
            }
        }
      	try{
           var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("User");
           var feedbackParams={
              'rating': context.rating,
              'description':context.description,
              'featureRequest':context.featureRequest,
             };
          userModel.customVerb('createUserFeedback',feedbackParams,completionCallback);
       }
       catch(err){
         self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
       }
    };
	
	Feedback_createFeedback_CommandHandler.prototype.validate = function(){
		
    };
    
    return Feedback_createFeedback_CommandHandler;
    
});