define([], function() {

  	function AlertsMsgs_dismissNotification_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_dismissNotification_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	AlertsMsgs_dismissNotification_CommandHandler.prototype.execute = function(command){
		var self=this;
		var params={
			notificationId:command.context.notificationId
		}
    function dismissNotificationcompletionCallback(status,response,error){
      if(status==kony.mvc.constants.STATUS_SUCCESS){
        self.sendResponse(command,status,response);
      }else{
        self.sendResponse(command,status,error);
      }
    }

    try{
      
      var notificationModel= kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Notifications");

      notificationModel.deleteNotification(params,dismissNotificationcompletionCallback);

    }catch(err){
      self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
    }
    };
	
	AlertsMsgs_dismissNotification_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_dismissNotification_CommandHandler;
    
});