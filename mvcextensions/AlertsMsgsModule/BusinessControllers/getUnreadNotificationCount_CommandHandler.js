define([], function() {

  	function AlertsMsgs_getUnreadNotificationCount_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_getUnreadNotificationCount_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	AlertsMsgs_getUnreadNotificationCount_CommandHandler.prototype.execute = function(command){
		var self = this;
        function getUnreadNotificationCountCompletionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
            var notificationModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Notifications");
            notificationModel.getUnreadNotificationCount({},getUnreadNotificationCountCompletionCallback);
        } catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
	
	AlertsMsgs_getUnreadNotificationCount_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_getUnreadNotificationCount_CommandHandler;
    
});