define([], function() {

  	function AlertsMsgs_getAlertsBySearch_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_getAlertsBySearch_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	AlertsMsgs_getAlertsBySearch_CommandHandler.prototype.execute = function(command){
		var self = this;
        var criteria= kony.mvc.Expression.eq("searchString", command.context.searchString);
        function getAlertsBySearchCompletionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
			  var notificationsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Notifications");
              notificationsModel.getByCriteria(criteria, getAlertsBySearchCompletionCallback);
        }
        catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
	
	AlertsMsgs_getAlertsBySearch_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_getAlertsBySearch_CommandHandler;
    
});