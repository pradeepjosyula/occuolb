define([], function() {

  	function AlertsMsgs_getRequests_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_getRequests_CommandHandler, kony.mvc.Business.CommandHandler);
  
    AlertsMsgs_getRequests_CommandHandler.prototype.execute = function(command){
      var self=this;
      function completionCallback(status,response,error){
        if(status==kony.mvc.constants.STATUS_SUCCESS){
          self.sendResponse(command,status,response);
        }else{
          self.sendResponse(command,status,error);
        }
      }

      var messagingModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SecureMessaging");
      var param = {
        "username" : command.context.username,
        "softDeleteFlag" : command.context.softDeleteFlag
      };
      messagingModel.getRequests(param, completionCallback);
     
    };
	AlertsMsgs_getRequests_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_getRequests_CommandHandler;
    
});