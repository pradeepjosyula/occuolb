define(['CommonUtilities'], function(CommonUtilities) {

  	function AlertsMsgs_downloadAttachment_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_downloadAttachment_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	AlertsMsgs_downloadAttachment_CommandHandler.prototype.execute = function(command){
        var mfURL = KNYMobileFabric.mainRef.config.selflink;
        mfURL = mfURL.substring(0, mfURL.indexOf("/authService"));
        var param = {};
        param.url =  mfURL + "/services/data/v1/RBObjects/operations/SecureMessaging/getMessageAttachment?media_id="+command.context.mediaId + "&filename=" + command.context.fileName;
        param.filename = command.context.fileName;
        CommonUtilities.downloadFile(param);
    };
	
	AlertsMsgs_downloadAttachment_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_downloadAttachment_CommandHandler;
    
});
