define([], function() {

  	function AlertsMsgs_getRequestCategories_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_getRequestCategories_CommandHandler, kony.mvc.Business.CommandHandler);
  
    AlertsMsgs_getRequestCategories_CommandHandler.prototype.execute = function(command){
      var self=this;
      function completionCallback(status,response,error){
        if(status==kony.mvc.constants.STATUS_SUCCESS){
          self.sendResponse(command,status,response);
        }else{
          self.sendResponse(command,status,error);
        }
      }
      var messagingModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SecureMessaging");
      messagingModel.getRequestCategory({}, completionCallback);
    };
	AlertsMsgs_getRequestCategories_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_getRequestCategories_CommandHandler;
    
});