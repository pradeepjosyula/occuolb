define([], function() {

  function AlertsMsgs_createNewRequestOrMessage_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(AlertsMsgs_createNewRequestOrMessage_CommandHandler, kony.mvc.Business.CommandHandler);

  AlertsMsgs_createNewRequestOrMessage_CommandHandler.prototype.execute = function(command){

    var self=this;
    try{
      var xhr = new XMLHttpRequest();
      var params = new FormData();

      if (command.context.files[0]) params.append("fileName", command.context.files[0].file);
      if (command.context.files[1]) params.append("fileName1", command.context.files[1].file);
      if (command.context.files[2]) params.append("fileName2", command.context.files[2].file);
      if (command.context.files[3]) params.append("fileName3", command.context.files[3].file);
      if (command.context.files[4]) params.append("fileName4", command.context.files[4].file);

      if(command.context.requestid) {
        params.append("requestid", command.context.requestid);
      }
      if(command.context.subject) {
        params.append("requestsubject", command.context.subject);
      }
      params.append("messagedescription", btoa(command.context.description));
      params.append("username", command.context.username);
      params.append("createdby", command.context.username);
      params.append("priority", "Medium");
      if(command.context.categoryid) {
        params.append("requestcategory_id", command.context.categoryid);
      }
      xhr.onreadystatechange=function() {
        if (this.readyState === 4 && this.status === 200) {
          var response = JSON.parse(this.responseText);
          self.sendResponse(command,kony.mvc.constants.STATUS_SUCCESS,response);
        } if (this.readyState === 4 && this.status !== 200) {
           self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,response);
        }
      };
      var mfURL = KNYMobileFabric.mainRef.config.selflink;
      mfURL = mfURL.substring(0, mfURL.indexOf("/authService"));
      var uploadURL = mfURL + "/services/data/v1/RBObjects/operations/SecureMessaging/createCustomerRequest";

      xhr.open("POST", uploadURL, true);
      xhr.setRequestHeader("X-Kony-Authorization", KNYMobileFabric.currentClaimToken);
      xhr.send(params);
    }catch(err){
      self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err); 
    }
  };

  AlertsMsgs_createNewRequestOrMessage_CommandHandler.prototype.validate = function(){

  };

  return AlertsMsgs_createNewRequestOrMessage_CommandHandler;

});
