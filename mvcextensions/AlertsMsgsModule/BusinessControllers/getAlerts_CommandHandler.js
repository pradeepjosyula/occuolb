define([], function() {

  	function AlertsMsgs_getAlerts_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_getAlerts_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	AlertsMsgs_getAlerts_CommandHandler.prototype.execute = function(command){
		var self = this;

        function getAlertsCompletionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
			  var notificationsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Notifications");
             notificationsModel.getAll(getAlertsCompletionCallback);
        }
        catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };
	
	AlertsMsgs_getAlerts_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_getAlerts_CommandHandler;
    
});