define([], function() {

  	function AlertsMsgs_getMessages_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_getMessages_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	AlertsMsgs_getMessages_CommandHandler.prototype.execute = function(command){
		var self=this;
      function completionCallback(status,response,error){
        if(status==kony.mvc.constants.STATUS_SUCCESS){
          self.sendResponse(command,status,response);
        }else{
          self.sendResponse(command,status,error);
        }
      }

      var messagingModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SecureMessaging");
      var param = {
        "request_id" : command.context.requestId
      };
      messagingModel.getAllMessagesForARequest(param, completionCallback);
    };
	
	AlertsMsgs_getMessages_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_getMessages_CommandHandler;
    
});