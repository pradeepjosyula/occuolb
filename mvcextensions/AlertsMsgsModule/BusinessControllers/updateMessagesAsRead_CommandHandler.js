define([], function() {

  	function AlertsMsgs_updateMessagesAsRead_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_updateMessagesAsRead_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	AlertsMsgs_updateMessagesAsRead_CommandHandler.prototype.execute = function(command){
		  var self=this;
      function completionCallback(status,response,error){
        if(status==kony.mvc.constants.STATUS_SUCCESS){
          self.sendResponse(command,status,response);
        }else {
          self.sendResponse(command,status,error);
        }
      }
      
      var messagingModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("SecureMessaging");
      var param = {
        "markallasread" : "true",
        "requestid" : command.context.requestId,
        "modifiedby" : command.context.username
      };
      messagingModel.updateRequest(param, completionCallback);
      
    };
	
	AlertsMsgs_updateMessagesAsRead_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_updateMessagesAsRead_CommandHandler;
    
});