define([], function() {

  	function AlertsMsgs_updateNotificationAsRead_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(AlertsMsgs_updateNotificationAsRead_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	AlertsMsgs_updateNotificationAsRead_CommandHandler.prototype.execute = function(command){
		
		var self = this;
		function completionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try{
			var notificationsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Notifications");
			var notificationsObject = new notificationsModel({
					"userNotificationId": command.context.userNotificationId
				});
			notificationsObject.partialUpdate(completionCallback);
		} catch(error) {
			self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
		}
    };
	
	AlertsMsgs_updateNotificationAsRead_CommandHandler.prototype.validate = function(){
		
    };
    
    return AlertsMsgs_updateNotificationAsRead_CommandHandler;
    
});