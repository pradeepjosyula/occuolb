define([], function () {
  var alertsData = [];
  var requestsData = [];
  var deletedRequests = [];
  var unreadMessagesCount = 0;
  var unreadNotificationCount = 0;

  function AlertsMsgs_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(AlertsMsgs_PresentationController, kony.mvc.Presentation.BasePresenter);

  AlertsMsgs_PresentationController.prototype.initializePresentationController = function () {

  };

  /**
    * showAlertsPage :This is the function which is used to show the Alerts Page depending on the Sender which can be from hamburger or Accounts Landing etc
    * @member of {AlertsMsgs_PresentationController}
    * @param {Sender , Data}  Sender can be Hamburger , AccountsLanding
    * return {} 
    * @throws {}
   */ 
  AlertsMsgs_PresentationController.prototype.showAlertsPage = function (sender, data) {
    this.loadAlertsComponents();
    this.showUnreadNotificationCount();
    if (data) {
      if (data.show === "CreateNewMessage") {
        this.showRequests(null, "CreateNewMessage");
      } else if (data.show === "ShowSelectedMessage") {
        //select the request and get the messages related to that request.
        this.showRequests(data.selectedRequestId);
      } else if (data.show === "Messages") {
        this.showRequests();
      }
    } else {
      this.showUnreadMessagesCount();
      this.showAlerts();
    }
  };

  /**
    * loadComponents :This is the function which is used to load the Alerts Components
    * @member of {AlertsMsgs_PresentationController}
    * @param {form , Data} 
    * return {} 
    * @throws {}
   */ 
  AlertsMsgs_PresentationController.prototype.loadAlertsComponents = function () {
    this.showProgressBar(); 
    this.loadComponents('frmNotificationsAndMessages');
  };

  /**
    * showView :This is the function which is used show the form with the required data
    * @member of {AlertsMsgs_PresentationController}
    * @param {form , Data} 
    * return {} 
    * @throws {}
   */ 
  AlertsMsgs_PresentationController.prototype.showView = function (frm, data) {
    this.presentUserInterface(frm, data);
  };

  /**
    * loadComponents :This is the function which is used reload the Notifications And Messages Page or from the hamburger Menu
    * @member of {AlertsMsgs_PresentationController}
    * @param {form} 
    * return {} 
    * @throws {}
   */ 
  AlertsMsgs_PresentationController.prototype.loadComponents = function (frm) {
    var scopeObj = this;
    var howToShowHamburgerMenu = function (sideMenuViewModel) {
      scopeObj.showView(frm, {
        "sideMenu": sideMenuViewModel
      });
    };
    scopeObj.SideMenu.init(howToShowHamburgerMenu);
    var presentTopBar = function (topBarViewModel) {
      scopeObj.showView(frm, {
        "topBar": topBarViewModel
      });
    };
    scopeObj.TopBar.init(presentTopBar);
  };
  
  /**
    * presentAlerts :This is the function which is used reload the Notifications And Messages Page
    * @member of {AlertsMsgs_PresentationController}
    * @param {data} 
    * return {} 
    * @throws {}
   */ 
  AlertsMsgs_PresentationController.prototype.presentAlerts = function (data) {
    this.presentUserInterface("frmNotificationsAndMessages", data);
  };

  AlertsMsgs_PresentationController.prototype.showProgressBar = function () {
    var self = this;
    self.presentAlerts({
      "ProgressBar": {
        show: true
      }
    });
  };

  /**
    * showAlerts :This is the function which is used to display the Alerts of the user
    * @member of {AlertsMsgs_PresentationController}
    * @param {} 
    * return {} 
    * @throws {}
   */ 

  AlertsMsgs_PresentationController.prototype.showAlerts = function () {
    var self = this;
    function GetAlertsCompletionCallback(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        alertsData = response.data;
        viewModel.data = response.data;
      } else {
        viewModel.data = [];
        viewModel.errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.fetchAlertsErrorMsg");
      }
      self.presentAlerts({
        "showAlertsViewModel": viewModel
      });
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.getAlerts", {}, GetAlertsCompletionCallback));

  };

  /**
    * showUnreadNotificationCount :This is the function which is used to show the unread notification count in the tab
    * @member of {AlertsMsgs_PresentationController}
    * @param {} 
    * return {} 
    * @throws {}
   */ 
  AlertsMsgs_PresentationController.prototype.showUnreadNotificationCount = function () {
    var self = this;
    function getUnreadNotificationCountCompletionCallback(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data !== undefined && response.data[0] !== undefined && response.data[0].unreadNotificationCount !== undefined) {
        viewModel.count = response.data[0].unreadNotificationCount;
      } else {
        viewModel.count = 0;
      }
      self.presentAlerts({
        "unreadNotificationCountViewModel": viewModel
      });
    }
    self.getUnreadNotificationCount(getUnreadNotificationCountCompletionCallback.bind(self));
  };

  /**
    * getAlertsDetails :This is the function which is used all the details for the particular notification
    * @member of {AlertsMsgs_PresentationController}
    * @param {String} notificationId -- The unique notification Id for the notification
    * return {JSON} the details of the notification like subject,Description ,Date etc.
    * @throws {}
   */ 
  AlertsMsgs_PresentationController.prototype.getAlertsDetails = function (userNotificationId) {
    var self = this;
    for (var key in alertsData) {
      if (alertsData.hasOwnProperty(key)) {
        var val = alertsData[key];
        if (val.userNotificationId == userNotificationId) {
          return val;
        }
      }
    }
    return null;
  };
  
  AlertsMsgs_PresentationController.prototype.hideProgressBar = function () {
    var self = this;
    self.presentAlerts({
      "ProgressBar": {
        show: false
      }
    });
  };
 
 /**
    * updateNotificationAsRead :This is the function which is used update the notification As read
    * @member of {AlertsMsgs_PresentationController}
    * @param {String} notificationId -- The unique notification Id for the notification
    * return {}
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.updateNotificationAsRead = function (notificationId) {
    var self = this;
    function updateNotificationAsReadCompletionCallback(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        var val = alertsData.filter(function (data) {
          if (data.userNotificationId == notificationId) return data;
        })[0];
        val.isRead = 1;
        viewModel.status = "success";
      } else {
        viewModel.status = "failure";
        viewModel.errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.updateNotificationErrorMsg");
      }
      self.presentAlerts({
        "updateNotificationAsReadViewModel": viewModel
      });
    }
    var param = {
      "userNotificationId": notificationId
    };
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.updateNotificationAsRead", param, updateNotificationAsReadCompletionCallback));
  };

  /**
    * dismissNotification :This is the function which is used to dismiss the Alert
    * @member of {AlertsMsgs_PresentationController}
    * @param {String} selectedUserNotificationId -- The notification Id which is to be deleted
    * return {}
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.dismissNotification = function (selectedUserNotificationId) {
    var self = this;
    function dismissAlertsCompletionCallBack(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        viewModel.status = "success";
      } else {
        viewModel.status = "error";
        viewModel.errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.dismissNotificationErrorMsg");
      }
      self.presentAlerts({
        "dismissAlertsViewModel": viewModel
      });
    }
    var params = {
      "notificationId": selectedUserNotificationId
    };
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.dismissAlerts", params, dismissAlertsCompletionCallBack));
  };

  /**
    * searchAlerts :This is the function which is used to search for the alert based on the search String entered
    * @member of {AlertsMsgs_PresentationController}
    * @param {String} SearchString -- the string to be searched across all the alerts
    * return {}
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.searchAlerts = function (searchString) {
    var self = this;
    var params = {
      searchString: searchString
    };
    function searchAlertsCompletionCallBack(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        alertsData = response.data;
        viewModel.data = response.data;
      } else {
        viewModel.data = [];
        viewModel.errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.searchAlertsErrorMsg");
      }
      self.presentAlerts({
        "searchAlertsViewModel": viewModel
      });
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.getAlertsBySearch", params, searchAlertsCompletionCallBack));
  };
   /**
    * showUnreadMessagesCount :This is the function which is used to show the unread messages count which is used to update it in the tab
    * @member of {AlertsMsgs_PresentationController}
    * @param {}  
    * return {}
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.showUnreadMessagesCount = function () {
    var self = this;
    function getUnreadMessagesCountCompletionCallback(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        viewModel.unReadMessagesCount = response.data;
      } else {
        viewModel.unReadMessagesCount = 0;
      }
      self.presentAlerts({
        "unreadMessagesCountViewModel": viewModel
      });
    }
    self.getUnreadMessagesCount(getUnreadMessagesCountCompletionCallback.bind(self));
  };

   /**
    * getUnreadMessagesOrNotificationsCount :This is the function which is used to get unread notifications count and unread messages count to show the red dot on the Alerts ICON
    * @member of {AlertsMsgs_PresentationController}
    * @param {unreadMsgsOrNotificationsCountCompletionCallback}  callBack method executed once the count of the notifications and the count of the messages is received
    * return {}
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.getUnreadMessagesOrNotificationsCount = function (unreadMsgsOrNotificationsCountCompletionCallback) {
    // if there is any unread notification or message then make it as true or false
    var self = this;
    function unreadMessagesCountCompletionCallback(response) {
      function completionCallback(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          self.unreadMessagesCount = response.data;
        } else {
          self.unreadMessagesCount = 0;
        }
        var totalUnreadCount = parseInt(self.unreadNotificationCount) + parseInt(self.unreadMessagesCount);
        if (totalUnreadCount > 0) {
          kony.mvc.MDAApplication.getSharedInstance().appContext.hasUnreadMessagesOrNotifications = true;
        } else {
          kony.mvc.MDAApplication.getSharedInstance().appContext.hasUnreadMessagesOrNotifications = false;
        }
        var viewModel = {
          "totalUnreadCount": totalUnreadCount
        };
        unreadMsgsOrNotificationsCountCompletionCallback(viewModel);
      }
      if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data !== undefined && response.data[0] !== undefined && response.data[0].unreadNotificationCount !== undefined) {
        self.unreadNotificationCount = response.data[0].unreadNotificationCount;
      } else {
        self.unreadNotificationCount = 0;
      }
      //calling the function to get the unread messages count
      self.getUnreadMessagesCount(completionCallback.bind(self));
    }
     //calling the function to get the unread notifications  count
    self.getUnreadNotificationCount(unreadMessagesCountCompletionCallback);
  };

  /**
    * getUnreadNotificationCount :This is the function which is used to get unread notifications count
    * @member of {AlertsMsgs_PresentationController}
    * @param {ureadNotificationCountCompletionCallback}  callBack method executed once the count of the notifications is received
    * return {}
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.getUnreadNotificationCount = function (ureadNotificationCountCompletionCallback) {
    var self = this;
    function completionCallback(response) {
      ureadNotificationCountCompletionCallback(response);
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.getUnreadNotificationCount", {}, completionCallback));
  };

   /**
    * getRequests :This is the function which is used to get all the requests of the particular user
    * @member of {AlertsMsgs_PresentationController}
    * @param {param,getRequestsCompletionCallback} param is JSON which consists of the userName and the SoftDelete whose value is false and the callBack method executed when the status of the response is success
    * return {}
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.getRequests = function (param, getRequestsCompletionCallback) {
    var self = this;
    function completionCallback(response) {
      getRequestsCompletionCallback(response);
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.getRequests", param, completionCallback));
  };

   /**
    * getMessages :This is the function which is used to get the messages for a request
    * @member of {AlertsMsgs_PresentationController}
    * @param {param,ureadMessagesCountCompletionCallback} param is JSON which consists of the request_id and the callBack method executed when the status of the response is success
    * return {}
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.getMessages = function (param, getMessagesCompletionCallback) {
    var self = this;
    function completionCallback(response) {
      getMessagesCompletionCallback(response);
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.getMessages", param, completionCallback));
  };

   /**
    * getUnreadMessagesCount :This is the function which is used to get the unread messages count all the requests
    * @member of {AlertsMsgs_PresentationController}
    * @param {ureadMessagesCountCompletionCallback} callBack method executed when the status of the reaponse is Success  
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.getUnreadMessagesCount = function (ureadMessagesCountCompletionCallback) {
    var self = this;
    function getUnreadMessagesCountCompletionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data !== undefined && response.data.customerrequests_view !== undefined) {
        response.data = self.countUnreadMessages(response.data.customerrequests_view);
      } else {
        //Server Error
        response.data = 0;
      }
      ureadMessagesCountCompletionCallback(response);
    }
    var param = {
      username: kony.mvc.MDAApplication.getSharedInstance().appContext.username,
      softDeleteFlag: "false"
    };
    self.getRequests(param, getUnreadMessagesCountCompletionCallback.bind(self));
  };

  /**
    * countUnreadMessages :This is the function which is count all the unread messages 
    * @member of {AlertsMsgs_PresentationController}
    * @param {data} data is array of all the requests 
    * @return {count} count  is the number of unread messages
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.countUnreadMessages = function (data) {
    var count = 0;
    if (data && data.length > 0) {
      var tempData = JSON.parse(JSON.stringify(data));
      count = tempData.map(function (item) {
        return parseInt(item.unreadmsgs);
      }).reduce(function (a, b) {
        return a + b;
      }, 0);
    }
    return count;
  };

  /**
    * showRequests :This is the function which is used to show all the requests of a logged in user
    * @member of {AlertsMsgs_PresentationController}
    * @param {selectedRequestId,createNewMessage} selectedRequestId -- get the request id selected and CreateNewMessage -- template to be shown when new message is clicked 
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.showRequests = function (selectedRequestId, createNewMessage) {
    var self = this;
    function getRequestsCompletionCallback(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data !== undefined && response.data.customerrequests_view !== undefined) {
        requestsData = response.data.customerrequests_view;
        self.unreadMessagesCount = self.countUnreadMessages(response.data.customerrequests_view);
        viewModel.data = response.data.customerrequests_view;
        viewModel.unReadMessagesCount = self.unreadMessagesCount;
        if (selectedRequestId) {
          viewModel.selectedRequestId = selectedRequestId;
        }
        if (createNewMessage) {
          viewModel.createNewMessage = "createNewMessage";
          self.getRequestCategories();
        }
      } else {
        viewModel.data = [];
        viewModel.unReadMessagesCount = 0;
        viewModel.errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.fetchMessagesErrorMsg");
      }
      self.presentAlerts({
        "showRequestsViewModel": viewModel
      });
    }
    var param = {
      username: kony.mvc.MDAApplication.getSharedInstance().appContext.username,
      softDeleteFlag: "false"
    };
    self.getRequests(param, getRequestsCompletionCallback.bind(self));
  };

   /**
    * showMessages :This is the function which is used to show all the messages for a particular request
    * @member of {AlertsMsgs_PresentationController}
    * @param {String} request_id which is the unique id generated when the request is created 
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.showMessages = function (requestId) {
    var self = this;
    function getMessagesCompletionCallback(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data !== undefined && response.data.messages !== undefined) {
        viewModel.data = response.data.messages;
      } else {
        //Server Error
        viewModel.data = [];
      }
      self.presentAlerts({
        "showMessagesViewModel": viewModel
      });
    }
    var param = {
      "requestId": requestId
    };
    self.getMessages(param, getMessagesCompletionCallback.bind(self));
  };

   /**
    * showDeletedRequests :This is the function which is used to show the messages which are deleted
    * @member of {AlertsMsgs_PresentationController}
    * @param {} 
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.showDeletedRequests = function () {
    var self = this;
    function getDeletedRequestsCompletionCallback(response) {
      var viewModel = {};
      if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data !== undefined && response.data.customerrequests_view !== undefined) {
        deletedRequests = response.data.customerrequests_view;
        requestsData = response.data.customerrequests_view;
        viewModel.data = response.data.customerrequests_view;
      } else {
        //Server Error
        viewModel.data = [];
        viewModel.errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.fetchDeletedMessagesErrorMsg");
      }
      self.presentAlerts({
        "showDeletedRequestsViewModel": viewModel
      });
    }
    var param = {
      "username": kony.mvc.MDAApplication.getSharedInstance().appContext.username,
      "softDeleteFlag": "true"
    };
    self.getRequests(param, getDeletedRequestsCompletionCallback.bind(self));
  };

  /**
    * createNewRequestOrMessage :This is the function which is used to create a new request or message
    * @member of {AlertsMsgs_PresentationController}
    * @param {requestParam}  requestParam which consists of files ,description , subject,category_id and userName
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.createNewRequestOrMessage = function (requestParam) {
    var self = this;
    function createRequestCompletionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        if(requestParam.requestid) {
          self.showRequests(requestParam.requestid);  
        } else {
          self.showRequests();
        }
      } else {
          //Server Error
          var errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.createMessageErrorMsg");
          if(requestParam.requestid) {
              self.presentAlerts({
                "createNewMessageError" : errorMsg
              });
          } else {
              self.presentAlerts({
                "createNewRequestError" : errorMsg
              });
          }
      }
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.createNewRequestOrMessage", requestParam, createRequestCompletionCallback));
  };

  /**
    * getRequestCategories :This is the function which is used to get the list of different categories for creating a new message
    * @member of {AlertsMsgs_PresentationController}
    * @param {}  
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.getRequestCategories = function () {
    var self = this;
    var viewModel = {};
    function getRequestCategoriesCompletionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data !== undefined && response.data.requestcategory !== undefined) {
        viewModel.data = response.data.requestcategory;
      } else {
        //Server Error
        viewModel.data = [];
      }
      self.presentAlerts({
          "createNewRequestOrMessagesViewModel": viewModel
        });
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.getRequestCategories", {}, getRequestCategoriesCompletionCallback));
  };

   /**
    * restoreRequest :the function to restore the request after it is deleted
    * @member of {AlertsMsgs_PresentationController}
    * @param {String}  requestId the unqiue of the request which is generated when the new request is created
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.restoreRequest = function (requestId) {
    var self = this;
    var param = {
      "requestId": requestId,
      "username": kony.mvc.MDAApplication.getSharedInstance().appContext.username
    };
    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.showUnreadMessagesCount();
        self.showDeletedRequests();
      } else {
        self.hideProgressBar();
      }
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.restoreDeletedRequest", param, completionCallback));
  };

  /**
    * hardDeleteRequest :the function to hardDeleteRequest the request and the request deleted permanently from the DeletedMessages tab
    * @member of {AlertsMsgs_PresentationController}
    * @param {String}  requestId the unqiue of the request which is generated when the new request is created
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.hardDeleteRequest = function (requestId) {
    var self = this;
    var param = {
      "requestId": requestId,
      "username": kony.mvc.MDAApplication.getSharedInstance().appContext.username
    };
    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.showDeletedRequests();
      } else {
        self.hideProgressBar();
      }
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.hardDeleteRequest", param, completionCallback));
  };
  /**
    * softDeleteRequest :the function to softdelete the request and the request is moved to DeletedMessages Tab
    * @member of {AlertsMsgs_PresentationController}
    * @param {String}  requestId the unqiue of the request which is generated when the new request is created
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.softDeleteRequest = function (requestId) {
    var self = this;
    var param = {
      "requestId": requestId,
      "username": kony.mvc.MDAApplication.getSharedInstance().appContext.username
    };

    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.showRequests();
      } else {
        self.hideProgressBar();
      }
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.softDeleteRequest", param, completionCallback));
  };
  
  /**
    * updateMessageAsRead :the function to update all the messages in the particular request as read 
    * @member of {AlertsMsgs_PresentationController}
    * @param {String}  requestId the unqiue of the request which is generated when the new request is created
    * @return {} 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.updateMessageAsRead = function (requestId) {
    var self = this;
    var param = {
      "requestId": requestId,
      "username": kony.mvc.MDAApplication.getSharedInstance().appContext.username
  };
    var val = requestsData.filter(function (data) {
      if (data.id == requestId) return data;
    })[0];
    var viewModel = {
      "readCount": val.unreadmsgs
    };
    val.unreadmsgs = 0;
    function updateMessageAsReadCompletionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        self.presentAlerts({
          "updateMessageAsReadSuccessViewModel": viewModel
        });
      }
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.markAllMessagesAsRead", param, updateMessageAsReadCompletionCallback));

  };
  /**
    * getRequestsDetails :the function to get all the details like subject,Description,unreadMsgs count etc of a particular request
    * @member of {AlertsMsgs_PresentationController}
    * @param {String}  requestId the unqiue of the request which is generated when the new request is created
    * @return {JSON} JSON of all the details of the request 
    * @throws {}
   */
  AlertsMsgs_PresentationController.prototype.getRequestsDetails = function (requestId) {
    var self = this;
    for (var key in requestsData) {
      if (requestsData.hasOwnProperty(key)) {
        var val = requestsData[key];
        if (val.id == requestId) {
          return val;
        }
      }
    }
    return null;
  };

  /**
   * Search the requests in Messages Tab
   * @param {String} searchString -  String to be searched in the data
   * @returns data which matches the search String
   * @throws Exception if something goes wrong. 
   */
    AlertsMsgs_PresentationController.prototype.searchRequest = function (searchString) {
    var self = this;
    var searchData = [];
    if(requestsData) {
      for (var i = 0; i < requestsData.length; i++) {
        if (requestsData[i].requestsubject.toLowerCase().indexOf(searchString.toLowerCase())>=0){
                    searchData.push(requestsData[i]);
                }
      }      
    }
    var unreadSearchMessagesCount = self.countUnreadMessages(searchData);
    var viewModel = {
      "data": searchData,
      "unreadSearchMessagesCount": unreadSearchMessagesCount
    };
    self.presentAlerts({
      "searchRequestsViewModel": viewModel
    });
  };

  /**
   * Search the requests in Deleted Messages Tab
   * @param {String} searchString -  String to be searched in the data
   * @returns data which matches the search String
   * @throws Exception if something goes wrong. 
   */
  AlertsMsgs_PresentationController.prototype.searchDeletedRequests = function (searchString) {
    var self = this;
    var searchData = [];
    if(deletedRequests) {
      for (var i = 0; i < deletedRequests.length; i++) {
        if (deletedRequests[i].requestsubject.toLowerCase().indexOf(searchString.toLowerCase())>=0) {
                    searchData.push(requestsData[i]);
                }
      }
    }
    var unreadSearchMessagesCount = self.countUnreadMessages(searchData);
    var viewModel = {
      "data": searchData,
      "unreadSearchMessagesCount": unreadSearchMessagesCount
    };
    self.presentAlerts({
      "showSearchDeletedRequestsViewModel": viewModel
    });
  };
   /**
    * downloadAttachment :Downloading the attachment of the message
    * @member of {AlertsMsgs_PresentationController}
    * @param {String,String} mediaId and fileName which is to be downloaded 
    * @return {}
    * @throws {}
   */

  AlertsMsgs_PresentationController.prototype.downloadAttachment = function (mediaId, fileName) {
    var self = this;
    var params = {
      mediaId: mediaId,
      fileName: fileName
    };

    function downloadAttachmentCompletionCallBack(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {

        //dismissAlertsSuccessCallBack();
      } else {

        //dismissAlertsErrorCallBack();
      }
    }
    self.businessController.execute(new kony.mvc.Business.Command("com.kony.alertsmsgs.downloadAttachment", params, downloadAttachmentCompletionCallBack));
  };
  return AlertsMsgs_PresentationController;
});