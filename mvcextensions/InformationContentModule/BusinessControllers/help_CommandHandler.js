define([], function() {

  	function InformationContent_help_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(InformationContent_help_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	InformationContent_help_CommandHandler.prototype.execute = function(command){
      var self=this;
      var category = command.context;
  		function completionCallBack(status, response, error) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) 
              	self.sendResponse(command, status, response);
            else 
              	self.sendResponse(command, status, error);
        }

  		try{
  		 	var Informationcontent = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Informationcontent");
        	Informationcontent.customVerb("getFAQs", {categoryName : category} , completionCallBack);
    	}catch(e){
    		self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE,{});
    	}
		
    };
	InformationContent_help_CommandHandler.prototype.validate = function(){
		
    };
    
    return InformationContent_help_CommandHandler;
    
});