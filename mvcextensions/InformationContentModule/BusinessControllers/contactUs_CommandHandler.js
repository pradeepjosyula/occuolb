define([], function() {

  	function InformationContent_contactUs_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(InformationContent_contactUs_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	InformationContent_contactUs_CommandHandler.prototype.execute = function(command){
  		var self=this;
      	
  		function completionCallBack(status, response, error) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) 
              	self.sendResponse(command, status, response);
            else 
              	self.sendResponse(command, status, error);
        }

  		try{
  		 	var Informationcontent = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Informationcontent");
        	Informationcontent.customVerb("getContactUs", {}, completionCallBack);
    	}catch(e){
    		self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE,{});
    	}
		
    };
	
	InformationContent_contactUs_CommandHandler.prototype.validate = function(){
		
    };
    
    return InformationContent_contactUs_CommandHandler;
    
});