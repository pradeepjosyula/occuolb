define(['CommonUtilities'], function (CommonUtilities) {

  function InformationContent_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(InformationContent_PresentationController, kony.mvc.Presentation.BasePresenter);

  InformationContent_PresentationController.prototype.initializePresentationController = function () {

  };

  /**
  * Takes category name of help as input parameter and calls command handler for getting the response from back-end. Differentiates between pre-login and postlogin view.
  * @member of {InformationContent_PresentationController}
  * @param {type} param: category name for help
  * @returns {void} - None
  * @throws {void} -None
  */
  InformationContent_PresentationController.prototype.showOnlineHelp = function (param1) {
    if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
      this.presentUserInterface('frmOnlineHelp', { "showOnlineHelp": { param: "preLoginView" } });
    } else {
      this.presentUserInterface('frmOnlineHelp', { "showOnlineHelp": { param: param1 } });
    }
  };

  /**
  * Takes category name of help as input parameter and calls command handler for getting the response from back-end.
  * @member of {InformationContent_PresentationController}
  * @param {type} param: category name for help
  * @returns {void} - None
  * @throws {void} -None
  */
  InformationContent_PresentationController.prototype.showOnlineHelpSubmenu = function (param, isSearchString) {
    var self = this;
    var searchStr = param;
    var isSearchString = isSearchString;
    if(isSearchString === true){
      param = {}; 
    }
    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        if(isSearchString === true){
        data= response.data;
        param = searchStr;
        self.presentUserInterface('frmOnlineHelp', { "showOnlineHelpResponse": {responseData:data, responseParam:param} });
        }else{
        self.presentUserInterface('frmOnlineHelp', { "showOnlineHelpResponse": {responseData:response.data} });
        }
        }
      else {
        self.presentUserInterface('frmOnlineHelp', { "showOnlineHelpResponse": { status: "error" } });
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.InformationContent.help", param, completionCallback));


  };

  /**
  * Function create viewModel for contact us
  * @member of {InformationContent_PresentationController}
  * @param {JSONObject} data: Contact Us data
  * @returns {void} - None
  * @throws {void} -None
  */
  InformationContent_PresentationController.prototype.createShowContactUsViewModel = function (data) {
    var self = this;

    var viewModel = [];
    for (var i in data) {
      var email = [];
      var phone = [];
      var heading = data[i].serviceTitle;
      for (var j in data[i].Email) {
        email.push(data[i].Email[j].value);
      }
      for (j in data[i].Phone) {
        phone.push(data[i].Phone[j].value);
      }
      viewModel.push({
        Email: email,
        Phone: phone,
        heading: heading,
      });
    }
    
    if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
      self.presentUserInterface('frmContactUsPrivacyTandC', { "showContactUs": { contactUs: viewModel, param: "preLoginView" } });
    } else {
      self.loadComponents("frmContactUsPrivacyTandC", "Contact Us");
      self.presentUserInterface('frmContactUsPrivacyTandC', { "showContactUs": { contactUs: viewModel, param: "postLoginView" } });
    }

  };

  /**
  * Method to fetch contactUs information Show ContactUs Page
  * @member of Class InformationContent_PresentationController
  * @param {void} - none
  * @returns {void} - None
  * @throws {void} -None
  */
  InformationContent_PresentationController.prototype.showContactUsPage = function () {
    var self = this;
    if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
      this.presentUserInterface('frmContactUsPrivacyTandC', { viewType: "preLogin" });
    } else {
      this.presentUserInterface('frmContactUsPrivacyTandC', { viewType: "postLogin" });
    }
    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        var viewModel = self.createShowContactUsViewModel(response.data.records);
        self.presentUserInterface('frmContactUsPrivacyTandC', { "showContactUs": viewModel });
      } else {
        self.presentUserInterface('frmContactUsPrivacyTandC', { "showContactUs": { status: "error" } });
      }
    }
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.InformationContent.contactUs", {}, completionCallback));
  };


  /**
  * Method to fetch privacy policy information Show privacy policy
  * @member of Class InformationContent_PresentationController
  * @param {void} - none
  * @returns {void} - None
  * @throws {void} -None
  */

  InformationContent_PresentationController.prototype.showPrivacyPolicyPage = function () {
    
    if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged){
      this.presentUserInterface('frmContactUsPrivacyTandC', { "showLoadingIndicatorPrivacyPolicy": {view:"preLogin"} });
    }else{
      this.loadComponents('frmContactUsPrivacyTandC', "Privacy Policy");
      this.presentUserInterface('frmContactUsPrivacyTandC', { "showLoadingIndicatorPrivacyPolicy": {view:"postLogin"} });
    }

  };

  InformationContent_PresentationController.prototype.showPrivacyPolicyAfterLoading = function () {
    
    var self = this;


    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
          self.presentUserInterface('frmContactUsPrivacyTandC', { "showPrivacyPolicy": { serviceData: response.data } });
      } else {
        self.presentUserInterface('frmContactUsPrivacyTandC', { "showPrivacyPolicy": { serviceData: "error" } });
      }
    }

    this.businessController.execute(new kony.mvc.Business.Command("com.kony.InformationContent.privacyPolicy", {}, completionCallback));
  };

  /**
* Takes form name as input parameter and data and loads the view of form
* @member of {InformationContent_PresentationController}
* @param {type} param: form name and data
* @returns {void} - None
* @throws {void} -None
*/

  InformationContent_PresentationController.prototype.showView = function (frm, data) {
    this.presentUserInterface(frm, data);
  };

  /**
* Takes form name and viewmodel for form, as input parameter and data and loads the common components of form
* @member of {InformationContent_PresentationController}
* @param {type} param: form name and viewmodel
* @returns {void} - None
* @throws {void} -None
*/

  InformationContent_PresentationController.prototype.loadComponents = function (frm, viewModel) {
    var scopeObj = this;
    var howToShowHamburgerMenu = function (sideMenuViewModel) {
      scopeObj.showView(frm, { "sideMenu": sideMenuViewModel, "sideSubMenu": viewModel });
    };

    scopeObj.SideMenu.init(howToShowHamburgerMenu);

    var presentTopBar = function (topBarViewModel) {
      scopeObj.showView(frm, { "topBar": topBarViewModel });
    };
    scopeObj.TopBar.init(presentTopBar);
  };


  /**
* Method to fetch terms and conditions information terms and conditions
* @member of Class InformationContent_PresentationController
* @param {void} - none
* @returns {void} - None
* @throws {void} -None
*/
  InformationContent_PresentationController.prototype.showTermsAndConditions = function () {
    
    var self = this;
    if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
      this.presentUserInterface('frmContactUsPrivacyTandC', { viewType: "preLogin" });
    } else {
      this.presentUserInterface('frmContactUsPrivacyTandC', { viewType: "postLogin" });
    }

    function completionCallback(response) {
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        
        if (!kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged) {
          self.presentUserInterface('frmContactUsPrivacyTandC', { "showTnC": { serviceData: response.data, param: "preLoginView" } });
        } else {
          CommonUtilities.showProgressBar(this.view);
          self.loadComponents('frmContactUsPrivacyTandC', "Terms and Conditions");
          self.presentUserInterface('frmContactUsPrivacyTandC', { "showTnC": { serviceData: response.data, param: "postLoginView" } });
        }

      } else {
        self.presentUserInterface('frmContactUsPrivacyTandC', { "showTnC": { serviceData: "error" } });
      }
    }

    this.businessController.execute(new kony.mvc.Business.Command("com.kony.InformationContent.termsAndConditions", {}, completionCallback));
  };

  /**
* Method to fetch FAQ's information Show FAQs from footer link
* @member of Class InformationContent_PresentationController
* @param {void} - none
* @returns {void} - None
* @throws {void} -None
*/
  InformationContent_PresentationController.prototype.showFAQs = function () {
    
    this.showOnlineHelp();

  };


  /**
* Method to Initialize HamburgerMenu
* @member of Class InformationContent_PresentationController
* @param {String} formName - Takes name of form to initialize topBar and sideMenu
* @returns {void} - None
* @throws {void} -None
*/
  InformationContent_PresentationController.prototype.loadHamburger = function (frm) {
    var self = this;
    var howToShowHamburgerMenu = function (sideMenuViewModel) {
      self.presentUserInterface(frm, { sideMenu: sideMenuViewModel });
    };
    self.SideMenu.init(howToShowHamburgerMenu);

    var presentTopBar = function (topBarViewModel) {
      self.presentUserInterface(frm, { topBar: topBarViewModel });
    };
    self.TopBar.init(presentTopBar);
  };


  return InformationContent_PresentationController;
});