define([], function() {

  	function NUO_signatureUpload_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(NUO_signatureUpload_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	NUO_signatureUpload_CommandHandler.prototype.execute = function(command){
		var self = this;
        function completionCallback(status, data, error) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, data);
            } else {
                self.sendResponse(command, status, error);
            }
        }
        var newUserModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUser");
        newUserModel.customVerb('signatureUpload', command.context, completionCallback);
    };
	
	NUO_signatureUpload_CommandHandler.prototype.validate = function(){
		
    };
    
    return NUO_signatureUpload_CommandHandler;
    
});