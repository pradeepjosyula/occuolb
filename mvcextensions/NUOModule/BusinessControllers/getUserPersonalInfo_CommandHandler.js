define([], function () {

    function NUO_getUserPersonalInfo_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(NUO_getUserPersonalInfo_CommandHandler, kony.mvc.Business.CommandHandler);

    NUO_getUserPersonalInfo_CommandHandler.prototype.execute = function (command) {
        var self = this;
        function completionCallback(status, data, error) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, data);
            } else {
                self.sendResponse(command, status, error);
            }
        }
        var newUserModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUser");
        newUserModel.customVerb('getUserPersonalInfo', {}, completionCallback);
    };

    NUO_getUserPersonalInfo_CommandHandler.prototype.validate = function () {
    };

    return NUO_getUserPersonalInfo_CommandHandler;

});