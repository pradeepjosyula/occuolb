define([], function() {
    //eslint-disable-next-line
    function NUO_getUserSelectedProducts_CommandHandler(commandId) {
      kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(
      NUO_getUserSelectedProducts_CommandHandler,
      kony.mvc.Business.CommandHandler
    );
     //eslint-disable-next-line
    NUO_getUserSelectedProducts_CommandHandler.prototype.execute = function(command) {
      var self = this;
  
     //eslint-disable-next-line    
      function completionCallBack(status, response, err) {
        if (status === kony.mvc.constants.STATUS_SUCCESS) {
            if (response.success) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, true);
            }
            else if (response.errmsg) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, false);                
            }
        } else {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
      }
      try {
        var  NewUserModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUser");
        NewUserModel.customVerb("verifyExistingUserName", {userName: command.context}, completionCallBack)
      } catch (err) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    };
  
   //eslint-disable-next-line
    NUO_getUserSelectedProducts_CommandHandler.prototype.validate = function() {};
  
    return NUO_getUserSelectedProducts_CommandHandler;
  });
  