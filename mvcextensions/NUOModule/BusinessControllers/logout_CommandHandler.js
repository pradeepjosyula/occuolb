define([], function() {

  function NUO_logout_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(NUO_logout_CommandHandler, kony.mvc.Business.CommandHandler);

  NUO_logout_CommandHandler.prototype.execute = function(command){
    var self=this;
    var authParams = {
        "loginOptions": {
            "isOfflineEnabled": false
        }
    };
    function successCallback(resSuccess){
      self.sendResponse(command,kony.mvc.constants.STATUS_SUCCESS);
    }
    function errorCallback(resError){
      self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,resError);
    }
    try{
      nuoAuthClient = KNYMobileFabric.getIdentityService("NUOApplicantLogin");
      nuoAuthClient.logout(successCallback,errorCallback,authParams);
    }catch(err){
      kony.print(err);
      self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
    }
  };

  NUO_logout_CommandHandler.prototype.validate = function(){

  };

  return NUO_logout_CommandHandler;

});