define([], function () {

    function NUO_createPersonalInfo_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(NUO_createPersonalInfo_CommandHandler, kony.mvc.Business.CommandHandler);

    NUO_createPersonalInfo_CommandHandler.prototype.execute = function (command) {
        var self = this;
        /*
        var userInfo = {
            "dateOfBirth": command.context.dateOfBirth,
            "addressLine1": command.context.addressLine1,
            "addressLine2": command.context.addressLine2,
            "city": command.context.city,
            "country": command.context.country,
            "state": command.context.state,
            "zipcode": command.context.zipcode,
            "userfirstname": command.context.firstName,
            "userlastname": command.context.lastName,
            "maritalStatus": command.context.maritalStatus,
            "noOfDependents": command.context.noOfdependents,
            "spouseFirstName": command.context.spouseName,
            "gender": command.context.gender,
            "ssn": command.context.ssn,
            "informationType": command.context.informationType
        };
        userInfo = {
            "employmentInfo" : command.context.employmentStatus,
            "company" : command.context.company,
            "jobProfile" : command.context.jobTitle,
            "experience" : command.context.experience,
            "informationType": command.context.informationType
        };
        userInfo = {
            "annualIncome" : command.context.annualIncome,
            "assets" : command.context.assets,
            "montlyExpenditure" : command.context.monthlyExpenditure,
            "informationType": command.context.informationType
        };*/
        function completionCallback(status, data, error) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, data);
            } else {
                self.sendResponse(command, status, error);
            }
        }
        var newUserModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUser");
        newUserModel.customVerb('createPersonalInfo', command.context, completionCallback);
    };

    NUO_createPersonalInfo_CommandHandler.prototype.validate = function () {

    };

    return NUO_createPersonalInfo_CommandHandler;

});