define([], function() {

  	function NUO_getExistingUserPhone_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(NUO_getExistingUserPhone_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	NUO_getExistingUserPhone_CommandHandler.prototype.execute = function(command){
		var self = this;
    var params={
      "phone" : command.context,
    };
    function completionCallBack(status,response,err){
      if(status==kony.mvc.constants.STATUS_SUCCESS){
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS,response);
      }
      else {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS,err);
      }
    }

    try{
      var userModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUser");
      userModel.customVerb("verifyExistingUserByPhone",params,completionCallBack);
    }
    catch(err){
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
    };
	
	NUO_getExistingUserPhone_CommandHandler.prototype.validate = function(){
		
    };
    
    return NUO_getExistingUserPhone_CommandHandler;
    
});