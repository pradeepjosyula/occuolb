define([], function() {

  function NUO_login_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(NUO_login_CommandHandler, kony.mvc.Business.CommandHandler);

  NUO_login_CommandHandler.prototype.execute = function(command){
    var self=this;
    var authParams;
    if (command.context.sessiontoken) {
      authParams = {
        "session_token": command.context.sessiontoken,
      }
    } else {
      authParams = {
        "username": command.context.username,
        "password": command.context.password,
        "loginOptions": {
          "isOfflineEnabled": false
        }
      };
    }

    function successCallback(resSuccess){
      self.sendResponse(command,kony.mvc.constants.STATUS_SUCCESS,{
        username: kony.sdk.getCurrentInstance().tokens.NUOApplicantLogin.provider_token.params.user_attributes.userName
      });
    }
    function errorCallback(resError){
      self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,resError);
    }
    try{
      nuoAuthClient = KNYMobileFabric.getIdentityService("NUOApplicantLogin");
      nuoAuthClient.login(authParams,successCallback,errorCallback);
    }catch(err){
      kony.print(err);
      self.sendResponse(command,kony.mvc.constants.STATUS_FAILURE,err);
    }
  };

  NUO_login_CommandHandler.prototype.validate = function(){

  };

  return NUO_login_CommandHandler;

});