define([], function() {

  	function NUO_requestState_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(NUO_requestState_CommandHandler, kony.mvc.Business.CommandHandler);
  
  	NUO_requestState_CommandHandler.prototype.execute = function(command){
		var self = this;

        function completionCallBack(status, response, err) {
            if (status === kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
            } else {
                self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
            }
        }
        
            var newUserModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUser");
            newUserModel.customVerb("getUserState", {}, completionCallBack);
    };
	
	NUO_requestState_CommandHandler.prototype.validate = function(){
		
    };
    
    return NUO_requestState_CommandHandler;
    
});