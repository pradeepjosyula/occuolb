define([], function() {
    //eslint-disable-next-line
    function NUO_saveUserSelectedProducts_CommandHandler(commandId) {
      kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(
      NUO_saveUserSelectedProducts_CommandHandler,
      kony.mvc.Business.CommandHandler
    );
     //eslint-disable-next-line
    NUO_saveUserSelectedProducts_CommandHandler.prototype.execute = function(command) {
      var self = this;

      var data = command.context.products.map(function (product) {    
        return {
          product:  JSON.stringify({
            productTypeId: product.productTypeId,
            productName: product.productName,
            productType: product.productType,
            productId: product.productId
          })
        };
      })
  
     //eslint-disable-next-line    
      function completionCallBack(status, response, err) {
        if (status === kony.mvc.constants.STATUS_SUCCESS) {
          self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
        } else {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
      }
      try { 
        var  NewUserProductsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUserProducts");
        var newUserProducts = new NewUserProductsModel({
          // Dirty -> Improve with better version -> TODO Shivam
          productLi: JSON.stringify(data).replace(/\"/g, "'").replace(/\\\"/g, "\"").replace(/\\'/g, "\"")
        })
        newUserProducts.save(completionCallBack)  
      } catch (err) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    };
  
   //eslint-disable-next-line
    NUO_saveUserSelectedProducts_CommandHandler.prototype.validate = function() {};
  
    return NUO_saveUserSelectedProducts_CommandHandler;
  });
  