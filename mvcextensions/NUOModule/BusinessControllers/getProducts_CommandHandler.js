define([], function() {
  //eslint-disable-next-line
  function NUO_getProducts_CommandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(
    NUO_getProducts_CommandHandler,
    kony.mvc.Business.CommandHandler
  );
   //eslint-disable-next-line
  NUO_getProducts_CommandHandler.prototype.execute = function(command) {
    var self = this;

   //eslint-disable-next-line    
    function completionCallBack(status, response, err) {
      if (status == kony.mvc.constants.STATUS_SUCCESS) {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
      } else {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    }
    try {
      var  NewProductsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Products");
      NewProductsModel.customVerb('getProductList',{}, completionCallBack)
    } catch (err) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
    }
  };

 //eslint-disable-next-line
  NUO_getProducts_CommandHandler.prototype.validate = function() {};

  return NUO_getProducts_CommandHandler;
});
