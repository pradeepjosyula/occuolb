define([], function() {

  	function NUO_createUserForEnroll_CommandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(NUO_createUserForEnroll_CommandHandler, kony.mvc.Business.CommandHandler);
  
  NUO_createUserForEnroll_CommandHandler.prototype.execute = function(command){
    var self = this;
    function completionCallback(status,response,err){
      if(status==kony.mvc.constants.STATUS_SUCCESS){
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS,response);
      }
      else {
        self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS,err);
      }
    }

    try{
      var newUserModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUser");
          var userObject=new newUserModel();
          userObject.userName = command.context.username;
          userObject.password = command.context.password;
          userObject.phone = command.context.phone;
          userObject.email = command.context.email;
          
          userObject.save(completionCallback);
      
    }
    catch(err){
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }
  };

  NUO_createUserForEnroll_CommandHandler.prototype.validate = function(){

  };
    
    return NUO_createUserForEnroll_CommandHandler;
    
});