define([], function() {
    //eslint-disable-next-line
    function NUO_getUserSelectedProducts_CommandHandler(commandId) {
      kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(
      NUO_getUserSelectedProducts_CommandHandler,
      kony.mvc.Business.CommandHandler
    );
     //eslint-disable-next-line
    NUO_getUserSelectedProducts_CommandHandler.prototype.execute = function(command) {
      var self = this;
  
     //eslint-disable-next-line    
      function completionCallBack(status, response, err) {
        if (status == kony.mvc.constants.STATUS_SUCCESS) {
          self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
        } else {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
      }
      try {
        var  NewProductsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("NewUserProducts");
        NewProductsModel.getAll(completionCallBack)
      } catch (err) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    };
  
   //eslint-disable-next-line
    NUO_getUserSelectedProducts_CommandHandler.prototype.validate = function() {};
  
    return NUO_getUserSelectedProducts_CommandHandler;
  });
  