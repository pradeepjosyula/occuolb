define(["CommonUtilities"], function (CommonUtilities) {
    var isErrorState = false;
    var progressBarCount = 0;

    // eslint-disable-next-line
    function NUO_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }

    inheritsFrom(NUO_PresentationController, kony.mvc.Presentation.BasePresenter);

    // eslint-disable-next-line
    NUO_PresentationController.prototype.initializePresentationController = function () { };

    /**
     * Entry Point method for new user onboarding
     * @return None
     */
    NUO_PresentationController.prototype.showNewUserOnBoarding = function (context) {
        progressBarCount = 0;
        isErrorState = false;
        context = context || {
            NUOLanding: "true"
        }
        this.showNewUserOnBoardingBasedOnContext(context)
    };

    NUO_PresentationController.prototype.showNewUserOnBoardingBasedOnContext = function (context) {
        progressBarCount = 0;
        isErrorState = false;
        if (context.NUOLanding) {
            this.presentNUO({
                NUOLanding: {}
            })
        }
    }

    /**
     * Present new user onboarding screen
     * @param {JSON} data View Model For NUO form
     * @return
     */
    NUO_PresentationController.prototype.presentNUO = function (data) {
        if (!isErrorState) {
            this.presentUserInterface("frmNewUserOnboarding", data);
        }
    };

    /**
     * Show Error Screen
     * @returns none
     */
    NUO_PresentationController.prototype.showErrorScreen = function () {
        this.forceHideProgressBar();        
        isErrorState = true;
        this.showServerDownScreen();
    };
    NUO_PresentationController.prototype.showServerDownScreen = function () {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        //Presenting the Login form without a viewmodel first as a work around for preShow of form being called after willUpdateUI.
        authModule.presentationController.presentUserInterface('frmLogin', {});
        
        this.navigateToServerDownScreen();
    };
    NUO_PresentationController.prototype.navigateToServerDownScreen = function () {
        kony.store.setItem('OLBLogoutStatus', {
            action: "ServerDown"
        });
        this.doLogout();
    };
    
    /**
     * Shows Progress bar - Indicates Loading State
     * @returns None
     */
    NUO_PresentationController.prototype.showProgressBar = function () {
        var self = this;
        if (progressBarCount === 0) {
            self.presentNUO({
                showProgressBar: "showProgressBar"
            });
        }
        progressBarCount++;
    };

    /**
     * Hides Progress bar - Indicates Non Loading State
     * @returns None
     */

    NUO_PresentationController.prototype.hideProgressBar = function () {
        var self = this;
        if (progressBarCount > 0) {
            progressBarCount--;
        }
        if (progressBarCount === 0) {
            self.presentNUO({
                hideProgressBar: "hideProgressBar"
            });
        }
    };
    /**
     * Hides Progress bar - Indicates Non Loading State
     * @returns None
     */

    NUO_PresentationController.prototype.forceHideProgressBar = function () {
        var self = this;
        progressBarCount = 0;
            self.presentNUO({
                hideProgressBar: "hideProgressBar"
            });
        
    };




    /**
     * Show Server Error Flex on UI
     * @param errorMessage
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.showServerErrorFlex = function (
        errorMessage
    ) {
        this.hideProgressBar();
        this.presentWireTransfer({
            serverError: errorMessage
        });
    };

    /** 
     * Shows Step 2 of new user on boarding - Product Selection
     * @param {JSON} context Data from previous step.
     * @returns {void} - None
    */

    NUO_PresentationController.prototype.navigateToProductSelection = function (context) {
        context = context || {};        
        this.getAllProducts(context);
        this.hideProgressBar();
    };
    /** 
     * get all available products and sends them to UI
     * @param {JSON} context Data from previous step.
     * @returns {void} - None
    */

    NUO_PresentationController.prototype.getAllProducts = function (context) {
        var self = this;

        function convertToProducts (responseItem) {
            return JSON.parse(responseItem.products)
        }

        var userName=kony.sdk.getCurrentInstance().tokens.NUOApplicantLogin.provider_token.params.user_attributes.userName;        
        function completionCallback(responseList) {
            self.hideProgressBar();
            if (responseList[0].status === kony.mvc.constants.STATUS_SUCCESS && responseList[1].status === kony.mvc.constants.STATUS_SUCCESS) {
                self.presentNUO({
                    productSelection: {
                        products: responseList[0].data.records,
                        username: userName,
                        selectedProducts: responseList[1].data.map(convertToProducts),
                        isRevisiting: context.isRevisiting
                    }
                })
            }
            else{
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        var commands = [new kony.mvc.Business.Command("com.kony.nuo.getProducts", {}), new kony.mvc.Business.Command("com.kony.nuo.getUserSelectedProducts", {})]
        kony.mvc.util.ParallelCommandExecuter.executeCommands(this.businessController, commands, completionCallback)
    };
    /** 
     * Save user products
     * @param {JSON} data data from UI
     * @returns {void} - None
    */

    NUO_PresentationController.prototype.saveUserProducts = function (productList) {
        var self = this;
        function completionCallback(response) {
            self.hideProgressBar();                            
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.navigateToUserInformationStep();
            }
            else{
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.saveUserSelectedProducts", {products: productList} , completionCallback));
        
    };
    /**
     * Checks the Existance of phone number
     * @param phoneNumber
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.checkPhoneNumber = function (phoneNumber) {
        var self = this;
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                if (response.data.success) {
                    self.createNewCustomer(phoneNumber);
                    return;
                }
                if (response.data.errmsg) {
                    self.hideProgressBar();
                    self.continueApplication(phoneNumber);
                    return;
                }
                // Check required for Already Enrolled Customer
                // If Check Available from Backend then call self.customerExists
            }
            else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.getExistingUserPhone", phoneNumber, completionCallback));
    };

    /**
    * Continue the application - User exists as new applicant
    * @param phoneNumber
    * @returns {void} - None
    * @throws {void} - None
    */

    NUO_PresentationController.prototype.continueApplication = function (phoneNumber) {
        this.presentNUO({
            continueApplication: {
                phoneNumber: phoneNumber
            }
        });
    } 

    /**
    * Notify UI that User exists as a Fully Enrolled Customer 
    * @param phoneNumber
    * @returns {void} - None
    * @throws {void} - None
    */


    NUO_PresentationController.prototype.customerExists = function (phoneNumber) {
        this.presentNUO({
            customerExists: {
                phoneNumber: phoneNumber
            }
        });
    }

    /**
    * User Already Exists - Requests OTP
    * @param phoneNumber
    * @returns {void} - None
    * @throws {void} - None
    */

    NUO_PresentationController.prototype.createNewCustomer = function (phoneNumber) {
        var self = this;
        self.requestOTP(function () {
            self.hideProgressBar();
            self.presentNUO({
                createNewCustomer: {
                    phoneNumber: phoneNumber
                }
            });
        })

    }

    /**
     * requests for the OTP
     * @param successCallBack Optional success call back - If not Specified it will contninue new application flow
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.requestOTP = function (successCallBack) {
       
        var self = this;

        var defaultOnSuccess = function (response) {
            self.hideProgressBar();
            self.presentNUO({ OTPRequested: response.data });
        }

        function completionRequestOTPCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                if (successCallBack) {
                    successCallBack(response.data);
                }
                else {
                    setTimeout(function() {
                        defaultOnSuccess(response.data);                        
                    }, 5000)
                }
            }
            else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }

        }
        if (!successCallBack) {
            self.showProgressBar();
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.requestOTP", {}, completionRequestOTPCallback));
    };

    /**
     * Validates the OTP
     * @param OTP
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.validateOTP = function (otp) {
        var self = this;
        var otpJSON = {
            "otp": otp
        };
        function completionOTPValidateCallback(response) {
            self.hideProgressBar();
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.presentNUO({ OTPValidated: response.data });
            }
            else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.validateOTP", otpJSON, completionOTPValidateCallback));
    };

    /**
     * Checks user name availibility and creates if username is available.
     * @param usernamePasswordJSON - JSON object of new user details
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.enrollUser = function (usernamePasswordJSON) {
      var self = this;
      self.showProgressBar();
      this.checkUserName(usernamePasswordJSON.username, this.createUser.bind(this, usernamePasswordJSON), function () {
        self.hideProgressBar();
        self.presentNUO({
            enrollError: 'userNameError' 
        })
      })
    };

     /**
     * Create user
     * @param usernamePasswordJSON - JSON object of new user details
     * @returns {void} - None
     * @throws {void} - None
     */

    NUO_PresentationController.prototype.createUser = function (usernamePasswordJSON) {
        var self = this;
        self.showProgressBar();
        function completionLoginCallback(response) {
            self.hideProgressBar();
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.doLogin(usernamePasswordJSON, self.navigateToProductSelection.bind(self), self.showErrorScreen.bind(self));
            }
            else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen(response);
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.createUserForEnroll", usernamePasswordJSON, completionLoginCallback));
    };
    /**
     * check username
     * @param usernamePasswordJSON - JSON object of new user details
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.checkUserName = function (username, successCallback, errorCallback) {
        var self = this;
        function completionLoginCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) { 
                if (response.data) {
                    successCallback();                    
                }
                else {
                    errorCallback();
                }
            }
            else {
                self.showErrorScreen();
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.checkUserName", username, completionLoginCallback));
    };

    /**
     * login for a new User
     * @param usernamePasswordJSON - JSON object of new user credentials
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.doLogin = function (usernamePasswordJSON, successCallBack, errorCallback) {
        var self = this;
        var params = {
            "username": usernamePasswordJSON.username,
            "password": usernamePasswordJSON.password
        }
        function completionLoginCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.registerIdleTimeout", {}, self.doLogout.bind(self)));
                successCallBack(response.data);
            }
            else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                errorCallback(response.data);
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.login", usernamePasswordJSON, completionLoginCallback));
    };
    /**
     * login for a new User
     * @param usernamePasswordJSON - JSON object of new user credentials
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.doCustomerLogin = function (password) {
        var self = this;
        var params = {
            "username": kony.sdk.getCurrentInstance().tokens.NUOApplicantLogin.provider_token.params.user_attributes.userName,
            "password":password
        }
        self.showProgressBar();
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.onLogin(params,authModule.presentationController.setIdleTimeout.bind(authModule.presentationController), function (loginResponse) {
            self.hideProgressBar();
            self.presentNUO({
                customerLoginError: loginResponse.data.details.errmsg
            })
        });
    };

    NUO_PresentationController.prototype.navigateToAccounts = function () {
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.showAccountsDashboard();
    }

    /**
     * Refreshes the application
     * @param usernamePasswordJSON - JSON object of new user credentials
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.doLogout = function () {
        var self = this;
        function completionLogoutCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.deRegisterIdleTimeout();
            }
            else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.logoutErrorCallback();
            }
        }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.logout", {}, completionLogoutCallback));
    };
    /**
    * Used to show the error downtime screen when the logout fails
    * @member of {NUO_PresentationController}
    * @return {}
    * @throws {}
   */
    NUO_PresentationController.prototype.logoutErrorCallback = function() {        
        var context = {
            "action": "ServerDown"
        };
        kony.store.setItem('OLBLogoutStatus', context);
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        //Presenting the Login form without a viewmodel first as a work around for preShow of form being called after willUpdateUI.
        authModule.presentationController.presentUserInterface('frmLogin', {});

        authModule.presentationController.presentUserInterface('frmLogin', context);
       
    };
    /**
    * deRegisterIdleTimeout : used to deregister the idle TimeOut 
    * @member of {NUO_PresentationController}
    * @param {context}
    * @return {}
    * @throws {}
   */
    NUO_PresentationController.prototype.deRegisterIdleTimeout = function () {
        var self = this;

        function deregisterCompletionCallback(response) {
            //deregister doesnt need any response.
            self.deregisterSuccessCallback();
        }
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.auth.deregisterIdleTimeout", {}, deregisterCompletionCallback));
    };
    /**
    * deregisterSuccessCallback : used to deregister when idle TimeOut occurs
    * @member of {NUO_PresentationController}
    * @param {}
    * @return {}
    * @throws {}
   */
    NUO_PresentationController.prototype.deregisterSuccessCallback = function () {
        window.location.reload();
    };
    /**
     * Entry method to User Information Step(Step 3)
     * @member NUO_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.navigateToUserInformationStep = function () {
        this.presentNUO({
            "showUserInformationStep" : "showUserInformationStep"
        });
    };

    /**
     * Entry method to Employment Information Step(Step 4)
     * @member NUO_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.navigateToEmploymentInformationStep = function () {
        this.presentNUO({
            "showEmploymentInformationViewModel" : "showEmploymentInformationViewModel"
        });
    };

    /**
     * Entry method to Financial Information Step(Step 5)
     * @member NUO_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.navigateToFinancialInformationStep = function () {
        this.presentNUO({
            "showFinancialInformationViewModel" : "showFinancialInformationViewModel"
        });
    };

    /**
     * Method to create user information 
     * @member NUO_PresentationController
     * @param {Object} - userInfo contains the details of the user 
     * @param {Object} - context which indicates whether to logout
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.createUserInformation = function (userInfo, context) {
        var self = this;
        var userInfo = userInfo;
        var context = context;
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.hideProgressBar();
                if(context && context.action === "saveAndClose") {
                    self.doLogout();
                } else {
                    if (userInfo.informationType === "PersonalInfo") {
                        self.navigateToEmploymentInformationStep();
                    } else if (userInfo.informationType === "EmploymentInfo") {
                        self.navigateToFinancialInformationStep();
                    } else {
                        self.navigateToUploadDocumentsStep();
                    }
                }
            } else if (response.status == kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.createPersonalInfo", userInfo, completionCallback));
    };

    /**
     * Method to get user information 
     * @member NUO_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.getUserInformation = function (viewModelName) {
        var self = this;
        var viewModelName = viewModelName;
        var viewModel = {};
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.hideProgressBar();
                viewModel[viewModelName] = {
                    data: response.data[0]
                };
                self.presentNUO(viewModel);
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.getUserPersonalInfo", {}, completionCallback));
    };

    /**
     * Entry point function to show documents upload screen
     * @member NUO_PresentationController
     * @param {void} employmentStatus 
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.navigateToUploadDocumentsStep = function () {
        var self = this;
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.hideProgressBar();
                self.presentNUO({
                    showUploadDocuments :{
                        employementStatus: response.data[0].employmentInfo
                    } 
                })
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.getUserPersonalInfo", {}, completionCallback));
    };

    /**
     * Upload documents
     * @member NUO_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.uploadDocuments = function (files) {
        var self = this;
        var addressProof={document:files.addressProof,
                          documentType:"Address"};
        var employmentProof={document:files.employmentProof,
                             documentType:"Employment"};
        var incomeProof={document:files.incomeProof,
                         documentType:"Income"};
        // function completionCallback(responseList) {
        //     if (responseList[0].status === kony.mvc.constants.STATUS_SUCCESS && responseList[1].status === kony.mvc.constants.STATUS_SUCCESS && responseList[2].status === kony.mvc.constants.STATUS_SUCCESS) {
        //         self.hideProgressBar();
        //         self.presentNUO({
        //             documentsSaved :"documentsSaved"
        //         })                
        //     } else {
        //         self.showErrorScreen();
        //     }
        // }
        var addressFlag=false;
        var employementFlag=false;
        var incomeFlag=false;
        var count=0;
        function completionCallbackAP(response) {            
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                addressFlag=true;
            }
            else{
                count++;
            }
            finalCallback();
        }
        function completionCallbackEP(response) {            
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                employementFlag=true;
            }
            else{
                count++;
            }
            finalCallback();
        }
        function completionCallbackIP(response) {            
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                incomeFlag=true;
            }
            else{
                count++;
            }
            finalCallback();
        }
        function finalCallback(){
            if(addressFlag && employementFlag && incomeFlag){
                self.hideProgressBar();
                self.presentNUO({
                    documentsSaved :"documentsSaved"
                })  
            }else if(count===3){
                self.showErrorScreen();
            }   
        }  
        self.showProgressBar();
        //Adding comments for a known MF issue
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.uploadDocument", addressProof, completionCallbackAP));
        console.log("..");
        console.log("..");
        console.log("..");
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.uploadDocument", employmentProof, completionCallbackEP));
        console.log("..");
        console.log("..");
        console.log("..");
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.uploadDocument", incomeProof, completionCallbackIP));

        // var commands = [new kony.mvc.Business.Command("com.kony.nuo.uploadDocument", addressProof),
        //                 new kony.mvc.Business.Command("com.kony.nuo.uploadDocument", employmentProof),
        //                 new kony.mvc.Business.Command("com.kony.nuo.uploadDocument", incomeProof)];
        // kony.mvc.util.ParallelCommandExecuter.executeCommands(this.businessController, commands, completionCallback)
    };

    /**
     * Upload documents
     * @member NUO_PresentationController
     * @param {void} employmentStatus 
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.getIdentityVerificationQuestion = function (context) {
        var self = this;
        self.presentNUO({
            identityVerificationQuestions : "identityVerificationQuestions"
        });
        
        //To do - Integrate with actual service to fetch SSN security questions
        function completionCallback(response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.hideProgressBar();
                self.presentNUO({
                    identityVerificationQuestions : response.questions
                })      
            }
        }
        //self.showProgressBar();
        //Call service to fetch Identity Verification Questions
    };          
NUO_PresentationController.prototype.getUserState = function (successCallBack) {
    var self = this;
    function completionCallback (response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
            successCallBack(response.data[0]);
        } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
            self.showErrorScreen();
        }
    }
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.requestState", {}, completionCallback));
    };

    /**
     * Method to get user information 
     * @member NUO_PresentationController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    NUO_PresentationController.prototype.loginAndContinueApplication = function (userNameAndPassword) {
        var self = this;
        self.showProgressBar();
        self.doLogin(userNameAndPassword, function (loginResponse) {
            self.getUserState(self.continueOrResetApplication.bind(self, loginResponse))
        },
            self.showLoginError.bind(this)
        )
    };


    NUO_PresentationController.prototype.showLoginError = function (loginErrorResponse) {
        this.hideProgressBar();
        this.presentNUO({
            loginError: loginErrorResponse.details.message
        })
    }

    function isApplicationComplete (userState) {
        var keys = ["userProducts", "userPersonalInfo", "userEmploymentInfo", "userFinancialInfo", "userSecurityQuestions"];
        var factor = 0;
        keys.forEach(function (key) {
            if (userState[key] === "true") {
                factor ++;
            }
        })
        if (factor === keys.length) {
            return true;
        }
        return false;
    }

    NUO_PresentationController.prototype.continueOrResetApplication = function (userDetails, userState) {
        this.hideProgressBar();
        if (isApplicationComplete(userState)) {
            this.presentNUO({
                uploadSignature: {}
            })
        }
        else {
            this.presentNUO({
                continueOrResetApplication: {
                    progress: this.calculateProgress(userState),
                    userState: userState,
                    userDetails: userDetails
                }
            })
        }
       
    }
    NUO_PresentationController.prototype.findStageAndContinueApplication = function (userDetails, userState) {
        if (userState.userProducts === "false") {
            this.navigateToProductSelection({isRevisiting: true});
        } else if (userState.userPersonalInfo === "false") {
            this.navigateToUserInformationStep();
        } else if (userState.userEmploymentInfo === "false") {
            this.navigateToEmploymentInformationStep();
        } else if (userState.userFinancialInfo === "false") {
           this.navigateToFinancialInformationStep();     
        } else if(userState.userSecurityQuestions === "false" ) {
            this.navigateToUploadDocumentsStep();
        } else if (userState.creditCheck === "false") {
            this.navigateToCreditCheckError(); 
        }
        else {
            this.navigateToCreditCheckSuccess();
        }
    }
    NUO_PresentationController.prototype.navigateToCreditCheckError = function () {
        this.presentNUO({
            userCreditCheckError: {}
        })
    }

    NUO_PresentationController.prototype.navigateToCreditCheckSuccess = function () {
        this.presentNUO({
            userCreditCheckSuccess: {}
        })
    }

    NUO_PresentationController.prototype.calculateProgress = function (userState) {
        var factor = 1;
        var keys = ["userProducts", "userPersonalInfo", "userEmploymentInfo", "userFinancialInfo", "userSecurityQuestions", "creditCheck"];
        keys.forEach(function (key) {
            if (userState[key] === "true") {
                factor ++;
            }
        })
        return  ( factor / (keys.length + 1) ) * 100;
    }
    NUO_PresentationController.prototype.resetApplication = function (userDetails, userState) {
        var self = this;
        function completionCallback (response) {
            self.hideProgressBar();
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.navigateToProductSelection();
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.resetNewUserData", {}, completionCallback));
    }
    NUO_PresentationController.prototype.doIdentityVerification = function (response) {
        var self = this;
        
        function completionCallback(response) {
            self.hideProgressBar();
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.presentNUO({
                    identityVerificationSuccess : "identityVerificationSuccess"
                })      
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.presentNUO({
                    identityVerificationError : response
                })
            }
        }
        self.showProgressBar();
        //As of now calling this service to update "UserSecurityQuestion" Flag
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.createPersonalInfo", {"informationType": "SecurityQuestions"}, completionCallback));
        //Call here actual service to do Identity Verification 
    }
    NUO_PresentationController.prototype.userCreditCheck = function () {
        var self = this;
        function completionCallback (response) {
            self.hideProgressBar();
            if (response.status === kony.mvc.constants.STATUS_SUCCESS && response.data[0].ssn) {
                self.doUserCreditCheckWithSSN(response.data[0].ssn);
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.getUserPersonalInfo", {}, completionCallback));
    }
    NUO_PresentationController.prototype.doUserCreditCheckWithSSN = function (ssn) {
        var self = this;
        function completionCallback (response) {
            self.hideProgressBar();
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.presentNUO({
                    userCreditCheckSuccess : "userCreditCheckSuccess"
                })
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.presentNUO({
                    userCreditCheckError : "userCreditCheckError"
                })
            }
        }
        self.showProgressBar();
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.creditCheck", {ssn:ssn}, completionCallback));
    }    
    NUO_PresentationController.prototype.uploadSignature = function (file) {
        var self = this;        
        function completionCallback (response) {
            self.hideProgressBar();
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.presentNUO({
                    uploadSignatureSuccess : "uploadSignatureSuccess"
                })
            } else if (response.status === kony.mvc.constants.STATUS_FAILURE) {
                self.showErrorScreen();
            }
        }
        self.showProgressBar();
        self.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.signatureUpload", {signatureImage:"abc"}, completionCallback));
    };

    
   NUO_PresentationController.prototype.showEnrollForm = function(){
     var self=this;
    function completionCallBack(response){
       if(response.status === kony.mvc.constants.STATUS_SUCCESS) {
          kony.mvc.MDAApplication.getSharedInstance().appContext.nuorules=response.data.records;
          self.hideProgressBar();
          self.presentNUO({
                    usernamepasswordrules : response.data.records
                })
       }
      else{
         self.showErrorScreen();
      }
    }
    self.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.nuo.getPasswordPolicies", {}, completionCallBack));

   };
    return NUO_PresentationController;
});
