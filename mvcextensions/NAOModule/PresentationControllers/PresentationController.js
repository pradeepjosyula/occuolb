define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {

    var isErrorState = false;
    var progressBarCount = 0;

    function NAO_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }

    inheritsFrom(NAO_PresentationController, kony.mvc.Presentation.BasePresenter);

    NAO_PresentationController.prototype.initializePresentationController = function() {
        
    };

     /**
     * Entry Point method for new account opening
     * @return None
     */
    NAO_PresentationController.prototype.showNewAccountOpening = function (context) {
        progressBarCount = 0;
        isErrorState = false;
        context = context || {
            NUOLanding: "true"
        }
        this.getProducts();
        this.presentNAO({
            resetForm: {}
        });
        this.loadComponents();
        // this.showNewUserOnBoardingBasedOnContext(context)
    };

    function filterProductsForNao (product) {
        var accountTypes = [
            OLBConstants.ACCOUNT_TYPE.SAVING,
            OLBConstants.ACCOUNT_TYPE.CHECKING,
            OLBConstants.ACCOUNT_TYPE.CREDITCARD,
            "Credit Card"  // Hard coding due to inconsistency between OLB and Admin console systems. 
        ];
        return accountTypes.indexOf(product.productType) > -1
    }



    NAO_PresentationController.prototype.getProducts = function () {
        var self = this;
        function completionCallback (response) {
            if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
                self.hideProgressBar();                
                self.presentNAO({
                    productSelection: {
                        products: response.data.records.filter(filterProductsForNao)
                    }
                })
            }
        }
        self.showProgressBar();
        this.businessController.execute(new kony.mvc.Business.Command("com.kony.nao.getProducts", {} , completionCallback));
    }

    /**
     * Shows Progress bar - Indicates Loading State
     * @returns None
     */
    NAO_PresentationController.prototype.showProgressBar = function () {
        var self = this;
        if (progressBarCount === 0) {
            self.presentNAO({
                showProgressBar: "showProgressBar"
            });
        }
        progressBarCount++;
    };

    /**
     * Hides Progress bar - Indicates Non Loading State
     * @returns None
     */

    NAO_PresentationController.prototype.hideProgressBar = function () {
        var self = this;
        if (progressBarCount > 0) {
            progressBarCount--;
        }
        if (progressBarCount === 0) {
            self.presentNAO({
                hideProgressBar: "hideProgressBar"
            });
        }
    };

    /**
     * Present new user onboarding screen
     * @param {JSON} data View Model For NUO form
     * @return
     */
    NAO_PresentationController.prototype.presentNAO = function (data) {
        if (!isErrorState) {
            this.presentUserInterface("frmNAO", data);
        }
    };

    /**
     * Show Server Error Flex on UI
     * @param errorMessage
     * @returns {void} - None
     * @throws {void} - None
     */
    NAO_PresentationController.prototype.showServerErrorFlex = function (
        errorMessage
    ) {
        this.hideProgressBar();
        this.presentNAO({
            serverError: errorMessage
        });
    };

    /**
     * Show Error Screen
     * @returns none
     */
    NAO_PresentationController.prototype.showErrorScreen = function () {
        isErrorState = true;
        CommonUtilities.showServerDownScreen();
    };

    /** 
     * Save user products
     * @param {JSON} data data from UI
     * @returns {void} - None
    */

   NAO_PresentationController.prototype.saveUserProducts = function (productList) {
    var self = this;
    function completionCallback(response) {
        if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
            self.hideProgressBar();                
            self.performCreditCheck(productList);
        }
        else{
            self.showErrorScreen();
        }
    }
    self.showProgressBar();
    this.businessController.execute(new kony.mvc.Business.Command("com.kony.nao.createAccountsFromProducts", {products: productList} , completionCallback));
    
};


    NAO_PresentationController.prototype.performCreditCheck = function (productList) {
        this.showAcknowledgement(productList); 
    }
    NAO_PresentationController.prototype.showAcknowledgement = function (productList) {
        this.presentNAO({
            showAcknowledgement: {
                selectedProducts: productList
            }
        })
    }

    NAO_PresentationController.prototype.loadNAOComponents = function () {
        this.loadComponents();
      };
    
      /*
       * Function to load components
       * @param : form
       */
      NAO_PresentationController.prototype.loadComponents = function (data) {
        var self = this;
        var howToShowHamburgerMenu = function (sideMenuViewModel) {
          self.presentNAO({
            "sideMenu": sideMenuViewModel
          });
        };
        self.SideMenu.init(howToShowHamburgerMenu);
    
        var presentTopBar = function (topBarViewModel) {
          self.presentNAO({
            "topBar": topBarViewModel
          });
        };
        self.TopBar.init(presentTopBar);
      };

    return NAO_PresentationController;
});