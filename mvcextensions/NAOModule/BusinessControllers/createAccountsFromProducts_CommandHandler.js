define([], function() {
    //eslint-disable-next-line
    function NAO_createAccountsFromProducts_CommandHandler(commandId) {
      kony.mvc.Business.CommandHandler.call(this, commandId);
    }
  
    inheritsFrom(
      NAO_createAccountsFromProducts_CommandHandler,
      kony.mvc.Business.CommandHandler
    );
     //eslint-disable-next-line
    NAO_createAccountsFromProducts_CommandHandler.prototype.execute = function(command) {
      var self = this;

      var data = command.context.products.map(function (product) {
        return {
          product:  JSON.stringify({
            productId: product.productId,
            productTypeId: product.productTypeId,
            productName: product.productName,
            productType: product.productType            
          })
        };
      })
  
     //eslint-disable-next-line    
      function completionCallBack(status, response, err) {
        if (status === kony.mvc.constants.STATUS_SUCCESS) {
          self.sendResponse(command, kony.mvc.constants.STATUS_SUCCESS, response);
        } else {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
        }
      }
      try {
        var  AccountsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
        var params = {
          // Dirty -> Improve with better version -> TODO Shivam
          productLi: JSON.stringify(data).replace(/\"/g, "'").replace(/\\\"/g, "\"").replace(/\\'/g, "\"")
        }
        AccountsModel.customVerb("newAccountOpening", params, completionCallBack)  
      } catch (err) {
        self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, err);
      }
    };
  
   //eslint-disable-next-line
    NAO_createAccountsFromProducts_CommandHandler.prototype.validate = function() {};
  
    return NAO_createAccountsFromProducts_CommandHandler;
  });
  