define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {
	
 function PersonalFinanceManagement_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
		this.initializePresentationController();
    }
    inheritsFrom(PersonalFinanceManagement_PresentationController, kony.mvc.Presentation.BasePresenter);
    /**
     * initializePresentationController - Method to intialize Presentation controller data , called by constructor
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {Object} dataInputs - dataInputs to configure Presentation controller. (optional) - useful when we need customize.
     * @returns {}
     * @throws {}
     */
    PersonalFinanceManagement_PresentationController.prototype.initializePresentationController = function() {
        var scopeObj = this;
        scopeObj.viewModel = {};
        scopeObj.serverErrorVieModel = 'serverError';
		scopeObj.sortUnCatTranascionsConfig = {
            'sortBy': 'transactionDate',
            'defaultSortBy': 'transactionDate',
            'order': OLBConstants.DESCENDING_KEY,
            'defaultOrder': OLBConstants.DESCENDING_KEY
        };
    };
	
	 /**
     * naviageteToAccountLandingPage : used to naviagte the dashboard page.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {} //sending particular month
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.naviageteToAccountLandingPage = function() {
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountsModule.presentationController.showAccountsDashboard();
    };
	
    /**
     * presentPFM : Method for used to initalize the form
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /viewmodel for form
     * @returns {} get all list of Years.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.initPFMForm = function(data) {
        var date = new Date();
        var self = this;
        self.loadPFMComponents();
        self.showProgressBar();
        var viewModel = {};
		viewModel.hideflex = true;
        viewModel.getYears = self.getPFMYears();
        viewModel.getMonths = self.getPFMMonthsByYear(date.getFullYear());
		viewModel.showMonthlyDonutChart = true;
        self.getMonthlySpending(true,date.getMonth(),date.getFullYear(),viewModel);
        self.getYearlySpending(date.getFullYear());
        self.getPFMRelatedAccounts(date.getMonth(),date.getFullYear());
        self.getPFMBudgetChart(date.getMonth(),date.getFullYear());
    };

	/**
     * formatMonthlyCategorizedTransactions : used to format the Monthly Categorized transactions.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /get monthly data
     * @returns {} 
     * @throws {} 
     */
        PersonalFinanceManagement_PresentationController.prototype.formatMonthlyCategorizedTransactions = function(data) {
            if (data.length != 0) {
                var categoryIds = [];
                var catgoriesArray = [];
                for (var i = 0; i < data.length; i++) {
                    var totalAmount = 0.0;
                    var transaction = data[i];
                    if (categoryIds.indexOf(transaction.categoryId) == -1) {
                        var categoryobj = {};
                        var headerObj = {};
                        var transactionobj = {};
                        var categoryArray = [];
                        categoryIds.push(transaction.categoryId);
                        headerObj.categoryName = transaction.categoryName;
                        headerObj.categoryColor = OLBConstants.CATEGORIES[transaction.categoryName];
                        for (var j = 0; j < data.length; j++) {
                            if (data[j].categoryId == data[i].categoryId) {
                                totalAmount += parseFloat(data[j].transactionAmount);
                                headerObj.totalAmount = totalAmount;
                                data[j].transactionDate = CommonUtilities.getFrontendDateString(data[j].transactionDate);
                                data[j].displayAmount = CommonUtilities.formatCurrencyWithCommas(data[j].transactionAmount)
                                categoryArray.push(data[j]);
                            }
                        }
                        transactionobj.categoryArray = categoryArray;
                        headerObj.totalAmount = CommonUtilities.formatCurrencyWithCommas(headerObj.totalAmount);
                        categoryobj.header = headerObj;
                        categoryobj.transactionobj = categoryArray;
                        catgoriesArray.push(categoryobj);
                    } else {}
                }
                return catgoriesArray;
            } else {}
        }
  
    /**
     * getMonthlyCategorizedTransactions : used to get Monthly Categorized transactions.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /get monthly data
     * @returns {} 
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.getMonthlyCategorizedTransactions = function(monthId) {
            var self = this;
            self.showProgressBar();
            if(!monthId)
            {
                var date = new Date();
                monthId = date.getMonth();
            }
            var reqObject = {
                'monthId': monthId
            };

            function completionCallback(response) {
                if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                    var viewModel = {};
                    viewModel.showMonthlyCategorizedTransactions = true;
                    viewModel.monthlyCategorizedTransactions = self.formatMonthlyCategorizedTransactions(response.data);
                    self.presentPFM(viewModel);
                } else {
                    self.presentPFM({onServerDownError : true});
                }
            }
            self.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.getMonthlyCategorizedTransactions', reqObject, completionCallback));
        }

    /**
     * formatPFMBudgetChartData : used to format the data.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /get All monthly data
     * @returns {} get all list of months.
     * @throws {} 
     */
      PersonalFinanceManagement_PresentationController.prototype.formatPFMBudgetChartData = function(yearsPFMBudgetdata) {
        function addRequirePFMBudgetChartFields(yearBudget) {
			var leftBudget = (Number(yearBudget.allocatedAmount) - Number(yearBudget.amountSpent));
            yearBudget.categoryName = yearBudget.categoryName;
            yearBudget.budget = Number(yearBudget.amountSpent);
            yearBudget.budgetColorCode = OLBConstants.CATEGORIES[yearBudget.categoryName];
            yearBudget.budgetAnnotationText = "";
            yearBudget.tooltipText = CommonUtilities.formatCurrencyWithCommas(yearBudget.amountSpent) +"  " +kony.i18n.getLocalizedString("i18n.common.usedfrom") +"  " +CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
            yearBudget.remaningBuget = leftBudget;
            if(leftBudget<0)
            {
              yearBudget.remaningBuget = 0;
              yearBudget.tooltipText = CommonUtilities.formatCurrencyWithCommas(Math.abs(leftBudget)) +  "  " +kony.i18n.getLocalizedString("i18n.common.over") +"  "+ CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
            }
            else
            {
                yearBudget.remaningBuget = leftBudget;
                yearBudget.tooltipText = CommonUtilities.formatCurrencyWithCommas(yearBudget.amountSpent) + "  " + kony.i18n.getLocalizedString("i18n.common.usedfrom") + "  " + CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
            }
            yearBudget.remainingBudgeTooltipText = CommonUtilities.formatCurrencyWithCommas(leftBudget) + "  " + kony.i18n.getLocalizedString("i18n.common.leftfrom") + "  " + CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
            yearBudget.remaningBugetColorCode = "color:" + OLBConstants.CATEGORIES[yearBudget.categoryName] + ";" + "opacity:0.3";
            yearBudget.remaingBudgetAnnotationText = "";
            yearBudget.remainingBudgeTooltipText= CommonUtilities.formatCurrencyWithCommas(leftBudget)+"  " + kony.i18n.getLocalizedString("i18n.common.leftfrom")+"  "  + CommonUtilities.formatCurrencyWithCommas(yearBudget.allocatedAmount);
            
            return yearBudget;
        }
        var pfmBudgetData = yearsPFMBudgetdata.map(addRequirePFMBudgetChartFields);
        return pfmBudgetData;
    };
    /**
     * getPFMBudgetChart : Method for used to draw budget chart.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {year,viewModel} /get All monthly data
     * @returns {} get all list of months.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.getPFMBudgetChart = function(monthId,year) {
            var self = this;
            self.showProgressBar();
            var reqObject = {
                'year': year,
                'monthId':monthId
            };

            function completionCallback(response) {
                if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                        var viewModel = {};
                        viewModel.showYearlyBudgetChart = true;
                        viewModel.yearlyBudgetData = self.formatPFMBudgetChartData(response.data);
                        self.presentPFM(viewModel);
                } else {
                    self.presentPFM({onServerDownError : true});
                }
            }
            self.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.getYearlyBudgetData', reqObject, completionCallback));
        }
        /**
         * presentPFM : Method for used to Format the Chart Data.
         * @member of {PersonalFinanceManagement_PresentationController}
         * @param {monthlyData} /get All monthly data
         * @returns {} get all list of months.
         * @throws {} 
         */
    PersonalFinanceManagement_PresentationController.prototype.formatPFMDonuctChartData = function(monthlyData) {
            function addRequireDonutChartFields(month) {
                month.label = month.categoryName;
                month.Value = Number(month.cashSpent);
                month.colorCode = OLBConstants.CATEGORIES[month.categoryName];
                return month;
            }
            var pfmData = monthlyData.map(addRequireDonutChartFields);
            return pfmData;
        }
        /**
         * presentPFM : Method for used to Format the Chart Data.
         * @member of {PersonalFinanceManagement_PresentationController}
         * @param {monthlyData} /get All Year data
         * @returns {} get all list of Years.
         * @throws {} 
         */
    PersonalFinanceManagement_PresentationController.prototype.formatPFMBarChartData = function(yearlyData) {
            function addRequireBarChartFields(yearSpending) {
                yearSpending.label = yearSpending.monthName;
                yearSpending.Value = Number(yearSpending.totalCashFlow);
                yearSpending.colorCode = "#3c6cbe";
                yearSpending.annotationText = CommonUtilities.formatCurrencyWithCommas(yearSpending.totalCashFlow);
				yearSpending.tooltipText = yearSpending.monthName +": " +CommonUtilities.formatCurrencyWithCommas(yearSpending.totalCashFlow);
                return yearSpending;
            }
            var pfmData = yearlyData.map(addRequireBarChartFields).reverse();
            return pfmData;
        }
        /**
         * presentPFM : Method for return list of years
         * @member of {PersonalFinanceManagement_PresentationController}
         * @param {data} /viewmodel for form
         * @returns {} get all list of Years.
         * @throws {} 
         */
    PersonalFinanceManagement_PresentationController.prototype.getPFMYears = function() {
        var pfmYears = [];
        var date = new Date();
        var presentYear = date.getFullYear();
        //ToDo No Of years will come from configuration.
        for (var i = 0; i < CommonUtilities.getConfiguration("pfmMaxYears"); i++) pfmYears.push(presentYear - i);
        return pfmYears;
    };
    /**
     * presentPFM : Method for return List Of Months.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /viewmodel for form
     * @returns {} get all list of months.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.getPFMMonthsByYear = function(year) {
        var pfmMonthsWithYear = {};
        pfmMonthsWithYear.year = year;
        var date = new Date();
        var presentYear = date.getFullYear();
        if (year == presentYear) {
            pfmMonthsWithYear.pfmMonths = CommonUtilities.returnMonths(date.getMonth());
        } else {
            pfmMonthsWithYear.pfmMonths = CommonUtilities.returnMonths();
        }
        return pfmMonthsWithYear;
    };
    /**
     * getMonthlySpending : used to get Montly Spening Data.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {monthId} //sending particular month
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.selectYear = function(year) {
        var viewModel = {};
        viewModel.getMonths = this.getPFMMonthsByYear(year);
        this.presentPFM(viewModel);
    };
    PersonalFinanceManagement_PresentationController.prototype.loadPFMComponents = function() {
        this.loadComponents('frmPersonalFinanceManagement');
    };
    /*
     * Function to load components
     * @param : form
     */
    PersonalFinanceManagement_PresentationController.prototype.loadComponents = function(frm, data) {
        var self = this;
        var howToShowHamburgerMenu = function(sideMenuViewModel) {
            self.presentPFM({
                "sideMenu": sideMenuViewModel
            });
        };
        self.SideMenu.init(howToShowHamburgerMenu);
        var presentTopBar = function(topBarViewModel) {
            self.presentPFM({
                "topBar": topBarViewModel
            });
        };
        self.TopBar.init(presentTopBar);
    };
    /**
     * getPFMAccounts : used to get All PFMAccouns.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {} 
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.getPFMRelatedAccounts = function(monthId,year) {
        var self = this;
        self.showProgressBar();
		var reqObject = {
            'year': year,
            'monthId':monthId
        };
        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                    var viewModel = {};
                    viewModel.showPFMAccounts = true;
                    viewModel.pfmAccounts = self.formatPFMAccounts(response.data.records);
                    self.presentPFM(viewModel);
            } else {
                self.presentPFM({onServerDownError : true});
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.getSelecetdPFMAccounts', reqObject,completionCallback));
    };
    /**
     * formatPFMAccounts : used to format the accounts.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} 
     * @returns {} get all pfm related accounts.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.formatPFMAccounts = function(accounts) {
        function formatPFMAccount(account) {
            account.accountID = kony.i18n.getLocalizedString("i18n.transfers.accountNumber")+" " +  CommonUtilities.getMaskedAccountNumber(account.accountID);
            account.availableBalance = CommonUtilities.formatCurrencyWithCommas(account.availableBalance);
            account.totalDebits =  CommonUtilities.getDisplayCurrencyFormat(account.totalDebitsMonth);
            account.totalCredits =  CommonUtilities.getDisplayCurrencyFormat(account.totalCreditMonths);
			account.currentBalance = CommonUtilities.formatCurrencyWithCommas(account.currentBalance);
            account.outstandingBalance = CommonUtilities.formatCurrencyWithCommas(account.outstandingBalance);
            return account;
        }
        var pfmAccounts = accounts.map(formatPFMAccount);
        return pfmAccounts;
    };
    /**
     * getMonthlySpending : used to get Montly Spening Data.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {monthId} //sending particular month
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
     PersonalFinanceManagement_PresentationController.prototype.getMonthlySpending = function(showBothDonutChart,monthId,year,frmView) {
        var self = this;
        self.showProgressBar();
        var reqObject = {
            'monthId': monthId,
            'year' : year
        };

        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                if (frmView === undefined) {
                    var viewModel = {};
					if(showBothDonutChart)
						 viewModel.showBothDonutCharts = true;
					else
						viewModel.showBothDonutCharts = false;
                    viewModel.showMonthlyDonutChart = true;
                    viewModel.monthlySpending = self.formatPFMDonuctChartData(response.data);
                    if(response.data.length !==0)
                     viewModel.totalCashSpent = CommonUtilities.formatCurrencyWithCommas(response.data[0].totalCashSpent);
                     self.presentPFM(viewModel);
                } else {
                    frmView.monthlySpending = self.formatPFMDonuctChartData(response.data);
                     if(response.data.length !==0)
                    frmView.totalCashSpent = CommonUtilities.formatCurrencyWithCommas(response.data[0].totalCashSpent);
				    if(showBothDonutChart)
						 frmView.showBothDonutCharts = true;
					else
						frmView.showBothDonutCharts = false;
                    self.presentPFM(frmView);
                }
            } else {
                self.presentPFM({onServerDownError : true});
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.getMonthlySpending', reqObject, completionCallback));
    };
    /**
     * getYearlySpending : used to get Yearly Spening Data.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {yearId} //sending particular year
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.getYearlySpending = function(year) {
        var self = this;
        self.showProgressBar();
        var reqObject = {
            'year': year
        };
	    var date = new Date();
        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
					var pfmYearData = response.data;
					if(date.getFullYear() == year)
					{
						pfmYearData = pfmYearData.slice(0,date.getMonth());
					}
                    var viewModel = {};
                    viewModel.showYearlyBarChart = true;
                    viewModel.yearlySpending = self.formatPFMBarChartData(pfmYearData);
                    self.presentPFM(viewModel);              
            } else {
                self.presentPFM({onServerDownError : true});
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.getYearlySpending', reqObject, completionCallback));
    };
    /**
     * createDataModelUnCategorizedTransations : Method for create data model for uncategorized transaction
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {transaction} /transaction array for form
     * @returns {}
     * @throws {} 
     */    
    PersonalFinanceManagement_PresentationController.prototype.createDataModelUnCategorizedTransations = function (transaction) {
        return {
            transactionId : transaction.transactionId,
            categoryName : transaction.categoryName,
            categoryId : transaction.categoryId,
            toAccountName : transaction.toAccountName,
            toAccountNumber : transaction.toAccountNumber,
            fromAccountName : transaction.fromAccountName,
            fromAccountNumber : transaction.fromAccountNumber,
            transactionAmount : CommonUtilities.formatCurrencyWithCommas(transaction.transactionAmount),
            transactionDate : CommonUtilities.formatDate(transaction.transactionDate),
            transactionDescription : transaction.transactionDescription
        }
    };
    /**
     * showBulkUpdateTransaction : Method to present the viewmodel in form
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {} /viewmodel for form
     * @returns {}
     * @throws {} 
     */    
    PersonalFinanceManagement_PresentationController.prototype.showBulkUpdateTransaction = function(){
        this.presentPFM({bulkUpdateTransactionList : this.transactionViewModel});
    };
    /**
     * showUnCategorizedTransaction : Method to display viewmodel in form
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /viewmodel for form
     * @returns {} 
     * @throws {} 
     */    
    PersonalFinanceManagement_PresentationController.prototype.showUnCategorizedTransaction = function(transactionObj){
         this.transactionViewModel = transactionObj.data.map(this.createDataModelUnCategorizedTransations);
         this.presentPFM({
            unCategorizedIdTransactionList: {
					data : this.transactionViewModel, 
					config: transactionObj.config

			}
        });
    };
    /**
     * fetchUnCategorizedTransations : Method for load uncategorised transaction list.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {monthId} / monthId to load
     * @returns {} \
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.fetchUnCategorizedTransations = function (monthId, yearId, dataInputs) {
			
	  var self = this;
        if (monthId) {
            this.monthId = monthId;
            this.yearId = yearId;
        }
        if (this.unCategorizedId === undefined || this.unCategorizedId === null) {
            this.fetchCategoryList();
            return;
        }

        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                self.showUnCategorizedTransaction({
                    data: response.data,
                    config: reqObject
                });
            } else {
                self.presentPFM({
                    onServerDownError: true
                });
            }
        }
        var sortInputs = self.sortUnCatTranascionsConfig;
		if(dataInputs) {
			sortInputs= CommonUtilities.Sorting.getSortConfigObject({
				sortBy: dataInputs.sortBy ,
				order: dataInputs.order
				}, self.sortUnCatTranascionsConfig);
		}
			
        var reqObject = {
            monthId: self.monthId,
            yearId: self.yearId,
            categoryId: self.unCategorizedId,
            sortBy: sortInputs.sortBy,
            order: sortInputs.order
        };
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.getUncategorizedTransactions', reqObject, completionCallback));
    };
    /**
     * bulkUpdateCategory : Method for update the transactions
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {transactionsList} /list of transactions
     * @returns {}
     * @throws {} 
     */    
    PersonalFinanceManagement_PresentationController.prototype.bulkUpdateCategory = function(transactionList){
        var self = this;
        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
 
            } else {
                self.presentPFM({onServerDownError : true});
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.bulkUpdateTransactions', {transactionList : transactionList}, completionCallback));
    }
    /**
     * fetchCategoryList : Method to load categories list.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {} 
     * @returns {} 
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.fetchCategoryList = function () {
        var self = this;
        function completionCallback(response) {
            if (response.status == kony.mvc.constants.STATUS_SUCCESS) {
                self.categoryList = response.data;
                self.unCategorizedId = response.data.filter(function(data){
                    if(data.categoryName === "Uncategorized"){
                        return data.categoryId;
                    }
                })[0].categoryId;
                self.fetchUnCategorizedTransations();
                self.presentPFM({categoryList : self.categoryList});
            } else {
                self.presentPFM({onServerDownError : true});
            }
        }
        self.businessController.execute(new kony.mvc.Business.Command('com.kony.PersonalFinanceManagement.getCalegoryList', {}, completionCallback));
    };    
    /**
     * presentPFM : Method for navigate to PersonalFinanceManagement form.
     * @member of {PersonalFinanceManagement_PresentationController}
     * @param {data} /viewmodel for form
     * @returns {} get all list of categories with spendings.
     * @throws {} 
     */
    PersonalFinanceManagement_PresentationController.prototype.presentPFM = function(data) {
        this.presentUserInterface("frmPersonalFinanceManagement", data);
    };
    PersonalFinanceManagement_PresentationController.prototype.showProgressBar = function(data) {
        var self = this;
        self.presentPFM({
            "showProgressBar": "showProgressBar"
        });
    };
    PersonalFinanceManagement_PresentationController.prototype.hideProgressBar = function(data) {
        var self = this;
        self.presentPFM({
            "hideProgressBar": "hideProgressBar"
        });
    };
    return PersonalFinanceManagement_PresentationController;
});