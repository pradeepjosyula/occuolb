define([], function() {

  	function PersonalFinanceManagement_getSelecetdPFMAccounts_commandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }
	
    inheritsFrom(PersonalFinanceManagement_getSelecetdPFMAccounts_commandHandler, kony.mvc.Business.CommandHandler);
  
  	PersonalFinanceManagement_getSelecetdPFMAccounts_commandHandler.prototype.execute = function(command){
		
		var self =this;
        
		function completionCallback(status, response, err) {
          if(status==kony.mvc.constants.STATUS_SUCCESS)
            self.sendResponse(command, status, response);
          else
            self.sendResponse(command, status, err);
        } 
    
        try  {
            var reqObj = {
                'monthId' : command.context.monthId,
                'year' : command.context.year
            };
            var  PFMAccountsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PFMAccounts");
            PFMAccountsModel.customVerb('getPFMAccounts',reqObj,completionCallback);
        }  catch  (error) {
          self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
		
    };
	
	PersonalFinanceManagement_getSelecetdPFMAccounts_commandHandler.prototype.validate = function(){
		
    };
    
    return PersonalFinanceManagement_getSelecetdPFMAccounts_commandHandler;
    
});