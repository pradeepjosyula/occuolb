define([], function() {

  function PersonalFinanceManagement_getYearlySpending_commandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(PersonalFinanceManagement_getYearlySpending_commandHandler, kony.mvc.Business.CommandHandler);

  PersonalFinanceManagement_getYearlySpending_commandHandler.prototype.execute = function(command){

    var self =this;
    function completionCallback(status, response, err) {
      if(status==kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, err);
    } 

    try  {
      var  PFMBarChartModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PFMBarGraph");
      var criteria=kony.mvc.Expression.eq("year",command.context.year);
        PFMBarChartModel.getByCriteria(criteria,completionCallback);
    }  catch  (error) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }


  };

  PersonalFinanceManagement_getYearlySpending_commandHandler.prototype.validate = function(){

  };

  return PersonalFinanceManagement_getYearlySpending_commandHandler;

});