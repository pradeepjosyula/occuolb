define([], function() {

  function PersonalFinanceManagement_getMonthlySpending_commandHandler(commandId) {
    kony.mvc.Business.CommandHandler.call(this, commandId);
  }

  inheritsFrom(PersonalFinanceManagement_getMonthlySpending_commandHandler, kony.mvc.Business.CommandHandler);

  PersonalFinanceManagement_getMonthlySpending_commandHandler.prototype.execute = function(command){

    var self =this;
    function completionCallback(status, response, err) {
      if(status==kony.mvc.constants.STATUS_SUCCESS)
        self.sendResponse(command, status, response);
      else
        self.sendResponse(command, status, err);
    } 

    try  {
      var  PFMPieChartModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PFMPieChart");
	  var criteria = kony.mvc.Expression.and(kony.mvc.Expression.eq("monthId", command.context.monthId), kony.mvc.Expression.eq("year", command.context.year));
      PFMPieChartModel.getByCriteria(criteria,completionCallback);
    }  catch  (error) {
      self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
    }

  };

  PersonalFinanceManagement_getMonthlySpending_commandHandler.prototype.validate = function(){

  };

  return PersonalFinanceManagement_getMonthlySpending_commandHandler;

});