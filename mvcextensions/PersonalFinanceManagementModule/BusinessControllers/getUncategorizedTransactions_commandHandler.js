define([], function () {

    function PersonalFinanceManagement_getUncategorizedTransactions_commandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(PersonalFinanceManagement_getUncategorizedTransactions_commandHandler, kony.mvc.Business.CommandHandler);

    PersonalFinanceManagement_getUncategorizedTransactions_commandHandler.prototype.execute = function (command) {
        var self = this;
        function completionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
            var PFMTransactions = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PFMTransactions");
            criteria = kony.mvc.Expression.and(kony.mvc.Expression.eq("monthId", command.context.monthId),
                        kony.mvc.Expression.eq("year", command.context.yearId),
                        kony.mvc.Expression.eq("categoryId", command.context.categoryId),
						kony.mvc.Expression.eq("sortBy", command.context.sortBy), 
						kony.mvc.Expression.eq("order", command.context.order))
            PFMTransactions.getByCriteria(criteria, completionCallback);
        }
        catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };

    PersonalFinanceManagement_getUncategorizedTransactions_commandHandler.prototype.validate = function () {

    };

    return PersonalFinanceManagement_getUncategorizedTransactions_commandHandler;

});