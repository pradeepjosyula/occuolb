define([], function () {

    function PersonalFinanceManagement_getCategoryList_commandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(PersonalFinanceManagement_getCategoryList_commandHandler, kony.mvc.Business.CommandHandler);

    PersonalFinanceManagement_getCategoryList_commandHandler.prototype.execute = function (command) {
        var self = this;
        function completionCallback(status, data, error) {
            self.sendResponse(command, status, status === kony.mvc.constants.STATUS_SUCCESS ? data : error);
        }
        try {
            var PFMCategory = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PFMCategory");
            PFMCategory.getAll(completionCallback);
        }
        catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }
    };

    PersonalFinanceManagement_getCategoryList_commandHandler.prototype.validate = function () {

    };

    return PersonalFinanceManagement_getCategoryList_commandHandler;

});