define([], function () {

    function PersonalFinanceManagement_bulkUpdateTransactions_commandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(PersonalFinanceManagement_bulkUpdateTransactions_commandHandler, kony.mvc.Business.CommandHandler);

    PersonalFinanceManagement_bulkUpdateTransactions_commandHandler.prototype.execute = function (command) {
        var self = this;

        function completionCallBack(status, response, error) {
            if (status == kony.mvc.constants.STATUS_SUCCESS) {
                self.sendResponse(command, status, response);
            } else {
                self.sendResponse(command, status, error);
            }
        }
        try {
            var PFMTransactions = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PFMTransactions");
            PFMTransactions.customVerb("updateBulkPFMTransaction", { "pfmtransactionlist": JSON.stringify(command.context.transactionList) }, completionCallBack);
        } catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }

    };

    PersonalFinanceManagement_bulkUpdateTransactions_commandHandler.prototype.validate = function () {

    };

    return PersonalFinanceManagement_bulkUpdateTransactions_commandHandler;

});