define([], function () {

    function PersonalFinanceManagement_getMonthlyCategorizedTransactions_commandHandler(commandId) {
        kony.mvc.Business.CommandHandler.call(this, commandId);
    }

    inheritsFrom(PersonalFinanceManagement_getMonthlyCategorizedTransactions_commandHandler, kony.mvc.Business.CommandHandler);

    PersonalFinanceManagement_getMonthlyCategorizedTransactions_commandHandler.prototype.execute = function (command) {

        var self = this;
        function completionCallback(status, response, err) {
            if (status == kony.mvc.constants.STATUS_SUCCESS)
                self.sendResponse(command, status, response);
            else
                self.sendResponse(command, status, err);
        }

        try {
            var PfmTransactions = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("PFMTransactions");
            var criteria = kony.mvc.Expression.and(
                kony.mvc.Expression.eq("monthId", command.context.monthId),
                kony.mvc.Expression.eq("getMonthlyTransactions", "true") 
            ); 
    
            PfmTransactions.getByCriteria(criteria, completionCallback);
        } catch (error) {
            self.sendResponse(command, kony.mvc.constants.STATUS_FAILURE, error);
        }

    };

    PersonalFinanceManagement_getMonthlyCategorizedTransactions_commandHandler.prototype.validate = function () {

    };

    return PersonalFinanceManagement_getMonthlyCategorizedTransactions_commandHandler;

});