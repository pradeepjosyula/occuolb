define([],function(){
	var mappings = {
		"alert" : "alert",
		"error" : "error",
		"success" : "success",
		"userFirstName" : "userFirstName",
		"accountID" : "accountID",
		"accountName" : "accountName",
		"accountType" : "accountType",
		"hex" : "hex",
		"availableBalance" : "availableBalance",
		"balanceType" : "balanceType",
		"productBalance" : "productBalance",
		"productBalanceType" : "productBalanceType",
		"isPrimaryOwner" : "isPrimaryOwner",
		"nickName" : "nickName",
		"shareID" : "shareID",
		"productInfo" : "productInfo",
		"payBill" : "payBill",
		"viewDetail" : "viewDetail",
		"sendMoney" : "sendMoney",
		"depositCheck" : "depositCheck",
		"debitCard" : "debitCard",
		"transfer" : "transfer",
		"isFavorite" : "isFavorite",
		"makePayment" : "makePayment",
		"stopCheck" : "stopCheck",
		"scheduledDate" : "scheduledDate",
		"amount" : "amount",
		"desc1" : "desc1",
		"desc2" : "desc2",
		"message" : "message",
	};
	Object.freeze(mappings);
	
	var typings = {
		"alert" : "string",
		"error" : "string",
		"success" : "boolean",
		"userFirstName" : "string",
		"accountID" : "string",
		"accountName" : "string",
		"accountType" : "string",
		"hex" : "string",
		"availableBalance" : "string",
		"balanceType" : "string",
		"productBalance" : "string",
		"productBalanceType" : "string",
		"isPrimaryOwner" : "boolean",
		"nickName" : "string",
		"shareID" : "string",
		"productInfo" : "string",
		"payBill" : "boolean",
		"viewDetail" : "boolean",
		"sendMoney" : "boolean",
		"depositCheck" : "boolean",
		"debitCard" : "boolean",
		"transfer" : "boolean",
		"isFavorite" : "boolean",
		"makePayment" : "boolean",
		"stopCheck" : "boolean",
		"scheduledDate" : "string",
		"amount" : "string",
		"desc1" : "string",
		"desc2" : "string",
		"message" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"userFirstName",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "OCCUObjects",
		tableName : "AccountsOCCU"
	};
	Object.freeze(config);
	
	return config;
})
