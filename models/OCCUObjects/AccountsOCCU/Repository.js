define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function AccountsOCCURepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	AccountsOCCURepository.prototype = Object.create(BaseRepository.prototype);
	AccountsOCCURepository.prototype.constructor = AccountsOCCURepository;

	//For Operation 'getAccountData' with service id 'accounts2663'
	AccountsOCCURepository.prototype.getAccountData = function(params,onCompletion){
		return AccountsOCCURepository.prototype.customVerb('getAccountData',params, onCompletion);
	};
	
	
	return AccountsOCCURepository;
})