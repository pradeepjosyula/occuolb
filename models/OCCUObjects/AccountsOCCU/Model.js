define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		alert : function(val, state){
			state['alert'] = val;
		},
		error : function(val, state){
			state['error'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		userFirstName : function(val, state){
			state['userFirstName'] = val;
		},
		accountID : function(val, state){
			state['accountID'] = val;
		},
		accountName : function(val, state){
			state['accountName'] = val;
		},
		accountType : function(val, state){
			state['accountType'] = val;
		},
		hex : function(val, state){
			state['hex'] = val;
		},
		availableBalance : function(val, state){
			state['availableBalance'] = val;
		},
		balanceType : function(val, state){
			state['balanceType'] = val;
		},
		productBalance : function(val, state){
			state['productBalance'] = val;
		},
		productBalanceType : function(val, state){
			state['productBalanceType'] = val;
		},
		isPrimaryOwner : function(val, state){
			state['isPrimaryOwner'] = val;
		},
		nickName : function(val, state){
			state['nickName'] = val;
		},
		shareID : function(val, state){
			state['shareID'] = val;
		},
		productInfo : function(val, state){
			state['productInfo'] = val;
		},
		payBill : function(val, state){
			state['payBill'] = val;
		},
		viewDetail : function(val, state){
			state['viewDetail'] = val;
		},
		sendMoney : function(val, state){
			state['sendMoney'] = val;
		},
		depositCheck : function(val, state){
			state['depositCheck'] = val;
		},
		debitCard : function(val, state){
			state['debitCard'] = val;
		},
		transfer : function(val, state){
			state['transfer'] = val;
		},
		isFavorite : function(val, state){
			state['isFavorite'] = val;
		},
		makePayment : function(val, state){
			state['makePayment'] = val;
		},
		stopCheck : function(val, state){
			state['stopCheck'] = val;
		},
		scheduledDate : function(val, state){
			state['scheduledDate'] = val;
		},
		amount : function(val, state){
			state['amount'] = val;
		},
		desc1 : function(val, state){
			state['desc1'] = val;
		},
		desc2 : function(val, state){
			state['desc2'] = val;
		},
		message : function(val, state){
			state['message'] = val;
		},
	};
	
	
	//Create the Model Class
	function AccountsOCCU(defaultValues){
		var privateState = {};
			privateState.alert = defaultValues?(defaultValues["alert"]?defaultValues["alert"]:null):null;
			privateState.error = defaultValues?(defaultValues["error"]?defaultValues["error"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.userFirstName = defaultValues?(defaultValues["userFirstName"]?defaultValues["userFirstName"]:null):null;
			privateState.accountID = defaultValues?(defaultValues["accountID"]?defaultValues["accountID"]:null):null;
			privateState.accountName = defaultValues?(defaultValues["accountName"]?defaultValues["accountName"]:null):null;
			privateState.accountType = defaultValues?(defaultValues["accountType"]?defaultValues["accountType"]:null):null;
			privateState.hex = defaultValues?(defaultValues["hex"]?defaultValues["hex"]:null):null;
			privateState.availableBalance = defaultValues?(defaultValues["availableBalance"]?defaultValues["availableBalance"]:null):null;
			privateState.balanceType = defaultValues?(defaultValues["balanceType"]?defaultValues["balanceType"]:null):null;
			privateState.productBalance = defaultValues?(defaultValues["productBalance"]?defaultValues["productBalance"]:null):null;
			privateState.productBalanceType = defaultValues?(defaultValues["productBalanceType"]?defaultValues["productBalanceType"]:null):null;
			privateState.isPrimaryOwner = defaultValues?(defaultValues["isPrimaryOwner"]?defaultValues["isPrimaryOwner"]:null):null;
			privateState.nickName = defaultValues?(defaultValues["nickName"]?defaultValues["nickName"]:null):null;
			privateState.shareID = defaultValues?(defaultValues["shareID"]?defaultValues["shareID"]:null):null;
			privateState.productInfo = defaultValues?(defaultValues["productInfo"]?defaultValues["productInfo"]:null):null;
			privateState.payBill = defaultValues?(defaultValues["payBill"]?defaultValues["payBill"]:null):null;
			privateState.viewDetail = defaultValues?(defaultValues["viewDetail"]?defaultValues["viewDetail"]:null):null;
			privateState.sendMoney = defaultValues?(defaultValues["sendMoney"]?defaultValues["sendMoney"]:null):null;
			privateState.depositCheck = defaultValues?(defaultValues["depositCheck"]?defaultValues["depositCheck"]:null):null;
			privateState.debitCard = defaultValues?(defaultValues["debitCard"]?defaultValues["debitCard"]:null):null;
			privateState.transfer = defaultValues?(defaultValues["transfer"]?defaultValues["transfer"]:null):null;
			privateState.isFavorite = defaultValues?(defaultValues["isFavorite"]?defaultValues["isFavorite"]:null):null;
			privateState.makePayment = defaultValues?(defaultValues["makePayment"]?defaultValues["makePayment"]:null):null;
			privateState.stopCheck = defaultValues?(defaultValues["stopCheck"]?defaultValues["stopCheck"]:null):null;
			privateState.scheduledDate = defaultValues?(defaultValues["scheduledDate"]?defaultValues["scheduledDate"]:null):null;
			privateState.amount = defaultValues?(defaultValues["amount"]?defaultValues["amount"]:null):null;
			privateState.desc1 = defaultValues?(defaultValues["desc1"]?defaultValues["desc1"]:null):null;
			privateState.desc2 = defaultValues?(defaultValues["desc2"]?defaultValues["desc2"]:null):null;
			privateState.message = defaultValues?(defaultValues["message"]?defaultValues["message"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"alert" : {
					get : function(){return privateState.alert},
					set : function(val){
						setterFunctions['alert'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"error" : {
					get : function(){return privateState.error},
					set : function(val){
						setterFunctions['error'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"userFirstName" : {
					get : function(){return privateState.userFirstName},
					set : function(val){throw Error("userFirstName cannot be changed."); },
					enumerable : true,
				},
				"accountID" : {
					get : function(){return privateState.accountID},
					set : function(val){
						setterFunctions['accountID'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountName" : {
					get : function(){return privateState.accountName},
					set : function(val){
						setterFunctions['accountName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountType" : {
					get : function(){return privateState.accountType},
					set : function(val){
						setterFunctions['accountType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"hex" : {
					get : function(){return privateState.hex},
					set : function(val){
						setterFunctions['hex'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"availableBalance" : {
					get : function(){return privateState.availableBalance},
					set : function(val){
						setterFunctions['availableBalance'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"balanceType" : {
					get : function(){return privateState.balanceType},
					set : function(val){
						setterFunctions['balanceType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productBalance" : {
					get : function(){return privateState.productBalance},
					set : function(val){
						setterFunctions['productBalance'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productBalanceType" : {
					get : function(){return privateState.productBalanceType},
					set : function(val){
						setterFunctions['productBalanceType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isPrimaryOwner" : {
					get : function(){return privateState.isPrimaryOwner},
					set : function(val){
						setterFunctions['isPrimaryOwner'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"nickName" : {
					get : function(){return privateState.nickName},
					set : function(val){
						setterFunctions['nickName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"shareID" : {
					get : function(){return privateState.shareID},
					set : function(val){
						setterFunctions['shareID'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productInfo" : {
					get : function(){return privateState.productInfo},
					set : function(val){
						setterFunctions['productInfo'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"payBill" : {
					get : function(){return privateState.payBill},
					set : function(val){
						setterFunctions['payBill'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"viewDetail" : {
					get : function(){return privateState.viewDetail},
					set : function(val){
						setterFunctions['viewDetail'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"sendMoney" : {
					get : function(){return privateState.sendMoney},
					set : function(val){
						setterFunctions['sendMoney'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"depositCheck" : {
					get : function(){return privateState.depositCheck},
					set : function(val){
						setterFunctions['depositCheck'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"debitCard" : {
					get : function(){return privateState.debitCard},
					set : function(val){
						setterFunctions['debitCard'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"transfer" : {
					get : function(){return privateState.transfer},
					set : function(val){
						setterFunctions['transfer'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isFavorite" : {
					get : function(){return privateState.isFavorite},
					set : function(val){
						setterFunctions['isFavorite'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"makePayment" : {
					get : function(){return privateState.makePayment},
					set : function(val){
						setterFunctions['makePayment'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"stopCheck" : {
					get : function(){return privateState.stopCheck},
					set : function(val){
						setterFunctions['stopCheck'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"scheduledDate" : {
					get : function(){return privateState.scheduledDate},
					set : function(val){
						setterFunctions['scheduledDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"amount" : {
					get : function(){return privateState.amount},
					set : function(val){
						setterFunctions['amount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"desc1" : {
					get : function(){return privateState.desc1},
					set : function(val){
						setterFunctions['desc1'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"desc2" : {
					get : function(){return privateState.desc2},
					set : function(val){
						setterFunctions['desc2'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"message" : {
					get : function(){return privateState.message},
					set : function(val){
						setterFunctions['message'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(AccountsOCCU);
	
	//Create new class level validator object
	BaseModel.Validator.call(AccountsOCCU);
	
	var registerValidatorBackup = AccountsOCCU.registerValidator;
	
	AccountsOCCU.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( AccountsOCCU.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'getAccountData' with service id 'accounts2663'
	AccountsOCCU.getAccountData = function(params, onCompletion){
		return AccountsOCCU.customVerb('getAccountData', params, onCompletion);
	};
	
	var relations = [
	];
	
	AccountsOCCU.relations = relations;
	
	AccountsOCCU.prototype.isValid = function(){
		return AccountsOCCU.isValid(this);
	};
	
	AccountsOCCU.prototype.objModelName = "AccountsOCCU";
	
	return AccountsOCCU;
});