define([],function(){
	var mappings = {
		"amount" : "amount",
		"date" : "date",
		"desc1" : "desc1",
		"desc2" : "desc2",
		"lowBalance" : "lowBalance",
	};
	Object.freeze(mappings);
	
	var typings = {
		"amount" : "string",
		"date" : "string",
		"desc1" : "string",
		"desc2" : "string",
		"lowBalance" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "OCCUObjects",
		tableName : "scheduledTransactions"
	};
	Object.freeze(config);
	
	return config;
})
