define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		amount : function(val, state){
			state['amount'] = val;
		},
		date : function(val, state){
			state['date'] = val;
		},
		desc1 : function(val, state){
			state['desc1'] = val;
		},
		desc2 : function(val, state){
			state['desc2'] = val;
		},
		lowBalance : function(val, state){
			state['lowBalance'] = val;
		},
	};
	
	
	//Create the Model Class
	function scheduledTransactions(defaultValues){
		var privateState = {};
			privateState.amount = defaultValues?(defaultValues["amount"]?defaultValues["amount"]:null):null;
			privateState.date = defaultValues?(defaultValues["date"]?defaultValues["date"]:null):null;
			privateState.desc1 = defaultValues?(defaultValues["desc1"]?defaultValues["desc1"]:null):null;
			privateState.desc2 = defaultValues?(defaultValues["desc2"]?defaultValues["desc2"]:null):null;
			privateState.lowBalance = defaultValues?(defaultValues["lowBalance"]?defaultValues["lowBalance"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"amount" : {
					get : function(){return privateState.amount},
					set : function(val){
						setterFunctions['amount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"date" : {
					get : function(){return privateState.date},
					set : function(val){
						setterFunctions['date'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"desc1" : {
					get : function(){return privateState.desc1},
					set : function(val){
						setterFunctions['desc1'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"desc2" : {
					get : function(){return privateState.desc2},
					set : function(val){
						setterFunctions['desc2'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"lowBalance" : {
					get : function(){return privateState.lowBalance},
					set : function(val){
						setterFunctions['lowBalance'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(scheduledTransactions);
	
	//Create new class level validator object
	BaseModel.Validator.call(scheduledTransactions);
	
	var registerValidatorBackup = scheduledTransactions.registerValidator;
	
	scheduledTransactions.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( scheduledTransactions.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	
	var relations = [
	];
	
	scheduledTransactions.relations = relations;
	
	scheduledTransactions.prototype.isValid = function(){
		return scheduledTransactions.isValid(this);
	};
	
	scheduledTransactions.prototype.objModelName = "scheduledTransactions";
	
	return scheduledTransactions;
});