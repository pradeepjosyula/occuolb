define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		asofDate : function(val, state){
			state['asofDate'] = val;
		},
		balance : function(val, state){
			state['balance'] = val;
		},
		balanceType : function(val, state){
			state['balanceType'] = val;
		},
		description : function(val, state){
			state['description'] = val;
		},
		minimumPaymentDue : function(val, state){
			state['minimumPaymentDue'] = val;
		},
		paymentDueDate : function(val, state){
			state['paymentDueDate'] = val;
		},
		pending : function(val, state){
			state['pending'] = val;
		},
		productBalance : function(val, state){
			state['productBalance'] = val;
		},
		productCode : function(val, state){
			state['productCode'] = val;
		},
		productInfo : function(val, state){
			state['productInfo'] = val;
		},
		productType : function(val, state){
			state['productType'] = val;
		},
		regDCount : function(val, state){
			state['regDCount'] = val;
		},
		shareId : function(val, state){
			state['shareId'] = val;
		},
	};
	
	
	//Create the Model Class
	function subAccounts(defaultValues){
		var privateState = {};
			privateState.asofDate = defaultValues?(defaultValues["asofDate"]?defaultValues["asofDate"]:null):null;
			privateState.balance = defaultValues?(defaultValues["balance"]?defaultValues["balance"]:null):null;
			privateState.balanceType = defaultValues?(defaultValues["balanceType"]?defaultValues["balanceType"]:null):null;
			privateState.description = defaultValues?(defaultValues["description"]?defaultValues["description"]:null):null;
			privateState.minimumPaymentDue = defaultValues?(defaultValues["minimumPaymentDue"]?defaultValues["minimumPaymentDue"]:null):null;
			privateState.paymentDueDate = defaultValues?(defaultValues["paymentDueDate"]?defaultValues["paymentDueDate"]:null):null;
			privateState.pending = defaultValues?(defaultValues["pending"]?defaultValues["pending"]:null):null;
			privateState.productBalance = defaultValues?(defaultValues["productBalance"]?defaultValues["productBalance"]:null):null;
			privateState.productCode = defaultValues?(defaultValues["productCode"]?defaultValues["productCode"]:null):null;
			privateState.productInfo = defaultValues?(defaultValues["productInfo"]?defaultValues["productInfo"]:null):null;
			privateState.productType = defaultValues?(defaultValues["productType"]?defaultValues["productType"]:null):null;
			privateState.regDCount = defaultValues?(defaultValues["regDCount"]?defaultValues["regDCount"]:null):null;
			privateState.shareId = defaultValues?(defaultValues["shareId"]?defaultValues["shareId"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"asofDate" : {
					get : function(){return privateState.asofDate},
					set : function(val){
						setterFunctions['asofDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"balance" : {
					get : function(){return privateState.balance},
					set : function(val){
						setterFunctions['balance'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"balanceType" : {
					get : function(){return privateState.balanceType},
					set : function(val){
						setterFunctions['balanceType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"description" : {
					get : function(){return privateState.description},
					set : function(val){
						setterFunctions['description'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"minimumPaymentDue" : {
					get : function(){return privateState.minimumPaymentDue},
					set : function(val){
						setterFunctions['minimumPaymentDue'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"paymentDueDate" : {
					get : function(){return privateState.paymentDueDate},
					set : function(val){
						setterFunctions['paymentDueDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"pending" : {
					get : function(){return privateState.pending},
					set : function(val){
						setterFunctions['pending'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productBalance" : {
					get : function(){return privateState.productBalance},
					set : function(val){
						setterFunctions['productBalance'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productCode" : {
					get : function(){return privateState.productCode},
					set : function(val){
						setterFunctions['productCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productInfo" : {
					get : function(){return privateState.productInfo},
					set : function(val){
						setterFunctions['productInfo'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productType" : {
					get : function(){return privateState.productType},
					set : function(val){
						setterFunctions['productType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"regDCount" : {
					get : function(){return privateState.regDCount},
					set : function(val){
						setterFunctions['regDCount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"shareId" : {
					get : function(){return privateState.shareId},
					set : function(val){
						setterFunctions['shareId'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(subAccounts);
	
	//Create new class level validator object
	BaseModel.Validator.call(subAccounts);
	
	var registerValidatorBackup = subAccounts.registerValidator;
	
	subAccounts.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( subAccounts.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	
	var relations = [
	];
	
	subAccounts.relations = relations;
	
	subAccounts.prototype.isValid = function(){
		return subAccounts.isValid(this);
	};
	
	subAccounts.prototype.objModelName = "subAccounts";
	
	return subAccounts;
});