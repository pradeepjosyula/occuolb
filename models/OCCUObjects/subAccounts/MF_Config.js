define([],function(){
	var mappings = {
		"asofDate" : "asofDate",
		"balance" : "balance",
		"balanceType" : "balanceType",
		"description" : "description",
		"minimumPaymentDue" : "minimumPaymentDue",
		"paymentDueDate" : "paymentDueDate",
		"pending" : "pending",
		"productBalance" : "productBalance",
		"productCode" : "productCode",
		"productInfo" : "productInfo",
		"productType" : "productType",
		"regDCount" : "regDCount",
		"shareId" : "shareId",
	};
	Object.freeze(mappings);
	
	var typings = {
		"asofDate" : "string",
		"balance" : "string",
		"balanceType" : "string",
		"description" : "string",
		"minimumPaymentDue" : "string",
		"paymentDueDate" : "string",
		"pending" : "string",
		"productBalance" : "string",
		"productCode" : "string",
		"productInfo" : "string",
		"productType" : "string",
		"regDCount" : "string",
		"shareId" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "OCCUObjects",
		tableName : "subAccounts"
	};
	Object.freeze(config);
	
	return config;
})
