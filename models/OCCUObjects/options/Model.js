define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		debitCard : function(val, state){
			state['debitCard'] = val;
		},
		depositCheck : function(val, state){
			state['depositCheck'] = val;
		},
		isFavorite : function(val, state){
			state['isFavorite'] = val;
		},
		makePayment : function(val, state){
			state['makePayment'] = val;
		},
		payBill : function(val, state){
			state['payBill'] = val;
		},
		sendMoney : function(val, state){
			state['sendMoney'] = val;
		},
		stopCheck : function(val, state){
			state['stopCheck'] = val;
		},
		transfer : function(val, state){
			state['transfer'] = val;
		},
		viewDetails : function(val, state){
			state['viewDetails'] = val;
		},
	};
	
	
	//Create the Model Class
	function options(defaultValues){
		var privateState = {};
			privateState.debitCard = defaultValues?(defaultValues["debitCard"]?defaultValues["debitCard"]:null):null;
			privateState.depositCheck = defaultValues?(defaultValues["depositCheck"]?defaultValues["depositCheck"]:null):null;
			privateState.isFavorite = defaultValues?(defaultValues["isFavorite"]?defaultValues["isFavorite"]:null):null;
			privateState.makePayment = defaultValues?(defaultValues["makePayment"]?defaultValues["makePayment"]:null):null;
			privateState.payBill = defaultValues?(defaultValues["payBill"]?defaultValues["payBill"]:null):null;
			privateState.sendMoney = defaultValues?(defaultValues["sendMoney"]?defaultValues["sendMoney"]:null):null;
			privateState.stopCheck = defaultValues?(defaultValues["stopCheck"]?defaultValues["stopCheck"]:null):null;
			privateState.transfer = defaultValues?(defaultValues["transfer"]?defaultValues["transfer"]:null):null;
			privateState.viewDetails = defaultValues?(defaultValues["viewDetails"]?defaultValues["viewDetails"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"debitCard" : {
					get : function(){return privateState.debitCard},
					set : function(val){
						setterFunctions['debitCard'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"depositCheck" : {
					get : function(){return privateState.depositCheck},
					set : function(val){
						setterFunctions['depositCheck'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isFavorite" : {
					get : function(){return privateState.isFavorite},
					set : function(val){
						setterFunctions['isFavorite'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"makePayment" : {
					get : function(){return privateState.makePayment},
					set : function(val){
						setterFunctions['makePayment'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"payBill" : {
					get : function(){return privateState.payBill},
					set : function(val){
						setterFunctions['payBill'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"sendMoney" : {
					get : function(){return privateState.sendMoney},
					set : function(val){
						setterFunctions['sendMoney'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"stopCheck" : {
					get : function(){return privateState.stopCheck},
					set : function(val){
						setterFunctions['stopCheck'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"transfer" : {
					get : function(){return privateState.transfer},
					set : function(val){
						setterFunctions['transfer'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"viewDetails" : {
					get : function(){return privateState.viewDetails},
					set : function(val){
						setterFunctions['viewDetails'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(options);
	
	//Create new class level validator object
	BaseModel.Validator.call(options);
	
	var registerValidatorBackup = options.registerValidator;
	
	options.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( options.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	
	var relations = [
	];
	
	options.relations = relations;
	
	options.prototype.isValid = function(){
		return options.isValid(this);
	};
	
	options.prototype.objModelName = "options";
	
	return options;
});