define([],function(){
	var mappings = {
		"debitCard" : "debitCard",
		"depositCheck" : "depositCheck",
		"isFavorite" : "isFavorite",
		"makePayment" : "makePayment",
		"payBill" : "payBill",
		"sendMoney" : "sendMoney",
		"stopCheck" : "stopCheck",
		"transfer" : "transfer",
		"viewDetails" : "viewDetails",
	};
	Object.freeze(mappings);
	
	var typings = {
		"debitCard" : "boolean",
		"depositCheck" : "boolean",
		"isFavorite" : "boolean",
		"makePayment" : "boolean",
		"payBill" : "boolean",
		"sendMoney" : "boolean",
		"stopCheck" : "boolean",
		"transfer" : "boolean",
		"viewDetails" : "boolean",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "OCCUObjects",
		tableName : "options"
	};
	Object.freeze(config);
	
	return config;
})
