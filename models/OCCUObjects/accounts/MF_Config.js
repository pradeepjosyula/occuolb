define([],function(){
	var mappings = {
		"accountID" : "accountID",
		"accountHolder" : "accountHolder",
		"isPrimaryOwner" : "isPrimaryOwner",
	};
	Object.freeze(mappings);
	
	var typings = {
		"accountID" : "string",
		"accountHolder" : "string",
		"isPrimaryOwner" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"accountID",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "OCCUObjects",
		tableName : "accounts"
	};
	Object.freeze(config);
	
	return config;
})
