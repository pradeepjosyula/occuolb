define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		accountID : function(val, state){
			state['accountID'] = val;
		},
		accountHolder : function(val, state){
			state['accountHolder'] = val;
		},
		isPrimaryOwner : function(val, state){
			state['isPrimaryOwner'] = val;
		},
	};
	
	
	//Create the Model Class
	function accounts(defaultValues){
		var privateState = {};
			privateState.accountID = defaultValues?(defaultValues["accountID"]?defaultValues["accountID"]:null):null;
			privateState.accountHolder = defaultValues?(defaultValues["accountHolder"]?defaultValues["accountHolder"]:null):null;
			privateState.isPrimaryOwner = defaultValues?(defaultValues["isPrimaryOwner"]?defaultValues["isPrimaryOwner"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"accountID" : {
					get : function(){return privateState.accountID},
					set : function(val){throw Error("accountID cannot be changed."); },
					enumerable : true,
				},
				"accountHolder" : {
					get : function(){return privateState.accountHolder},
					set : function(val){
						setterFunctions['accountHolder'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isPrimaryOwner" : {
					get : function(){return privateState.isPrimaryOwner},
					set : function(val){
						setterFunctions['isPrimaryOwner'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(accounts);
	
	//Create new class level validator object
	BaseModel.Validator.call(accounts);
	
	var registerValidatorBackup = accounts.registerValidator;
	
	accounts.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( accounts.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	
	var relations = [
	];
	
	accounts.relations = relations;
	
	accounts.prototype.isValid = function(){
		return accounts.isValid(this);
	};
	
	accounts.prototype.objModelName = "accounts";
	
	return accounts;
});