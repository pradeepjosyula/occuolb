define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function CheckRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	CheckRepository.prototype = Object.create(BaseRepository.prototype);
	CheckRepository.prototype.constructor = CheckRepository;

	//For Operation 'getCheck' with service id 'getCheck6465'
	CheckRepository.prototype.getCheck = function(params,onCompletion){
		return CheckRepository.prototype.customVerb('getCheck',params, onCompletion);
	};
	//For Operation 'stopCheck' with service id 'stopCheck5412'
	CheckRepository.prototype.stopCheck = function(params,onCompletion){
		return CheckRepository.prototype.customVerb('stopCheck',params, onCompletion);
	};
	
	
	return CheckRepository;
})