define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		accountNumber : function(val, state){
			state['accountNumber'] = val;
		},
		checkNumber : function(val, state){
			state['checkNumber'] = val;
		},
		processDate : function(val, state){
			state['processDate'] = val;
		},
		bufferFront : function(val, state){
			state['bufferFront'] = val;
		},
		bufferBack : function(val, state){
			state['bufferBack'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		error : function(val, state){
			state['error'] = val;
		},
		productType : function(val, state){
			state['productType'] = val;
		},
		productId : function(val, state){
			state['productId'] = val;
		},
		checkNumberHighRange : function(val, state){
			state['checkNumberHighRange'] = val;
		},
		payee : function(val, state){
			state['payee'] = val;
		},
	};
	
	
	//Create the Model Class
	function Check(defaultValues){
		var privateState = {};
			privateState.accountNumber = defaultValues?(defaultValues["accountNumber"]?defaultValues["accountNumber"]:null):null;
			privateState.checkNumber = defaultValues?(defaultValues["checkNumber"]?defaultValues["checkNumber"]:null):null;
			privateState.processDate = defaultValues?(defaultValues["processDate"]?defaultValues["processDate"]:null):null;
			privateState.bufferFront = defaultValues?(defaultValues["bufferFront"]?defaultValues["bufferFront"]:null):null;
			privateState.bufferBack = defaultValues?(defaultValues["bufferBack"]?defaultValues["bufferBack"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.error = defaultValues?(defaultValues["error"]?defaultValues["error"]:null):null;
			privateState.productType = defaultValues?(defaultValues["productType"]?defaultValues["productType"]:null):null;
			privateState.productId = defaultValues?(defaultValues["productId"]?defaultValues["productId"]:null):null;
			privateState.checkNumberHighRange = defaultValues?(defaultValues["checkNumberHighRange"]?defaultValues["checkNumberHighRange"]:null):null;
			privateState.payee = defaultValues?(defaultValues["payee"]?defaultValues["payee"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"accountNumber" : {
					get : function(){return privateState.accountNumber},
					set : function(val){throw Error("accountNumber cannot be changed."); },
					enumerable : true,
				},
				"checkNumber" : {
					get : function(){return privateState.checkNumber},
					set : function(val){
						setterFunctions['checkNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"processDate" : {
					get : function(){return privateState.processDate},
					set : function(val){
						setterFunctions['processDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bufferFront" : {
					get : function(){return privateState.bufferFront},
					set : function(val){
						setterFunctions['bufferFront'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bufferBack" : {
					get : function(){return privateState.bufferBack},
					set : function(val){
						setterFunctions['bufferBack'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"error" : {
					get : function(){return privateState.error},
					set : function(val){
						setterFunctions['error'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productType" : {
					get : function(){return privateState.productType},
					set : function(val){
						setterFunctions['productType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productId" : {
					get : function(){return privateState.productId},
					set : function(val){
						setterFunctions['productId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"checkNumberHighRange" : {
					get : function(){return privateState.checkNumberHighRange},
					set : function(val){
						setterFunctions['checkNumberHighRange'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"payee" : {
					get : function(){return privateState.payee},
					set : function(val){
						setterFunctions['payee'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(Check);
	
	//Create new class level validator object
	BaseModel.Validator.call(Check);
	
	var registerValidatorBackup = Check.registerValidator;
	
	Check.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( Check.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'getCheck' with service id 'getCheck6465'
	Check.getCheck = function(params, onCompletion){
		return Check.customVerb('getCheck', params, onCompletion);
	};
	//For Operation 'stopCheck' with service id 'stopCheck5412'
	Check.stopCheck = function(params, onCompletion){
		return Check.customVerb('stopCheck', params, onCompletion);
	};
	
	var relations = [
	];
	
	Check.relations = relations;
	
	Check.prototype.isValid = function(){
		return Check.isValid(this);
	};
	
	Check.prototype.objModelName = "Check";
	
	return Check;
});