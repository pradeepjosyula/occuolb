define([],function(){
	var mappings = {
		"accountNumber" : "accountNumber",
		"checkNumber" : "checkNumber",
		"processDate" : "processDate",
		"bufferFront" : "bufferFront",
		"bufferBack" : "bufferBack",
		"success" : "success",
		"error" : "error",
		"productType" : "productType",
		"productId" : "productId",
		"checkNumberHighRange" : "checkNumberHighRange",
		"payee" : "payee",
	};
	Object.freeze(mappings);
	
	var typings = {
		"accountNumber" : "string",
		"checkNumber" : "string",
		"processDate" : "string",
		"bufferFront" : "string",
		"bufferBack" : "string",
		"success" : "boolean",
		"error" : "string",
		"productType" : "string",
		"productId" : "string",
		"checkNumberHighRange" : "string",
		"payee" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"accountNumber",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "Check"
	};
	Object.freeze(config);
	
	return config;
})
