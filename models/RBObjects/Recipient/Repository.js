define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function RecipientRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	RecipientRepository.prototype = Object.create(BaseRepository.prototype);
	RecipientRepository.prototype.constructor = RecipientRepository;

	//For Operation 'createRecipient' with service id 'createRecipient3067'
	RecipientRepository.prototype.createRecipient = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('createRecipient',params, onCompletion);
	};
	//For Operation 'editExternalAccount' with service id 'editExternalAccount8188'
	RecipientRepository.prototype.editExternalAccount = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('editExternalAccount',params, onCompletion);
	};
	//For Operation 'getRecipientsList' with service id 'getRecipientList5494'
	RecipientRepository.prototype.getRecipientsList = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('getRecipientsList',params, onCompletion);
	};
	//For Operation 'activateExternalRecipient' with service id 'activateExternalAccount7709'
	RecipientRepository.prototype.activateExternalRecipient = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('activateExternalRecipient',params, onCompletion);
	};
	//For Operation 'getAllRecipients' with service id 'getAllRecipients6453'
	RecipientRepository.prototype.getAllRecipients = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('getAllRecipients',params, onCompletion);
	};
	//For Operation 'removeRecipient' with service id 'removeRecipient5793'
	RecipientRepository.prototype.removeRecipient = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('removeRecipient',params, onCompletion);
	};
	//For Operation 'getRecipients' with service id 'getRecipients5449'
	RecipientRepository.prototype.getRecipients = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('getRecipients',params, onCompletion);
	};
	//For Operation 'removeExternalAccounts' with service id 'deleteExternalAccount7392'
	RecipientRepository.prototype.removeExternalAccounts = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('removeExternalAccounts',params, onCompletion);
	};
	//For Operation 'getMyAccountsToTransfer' with service id 'getAccountsToTransfer3569'
	RecipientRepository.prototype.getMyAccountsToTransfer = function(params,onCompletion){
		return RecipientRepository.prototype.customVerb('getMyAccountsToTransfer',params, onCompletion);
	};
	
	
	return RecipientRepository;
})