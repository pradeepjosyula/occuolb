define([],function(){
	var mappings = {
		"username" : "username",
		"accountNumber" : "accountNumber",
		"productType" : "productType",
		"productId" : "productId",
		"accountLastName" : "accountLastName",
		"description" : "description",
		"success" : "success",
		"error" : "error",
		"fromAccountNumber" : "fromAccountNumber",
		"fromOwner" : "fromOwner",
		"fromProductId" : "fromProductId",
		"fromProductType" : "fromProductType",
		"fromDescription" : "fromDescription",
		"fromBalance" : "fromBalance",
		"regD" : "regD",
		"toBalance" : "toBalance",
		"toOwner" : "toOwner",
		"toDue" : "toDue",
		"toDate" : "toDate",
		"addedOn" : "addedOn",
		"externalAccountId" : "externalAccountId",
		"amount1" : "amount1",
		"amount2" : "amount2",
		"editOption" : "editOption",
		"viewActivityOption" : "viewActivityOption",
		"deleteOption" : "deleteOption",
		"activate" : "activate",
		"status" : "status",
		"recipientType" : "recipientType",
		"accountName" : "accountName",
		"accountType" : "accountType",
		"id" : "id",
		"externalTransferOption" : "externalTransferOption",
		"nickName" : "nickName",
		"toStmtBal" : "toStmtBal",
	};
	Object.freeze(mappings);
	
	var typings = {
		"username" : "string",
		"accountNumber" : "string",
		"productType" : "string",
		"productId" : "string",
		"accountLastName" : "string",
		"description" : "string",
		"success" : "string",
		"error" : "string",
		"fromAccountNumber" : "string",
		"fromOwner" : "string",
		"fromProductId" : "string",
		"fromProductType" : "string",
		"fromDescription" : "string",
		"fromBalance" : "string",
		"regD" : "number",
		"toBalance" : "string",
		"toOwner" : "string",
		"toDue" : "string",
		"toDate" : "string",
		"addedOn" : "string",
		"externalAccountId" : "string",
		"amount1" : "string",
		"amount2" : "string",
		"editOption" : "boolean",
		"viewActivityOption" : "boolean",
		"deleteOption" : "boolean",
		"activate" : "boolean",
		"status" : "string",
		"recipientType" : "string",
		"accountName" : "string",
		"accountType" : "string",
		"id" : "string",
		"externalTransferOption" : "boolean",
		"nickName" : "string",
		"toStmtBal" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"username",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "Recipient"
	};
	Object.freeze(config);
	
	return config;
})
