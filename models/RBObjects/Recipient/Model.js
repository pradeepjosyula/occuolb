define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		username : function(val, state){
			state['username'] = val;
		},
		accountNumber : function(val, state){
			state['accountNumber'] = val;
		},
		productType : function(val, state){
			state['productType'] = val;
		},
		productId : function(val, state){
			state['productId'] = val;
		},
		accountLastName : function(val, state){
			state['accountLastName'] = val;
		},
		description : function(val, state){
			state['description'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		error : function(val, state){
			state['error'] = val;
		},
		fromAccountNumber : function(val, state){
			state['fromAccountNumber'] = val;
		},
		fromOwner : function(val, state){
			state['fromOwner'] = val;
		},
		fromProductId : function(val, state){
			state['fromProductId'] = val;
		},
		fromProductType : function(val, state){
			state['fromProductType'] = val;
		},
		fromDescription : function(val, state){
			state['fromDescription'] = val;
		},
		fromBalance : function(val, state){
			state['fromBalance'] = val;
		},
		regD : function(val, state){
			state['regD'] = val;
		},
		toBalance : function(val, state){
			state['toBalance'] = val;
		},
		toOwner : function(val, state){
			state['toOwner'] = val;
		},
		toDue : function(val, state){
			state['toDue'] = val;
		},
		toDate : function(val, state){
			state['toDate'] = val;
		},
		addedOn : function(val, state){
			state['addedOn'] = val;
		},
		externalAccountId : function(val, state){
			state['externalAccountId'] = val;
		},
		amount1 : function(val, state){
			state['amount1'] = val;
		},
		amount2 : function(val, state){
			state['amount2'] = val;
		},
		editOption : function(val, state){
			state['editOption'] = val;
		},
		viewActivityOption : function(val, state){
			state['viewActivityOption'] = val;
		},
		deleteOption : function(val, state){
			state['deleteOption'] = val;
		},
		activate : function(val, state){
			state['activate'] = val;
		},
		status : function(val, state){
			state['status'] = val;
		},
		recipientType : function(val, state){
			state['recipientType'] = val;
		},
		accountName : function(val, state){
			state['accountName'] = val;
		},
		accountType : function(val, state){
			state['accountType'] = val;
		},
		id : function(val, state){
			state['id'] = val;
		},
		externalTransferOption : function(val, state){
			state['externalTransferOption'] = val;
		},
		nickName : function(val, state){
			state['nickName'] = val;
		},
		toStmtBal : function(val, state){
			state['toStmtBal'] = val;
		},
	};
	
	
	//Create the Model Class
	function Recipient(defaultValues){
		var privateState = {};
			privateState.username = defaultValues?(defaultValues["username"]?defaultValues["username"]:null):null;
			privateState.accountNumber = defaultValues?(defaultValues["accountNumber"]?defaultValues["accountNumber"]:null):null;
			privateState.productType = defaultValues?(defaultValues["productType"]?defaultValues["productType"]:null):null;
			privateState.productId = defaultValues?(defaultValues["productId"]?defaultValues["productId"]:null):null;
			privateState.accountLastName = defaultValues?(defaultValues["accountLastName"]?defaultValues["accountLastName"]:null):null;
			privateState.description = defaultValues?(defaultValues["description"]?defaultValues["description"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.error = defaultValues?(defaultValues["error"]?defaultValues["error"]:null):null;
			privateState.fromAccountNumber = defaultValues?(defaultValues["fromAccountNumber"]?defaultValues["fromAccountNumber"]:null):null;
			privateState.fromOwner = defaultValues?(defaultValues["fromOwner"]?defaultValues["fromOwner"]:null):null;
			privateState.fromProductId = defaultValues?(defaultValues["fromProductId"]?defaultValues["fromProductId"]:null):null;
			privateState.fromProductType = defaultValues?(defaultValues["fromProductType"]?defaultValues["fromProductType"]:null):null;
			privateState.fromDescription = defaultValues?(defaultValues["fromDescription"]?defaultValues["fromDescription"]:null):null;
			privateState.fromBalance = defaultValues?(defaultValues["fromBalance"]?defaultValues["fromBalance"]:null):null;
			privateState.regD = defaultValues?(defaultValues["regD"]?defaultValues["regD"]:null):null;
			privateState.toBalance = defaultValues?(defaultValues["toBalance"]?defaultValues["toBalance"]:null):null;
			privateState.toOwner = defaultValues?(defaultValues["toOwner"]?defaultValues["toOwner"]:null):null;
			privateState.toDue = defaultValues?(defaultValues["toDue"]?defaultValues["toDue"]:null):null;
			privateState.toDate = defaultValues?(defaultValues["toDate"]?defaultValues["toDate"]:null):null;
			privateState.addedOn = defaultValues?(defaultValues["addedOn"]?defaultValues["addedOn"]:null):null;
			privateState.externalAccountId = defaultValues?(defaultValues["externalAccountId"]?defaultValues["externalAccountId"]:null):null;
			privateState.amount1 = defaultValues?(defaultValues["amount1"]?defaultValues["amount1"]:null):null;
			privateState.amount2 = defaultValues?(defaultValues["amount2"]?defaultValues["amount2"]:null):null;
			privateState.editOption = defaultValues?(defaultValues["editOption"]?defaultValues["editOption"]:null):null;
			privateState.viewActivityOption = defaultValues?(defaultValues["viewActivityOption"]?defaultValues["viewActivityOption"]:null):null;
			privateState.deleteOption = defaultValues?(defaultValues["deleteOption"]?defaultValues["deleteOption"]:null):null;
			privateState.activate = defaultValues?(defaultValues["activate"]?defaultValues["activate"]:null):null;
			privateState.status = defaultValues?(defaultValues["status"]?defaultValues["status"]:null):null;
			privateState.recipientType = defaultValues?(defaultValues["recipientType"]?defaultValues["recipientType"]:null):null;
			privateState.accountName = defaultValues?(defaultValues["accountName"]?defaultValues["accountName"]:null):null;
			privateState.accountType = defaultValues?(defaultValues["accountType"]?defaultValues["accountType"]:null):null;
			privateState.id = defaultValues?(defaultValues["id"]?defaultValues["id"]:null):null;
			privateState.externalTransferOption = defaultValues?(defaultValues["externalTransferOption"]?defaultValues["externalTransferOption"]:null):null;
			privateState.nickName = defaultValues?(defaultValues["nickName"]?defaultValues["nickName"]:null):null;
			privateState.toStmtBal = defaultValues?(defaultValues["toStmtBal"]?defaultValues["toStmtBal"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"username" : {
					get : function(){return privateState.username},
					set : function(val){throw Error("username cannot be changed."); },
					enumerable : true,
				},
				"accountNumber" : {
					get : function(){return privateState.accountNumber},
					set : function(val){
						setterFunctions['accountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productType" : {
					get : function(){return privateState.productType},
					set : function(val){
						setterFunctions['productType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"productId" : {
					get : function(){return privateState.productId},
					set : function(val){
						setterFunctions['productId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountLastName" : {
					get : function(){return privateState.accountLastName},
					set : function(val){
						setterFunctions['accountLastName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"description" : {
					get : function(){return privateState.description},
					set : function(val){
						setterFunctions['description'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"error" : {
					get : function(){return privateState.error},
					set : function(val){
						setterFunctions['error'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromAccountNumber" : {
					get : function(){return privateState.fromAccountNumber},
					set : function(val){
						setterFunctions['fromAccountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromOwner" : {
					get : function(){return privateState.fromOwner},
					set : function(val){
						setterFunctions['fromOwner'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromProductId" : {
					get : function(){return privateState.fromProductId},
					set : function(val){
						setterFunctions['fromProductId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromProductType" : {
					get : function(){return privateState.fromProductType},
					set : function(val){
						setterFunctions['fromProductType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromDescription" : {
					get : function(){return privateState.fromDescription},
					set : function(val){
						setterFunctions['fromDescription'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromBalance" : {
					get : function(){return privateState.fromBalance},
					set : function(val){
						setterFunctions['fromBalance'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"regD" : {
					get : function(){return privateState.regD},
					set : function(val){
						setterFunctions['regD'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toBalance" : {
					get : function(){return privateState.toBalance},
					set : function(val){
						setterFunctions['toBalance'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toOwner" : {
					get : function(){return privateState.toOwner},
					set : function(val){
						setterFunctions['toOwner'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toDue" : {
					get : function(){return privateState.toDue},
					set : function(val){
						setterFunctions['toDue'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toDate" : {
					get : function(){return privateState.toDate},
					set : function(val){
						setterFunctions['toDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"addedOn" : {
					get : function(){return privateState.addedOn},
					set : function(val){
						setterFunctions['addedOn'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"externalAccountId" : {
					get : function(){return privateState.externalAccountId},
					set : function(val){
						setterFunctions['externalAccountId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"amount1" : {
					get : function(){return privateState.amount1},
					set : function(val){
						setterFunctions['amount1'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"amount2" : {
					get : function(){return privateState.amount2},
					set : function(val){
						setterFunctions['amount2'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"editOption" : {
					get : function(){return privateState.editOption},
					set : function(val){
						setterFunctions['editOption'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"viewActivityOption" : {
					get : function(){return privateState.viewActivityOption},
					set : function(val){
						setterFunctions['viewActivityOption'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"deleteOption" : {
					get : function(){return privateState.deleteOption},
					set : function(val){
						setterFunctions['deleteOption'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"activate" : {
					get : function(){return privateState.activate},
					set : function(val){
						setterFunctions['activate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"status" : {
					get : function(){return privateState.status},
					set : function(val){
						setterFunctions['status'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"recipientType" : {
					get : function(){return privateState.recipientType},
					set : function(val){
						setterFunctions['recipientType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountName" : {
					get : function(){return privateState.accountName},
					set : function(val){
						setterFunctions['accountName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountType" : {
					get : function(){return privateState.accountType},
					set : function(val){
						setterFunctions['accountType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"id" : {
					get : function(){return privateState.id},
					set : function(val){
						setterFunctions['id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"externalTransferOption" : {
					get : function(){return privateState.externalTransferOption},
					set : function(val){
						setterFunctions['externalTransferOption'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"nickName" : {
					get : function(){return privateState.nickName},
					set : function(val){
						setterFunctions['nickName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toStmtBal" : {
					get : function(){return privateState.toStmtBal},
					set : function(val){
						setterFunctions['toStmtBal'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(Recipient);
	
	//Create new class level validator object
	BaseModel.Validator.call(Recipient);
	
	var registerValidatorBackup = Recipient.registerValidator;
	
	Recipient.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( Recipient.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'createRecipient' with service id 'createRecipient3067'
	Recipient.createRecipient = function(params, onCompletion){
		return Recipient.customVerb('createRecipient', params, onCompletion);
	};
	//For Operation 'editExternalAccount' with service id 'editExternalAccount8188'
	Recipient.editExternalAccount = function(params, onCompletion){
		return Recipient.customVerb('editExternalAccount', params, onCompletion);
	};
	//For Operation 'getRecipientsList' with service id 'getRecipientList5494'
	Recipient.getRecipientsList = function(params, onCompletion){
		return Recipient.customVerb('getRecipientsList', params, onCompletion);
	};
	//For Operation 'activateExternalRecipient' with service id 'activateExternalAccount7709'
	Recipient.activateExternalRecipient = function(params, onCompletion){
		return Recipient.customVerb('activateExternalRecipient', params, onCompletion);
	};
	//For Operation 'getAllRecipients' with service id 'getAllRecipients6453'
	Recipient.getAllRecipients = function(params, onCompletion){
		return Recipient.customVerb('getAllRecipients', params, onCompletion);
	};
	//For Operation 'removeRecipient' with service id 'removeRecipient5793'
	Recipient.removeRecipient = function(params, onCompletion){
		return Recipient.customVerb('removeRecipient', params, onCompletion);
	};
	//For Operation 'getRecipients' with service id 'getRecipients5449'
	Recipient.getRecipients = function(params, onCompletion){
		return Recipient.customVerb('getRecipients', params, onCompletion);
	};
	//For Operation 'removeExternalAccounts' with service id 'deleteExternalAccount7392'
	Recipient.removeExternalAccounts = function(params, onCompletion){
		return Recipient.customVerb('removeExternalAccounts', params, onCompletion);
	};
	//For Operation 'getMyAccountsToTransfer' with service id 'getAccountsToTransfer3569'
	Recipient.getMyAccountsToTransfer = function(params, onCompletion){
		return Recipient.customVerb('getMyAccountsToTransfer', params, onCompletion);
	};
	
	var relations = [
	];
	
	Recipient.relations = relations;
	
	Recipient.prototype.isValid = function(){
		return Recipient.isValid(this);
	};
	
	Recipient.prototype.objModelName = "Recipient";
	
	return Recipient;
});