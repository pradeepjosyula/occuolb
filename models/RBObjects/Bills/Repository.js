define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function BillsRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	BillsRepository.prototype = Object.create(BaseRepository.prototype);
	BillsRepository.prototype.constructor = BillsRepository;

	//For Operation 'deleteScheduledPayment' with service id 'deleteScheduledPayment9951'
	BillsRepository.prototype.deleteScheduledPayment = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('deleteScheduledPayment',params, onCompletion);
	};
	//For Operation 'scheduleAllPayments' with service id 'scheduleAllPayments1827'
	BillsRepository.prototype.scheduleAllPayments = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('scheduleAllPayments',params, onCompletion);
	};
	//For Operation 'editScheduledPayment' with service id 'editScheduledPayments6379'
	BillsRepository.prototype.editScheduledPayment = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('editScheduledPayment',params, onCompletion);
	};
	//For Operation 'getPostedBillPays' with service id 'getPostedBillPays6561'
	BillsRepository.prototype.getPostedBillPays = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('getPostedBillPays',params, onCompletion);
	};
	//For Operation 'getBillsForBiller' with service id 'getBillsForBiller7675'
	BillsRepository.prototype.getBillsForBiller = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('getBillsForBiller',params, onCompletion);
	};
	//For Operation 'getPreviousBillsForBiller' with service id 'getPreviousBillsForBiller6633'
	BillsRepository.prototype.getPreviousBillsForBiller = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('getPreviousBillsForBiller',params, onCompletion);
	};
	//For Operation 'getPendingBillPays' with service id 'getScheduledBillPays1577'
	BillsRepository.prototype.getPendingBillPays = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('getPendingBillPays',params, onCompletion);
	};
	//For Operation 'getScheduledBillPays' with service id 'getScheduledBillPays2741'
	BillsRepository.prototype.getScheduledBillPays = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('getScheduledBillPays',params, onCompletion);
	};
	//For Operation 'schedulePayment' with service id 'schedulePayment8438'
	BillsRepository.prototype.schedulePayment = function(params,onCompletion){
		return BillsRepository.prototype.customVerb('schedulePayment',params, onCompletion);
	};
	
	
	return BillsRepository;
})