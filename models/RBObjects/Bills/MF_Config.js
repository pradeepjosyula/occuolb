define([],function(){
	var mappings = {
		"balanceAmount" : "balanceAmount",
		"billDueDate" : "billDueDate",
		"billerCategory" : "billerCategory",
		"billerName" : "billerName",
		"billGeneratedDate" : "billGeneratedDate",
		"description" : "description",
		"dueAmount" : "dueAmount",
		"ebillStatus" : "ebillStatus",
		"ebillURL" : "ebillURL",
		"fromAccountName" : "fromAccountName",
		"fromAccountNumber" : "fromAccountNumber",
		"id" : "id",
		"order" : "order",
		"paidAmount" : "paidAmount",
		"paidDate" : "paidDate",
		"payeeId" : "payeeId",
		"payeeName" : "payeeName",
		"sortBy" : "sortBy",
		"success" : "success",
		"error" : "error",
		"username" : "username",
		"paymentDetails" : "paymentDetails",
		"account" : "account",
		"data" : "data",
		"accountCode" : "accountCode",
		"confirmationNumber" : "confirmationNumber",
		"createdUserCode" : "createdUserCode",
		"deliveryDate" : "deliveryDate",
		"processingDate" : "processingDate",
		"remainingPayments" : "remainingPayments",
		"scheduledPaymentId" : "scheduledPaymentId",
		"totalNumberOfPayments" : "totalNumberOfPayments",
		"updatedUserCode" : "updatedUserCode",
		"checkNumber" : "checkNumber",
	};
	Object.freeze(mappings);
	
	var typings = {
		"balanceAmount" : "string",
		"billDueDate" : "string",
		"billerCategory" : "string",
		"billerName" : "string",
		"billGeneratedDate" : "string",
		"description" : "string",
		"dueAmount" : "string",
		"ebillStatus" : "string",
		"ebillURL" : "string",
		"fromAccountName" : "string",
		"fromAccountNumber" : "string",
		"id" : "string",
		"order" : "string",
		"paidAmount" : "string",
		"paidDate" : "string",
		"payeeId" : "string",
		"payeeName" : "string",
		"sortBy" : "string",
		"success" : "string",
		"error" : "string",
		"username" : "string",
		"paymentDetails" : "string",
		"account" : "string",
		"data" : "string",
		"accountCode" : "string",
		"confirmationNumber" : "number",
		"createdUserCode" : "string",
		"deliveryDate" : "string",
		"processingDate" : "string",
		"remainingPayments" : "number",
		"scheduledPaymentId" : "number",
		"totalNumberOfPayments" : "number",
		"updatedUserCode" : "string",
		"checkNumber" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"id",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "Bills"
	};
	Object.freeze(config);
	
	return config;
})
