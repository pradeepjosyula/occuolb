define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		balanceAmount : function(val, state){
			state['balanceAmount'] = val;
		},
		billDueDate : function(val, state){
			state['billDueDate'] = val;
		},
		billerCategory : function(val, state){
			state['billerCategory'] = val;
		},
		billerName : function(val, state){
			state['billerName'] = val;
		},
		billGeneratedDate : function(val, state){
			state['billGeneratedDate'] = val;
		},
		description : function(val, state){
			state['description'] = val;
		},
		dueAmount : function(val, state){
			state['dueAmount'] = val;
		},
		ebillStatus : function(val, state){
			state['ebillStatus'] = val;
		},
		ebillURL : function(val, state){
			state['ebillURL'] = val;
		},
		fromAccountName : function(val, state){
			state['fromAccountName'] = val;
		},
		fromAccountNumber : function(val, state){
			state['fromAccountNumber'] = val;
		},
		id : function(val, state){
			state['id'] = val;
		},
		order : function(val, state){
			state['order'] = val;
		},
		paidAmount : function(val, state){
			state['paidAmount'] = val;
		},
		paidDate : function(val, state){
			state['paidDate'] = val;
		},
		payeeId : function(val, state){
			state['payeeId'] = val;
		},
		payeeName : function(val, state){
			state['payeeName'] = val;
		},
		sortBy : function(val, state){
			state['sortBy'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		error : function(val, state){
			state['error'] = val;
		},
		username : function(val, state){
			state['username'] = val;
		},
		paymentDetails : function(val, state){
			state['paymentDetails'] = val;
		},
		account : function(val, state){
			state['account'] = val;
		},
		data : function(val, state){
			state['data'] = val;
		},
		accountCode : function(val, state){
			state['accountCode'] = val;
		},
		confirmationNumber : function(val, state){
			state['confirmationNumber'] = val;
		},
		createdUserCode : function(val, state){
			state['createdUserCode'] = val;
		},
		deliveryDate : function(val, state){
			state['deliveryDate'] = val;
		},
		processingDate : function(val, state){
			state['processingDate'] = val;
		},
		remainingPayments : function(val, state){
			state['remainingPayments'] = val;
		},
		scheduledPaymentId : function(val, state){
			state['scheduledPaymentId'] = val;
		},
		totalNumberOfPayments : function(val, state){
			state['totalNumberOfPayments'] = val;
		},
		updatedUserCode : function(val, state){
			state['updatedUserCode'] = val;
		},
		checkNumber : function(val, state){
			state['checkNumber'] = val;
		},
	};
	
	
	//Create the Model Class
	function Bills(defaultValues){
		var privateState = {};
			privateState.balanceAmount = defaultValues?(defaultValues["balanceAmount"]?defaultValues["balanceAmount"]:null):null;
			privateState.billDueDate = defaultValues?(defaultValues["billDueDate"]?defaultValues["billDueDate"]:null):null;
			privateState.billerCategory = defaultValues?(defaultValues["billerCategory"]?defaultValues["billerCategory"]:null):null;
			privateState.billerName = defaultValues?(defaultValues["billerName"]?defaultValues["billerName"]:null):null;
			privateState.billGeneratedDate = defaultValues?(defaultValues["billGeneratedDate"]?defaultValues["billGeneratedDate"]:null):null;
			privateState.description = defaultValues?(defaultValues["description"]?defaultValues["description"]:null):null;
			privateState.dueAmount = defaultValues?(defaultValues["dueAmount"]?defaultValues["dueAmount"]:null):null;
			privateState.ebillStatus = defaultValues?(defaultValues["ebillStatus"]?defaultValues["ebillStatus"]:null):null;
			privateState.ebillURL = defaultValues?(defaultValues["ebillURL"]?defaultValues["ebillURL"]:null):null;
			privateState.fromAccountName = defaultValues?(defaultValues["fromAccountName"]?defaultValues["fromAccountName"]:null):null;
			privateState.fromAccountNumber = defaultValues?(defaultValues["fromAccountNumber"]?defaultValues["fromAccountNumber"]:null):null;
			privateState.id = defaultValues?(defaultValues["id"]?defaultValues["id"]:null):null;
			privateState.order = defaultValues?(defaultValues["order"]?defaultValues["order"]:null):null;
			privateState.paidAmount = defaultValues?(defaultValues["paidAmount"]?defaultValues["paidAmount"]:null):null;
			privateState.paidDate = defaultValues?(defaultValues["paidDate"]?defaultValues["paidDate"]:null):null;
			privateState.payeeId = defaultValues?(defaultValues["payeeId"]?defaultValues["payeeId"]:null):null;
			privateState.payeeName = defaultValues?(defaultValues["payeeName"]?defaultValues["payeeName"]:null):null;
			privateState.sortBy = defaultValues?(defaultValues["sortBy"]?defaultValues["sortBy"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.error = defaultValues?(defaultValues["error"]?defaultValues["error"]:null):null;
			privateState.username = defaultValues?(defaultValues["username"]?defaultValues["username"]:null):null;
			privateState.paymentDetails = defaultValues?(defaultValues["paymentDetails"]?defaultValues["paymentDetails"]:null):null;
			privateState.account = defaultValues?(defaultValues["account"]?defaultValues["account"]:null):null;
			privateState.data = defaultValues?(defaultValues["data"]?defaultValues["data"]:null):null;
			privateState.accountCode = defaultValues?(defaultValues["accountCode"]?defaultValues["accountCode"]:null):null;
			privateState.confirmationNumber = defaultValues?(defaultValues["confirmationNumber"]?defaultValues["confirmationNumber"]:null):null;
			privateState.createdUserCode = defaultValues?(defaultValues["createdUserCode"]?defaultValues["createdUserCode"]:null):null;
			privateState.deliveryDate = defaultValues?(defaultValues["deliveryDate"]?defaultValues["deliveryDate"]:null):null;
			privateState.processingDate = defaultValues?(defaultValues["processingDate"]?defaultValues["processingDate"]:null):null;
			privateState.remainingPayments = defaultValues?(defaultValues["remainingPayments"]?defaultValues["remainingPayments"]:null):null;
			privateState.scheduledPaymentId = defaultValues?(defaultValues["scheduledPaymentId"]?defaultValues["scheduledPaymentId"]:null):null;
			privateState.totalNumberOfPayments = defaultValues?(defaultValues["totalNumberOfPayments"]?defaultValues["totalNumberOfPayments"]:null):null;
			privateState.updatedUserCode = defaultValues?(defaultValues["updatedUserCode"]?defaultValues["updatedUserCode"]:null):null;
			privateState.checkNumber = defaultValues?(defaultValues["checkNumber"]?defaultValues["checkNumber"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"balanceAmount" : {
					get : function(){return privateState.balanceAmount},
					set : function(val){
						setterFunctions['balanceAmount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billDueDate" : {
					get : function(){return privateState.billDueDate},
					set : function(val){
						setterFunctions['billDueDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billerCategory" : {
					get : function(){return privateState.billerCategory},
					set : function(val){
						setterFunctions['billerCategory'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billerName" : {
					get : function(){return privateState.billerName},
					set : function(val){
						setterFunctions['billerName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billGeneratedDate" : {
					get : function(){return privateState.billGeneratedDate},
					set : function(val){
						setterFunctions['billGeneratedDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"description" : {
					get : function(){return privateState.description},
					set : function(val){
						setterFunctions['description'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"dueAmount" : {
					get : function(){return privateState.dueAmount},
					set : function(val){
						setterFunctions['dueAmount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"ebillStatus" : {
					get : function(){return privateState.ebillStatus},
					set : function(val){
						setterFunctions['ebillStatus'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"ebillURL" : {
					get : function(){return privateState.ebillURL},
					set : function(val){
						setterFunctions['ebillURL'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromAccountName" : {
					get : function(){return privateState.fromAccountName},
					set : function(val){
						setterFunctions['fromAccountName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromAccountNumber" : {
					get : function(){return privateState.fromAccountNumber},
					set : function(val){
						setterFunctions['fromAccountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"id" : {
					get : function(){return privateState.id},
					set : function(val){throw Error("id cannot be changed."); },
					enumerable : true,
				},
				"order" : {
					get : function(){return privateState.order},
					set : function(val){
						setterFunctions['order'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"paidAmount" : {
					get : function(){return privateState.paidAmount},
					set : function(val){
						setterFunctions['paidAmount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"paidDate" : {
					get : function(){return privateState.paidDate},
					set : function(val){
						setterFunctions['paidDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"payeeId" : {
					get : function(){return privateState.payeeId},
					set : function(val){
						setterFunctions['payeeId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"payeeName" : {
					get : function(){return privateState.payeeName},
					set : function(val){
						setterFunctions['payeeName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"sortBy" : {
					get : function(){return privateState.sortBy},
					set : function(val){
						setterFunctions['sortBy'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"error" : {
					get : function(){return privateState.error},
					set : function(val){
						setterFunctions['error'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"username" : {
					get : function(){return privateState.username},
					set : function(val){
						setterFunctions['username'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"paymentDetails" : {
					get : function(){return privateState.paymentDetails},
					set : function(val){
						setterFunctions['paymentDetails'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"account" : {
					get : function(){return privateState.account},
					set : function(val){
						setterFunctions['account'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"data" : {
					get : function(){return privateState.data},
					set : function(val){
						setterFunctions['data'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountCode" : {
					get : function(){return privateState.accountCode},
					set : function(val){
						setterFunctions['accountCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"confirmationNumber" : {
					get : function(){return privateState.confirmationNumber},
					set : function(val){
						setterFunctions['confirmationNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"createdUserCode" : {
					get : function(){return privateState.createdUserCode},
					set : function(val){
						setterFunctions['createdUserCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"deliveryDate" : {
					get : function(){return privateState.deliveryDate},
					set : function(val){
						setterFunctions['deliveryDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"processingDate" : {
					get : function(){return privateState.processingDate},
					set : function(val){
						setterFunctions['processingDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"remainingPayments" : {
					get : function(){return privateState.remainingPayments},
					set : function(val){
						setterFunctions['remainingPayments'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"scheduledPaymentId" : {
					get : function(){return privateState.scheduledPaymentId},
					set : function(val){
						setterFunctions['scheduledPaymentId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"totalNumberOfPayments" : {
					get : function(){return privateState.totalNumberOfPayments},
					set : function(val){
						setterFunctions['totalNumberOfPayments'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"updatedUserCode" : {
					get : function(){return privateState.updatedUserCode},
					set : function(val){
						setterFunctions['updatedUserCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"checkNumber" : {
					get : function(){return privateState.checkNumber},
					set : function(val){
						setterFunctions['checkNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(Bills);
	
	//Create new class level validator object
	BaseModel.Validator.call(Bills);
	
	var registerValidatorBackup = Bills.registerValidator;
	
	Bills.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( Bills.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'deleteScheduledPayment' with service id 'deleteScheduledPayment9951'
	Bills.deleteScheduledPayment = function(params, onCompletion){
		return Bills.customVerb('deleteScheduledPayment', params, onCompletion);
	};
	//For Operation 'scheduleAllPayments' with service id 'scheduleAllPayments1827'
	Bills.scheduleAllPayments = function(params, onCompletion){
		return Bills.customVerb('scheduleAllPayments', params, onCompletion);
	};
	//For Operation 'editScheduledPayment' with service id 'editScheduledPayments6379'
	Bills.editScheduledPayment = function(params, onCompletion){
		return Bills.customVerb('editScheduledPayment', params, onCompletion);
	};
	//For Operation 'getPostedBillPays' with service id 'getPostedBillPays6561'
	Bills.getPostedBillPays = function(params, onCompletion){
		return Bills.customVerb('getPostedBillPays', params, onCompletion);
	};
	//For Operation 'getBillsForBiller' with service id 'getBillsForBiller7675'
	Bills.getBillsForBiller = function(params, onCompletion){
		return Bills.customVerb('getBillsForBiller', params, onCompletion);
	};
	//For Operation 'getPreviousBillsForBiller' with service id 'getPreviousBillsForBiller6633'
	Bills.getPreviousBillsForBiller = function(params, onCompletion){
		return Bills.customVerb('getPreviousBillsForBiller', params, onCompletion);
	};
	//For Operation 'getPendingBillPays' with service id 'getScheduledBillPays1577'
	Bills.getPendingBillPays = function(params, onCompletion){
		return Bills.customVerb('getPendingBillPays', params, onCompletion);
	};
	//For Operation 'getScheduledBillPays' with service id 'getScheduledBillPays2741'
	Bills.getScheduledBillPays = function(params, onCompletion){
		return Bills.customVerb('getScheduledBillPays', params, onCompletion);
	};
	//For Operation 'schedulePayment' with service id 'schedulePayment8438'
	Bills.schedulePayment = function(params, onCompletion){
		return Bills.customVerb('schedulePayment', params, onCompletion);
	};
	
	var relations = [
	];
	
	Bills.relations = relations;
	
	Bills.prototype.isValid = function(){
		return Bills.isValid(this);
	};
	
	Bills.prototype.objModelName = "Bills";
	
	return Bills;
});