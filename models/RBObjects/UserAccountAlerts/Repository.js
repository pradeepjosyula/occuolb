define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function UserAccountAlertsRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	UserAccountAlertsRepository.prototype = Object.create(BaseRepository.prototype);
	UserAccountAlertsRepository.prototype.constructor = UserAccountAlertsRepository;

	//For Operation 'modifyAccountAlert' with service id 'modifyAccountAlert8294'
	UserAccountAlertsRepository.prototype.modifyAccountAlert = function(params,onCompletion){
		return UserAccountAlertsRepository.prototype.customVerb('modifyAccountAlert',params, onCompletion);
	};
	//For Operation 'getAlertChanelsByAccount' with service id 'getAccountAlerts1005'
	UserAccountAlertsRepository.prototype.getAlertChanelsByAccount = function(params,onCompletion){
		return UserAccountAlertsRepository.prototype.customVerb('getAlertChanelsByAccount',params, onCompletion);
	};
	//For Operation 'modifySecurityAlert' with service id 'modifyAccountAlert1359'
	UserAccountAlertsRepository.prototype.modifySecurityAlert = function(params,onCompletion){
		return UserAccountAlertsRepository.prototype.customVerb('modifySecurityAlert',params, onCompletion);
	};
	//For Operation 'getAccountAlerts' with service id 'getAccountAlerts4007'
	UserAccountAlertsRepository.prototype.getAccountAlerts = function(params,onCompletion){
		return UserAccountAlertsRepository.prototype.customVerb('getAccountAlerts',params, onCompletion);
	};
	//For Operation 'getSecurityAlerts' with service id 'getAccountAlerts7370'
	UserAccountAlertsRepository.prototype.getSecurityAlerts = function(params,onCompletion){
		return UserAccountAlertsRepository.prototype.customVerb('getSecurityAlerts',params, onCompletion);
	};
	
	
	return UserAccountAlertsRepository;
})