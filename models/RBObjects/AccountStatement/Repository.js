define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function AccountStatementRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	AccountStatementRepository.prototype = Object.create(BaseRepository.prototype);
	AccountStatementRepository.prototype.constructor = AccountStatementRepository;

	//For Operation 'showDownloadStatements' with service id 'showDownloadStatements4948'
	AccountStatementRepository.prototype.showDownloadStatements = function(params,onCompletion){
		return AccountStatementRepository.prototype.customVerb('showDownloadStatements',params, onCompletion);
	};
	//For Operation 'getDocument' with service id 'getDocument6604'
	AccountStatementRepository.prototype.getDocument = function(params,onCompletion){
		return AccountStatementRepository.prototype.customVerb('getDocument',params, onCompletion);
	};
	//For Operation 'searchDocuments' with service id 'searchDocuments8621'
	AccountStatementRepository.prototype.searchDocuments = function(params,onCompletion){
		return AccountStatementRepository.prototype.customVerb('searchDocuments',params, onCompletion);
	};
	
	
	return AccountStatementRepository;
})