define([],function(){
	var mappings = {
		"StatementDescription" : "StatementDescription",
		"StatementLink" : "StatementLink",
		"StatementMonth" : "StatementMonth",
		"accountID" : "accountID",
		"format" : "format",
		"year" : "year",
		"startYear" : "startYear",
		"endYear" : "endYear",
		"docId" : "docId",
		"docDate" : "docDate",
		"docType" : "docType",
		"success" : "success",
		"error" : "error",
		"buffer" : "buffer",
	};
	Object.freeze(mappings);
	
	var typings = {
		"StatementDescription" : "string",
		"StatementLink" : "string",
		"StatementMonth" : "string",
		"accountID" : "string",
		"format" : "string",
		"year" : "string",
		"startYear" : "string",
		"endYear" : "string",
		"docId" : "string",
		"docDate" : "string",
		"docType" : "string",
		"success" : "boolean",
		"error" : "string",
		"buffer" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"accountID",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "AccountStatement"
	};
	Object.freeze(config);
	
	return config;
})
