define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		StatementDescription : function(val, state){
			state['StatementDescription'] = val;
		},
		StatementLink : function(val, state){
			state['StatementLink'] = val;
		},
		StatementMonth : function(val, state){
			state['StatementMonth'] = val;
		},
		accountID : function(val, state){
			state['accountID'] = val;
		},
		format : function(val, state){
			state['format'] = val;
		},
		year : function(val, state){
			state['year'] = val;
		},
		startYear : function(val, state){
			state['startYear'] = val;
		},
		endYear : function(val, state){
			state['endYear'] = val;
		},
		docId : function(val, state){
			state['docId'] = val;
		},
		docDate : function(val, state){
			state['docDate'] = val;
		},
		docType : function(val, state){
			state['docType'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		error : function(val, state){
			state['error'] = val;
		},
		buffer : function(val, state){
			state['buffer'] = val;
		},
	};
	
	
	//Create the Model Class
	function AccountStatement(defaultValues){
		var privateState = {};
			privateState.StatementDescription = defaultValues?(defaultValues["StatementDescription"]?defaultValues["StatementDescription"]:null):null;
			privateState.StatementLink = defaultValues?(defaultValues["StatementLink"]?defaultValues["StatementLink"]:null):null;
			privateState.StatementMonth = defaultValues?(defaultValues["StatementMonth"]?defaultValues["StatementMonth"]:null):null;
			privateState.accountID = defaultValues?(defaultValues["accountID"]?defaultValues["accountID"]:null):null;
			privateState.format = defaultValues?(defaultValues["format"]?defaultValues["format"]:null):null;
			privateState.year = defaultValues?(defaultValues["year"]?defaultValues["year"]:null):null;
			privateState.startYear = defaultValues?(defaultValues["startYear"]?defaultValues["startYear"]:null):null;
			privateState.endYear = defaultValues?(defaultValues["endYear"]?defaultValues["endYear"]:null):null;
			privateState.docId = defaultValues?(defaultValues["docId"]?defaultValues["docId"]:null):null;
			privateState.docDate = defaultValues?(defaultValues["docDate"]?defaultValues["docDate"]:null):null;
			privateState.docType = defaultValues?(defaultValues["docType"]?defaultValues["docType"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.error = defaultValues?(defaultValues["error"]?defaultValues["error"]:null):null;
			privateState.buffer = defaultValues?(defaultValues["buffer"]?defaultValues["buffer"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"StatementDescription" : {
					get : function(){return privateState.StatementDescription},
					set : function(val){
						setterFunctions['StatementDescription'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"StatementLink" : {
					get : function(){return privateState.StatementLink},
					set : function(val){
						setterFunctions['StatementLink'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"StatementMonth" : {
					get : function(){return privateState.StatementMonth},
					set : function(val){
						setterFunctions['StatementMonth'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountID" : {
					get : function(){return privateState.accountID},
					set : function(val){throw Error("accountID cannot be changed."); },
					enumerable : true,
				},
				"format" : {
					get : function(){return privateState.format},
					set : function(val){
						setterFunctions['format'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"year" : {
					get : function(){return privateState.year},
					set : function(val){
						setterFunctions['year'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"startYear" : {
					get : function(){return privateState.startYear},
					set : function(val){
						setterFunctions['startYear'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"endYear" : {
					get : function(){return privateState.endYear},
					set : function(val){
						setterFunctions['endYear'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"docId" : {
					get : function(){return privateState.docId},
					set : function(val){
						setterFunctions['docId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"docDate" : {
					get : function(){return privateState.docDate},
					set : function(val){
						setterFunctions['docDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"docType" : {
					get : function(){return privateState.docType},
					set : function(val){
						setterFunctions['docType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"error" : {
					get : function(){return privateState.error},
					set : function(val){
						setterFunctions['error'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"buffer" : {
					get : function(){return privateState.buffer},
					set : function(val){
						setterFunctions['buffer'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(AccountStatement);
	
	//Create new class level validator object
	BaseModel.Validator.call(AccountStatement);
	
	var registerValidatorBackup = AccountStatement.registerValidator;
	
	AccountStatement.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( AccountStatement.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'showDownloadStatements' with service id 'showDownloadStatements4948'
	AccountStatement.showDownloadStatements = function(params, onCompletion){
		return AccountStatement.customVerb('showDownloadStatements', params, onCompletion);
	};
	//For Operation 'getDocument' with service id 'getDocument6604'
	AccountStatement.getDocument = function(params, onCompletion){
		return AccountStatement.customVerb('getDocument', params, onCompletion);
	};
	//For Operation 'searchDocuments' with service id 'searchDocuments8621'
	AccountStatement.searchDocuments = function(params, onCompletion){
		return AccountStatement.customVerb('searchDocuments', params, onCompletion);
	};
	
	var relations = [
	];
	
	AccountStatement.relations = relations;
	
	AccountStatement.prototype.isValid = function(){
		return AccountStatement.isValid(this);
	};
	
	AccountStatement.prototype.objModelName = "AccountStatement";
	
	return AccountStatement;
});