define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function PayeeRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	PayeeRepository.prototype = Object.create(BaseRepository.prototype);
	PayeeRepository.prototype.constructor = PayeeRepository;

	//For Operation 'GetPaymentOptions' with service id 'getPaymentOptions3173'
	PayeeRepository.prototype.GetPaymentOptions = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('GetPaymentOptions',params, onCompletion);
	};
	//For Operation 'updateRecipient' with service id 'updateRecipient9351'
	PayeeRepository.prototype.updateRecipient = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('updateRecipient',params, onCompletion);
	};
	//For Operation 'eBillAuthentication' with service id 'eBillAuthentication6162'
	PayeeRepository.prototype.eBillAuthentication = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('eBillAuthentication',params, onCompletion);
	};
	//For Operation 'getWireTransferRecipient' with service id 'getWireTransferRecipient4881'
	PayeeRepository.prototype.getWireTransferRecipient = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('getWireTransferRecipient',params, onCompletion);
	};
	//For Operation 'addRecipient' with service id 'addRecipient4640'
	PayeeRepository.prototype.addRecipient = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('addRecipient',params, onCompletion);
	};
	//For Operation 'selecteBillSite' with service id 'selecteBillSite6084'
	PayeeRepository.prototype.selecteBillSite = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('selecteBillSite',params, onCompletion);
	};
	//For Operation 'editPayee' with service id 'editPayee4489'
	PayeeRepository.prototype.editPayee = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('editPayee',params, onCompletion);
	};
	//For Operation 'saveRecipientAfterWireTransfer' with service id 'saveRecipientAfterWireTransfer1796'
	PayeeRepository.prototype.saveRecipientAfterWireTransfer = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('saveRecipientAfterWireTransfer',params, onCompletion);
	};
	//For Operation 'deletePayee' with service id 'deletePayee9154'
	PayeeRepository.prototype.deletePayee = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('deletePayee',params, onCompletion);
	};
	//For Operation 'addPayee' with service id 'addPayee7490'
	PayeeRepository.prototype.addPayee = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('addPayee',params, onCompletion);
	};
	//For Operation 'deleteEBill' with service id 'deleteEBill9021'
	PayeeRepository.prototype.deleteEBill = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('deleteEBill',params, onCompletion);
	};
	//For Operation 'getRecentPayee' with service id 'getRecentPayees8811'
	PayeeRepository.prototype.getRecentPayee = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('getRecentPayee',params, onCompletion);
	};
	//For Operation 'addeBill' with service id 'addEBill1193'
	PayeeRepository.prototype.addeBill = function(params,onCompletion){
		return PayeeRepository.prototype.customVerb('addeBill',params, onCompletion);
	};
	
	
	return PayeeRepository;
})