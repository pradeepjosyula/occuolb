define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		accountNumber : function(val, state){
			state['accountNumber'] = val;
		},
		addressLine1 : function(val, state){
			state['addressLine1'] = val;
		},
		addressLine2 : function(val, state){
			state['addressLine2'] = val;
		},
		billDescription : function(val, state){
			state['billDescription'] = val;
		},
		billDueDate : function(val, state){
			state['billDueDate'] = val;
		},
		billerCategory : function(val, state){
			state['billerCategory'] = val;
		},
		billerId : function(val, state){
			state['billerId'] = val;
		},
		billGeneratedDate : function(val, state){
			state['billGeneratedDate'] = val;
		},
		billid : function(val, state){
			state['billid'] = val;
		},
		cityName : function(val, state){
			state['cityName'] = val;
		},
		companyName : function(val, state){
			state['companyName'] = val;
		},
		dueAmount : function(val, state){
			state['dueAmount'] = val;
		},
		EBillEnable : function(val, state){
			state['EBillEnable'] = val;
		},
		eBillStatus : function(val, state){
			state['eBillStatus'] = val;
		},
		eBillSupport : function(val, state){
			state['eBillSupport'] = val;
		},
		ebillURL : function(val, state){
			state['ebillURL'] = val;
		},
		email : function(val, state){
			state['email'] = val;
		},
		errmsg : function(val, state){
			state['errmsg'] = val;
		},
		lastPaidAmount : function(val, state){
			state['lastPaidAmount'] = val;
		},
		lastPaidDate : function(val, state){
			state['lastPaidDate'] = val;
		},
		limit : function(val, state){
			state['limit'] = val;
		},
		nameOnBill : function(val, state){
			state['nameOnBill'] = val;
		},
		notes : function(val, state){
			state['notes'] = val;
		},
		offset : function(val, state){
			state['offset'] = val;
		},
		order : function(val, state){
			state['order'] = val;
		},
		paidAmount : function(val, state){
			state['paidAmount'] = val;
		},
		payeeAccountNumber : function(val, state){
			state['payeeAccountNumber'] = val;
		},
		payeeId : function(val, state){
			state['payeeId'] = val;
		},
		payeeName : function(val, state){
			state['payeeName'] = val;
		},
		payeeNickName : function(val, state){
			state['payeeNickName'] = val;
		},
		phone : function(val, state){
			state['phone'] = val;
		},
		searchString : function(val, state){
			state['searchString'] = val;
		},
		sortBy : function(val, state){
			state['sortBy'] = val;
		},
		state : function(val, state){
			state['state'] = val;
		},
		street : function(val, state){
			state['street'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		zipCode : function(val, state){
			state['zipCode'] = val;
		},
		type : function(val, state){
			state['type'] = val;
		},
		country : function(val, state){
			state['country'] = val;
		},
		swiftCode : function(val, state){
			state['swiftCode'] = val;
		},
		routingCode : function(val, state){
			state['routingCode'] = val;
		},
		bankName : function(val, state){
			state['bankName'] = val;
		},
		bankAddressLine1 : function(val, state){
			state['bankAddressLine1'] = val;
		},
		bankAddressLine2 : function(val, state){
			state['bankAddressLine2'] = val;
		},
		bankCity : function(val, state){
			state['bankCity'] = val;
		},
		bankState : function(val, state){
			state['bankState'] = val;
		},
		bankZip : function(val, state){
			state['bankZip'] = val;
		},
		IBAN : function(val, state){
			state['IBAN'] = val;
		},
		wireAccountType : function(val, state){
			state['wireAccountType'] = val;
		},
		internationalRoutingCode : function(val, state){
			state['internationalRoutingCode'] = val;
		},
		transactionId : function(val, state){
			state['transactionId'] = val;
		},
		isManuallyAdded : function(val, state){
			state['isManuallyAdded'] = val;
		},
		optionEdit : function(val, state){
			state['optionEdit'] = val;
		},
		optionDelete : function(val, state){
			state['optionDelete'] = val;
		},
		optionActivate : function(val, state){
			state['optionActivate'] = val;
		},
		optionDeactivate : function(val, state){
			state['optionDeactivate'] = val;
		},
		username : function(val, state){
			state['username'] = val;
		},
		Biller : function(val, state){
			state['Biller'] = val;
		},
		paymentMethod : function(val, state){
			state['paymentMethod'] = val;
		},
		custom : function(val, state){
			state['custom'] = val;
		},
		error : function(val, state){
			state['error'] = val;
		},
		nextAction : function(val, state){
			state['nextAction'] = val;
		},
		sessionId : function(val, state){
			state['sessionId'] = val;
		},
		key : function(val, state){
			state['key'] = val;
		},
		value : function(val, state){
			state['value'] = val;
		},
		pairs : function(val, state){
			state['pairs'] = val;
		},
		activate : function(val, state){
			state['activate'] = val;
		},
		businessDaysToDeliver : function(val, state){
			state['businessDaysToDeliver'] = val;
		},
		NextAvailablePaymentDeliveryDate : function(val, state){
			state['NextAvailablePaymentDeliveryDate'] = val;
		},
		nextAvailablePaymentProcessingDate : function(val, state){
			state['nextAvailablePaymentProcessingDate'] = val;
		},
		eBillDueDate : function(val, state){
			state['eBillDueDate'] = val;
		},
		eBillAmountDue : function(val, state){
			state['eBillAmountDue'] = val;
		},
		eBillBalance : function(val, state){
			state['eBillBalance'] = val;
		},
	};
	
	
	//Create the Model Class
	function Payee(defaultValues){
		var privateState = {};
			privateState.accountNumber = defaultValues?(defaultValues["accountNumber"]?defaultValues["accountNumber"]:null):null;
			privateState.addressLine1 = defaultValues?(defaultValues["addressLine1"]?defaultValues["addressLine1"]:null):null;
			privateState.addressLine2 = defaultValues?(defaultValues["addressLine2"]?defaultValues["addressLine2"]:null):null;
			privateState.billDescription = defaultValues?(defaultValues["billDescription"]?defaultValues["billDescription"]:null):null;
			privateState.billDueDate = defaultValues?(defaultValues["billDueDate"]?defaultValues["billDueDate"]:null):null;
			privateState.billerCategory = defaultValues?(defaultValues["billerCategory"]?defaultValues["billerCategory"]:null):null;
			privateState.billerId = defaultValues?(defaultValues["billerId"]?defaultValues["billerId"]:null):null;
			privateState.billGeneratedDate = defaultValues?(defaultValues["billGeneratedDate"]?defaultValues["billGeneratedDate"]:null):null;
			privateState.billid = defaultValues?(defaultValues["billid"]?defaultValues["billid"]:null):null;
			privateState.cityName = defaultValues?(defaultValues["cityName"]?defaultValues["cityName"]:null):null;
			privateState.companyName = defaultValues?(defaultValues["companyName"]?defaultValues["companyName"]:null):null;
			privateState.dueAmount = defaultValues?(defaultValues["dueAmount"]?defaultValues["dueAmount"]:null):null;
			privateState.EBillEnable = defaultValues?(defaultValues["EBillEnable"]?defaultValues["EBillEnable"]:null):null;
			privateState.eBillStatus = defaultValues?(defaultValues["eBillStatus"]?defaultValues["eBillStatus"]:null):null;
			privateState.eBillSupport = defaultValues?(defaultValues["eBillSupport"]?defaultValues["eBillSupport"]:null):null;
			privateState.ebillURL = defaultValues?(defaultValues["ebillURL"]?defaultValues["ebillURL"]:null):null;
			privateState.email = defaultValues?(defaultValues["email"]?defaultValues["email"]:null):null;
			privateState.errmsg = defaultValues?(defaultValues["errmsg"]?defaultValues["errmsg"]:null):null;
			privateState.lastPaidAmount = defaultValues?(defaultValues["lastPaidAmount"]?defaultValues["lastPaidAmount"]:null):null;
			privateState.lastPaidDate = defaultValues?(defaultValues["lastPaidDate"]?defaultValues["lastPaidDate"]:null):null;
			privateState.limit = defaultValues?(defaultValues["limit"]?defaultValues["limit"]:null):null;
			privateState.nameOnBill = defaultValues?(defaultValues["nameOnBill"]?defaultValues["nameOnBill"]:null):null;
			privateState.notes = defaultValues?(defaultValues["notes"]?defaultValues["notes"]:null):null;
			privateState.offset = defaultValues?(defaultValues["offset"]?defaultValues["offset"]:null):null;
			privateState.order = defaultValues?(defaultValues["order"]?defaultValues["order"]:null):null;
			privateState.paidAmount = defaultValues?(defaultValues["paidAmount"]?defaultValues["paidAmount"]:null):null;
			privateState.payeeAccountNumber = defaultValues?(defaultValues["payeeAccountNumber"]?defaultValues["payeeAccountNumber"]:null):null;
			privateState.payeeId = defaultValues?(defaultValues["payeeId"]?defaultValues["payeeId"]:null):null;
			privateState.payeeName = defaultValues?(defaultValues["payeeName"]?defaultValues["payeeName"]:null):null;
			privateState.payeeNickName = defaultValues?(defaultValues["payeeNickName"]?defaultValues["payeeNickName"]:null):null;
			privateState.phone = defaultValues?(defaultValues["phone"]?defaultValues["phone"]:null):null;
			privateState.searchString = defaultValues?(defaultValues["searchString"]?defaultValues["searchString"]:null):null;
			privateState.sortBy = defaultValues?(defaultValues["sortBy"]?defaultValues["sortBy"]:null):null;
			privateState.state = defaultValues?(defaultValues["state"]?defaultValues["state"]:null):null;
			privateState.street = defaultValues?(defaultValues["street"]?defaultValues["street"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.zipCode = defaultValues?(defaultValues["zipCode"]?defaultValues["zipCode"]:null):null;
			privateState.type = defaultValues?(defaultValues["type"]?defaultValues["type"]:null):null;
			privateState.country = defaultValues?(defaultValues["country"]?defaultValues["country"]:null):null;
			privateState.swiftCode = defaultValues?(defaultValues["swiftCode"]?defaultValues["swiftCode"]:null):null;
			privateState.routingCode = defaultValues?(defaultValues["routingCode"]?defaultValues["routingCode"]:null):null;
			privateState.bankName = defaultValues?(defaultValues["bankName"]?defaultValues["bankName"]:null):null;
			privateState.bankAddressLine1 = defaultValues?(defaultValues["bankAddressLine1"]?defaultValues["bankAddressLine1"]:null):null;
			privateState.bankAddressLine2 = defaultValues?(defaultValues["bankAddressLine2"]?defaultValues["bankAddressLine2"]:null):null;
			privateState.bankCity = defaultValues?(defaultValues["bankCity"]?defaultValues["bankCity"]:null):null;
			privateState.bankState = defaultValues?(defaultValues["bankState"]?defaultValues["bankState"]:null):null;
			privateState.bankZip = defaultValues?(defaultValues["bankZip"]?defaultValues["bankZip"]:null):null;
			privateState.IBAN = defaultValues?(defaultValues["IBAN"]?defaultValues["IBAN"]:null):null;
			privateState.wireAccountType = defaultValues?(defaultValues["wireAccountType"]?defaultValues["wireAccountType"]:null):null;
			privateState.internationalRoutingCode = defaultValues?(defaultValues["internationalRoutingCode"]?defaultValues["internationalRoutingCode"]:null):null;
			privateState.transactionId = defaultValues?(defaultValues["transactionId"]?defaultValues["transactionId"]:null):null;
			privateState.isManuallyAdded = defaultValues?(defaultValues["isManuallyAdded"]?defaultValues["isManuallyAdded"]:null):null;
			privateState.optionEdit = defaultValues?(defaultValues["optionEdit"]?defaultValues["optionEdit"]:null):null;
			privateState.optionDelete = defaultValues?(defaultValues["optionDelete"]?defaultValues["optionDelete"]:null):null;
			privateState.optionActivate = defaultValues?(defaultValues["optionActivate"]?defaultValues["optionActivate"]:null):null;
			privateState.optionDeactivate = defaultValues?(defaultValues["optionDeactivate"]?defaultValues["optionDeactivate"]:null):null;
			privateState.username = defaultValues?(defaultValues["username"]?defaultValues["username"]:null):null;
			privateState.Biller = defaultValues?(defaultValues["Biller"]?defaultValues["Biller"]:null):null;
			privateState.paymentMethod = defaultValues?(defaultValues["paymentMethod"]?defaultValues["paymentMethod"]:null):null;
			privateState.custom = defaultValues?(defaultValues["custom"]?defaultValues["custom"]:null):null;
			privateState.error = defaultValues?(defaultValues["error"]?defaultValues["error"]:null):null;
			privateState.nextAction = defaultValues?(defaultValues["nextAction"]?defaultValues["nextAction"]:null):null;
			privateState.sessionId = defaultValues?(defaultValues["sessionId"]?defaultValues["sessionId"]:null):null;
			privateState.key = defaultValues?(defaultValues["key"]?defaultValues["key"]:null):null;
			privateState.value = defaultValues?(defaultValues["value"]?defaultValues["value"]:null):null;
			privateState.pairs = defaultValues?(defaultValues["pairs"]?defaultValues["pairs"]:null):null;
			privateState.activate = defaultValues?(defaultValues["activate"]?defaultValues["activate"]:null):null;
			privateState.businessDaysToDeliver = defaultValues?(defaultValues["businessDaysToDeliver"]?defaultValues["businessDaysToDeliver"]:null):null;
			privateState.NextAvailablePaymentDeliveryDate = defaultValues?(defaultValues["NextAvailablePaymentDeliveryDate"]?defaultValues["NextAvailablePaymentDeliveryDate"]:null):null;
			privateState.nextAvailablePaymentProcessingDate = defaultValues?(defaultValues["nextAvailablePaymentProcessingDate"]?defaultValues["nextAvailablePaymentProcessingDate"]:null):null;
			privateState.eBillDueDate = defaultValues?(defaultValues["eBillDueDate"]?defaultValues["eBillDueDate"]:null):null;
			privateState.eBillAmountDue = defaultValues?(defaultValues["eBillAmountDue"]?defaultValues["eBillAmountDue"]:null):null;
			privateState.eBillBalance = defaultValues?(defaultValues["eBillBalance"]?defaultValues["eBillBalance"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"accountNumber" : {
					get : function(){return privateState.accountNumber},
					set : function(val){
						setterFunctions['accountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"addressLine1" : {
					get : function(){return privateState.addressLine1},
					set : function(val){
						setterFunctions['addressLine1'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"addressLine2" : {
					get : function(){return privateState.addressLine2},
					set : function(val){
						setterFunctions['addressLine2'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billDescription" : {
					get : function(){return privateState.billDescription},
					set : function(val){
						setterFunctions['billDescription'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billDueDate" : {
					get : function(){return privateState.billDueDate},
					set : function(val){
						setterFunctions['billDueDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billerCategory" : {
					get : function(){return privateState.billerCategory},
					set : function(val){
						setterFunctions['billerCategory'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billerId" : {
					get : function(){return privateState.billerId},
					set : function(val){
						setterFunctions['billerId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billGeneratedDate" : {
					get : function(){return privateState.billGeneratedDate},
					set : function(val){
						setterFunctions['billGeneratedDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billid" : {
					get : function(){return privateState.billid},
					set : function(val){
						setterFunctions['billid'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cityName" : {
					get : function(){return privateState.cityName},
					set : function(val){
						setterFunctions['cityName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"companyName" : {
					get : function(){return privateState.companyName},
					set : function(val){
						setterFunctions['companyName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"dueAmount" : {
					get : function(){return privateState.dueAmount},
					set : function(val){
						setterFunctions['dueAmount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"EBillEnable" : {
					get : function(){return privateState.EBillEnable},
					set : function(val){
						setterFunctions['EBillEnable'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"eBillStatus" : {
					get : function(){return privateState.eBillStatus},
					set : function(val){
						setterFunctions['eBillStatus'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"eBillSupport" : {
					get : function(){return privateState.eBillSupport},
					set : function(val){
						setterFunctions['eBillSupport'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"ebillURL" : {
					get : function(){return privateState.ebillURL},
					set : function(val){
						setterFunctions['ebillURL'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"email" : {
					get : function(){return privateState.email},
					set : function(val){
						setterFunctions['email'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"errmsg" : {
					get : function(){return privateState.errmsg},
					set : function(val){
						setterFunctions['errmsg'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"lastPaidAmount" : {
					get : function(){return privateState.lastPaidAmount},
					set : function(val){
						setterFunctions['lastPaidAmount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"lastPaidDate" : {
					get : function(){return privateState.lastPaidDate},
					set : function(val){
						setterFunctions['lastPaidDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"limit" : {
					get : function(){return privateState.limit},
					set : function(val){
						setterFunctions['limit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"nameOnBill" : {
					get : function(){return privateState.nameOnBill},
					set : function(val){
						setterFunctions['nameOnBill'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"notes" : {
					get : function(){return privateState.notes},
					set : function(val){
						setterFunctions['notes'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"offset" : {
					get : function(){return privateState.offset},
					set : function(val){
						setterFunctions['offset'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"order" : {
					get : function(){return privateState.order},
					set : function(val){
						setterFunctions['order'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"paidAmount" : {
					get : function(){return privateState.paidAmount},
					set : function(val){
						setterFunctions['paidAmount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"payeeAccountNumber" : {
					get : function(){return privateState.payeeAccountNumber},
					set : function(val){
						setterFunctions['payeeAccountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"payeeId" : {
					get : function(){return privateState.payeeId},
					set : function(val){throw Error("payeeId cannot be changed."); },
					enumerable : true,
				},
				"payeeName" : {
					get : function(){return privateState.payeeName},
					set : function(val){
						setterFunctions['payeeName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"payeeNickName" : {
					get : function(){return privateState.payeeNickName},
					set : function(val){
						setterFunctions['payeeNickName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"phone" : {
					get : function(){return privateState.phone},
					set : function(val){
						setterFunctions['phone'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"searchString" : {
					get : function(){return privateState.searchString},
					set : function(val){
						setterFunctions['searchString'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"sortBy" : {
					get : function(){return privateState.sortBy},
					set : function(val){
						setterFunctions['sortBy'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"state" : {
					get : function(){return privateState.state},
					set : function(val){
						setterFunctions['state'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"street" : {
					get : function(){return privateState.street},
					set : function(val){
						setterFunctions['street'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"zipCode" : {
					get : function(){return privateState.zipCode},
					set : function(val){
						setterFunctions['zipCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"type" : {
					get : function(){return privateState.type},
					set : function(val){
						setterFunctions['type'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"country" : {
					get : function(){return privateState.country},
					set : function(val){
						setterFunctions['country'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"swiftCode" : {
					get : function(){return privateState.swiftCode},
					set : function(val){
						setterFunctions['swiftCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"routingCode" : {
					get : function(){return privateState.routingCode},
					set : function(val){
						setterFunctions['routingCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bankName" : {
					get : function(){return privateState.bankName},
					set : function(val){
						setterFunctions['bankName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bankAddressLine1" : {
					get : function(){return privateState.bankAddressLine1},
					set : function(val){
						setterFunctions['bankAddressLine1'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bankAddressLine2" : {
					get : function(){return privateState.bankAddressLine2},
					set : function(val){
						setterFunctions['bankAddressLine2'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bankCity" : {
					get : function(){return privateState.bankCity},
					set : function(val){
						setterFunctions['bankCity'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bankState" : {
					get : function(){return privateState.bankState},
					set : function(val){
						setterFunctions['bankState'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"bankZip" : {
					get : function(){return privateState.bankZip},
					set : function(val){
						setterFunctions['bankZip'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"IBAN" : {
					get : function(){return privateState.IBAN},
					set : function(val){
						setterFunctions['IBAN'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"wireAccountType" : {
					get : function(){return privateState.wireAccountType},
					set : function(val){
						setterFunctions['wireAccountType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"internationalRoutingCode" : {
					get : function(){return privateState.internationalRoutingCode},
					set : function(val){
						setterFunctions['internationalRoutingCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"transactionId" : {
					get : function(){return privateState.transactionId},
					set : function(val){
						setterFunctions['transactionId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"isManuallyAdded" : {
					get : function(){return privateState.isManuallyAdded},
					set : function(val){
						setterFunctions['isManuallyAdded'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"optionEdit" : {
					get : function(){return privateState.optionEdit},
					set : function(val){
						setterFunctions['optionEdit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"optionDelete" : {
					get : function(){return privateState.optionDelete},
					set : function(val){
						setterFunctions['optionDelete'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"optionActivate" : {
					get : function(){return privateState.optionActivate},
					set : function(val){
						setterFunctions['optionActivate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"optionDeactivate" : {
					get : function(){return privateState.optionDeactivate},
					set : function(val){
						setterFunctions['optionDeactivate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"username" : {
					get : function(){return privateState.username},
					set : function(val){
						setterFunctions['username'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Biller" : {
					get : function(){return privateState.Biller},
					set : function(val){
						setterFunctions['Biller'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"paymentMethod" : {
					get : function(){return privateState.paymentMethod},
					set : function(val){
						setterFunctions['paymentMethod'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"custom" : {
					get : function(){return privateState.custom},
					set : function(val){
						setterFunctions['custom'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"error" : {
					get : function(){return privateState.error},
					set : function(val){
						setterFunctions['error'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"nextAction" : {
					get : function(){return privateState.nextAction},
					set : function(val){
						setterFunctions['nextAction'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"sessionId" : {
					get : function(){return privateState.sessionId},
					set : function(val){
						setterFunctions['sessionId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"key" : {
					get : function(){return privateState.key},
					set : function(val){
						setterFunctions['key'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"value" : {
					get : function(){return privateState.value},
					set : function(val){
						setterFunctions['value'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"pairs" : {
					get : function(){return privateState.pairs},
					set : function(val){
						setterFunctions['pairs'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"activate" : {
					get : function(){return privateState.activate},
					set : function(val){
						setterFunctions['activate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"businessDaysToDeliver" : {
					get : function(){return privateState.businessDaysToDeliver},
					set : function(val){
						setterFunctions['businessDaysToDeliver'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"NextAvailablePaymentDeliveryDate" : {
					get : function(){return privateState.NextAvailablePaymentDeliveryDate},
					set : function(val){
						setterFunctions['NextAvailablePaymentDeliveryDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"nextAvailablePaymentProcessingDate" : {
					get : function(){return privateState.nextAvailablePaymentProcessingDate},
					set : function(val){
						setterFunctions['nextAvailablePaymentProcessingDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"eBillDueDate" : {
					get : function(){return privateState.eBillDueDate},
					set : function(val){
						setterFunctions['eBillDueDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"eBillAmountDue" : {
					get : function(){return privateState.eBillAmountDue},
					set : function(val){
						setterFunctions['eBillAmountDue'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"eBillBalance" : {
					get : function(){return privateState.eBillBalance},
					set : function(val){
						setterFunctions['eBillBalance'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(Payee);
	
	//Create new class level validator object
	BaseModel.Validator.call(Payee);
	
	var registerValidatorBackup = Payee.registerValidator;
	
	Payee.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( Payee.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'GetPaymentOptions' with service id 'getPaymentOptions3173'
	Payee.GetPaymentOptions = function(params, onCompletion){
		return Payee.customVerb('GetPaymentOptions', params, onCompletion);
	};
	//For Operation 'updateRecipient' with service id 'updateRecipient9351'
	Payee.updateRecipient = function(params, onCompletion){
		return Payee.customVerb('updateRecipient', params, onCompletion);
	};
	//For Operation 'eBillAuthentication' with service id 'eBillAuthentication6162'
	Payee.eBillAuthentication = function(params, onCompletion){
		return Payee.customVerb('eBillAuthentication', params, onCompletion);
	};
	//For Operation 'getWireTransferRecipient' with service id 'getWireTransferRecipient4881'
	Payee.getWireTransferRecipient = function(params, onCompletion){
		return Payee.customVerb('getWireTransferRecipient', params, onCompletion);
	};
	//For Operation 'addRecipient' with service id 'addRecipient4640'
	Payee.addRecipient = function(params, onCompletion){
		return Payee.customVerb('addRecipient', params, onCompletion);
	};
	//For Operation 'selecteBillSite' with service id 'selecteBillSite6084'
	Payee.selecteBillSite = function(params, onCompletion){
		return Payee.customVerb('selecteBillSite', params, onCompletion);
	};
	//For Operation 'editPayee' with service id 'editPayee4489'
	Payee.editPayee = function(params, onCompletion){
		return Payee.customVerb('editPayee', params, onCompletion);
	};
	//For Operation 'saveRecipientAfterWireTransfer' with service id 'saveRecipientAfterWireTransfer1796'
	Payee.saveRecipientAfterWireTransfer = function(params, onCompletion){
		return Payee.customVerb('saveRecipientAfterWireTransfer', params, onCompletion);
	};
	//For Operation 'deletePayee' with service id 'deletePayee9154'
	Payee.deletePayee = function(params, onCompletion){
		return Payee.customVerb('deletePayee', params, onCompletion);
	};
	//For Operation 'addPayee' with service id 'addPayee7490'
	Payee.addPayee = function(params, onCompletion){
		return Payee.customVerb('addPayee', params, onCompletion);
	};
	//For Operation 'deleteEBill' with service id 'deleteEBill9021'
	Payee.deleteEBill = function(params, onCompletion){
		return Payee.customVerb('deleteEBill', params, onCompletion);
	};
	//For Operation 'getRecentPayee' with service id 'getRecentPayees8811'
	Payee.getRecentPayee = function(params, onCompletion){
		return Payee.customVerb('getRecentPayee', params, onCompletion);
	};
	//For Operation 'addeBill' with service id 'addEBill1193'
	Payee.addeBill = function(params, onCompletion){
		return Payee.customVerb('addeBill', params, onCompletion);
	};
	
	var relations = [
	];
	
	Payee.relations = relations;
	
	Payee.prototype.isValid = function(){
		return Payee.isValid(this);
	};
	
	Payee.prototype.objModelName = "Payee";
	
	return Payee;
});