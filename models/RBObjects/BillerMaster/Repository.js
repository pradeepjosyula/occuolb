define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function BillerMasterRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	BillerMasterRepository.prototype = Object.create(BaseRepository.prototype);
	BillerMasterRepository.prototype.constructor = BillerMasterRepository;

	//For Operation 'validateBillerByAccount' with service id 'validateBillerAccount9055'
	BillerMasterRepository.prototype.validateBillerByAccount = function(params,onCompletion){
		return BillerMasterRepository.prototype.customVerb('validateBillerByAccount',params, onCompletion);
	};
	//For Operation 'searchBillerByName' with service id 'searchBillers5549'
	BillerMasterRepository.prototype.searchBillerByName = function(params,onCompletion){
		return BillerMasterRepository.prototype.customVerb('searchBillerByName',params, onCompletion);
	};
	//For Operation 'getBillerByAccountNumber' with service id 'getBillerByAccountNumber7970'
	BillerMasterRepository.prototype.getBillerByAccountNumber = function(params,onCompletion){
		return BillerMasterRepository.prototype.customVerb('getBillerByAccountNumber',params, onCompletion);
	};
	
	
	return BillerMasterRepository;
})