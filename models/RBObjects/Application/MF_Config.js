define([],function(){
	var mappings = {
		"AppVersion" : "AppVersion",
		"bankName" : "bankName",
		"bannerURL" : "bannerURL",
		"branchType" : "branchType",
		"businessDays" : "businessDays",
		"currencyCode" : "currencyCode",
		"distanceUnit" : "distanceUnit",
		"errmsg" : "errmsg",
		"facialLicenseServerUrl" : "facialLicenseServerUrl",
		"facialLicenseString" : "facialLicenseString",
		"isUpdate" : "isUpdate",
		"isUpdateMandatory" : "isUpdateMandatory",
		"MainBranchaddressLine1" : "MainBranchaddressLine1",
		"MainBranchaddressLine2" : "MainBranchaddressLine2",
		"MainBranchCity" : "MainBranchCity",
		"MainBranchLatitude" : "MainBranchLatitude",
		"MainBranchLongitude" : "MainBranchLongitude",
		"MainBranchPhone" : "MainBranchPhone",
		"MainBranchServices" : "MainBranchServices",
		"MainBranchState" : "MainBranchState",
		"MainBranchWorkingHours" : "MainBranchWorkingHours",
		"MainBranchzipCode" : "MainBranchzipCode",
		"ocrApiKey" : "ocrApiKey",
		"ocrSecretKey" : "ocrSecretKey",
		"OSType" : "OSType",
		"OSversion" : "OSversion",
		"versionLink" : "versionLink",
	};
	Object.freeze(mappings);
	
	var typings = {
		"AppVersion" : "string",
		"bankName" : "string",
		"bannerURL" : "string",
		"branchType" : "string",
		"businessDays" : "string",
		"currencyCode" : "string",
		"distanceUnit" : "string",
		"errmsg" : "string",
		"facialLicenseServerUrl" : "string",
		"facialLicenseString" : "string",
		"isUpdate" : "boolean",
		"isUpdateMandatory" : "boolean",
		"MainBranchaddressLine1" : "string",
		"MainBranchaddressLine2" : "string",
		"MainBranchCity" : "string",
		"MainBranchLatitude" : "string",
		"MainBranchLongitude" : "string",
		"MainBranchPhone" : "string",
		"MainBranchServices" : "string",
		"MainBranchState" : "string",
		"MainBranchWorkingHours" : "string",
		"MainBranchzipCode" : "string",
		"ocrApiKey" : "string",
		"ocrSecretKey" : "string",
		"OSType" : "string",
		"OSversion" : "string",
		"versionLink" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "Application"
	};
	Object.freeze(config);
	
	return config;
})
