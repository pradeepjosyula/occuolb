define([],function(){
	var mappings = {
		"accountid" : "accountid",
		"createdby" : "createdby",
		"customer_id" : "customer_id",
		"filename" : "filename",
		"harddelete" : "harddelete",
		"markallasread" : "markallasread",
		"media_id" : "media_id",
		"MessageDescription" : "MessageDescription",
		"modifiedby" : "modifiedby",
		"priority" : "priority",
		"requestcategory_id" : "requestcategory_id",
		"requestid" : "requestid",
		"requestsubject" : "requestsubject",
		"softdelete" : "softdelete",
		"softDeleteFlag" : "softDeleteFlag",
		"Status" : "Status",
		"username" : "username",
		"status_id" : "status_id",
		"requestCreatedDate" : "requestCreatedDate",
		"id" : "id",
		"ReplySequence" : "ReplySequence",
		"lastmodifiedts" : "lastmodifiedts",
		"CustomerRequest_id" : "CustomerRequest_id",
		"synctimestamp" : "synctimestamp",
		"IsRead" : "IsRead",
		"createdts" : "createdts",
		"filter" : "filter",
		"categoryName" : "categoryName",
		"categoryEmail" : "categoryEmail",
		"repliedBy" : "repliedBy",
	};
	Object.freeze(mappings);
	
	var typings = {
		"accountid" : "string",
		"createdby" : "string",
		"customer_id" : "string",
		"filename" : "string",
		"harddelete" : "string",
		"markallasread" : "string",
		"media_id" : "string",
		"MessageDescription" : "string",
		"modifiedby" : "string",
		"priority" : "string",
		"requestcategory_id" : "string",
		"requestid" : "string",
		"requestsubject" : "string",
		"softdelete" : "string",
		"softDeleteFlag" : "string",
		"Status" : "string",
		"username" : "string",
		"status_id" : "string",
		"requestCreatedDate" : "string",
		"id" : "string",
		"ReplySequence" : "string",
		"lastmodifiedts" : "string",
		"CustomerRequest_id" : "string",
		"synctimestamp" : "string",
		"IsRead" : "string",
		"createdts" : "string",
		"filter" : "string",
		"categoryName" : "string",
		"categoryEmail" : "string",
		"repliedBy" : "number",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"customer_id",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "SecureMessaging"
	};
	Object.freeze(config);
	
	return config;
})
