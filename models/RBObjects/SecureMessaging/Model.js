define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		accountid : function(val, state){
			state['accountid'] = val;
		},
		createdby : function(val, state){
			state['createdby'] = val;
		},
		customer_id : function(val, state){
			state['customer_id'] = val;
		},
		filename : function(val, state){
			state['filename'] = val;
		},
		harddelete : function(val, state){
			state['harddelete'] = val;
		},
		markallasread : function(val, state){
			state['markallasread'] = val;
		},
		media_id : function(val, state){
			state['media_id'] = val;
		},
		MessageDescription : function(val, state){
			state['MessageDescription'] = val;
		},
		modifiedby : function(val, state){
			state['modifiedby'] = val;
		},
		priority : function(val, state){
			state['priority'] = val;
		},
		requestcategory_id : function(val, state){
			state['requestcategory_id'] = val;
		},
		requestid : function(val, state){
			state['requestid'] = val;
		},
		requestsubject : function(val, state){
			state['requestsubject'] = val;
		},
		softdelete : function(val, state){
			state['softdelete'] = val;
		},
		softDeleteFlag : function(val, state){
			state['softDeleteFlag'] = val;
		},
		Status : function(val, state){
			state['Status'] = val;
		},
		username : function(val, state){
			state['username'] = val;
		},
		status_id : function(val, state){
			state['status_id'] = val;
		},
		requestCreatedDate : function(val, state){
			state['requestCreatedDate'] = val;
		},
		id : function(val, state){
			state['id'] = val;
		},
		ReplySequence : function(val, state){
			state['ReplySequence'] = val;
		},
		lastmodifiedts : function(val, state){
			state['lastmodifiedts'] = val;
		},
		CustomerRequest_id : function(val, state){
			state['CustomerRequest_id'] = val;
		},
		synctimestamp : function(val, state){
			state['synctimestamp'] = val;
		},
		IsRead : function(val, state){
			state['IsRead'] = val;
		},
		createdts : function(val, state){
			state['createdts'] = val;
		},
		filter : function(val, state){
			state['filter'] = val;
		},
		categoryName : function(val, state){
			state['categoryName'] = val;
		},
		categoryEmail : function(val, state){
			state['categoryEmail'] = val;
		},
		repliedBy : function(val, state){
			state['repliedBy'] = val;
		},
	};
	
	
	//Create the Model Class
	function SecureMessaging(defaultValues){
		var privateState = {};
			privateState.accountid = defaultValues?(defaultValues["accountid"]?defaultValues["accountid"]:null):null;
			privateState.createdby = defaultValues?(defaultValues["createdby"]?defaultValues["createdby"]:null):null;
			privateState.customer_id = defaultValues?(defaultValues["customer_id"]?defaultValues["customer_id"]:null):null;
			privateState.filename = defaultValues?(defaultValues["filename"]?defaultValues["filename"]:null):null;
			privateState.harddelete = defaultValues?(defaultValues["harddelete"]?defaultValues["harddelete"]:null):null;
			privateState.markallasread = defaultValues?(defaultValues["markallasread"]?defaultValues["markallasread"]:null):null;
			privateState.media_id = defaultValues?(defaultValues["media_id"]?defaultValues["media_id"]:null):null;
			privateState.MessageDescription = defaultValues?(defaultValues["MessageDescription"]?defaultValues["MessageDescription"]:null):null;
			privateState.modifiedby = defaultValues?(defaultValues["modifiedby"]?defaultValues["modifiedby"]:null):null;
			privateState.priority = defaultValues?(defaultValues["priority"]?defaultValues["priority"]:null):null;
			privateState.requestcategory_id = defaultValues?(defaultValues["requestcategory_id"]?defaultValues["requestcategory_id"]:null):null;
			privateState.requestid = defaultValues?(defaultValues["requestid"]?defaultValues["requestid"]:null):null;
			privateState.requestsubject = defaultValues?(defaultValues["requestsubject"]?defaultValues["requestsubject"]:null):null;
			privateState.softdelete = defaultValues?(defaultValues["softdelete"]?defaultValues["softdelete"]:null):null;
			privateState.softDeleteFlag = defaultValues?(defaultValues["softDeleteFlag"]?defaultValues["softDeleteFlag"]:null):null;
			privateState.Status = defaultValues?(defaultValues["Status"]?defaultValues["Status"]:null):null;
			privateState.username = defaultValues?(defaultValues["username"]?defaultValues["username"]:null):null;
			privateState.status_id = defaultValues?(defaultValues["status_id"]?defaultValues["status_id"]:null):null;
			privateState.requestCreatedDate = defaultValues?(defaultValues["requestCreatedDate"]?defaultValues["requestCreatedDate"]:null):null;
			privateState.id = defaultValues?(defaultValues["id"]?defaultValues["id"]:null):null;
			privateState.ReplySequence = defaultValues?(defaultValues["ReplySequence"]?defaultValues["ReplySequence"]:null):null;
			privateState.lastmodifiedts = defaultValues?(defaultValues["lastmodifiedts"]?defaultValues["lastmodifiedts"]:null):null;
			privateState.CustomerRequest_id = defaultValues?(defaultValues["CustomerRequest_id"]?defaultValues["CustomerRequest_id"]:null):null;
			privateState.synctimestamp = defaultValues?(defaultValues["synctimestamp"]?defaultValues["synctimestamp"]:null):null;
			privateState.IsRead = defaultValues?(defaultValues["IsRead"]?defaultValues["IsRead"]:null):null;
			privateState.createdts = defaultValues?(defaultValues["createdts"]?defaultValues["createdts"]:null):null;
			privateState.filter = defaultValues?(defaultValues["filter"]?defaultValues["filter"]:null):null;
			privateState.categoryName = defaultValues?(defaultValues["categoryName"]?defaultValues["categoryName"]:null):null;
			privateState.categoryEmail = defaultValues?(defaultValues["categoryEmail"]?defaultValues["categoryEmail"]:null):null;
			privateState.repliedBy = defaultValues?(defaultValues["repliedBy"]?defaultValues["repliedBy"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"accountid" : {
					get : function(){return privateState.accountid},
					set : function(val){
						setterFunctions['accountid'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"createdby" : {
					get : function(){return privateState.createdby},
					set : function(val){
						setterFunctions['createdby'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"customer_id" : {
					get : function(){return privateState.customer_id},
					set : function(val){throw Error("customer_id cannot be changed."); },
					enumerable : true,
				},
				"filename" : {
					get : function(){return privateState.filename},
					set : function(val){
						setterFunctions['filename'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"harddelete" : {
					get : function(){return privateState.harddelete},
					set : function(val){
						setterFunctions['harddelete'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"markallasread" : {
					get : function(){return privateState.markallasread},
					set : function(val){
						setterFunctions['markallasread'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"media_id" : {
					get : function(){return privateState.media_id},
					set : function(val){
						setterFunctions['media_id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"MessageDescription" : {
					get : function(){return privateState.MessageDescription},
					set : function(val){
						setterFunctions['MessageDescription'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"modifiedby" : {
					get : function(){return privateState.modifiedby},
					set : function(val){
						setterFunctions['modifiedby'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"priority" : {
					get : function(){return privateState.priority},
					set : function(val){
						setterFunctions['priority'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"requestcategory_id" : {
					get : function(){return privateState.requestcategory_id},
					set : function(val){
						setterFunctions['requestcategory_id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"requestid" : {
					get : function(){return privateState.requestid},
					set : function(val){
						setterFunctions['requestid'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"requestsubject" : {
					get : function(){return privateState.requestsubject},
					set : function(val){
						setterFunctions['requestsubject'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"softdelete" : {
					get : function(){return privateState.softdelete},
					set : function(val){
						setterFunctions['softdelete'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"softDeleteFlag" : {
					get : function(){return privateState.softDeleteFlag},
					set : function(val){
						setterFunctions['softDeleteFlag'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Status" : {
					get : function(){return privateState.Status},
					set : function(val){
						setterFunctions['Status'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"username" : {
					get : function(){return privateState.username},
					set : function(val){
						setterFunctions['username'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"status_id" : {
					get : function(){return privateState.status_id},
					set : function(val){
						setterFunctions['status_id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"requestCreatedDate" : {
					get : function(){return privateState.requestCreatedDate},
					set : function(val){
						setterFunctions['requestCreatedDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"id" : {
					get : function(){return privateState.id},
					set : function(val){
						setterFunctions['id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"ReplySequence" : {
					get : function(){return privateState.ReplySequence},
					set : function(val){
						setterFunctions['ReplySequence'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"lastmodifiedts" : {
					get : function(){return privateState.lastmodifiedts},
					set : function(val){
						setterFunctions['lastmodifiedts'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"CustomerRequest_id" : {
					get : function(){return privateState.CustomerRequest_id},
					set : function(val){
						setterFunctions['CustomerRequest_id'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"synctimestamp" : {
					get : function(){return privateState.synctimestamp},
					set : function(val){
						setterFunctions['synctimestamp'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"IsRead" : {
					get : function(){return privateState.IsRead},
					set : function(val){
						setterFunctions['IsRead'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"createdts" : {
					get : function(){return privateState.createdts},
					set : function(val){
						setterFunctions['createdts'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"filter" : {
					get : function(){return privateState.filter},
					set : function(val){
						setterFunctions['filter'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"categoryName" : {
					get : function(){return privateState.categoryName},
					set : function(val){
						setterFunctions['categoryName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"categoryEmail" : {
					get : function(){return privateState.categoryEmail},
					set : function(val){
						setterFunctions['categoryEmail'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"repliedBy" : {
					get : function(){return privateState.repliedBy},
					set : function(val){
						setterFunctions['repliedBy'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(SecureMessaging);
	
	//Create new class level validator object
	BaseModel.Validator.call(SecureMessaging);
	
	var registerValidatorBackup = SecureMessaging.registerValidator;
	
	SecureMessaging.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( SecureMessaging.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'createMessageByCustomerRequest' with service id 'dbo_requestmessage_create8720'
	SecureMessaging.createMessageByCustomerRequest = function(params, onCompletion){
		return SecureMessaging.customVerb('createMessageByCustomerRequest', params, onCompletion);
	};
	//For Operation 'updateRequest' with service id 'updateRequest4522'
	SecureMessaging.updateRequest = function(params, onCompletion){
		return SecureMessaging.customVerb('updateRequest', params, onCompletion);
	};
	//For Operation 'getRequestCategory' with service id 'dbo_requestcategory_get3212'
	SecureMessaging.getRequestCategory = function(params, onCompletion){
		return SecureMessaging.customVerb('getRequestCategory', params, onCompletion);
	};
	//For Operation 'getRequests' with service id 'dbo_customerrequest_get4026'
	SecureMessaging.getRequests = function(params, onCompletion){
		return SecureMessaging.customVerb('getRequests', params, onCompletion);
	};
	//For Operation 'getAllMessagesForARequest' with service id 'dbo_requestmessage_get4317'
	SecureMessaging.getAllMessagesForARequest = function(params, onCompletion){
		return SecureMessaging.customVerb('getAllMessagesForARequest', params, onCompletion);
	};
	//For Operation 'getUnreadMessageCount' with service id 'dbo_requestmessage_get7727'
	SecureMessaging.getUnreadMessageCount = function(params, onCompletion){
		return SecureMessaging.customVerb('getUnreadMessageCount', params, onCompletion);
	};
	//For Operation 'createCustomerRequest' with service id 'dbo_customerrequest_create3517'
	SecureMessaging.createCustomerRequest = function(params, onCompletion){
		return SecureMessaging.customVerb('createCustomerRequest', params, onCompletion);
	};
	//For Operation 'CreateNewCustomerRequestWithoutAttachment' with service id 'CreateNewCustomerRequest_RB9723'
	SecureMessaging.CreateNewCustomerRequestWithoutAttachment = function(params, onCompletion){
		return SecureMessaging.customVerb('CreateNewCustomerRequestWithoutAttachment', params, onCompletion);
	};
	//For Operation 'getMessageAttachment' with service id 'getMessageAttachment5345'
	SecureMessaging.getMessageAttachment = function(params, onCompletion){
		return SecureMessaging.customVerb('getMessageAttachment', params, onCompletion);
	};
	
	var relations = [
	];
	
	SecureMessaging.relations = relations;
	
	SecureMessaging.prototype.isValid = function(){
		return SecureMessaging.isValid(this);
	};
	
	SecureMessaging.prototype.objModelName = "SecureMessaging";
	
	return SecureMessaging;
});