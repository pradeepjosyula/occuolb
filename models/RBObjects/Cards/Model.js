define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		accountId : function(val, state){
			state['accountId'] = val;
		},
		Action : function(val, state){
			state['Action'] = val;
		},
		cardHolderName : function(val, state){
			state['cardHolderName'] = val;
		},
		cardId : function(val, state){
			state['cardId'] = val;
		},
		cardNumber : function(val, state){
			state['cardNumber'] = val;
		},
		cardStatus : function(val, state){
			state['cardStatus'] = val;
		},
		cardType : function(val, state){
			state['cardType'] = val;
		},
		errmsg : function(val, state){
			state['errmsg'] = val;
		},
		expiryDate : function(val, state){
			state['expiryDate'] = val;
		},
		Reason : function(val, state){
			state['Reason'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		userId : function(val, state){
			state['userId'] = val;
		},
		userName : function(val, state){
			state['userName'] = val;
		},
		creditLimit : function(val, state){
			state['creditLimit'] = val;
		},
		availableCredit : function(val, state){
			state['availableCredit'] = val;
		},
		serviceProvider : function(val, state){
			state['serviceProvider'] = val;
		},
		billingAddress : function(val, state){
			state['billingAddress'] = val;
		},
		cardProductName : function(val, state){
			state['cardProductName'] = val;
		},
		secondaryCardHolder : function(val, state){
			state['secondaryCardHolder'] = val;
		},
		withdrawlLimit : function(val, state){
			state['withdrawlLimit'] = val;
		},
		accountNumber : function(val, state){
			state['accountNumber'] = val;
		},
		accountName : function(val, state){
			state['accountName'] = val;
		},
		maskedAccountNumber : function(val, state){
			state['maskedAccountNumber'] = val;
		},
		maskedCardNumber : function(val, state){
			state['maskedCardNumber'] = val;
		},
	};
	
	
	//Create the Model Class
	function Cards(defaultValues){
		var privateState = {};
			privateState.accountId = defaultValues?(defaultValues["accountId"]?defaultValues["accountId"]:null):null;
			privateState.Action = defaultValues?(defaultValues["Action"]?defaultValues["Action"]:null):null;
			privateState.cardHolderName = defaultValues?(defaultValues["cardHolderName"]?defaultValues["cardHolderName"]:null):null;
			privateState.cardId = defaultValues?(defaultValues["cardId"]?defaultValues["cardId"]:null):null;
			privateState.cardNumber = defaultValues?(defaultValues["cardNumber"]?defaultValues["cardNumber"]:null):null;
			privateState.cardStatus = defaultValues?(defaultValues["cardStatus"]?defaultValues["cardStatus"]:null):null;
			privateState.cardType = defaultValues?(defaultValues["cardType"]?defaultValues["cardType"]:null):null;
			privateState.errmsg = defaultValues?(defaultValues["errmsg"]?defaultValues["errmsg"]:null):null;
			privateState.expiryDate = defaultValues?(defaultValues["expiryDate"]?defaultValues["expiryDate"]:null):null;
			privateState.Reason = defaultValues?(defaultValues["Reason"]?defaultValues["Reason"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.userId = defaultValues?(defaultValues["userId"]?defaultValues["userId"]:null):null;
			privateState.userName = defaultValues?(defaultValues["userName"]?defaultValues["userName"]:null):null;
			privateState.creditLimit = defaultValues?(defaultValues["creditLimit"]?defaultValues["creditLimit"]:null):null;
			privateState.availableCredit = defaultValues?(defaultValues["availableCredit"]?defaultValues["availableCredit"]:null):null;
			privateState.serviceProvider = defaultValues?(defaultValues["serviceProvider"]?defaultValues["serviceProvider"]:null):null;
			privateState.billingAddress = defaultValues?(defaultValues["billingAddress"]?defaultValues["billingAddress"]:null):null;
			privateState.cardProductName = defaultValues?(defaultValues["cardProductName"]?defaultValues["cardProductName"]:null):null;
			privateState.secondaryCardHolder = defaultValues?(defaultValues["secondaryCardHolder"]?defaultValues["secondaryCardHolder"]:null):null;
			privateState.withdrawlLimit = defaultValues?(defaultValues["withdrawlLimit"]?defaultValues["withdrawlLimit"]:null):null;
			privateState.accountNumber = defaultValues?(defaultValues["accountNumber"]?defaultValues["accountNumber"]:null):null;
			privateState.accountName = defaultValues?(defaultValues["accountName"]?defaultValues["accountName"]:null):null;
			privateState.maskedAccountNumber = defaultValues?(defaultValues["maskedAccountNumber"]?defaultValues["maskedAccountNumber"]:null):null;
			privateState.maskedCardNumber = defaultValues?(defaultValues["maskedCardNumber"]?defaultValues["maskedCardNumber"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"accountId" : {
					get : function(){return privateState.accountId},
					set : function(val){
						setterFunctions['accountId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Action" : {
					get : function(){return privateState.Action},
					set : function(val){
						setterFunctions['Action'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardHolderName" : {
					get : function(){return privateState.cardHolderName},
					set : function(val){
						setterFunctions['cardHolderName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardId" : {
					get : function(){return privateState.cardId},
					set : function(val){throw Error("cardId cannot be changed."); },
					enumerable : true,
				},
				"cardNumber" : {
					get : function(){return privateState.cardNumber},
					set : function(val){
						setterFunctions['cardNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardStatus" : {
					get : function(){return privateState.cardStatus},
					set : function(val){
						setterFunctions['cardStatus'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardType" : {
					get : function(){return privateState.cardType},
					set : function(val){
						setterFunctions['cardType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"errmsg" : {
					get : function(){return privateState.errmsg},
					set : function(val){
						setterFunctions['errmsg'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"expiryDate" : {
					get : function(){return privateState.expiryDate},
					set : function(val){
						setterFunctions['expiryDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"Reason" : {
					get : function(){return privateState.Reason},
					set : function(val){
						setterFunctions['Reason'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"userId" : {
					get : function(){return privateState.userId},
					set : function(val){
						setterFunctions['userId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"userName" : {
					get : function(){return privateState.userName},
					set : function(val){
						setterFunctions['userName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"creditLimit" : {
					get : function(){return privateState.creditLimit},
					set : function(val){
						setterFunctions['creditLimit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"availableCredit" : {
					get : function(){return privateState.availableCredit},
					set : function(val){
						setterFunctions['availableCredit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"serviceProvider" : {
					get : function(){return privateState.serviceProvider},
					set : function(val){
						setterFunctions['serviceProvider'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"billingAddress" : {
					get : function(){return privateState.billingAddress},
					set : function(val){
						setterFunctions['billingAddress'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"cardProductName" : {
					get : function(){return privateState.cardProductName},
					set : function(val){
						setterFunctions['cardProductName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"secondaryCardHolder" : {
					get : function(){return privateState.secondaryCardHolder},
					set : function(val){
						setterFunctions['secondaryCardHolder'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"withdrawlLimit" : {
					get : function(){return privateState.withdrawlLimit},
					set : function(val){
						setterFunctions['withdrawlLimit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountNumber" : {
					get : function(){return privateState.accountNumber},
					set : function(val){
						setterFunctions['accountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountName" : {
					get : function(){return privateState.accountName},
					set : function(val){
						setterFunctions['accountName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"maskedAccountNumber" : {
					get : function(){return privateState.maskedAccountNumber},
					set : function(val){
						setterFunctions['maskedAccountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"maskedCardNumber" : {
					get : function(){return privateState.maskedCardNumber},
					set : function(val){
						setterFunctions['maskedCardNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(Cards);
	
	//Create new class level validator object
	BaseModel.Validator.call(Cards);
	
	var registerValidatorBackup = Cards.registerValidator;
	
	Cards.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( Cards.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'getCardsByUsername' with service id 'getCardsByUsername8684'
	Cards.getCardsByUsername = function(params, onCompletion){
		return Cards.customVerb('getCardsByUsername', params, onCompletion);
	};
	//For Operation 'cancelCard' with service id 'updateCard1812'
	Cards.cancelCard = function(params, onCompletion){
		return Cards.customVerb('cancelCard', params, onCompletion);
	};
	//For Operation 'reportLost' with service id 'updateCard1595'
	Cards.reportLost = function(params, onCompletion){
		return Cards.customVerb('reportLost', params, onCompletion);
	};
	//For Operation 'getCardListForEnrolment' with service id 'getCardListForEnrolment8376'
	Cards.getCardListForEnrolment = function(params, onCompletion){
		return Cards.customVerb('getCardListForEnrolment', params, onCompletion);
	};
	//For Operation 'replaceCard' with service id 'updateCard7455'
	Cards.replaceCard = function(params, onCompletion){
		return Cards.customVerb('replaceCard', params, onCompletion);
	};
	//For Operation 'changePIN' with service id 'updateCard5012'
	Cards.changePIN = function(params, onCompletion){
		return Cards.customVerb('changePIN', params, onCompletion);
	};
	//For Operation 'unlockCard' with service id 'updateCard7894'
	Cards.unlockCard = function(params, onCompletion){
		return Cards.customVerb('unlockCard', params, onCompletion);
	};
	//For Operation 'lockCard' with service id 'deleteTransactionsForLockedCard3720'
	Cards.lockCard = function(params, onCompletion){
		return Cards.customVerb('lockCard', params, onCompletion);
	};
	
	var relations = [
	];
	
	Cards.relations = relations;
	
	Cards.prototype.isValid = function(){
		return Cards.isValid(this);
	};
	
	Cards.prototype.objModelName = "Cards";
	
	return Cards;
});