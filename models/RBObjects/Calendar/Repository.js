define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function CalendarRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	CalendarRepository.prototype = Object.create(BaseRepository.prototype);
	CalendarRepository.prototype.constructor = CalendarRepository;

	//For Operation 'getBusinessDays' with service id 'getBusinessDays7665'
	CalendarRepository.prototype.getBusinessDays = function(params,onCompletion){
		return CalendarRepository.prototype.customVerb('getBusinessDays',params, onCompletion);
	};
	
	
	return CalendarRepository;
})