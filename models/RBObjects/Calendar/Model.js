define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		year : function(val, state){
			state['year'] = val;
		},
		month : function(val, state){
			state['month'] = val;
		},
		dayOfMonth : function(val, state){
			state['dayOfMonth'] = val;
		},
		dayOfWeek : function(val, state){
			state['dayOfWeek'] = val;
		},
		validPaymentDay : function(val, state){
			state['validPaymentDay'] = val;
		},
		weeks : function(val, state){
			state['weeks'] = val;
		},
		days : function(val, state){
			state['days'] = val;
		},
	};
	
	
	//Create the Model Class
	function Calendar(defaultValues){
		var privateState = {};
			privateState.year = defaultValues?(defaultValues["year"]?defaultValues["year"]:null):null;
			privateState.month = defaultValues?(defaultValues["month"]?defaultValues["month"]:null):null;
			privateState.dayOfMonth = defaultValues?(defaultValues["dayOfMonth"]?defaultValues["dayOfMonth"]:null):null;
			privateState.dayOfWeek = defaultValues?(defaultValues["dayOfWeek"]?defaultValues["dayOfWeek"]:null):null;
			privateState.validPaymentDay = defaultValues?(defaultValues["validPaymentDay"]?defaultValues["validPaymentDay"]:null):null;
			privateState.weeks = defaultValues?(defaultValues["weeks"]?defaultValues["weeks"]:null):null;
			privateState.days = defaultValues?(defaultValues["days"]?defaultValues["days"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"year" : {
					get : function(){return privateState.year},
					set : function(val){throw Error("year cannot be changed."); },
					enumerable : true,
				},
				"month" : {
					get : function(){return privateState.month},
					set : function(val){throw Error("month cannot be changed."); },
					enumerable : true,
				},
				"dayOfMonth" : {
					get : function(){return privateState.dayOfMonth},
					set : function(val){
						setterFunctions['dayOfMonth'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"dayOfWeek" : {
					get : function(){return privateState.dayOfWeek},
					set : function(val){
						setterFunctions['dayOfWeek'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"validPaymentDay" : {
					get : function(){return privateState.validPaymentDay},
					set : function(val){
						setterFunctions['validPaymentDay'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"weeks" : {
					get : function(){return privateState.weeks},
					set : function(val){
						setterFunctions['weeks'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"days" : {
					get : function(){return privateState.days},
					set : function(val){
						setterFunctions['days'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(Calendar);
	
	//Create new class level validator object
	BaseModel.Validator.call(Calendar);
	
	var registerValidatorBackup = Calendar.registerValidator;
	
	Calendar.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( Calendar.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'getBusinessDays' with service id 'getBusinessDays7665'
	Calendar.getBusinessDays = function(params, onCompletion){
		return Calendar.customVerb('getBusinessDays', params, onCompletion);
	};
	
	var relations = [
	];
	
	Calendar.relations = relations;
	
	Calendar.prototype.isValid = function(){
		return Calendar.isValid(this);
	};
	
	Calendar.prototype.objModelName = "Calendar";
	
	return Calendar;
});