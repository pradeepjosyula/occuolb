define([],function(){
	var mappings = {
		"year" : "year",
		"month" : "month",
		"dayOfMonth" : "dayOfMonth",
		"dayOfWeek" : "dayOfWeek",
		"validPaymentDay" : "validPaymentDay",
		"weeks" : "weeks",
		"days" : "days",
	};
	Object.freeze(mappings);
	
	var typings = {
		"year" : "number",
		"month" : "number",
		"dayOfMonth" : "number",
		"dayOfWeek" : "number",
		"validPaymentDay" : "boolean",
		"weeks" : "string",
		"days" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"month",
					"year",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "Calendar"
	};
	Object.freeze(config);
	
	return config;
})
