define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		username : function(val, state){
			state['username'] = val;
		},
		fromAccountNumber : function(val, state){
			state['fromAccountNumber'] = val;
		},
		fromProductType : function(val, state){
			state['fromProductType'] = val;
		},
		fromProductId : function(val, state){
			state['fromProductId'] = val;
		},
		toAccountNumber : function(val, state){
			state['toAccountNumber'] = val;
		},
		toProductId : function(val, state){
			state['toProductId'] = val;
		},
		toProductType : function(val, state){
			state['toProductType'] = val;
		},
		amount : function(val, state){
			state['amount'] = val;
		},
		comment : function(val, state){
			state['comment'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		error : function(val, state){
			state['error'] = val;
		},
		frequency : function(val, state){
			state['frequency'] = val;
		},
		startDate : function(val, state){
			state['startDate'] = val;
		},
		endDate : function(val, state){
			state['endDate'] = val;
		},
		day1 : function(val, state){
			state['day1'] = val;
		},
		day2 : function(val, state){
			state['day2'] = val;
		},
		availabelbalance : function(val, state){
			state['availabelbalance'] = val;
		},
		transfersuccess : function(val, state){
			state['transfersuccess'] = val;
		},
		toAccountLastName : function(val, state){
			state['toAccountLastName'] = val;
		},
		nextTransfer : function(val, state){
			state['nextTransfer'] = val;
		},
		edit : function(val, state){
			state['edit'] = val;
		},
		externalAccountId : function(val, state){
			state['externalAccountId'] = val;
		},
		accountCode : function(val, state){
			state['accountCode'] = val;
		},
		processingDate : function(val, state){
			state['processingDate'] = val;
		},
		tansferType : function(val, state){
			state['tansferType'] = val;
		},
		numberOfTransfers : function(val, state){
			state['numberOfTransfers'] = val;
		},
		fromAccount : function(val, state){
			state['fromAccount'] = val;
		},
		description : function(val, state){
			state['description'] = val;
		},
		toAccount : function(val, state){
			state['toAccount'] = val;
		},
		confirmationNumber : function(val, state){
			state['confirmationNumber'] = val;
		},
		scheduledTransferId : function(val, state){
			state['scheduledTransferId'] = val;
		},
		postingDate : function(val, state){
			state['postingDate'] = val;
		},
		occurrences : function(val, state){
			state['occurrences'] = val;
		},
		externalId : function(val, state){
			state['externalId'] = val;
		},
	};
	
	
	//Create the Model Class
	function Transfer(defaultValues){
		var privateState = {};
			privateState.username = defaultValues?(defaultValues["username"]?defaultValues["username"]:null):null;
			privateState.fromAccountNumber = defaultValues?(defaultValues["fromAccountNumber"]?defaultValues["fromAccountNumber"]:null):null;
			privateState.fromProductType = defaultValues?(defaultValues["fromProductType"]?defaultValues["fromProductType"]:null):null;
			privateState.fromProductId = defaultValues?(defaultValues["fromProductId"]?defaultValues["fromProductId"]:null):null;
			privateState.toAccountNumber = defaultValues?(defaultValues["toAccountNumber"]?defaultValues["toAccountNumber"]:null):null;
			privateState.toProductId = defaultValues?(defaultValues["toProductId"]?defaultValues["toProductId"]:null):null;
			privateState.toProductType = defaultValues?(defaultValues["toProductType"]?defaultValues["toProductType"]:null):null;
			privateState.amount = defaultValues?(defaultValues["amount"]?defaultValues["amount"]:null):null;
			privateState.comment = defaultValues?(defaultValues["comment"]?defaultValues["comment"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.error = defaultValues?(defaultValues["error"]?defaultValues["error"]:null):null;
			privateState.frequency = defaultValues?(defaultValues["frequency"]?defaultValues["frequency"]:null):null;
			privateState.startDate = defaultValues?(defaultValues["startDate"]?defaultValues["startDate"]:null):null;
			privateState.endDate = defaultValues?(defaultValues["endDate"]?defaultValues["endDate"]:null):null;
			privateState.day1 = defaultValues?(defaultValues["day1"]?defaultValues["day1"]:null):null;
			privateState.day2 = defaultValues?(defaultValues["day2"]?defaultValues["day2"]:null):null;
			privateState.availabelbalance = defaultValues?(defaultValues["availabelbalance"]?defaultValues["availabelbalance"]:null):null;
			privateState.transfersuccess = defaultValues?(defaultValues["transfersuccess"]?defaultValues["transfersuccess"]:null):null;
			privateState.toAccountLastName = defaultValues?(defaultValues["toAccountLastName"]?defaultValues["toAccountLastName"]:null):null;
			privateState.nextTransfer = defaultValues?(defaultValues["nextTransfer"]?defaultValues["nextTransfer"]:null):null;
			privateState.edit = defaultValues?(defaultValues["edit"]?defaultValues["edit"]:null):null;
			privateState.externalAccountId = defaultValues?(defaultValues["externalAccountId"]?defaultValues["externalAccountId"]:null):null;
			privateState.accountCode = defaultValues?(defaultValues["accountCode"]?defaultValues["accountCode"]:null):null;
			privateState.processingDate = defaultValues?(defaultValues["processingDate"]?defaultValues["processingDate"]:null):null;
			privateState.tansferType = defaultValues?(defaultValues["tansferType"]?defaultValues["tansferType"]:null):null;
			privateState.numberOfTransfers = defaultValues?(defaultValues["numberOfTransfers"]?defaultValues["numberOfTransfers"]:null):null;
			privateState.fromAccount = defaultValues?(defaultValues["fromAccount"]?defaultValues["fromAccount"]:null):null;
			privateState.description = defaultValues?(defaultValues["description"]?defaultValues["description"]:null):null;
			privateState.toAccount = defaultValues?(defaultValues["toAccount"]?defaultValues["toAccount"]:null):null;
			privateState.confirmationNumber = defaultValues?(defaultValues["confirmationNumber"]?defaultValues["confirmationNumber"]:null):null;
			privateState.scheduledTransferId = defaultValues?(defaultValues["scheduledTransferId"]?defaultValues["scheduledTransferId"]:null):null;
			privateState.postingDate = defaultValues?(defaultValues["postingDate"]?defaultValues["postingDate"]:null):null;
			privateState.occurrences = defaultValues?(defaultValues["occurrences"]?defaultValues["occurrences"]:null):null;
			privateState.externalId = defaultValues?(defaultValues["externalId"]?defaultValues["externalId"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"username" : {
					get : function(){return privateState.username},
					set : function(val){throw Error("username cannot be changed."); },
					enumerable : true,
				},
				"fromAccountNumber" : {
					get : function(){return privateState.fromAccountNumber},
					set : function(val){
						setterFunctions['fromAccountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromProductType" : {
					get : function(){return privateState.fromProductType},
					set : function(val){
						setterFunctions['fromProductType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromProductId" : {
					get : function(){return privateState.fromProductId},
					set : function(val){
						setterFunctions['fromProductId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toAccountNumber" : {
					get : function(){return privateState.toAccountNumber},
					set : function(val){
						setterFunctions['toAccountNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toProductId" : {
					get : function(){return privateState.toProductId},
					set : function(val){
						setterFunctions['toProductId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toProductType" : {
					get : function(){return privateState.toProductType},
					set : function(val){
						setterFunctions['toProductType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"amount" : {
					get : function(){return privateState.amount},
					set : function(val){
						setterFunctions['amount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"comment" : {
					get : function(){return privateState.comment},
					set : function(val){
						setterFunctions['comment'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"error" : {
					get : function(){return privateState.error},
					set : function(val){
						setterFunctions['error'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"frequency" : {
					get : function(){return privateState.frequency},
					set : function(val){
						setterFunctions['frequency'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"startDate" : {
					get : function(){return privateState.startDate},
					set : function(val){
						setterFunctions['startDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"endDate" : {
					get : function(){return privateState.endDate},
					set : function(val){
						setterFunctions['endDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"day1" : {
					get : function(){return privateState.day1},
					set : function(val){
						setterFunctions['day1'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"day2" : {
					get : function(){return privateState.day2},
					set : function(val){
						setterFunctions['day2'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"availabelbalance" : {
					get : function(){return privateState.availabelbalance},
					set : function(val){
						setterFunctions['availabelbalance'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"transfersuccess" : {
					get : function(){return privateState.transfersuccess},
					set : function(val){
						setterFunctions['transfersuccess'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toAccountLastName" : {
					get : function(){return privateState.toAccountLastName},
					set : function(val){
						setterFunctions['toAccountLastName'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"nextTransfer" : {
					get : function(){return privateState.nextTransfer},
					set : function(val){
						setterFunctions['nextTransfer'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"edit" : {
					get : function(){return privateState.edit},
					set : function(val){
						setterFunctions['edit'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"externalAccountId" : {
					get : function(){return privateState.externalAccountId},
					set : function(val){
						setterFunctions['externalAccountId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"accountCode" : {
					get : function(){return privateState.accountCode},
					set : function(val){
						setterFunctions['accountCode'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"processingDate" : {
					get : function(){return privateState.processingDate},
					set : function(val){
						setterFunctions['processingDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"tansferType" : {
					get : function(){return privateState.tansferType},
					set : function(val){
						setterFunctions['tansferType'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"numberOfTransfers" : {
					get : function(){return privateState.numberOfTransfers},
					set : function(val){
						setterFunctions['numberOfTransfers'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"fromAccount" : {
					get : function(){return privateState.fromAccount},
					set : function(val){
						setterFunctions['fromAccount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"description" : {
					get : function(){return privateState.description},
					set : function(val){
						setterFunctions['description'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"toAccount" : {
					get : function(){return privateState.toAccount},
					set : function(val){
						setterFunctions['toAccount'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"confirmationNumber" : {
					get : function(){return privateState.confirmationNumber},
					set : function(val){
						setterFunctions['confirmationNumber'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"scheduledTransferId" : {
					get : function(){return privateState.scheduledTransferId},
					set : function(val){
						setterFunctions['scheduledTransferId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"postingDate" : {
					get : function(){return privateState.postingDate},
					set : function(val){
						setterFunctions['postingDate'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"occurrences" : {
					get : function(){return privateState.occurrences},
					set : function(val){
						setterFunctions['occurrences'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"externalId" : {
					get : function(){return privateState.externalId},
					set : function(val){
						setterFunctions['externalId'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(Transfer);
	
	//Create new class level validator object
	BaseModel.Validator.call(Transfer);
	
	var registerValidatorBackup = Transfer.registerValidator;
	
	Transfer.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( Transfer.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'createTransfer' with service id 'createTransfer5195'
	Transfer.createTransfer = function(params, onCompletion){
		return Transfer.customVerb('createTransfer', params, onCompletion);
	};
	//For Operation 'editScheduledTransfer' with service id 'EditScheduledTransfer2567'
	Transfer.editScheduledTransfer = function(params, onCompletion){
		return Transfer.customVerb('editScheduledTransfer', params, onCompletion);
	};
	//For Operation 'deleteExternalScheduledTransfer' with service id 'deleteExternalScheduledTransfer7938'
	Transfer.deleteExternalScheduledTransfer = function(params, onCompletion){
		return Transfer.customVerb('deleteExternalScheduledTransfer', params, onCompletion);
	};
	//For Operation 'deleteScheduledTransfer' with service id 'deleteScheduledTransfer8081'
	Transfer.deleteScheduledTransfer = function(params, onCompletion){
		return Transfer.customVerb('deleteScheduledTransfer', params, onCompletion);
	};
	//For Operation 'editExternalScheduledTransfer' with service id 'updateExternalScheduledTransfer6307'
	Transfer.editExternalScheduledTransfer = function(params, onCompletion){
		return Transfer.customVerb('editExternalScheduledTransfer', params, onCompletion);
	};
	//For Operation 'scheduleTransfer' with service id 'scheduleTransfer1320'
	Transfer.scheduleTransfer = function(params, onCompletion){
		return Transfer.customVerb('scheduleTransfer', params, onCompletion);
	};
	//For Operation 'getScheduledTransfers' with service id 'getScheduledTransfers5300'
	Transfer.getScheduledTransfers = function(params, onCompletion){
		return Transfer.customVerb('getScheduledTransfers', params, onCompletion);
	};
	//For Operation 'scheduleExternalTransfer' with service id 'scheduleExternalTranfer5842'
	Transfer.scheduleExternalTransfer = function(params, onCompletion){
		return Transfer.customVerb('scheduleExternalTransfer', params, onCompletion);
	};
	
	var relations = [
	];
	
	Transfer.relations = relations;
	
	Transfer.prototype.isValid = function(){
		return Transfer.isValid(this);
	};
	
	Transfer.prototype.objModelName = "Transfer";
	
	return Transfer;
});