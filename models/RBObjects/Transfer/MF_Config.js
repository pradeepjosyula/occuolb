define([],function(){
	var mappings = {
		"username" : "username",
		"fromAccountNumber" : "fromAccountNumber",
		"fromProductType" : "fromProductType",
		"fromProductId" : "fromProductId",
		"toAccountNumber" : "toAccountNumber",
		"toProductId" : "toProductId",
		"toProductType" : "toProductType",
		"amount" : "amount",
		"comment" : "comment",
		"success" : "success",
		"error" : "error",
		"frequency" : "frequency",
		"startDate" : "startDate",
		"endDate" : "endDate",
		"day1" : "day1",
		"day2" : "day2",
		"availabelbalance" : "availabelbalance",
		"transfersuccess" : "transfersuccess",
		"toAccountLastName" : "toAccountLastName",
		"nextTransfer" : "nextTransfer",
		"edit" : "edit",
		"externalAccountId" : "externalAccountId",
		"accountCode" : "accountCode",
		"processingDate" : "processingDate",
		"tansferType" : "tansferType",
		"numberOfTransfers" : "numberOfTransfers",
		"fromAccount" : "fromAccount",
		"description" : "description",
		"toAccount" : "toAccount",
		"confirmationNumber" : "confirmationNumber",
		"scheduledTransferId" : "scheduledTransferId",
		"postingDate" : "postingDate",
		"occurrences" : "occurrences",
		"externalId" : "externalId",
	};
	Object.freeze(mappings);
	
	var typings = {
		"username" : "string",
		"fromAccountNumber" : "string",
		"fromProductType" : "string",
		"fromProductId" : "string",
		"toAccountNumber" : "string",
		"toProductId" : "string",
		"toProductType" : "string",
		"amount" : "string",
		"comment" : "string",
		"success" : "string",
		"error" : "string",
		"frequency" : "string",
		"startDate" : "string",
		"endDate" : "string",
		"day1" : "number",
		"day2" : "number",
		"availabelbalance" : "string",
		"transfersuccess" : "string",
		"toAccountLastName" : "string",
		"nextTransfer" : "string",
		"edit" : "string",
		"externalAccountId" : "string",
		"accountCode" : "string",
		"processingDate" : "string",
		"tansferType" : "string",
		"numberOfTransfers" : "string",
		"fromAccount" : "string",
		"description" : "string",
		"toAccount" : "string",
		"confirmationNumber" : "string",
		"scheduledTransferId" : "string",
		"postingDate" : "string",
		"occurrences" : "string",
		"externalId" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"username",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "Transfer"
	};
	Object.freeze(config);
	
	return config;
})
