define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function TransferRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	TransferRepository.prototype = Object.create(BaseRepository.prototype);
	TransferRepository.prototype.constructor = TransferRepository;

	//For Operation 'createTransfer' with service id 'createTransfer5195'
	TransferRepository.prototype.createTransfer = function(params,onCompletion){
		return TransferRepository.prototype.customVerb('createTransfer',params, onCompletion);
	};
	//For Operation 'editScheduledTransfer' with service id 'EditScheduledTransfer2567'
	TransferRepository.prototype.editScheduledTransfer = function(params,onCompletion){
		return TransferRepository.prototype.customVerb('editScheduledTransfer',params, onCompletion);
	};
	//For Operation 'deleteExternalScheduledTransfer' with service id 'deleteExternalScheduledTransfer7938'
	TransferRepository.prototype.deleteExternalScheduledTransfer = function(params,onCompletion){
		return TransferRepository.prototype.customVerb('deleteExternalScheduledTransfer',params, onCompletion);
	};
	//For Operation 'deleteScheduledTransfer' with service id 'deleteScheduledTransfer8081'
	TransferRepository.prototype.deleteScheduledTransfer = function(params,onCompletion){
		return TransferRepository.prototype.customVerb('deleteScheduledTransfer',params, onCompletion);
	};
	//For Operation 'editExternalScheduledTransfer' with service id 'updateExternalScheduledTransfer6307'
	TransferRepository.prototype.editExternalScheduledTransfer = function(params,onCompletion){
		return TransferRepository.prototype.customVerb('editExternalScheduledTransfer',params, onCompletion);
	};
	//For Operation 'scheduleTransfer' with service id 'scheduleTransfer1320'
	TransferRepository.prototype.scheduleTransfer = function(params,onCompletion){
		return TransferRepository.prototype.customVerb('scheduleTransfer',params, onCompletion);
	};
	//For Operation 'getScheduledTransfers' with service id 'getScheduledTransfers5300'
	TransferRepository.prototype.getScheduledTransfers = function(params,onCompletion){
		return TransferRepository.prototype.customVerb('getScheduledTransfers',params, onCompletion);
	};
	//For Operation 'scheduleExternalTransfer' with service id 'scheduleExternalTranfer5842'
	TransferRepository.prototype.scheduleExternalTransfer = function(params,onCompletion){
		return TransferRepository.prototype.customVerb('scheduleExternalTransfer',params, onCompletion);
	};
	
	
	return TransferRepository;
})