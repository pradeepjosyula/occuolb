define([],function(){
	var BaseModel = kony.mvc.Data.BaseModel;
	
	var setterFunctions = {
		username : function(val, state){
			state['username'] = val;
		},
		externalAccountId : function(val, state){
			state['externalAccountId'] = val;
		},
		amount1 : function(val, state){
			state['amount1'] = val;
		},
		amount2 : function(val, state){
			state['amount2'] = val;
		},
		success : function(val, state){
			state['success'] = val;
		},
		data : function(val, state){
			state['data'] = val;
		},
		error : function(val, state){
			state['error'] = val;
		},
	};
	
	
	//Create the Model Class
	function MicroDeposit(defaultValues){
		var privateState = {};
			privateState.username = defaultValues?(defaultValues["username"]?defaultValues["username"]:null):null;
			privateState.externalAccountId = defaultValues?(defaultValues["externalAccountId"]?defaultValues["externalAccountId"]:null):null;
			privateState.amount1 = defaultValues?(defaultValues["amount1"]?defaultValues["amount1"]:null):null;
			privateState.amount2 = defaultValues?(defaultValues["amount2"]?defaultValues["amount2"]:null):null;
			privateState.success = defaultValues?(defaultValues["success"]?defaultValues["success"]:null):null;
			privateState.data = defaultValues?(defaultValues["data"]?defaultValues["data"]:null):null;
			privateState.error = defaultValues?(defaultValues["error"]?defaultValues["error"]:null):null;
		//Using parent contructor to create other properties req. to kony sdk	
			BaseModel.call(this);

		//Defining Getter/Setters
			Object.defineProperties(this,{
				"username" : {
					get : function(){return privateState.username},
					set : function(val){throw Error("username cannot be changed."); },
					enumerable : true,
				},
				"externalAccountId" : {
					get : function(){return privateState.externalAccountId},
					set : function(val){
						setterFunctions['externalAccountId'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"amount1" : {
					get : function(){return privateState.amount1},
					set : function(val){
						setterFunctions['amount1'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"amount2" : {
					get : function(){return privateState.amount2},
					set : function(val){
						setterFunctions['amount2'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"success" : {
					get : function(){return privateState.success},
					set : function(val){
						setterFunctions['success'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"data" : {
					get : function(){return privateState.data},
					set : function(val){
						setterFunctions['data'].call(this,val,privateState);
					},
					enumerable : true,
				},
				"error" : {
					get : function(){return privateState.error},
					set : function(val){
						setterFunctions['error'].call(this,val,privateState);
					},
					enumerable : true,
				},
			});

	}
	
	//Setting BaseModel as Parent to this Model
	BaseModel.isParentOf(MicroDeposit);
	
	//Create new class level validator object
	BaseModel.Validator.call(MicroDeposit);
	
	var registerValidatorBackup = MicroDeposit.registerValidator;
	
	MicroDeposit.registerValidator = function(){
		var propName = arguments[0];
		if(!setterFunctions[propName].changed){
			var setterBackup = setterFunctions[propName];
			setterFunctions[arguments[0]] = function(){
				if( MicroDeposit.isValid(this, propName, val) ){
					return setterBackup.apply(null, arguments);
				}else{
					throw Error("Validation failed for "+ propName +" : "+val);
				}
			}
			setterFunctions[arguments[0]].changed = true;
		}
		return registerValidatorBackup.apply(null, arguments);
	}
	
	//Extending Model for custom operations
	//For Operation 'activateExternalAccount' with service id 'activateExternalAccount6201'
	MicroDeposit.activateExternalAccount = function(params, onCompletion){
		return MicroDeposit.customVerb('activateExternalAccount', params, onCompletion);
	};
	
	var relations = [
	];
	
	MicroDeposit.relations = relations;
	
	MicroDeposit.prototype.isValid = function(){
		return MicroDeposit.isValid(this);
	};
	
	MicroDeposit.prototype.objModelName = "MicroDeposit";
	
	return MicroDeposit;
});