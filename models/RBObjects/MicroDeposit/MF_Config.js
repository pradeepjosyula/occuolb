define([],function(){
	var mappings = {
		"username" : "username",
		"externalAccountId" : "externalAccountId",
		"amount1" : "amount1",
		"amount2" : "amount2",
		"success" : "success",
		"data" : "data",
		"error" : "error",
	};
	Object.freeze(mappings);
	
	var typings = {
		"username" : "string",
		"externalAccountId" : "string",
		"amount1" : "string",
		"amount2" : "string",
		"success" : "boolean",
		"data" : "boolean",
		"error" : "string",
	}
	Object.freeze(typings);
	
	var primaryKeys = [
					"username",
	];
	Object.freeze(primaryKeys);
	
	var config = {
		mappings : mappings,
		typings : typings,
		primaryKeys : primaryKeys,
		serviceName : "RBObjects",
		tableName : "MicroDeposit"
	};
	Object.freeze(config);
	
	return config;
})
