define([],function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;
	
	//Create the Repository Class
	function MicroDepositRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};
	
	//Setting BaseRepository as Parent to this Repository
	MicroDepositRepository.prototype = Object.create(BaseRepository.prototype);
	MicroDepositRepository.prototype.constructor = MicroDepositRepository;

	//For Operation 'activateExternalAccount' with service id 'activateExternalAccount6201'
	MicroDepositRepository.prototype.activateExternalAccount = function(params,onCompletion){
		return MicroDepositRepository.prototype.customVerb('activateExternalAccount',params, onCompletion);
	};
	
	
	return MicroDepositRepository;
})