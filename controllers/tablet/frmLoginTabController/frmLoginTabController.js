define({ 

verifyUser:function (){
     this.view.flxMain.skin = 'sknFlexLoginErrorBackgroundKA';
     this.view.flxLogin.isVisible = false;
     this.view.flxVerification.isVisible = true;
     this.view.forceLayout();
},

login:function(){
this.validateUser(validationSuccessCall.bind(this),validationErrorCall.bind(this));
  function validationSuccessCall(){
//  alert("success");
    this.view.flxLogin.isVisible = false;
    this.view.flxEnrollOrServerError.isVisible = true;
    this.view.forceLayout();	
  }
  function validationErrorCall(){
    this.view.flxLogin.isVisible = false;
    this.view.flxBlocked.isVisible = true;
    this.view.forceLayout();    
  }
},
  
validateUser:function(success,failure){
  if(this.view.main.tbxPassword.text ){
    success();
  }
  else{
    failure();
  }
},
  
welcomeVerifiedUser:function(){
  this.view.flxVerification.isVisible = false;
  this.view.flxWelcomeBack.isVisible = true;
  this.view.forceLayout();
},
  
goToPasswordResetOptionsPage:function(){
  this.view.flxWelcomeBack.isVisible = false;
  this.view.flxResetPasswordOptions.isVisible = true;
  this.view.forceLayout();
},
  
goToResetUsingOTP:function(){
  this.view.flxResetPasswordOptions.isVisible = false;
  this.view.flxSendOTP.isVisible = true;
  this.view.forceLayout();
},  
  
showEnterOTPPage:function(){
  this.view.flxSendOTP.isVisible = false;
  this.view.resetusingOTPEnterOTP.flxCVV.isVisible = true;
  this.view.resetusingOTPEnterOTP.lblResendOTP.isVisible = true;
  this.view.resetusingOTPEnterOTP.btnNext.text = kony.i18n.getLocalizedString("i18n.common.next");
  this.view.flxResetUsingOTP.isVisible = true;
  this.view.forceLayout();
},
  
showEnterCVVPage:function(){
  this.view.flxResetPasswordOptions.isVisible = false;
  this.view.flxResetUsingCVV.isVisible = true;
  this.view.forceLayout();
},
  
showResetPasswordPage:function(){
  this.view.flxResetUsingCVV.isVisible = false;
  this.view.flxResetUsingOTP.isVisible = false;
  this.view.flxResetPassword.isVisible = true;
  this.view.forceLayout();
},

showResetConfirmationPage:function(){
  this.view.flxResetPassword.isVisible = false;
  this.view.flxResetSuccessful.isVisible = true;
  this.view.forceLayout();
},

showUserNameSuggestions:function(){
  this.view.main.flxUserDropdown.isVisible = true;
  this.view.forceLayout();
},
 
selectUserName:function(){
  var val = this.view.main.segUsers.selectedItems[0].lblusers; 
  this.view.main.tbxUserName.text = val;
  this.view.main.flxUserDropdown.isVisible = false;
  this.view.forceLayout();  
},

showPassword:function(){
  alert('hi');
  this.view.main.tbxPassword.secureTextEntry = true;
  this.view.forceLayout(); 
},
  
rememberMe:function(){
  if(this.view.main.imgRememberMe.src == 'unchecked_box.png'){
    this.view.main.imgRememberMe.src = 'checked_box.png';
  }
  else{
    this.view.main.imgRememberMe.src = 'unchecked_box.png';
  }
  this.view.forceLayout();
}
  
 });