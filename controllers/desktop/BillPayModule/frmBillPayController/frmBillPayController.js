define(['CommonUtilities'], function (CommonUtilities) {

  return{

    shouldUpdateUI: function (viewModel) {
      return viewModel !== undefined && viewModel !== null;
    },
    /**
     * Function to make changes to UI
     * Parameters: billPayViewModel {Object}
     */
    willUpdateUI: function (billPayViewModel) {

      if(billPayViewModel.ProgressBar){
        if(billPayViewModel.ProgressBar.show){
          this.showProgressBar();
        }
        else {
          this.hideProgressBar();
        }
      } 
      else if (billPayViewModel.onServerDownError) {
        this.showServerDownForm(billPayViewModel.onServerDownError);
      }
      else if (billPayViewModel.serverError) {
        this.hideProgressBar();
        this.setServerError(true);
      }
      else {
        this.setServerError(false);
        if (billPayViewModel.sideMenu) {
          this.updateHamburgerMenu(billPayViewModel.sideMenu);
        }
        if (billPayViewModel.topBar) {
          this.updateTopBar(billPayViewModel.topBar);
        }
        if (billPayViewModel.myPaymentAccounts) {
          this.hideProgressBar();                
          this.bindMyPaymentAccountsData(billPayViewModel.myPaymentAccounts);
          this.setPayFromData(billPayViewModel.myPaymentAccounts);
          if(kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility === "Not Activated") {
            this.setAccountsForActivationScreen(billPayViewModel.myPaymentAccounts);  
          }
        }
        if (billPayViewModel.showDeactivatedView) {
          this.showBillPayActivationScreen();
        }
        if (billPayViewModel.showNotEligibleView) {
          this.showBillPayActivationNotEligibileScreen();
        }
        if(billPayViewModel.BulkPay){
          this.setAllPayeesSegmentData(billPayViewModel);
        }
        if(billPayViewModel.modifyBulkPay){
          this.setAllPayeesUI();
          this.setModifyBulkPayData(billPayViewModel.modifyBulkPay);
        }
        if (billPayViewModel.managePayee) {                
          this.setManagePayeeUI(billPayViewModel);
          this.hideProgressBar();
        }
        if (billPayViewModel.billerCategories) {  
          this.setDataForAccountCategories(billPayViewModel.billerCategories);
        }
        if (billPayViewModel.scheduledBills) {
          this.bindScheduleBillsSegment(billPayViewModel.scheduledBills,billPayViewModel.config);
          this.hideProgressBar();                
        }
        if (billPayViewModel.navigateToPayBill) {
          this.setDataForPayABill(billPayViewModel.navigateToPayBill);
          this.hideProgressBar();                
        }
        if (billPayViewModel.deleteSchedule) {    
          //TODO : show Confirmation message / failure message.
          if (billPayViewModel.deleteSchedule.successData) {
            this.setScheduledFlex();
          }
          if (billPayViewModel.deleteSchedule.failureData) {
            //alert(billPayViewModel.deleteSchedule.failureData.errmsg); //TODO : Error handeling 
          }
          this.hideProgressBar();            
        }
        if (billPayViewModel.payABillData) {
          this.setDataForPayABill(billPayViewModel.payABillData);
        }
        if (billPayViewModel.payABill) {
          this.setDataForPayABill(billPayViewModel.payABill);
        }
        if (billPayViewModel.payABillWithContext) {
          this.setDataForPayABill(billPayViewModel.payABillWithContext.data, billPayViewModel.payABillWithContext.context);
        }

        if (billPayViewModel.intialView) {
          this.showIntialBillPayUI(billPayViewModel.intialView);
        }
        if (billPayViewModel.billpayHistory) {
          if (billPayViewModel.billpayHistory.sender) {
            this.setHistoryUI();
          }
          else {
            this.binduserBillPayHistory(billPayViewModel);
          }
        }

        if (billPayViewModel.payeeActivities) {   
          this.showPayeeViewActivities(billPayViewModel.payeeActivities);
          this.hideProgressBar();             
        }
        if(billPayViewModel.allPayees) {      
          this.populatePayeesinListBox(billPayViewModel.allPayees);
          this.hideProgressBar();          
        }
        if (billPayViewModel.showOneTimePayment) {
          var dataObj=billPayViewModel.showOneTimePayment;
          this.setDataForShowOneTimePayment(dataObj.data,dataObj.sender);
          this.hideProgressBar();                
        }
        if (billPayViewModel.searchBillPayPayees) {
          this.setDataForSearchPayees(billPayViewModel.searchBillPayPayees);
          this.hideProgressBar();                
        }
        if (billPayViewModel.totalActivatedEbills) { //bind total e-bills amount
          //this.hideProgressBar();  //double progress bar
          this.bindTotalEbillAmountDue(billPayViewModel.totalActivatedEbills);
        }
        if (billPayViewModel.dueBills && billPayViewModel.ignorePaymentDue !== true) { //bind payment due bills
          this.bindPaymentDueSegment(billPayViewModel.dueBills, billPayViewModel.config);
          this.hideProgressBar(); 
        }
        if(billPayViewModel.hideAllFlx){
          this.hideAllFlx();
        }

      }
      this.AdjustScreen();
    },
    /**
     * Handle on setDataForSearchPayeeswill
     *  
     */
    setDataForSearchPayees : function(viewModel)
    {
      var scopeObj = this;
      viewModel.searchView = true;
      kony.olb.utils.hideProgressBar(this.view);
      scopeObj.view.flxPagination.setVisibility(false);
      if (viewModel.error) {  
        return;
      }
      scopeObj.setManagePayeesSegmentData(viewModel);
    },

    setModifyBulkPayData: function(data){
      if(data[0].paymentDue){
        this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
        this.setSkinActive(this.view.tableView.tableTabs.btnTransfers);
        this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
        this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
        this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws)
      }
      else{
        this.setSkinActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
        this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
        this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
        this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
        this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
      }
      this.view.tableView.segmentBillpay.setData(data);
    },
    populatePayeesinListBox: function (data) {
      function convertPayeeToKeyValue (payee) {
        return [payee.payeeId, payee.payeeNickName];
      }
      this.allPayees = data.filter(function(payee) {
        return payee.ebillStatus !== "0"
      });
      this.view.payABill.lbxpayee.masterData = this.allPayees.map(convertPayeeToKeyValue);
      this.view.payABill.lbxpayee.onSelection = this.onPayeeSelect.bind(this);
    },
    onPayeeSelect: function () {
      var selectedKey = this.view.payABill.lbxpayee.selectedKey;
      var selectedPayee = this.allPayees.find(function (payee) {
        return payee.payeeId === selectedKey;
      });
      this.populateValuesFromPayee(selectedPayee);
    },
    populateValuesFromPayee: function (payee) {
      payee.billDueDate = this.formatDate(payee.billDueDate, 'dd/mm/yyyy');
      this.setDataForPayABill(payee, 'quickAction');
    },
    setServerError: function (isError) {
      var scopeObj = this;
      scopeObj.view.flxDowntimeWarning.setVisibility(isError);
      if (isError) {
        scopeObj.view.rtxDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
        scopeObj.view.flxMyPaymentAccounts.top = '130dp';
        scopeObj.view.flxBillPay.top = '130dp';
        scopeObj.view.flxTotalEbillAmountDue.top = '486dp';
        scopeObj.view.flxaddpayeemakeonetimepayment.top = '619dp';
      } else {
        scopeObj.view.flxMyPaymentAccounts.top = '0dp';
        //scopeObj.view.flxMyPaymentAccounts.top = '30dp';
        scopeObj.view.flxBillPay.top = '30dp';
        scopeObj.view.flxTotalEbillAmountDue.top = '20dp';
        //scopeObj.view.flxTotalEbillAmountDue.top = '386dp';
        scopeObj.view.flxaddpayeemakeonetimepayment.top = '20dp';
        //scopeObj.view.flxaddpayeemakeonetimepayment.top = '519dp';
      }
      scopeObj.view.forceLayout();
    },
    disableButton : function (button) {
      button.setEnabled(false);
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";     
    },
    enableButton : function(button){
      button.setEnabled(true);   
      button.skin = "sknbtnLatoffffff15px";
      button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";    
    },
    checkIfAllSearchFieldsAreFilled:function(){
      var self = this;
      var re = new RegExp("^([0-9])+(\.[0-9]{1,2})?$");
      if (this.view.oneTimePayment.flxDetails.txtPaymentAmount.text === null ||
          this.view.oneTimePayment.flxDetails.txtPaymentAmount.text === "" ||
          isNaN(this.view.oneTimePayment.flxDetails.txtPaymentAmount.text.replace(/,/g, "")) ||
          !re.test(this.view.oneTimePayment.flxDetails.txtPaymentAmount.text.replace(/,/g, "")) ||
          (parseFloat(this.view.oneTimePayment.flxDetails.txtPaymentAmount.text.replace(/,/g, "")) <= 0)) {
        self.disableButton(this.view.oneTimePayment.btnNext);
        return;
      } else {
        self.enableButton(this.view.oneTimePayment.btnNext);
      }
    },
    /**
     * Methoid to handle Server errors.
     * Will navigate to serverdown page.
     */
    showServerDownForm: function (onServerDownError) {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.navigateToServerDownScreen();
    },
    /**
* Method to set UI for Pay a Bill, on Navigate
*/
    setPayABillUI: function () {
      this.view.btnConfirm.setVisibility(false);
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "PAY A BILL"
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip= "PAY A BILL";
      this.view.payABill.isVisible = true;
      this.view.flxOneTImePayment.setVisibility(false);
      //this.view.btnConfirm.setVisibility(false);
      this.view.tableView.isVisible = false;
      this.view.tableView.Search.setVisibility(false);
      this.view.forceLayout();
    },
    /**
* Method to set UI for History Tab, on Navigate
*/
    setHistoryUI: function () {
      this.selectedTab = "history";
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "HISTORY"
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.History");		
      this.view.tableView.isVisible = true;
      this.view.tableView.flxBillPayScheduled.setVisibility(false);
      this.setDataForHistory();
      this.view.payABill.setVisibility(false);
      this.view.flxOneTImePayment.setVisibility(false);
      this.view.flxActivatebillpays.setVisibility(false);
      //this.view.btnConfirm.setVisibility(true);
      this.view.tableView.Search.setVisibility(false);

      this.view.forceLayout();
    },
    /**
* Method to set UI for All Payees Tab, on Navigate
*/
    setAllPayeesUI: function () {
      this.selectedTab = "allPayees";
      // this.presenter.showBillPayData();
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "ALL PAYEES"
      }]);
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "ALL PAYEES"
      }]);
      this.setSkinActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
      this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
      this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
      this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
      this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.AllPayees");	
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");	
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.AllPayees");	
      this.view.tableView.isVisible = true;
      this.view.payABill.setVisibility(false);
      this.view.flxBillPay.setVisibility(true);
      this.view.flxBillPayManagePayeeActivitymodified.setVisibility(false);
      this.view.transferActivitymodified.setVisibility(false);
      this.view.transferActivitymodified.flxSegmentBillPay.setVisibility(false);
      this.view.tableView.flxBillPayAllPayees.setVisibility(true);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
      this.view.tableView.flxBillPayScheduled.setVisibility(false);
      this.view.flxOneTImePayment.setVisibility(false);
      this.view.flxActivatebillpays.setVisibility(false);
      this.view.btnConfirm.setVisibility(true);
      this.view.tableView.Search.setVisibility(false);
      this.view.tableView.flxNoPayment.setVisibility(false);
      this.view.tableView.flxBillPayAllPayees.setVisibility(true);
      this.view.tableView.tableSubHeaderHistory.setVisibility(false);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
      this.view.tableView.lblDueDate.text = kony.i18n.getLocalizedString("i18n.billPay.DueDate");
      this.view.forceLayout();

    },
    /**
* Method to set UI for Manage Payee Tab, on Navigate
*/
    setManagePayeeUI: function (data) {
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.setBottomforManagePayees();
      if (data) {
        this.savedBillPayViewModel = data;
      }
      else{
        //  if(this.savedBillPayViewModel)
        //  data = this.savedBillPayViewModel;
        this.presenter.showBillPayData( null ,{show : "ManagePayees"});
        return;
      }
      this.selectedTab = "managePayees";
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "MANAGE PAYEES"
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");	
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.ManagePayee");
      this.view.tableView.setVisibility(true);
      this.view.flxBillPay.setVisibility(true);
      this.view.flxBillPayManagePayeeActivitymodified.setVisibility(false);
      this.view.transferActivitymodified.setVisibility(false);
      this.view.tableView.flxBillPayScheduled.setVisibility(false);
      this.view.payABill.setVisibility(false);
      this.view.flxPagination.setVisibility(true);
      this.view.flxOneTImePayment.setVisibility(false);
      this.view.flxActivatebillpays.setVisibility(false);
      //this.view.btnConfirm.setVisibility(true);    Commenting for R1 scope Only
      this.view.tableView.Search.setVisibility(false);

      this.setManagePayeesSegmentData(data);
      CommonUtilities.Sorting.updateSortFlex(this.managePayeeSortMap, data.sortInputs);

      this.view.forceLayout();
    },

    setPaymentDueUI: function () {
      this.selectedTab = "paymentDue";
      this.setBottomDefaultValues();
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "PAYMENT DUE"
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");	
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.PaymentDue");		
      this.view.tableView.flxBillPayAllPayees.setVisibility(true);
      this.view.tableView.flxBillPayScheduled.setVisibility(false);
      this.view.tableView.tableSubHeaderHistory.setVisibility(false);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
      this.view.tableView.lblDueDate.text = kony.i18n.getLocalizedString("i18n.billPay.DueDate");
      this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
      this.setSkinActive(this.view.tableView.tableTabs.btnTransfers);
      this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
      this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
      this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
    },
    setUIBasedOnTab: function () {
      if (this.selectedTab === "managePayees")
        this.setManagePayeeUI();
      else if (this.selectedTab === "history")
        this.setHistoryUI();
      else if (this.selectedTab === "allPayees")
        this.setAllPayeesSegmentData();
      else if (this.selectedTab === "paymentDue")
        this.setPaymentDueSegmentData();
      else if (this.selectedTab === "scheduled")
        this.setScheduledFlex();
    },
    resetUIDuringActivation : function() {
      var self = this;
      self.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.activateBillPay")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.activateBillPay");
      self.view.Activatebillpays.lblHeader.text = kony.i18n.getLocalizedString("i18n.billPay.activateBillPay");
      self.view.flxActivatebillpays.width = "88%";
      self.view.flxActivatebillpays.setVisibility(true);
      self.view.flxBillPay.setVisibility(false);
      self.view.flxPagination.setVisibility(false);
      self.view.flxActivateBiller.setVisibility(false);
      self.view.flxViewEbill.setVisibility(false);
      self.view.flxTotalEbillAmountDue.setVisibility(false);
      self.view.flxBillPayManagePayeeActivity.setVisibility(false);
      self.view.flxaddpayeemakeonetimepayment.setVisibility(true);
      self.view.flxActivateBiller.setVisibility(false);
      self.view.customheader.topmenu.flxMenu.skin = "slFbox";
      self.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      self.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      self.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      self.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      self.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      self.resetSingeBillPay();
      self.view.forceLayout();
    },
    setAccountsForActivationScreen : function(viewModel) {
      var list = [];
      for (i=0; i<viewModel.length; i++) {
        var tempList = [];
        tempList.push(viewModel[i].accountID);
        tempList.push(viewModel[i].accountName + "XXXX" + viewModel[i].accountID.slice(-4));
        list.push(tempList);
      }
      this.view.Activatebillpays.listbxAccountType.masterData = list;
      this.view.forceLayout();
    },
    showBillPayActivationScreen : function() {
      var self = this;
      self.resetUIDuringActivation();
      self.view.Activatebillpays.flxActivateMain.setVisibility(true);
      self.view.Activatebillpays.flxActivateAcknowledgement.setVisibility(false);
      self.view.Activatebillpays.flxActiveBillPayNotEligible.setVisibility(false);
      self.view.Activatebillpays.imgChecbox.src = "unchecked_box.png";
      self.view.Activatebillpays.lblWarning.text = kony.i18n.getLocalizedString("i18n.billPay.activateBillPayMsg");
      self.view.Activatebillpays.btnProceed.onClick = function () {
        // validate accept terms and conditions is checked
        if(self.view.Activatebillpays.imgChecbox.src === "unchecked_box.png") {
          self.view.Activatebillpays.lblWarning.text = kony.i18n.getLocalizedString("i18n.billPay.activateBillPayProceedError");
        } else {
          kony.print("billpay activate step");
          var accountNumber = self.view.Activatebillpays.listbxAccountType.selectedKey;
          var param = {
            "userName" : kony.mvc.MDAApplication.getSharedInstance().appContext.username,
            "default_account_billPay" : accountNumber
          }
          self.presenter.activateBillPayForUser(param);
          self.showBillPayActivationAcknowledgementScreen();
        }
      };
      self.view.Activatebillpays.flxCheckbox.onClick = function () {
        if(self.view.Activatebillpays.imgChecbox.src === "unchecked_box.png") {
          self.view.Activatebillpays.imgChecbox.src = "checked_box.png";
        } else {
          self.view.Activatebillpays.imgChecbox.src = "unchecked_box.png";
        }
      };
      self.view.forceLayout();
    },
    showBillPayActivationAcknowledgementScreen : function() {
      var self = this;
      self.resetUIDuringActivation();
      self.view.Activatebillpays.flxActivateMain.setVisibility(false);
      self.view.Activatebillpays.flxActivateAcknowledgement.setVisibility(true);
      self.view.Activatebillpays.flxActiveBillPayNotEligible.setVisibility(false);
      self.view.flxActivatebillpays.width = "58.2%";
      self.view.Activatebillpays.CopylblHeader0h02f9026a70b47.text = kony.i18n.getLocalizedString("i18n.billPay.activateBillPay");
      self.view.Activatebillpays.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.billPay.activateBillPaySuccessMsg");
      self.view.flxaddpayeemakeonetimepayment.top = '400dp';
      self.view.flxaddpayeemakeonetimepayment.setVisibility(true); 
      self.view.Activatebillpays.btnAddPayee.onClick = self.onClickAddPayee.bind(self);    
      self.view.Activatebillpays.btnMakeOneTimePayment.onClick = self.onClickOneTimePayement.bind(self);   
      self.view.forceLayout();
    },
    showBillPayActivationNotEligibileScreen : function() {
      var self = this;
      self.resetUIDuringActivation();
      self.view.Activatebillpays.flxActivateMain.setVisibility(false);
      self.view.Activatebillpays.flxActivateAcknowledgement.setVisibility(false);
      self.view.Activatebillpays.flxActiveBillPayNotEligible.setVisibility(true);
      self.view.Activatebillpays.lblHeaderNotEligible.text = kony.i18n.getLocalizedString("i18n.billPay.activateBillPay");
      self.view.Activatebillpays.lblWarningNotEligible.text = kony.i18n.getLocalizedString("i18n.billPay.activateBillPayNotEligibleMsg");
      self.view.Activatebillpays.btnCancelNotEligible.onClick = this.showAccountsDashboard;
      self.view.forceLayout();
    },
    showAccountsDashboard : function() {
      var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      accountModule.presentationController.showAccountsDashboard();
    },
    frmBillPayPreShow: function () {
      this.view.customheader.forceCloseHamburger();
      this.view.tableView.Search.txtSearch.width = "100%";
      this.view.tableView.imgSearch.src = "search_blue.png";
      this.view.flxActivatebillpays.setVisibility(false);
      this.view.flxBillPay.setVisibility(true);
      this.view.tableView.Search.setVisibility(false);
      this.view.flxBillPayManagePayeeActivity.setVisibility(false);
      this.view.flxViewEbill.setVisibility(false);
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      /// Strat
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
      /// End
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      this.view.tableView.flxBillPayAllPayees.setVisibility(false);
      this.view.tableView.flxBillPayScheduled.setVisibility(false);
      this.view.tableView.tableSubHeaderHistory.setVisibility(false);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(true); 
      this.view.tableView.lblDueDate.text = kony.i18n.getLocalizedString("i18n.billPay.DueDate");
      this.view.tableView.tableTabs.btnWithdraws.text = kony.i18n.getLocalizedString("i18n.billPay.ManagePayee");
      this.view.tableView.tableTabs.BtnAll.text = kony.i18n.getLocalizedString("i18n.billPay.AllPayees");
      this.view.mypaymentAccounts.lblHeading.text = kony.i18n.getLocalizedString("i18n.BillPay.lblMyPaymentAccounts");
      //this.resetSingeBillPay();
      var self = this;
      //this.view.flxOneTImePayment.setVisibility(false);
      this.setServerError(false);
      this.view.btnMakeonetimepayment.onClick = this.onClickOneTimePayement;
      this.view.btnAddPayee.onClick = this.onClickAddPayee;
      this.view.forceLayout();
    },
    onClickAddPayee : function() {
      var AddPayeeModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AddPayeeModule");
      AddPayeeModule.presentationController.navigateToAddPayee();
    },
    onClickOneTimePayement :function(isCancel){
      var AddPayeeModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AddPayeeModule");
      AddPayeeModule.presentationController.navigateToOneTimePayement(isCancel);
    },
    resetSingeBillPay: function () {
      //this.view.flxBillPay.payABill.txtNotes.text = "";
      this.view.flxBillPay.payABill.CalDeliverBy.setVisibility(false);
      this.view.flxBillPay.payABill.lblDelieverBy.setVisibility(false);
      //this.view.flxBillPay.payABill.txtSearch.text= "";
    },
    frmBillPayPostShow: function () {
      var billPayEligibility = kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility;
      if(billPayEligibility === "Activated") {
        this.loadTotalEBillsAmount();
      }
      this.view.flxAddPayee.skin = "sknFlexRoundedBorderFFFFFF3Px";
      this.AdjustScreen();
      this.setFlowActions();
    },
    //UI Code
    AdjustScreen: function(data) {
      if(data !== undefined)
      {
      }else{
        data = 0;
      }  
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
          this.view.flxFooter.top = mainheight + diff + data +"dp";
        else
          this.view.flxFooter.top = mainheight + data + "dp";        
      } else {
        this.view.flxFooter.top = mainheight + data + "dp";
      }
      this.view.forceLayout();
    },   
    setFlowActions: function() {
      var scopeobj = this;
      this.view.Activatebillpays.flxinfo.onClick = function() {
        if(scopeobj.view.AllForms.isVisible === true)
          scopeobj.view.AllForms.isVisible = false;
        else
          scopeobj.view.AllForms.isVisible = true;
      };
      this.view.AllForms.flxCross.onClick = function() {
        scopeobj.view.AllForms.isVisible = false;
      };
    },
    loadTotalEBillsAmount: function () {
      var scopeObj = this;
      //scopeObj.showProgressBar();
      if(!scopeObj.presenter) { // TODO :: tmp fix for presenter issue.
        scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;           
      }
      scopeObj.presenter.fetchPaymentDueBills({
        'resetSorting': true,
        'ignorePaymentDue' : true
      });
    },
    initActions: function () {
      var scopeObj = this;
      this.selectedTab = "allPayees";
      this.limit = 10;
      this.offset = 0;	//changing it to 0 everytime, to show 1-10 records on landing
      //Attach BillPay Module.
      var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      billPayModule.presentationController.initializePresentationController();
      //Bill Pay Configuaration
      if(CommonUtilities.getConfiguration('billPayOneTimePayment')==="true") 
        this.view.flxmakeonetimepayment.setVisibility(true);
      else
        this.view.flxmakeonetimepayment.setVisibility(false);

      scopeObj.view.flxDowntimeWarning.setVisibility(false);
      scopeObj.setPagination({ show : false});

      this.view.customheader.headermenu.btnLogout.onClick = function () {
        kony.print("btn logout pressed");
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxContainer.frame.height;
        scopeObj.view.flxLogout.height = height + "dp";
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      }
      this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      }
      //for R2 scope
      //this.view.tableView.tableTabs.BtnAll.onClick = this.setAllPayeesSegmentData.bind(this);
      //this.view.tableView.tableTabs.btnWithdraws.onClick = this.setManagePayeesSegmentData.bind(this);
      //for R1 scope
      //  this.view.payABill.txtSearch.onBeginEditing =this.validateAmount.bind(this);
      this.view.tableView.tableTabs.BtnAll.onClick = this.presenter.showBillPayData.bind(billPayModule.presentationController);
      this.view.tableView.tableTabs.btnTransfers.onClick = this.setPaymentDueSegmentData.bind(this);
      this.view.tableView.tableTabs.btnDeposits.onClick = this.setScheduledFlex.bind(this);
      this.view.tableView.tableTabs.btnChecks.onClick = this.setDataForHistory.bind(this);        
      this.view.tableView.tableTabs.btnWithdraws.onClick = this.presenter.showBillPayData.bind(billPayModule.presentationController, null  ,{show : "ManagePayees"});
      this.view.btnConfirm.onClick = this.showConfirmBulkPay.bind(this);
      this.view.tableView.Search.btnConfirm.onClick =this.onSearchBtnClick.bind(this);
      this.view.tableView.flxSearch.onClick = this.toggleSearchBox.bind(this);
      this.view.tableView.Search.txtSearch.onKeyUp = this.onTxtSearchKeyUp.bind(this);
      this.view.tableView.Search.txtSearch.onDone = this.onSearchBtnClick.bind(this);
      this.view.tableView.Search.flxClearBtn.onClick = this.onSearchClearBtnClick.bind(this);

      this.view.payABill.btnCancel.onClick = function () {
        scopeObj.view.payABill.setVisibility(false);
        scopeObj.view.btnConfirm.setVisibility(true);
        scopeObj.view.tableView.setVisibility(true);
        scopeObj.setUIBasedOnTab();
      };
      this.view.ViewEBill.flxImgCancel.onClick = function () {
        scopeObj.view.flxViewEbill.setVisibility(false);
      }
      //Manage Payee 
      scopeObj.managePayeeSortMap = [
        { name : 'payeeNickName' ,  imageFlx : scopeObj.view.tableView.imgBillerSort , clickContainer : scopeObj.view.tableView.flxBillerName}
      ];
      CommonUtilities.Sorting.setSortingHandlers(scopeObj.managePayeeSortMap, scopeObj.onManagePayeeBillerNameClickHandler, scopeObj);

      scopeObj.scheduledSortMap = [
        { name : 'scheduledDate' ,  imageFlx : scopeObj.view.tableView.CopyimgSortPayee0h18cd98de27745 , clickContainer : scopeObj.view.tableView.CopyflxPayee0g1766db8a5a746},
        { name : 'nickName' ,  imageFlx : scopeObj.view.tableView.CopyimgSortPayee0b9e84f45c64f4c , clickContainer : scopeObj.view.tableView.CopyflxPayee0geacd45d25de4f}
        //  { name : 'billDueAmount' ,  imageFlx : scopeObj.view.tableView.CopyimgSortDatee0i6778bd50dbf47 , clickContainer : scopeObj.view.tableView.CopyflxDate0e9f719bff5034e},
        //  { name : 'amount' ,  imageFlx : scopeObj.view.tableView.CopyimgBill0idfbb778171e48 , clickContainer : scopeObj.view.tableView.CopyflxBill0a0a4340e41b14e},            
      ];
      //scopeObj.view.tableView.CopyimgSortDatee0i6778bd50dbf47.setVisibility(true); //Sorting disabled 
      //scopeObj.view.tableView.CopyimgBill0idfbb778171e48.setVisibility(true);//Sorting disabled 
      CommonUtilities.Sorting.setSortingHandlers(scopeObj.scheduledSortMap, scopeObj.onScheduledSortClickHandler, scopeObj);

      scopeObj.historySortMap = [
        { name : 'transactionDate' ,  imageFlx : scopeObj.view.tableView.tableSubHeaderHistory.imgSortDate , clickContainer : scopeObj.view.tableView.tableSubHeaderHistory.flxSortDate},
        { name : 'nickName' ,  imageFlx : scopeObj.view.tableView.tableSubHeaderHistory.imgSortDescription , clickContainer : scopeObj.view.tableView.tableSubHeaderHistory.flxSendTo},
        { name : 'amount' ,  imageFlx : scopeObj.view.tableView.tableSubHeaderHistory.imgSortAmount , clickContainer : scopeObj.view.tableView.tableSubHeaderHistory.flxAmount},
        { name : 'statusDesc' ,  imageFlx : scopeObj.view.tableView.tableSubHeaderHistory.imgSortCategory , clickContainer : scopeObj.view.tableView.tableSubHeaderHistory.flxStatus},            
      ];
      scopeObj.view.tableView.tableSubHeaderHistory.imgSortType.setVisibility(false);
      CommonUtilities.Sorting.setSortingHandlers(scopeObj.historySortMap, scopeObj.onHistorySortClickHandler, scopeObj);

      scopeObj.paymentDueSortMap = [{ 
        name: 'nickName',
        imageFlx: scopeObj.view.tableView.imgSortPayee,
        clickContainer: scopeObj.view.tableView.flxPayee
      },
                                    { name: 'billDueDate', imageFlx: this.view.tableView.imgSortDatee, clickContainer: this.view.tableView.flxDate }
                                   ];

      scopeObj.allPayeesSortMap = [{
        name: 'payeeNickName',
        imageFlx: scopeObj.view.tableView.imgSortPayee,
        clickContainer: scopeObj.view.tableView.flxPayee
      }];

      if(CommonUtilities.getConfiguration('billPaySearch')==="true")  
        scopeObj.view.tableView.flxSearch.setVisibility(true);
      else
        scopeObj.view.tableView.flxSearch.setVisibility(false);
      //Hide Payment acitivity Sorting icons.
      this.view.transferActivitymodified.imgSortFrom.setVisibility(false);
      this.view.transferActivitymodified.imgSortDate.setVisibility(false);
      this.view.transferActivitymodified.CopyimgSortDate0b28c91ef371c48.setVisibility(false); 
    },
    getDateComponents: function(dateString) {
      if (dateString == kony.i18n.getLocalizedString("i18n.common.none")) dateString = kony.os.date(CommonUtilities.getConfiguration('frontendDateFormat'));
      var dateComponents = kony.os.dateComponents(dateString, CommonUtilities.getConfiguration('frontendDateFormat'));
      if (dateComponents !== null) {
        return [dateComponents["day"], dateComponents["month"], dateComponents["year"]];
      } else return '';
    },
    getvalidStartDate:function(){
      var date=new Date();
      var dd=date.getDate();
      var mm=date.getMonth()+1;
      var yy=date.getFullYear();
      return [dd,mm,yy,0,0,0];		
    },
    createNewBulkPayData: function(data) {
      var lblPayeeDate;
      var lblDueDate;
      var lblBill;
      var btnEbill;
      var btnViewEbill;
      var lblPayee;
      var btnActivateEBill;
      var btnViewEbill;
      var txtAmount;
      var scopeObj = this;
      var self = this;
      lblPayee = data.payeeNickName ? data.payeeNickName :  data.payeeName;
      var nonValue = kony.i18n.getLocalizedString("i18n.common.none");
      if (data.billid === "0") {
        lblPayeeDate = kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + kony.i18n.getLocalizedString("i18n.billPay.noPaymentActivity");
        lblDueDate = nonValue;
        lblBill = nonValue;
        txtAmount = {
          "placeholder": "Enter Amount",
          "text": ""
        };
      } else {
        lblPayeeDate = kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + (self.presenter.formatCurrency(data.lastPaidAmount) || nonValue) + " " + kony.i18n.getLocalizedString("i18n.common.on") + " " + this.presenter.getDateFromDateString(data.lastPaidDate);
        lblDueDate = data.billDueDate ? this.presenter.getDateFromDateString(data.billDueDate) : nonValue;
        lblBill = data.dueAmount ? self.presenter.formatCurrency(data.dueAmount) : nonValue;
        txtAmount = {
          "placeholder": "Enter Amount",
          "text": data.dueAmount ? self.presenter.formatCurrency(data.dueAmount).slice(1) : nonValue,
        };
      }
      var lblNotes = data.notes ? data.notes : '';
      var calSendOn = {
        "dateComponents": data.sendOn ? this.getDateComponents(data.sendOn) : this.getDateComponents(kony.os.date(CommonUtilities.getConfiguration('frontendDateFormat'))),
        "dateFormat":"MM/dd/yyyy",
        "validStartDate":self.getvalidStartDate(),
      };
      var calDeliverBy = {
        "dateComponents":this.getDateComponents(lblDueDate),
        "dateFormat":"MM/dd/yyyy",
        "validStartDate":self.getvalidStartDate(),
      };
      if (data.eBillSupport == "false" || data.isManuallyAdded == "true") {
        btnEbill = {
          "text": "SOME TEXT",
          "skin": "sknbtnebillactive",
          "isVisible": false
        };
      } else {
        if (data.eBillStatus == 1) {
          var eBillViewModel = {
            billGeneratedDate: data.billGeneratedDate,
            amount: data.dueAmount,
            ebillURL: data.ebillURL
          };
          btnEbill = {
            "text": "SOME TEXT",
            "skin": "sknbtnebillactive",
            "onClick": (data.billid === undefined || data.billid === null || data.billid == "0") ? null : scopeObj.viewEBill.bind(scopeObj, {
              "billGeneratedDate": scopeObj.presenter.getDateFromDateString(data.billGeneratedDate),
              "amount": data.dueAmount,
              "ebillURL": data.ebillURL
            })
          };
          lblDueDate = {
            "text": lblDueDate,
            "isVisible": true
          };
          btnActivateEBill = {
            "isVisible": false,
            "text": "ACTIVATE",
            "onClick": null,
          };
        } else {
          btnEbill = {
            "text": "SOME TEXT",
            "skin": "sknBtnImgInactiveEbill",
            "onClick": null
          };
          lblDueDate = {
            "text": lblDueDate,
            "isVisible": false
          };
          btnActivateEBill = {
            "isVisible": true,
            "text": "ACTIVATE",
            "onClick": self.activateallPayeesEBill,
          };
        }
      }
      return {
        "payeeId": data.payeeId,
        "billid": data.billid,
        "payeeAccountNumber": data.payeeAccountNumber,
        "lblSeparatorBottom": {
          "text": " "
        },
        "lblIdentifier": {
          "text": " ",
          "skin": "skin Name"
        },
        "imgDropdown": "arrow_down.png",
        "lblPayee": lblPayee,
        "lblPayeeDate": lblPayeeDate,
        "btnEbill": btnEbill,
        "lblDueDate": lblDueDate,
        "lblBill": lblBill,
        "txtAmount": txtAmount,
        "lblSeparator": "A",
        "lblPayFrom": kony.i18n.getLocalizedString("i18n.billPay.PayFrom"),
        "lblSendOn": kony.i18n.getLocalizedString("i18n.billPay.sendOn"),
        "lblDeliverBy": kony.i18n.getLocalizedString("i18n.billPay.DeliverBy"),
        "lblPayFromList": self.presenter.viewModel.preferredAccountName,
        "imgListBoxDropdown": "arrrow_down.png",
        "calSendOn": calSendOn,
        "calDeliverBy": calDeliverBy,
        "lblNotes": kony.i18n.getLocalizedString("i18n.billPay.Notes"),
        "lblCategory": kony.i18n.getLocalizedString("i18n.billPay.category"),
        "txtNotes": {
          "placeholder": kony.i18n.getLocalizedString("i18n.common.optional"),
          "text": ""
        },
        "lblCategoryList": "Phone Bill",
        "imgCategoryListDropdown": "arrrow_down.png",
        "btnViewEBill": btnViewEbill,
        "lblLineOne": "1",
        "lblLineTwo": "2",
        "accountNumber":data.accountNumber,
        "payeeNickName":data.nickName,
        "payeeName":data.payeeName,
        "template": "flxBillPayAllPayees",
        "btnActivateEbill": btnActivateEBill,
        "lblDollar":kony.i18n.getLocalizedString("i18n.common.currencySymbol"),
        "preferredAccountNumber": self.presenter.viewModel.preferredAccountNumber,
      }
    },
    getNewBulkPayData: function(billPayViewModel) {
      billPayViewModel.formattedBulkBillPayRecords = billPayViewModel.BulkPay.map(this.createNewBulkPayData);
    },
    activateEBill: function() {
      var self = this;
      var index = this.view.tableView.segmentBillpay.selectedIndex[1];
      var data = this.view.tableView.segmentBillpay.data;
      var selectedData = data[index];
      self.activateEbillUI(selectedData, 0);

    },
    activateallPayeesEBill: function() {
      var self = this;
      var index = this.view.tableView.segmentBillpay.selectedIndex[1];
      var data = this.view.tableView.segmentBillpay.data;
      var selectedData = data[index];
      selectedData.allPayees = true;
      self.activateEbillUI(selectedData, 0);

    },
    showErrorFlex : function(message){
      this.view.rtxDowntimeWarning.text= message;
      this.view.flxDowntimeWarning.setVisibility(true);
      this.view.flxDowntimeWarning.setFocus(true);
      this.view.forceLayout();
    },
    hideErrorFlex : function(){
      this.view.flxDowntimeWarning.setVisibility(false);
      this.view.forceLayout();
    },
    validateBulkPayData: function() {
      var data = this.view.tableView.segmentBillpay.data;
      var self = this;
      var isInputValid = false;
      var isValidRecordFound = false;
      for (record in data) {
        if (data[record].txtAmount.text != '' && data[record].txtAmount.text != null && parseFloat(data[record].txtAmount.text.replace(/,/g, "")) > 0) {
          isInputValid = true;
          isValidRecordFound = true;
        } else if (isNaN(data[record].txtAmount.text)) {
          isInputValid = false;
          break;
        }
      }
      if (isInputValid) {
        self.hideErrorFlex();
        self.showConfirmBulkPay();
      }
      else if(isValidRecordFound == false){
        self.showErrorFlex(kony.i18n.getLocalizedString("i18n.billPay.EnterAValidInput"));
      }
      else{
        self.showErrorFlex(kony.i18n.getLocalizedString("i18n.billPay.EnterAValidInput"));
      }
    },
    setAllPayeesSegmentData: function(billPayViewModel) {
      if(!billPayViewModel){
        this.presenter.showBillPayData();
        return;
      }
      this.setAllPayeesUI();
      this.setBottomDefaultValues();
      var scopeObj = this;
      if(billPayViewModel) { 
        if (billPayViewModel.BulkPay.length > 0) {
          this.view.btnConfirm.onClick = this.validateBulkPayData.bind(this);
          this.view.btnConfirm.setVisibility(true);
          this.getNewBulkPayData(billPayViewModel);
          var dataMap = {
            "lblLineOne": "lblLineOne",
            "lblLineTwo": "lblLineTwo",
            "lblIdentifier": "lblIdentifier",
            "imgDropdown": "imgDropdown",
            "lblPayee": "lblPayee",
            "lblPayeeDate": "lblPayeeDate",
            "btnEbill": "btnEbill",
            "lblDueDate": "lblDueDate",
            "lblBill": "lblBill",
            "txtAmount": "txtAmount",
            "lblSeparator": "lblSeparator",
            "lblPayFrom": "lblPayFrom",
            "lblSendOn": "lblSendOn",
            "lblDeliverBy": "lblDeliverBy",
            "lblPayFromList": "lblPayFromList",
            "imgListBoxDropdown": "imgListBoxDropdown",
            "calSendOn": "calSendOn",
            "calDeliverBy": "calDeliverBy",
            "lblNotes": "lblNotes",
            "lblCategory": "lblCategory",
            "txtNotes": "txtNotes",
            "lblCategoryList": "lblCategoryList",
            "imgCategoryListDropdown": "imgCategoryListDropdown",
            "btnViewEBill": "btnViewEBill",
            "lblDollar":"lblDollar",
            "lblSeparatorBottom": "lblSeparatorBottom",
            "btnActivateEbill": "btnActivateEbill"
          };
          var d = new Date().getFullYear() + "";
          this.view.tableView.segmentBillpay.widgetDataMap = dataMap;
          this.view.tableView.segmentBillpay.setData(billPayViewModel.formattedBulkBillPayRecords);
          this.view.tableView.segmentBillpay.setVisibility(true);
          CommonUtilities.Sorting.setSortingHandlers(scopeObj.allPayeesSortMap, scopeObj.onAllPayeeSortClickHanlder, scopeObj);
          CommonUtilities.Sorting.updateSortFlex(scopeObj.allPayeesSortMap, billPayViewModel.config, scopeObj);
          scopeObj.view.tableView.imgSortDatee.setVisibility(false);
          scopeObj.view.tableView.flxDate.onClick = null; }
      } else {
        scopeObj.showNoPayementDetails({
          noPaymentMessageI18Key: "i18n.billPay.noPayeesMessage"
        });
      }
      this.hideProgressBar();
    },
    setSkinActive: function (obj) {
      obj.skin = "sknBtnAccountSummarySelected";
    },
    setSkinActiveAllPayees: function (obj) {
      obj.skin = "sknBtnAccountSummarySelectedmod";
    },
    setSkinInActive: function (obj) {
      obj.skin = "sknBtnAccountSummaryUnselected";
    },
    setSkinInActiveAllPayees: function (obj) {
      obj.skin = "sknBtnAccountSummaryUnselectedmodified";
    },
    setPaymentDueSegmentData: function () {
      //load payment due bills
      this.loadPaymentDueBills();

    },
    bindPaymentDueSegment: function(viewModel, config) {
      var scopeObj = this;
      this.setPaymentDueUI();
      if (viewModel.length === 0) {
        scopeObj.showNoPayementDetails({
          noPaymentMessageI18Key: "i18n.billPay.noPaymentDueMessage"
        });
      } else {
        var dataMap = {
          "lblIdentifier": "lblIdentifier",
          "imgDropdown": "imgDropdown",
          "lblPayee": "lblPayee",
          "lblPayeeDate": "lblPayeeDate",
          "btnEbill": "btnEbill",
          "lblDueDate": "lblDueDate",
          "lblBill": "lblBill",
          "txtAmount": "txtAmount",
          "lblSeparator": "lblSeparator",
          "lblPayFrom": "lblPayFrom",
          "lblSendOn": "lblSendOn",
          "lblDeliverBy": "lblDeliverBy",
          "lblPayFromList": "lblPayFromList",
          "imgListBoxDropdown": "imgListBoxDropdown",
          "calSendOn": "calSendOn",
          "calDeliverBy": "calDeliverBy",
          "lblNotes": "lblNotes",
          "lblCategory": "lblCategory",
          "txtNotes": "txtNotes",
          "lblCategoryList": "lblCategoryList",
          "imgCategoryListDropdown": "imgCategoryListDropdown",
          "btnViewEBill": "btnViewEBill",
          "btnPayBill": "btnPayBill",
          "lblfromAccountNumber": "lblfromAccountNumber",
          "lblBillId": "lblBillId",
          "lblPayeeId": "lblPayeeId",
          "preferredAccountNumber": "preferredAccountNumber",
          "preferredAccountName": "preferredAccountName",
          "paymentDue":"paymentDue",
        };
        var d = new Date();
        var dateFormat = kony.onlineBanking.configurations.getConfiguration("frontendDateFormat");
        viewModel = viewModel.map(function(dataItem) {
          return {
            "preferredAccountName": scopeObj.presenter.viewModel.preferredAccountName,
            "preferredAccountNumber": scopeObj.presenter.viewModel.preferredAccountNumber,
            "lblSeparatorBottom": {
              "text": " "
            },
            "lblIdentifier": {
              "text": " ",
              "skin": "skin Name"
            },
            "imgDropdown": "arrow_down.png",
            "lblPayee": dataItem.payeeName,
            "lblPayeeDate": kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " +  ((dataItem.paidAmount && parseFloat(dataItem.paidAmount.replace(/,/g,"").slice(1))>0) ? ((dataItem.paidAmount + " " + kony.i18n.getLocalizedString("i18n.common.on") + " " + dataItem.paidDate)) : ("No Payment Activity")),
            "btnEbill": {
              "text": "SOME TEXT",
              "skin": dataItem.ebillStatus == 1 ? "sknbtnebillactive" : "sknBtnImgInactiveEbill",
              "onClick": dataItem.ebillStatus == 1 ? scopeObj.viewEBill.bind(scopeObj, {
                "billGeneratedDate": dataItem.billGeneratedDate,
                "amount": dataItem.dueAmount,
                "ebillURL": dataItem.ebillURL
              }) : null
            },
            "lblDueDate": dataItem.billDueDate,
            "lblBill": dataItem.dueAmount,
            "txtAmount": {
              "placeholder": kony.i18n.getLocalizedString("i18n.common.EnterAmount"),
              "text": dataItem.dueAmount.slice(1).replace(/,/g, "")
            },
            /*"btnPayBill": {	//Mapping this only for R1
                        "text": "PAY BILL",
                        "onClick": function () {
                            var payABillData = {
                                "payeeNickname": dataItem.payeeName,
                                "lastPaidAmount": dataItem.paidAmount,
                                "lastPaidDate": dataItem.paidDate,
                                "dueAmount": dataItem.dueAmount,
                                "billDueDate": dataItem.billDueDate,
                                "eBillSupport": dataItem.eBillSupport,
                                "eBillStatus": dataItem.eBillStatus,
                                "billid": dataItem.billid,
                                "billGeneratedDate": dataItem.billGeneratedDate,
                                "ebillURL": dataItem.ebillURL
                            };
                            scopeObj.setDataForPayABill(payABillData);
                        }
                    },*/
            "lblSeparator": "A",
            "lblPayFrom": kony.i18n.getLocalizedString("i18n.billPay.PayFrom"),
            "lblSendOn": kony.i18n.getLocalizedString("i18n.transfers.send_on"),
            "lblDeliverBy": kony.i18n.getLocalizedString("i18n.billPay.DeliverBy"),
            "lblPayFromList": dataItem.fromAccountName,
            "imgListBoxDropdown": "arrrow_down.png",
            "calSendOn": {
              "dateComponents":scopeObj.getDateComponents(kony.os.date(CommonUtilities.getConfiguration('frontendDateFormat'))),
              "dateFormat":dateFormat,
              "validStartDate":scopeObj.getvalidStartDate(),
            },
            "calDeliverBy": {
              "dateComponents":scopeObj.getDateComponents(dataItem.billDueDate),
              "dateFormat":dateFormat,
              "validStartDate":scopeObj.getvalidStartDate(),
            },
            "lblNotes": kony.i18n.getLocalizedString("i18n.billPay.Notes"),
            "lblCategory": kony.i18n.getLocalizedString("i18n.billPay.category"),
            "txtNotes": {
              "placeholder": kony.i18n.getLocalizedString("i18n.common.optional"),
              "text": ""
            },
            "lblCategoryList": dataItem.billerCategoryName,
            "imgCategoryListDropdown": "arrrow_down.png",
            "btnViewEBill": {
              "text": kony.i18n.getLocalizedString("i18n.billPay.viewEBill"),
              "onClick": dataItem.ebillStatus == 1 ? scopeObj.viewEBill.bind(scopeObj, {
                "billGeneratedDate": dataItem.billGeneratedDate,
                "amount": dataItem.dueAmount,
                "ebillURL": dataItem.ebillURL
              }) : null
            },
            "lblLineOne": "1",
            "lblLineTwo": "2",
            "template": "flxBillPayAllPayees",
            "lblBillId": dataItem.billid,
            "lblfromAccountNumber": dataItem.fromAccountNumber,
            "lblPayeeId": dataItem.payeeid,
            "paymentDue":"paymentDue",

          };
        });
        scopeObj.view.tableView.flxNoPayment.setVisibility(false);
        scopeObj.view.tableView.segmentBillpay.setVisibility(true);
        scopeObj.view.tableView.segmentBillpay.onClick = scopeObj.onPaymentDueSegmentRowClick;
        scopeObj.view.btnConfirm.setVisibility(true);
        scopeObj.view.btnConfirm.onClick = scopeObj.showConfirmBulkPay.bind(this);
        scopeObj.view.tableView.segmentBillpay.widgetDataMap = dataMap;
        scopeObj.view.tableView.segmentBillpay.setData(viewModel);
        CommonUtilities.Sorting.setSortingHandlers(scopeObj.paymentDueSortMap, scopeObj.onPaymentDueSortClickHandler, scopeObj);
        CommonUtilities.Sorting.updateSortFlex(scopeObj.paymentDueSortMap, config);
        scopeObj.view.tableView.imgSortDatee.setVisibility(true);
        scopeObj.setPagination({
          show: false
        });
      }
      this.setSearchFlexVisibility(false);
      scopeObj.view.forceLayout();
    },
    onPaymentDueSegmentRowClick: function () {
      return null;//TODO : for R2.
    },
    bindScheduleBillsSegment: function (viewModel, config) {
      var scopeObj = this;

      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "SCHEDULED"
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.scheduled");		
      this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
      this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
      this.setSkinActive(this.view.tableView.tableTabs.btnDeposits);
      this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
      this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
      this.view.tableView.flxBillPayScheduled.setVisibility(true);
      this.view.tableView.flxBillPayAllPayees.setVisibility(false);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
      this.view.tableView.tableSubHeaderHistory.setVisibility(false);
      this.view.tableView.lblDueDate.text = kony.i18n.getLocalizedString("i18n.billpay.scheduledDate");
      this.view.tableView.lblAmount.text = kony.i18n.getLocalizedString("i18n.transfers.PaymentAmount");
      this.setBottomDefaultValues();

      if (viewModel.length === 0) { //Display No payment due details
        scopeObj.showNoPayementDetails({
          noPaymentMessageI18Key: "i18n.billPay.noPaymentScheduleMessage",
          showActionMessageI18Key: "i18n.billPay.ScheduleAPayment"
        });
      } else {
        var dataMap = {
          "lblIdentifier": "lblIdentifier",
          "lblSeperatorone": "lblSeperatorone",
          "lblSeparator": "lblSeparator",
          "imgDropdown": "imgDropdown",
          "lblDate": "lblDate",
          "lblPayeeName": "lblPayeeName",
          "lblBillDueAmount": "lblBillDueAmount",
          "lblPaidAmount": "lblPaidAmount",
          "btnEdit": "btnEdit",
          "lblRefrenceNumber": "lblRefrenceNumber",
          "lblRefrenceNumberValue": "lblRefrenceNumberValue",
          "lblSentFrom": "lblSentFrom",
          "lblSentFromValue": "lblSentFromValue",
          "lblNotes": "lblNotes",
          "lblNotesValue": "lblNotesValue",
          "btnCancel": "btnCancel",
          "btnEbill": "btnEbill"
        };
        viewModel = viewModel.map(function (dataItem) {

          return {
            "lblSeparatorBottom": {
              "text": " "
            },
            "lblIdentifier": {
              "text": " ",
              "skin": "skin Name"
            },
            "imgDropdown": "arrow_down.png",
            "lblDate": dataItem.scheduledDate,
            "lblPayeeName": dataItem.payeeName,
            "lblBillDueAmount": dataItem.billDueAmount,
            "lblPaidAmount": dataItem.paidAmount,
            "btnEdit": {
              "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
              "onClick": scopeObj.onScheduledEditBtnClick.bind(scopeObj, dataItem)
            },
            "lblSeparator": "A",
            "lblSeperatorone": "A",
            "lblRefrenceNumber": kony.i18n.getLocalizedString("i18n.billPay.referenceNumber"),
            "lblRefrenceNumberValue": dataItem.referenceNumber,
            "lblSentFrom": kony.i18n.getLocalizedString("i18n.billPay.sentFrom"),
            "lblSentFromValue": dataItem.fromAccountName,
            "lblNotes": kony.i18n.getLocalizedString("i18n.billPay.Notes"),
            "lblNotesValue": dataItem.notes ? dataItem.notes : kony.i18n.getLocalizedString("i18n.common.none"),
            "btnCancel": {
              "text": kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
              "onClick": scopeObj.onScheduledCancelBtnClick.bind(scopeObj,  dataItem)
            },
            "btnEbill": {
              "text": dataItem.eBillStatus == 1 ? kony.i18n.getLocalizedString("i18n.billPay.viewEBill") : '',
              "onClick": dataItem.eBillStatus == 1 ? scopeObj.viewEBill.bind(scopeObj, { 
                "billGeneratedDate": dataItem.billGeneratedDate, 
                "amount": dataItem.billDueAmount,
                "ebillURL" : dataItem.ebillURL
              }) : null
            },
            "flxDropdown": {
              "onClick": scopeObj.onScheduledSegmentRowClick //TODO : Need to update with working flex onclick binding.
            },
            "template": "flxBillPayScheduled"
          }

        });


        scopeObj.view.tableView.flxNoPayment.setVisibility(false);
        scopeObj.view.tableView.segmentBillpay.setVisibility(true);
        scopeObj.view.tableView.segmentBillpay.widgetDataMap = dataMap;
        scopeObj.view.tableView.segmentBillpay.setData(viewModel);
        CommonUtilities.Sorting.updateSortFlex(this.scheduledSortMap, config);
        scopeObj.setPagination( { show : false});
        //scopeObj.view.tableView.segmentBillpay.onClick = scopeObj.onScheduledSegmentRowClick;
      }
      this.setSearchFlexVisibility(false);
      scopeObj.view.forceLayout();
    },
    onScheduledEditBtnClick: function (dataItem) {
      var scopeObj = this;
      var payABillData = {};

      if (dataItem) {
        payABillData = {
          "payeeNickname": dataItem.payeeName,
          "dueAmount": dataItem.billDueAmount,
          "payeeId": dataItem.payeeId,
          "billid": dataItem.billid,
          "sendOn": dataItem.scheduledDate,
          "notes": dataItem.notes,
          "amount": dataItem.paidAmount,
          "fromAccountName": dataItem.fromAccountName,
          "fromAccountNumber" : dataItem.fromAccountNumber,
          "referenceNumber": dataItem.referenceNumber,
          "lastPaidAmount": dataItem.lastPaidAmount,
          "lastPaidDate": dataItem.lastPaidDate,
          "eBillSupport": dataItem.eBillSupport,
          "eBillStatus": dataItem.eBillStatus,
          "billDueDate": dataItem.dueDate,
          "billCategory": dataItem.billCategory,
          "billGeneratedDate": dataItem.billGeneratedDate,
          "ebillURL": dataItem.ebillURL
        }
      };		
      scopeObj.setDataForPayABill(payABillData);
      scopeObj.AdjustScreen();
    },
    onScheduledSegmentRowClick: function () {
      var scopeObj = this;
      var index = scopeObj.view.tableView.segmentBillpay.selectedIndex[1];
      var data = scopeObj.view.tableView.segmentBillpay.data;
      for (i = 0; i < data.length; i++) {
        if (i === index) {
          if (data[index].template == "flxBillPayScheduled") {
            data[index].imgDropdown = "chevron_up.png";
            data[index].template = "flxBillPayScheduledSelected";
          } else {
            data[index].imgDropdown = "arrow_down.png";
            data[index].template = "flxBillPayScheduled";
          }
        } else {
          data[i].imgDropdown = "arrow_down.png";
          data[i].template = "flxBillPayScheduled";
        }
      }
      scopeObj.view.tableView.segmentBillpay.setData(data);
    },
    showNoPayementDetails: function (viewModel) {
      var scopeObj = this;
      if (viewModel) {
        scopeObj.view.tableView.flxNoPayment.setVisibility(true);
        scopeObj.view.tableView.segmentBillpay.setVisibility(false);
        scopeObj.setPagination({ show : false});
        scopeObj.view.tableView.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString(viewModel.noPaymentMessageI18Key);
        if (viewModel.showActionMessageI18Key) {
          scopeObj.view.tableView.lblScheduleAPayment.setVisibility(true);
          scopeObj.view.tableView.lblScheduleAPayment.text = kony.i18n.getLocalizedString(viewModel.showActionMessageI18Key);
        } else {
          scopeObj.view.tableView.lblScheduleAPayment.setVisibility(false);
        }
      }

    },
    setScheduledFlex: function () {
      this.selectedTab = "scheduled";
      //fetch Scheduled Bills
      this.showProgressBar();
      this.presenter.fetchScheduledBills({
        'resetSorting' : true
      });
    },
    flxSearchOnClick: function () {
      if (this.view.tableView.Search.isVisible === true) {
        this.view.tableView.Search.isVisible = false;
        //             this.view.tableView.imgSearch.width="17dp";
        //             this.view.tableView.imgSearch.height="17dp";
        this.view.tableView.imgSearch.src = "search_blue.png";
        this.view.tableView.flxHorizontalLine2.isVisible = false;
        this.view.forceLayout();
      } else {
        this.view.tableView.Search.isVisible = true;
        //             this.view.tableView.imgSearch.width="47dp";
        //             this.view.tableView.imgSearch.height="47dp";
        this.view.tableView.imgSearch.src = "selecetd_search.png";
        this.view.tableView.flxHorizontalLine2.isVisible = true;
        //this.view.flxCustomListBoxContainerPayFrom.isVisible=false;
        //this.view.flxCustomListBoxContainerCategory.isVisible=false;
        this.view.forceLayout();
      }
    },

    showSearchFlex: function()
    {
      this.view.tableView.Search.setVisibility(true);
      this.view.tableView.Search.txtSearch.text = '';
      this.disableSearch();
      this.view.forceLayout();
    },

    toggleSearchBox: function(searchView) {
      this.setSearchFlexVisibility(!this.view.tableView.Search.isVisible);
      if (this.view.tableView.tableTabs.btnWithdraws.skin === "sknBtnAccountSummaryUnselected"  ||  this.searchView === true) {
        kony.olb.utils.showProgressBar(this.view);
        var noOfRecords = {
          "offset": 0,
          "limit":10,
          "resetSorting": true
        };
        if(searchView === false)
          this.presenter.managePayeeData(noOfRecords);
        else
          this.presenter.managePayeeData(noOfRecords,this.showSearchFlex);
      }
      if (!this.view.tableView.Search.isVisible) {
        this.searchView = false;
        this.prevSearchText = '';
      }
      this.AdjustScreen();
      this.view.forceLayout();
    },

    /**
   * Set search flex visibility.
   * @Input flag {boolean} true/flase.
   * 
   */
    setSearchFlexVisibility: function(flag) {
      if (typeof flag === "boolean") {
        this.view.tableView.Search.txtSearch.placeholder = kony.i18n.getLocalizedString("i18n.transfers.searchTransferPayees");
        this.view.tableView.imgSearch.src = flag ? "selecetd_search.png" : "search_blue.png";
        this.view.tableView.Search.setVisibility(flag); 
        // this.view.tableView.flxSearchSortSeparator.setVisibility(flag);
        if (flag === true) {
          this.view.tableView.Search.txtSearch.text = '';
          this.view.tableView.Search.txtSearch.setFocus(true);
          this.disableSearch();
        }
      } else {
        kony.print("Invalid input.");
      }
      this.AdjustScreen();
    },

    /**
  * Disable search 
  */
    disableSearch: function () {
      this.disableButton(this.view.tableView.Search.btnConfirm);
      this.view.tableView.Search.flxClearBtn.setVisibility(false);
    },

    /**
 * Disable search 
 */
    enableSearch: function () {
      this.enableButton(this.view.tableView.Search.btnConfirm);
      this.view.tableView.Search.flxClearBtn.setVisibility(true);
      this.view.forceLayout();
    },

    /**
  * On Search text change
  */
    onTxtSearchKeyUp: function(event){
      var scopeObj = this;
      var searchKeyword = scopeObj.view.tableView.Search.txtSearch.text.trim();
      if(searchKeyword.length > 0){
        scopeObj.enableSearch();
      } else{
        scopeObj.disableSearch();
      }
    },

    /**
  * BillPay Payees Search clear button click handler.
  *
  */
    onSearchClearBtnClick: function() {
      var scopeObj = this;
      scopeObj.view.tableView.Search.txtSearch.text = "";
      if (this.searchView === true) {
        var noOfRecords = {
          "offset": 0,
          "limit": 10
        };
        kony.olb.utils.showProgressBar(this.view);
        this.presenter.managePayeeData(noOfRecords, this.showSearchFlex);
      }
      this.searchView = false; 
    },
    /**
     * Billpay  Payees Search button click handler.
     *
     */
    onSearchBtnClick: function() {
      var scopeObj = this;
      var searchKeyword = scopeObj.view.tableView.Search.txtSearch.text.trim();
      if (searchKeyword.length >= 0 && scopeObj.prevSearchText !== searchKeyword) {
        kony.olb.utils.showProgressBar(this.view);
        scopeObj.presenter.searchBillPayPayees({
          'searchKeyword': searchKeyword
        });
        scopeObj.searchView = true;
        scopeObj.prevSearchText = '';
      }
    },

    setDataForHistory: function () {
      var scopeObj = this;
      scopeObj.offset = 0;
      scopeObj.showProgressBar();
      scopeObj.presenter.showBillPayHistory(null, {
        'resetSorting' : true
      });
    },

    /**
     * Method to set BillPay intial view.
     * 
     */
    showIntialBillPayUI : function(data){
      var scopeObj = this;
      this.view.tableView.segmentBillpay.setVisibility(false);        
      if(data.show === "History") {
        scopeObj.showHistoryUI();
      }
      else if(data.show === "PayABill") {
        scopeObj.view.payABill.flxError.setVisibility(false);
        scopeObj.showPayABill(data.data);
      } else if(data.show === "AllPayees") {
        scopeObj.setAllPayeesUI();
      } else if(data.show === "ManagePayees") {
        //scopeObj.setManagePayeeUI();
      }       
    },
    showHistoryUI : function(){
      var scopeObj = this;
      this.view.tableView.isVisible = true;
      this.view.payABill.setVisibility(false); 
      this.view.flxOneTImePayment.setVisibility(false);
      scopeObj.setServerError(false);
      scopeObj.hideProgressBar();       
      this.setBottomforManagePayees();
      this.selectedTab = "history";
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "HISTORY"
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.accounts.billPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.History");
      this.view.tableView.flxBillPayAllPayees.setVisibility(false);
      this.view.tableView.tableSubHeaderHistory.setVisibility(true);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
      this.view.tableView.flxNoPayment.setVisibility(false);
      this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
      this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
      this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
      this.setSkinActive(this.view.tableView.tableTabs.btnChecks);
      this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
      this.view.tableView.flxBillPayScheduled.setVisibility(false);
    },
    binduserBillPayHistory: function (data) {
      var scopeObj = this;
      scopeObj.showHistoryUI();
      if (data.billpayHistory.length === 0) {
        scopeObj.showNoPayementDetails({
          noPaymentMessageI18Key: "i18n.billpay.noTransactionHistory"
        });
      }
      else {
        var dataMap = {
          "lblIdentifier": "lblIdentifier",
          "imgDropdown": "imgDropdown",
          "lblDate": "lblDate",
          "lblSendTo": "lblSendTo",
          "lblSortAmount": "lblSortAmount",
          "lblSortBalance": "lblSortBalance",
          "btnRepeat": "btnRepeat",
          "lblRefrenceNumber": "lblRefrenceNumber",
          "lblRefrenceNumberValue": "lblRefrenceNumberValue",
          "lblSentFrom": "lblSentFrom",
          "lblSentFromValue": "lblSentFromValue",
          "lblNotes": "lblNotes",
          "lblNotesValue": "lblNotesValue",
          "btnEdit": "btnEdit",
          "lblSeparator": "lblSeparator",
          "lblSeperatorone": "lblSeperatorone"
        }
        var userbillPayHistory = [];

        userbillPayHistory = data.billpayHistory.map(function (dataItem) {
          return {
            "lblIdentifier": "a",
            "imgDropdown": "arrow_down.png",
            "lblSeparator": "A",
            "lblSeperatorone": "A",
            "lblDate": dataItem.lastPaidDate,
            "lblSendTo": dataItem.payeeNickname,
            "lblSortAmount": dataItem.lastPaidAmount,
            "lblSortBalance": dataItem.Status,
            "btnRepeat": {
              "text": "REPEAT",
            },
            "lblRefrenceNumber": "Refrence Number",
            "lblRefrenceNumberValue": dataItem.RefrenceNumber,
            "lblSentFrom": "Sent From",
            "lblSentFromValue": dataItem.SentFrom,
            "lblNotes": "Note",
            "lblNotesValue": dataItem.Notes ? dataItem.Notes : kony.i18n.getLocalizedString("i18n.common.none"),
            // "btnEdit": {
            //     "text": "EDIT",
            //     "onClick": null
            // },
            "template": "flxMain"
          }
        });
        this.view.tableView.segmentBillpay.widgetDataMap = dataMap;
        //this.view.lblPagination.text = (data.noOfRecords.offset + 1) + " - " + (data.noOfRecords.offset + data.noOfRecords.limit) + " " + kony.i18n.getLocalizedString("i18n.common.transactions");
        this.view.tableView.segmentBillpay.setData(userbillPayHistory);
        this.view.tableView.segmentBillpay.setVisibility(true);
        CommonUtilities.Sorting.updateSortFlex(this.historySortMap, data.config);
        this.setPagination({
          'show': true,
          'offset': data.noOfRecords.offset,
          'limit': data.noOfRecords.limit,
          'recordsLength': data.billpayHistory.length,
          'text' : kony.i18n.getLocalizedString("i18n.common.transactions")
        });            
      }
      this.setSearchFlexVisibility(false);
      scopeObj.view.forceLayout();
    },
    viewEBill: function (viewModel) {

      if (viewModel) {
        var scopeObj = this;
        var nonValue = scopeObj.nonValue;
        var height_to_set = 140 + scopeObj.view.flxContainer.frame.height;
        scopeObj.view.flxViewEbill.height = height_to_set + "dp";

        scopeObj.view.ViewEBill.lblPostDateValue.text = viewModel.billGeneratedDate || nonValue;
        scopeObj.view.ViewEBill.lblAmountValue.text = viewModel.amount || nonValue;
        scopeObj.view.ViewEBill.flxMemo.setVisibility(false); //TODO: Not required for R2.
        if(viewModel.ebillURL){
          scopeObj.view.ViewEBill.flxDownload.onClick = scopeObj.downloadFile.bind(scopeObj, {
            'url' : viewModel.ebillURL
          });
          scopeObj.view.ViewEBill.flxDownload.setVisibility(true);
        }else {
          scopeObj.view.ViewEBill.flxDownload.setVisibility(false);
          scopeObj.view.ViewEBill.flxDownload.onClick = null;
        }

        scopeObj.view.flxViewEbill.setVisibility(true);
        scopeObj.view.forceLayout();
      } else {
        onError("Error: Invalid viewModel : " + viewModel);
      }
    },
    /**
     *  Download file
     */
    downloadFile : function(data){
      if(data) {
        this.presenter.downloadFile({
          'url' : data.url,
          'filename' : kony.i18n.getLocalizedString('i18n.billPay.Bill')
        })
      }
    },
    showConfirmBulkPay: function() {
      var self = this;
      var bulkPayRecords = this.view.tableView.segmentBillpay.data;
      self.view.payABill.flxError.setVisibility(false);
      self.view.flxOneTImePayment.setVisibility(false);
      var records = [];
      var totalSum = 0;
      var index;

      function getDateFromConfiguration(dateComponents) {
        var dateFormat = CommonUtilities.getConfiguration('frontendDateFormat')
        if (dateFormat == 'dd/mm/yyyy') {
          return dateComponents.join("/");
        } else if (dateFormat == 'mm/dd/yyyy') {
          return dateComponents[1] + "/" + dateComponents[0] + "/" + dateComponents[2];
        }
      }
      for (index = 0; index < bulkPayRecords.length; index++) {
        var txtAmount = bulkPayRecords[index].txtAmount.text;
        if (txtAmount != '' && txtAmount != null && (parseFloat(txtAmount.replace(/,/g, "")) > 0)) {
          totalSum = totalSum + parseFloat(bulkPayRecords[index].txtAmount.text.replace(/,/g, ""));
          records.push({
            "lblPaymentAccount": bulkPayRecords[index].preferredAccountName,
            "lblPayee": bulkPayRecords[index].lblPayee,
            "lblSendOn": getDateFromConfiguration(bulkPayRecords[index].calSendOn.dateComponents),
            "lblDeliverBy": getDateFromConfiguration(bulkPayRecords[index].calDeliverBy.dateComponents),
            "lblAmount": self.presenter.formatCurrency((bulkPayRecords[index].txtAmount.text).replace(/,/g,"")),
            "lblPaymentAccount": bulkPayRecords[index].lblPayFromList,
            "payeeId": bulkPayRecords[index].payeeId !== undefined ? bulkPayRecords[index].payeeId : bulkPayRecords[index].lblPayeeId,
            "billid": bulkPayRecords[index].billId !== undefined ? bulkPayRecords[index].billId : bulkPayRecords[index].lblBillId,
            "payeeAccountNumber": bulkPayRecords[index].payeeAccountNumber,
            "accountNumber": bulkPayRecords[index].preferredAccountNumber,
          });
        }
      }
      billPayData = {};
      billPayData.records = records;
      billPayData.totalSum = self.presenter.formatCurrency(totalSum);
      billPayData.bulkPayRecords = bulkPayRecords;
      self.presenter.setDataForBulkBillPayConfirm(billPayData);
      self.view.payABill.flxError.setVisibility(false);
      this.view.forceLayout();
    },
    openAddPayeeForm: function () {
      //var ntf = new kony.mvc.Navigation("frmAddPayee");
      //ntf.navigate();
      var addPayeeModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AddPayeeModule");
      addPayeeModule.presentationController.navigateToAddPayee();
    },

    showPayABill: function () {
      var scopeObj=this;

      CommonUtilities.disableOldDaySelection(this.view.payABill.calSendOn);
      CommonUtilities.disableOldDaySelection(this.view.payABill.CalDeliverBy);
      this.view.btnConfirm.setVisibility(false);
      //this.view.tablePagination.setVisibility(false);
      this.view.flxPagination.setVisibility(false);
      // this.view.flxBottom.height = "140dp";
      this.view.flxTermsAndConditions.top = "30dp";
      this.view.payABill.isVisible = true;
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "PAY A BILL"
      }]);
      this.view.tableView.isVisible = false;
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.accounts.payABill");
      this.view.payABill.txtNotes.maxTextLength = scopeObj.OLBConstants.NOTES_LENGTH;

    },
    bindMyPaymentAccountsData: function (viewModel) {
      var scopeObj = this;
      if(viewModel && viewModel.length === 0) {
        this.view.flxMyPaymentAccounts.setVisibility(false);
        this.onError("No Payment Acccounts.")
        return;
      }

      this.view.flxMyPaymentAccounts.setVisibility(true);
      var dataMap = {
        "lblLeft": "lblLeft",
        "lblAccountName": "lblAccountName",
        "flxLeft": "flxLeft",
        "lblBalance": "lblBalance",
        "lblAvailableBalance": "lblAvailableBalance",
        "lblHiddenAccountNumber": "lblHiddenAccountNumber"
      };
      //TODO: need standard and global confirguration
      var AccountsConfig = {
        "Checking": {
          'skin': 'sknFlx9060b7',
          'lblAvailableBalance': 'Available Balance',
          'balanceType': 'availableBalance'
        },
        "Savings": {
          'skin': 'sknFlx26D0CE',
          'lblAvailableBalance': 'Available Balance',
          'balanceType': 'availableBalance'
        },
        "CreditCard": {
          'skin': 'sknFlxF4BA22',
          'lblAvailableBalance': 'Current Balance',
          'balanceType': 'currentBalance'
        },
        "Deposit": {
          'skin': 'sknFlx4a90e2',
          'lblAvailableBalance': 'Available Balance',
          'balanceType': 'availableBalance'
        },
        "Mortgage": {
          'skin': 'sknFlx8d6429',
          'lblAvailableBalance': 'Available Balance',
          'balanceType': 'availableBalance'
        }
      };

      viewModel = viewModel.map(function (account) {
        accountConfig = AccountsConfig[account.accountType];
        return {
          "flxLeft": {
            "skin": accountConfig.skin
          },
          "lblLeft": {
            "text": " "
          },
          "lblAccountName": account.accountName,
          "lblBalance": scopeObj.formatCurrency(account[accountConfig.balanceType]),
          "lblAvailableBalance": accountConfig.lblAvailableBalance,
          "lblHiddenAccountNumber": account.accountID
        };
      });

      this.view.mypaymentAccounts.segMypaymentAccounts.widgetDataMap = dataMap;
      this.view.mypaymentAccounts.segMypaymentAccounts.setData(viewModel);
      //Used to Bind Bill Pay Accounts in Single Bill Pay Page.
      //var billPayAccountObj = this.presenterObj.getBillPayAccounts(this.view.mypaymentAccounts.segMypaymentAccounts.data);
      //this.view.flxBillPay.payABill.lbxpayFromValue.masterdata = billPayAccountObj;
      this.view.forceLayout();
    },
    loadMyPaymentAccountsData: function () {
      this.presenter.fetchAccountsList();
    },
    onScheduledCancelBtnClick: function (dataItem) {
      var scopeObj = this;
      var params = {
        transactionId: dataItem.referenceNumber
      };
      scopeObj.showDeletePopup();
      scopeObj.view.deletePopup.btnYes.onClick = scopeObj.deleteScheduledTransaction.bind(scopeObj, params);
      scopeObj.view.deletePopup.btnNo.onClick = function () {
        scopeObj.view.flxDelete.left = "-100%";
      };
      scopeObj.view.deletePopup.flxCross.onClick = function () {
        scopeObj.view.flxDelete.left = "-100%";
      };
    },
    showDeletePopup: function () {
      var scopeObj = this;
      scopeObj.view.deletePopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      scopeObj.view.deletePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("I18n.billPay.QuitTransactionMsg");
      scopeObj.view.flxDelete.left = "0%";
    },
    deleteScheduledTransaction: function (params) {
      var scopeObj = this;
      scopeObj.view.flxDelete.left = "-100%";
      scopeObj.presenter.deleteScheduledTransaction(params);
    },
    loadPaymentDueBills: function () {
      var scopeObj = this;
      //scopeObj.showProgressBar();

      this.presenter.fetchPaymentDueBills(
        {
          "resetSorting": true                
        }
      );
    },
    bindTotalEbillAmountDue: function (viewModel) {
      var scopeObj = this;
      if (viewModel && viewModel.count === 0) {
        this.view.flxTotalEbillAmountDue.setVisibility(false);
      }
      else {
        this.view.flxTotalEbillAmountDue.setVisibility(true);
        scopeObj.view.lblBills.text = viewModel.count + " " + kony.i18n.getLocalizedString("i18n.billPay.eBills");
        scopeObj.view.lblEbillAmountDueValue.text = viewModel.totalDueAmount;
      }
    },
    setBottomforManagePayees: function () {
      //this.view.tablePagination.setVisibility(true);
      this.view.flxPagination.setVisibility(true);
      // this.view.flxBottom.height = "190dp";
      this.view.flxTermsAndConditions.top = "30dp";
      this.view.btnConfirm.setVisibility(false);
      this.view.flxBottom.forceLayout();
    },
    setBottomDefaultValues: function () {
      //this.view.tablePagination.setVisibility(false);
      this.view.flxPagination.setVisibility(false);
      this.view.btnConfirm.setVisibility(false); //TODO: for R2
      // this.view.flxBottom.height = "220dp";
      this.view.flxTermsAndConditions.top = "30dp";
      this.view.flxBottom.forceLayout();
    },

    /**
    * Method to set Pagination.
    */
    setPagination: function(data) {
      var scopeObj = this;
      if (data && data.show === true) {
        this.view.flxPagination.setVisibility(true);
        var offset = data.offset;
        var limit = data.limit || scopeObj.SEGMENT_ROW_LIMIT;
        var recordsLength = data.recordsLength;

        this.view.lblPagination.text = (offset + 1) + " - " + (offset + recordsLength) + " " + data.text;

        //PREVIOUS
        if (data.offset > 0) {
          scopeObj.view.imgPaginationPrevious.src = "pagination_back_active.png";
          scopeObj.view.flxPaginationPrevious.onClick = scopeObj.paginationPrevious.bind(scopeObj, {
            offset :  data.offset - limit,
            limit: limit,
            preservePrevSorting: true
          });
        }
        else {
          scopeObj.view.imgPaginationPrevious.src = "pagination_back_inactive.png";
          scopeObj.view.flxPaginationPrevious.onClick = null;
        }

        //NEXT
        if (recordsLength >= scopeObj.SEGMENT_ROW_LIMIT) {
          scopeObj.view.imgPaginationNext.src = "pagination_next_active.png";
          scopeObj.view.flxPaginationNext.onClick = scopeObj.paginationNext.bind(scopeObj, {
            offset :  data.offset + limit,
            limit: limit,
            preservePrevSorting: true
          });
        } else {
          scopeObj.view.imgPaginationNext.src = "pagination_next_inactive.png";
          scopeObj.view.flxPaginationNext.onClick = null;
        }
      }
      else {
        scopeObj.view.flxPagination.setVisibility(false);
        scopeObj.view.flxPaginationPrevious.onClick = null;
        scopeObj.view.flxPaginationNext.onClick = null;
      }
    },

    /**
    * Method to show previous set of data in segment, if any
    */
    paginationPrevious: function (data) {
      if(data) {
        this.showProgressBar();
        this.setDataForSelectedTab(data);            
      }

      // var noOfRecords;
      // this.offset -= 10;
      // if (this.offset >= 0) {
      //     noOfRecords = { "offset": this.offset, "limit": this.limit };
      //     this.view.imgPaginationPrevious.src = "pagination_back_active.png";
      //     this.setDataForSelectedTab(noOfRecords);
      //     if (this.offset === 0)
      //         this.view.imgPaginationPrevious.src = "pagination_back_inactive.png";

      // }
      // else {
      //     this.offset = 0;
      //     this.view.imgPaginationPrevious.src = "pagination_back_inactive.png";

      // }
    },

    /**
    * Method to show next set of data in segment
    */
    paginationNext: function (data) {
      // if (this.view.imgPaginationNext.src === "pagination_next_active.png") {
      //     this.view.imgPaginationPrevious.src = "pagination_back_active.png";
      //     this.offset += 10;
      //     var noOfRecords = { "offset": this.offset, "limit": this.limit };
      //     this.setDataForSelectedTab(noOfRecords);
      // }
      if(data) {
        this.showProgressBar();
        this.setDataForSelectedTab(data);            
      }
    },
    setDataForSelectedTab: function (noOfRecords) {
      if (this.selectedTab === "managePayees")
        this.presenter.managePayeeData(noOfRecords);
      else if (this.selectedTab === "history")
        this.presenter.showBillPayHistory(null, noOfRecords)
      else {
        this.presenter.getAllPayeeDataForPagination(noOfRecords)
           }
        },
    bindViewActivityData: function(viewModel){
      function formatBillString(payeeNickName, fromAccountName, billDate){
        var dateFormat = CommonUtilities.getConfiguration("frontendDateFormat");
        //TODO: Get month in a more efficient way
        var monthIndex = dateFormat.split("/")[0] === 'mm' ? 0 : 1;
        var year = billDate.split('/')[2];
        var monthNumber = billDate.split('/')[monthIndex];
        var monthStrings = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var monthString = monthStrings[monthNumber - 1];
        return payeeNickName.split(' ')[0]+'-'+fromAccountName.split(' ')[0]+'-'+monthString+'-'+year;
      }

      var self = this;
      if(viewModel.length === 0){
        return;
      }
      var payeeName = viewModel[0].payeeName; 
      var payeeAccountNumber = viewModel[0].payeeAccountNumber;
      var totalAmount = viewModel[0].lastPaidAmount;
      var dataMap = {
        "lblDate": "lblDate",
        "flxbills" :  "flxbills",
        "btnBills": "btnBills",
        "lblpaiddate": "lblpaiddate",
        "lblFrom": "lblFrom",
        "lblAmount": "lblAmount",
        "lblStatus": "lblStatus"
      };

      viewModel = viewModel.map(function (dataItem) {
        return {
          "lblDate": dataItem.billGeneratedDate,
          "flxbills": {
            "onClick": CommonUtilities.getConfiguration("canViewPastEBills")=== 'true' && dataItem.eBillStatus == 1 ? self.viewEBill.bind(self, {
              "billGeneratedDate": dataItem.billGeneratedDate,
              "amount": dataItem.billDueAmount,
              "ebillURL": dataItem.ebillURL
            }) : null
          },
          "btnBills": {
            "text": formatBillString(dataItem.payeeNickName, dataItem.fromAccountName, dataItem.billGeneratedDate), //TO-DO: Formatted string with Account name, Month, year
          },
          "lblpaiddate": dataItem.lastPaidDate,
          "lblFrom": dataItem.fromAccountName,
          "lblAmount": dataItem.amount,
          "lblStatus": dataItem.statusDescription
        };
      });
      this.view.transferActivitymodified.lblHeader.text = kony.i18n.getLocalizedString("i18n.billPay.PaymentActivity");
      this.view.transferActivitymodified.lblFromTitle.text = "Biller name";
      this.view.transferActivitymodified.lblAccountName.text = payeeName;
      this.view.transferActivitymodified.lblAccountHolder.text = payeeAccountNumber?payeeAccountNumber:" ";
      this.view.transferActivitymodified.lblAmountDeductedTitle.text = "Amount Paid till Date";
      this.view.transferActivitymodified.lblAmountDeducted.text = totalAmount;
      //this.view.transferActivitymodified.btnClose.setVisibility(false);

      this.view.transferActivitymodified.segBillPayActivity.setVisibility(true);
      this.view.transferActivitymodified.tablePaginationBillpay.setVisibility(false);
      this.view.transferActivitymodified.segBillPayActivity.widgetDataMap = dataMap;
      this.view.transferActivitymodified.segBillPayActivity.setData(viewModel);

      this.view.forceLayout();

    },
    /**
* Method to set data for Manage Payee Segment
* @params params : Data of all the payees
*/
    setManagePayeesSegmentData: function (data) {
      var self = this;
      this.setBottomforManagePayees();
      var scopeObj = this;
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "MANAGE PAYEES"
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.ManagePayee");
      this.view.tableView.segmentBillpay.setVisibility(true);
      this.view.tableView.flxNoPayment.setVisibility(false);
      this.view.tableView.flxBillPayAllPayees.setVisibility(false);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(true);
      this.view.tableView.tableSubHeaderHistory.setVisibility(false);
      this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
      this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
      this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
      this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
      this.setSkinActive(this.view.tableView.tableTabs.btnWithdraws);
      if(data.managePayee.length === 0){
        scopeObj.showNoPayementDetails({
          noPaymentMessageI18Key: "i18n.billPay.noPayeesMessage"
        });
        if(data.searchView)
        {
          scopeObj.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
        }else{
          scopeObj.view.tableView.FlxBillpayeeManagePayees.setVisibility(true);
        }
      } 
      else {
        if(data.noOfRecords)
        {
          scopeObj.setPagination({
            'show': true,
            'offset': data.noOfRecords.offset,
            'limit': data.noOfRecords.limit,
            'recordsLength': data.managePayee.length,
            'text': kony.i18n.getLocalizedString("i18n.billpay.payees")
          });  
        }

        var searchView = data.Searchview;  			
        data = data.managePayee;

        var dataMap = {
          "lblIdentifier": "lblIdentifier",
          "imgDropdown": "imgDropdown",
          "lblPayee": "lblPayee",
          "btnEbill": "btnEbill",
          "lblLastPayment": "lblLastPayment",
          "lblLastPaymentDate": "lblLastPaymentDate",
          "lblNextBill": "lblNextBill",
          "lblNextBillDate": "lblNextBillDate",
          "btnPayBill": "btnPayBill",
          "lblSeparator": "lblSeparator",
          "lblAccountNumberTitle": "lblAccountNumberTitle",
          "lblAccountNumberValue": "lblAccountNumberValue",
          "lblBankAddressOne": "lblBankAddressOne",
          "lblBankAddressTwo": "lblBankAddressTwo",
          "btnViewActivity": "btnViewActivity",
          "btnViewEbill": "btnViewEbill",
          "btnEditBiller": "btnEditBiller",
          "btnDeleteBiller": "btnDeleteBiller",
          "lblSeparatorBottom": "lblSeparatorBottom",
          "lblError": "lblError",
          "txtPayee": "txtPayee",
          "txtBankName": "txtBankName",
          "txtAddress": "txtAddress",
          "txtCity": "txtCity",
          "tbxState": "tbxState",
          "tbxPinCode": "tbxPinCode",
          "btnSave": "btnSave",
          "btnCancel": "btnCancel",
          "lblBillerName": "lblBillerName",
        };
        var scheduledBillsData = [];
        var d = new Date().getFullYear() + "";

        function getDateFromDateString(dateStr) {
          if (dateStr) {
            return CommonUtilities.getFrontendDateString(dateStr,CommonUtilities.getConfiguration('frontendDateFormat'));
          }
          else {
            return "";
          }
        }

        if (data.length > 0) {
          scheduledBillsData = data.map(function (dataItem) {
            var data2 = {};
            var data3 = {};
            var data1 = {
              "payeeId": dataItem.payeeId,//required for Update Function				
              "lblSeparatorTwo": { "text": "" },
              "lblSeparatorBottom": {
                "text": " "
              },
              "lblIdentifier": {
                "text": " ",
                "skin": "skin Name"
              },
              "imgDropdown": "arrow_down.png",
              "lblPayee": dataItem.payeeNickName ? dataItem.payeeNickName : '',
              "lblSeparator": {
                "text": " "
              },
              "lblAccountNumberTitle": "Acc Num : ",
              "lblAccountNumberValue": dataItem.accountNumber?dataItem.accountNumber:'',
              "lblBankAddressOne": dataItem.payeeName?dataItem.payeeName:'',
              "lblBankAddressTwo": dataItem.addressLine1?dataItem.addressLine1:'',
              "btnViewActivity": {
                "text": "VIEW ACTIVITY",
                "onClick": scopeObj.viewBillPayActivity.bind(scopeObj,dataItem)
              },
              "btnEditBiller": {
                "text": "EDIT BILLER",
                "onClick": function() {
                  scopeObj.changeToEditBiller();	
                }
              },
              "btnDeleteBiller": {
                "text": "DELETE BILLER",
                "onClick": function() {
                  scopeObj.editDeleteOnClick(self.offset);
                }
              }, 
              "template": "flxBillPayManagePayees",
              //part of Edit Template	
              "txtPayee": dataItem.payeeNickName,				
              "lblBillerName":dataItem.payeeName,
              "lblError":" ",
              "txtBankName": dataItem.addressLine1?dataItem.addressLine1:' ',
              "txtAddress":dataItem.addressLine2?dataItem.addressLine2:' ',				
              "txtCity": dataItem.cityName?dataItem.cityName:' ',
              "tbxState": dataItem.state?dataItem.state:' ',
              "tbxPinCode": dataItem.zipCode?dataItem.zipCode:' ',
              "btnSave": {				
                "text": "SAVE",				
                "onClick": function() {		
                  scopeObj.editSaveOnClick(self.offset);				
                  kony.print("Save button pressed");	
                }
              }, 				
              "btnCancel": {				
                "text": "CANCEL",				
                "onClick": function() {				
                  scopeObj.editCancelOnClick(dataItem);				
                  kony.print("Cancel button pressed");				
                }				
              }, 		
            };
            if (dataItem.billid === "0"){
              data3 = {
                "lblLastPayment": "No Payment Activity",
                "lblLastPaymentDate": "",
                "lblNextBill": "No Payment Activity",
                "lblNextBillDate": ""
              };
            }
            else{                    
              data3 = {
                "lblLastPayment": dataItem.lastPaidAmount?scopeObj.formatCurrency(dataItem.lastPaidAmount):'',
                "lblLastPaymentDate": getDateFromDateString(dataItem.lastPaidDate),
                "lblNextBill": dataItem.dueAmount?scopeObj.formatCurrency(dataItem.dueAmount):'',
                "lblNextBillDate": "Due Date: "+ getDateFromDateString(dataItem.billDueDate)
              };
            }
            if(dataItem.eBillSupport === "false" || dataItem.isManuallyAdded == "true"){
              data2 = {
                "lblLastPayment":" ",
                "lblLastPaymentDate": " ",
                "lblNextBill": " ",
                "lblNextBillDate": " ",
                "btnPayBill": {
                  "text": "PAY BILL",
                  "onClick": function() {
                    var payABillData = {
                      "payeeNickname": dataItem.payeeNickName,
                      "lastPaidAmount": dataItem.lastPaidAmount,
                      "lastPaidDate": getDateFromDateString(dataItem.lastPaidDate),
                      "dueAmount": scopeObj.formatCurrency(dataItem.dueAmount),
                      "billDueDate": getDateFromDateString(dataItem.billDueDate),
                      "eBillSupport": dataItem.eBillSupport,
                      "eBillStatus": dataItem.ebillStatus,
                      "billid": dataItem.billid,
                      "payeeId": dataItem.payeeId,
                      "accountNumber": dataItem.accountNumber,
                      "billGeneratedDate": getDateFromDateString(dataItem.billGeneratedDate),
                      "ebillURL": dataItem.ebillURL,
                      "searchView": scopeObj.searchView
                    };
                    self.setDataForPayABill(payABillData);
                  }
                },
                "lblAccountNumberTitle": "Acc Num : ",
                "lblAccountNumberValue": dataItem.accountNumber?dataItem.accountNumber:'',
                "lblBankAddressOne": dataItem.payeeName?dataItem.payeeName:'',
                "lblBankAddressTwo": dataItem.addressLine2?dataItem.addressLine2:'',}
            }
            else{
              if (dataItem.eBillStatus == "1"){
                data2 = {
                  "btnEbill": {
                    "text": "SOME TEXT",
                    "skin": "sknbtnebillactive",     
                    "onClick" : dataItem.billid === "0" ? null : scopeObj.viewEBill.bind(scopeObj, { 
                      "billGeneratedDate": getDateFromDateString(dataItem.billGeneratedDate), 
                      "amount": scopeObj.formatCurrency(dataItem.dueAmount),
                      "ebillURL" : dataItem.ebillURL
                    })
                  },
                  "btnPayBill": {
                    "text": "PAY BILL",
                    "onClick":function(){
                      var payABillData={
                        "payeeNickname"  :dataItem.payeeNickName,
                        "lastPaidAmount"   :dataItem.lastPaidAmount,
                        "lastPaidDate"     :getDateFromDateString(dataItem.lastPaidDate),
                        "dueAmount"      : scopeObj.formatCurrency(dataItem.dueAmount),
                        "billDueDate"    :getDateFromDateString(dataItem.billDueDate),
                        "eBillSupport"   :dataItem.eBillSupport,
                        "eBillStatus"    :dataItem.ebillStatus, 
                        "billid"         :dataItem.billid,
                        "payeeId"          :dataItem.payeeId,
                        "accountNumber"    :dataItem.accountNumber,
                        "billGeneratedDate" : getDateFromDateString(dataItem.billGeneratedDate),
                        "ebillURL" : dataItem.ebillURL,
                        "searchView" : scopeObj.searchView
                      };
                      self.setDataForPayABill(payABillData);      
                    }
                  },
                  "btnViewEbill": {
                    "text": kony.i18n.getLocalizedString("i18n.billpay.deactivate"),
                    "onClick": function() {		
                      scopeObj.editDeactivateOnClick(self.offset);		
                    }		
                  }
                };
              }
              else {
                data2 = {
                  "btnEbill": {
                    "text": "SOME TEXT",
                    "skin": "sknBtnImgInactiveEbill",
                    "onClick" :  null

                  },
                  "btnPayBill": {
                    "text": "ACTIVATE",
                    "onClick":function(){
                      self.activateEbillUI(dataItem,self.offset);
                    } 
                  },
                  "btnViewEbill": {
                    "text": kony.i18n.getLocalizedString("i18n.billpay.deactivate"),
                    "onClick": null,
                    "skin":"sknBtnLato3343a813pxOp40",          
                  }
                };
              }
            }
            var data2_keys =  Object.keys(data2);
            for ( i in data2_keys){
              data1[data2_keys[i]] = data2[data2_keys[i]];
            }
            var data3_keys =  Object.keys(data3);
            for ( i in data3_keys){
              data1[data3_keys[i]] = data3[data3_keys[i]];
            }
            return data1;

          });
        }
        this.view.tableView.segmentBillpay.widgetDataMap = dataMap;
        this.view.tableView.segmentBillpay.setData(scheduledBillsData);
        scopeObj.view.tableView.flxNoPayment.setVisibility(false);
        if(this.searchView)
          this.view.flxPagination.setVisibility(false);
      }
      this.view.forceLayout();
    },

    setDataForShowOneTimePayment : function (data,sender) {
      var self = this;
      this.showOneTimePayment();
      var oneTimePayment = this.view.oneTimePayment;
      if(data.amount != null) {
        self.enableButton(this.view.oneTimePayment.btnNext);
      } else {
        self.disableButton(this.view.oneTimePayment.btnNext);
      }
      function callback() {
        var formattedOneTimePayment = {
          "payeeId": data.payeeId,
          "payFrom": self.view.oneTimePayment.flxDetails.lbxPayFrom.selectedkeyvalue[1],
          "fromAccountNumber": self.view.oneTimePayment.flxDetails.lbxPayFrom.selectedkeyvalue[0],
          "payeeName": self.view.oneTimePayment.lblToOne.text,
          "amount":  self.view.oneTimePayment.flxDetails.txtPaymentAmount.text,
          "sendOn": self.view.oneTimePayment.flxDetails.flxCalSendOn.calSendOn.formatteddate,
          "deliveryDate":self.view.oneTimePayment.flxDetails.flxCalEndingOn.calEndingOn.date,
          "categories": self.view.oneTimePayment.flxDetails.lbxCategory.selectedkeyvalue[1],
          "billCategory": self.view.oneTimePayment.flxDetails.lbxCategory.selectedkeyvalue[0],
          "notes": self.view.oneTimePayment.flxDetails.txtNotes.text,
          "payeeNickname": data.payeeNickname,
          "referenceNumber": data.referenceNumber,
          "accountNumber": data.accountNumber,
          "billerName" : data.billerName,
          "mobileNumber" : data.mobileNumber,
          "gettingFromOneTimePayment":true 
        };
        self.presenter.setDataForConfirm(formattedOneTimePayment);
      }
      function onAmountChanged() {
        if (self.view.oneTimePayment.flxDetails.txtPaymentAmount.text) {
          self.view.oneTimePayment.flxDetails.txtPaymentAmount.text = CommonUtilities.formatCurrencyWithCommas(self.view.oneTimePayment.flxDetails.txtPaymentAmount.text,true);
        } 
      }
      function callOnKeyUp(){   
        self.checkIfAllSearchFieldsAreFilled();
      }
      function cancelCallBack()
      { 
        self.onClickOneTimePayement(true);
      }
      this.view.oneTimePayment.btnCancel.onClick = cancelCallBack;
      this.view.oneTimePayment.flxAddOrCancelKA.setVisibility(true);
      this.view.oneTimePayment.flxAddOrCancelKA.btnNext.onClick = callback;
      this.view.oneTimePayment.flxDetails.txtPaymentAmount.onKeyUp = callOnKeyUp;
      this.view.oneTimePayment.flxDetails.txtPaymentAmount.onTextChange = onAmountChanged;
      //this.view.oneTimePayment.btnConfirm.onClick = callback;
      this.view.oneTimePayment.flxDetails.lbxPayFrom.selectedKey = data.fromAccountNumber ? data.fromAccountNumber : this.view.oneTimePayment.flxDetails.lbxPayFrom.masterData[0][0];
      this.view.oneTimePayment.flxDetails.lbxCategory.selectedKey = data.billCategory ? data.billCategory : this.view.oneTimePayment.flxDetails.lbxCategory.masterData[0][0];
      CommonUtilities.disableOldDaySelection(this.view.oneTimePayment.flxDetails.flxCalSendOn.calSendOn,data.sendOn); 
      CommonUtilities.disableOldDaySelection(this.view.oneTimePayment.flxDetails.flxCalEndingOn.calEndingOn,data.deliveryDate);
      this.view.oneTimePayment.flxDetails.txtNotes.text = data.notes ? data.notes : '';
      this.view.oneTimePayment.flxDetails.txtPaymentAmount.text  = data.amount ? (data.amount.indexOf('$') >= 0 ? data.amount.substr(1) : data.amount) : ''; //TODO: update with localize currency
      this.view.oneTimePayment.lblAccountNumberValue.text = data.accountNumber ? data.accountNumber : '';
      this.view.oneTimePayment.lblToOne.text = data.billerName ? data.billerName : '';
      this.view.oneTimePayment.lbloTwo.text = data.mobileNumber ? data.mobileNumber : '';

      if(sender==='acknowledgement'){
        this.view.oneTimePayment.flxDetails.txtPaymentAmount.text  = "";
      }
      this.view.forceLayout();
    },

    showOneTimePayment: function () {
      this.view.flxOneTImePayment.setVisibility(true);
      this.view.oneTimePayment.flxDetails.txtPaymentAmount.text="";
      this.view.oneTimePayment.flxSearchPayee.setVisibility(false);
      this.view.oneTimePayment.flxDetails.setVisibility(true);
      this.view.btnConfirm.setVisibility(false);
      this.view.flxPagination.setVisibility(false);
      // this.view.flxBottom.height = "140dp";
      this.view.flxTermsAndConditions.top = "30dp";
      this.view.payABill.isVisible = false;
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "ONE TIME PAYMENT"
      }]);
      this.view.tableView.isVisible = false;
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip="ONE TIME PAYMENT";		

    },

    /**
    * Method to set data for Pay Bill
	* @params params : Data of row item in which PayBill is clicked
    */
    setDataForPayABill: function (data, context) {
      var self = this;
      this.showPayABill();
      var payABill = this.view.payABill;
      self.view.payABill.flxError.setVisibility(false);
      self.view.flxOneTImePayment.setVisibility(false);
      self.view.flxPagination.setVisibility(false);
      if(data.amount !=null)
      {
        if(context == "acknowledgement")
        {
          self.disableButton(self.view.payABill.btnConfirm);
        }else
        {
          self.enableButton(self.view.payABill.btnConfirm);
        }
      }else{
        self.disableButton(self.view.payABill.btnConfirm);
      }
      function callback() {
        var formattedPayABill = {
          "payeeId": data.payeeId,
          "payFrom": self.view.payABill.lbxpayFromValue.selectedkeyvalue[1],
          "fromAccountNumber": self.view.payABill.lbxpayFromValue.selectedkeyvalue[0],
          "payeeName": self.view.payABill.lblPayeeName.text,
          "amount": self.view.payABill.txtSearch.text,
          "sendOn": self.view.payABill.calSendOn.formatteddate,
          "categories": self.view.payABill.lbxCategoryValue.selectedkeyvalue[1],
          "billCategory": self.view.payABill.lbxCategoryValue.selectedkeyvalue[0],
          "notes": self.view.payABill.txtNotes.text,
          "payeeNickname": data.payeeNickname,
          "billid": data.billid,
          "lastPaidAmount": data.lastPaidAmount,
          "lastPaidDate": data.lastPaidDate,
          "dueAmount": data.dueAmount,
          "billDueDate": data.billDueDate,
          "eBillSupport": data.eBillSupport,
          "eBillStatus": data.eBillStatus,
          "referenceNumber": data.referenceNumber,
          "accountNumber": data.accountNumber,
          "deliveryDate" :  self.view.payABill.CalDeliverBy.formatteddate
        };
        if (context == "quickAction")
        {
          formattedPayABill.context = "quickAction";
          formattedPayABill.payeeName = self.view.payABill.lbxpayee.selectedkeyvalue[1]

        }
        self.presenter.setDataForConfirm(formattedPayABill);
      }
      function callOnKeyUp() {
        var re = new RegExp("^([0-9])+(\.[0-9]{1,2})?$");
        if (self.view.payABill.txtSearch.text === null ||
            self.view.payABill.txtSearch.text === "" ||
            isNaN(self.view.payABill.txtSearch.text.replace(/,/g, "")) ||
            !re.test(self.view.payABill.txtSearch.text.replace(/,/g, "")) ||
            (parseFloat(self.view.payABill.txtSearch.text.replace(/,/g, "")) <= 0)) {
          self.disableButton(self.view.payABill.btnConfirm);
          return;
        } else {
          self.enableButton(self.view.payABill.btnConfirm);
        }
      }
      function onAmountChanged() {
        if (self.view.payABill.txtSearch.text) {
          self.view.payABill.txtSearch.text = CommonUtilities.formatCurrencyWithCommas(self.view.payABill.txtSearch.text,true);
        } 
      }
      this.view.payABill.btnConfirm.onClick = callback;
      this.view.payABill.txtSearch.onKeyUp = callOnKeyUp;
      this.view.payABill.txtSearch.onTextChange = onAmountChanged;
      if(context == "quickAction" || data.context == "quickAction") {
        this.view.payABill.lbxpayee.selectedKey = data.payeeId ? data.payeeId : this.view.payABill.lbxpayee.masterData[0][0];
        this.view.payABill.lbxpayee.setVisibility(true);
        this.view.payABill.lblLastPayment.setVisibility(false);
        this.fromAccountNumber = data.fromAccountNumber;
      }
      else {
        this.view.payABill.lbxpayee.setVisibility(false);
        this.view.payABill.lblLastPayment.setVisibility(true);            
        this.view.payABill.lblPayeeName.text = data.payeeNickname;
        this.fromAccountNumber = null;        
      }
      var nonValue = kony.i18n.getLocalizedString("i18n.common.none");
      if (data.billid === "0" || data.billid === undefined || data.billid === null) {
        this.view.payABill.lblDueDateValue.text = nonValue;
        this.view.payABill.lblBillValue.text = nonValue;
      }
      else {
        this.view.payABill.lblDueDateValue.text = data.billDueDate ? CommonUtilities.getFrontendDateString(data.billDueDate) : nonValue;
        this.view.payABill.lblBillValue.text = data.dueAmount ? data.dueAmount : nonValue;
      }
      if(data.lastPaidAmount && data.lastPaidAmount){
        this.view.payABill.lblLastPayment.text = kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": $" + (data.lastPaidAmount && data.lastPaidAmount.indexOf('$') >= 0 ? data.lastPaidAmount.substr(1) : data.lastPaidAmount || nonValue) + " " + kony.i18n.getLocalizedString("i18n.common.on") + " " + CommonUtilities.getFrontendDateString(data.lastPaidDate);            
      }
      else {
        this.view.payABill.lblLastPayment.text = kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + kony.i18n.getLocalizedString("i18n.billPay.noPaymentActivity");            
      }
      this.view.payABill.txtNotes.text = data.notes ? data.notes : '';
      this.view.payABill.txtSearch.text = data.amount ? (data.amount.indexOf('$') >= 0 ? Math.abs(data.amount.substr(1)) : Math.abs(data.amount)) : ''; //TODO: update with localize currency

      //this.view.payABill.calSendOn.date = data.scheduledDate ? data.scheduledDate : kony.os.date("dd/MM/yyyy");
      //if (context == "modify") {
      var preferredAccNum=self.presenter.viewModel.preferredAccountNumber;
      this.view.payABill.lbxpayFromValue.selectedKey = data.fromAccountNumber?data.fromAccountNumber:(preferredAccNum?preferredAccNum:this.view.payABill.lbxpayFromValue.masterData[0][0]);
      this.view.payABill.lbxCategoryValue.selectedKey = data.billCategory ? data.billCategory : this.view.payABill.lbxCategoryValue.masterData[0][0];
      var dateformate=CommonUtilities.getConfiguration('frontendDateFormat');
      this.view.payABill.calSendOn.dateFormat=dateformate;			
      this.view.payABill.CalDeliverBy.dateFormat=dateformate;	  	   
      this.view.payABill.calSendOn.date = data.sendOn ? data.sendOn : kony.os.date(CommonUtilities.getConfiguration('frontendDateFormat'));
      this.view.payABill.CalDeliverBy.date = data.deliveryDate ? data.deliveryDate : kony.os.date(CommonUtilities.getConfiguration('frontendDateFormat'));
      //}

      if (context == "acknowledgement") {
        self.view.payABill.txtSearch.text = "";
      }

      if(data.eBillSupport == "true"){
        this.view.payABill.btnEbill.setVisibility(true);
        data.eBillStatus = data.billid && data.billid != "0" ? '1' : '0';  //TODO : remove once eBillStatus available from service
        if (data.eBillStatus == "1")  {

          var eBillViewModel = {
            billGeneratedDate: data.billGeneratedDate,
            amount: data.dueAmount,
            ebillURL : data.ebillURL
          };
          this.view.payABill.btnEbill.skin = "sknbtnebillactive";
          this.view.payABill.btnEbill.setVisibility(true);
          this.view.payABill.btnEbill.onClick = this.viewEBill.bind(this, eBillViewModel);
          this.view.payABill.btnViewEbill.setVisibility(true);
          this.view.payABill.btnViewEbill.onClick = this.viewEBill.bind(this, eBillViewModel);       
        }
        else {

          this.view.payABill.btnEbill.skin ="sknbtnebillactive";
          this.view.payABill.btnEbill.onClick = null;
          this.view.payABill.btnViewEbill.setVisibility(false);
          this.view.payABill.btnViewEbill.onClick = null;
        }

      }
      else {
        this.view.payABill.btnEbill.setVisibility(false);  
        this.view.payABill.btnEbill.skin ="sknBtnImgInactiveEbill";
        this.view.payABill.btnEbill.onClick = null;
        this.view.payABill.btnViewEbill.setVisibility(false);
        this.view.payABill.btnViewEbill.onClick = null;              
      }

      if(data.searchView === true)
      {
        this.view.payABill.btnCancel.onClick = this.showCurrentFlex;
      }else{
        this.view.payABill.btnCancel.onClick = data.onCancel || this.cancelCurrentFlex;
      }
      if(data.amount!== undefined && data.amount !== null) {
        if(data.amount.indexOf(kony.i18n.getLocalizedString("i18n.common.currencySymbol")) === -1){
          this.view.payABill.txtSearch.text = data.amount;
        }else
        {
          this.view.payABill.txtSearch.text = data.amount.slice(1);
        }					                
      }else{
        this.view.payABill.txtSearch.text = "";
      }
      this.view.breadcrumb.setFocus(true);
      this.view.forceLayout();
    },

    showCurrentFlex : function()
    {
      this.view.tableView.isVisible = true;
      this.view.payABill.isVisible = false;
    },
    cancelCurrentFlex : function()
    {
      this.view.payABill.setVisibility(false);
      this.view.btnConfirm.setVisibility(true);
      this.view.tableView.setVisibility(true);
      this.setUIBasedOnTab();
    },

    setDataForAccountCategories: function (data) {
      var formattedCategories = this.generateListboxDataCategories(data);
      this.view.payABill.lbxCategoryValue.masterData = formattedCategories;
      this.view.oneTimePayment.flxDetails.lbxCategory.masterData = formattedCategories;
    },
    setPayFromData: function (data) {
      var self=this;
      var preferredAccNum=self.presenter.viewModel.preferredAccountNumber;
      this.view.payABill.lbxpayFromValue.masterData = this.generatePayFromData(data);
      this.view.payABill.lbxpayFromValue.selectedKey = this.fromAccountNumber?this.fromAccountNumber:(preferredAccNum?preferredAccNum:this.view.payABill.lbxpayFromValue.masterData[0][0]);
      this.view.oneTimePayment.flxDetails.lbxPayFrom.masterData = this.generatePayFromData(data);
    },
    generateListboxDataCategories: function (data) {
      var list = [];

      for (i = 0; i < data.length; i++) {
        var tempList = [];
        tempList.push(data[i].id);
        tempList.push(data[i].categoryName);
        list.push(tempList);
      }
      return list;
    },


    generatePayFromData: function (data) {
      var list = [];
      this.accountinfo = [];
      for (var i = 0; i < data.length; i++) {
        var tempList = [];
        tempList.push(data[i].accountID);
        tempList.push(data[i].accountName);
        list.push(tempList);
      }
      for (i = 0; i < data.length; i++) {
        var temp = [];
        temp.push(data[i].accountID);
        temp.push(data[i].availableBalance);
        this.accountinfo.push(temp);
      }
      return list;
    },
    validateAmount:  function (amount) {
      if (amount  ===  ""  ||  amount  ===  undefined  ||  isNaN(amount)  ||  parseFloat(amount)  <=  0)
        return  false;
      return  true;
    },
    validateAmountAccountBal: function (accBal, amount) {
      if (parseFloat(amount) <= parseFloat(accBal))
        return true;
      return false;
    },
    getAvailableBal: function (key) {
      var accountsInfo = this.accountinfo;
      for (var i in accountsInfo) {
        if (accountsInfo[i][0] === key)
          return accountsInfo[i][1];
      }
    },
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel,"BILL PAY","Pay a Bill");
      if(this.selectedTab === "managePayees")
        this.view.customheader.customhamburger.activateMenu("BILL PAY", "My Payee List");
      if(this.selectedTab === "history")
        this.view.customheader.customhamburger.activateMenu("BILL PAY", "Bill Pay History");
      if(this.selectedTab === "allPayees")
        this.view.customheader.customhamburger.activateMenu("BILL PAY", "Pay a Bill");
    },
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },
    /**
     * View Actvityty
     */
    viewBillPayActivity: function (dataItem) {

      this.presenter.fetchPayeeBills({
        "payeeId": dataItem.payeeId
      });
    },
    /**		
	 * Method to show Edit column on click of EDIT in manage Bill Payee		
	 */		
    changeToEditBiller: function () {		
      var data = this.view.tableView.segmentBillpay.data;		
      var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];		
      for (i = 0; i < data.length; i++) {		
        data[i].template = "flxBillPayManagePayees";		
      }		
      data[index].template = "flxBillPayManagePayeesEdit";		
      //     if (data !== null || data[index].payeeId !== null || data[index].payeeId !== "") {		
      //       if (data[index].eBillSupport === true && data[index].eBillStatus === "1") { // eBill is Supported and activated		
      //         data[index].btnViewEbill.text = "DEACTIVATE";		
      //       } else if (data[index].eBillSupport === true && data[index].eBillStatus === "0") { //eBill is deactivated		
      //         data[index].btnViewEbill.text = "VIEW E-BILL";		
      //       } else {		
      //         data[index].btnViewEbill.isVisible = false;		
      //       }		
      //     }		
      this.view.tableView.segmentBillpay.setDataAt(data[index], index);	
      this.AdjustScreen(200);
      this.view.forceLayout();
    },

    /**		
		  * Method for Cancel Button in Edit ManagePayee		
		  */		
    editCancelOnClick: function (dataItem) {		
      var data = this.view.tableView.segmentBillpay.data;		
      var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];		
      data[index].template = "flxBillPayManagePayeesSelected";
      data[index].txtPayee=dataItem.payeeNickName;
      data[index].txtBankName=dataItem.addressLine1?dataItem.addressLine1:' ';
      data[index].txtAddress=dataItem.addressLine2?dataItem.addressLine2:' ';
      data[index].txtCity=dataItem.cityName?dataItem.cityName:' ';
      data[index].tbxState=dataItem.state?dataItem.state:' ';
      data[index].tbxPinCode=dataItem.zipCode?dataItem.zipCode:' ';
      data[index].lblError=" ";
      this.view.tableView.segmentBillpay.setDataAt(data[index], index);	
      this.view.forceLayout();
    },
    /**		
		  * Method to Save Edited data in Edit ManagePayee on click of Save button.		
		  * @params: offset value		
		  */		
    editSaveOnClick: function (offsetVal) {	
      var scopeObj=this;
      var data = this.view.tableView.segmentBillpay.data;		
      var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];		
      var updateData = {
        "payeeId": data[index].payeeId,		
        "payeeNickName": data[index].txtPayee,	
        "addressLine1": data[index].txtBankName,
        "addressLine2": data[index].txtAddress,
        "state": data[index].tbxState,
        "zipCode":data[index].tbxPinCode,		
        "cityName": data[index].txtCity,			
      };		
      var isValid;		
      if (data[index] !== undefined || data[index] !== null || data[index] !== "" ||		
          updateData.payeeId !== null || updateData.payeeId !== "" || updateData.payeeId !== undefined) {		
        isValid = true;		
      } else {		
        isValid = false;		
      }		
      if (isValid) {	
        if(this.validateEditManagePayees(updateData)){
          scopeObj.showProgressBar();
          this.presenter.updateManagePayee(updateData, offsetVal);
          data[index].lblError=" ";
          data[index].template = "flxBillPayManagePayeesSelected";		
          this.view.tableView.segmentBillpay.setDataAt(data[index], index);
        }
        else
        {
          kony.print("data edit error");//error scenario
          data[index].template = "flxBillPayManagePayeesEdit";	
          data[index].txtPayee=updateData.payeeNickName;
          data[index].txtBankName=updateData.addressLine1?updateData.addressLine1:' ';
          data[index].txtAddress=updateData.addressLine2?updateData.addressLine2:' ';
          data[index].txtCity=updateData.cityName?updateData.cityName:' ';
          data[index].tbxState=updateData.state?updateData.state:' ';
          data[index].tbxPinCode=updateData.zipCode?updateData.zipCode:' ';
          data[index].lblError=kony.i18n.getLocalizedString("i18n.common.errorEditData");
          this.view.tableView.segmentBillpay.setDataAt(data[index], index);

        }
      }		
      this.view.forceLayout();
    },
    /**
	*Method to validate data entered by user while editing manage payees
	*/
    validateEditManagePayees:function(updateData)
    {
      var isValid = false;	
      if(updateData.payeeNickName){
        isValid = true;
      }
      if(updateData.addressLine1){
        isValid = true;
      }
      if(updateData.addressLine2){
        //optional
      }
      if(updateData.state){
        if(/^[a-zA-Z]+$/.test(updateData.state))
          isValid = true;
        else
          isValid = false;
      }
      if(updateData.cityName){
        if(/^[a-zA-Z]+$/.test(updateData.cityName))
          isValid = true;
        else
          isValid = false;
      }
      if(updateData.zipCode){
        isValid=true;
      }else{
        isValid=false;
      }
      return isValid;
    },

    /**		
            * Method to delete biller in Edit ManagePayee on click of Delete Biller button.		
            * @params: offset value		
            */
    editDeleteOnClick: function (offsetVal) {

      var scopeObj = this;
      var data = this.view.tableView.segmentBillpay.data;
      var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];
      var deleteData = {
        "payeeId": data[index].payeeId,
      };
      scopeObj.view.deletePopup.lblHeading.text = "Delete";
      scopeObj.view.deletePopup.lblPopupMessage.text = "Are you sure, you want to delete Biller?";
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxContainer.frame.height;		
      scopeObj.view.flxDelete.height = height + "dp";		
      scopeObj.view.flxDelete.left = "0%";
      this.view.deletePopup.btnYes.onClick = function () { //onClick for YES button in popup		
        scopeObj.view.flxDelete.left = "-100%";
        if (deleteData !== null || deleteData.payeeId !== null || deleteData.payeeId !== "") {
          scopeObj.presenter.deleteManagePayee(deleteData, offsetVal);
        }
      };
      this.view.deletePopup.btnNo.onClick = function () { //onClick for NO button in popup		
        kony.print("btn no pressed");
        scopeObj.view.flxDelete.left = "-100%";
      };
      this.view.deletePopup.flxCross.onClick = function () { //onClick for CLOSE button in popup		
        kony.print("btn no pressed");
        scopeObj.view.flxDelete.left = "-100%";
      };
      this.AdjustScreen();
    },
    /**		
            * Method to call deActivate function on click of deactivate.		
            * @params: offset value		
            */
    editDeactivateOnClick: function (offsetNo) {
      var scopeObj = this;
      var data = this.view.tableView.segmentBillpay.data;
      var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];
      if (data !== null || data[index].payeeId !== null || data[index].payeeId !== "") {
        scopeObj.showProgressBar();		
        scopeObj.presenter.deactivateEbill(data[index].payeeId, offsetNo);
      }
    },
    onViewActiviyBackToPayeeList : function(data){
      var scopeObj = this;
      scopeObj.presenter.managePayeeData({ },scopeObj.changeSelectedTemplate.bind(scopeObj,data));       
    },
    changeSelectedTemplate:function(dataModel){
      var scopeObj = this;
      var data=dataModel.data;
      var index=dataModel.index;
      data[index].template = "flxBillPayManagePayeesSelected";		
      scopeObj.view.tableView.segmentBillpay.setDataAt(data[index], index);
      scopeObj.view.forceLayout();
    },
    /**
     * Display View Activities
     * 
     */
    showPayeeViewActivities: function (viewModel) {
      var scopeObj = this;
      var data = this.view.tableView.segmentBillpay.data;		
      var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];	
      var sendData={
        "data":data,
        "index":index
      };
      this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
      }, {
        text: "VIEW ACTIVITY"
      }]);
      this.view.transferActivitymodified.btnbacktopayeelist.onClick = scopeObj.onViewActiviyBackToPayeeList.bind(scopeObj, sendData);
      if (viewModel) {
        //TODO : update UI.
        kony.print(viewModel);
        if (viewModel.length > 0) {
          //this.view.flxBillPayManagePayeeActivity.setVisibility(true);
          this.view.transferActivitymodified.setVisibility(true);
          this.view.transferActivitymodified.flxSegmentBillPay.setVisibility(true);
          this.view.transferActivitymodified.flxNoRecords.setVisibility(false);
          scopeObj.bindViewActivityData(viewModel);
          this.view.flxPagination.setVisibility(false);
          this.view.flxBillPay.setVisibility(false);
          this.view.flxBillPayManagePayeeActivitymodified.setVisibility(true);
          this.view.forceLayout();
        }
        else {
          //this.view.flxBillPayManagePayeeActivity.setVisibility(false);
          this.view.transferActivitymodified.setVisibility(true);
          this.view.flxBillPay.setVisibility(false);
          this.view.flxPagination.setVisibility(false);
          this.view.transferActivitymodified.lblAmountDeducted.text=scopeObj.currencySymbol+"0";
          this.view.transferActivitymodified.flxSegmentBillPay.setVisibility(false);
          this.view.transferActivitymodified.flxNoRecords.setVisibility(true);
          this.view.flxBillPayManagePayeeActivitymodified.setVisibility(true);
          this.view.forceLayout();
        }
      }
      else {
        scopeObj.onError("showPayeeViewActivities : Invalid ViewModel");
      }
    },

    /**
     * Handle on error scenarios
     * @param : error object to handle / string 
     *  
     */
    onError: function (obj) {
      var scopeObj = this;
      if (typeof obj === "string") {
        kony.print(obj);
      }
    },
    /**
     * Return formatted currency string 
     * @param : currency string/number Ex: "$15.23" / "15.23"
     * @return : currency value Ex : "$15.23"
     */
    formatCurrency: function (value) {
      var scopeObj = this;
      if (!value) {
        return scopeObj.onError("Invalid Currency.");
      }
      return value.toString().indexOf(scopeObj.currencySymbol) >= 0 ? value : scopeObj.addCurrency(value);
    },

    /**
     * Return raw currency value 
     * @param : currency string/number Ex: "$15.23" / "15.23"
     * @return : currency value Ex : "15.23"
     */
    getCurrencyValue: function (value) {
      var scopeObj = this;
      if (!value) {
        return scopeObj.onError("Invalid Currency.");
      }
      return value.toString().indexOf(scopeObj.currencySymbol) >= 0 ? value.toString().replace(scopeObj.currencySymbol, '') : value;
    },

    /**
     * Return append/prepend currency symbol. 
     * @param : currency number Ex: "15.23" 
     * @return : currency string Ex : "$15.23"
     */
    addCurrency: function (value) {
      return value ? this.presenter.formatCurrency(value) : null;
    },

    /**
     * Utility method for Date formatter
     * @param : {string} dateStr , date in string format
     * @return : {string} date in dd/mm/yyyy format
     */
    formatDate: function (value, format) {
      return value ? this.presenter.getDateFromDateString(value, format) : null;
    },

    /**
     * Hide Progress bar
     */
    hideProgressBar : function(data){
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
     * Hide Progress bar
     */
    showProgressBar : function(data){
      CommonUtilities.showProgressBar(this.view);
    },

    /**
     * On Flx Bill Name Click handler
     */
    onManagePayeeBillerNameClickHandler: function(event, data){
      var scopeObj = this;
      scopeObj.showProgressBar();
      scopeObj.presenter.managePayeeData(data);        
      scopeObj.setSearchFlexVisibility(false);
    },

    /**
     * On Scheduled sort Click handler
     */
    onScheduledSortClickHandler: function(event, data){
      var scopeObj = this;
      scopeObj.showProgressBar();
      scopeObj.presenter.fetchScheduledBills(data);        
      scopeObj.setSearchFlexVisibility(false);
    },

    /**
     * On History Sort Click handler
     */
    onHistorySortClickHandler: function(event, data){
      var scopeObj = this;
      scopeObj.showProgressBar();
      scopeObj.presenter.showBillPayHistory(null, data);        
      scopeObj.setSearchFlexVisibility(false);
    },

    /**
     * On Payment Due Sort Click handler
     */
    onPaymentDueSortClickHandler: function(event, data){
      var scopeObj = this;
      scopeObj.showProgressBar();
      scopeObj.presenter.fetchPaymentDueBills(data);        
      scopeObj.setSearchFlexVisibility(false);
    },

    /**
     * On All payees Sort Click handler
     */
    onAllPayeeSortClickHanlder: function(event, data){
      var scopeObj = this;
      scopeObj.showProgressBar();
      scopeObj.presenter.allPayeeData(data);        
    },


    /**
	 * To hide pay a bill UI
	*/
    hideAllFlx:function(){
      this.view.p2pActivity.setVisibility(false);	
      this.view.payABill.setVisibility(false);
    },

    /**
     * Localized strings.
     */
    currencySymbol: kony.i18n.getLocalizedString("i18n.common.currencySymbol"),
    nonValue: kony.i18n.getLocalizedString("i18n.common.none"),
    //Load Modules
    OLBConstants: require('OLBConstants'),
    SEGMENT_ROW_LIMIT: 10,
    activateEbillUI:function(data,offset){
      var self=this;
      var height = self.view.flxHeader.frame.height + self.view.flxContainer.frame.height;		         
      self.view.flxActivateBiller.height = height + "dp";
      self.view.ActivateBiller.lblAccountNumberValue.text=data.accountNumber?data.accountNumber:" ";
      self.view.ActivateBiller.lblBillerNameValue.text=data.payeeNickName?data.payeeNickName:data.payeeName;
      self.view.flxActivateBiller.setVisibility(true);      
      self.view.ActivateBiller.btnProceed.onClick=function(){
        self.showProgressBar();
        self.view.flxActivateBiller.setVisibility(false);
        self.presenter.modifyEbillStatus(data,offset);  
      };
      self.view.ActivateBiller.btnCancel.onClick=function(){
        self.view.flxActivateBiller.setVisibility(false);
      };
      self.view.ActivateBiller.flxCross.onClick=function(){
        self.view.flxActivateBiller.setVisibility(false);
      };
      self.view.ActivateBiller.flxHeader.setFocus(true);
    }
  }
});
