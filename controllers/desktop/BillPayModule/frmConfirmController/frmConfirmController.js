define(['commonUtilities'],function(commonUtilities){
  return {
  willUpdateUI: function (viewModel) {
    kony.olb.utils.hideProgressBar(this.view);    
    if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
    if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
    if(viewModel.transferConfirm){
      this.updateTransferConfirmDetails(viewModel.transferConfirm);
    }
    if(viewModel.payABill){
      this.updateBillPayConfirmForm(viewModel.payABill);
      this.bindDataValues(viewModel.payABill);
    }
    if(viewModel.bulkPayRecords){
      this.preShowFrmConfirmBulkPay();
      this.setDataForConfirmBulkPay(viewModel.bulkPayRecords);
    }
    this.AdjustScreen();
  },
  setDataForConfirmBulkPay: function(bulkPayRecords){
    this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
    bulkPayWidgetDataMap = {
      "lblPaymentAccount": "lblPaymentAccount",
      "lblPayee": "lblPayee",
      "lblSendOn": "lblSendOn",
      "lblDeliverBy": "lblDeliverBy",
      "lblAmount": "lblAmount",
  };
  var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
  this.view.confirmButtons.btnCancel.onClick = billPayModule.presentationController.allPayeeData.bind(this.presenter);
  this.view.confirmButtons.btnModify.onClick = billPayModule.presentationController.modifyBulkPay.bind(this.presenter, bulkPayRecords.bulkPayRecords);
  this.view.confirmButtons.btnConfirm.onClick = billPayModule.presentationController.createBulkPayments.bind(this.presenter, bulkPayRecords.records);
  this.view.lblAmountValue.text =  bulkPayRecords.totalSum;
  this.view.segBill.widgetDataMap = bulkPayWidgetDataMap;
  this.view.segBill.setData(bulkPayRecords.records);
  this.view.forceLayout();
  },
  
  updateTransferConfirmDetails: function (viewModel) {
    var scopeObj = this;
    this.preShowFrmConfirmSinglePay();  
    this.view.flxPopup.setVisibility(false);    
  this.view.confirmDialog.flxdeliverby.setVisibility(false);
	this.view.confirmDialog.keyValueFrequency.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"); 
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfer')}, {text:kony.i18n.getLocalizedString("i18n.transfers.confirmTransfer")}]);
	 this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.confirmTransfer");
    this.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString('i18n.transfer.QuitTransfe');
    this.view.lblConfirmBillPay.text = kony.i18n.getLocalizedString('i18n.transfers.confirmTransfer');
    this.view.confirmDialog.keyValueFrom.lblValue.text = viewModel.getAccountName(viewModel.accountFrom) + '....'+ viewModel.getLastFourDigit(viewModel.accountFrom.accountID);
    this.view.confirmDialog.keyValueTo.lblValue.text = viewModel.accountTo.accountID ? (viewModel.getAccountName(viewModel.accountTo) + '....'+ viewModel.getLastFourDigit(viewModel.accountTo.accountID)) : viewModel.accountTo.nickName;
    this.view.confirmDialog.keyValueAmount.lblValue.text = this.presenter.formatCurrency(viewModel.amount, true);
    this.view.confirmDialog.keyValuePaymentDate.lblValue.text = viewModel.sendOnDate;
    this.view.confirmDialog.keyValueFrequency.lblValue.text = kony.i18n.getLocalizedString(viewModel.frequency);
    this.view.confirmDialog.keyValueNote.lblValue.text = viewModel.notes;
    if (viewModel.frequencyKey !== "Once" && viewModel.howLongKey === "DATE_RANGE") {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
      this.view.confirmDialog.keyValueFrequencyType.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblendingon");
      this.view.confirmDialog.keyValueFrequencyType.lblValue.text = viewModel.endOnDate;
    }
    else if (viewModel.frequencyKey !== "Once" && viewModel.howLongKey === "NO_OF_RECURRENCES") {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
      this.view.confirmDialog.keyValueFrequencyType.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
      this.view.confirmDialog.keyValueFrequencyType.lblValue.text = viewModel.noOfRecurrences;
    }
    else {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
    }
    this.view.confirmDialog.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmTransaction');
    this.view.confirmDialog.confirmButtons.btnConfirm.onClick = function () {
      kony.olb.utils.showProgressBar(this.view);      
      viewModel.onTransactionSubmit(viewModel);
    }.bind(this);
    this.view.confirmDialog.confirmButtons.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.common.cancelTransaction');
    this.view.confirmDialog.confirmButtons.btnCancel.onClick = this.showTransferCancelPopup.bind(this, function () {
      viewModel.cancelTransaction(viewModel);
    })
    this.view.confirmDialog.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');
    this.view.confirmDialog.confirmButtons.btnModify.onClick = function () {
      viewModel.modifyTransaction(viewModel);
    }
  },
  showTransferCancelPopup: function (onCancelListener) {
    var self = this;
    this.view.CustomPopup.lblHeading.text = "QUIT TRANSFERS";    
    this.view.flxPopup.setVisibility(true);
    var height = self.view.flxHeader.frame.height + self.view.flxMainContainer.frame.height;
    self.view.flxPopup.height = height + "dp";
    this.view.CustomPopup.btnYes.onClick = onCancelListener;
    this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString('i18n.common.cancelTransaction');
    this.view.CustomPopup.flxCross.onClick = function() {
      self.view.flxPopup.setVisibility(false);	
    };
    this.view.CustomPopup.btnNo.onClick =function(){
      self.view.flxPopup.setVisibility(false);
    }
    this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString('i18n.common.dontCancelTransaction');
    this.view.forceLayout();
  },

  initActions: function () {
    this.view.customheader.topmenu.flxMenu.skin="slFbox";
         this.view.customheader.topmenu.flxaccounts.skin="slFbox";
         this.view.customheader.topmenu.flxTransfersAndPay.skin="sknFlxFFFFFbrdr3343a8";
         this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
         this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
         this.view.customheader.topmenu.flxaccounts.skin="sknHoverTopmenu7f7f7pointer"; 
    var scopeObj = this;
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopupLogout.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopupLogout.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainContainer.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    };

    this.view.CustomPopupLogout.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopupLogout.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";

    }
    this.view.CustomPopupLogout.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    }
  },
  
  showBulkPayCancelPopUp: function(){
    var self = this;
    this.view.flxPopup.setVisibility(true);
    var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
    var height = self.view.flxHeader.frame.height + self.view.flxMainContainer.frame.height;
    self.view.flxPopup.height = height + "dp";
    this.view.CustomPopup.btnYes.onClick = billPayModule.presentationController.allPayeeData.bind(billPayModule.presentationController);
    this.view.CustomPopup.flxCross.onClick = function() {
      self.view.flxPopup.setVisibility(false);	
    };
    this.view.CustomPopup.btnNo.onClick =function(){
      self.view.flxPopup.setVisibility(false);
    }
    this.view.forceLayout();
      },
      preShowFrmConfirmBulkPay: function() {
          this.view.breadcrumb.setBreadcrumbData([{
      text: "BILL PAY",callback:this.showBulkPayCancelPopUp
          }, {
      text: "CONFIRM BILL PAY"
          }]);
		   this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
           this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billpay.confirmbillpayCapsOn");
          this.view.flxMainSinglePay.setVisibility(false);
          this.view.flxMainBulkPay.setVisibility(true);
          this.view.flxPopup.setVisibility(false);
      },
  preShowFrmConfirmSinglePay: function () {
    this.view.flxMainSinglePay.setVisibility(true);
    this.view.flxMainBulkPay.setVisibility(false);
    this.view.customheader.forceCloseHamburger();
  },
  postShowFrmConfirm: function () {
    this.view.confirmButtons.skin = "sknFlxe9ebeeop100";
    this.customiseHeaderComponentforAcknowledgement();
    this.AdjustScreen();
  },
     //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  },       
 
  /* Bind data form */
  bindDataValues: function (data) {
    var self = this;
    this.view.confirmDialog.keyValueFrom.lblValue.text = data.payFrom;
    this.view.confirmDialog.keyValueTo.lblValue.text = data.payeeName;
    this.view.confirmDialog.keyValueAmount.lblValue.text = data.amount;
    this.view.confirmDialog.keyValuePaymentDate.lblValue.text = data.sendOn;
    this.view.confirmDialog.keyValueFrequency.lblValue.text = data.categories;
    this.view.confirmDialog.keyValueFrequency.lblKey.text = kony.i18n.getLocalizedString("i18n.billPay.category");
    this.view.confirmDialog.keyValueNote.lblValue.text = data.notes;
    this.view.confirmDialog.flxdeliverby.lblKey.text = "Deliver By";
    this.view.confirmDialog.flxdeliverby.lblValue.text =  data.deliveryDate;
    this.view.forceLayout();
  },

  customiseHeaderComponentforAcknowledgement: function () {
    this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
    this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
  },
  showAcknowledgementBulkPay: function () {
    var navObj = new kony.mvc.Navigation("frmAcknowledgement");
    navObj.navigate(this.context);
  },
  updateBillPayConfirmForm:function(viewModel){
     var self=this;
     this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
     this.view.flxMainSinglePay.setVisibility(true);
	 this.view.confirmDialog.flxdeliverby.setVisibility(true);
     this.view.flxMainBulkPay.setVisibility(false);
     this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.billPay.BillPay')}, {text:kony.i18n.getLocalizedString("i18n.billpay.confirmbillpayCapsOn")}]);
	this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billpay.confirmbillpayCapsOn");
	this.view.CustomPopup.lblHeading.text=kony.i18n.getLocalizedString('i18n.billPay.QuitBillPay');
     this.view.confirmDialog.confirmButtons.btnCancel.onClick = function () {
       var height_to_set = 140 + self.view.flxMainContainer.frame.height;
       self.view.flxPopup.height = height_to_set + "dp";
       self.view.flxPopup.setVisibility(true);
	};
    this.view.confirmDialog.confirmButtons.btnModify.onClick = function () {
        if(viewModel.gettingFromOneTimePayment)
          {
            self.presenter.showOneTimePayment(viewModel);
          }else
            {
              if(viewModel.context == "quickAction")
                {
                  self.presenter.showPayABill(viewModel,"quickAction");
                }else
                {
                  self.presenter.showPayABillModify(viewModel);
                }
              
            }  	
    };
     this.view.confirmDialog.confirmButtons.btnConfirm.onClick = function () {
      var obj1 = {
        "showFlex": "singlepay"
      };
      kony.olb.utils.showProgressBar(this.view);
      self.presenter.singlePayAcknowlegement(viewModel);
    }.bind(this);
    this.view.CustomPopup.btnYes.onClick = function () {
		self.view.flxPopup.setVisibility(false);
      	self.presenter.showBillPayData();
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      self.view.flxPopup.setVisibility(false);
      self.view.forceLayout(); 
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      self.view.flxPopup.setVisibility(false);
      self.view.forceLayout();
    };
  },

  updateHamburgerMenu: function(sideMenuModel) {
        if (this.presenter && this.presenter.loadHamburgerTransfers) {
            this.view.customheader.initHamburger(sideMenuModel, "Transfers", "Transfer Money");
        }
        if (this.presenter && this.presenter.loadHamburgerBillPay) {
            this.view.customheader.initHamburger(sideMenuModel, "Bill Pay", "Pay a Bill");
        }
    },
  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  }
} 
});
