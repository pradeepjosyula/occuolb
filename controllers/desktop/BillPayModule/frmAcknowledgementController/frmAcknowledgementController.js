define(['commonUtilities','OLBConstants'],function(commonUtilities,OLBConstants){
  
  return {
  initActions: function () {
    var scopeObj = this;
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    // this.view.btnMakeAnotherPayment.onClick = function () {
    //   var navObj = new kony.mvc.Navigation("frmBillPay");
    //   navObj.navigate();
    // };
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CustomPopup.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    // this.view.btnViewPaymentActivity.onClick = function () {
    //   var obj1 = {
    //     "tabname": "history"
    //   };
    //   var navObj = new kony.mvc.Navigation("frmBillPay");
    //   navObj.navigate(obj1);
    // };
   this.view.btnSavePayee.onClick = this.onClickSavePayee;
   if(commonUtilities.getConfiguration("printingTransactionDetails")==="true"){
    this.view.imgPrintBulk.setVisibility(true);
    this.view.CopyimgPrint0cb1a69de676e48.setVisibility(true);     
    this.view.imgPrintBulk.onTouchStart = this.onClickPrintBulk;
    this.view.CopyimgPrint0cb1a69de676e48.onTouchStart = this.onClickPrint;
   }else{
    this.view.imgPrintBulk.setVisibility(false);
    this.view.CopyimgPrint0cb1a69de676e48.setVisibility(false);
   }
  },
  onClickPrintBulk: function(){
    var module = this.view.lblBillPayAcknowledgement.text.split("-")[0];    
    this.presenter.showPrintPage({transactionList : {"details":this.view.segBill.data, "module" : module}}); 
  },
  onClickPrint : function(){
    var printData = [];
    printData.push({
      key: this.view.acknowledgment.lblRefrenceNumber.text,
      value: this.view.acknowledgment.lblRefrenceNumberValue.text
    });
    printData.push({
      key: "Account Name",
      value: this.view.Balance.lblSavingsAccount.text
    });
    printData.push({
      key: "Available Balance",
      value: this.view.Balance.lblBalanceValue.text
    });
    printData.push({
      key: this.view.confirmDialogAccounts.keyValueBankName.lblKey.text,
      value: this.view.confirmDialogAccounts.keyValueBankName.lblValue.text
    });
    printData.push({
      key: this.view.confirmDialogAccounts.keyValueCountryName.lblKey.text,
      value: this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text
    });
    printData.push({
      key: this.view.confirmDialogAccounts.keyValueAccountType.lblKey.text,
      value: this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text
    });
    printData.push({
      key: this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text,
      value: this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text
    });
    printData.push({
      key: this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text,
      value: this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text
    });
    printData.push({
      key: this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text,
      value: this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text
    });
    printData.push({
      key: this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text,
      value: this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text
    });    
    if(this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text != ""){
      printData.push({
        key: this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text,
        value: this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text
      });
    }
    var module = this.view.lblBillPayAcknowledgement.text.split("-")[0];    
    this.presenter.showPrintPage({transactionSuccessData : {"details" : printData, "module" : module}});
  },  
  onClickSavePayee : function(){
    var AddPayeeModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AddPayeeModule");
    AddPayeeModule.presentationController.showUpdateBillerPageWithTransaction({
      transactionId : this.view.acknowledgment.lblRefrenceNumberValue.text
    });    
  },
  willUpdateUI: function (viewModel) {
     if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
    if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
    if(viewModel.transferAcknowledge) {this.updateTransferAcknowledgeUI(viewModel.transferAcknowledge);}
    if(viewModel.ackPayABill)
      {
        this.updateBillPayAcknowledgeUI(viewModel.ackPayABill);
      }
      if (viewModel.bulkPay) {
        this.updateBulkPayUIAcknowledge(viewModel.bulkPay);
      }
    this.AdjustScreen();
  },
  onClickMakePayment: function(){
    this.presenter.showBillPayData(null, {
      show : 'AllPayees',
      sender : 'acknowledgement',
      resetSorting: true
    });  
  },
  onClickPaymentActivity: function(){
      this.presenter.showBillPayData(null, {
        show : 'History', 
        sender : 'acknowledgement',
        resetSorting: true
      });
  },
  updateBulkPayUIAcknowledge: function(viewModel) {
    var self = this;
    var totalSum = 0;
    this.view.breadcrumb.setBreadcrumbData([{
        text: "BILL PAY"
    }, {
        text: "BILL PAY-ACKNOWLEDGEMENT"
    }]);
	this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.billPayAcknowledgement");
    this.view.flxContainer.isVisible = false;
    this.view.flxMainBulkPay.isVisible = true;
    bulkPayAckWidgetDataMap = {
        "lblRefernceNumber": "lblRefernceNumber",
        "lblPayee": "lblPayee",
        "lblPayeeAddress": "lblPayeeAddress",
        "lblPaymentAccount": "lblPaymentAccount",
        "lblEndingBalanceAccount": "lblEndingBalanceAccount",
        "lblSendOn": "lblSendOn",
        "lblDeliverBy": "lblDeliverBy",
        "lblAmount": "lblAmount",
        "imgAcknowledgement": "imgAcknowledgement"
    };
    viewModel = viewModel.map(function(dataItem) {
        var ackImage;
        var deliverBy = dataItem.deliverBy;
    if(deliverBy.length > 10)
      deliverBy = self.presenter.getDateFromDateString(dataItem.deliverBy);
    else
      deliverBy = dataItem.deliverBy;
        if (dataItem.transactionId !== "" || dataItem.transactionId !== null || dataItem.transactionId !== undefined) {
            ackImage = "bulk_billpay_success.png";
            totalSum = totalSum + parseFloat(dataItem.amount);
        } else {
            ackImage = "bulk_pay_unsuccessfull.png";
        }
        return {
            "lblRefernceNumber": dataItem.transactionId,
            "lblPayee": dataItem.payeeName,
            "lblPayeeAddress": "Address: " + dataItem.payeeAddressLine1,
            "lblPaymentAccount": dataItem.fromAccountName,
            "lblEndingBalanceAccount": "Ending Balance: " + self.presenter.formatCurrency(dataItem.fromAccountBalance),
            "lblSendOn": self.presenter.getDateFromDateString(dataItem.transactionDate),
            "lblDeliverBy": deliverBy,
            "lblAmount": self.presenter.formatCurrency(dataItem.amount),
            "imgAcknowledgement": ackImage,
        };
      });
    this.view.lblAmountValue.text = this.view.lblAmountValue.text =  self.presenter.formatCurrency(totalSum);
    this.view.segBill.widgetDataMap = bulkPayAckWidgetDataMap;
    this.view.segBill.setData(viewModel);
    this.view.btnViewPaymentActivity.onClick = this.onClickPaymentActivity;
    this.view.btnMakeAnotherPayment.onClick = this.onClickMakePayment;
    this.view.flxSuccessMessage.setVisibility(false);
    this.view.forceLayout();
},
  
  updateTransferAcknowledgeUI: function (viewModel) {
    this.customizeUIForTransferAcknowledege();
    if (this.presenter.MakeTransfer.isFutureDate(viewModel.transferData.sendOnDateComponents) || viewModel.transferData.frequencyKey !== "Once") {
      this.showTransferAcknowledgeForScheduledTransaction(viewModel);
    }
    else{
      this.showTransferAcknowledgeForRecentTransaction(viewModel);
    }
      
    this.view.confirmDialogAccounts.keyValueBankName.lblValue.text =  viewModel.transferData.getAccountName(viewModel.transferData.accountFrom) + '....'+ viewModel.transferData.getLastFourDigit(viewModel.transferData.accountFrom.accountID);
    this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = viewModel.transferData.accountTo.beneficiaryName ? viewModel.transferData.accountTo.nickName : (viewModel.transferData.getAccountName(viewModel.transferData.accountTo) + '....'+ viewModel.transferData.getLastFourDigit(viewModel.transferData.accountTo.accountID)) ;
    this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = this.presenter.formatCurrency(viewModel.transferData.amount, true);
    this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = viewModel.transferData.sendOnDate;
    this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =  kony.i18n.getLocalizedString(viewModel.transferData.frequency);
    this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = viewModel.transferData.notes;
    this.view.btnMakeTransfer.onClick = function () {
      this.presenter.showTransferScreen();
    }.bind(this);
    this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString('i18n.transfers.make_transfer');
    this.view.btnAddAnotherAccount.onClick = function () {
      this.presenter.showTransferScreen({initialView: 'recent'});
    }.bind(this);
    this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString('i18n.transfers.addAnotherAccount');
    
  },
  showTransferAcknowledgeForRecentTransaction (viewModel) {
    this.view.acknowledgment.setVisibility(true);  
    this.view.Balance.setVisibility(true);
    this.view.acknowledgmentModify.setVisibility(false);    
    this.view.acknowledgment.lblRefrenceNumberValue.text = viewModel.transferData.referenceId;
    this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);    
    this.view.Balance.lblSavingsAccount.text = viewModel.transferData.accountFrom.accountName;
    this.view.Balance.lblBalanceValue.text =  this.presenter.formatCurrency(viewModel.transferData.accountFrom.availableBalance); 
  },
  showTransferAcknowledgeForScheduledTransaction (viewModel) {
    function getDateFromDateComponents (dateComponents) {
      var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
      return   [dateComponents[0], monthNames[dateComponents[1] - 1], dateComponents[2]].join(" ");      
    }
    this.view.acknowledgmentModify.setVisibility(true);
    this.view.acknowledgment.setVisibility(false);     
    this.view.Balance.setVisibility(false);
    this.view.acknowledgmentModify.btnModifyTransfer.text = kony.i18n.getLocalizedString('i18n.transfer.MODIFYTRANSACTION');
    this.view.acknowledgmentModify.btnModifyTransfer.onClick = this.modifyTransfer.bind(this, viewModel.transferData);
    this.view.acknowledgmentModify.btnModifyTransfer.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');    
    this.view.acknowledgmentModify.lblRefrenceNumberValue.text = viewModel.transferData.referenceId;
    this.view.acknowledgmentModify.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.Transfers.YourTransactionHasBeenScheduledfor");
    this.view.acknowledgmentModify.lblTransactionDate.text = getDateFromDateComponents(viewModel.transferData.sendOnDateComponents);
    if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.howLongKey === "DATE_RANGE") {
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblendingon");
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = viewModel.transferData.endOnDate;
    this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);    
    
    }
    else if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.howLongKey === "NO_OF_RECURRENCES") {
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = viewModel.transferData.noOfRecurrences;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);        
    }
    else {
    this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);        
    }
  },
  modifyTransfer: function (transferData) {
    this.presenter.showMakeTransferForEditTransaction({
      transactionType: this.presenter.MakeTransfer.getTransferType(transferData.accountTo),
      toAccountNumber: transferData.accountTo.accountID,
      fromAccountNumber: transferData.accountFrom.accountID,
      ExternalAccountNumber: transferData.accountTo.accountNumber,
      amount: transferData.amount,
      frequencyType: transferData.frequencyKey,
      noOfRecurrences: transferData.noOfRecurrences,
      notes: transferData.notes,
      scheduledDate: transferData.sendOnDate,
      frequencyEndDate: transferData.endOnDate,
      frequencyStartDate: transferData.sendOnDate,
      isScheduled: "1",
      transactionDate: transferData.sendOnDate,
      transactionId: transferData.referenceId,
    });
  },
  updateBillPayAcknowledgeUI: function (data) {
	var self =this;  
	if(data.savedData.gettingFromOneTimePayment)
       {
          this.view.btnSavePayee.setVisibility(true);
       }else
            {
              this.view.btnSavePayee.setVisibility(false);
          }  
    this.customizeUIForBillPayAcknowledege();
    this.registerActionsBillPay(data.savedData);
    this.view.acknowledgment.lblRefrenceNumberValue.text = data.successfulData;
    this.view.Balance.lblSavingsAccount.text = data.accountData.accountName;
    this.view.Balance.lblBalanceValue.text = commonUtilities.formatCurrencyWithCommas(data.accountData.availableBalance,false);    
    this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.savedData.payFrom;
    this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = data.savedData.payeeName;
    this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = data.savedData.amount;
    this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.savedData.sendOn;
    this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =  data.savedData.categories;
    this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.savedData.notes;
    this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text = "Note";
    this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text =  "Payment Date";
    this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text = kony.i18n.getLocalizedString("i18n.billPay.category");
	this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text =  "Deliver By";
	this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text =  data.savedData.deliveryDate;
	this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
    this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(true); 
  },
  registerActionsBillPay:function(data){
    var self = this;
    if (data.gettingFromOneTimePayment == true) {
      this.view.btnMakeTransfer.onClick = function() {
          self.presenter.showOneTimePayment(data,'acknowledgement');
      };
  } else {
      this.view.btnMakeTransfer.onClick = function() {
          self.presenter.showBillPayData(null, {
              show: 'PayABill',
              sender: 'acknowledgement',
              data: data
          });
      };
  }

    this.view.btnAddAnotherAccount.onClick = function () {
      kony.olb.utils.showProgressBar(this.view);
      self.presenter.showBillPayData("acknowledgement", {
        show: 'History',
        sender: 'acknowledgement',
        resetSorting: true
      });
    };
  },
  preShowFrmAcknowledgemnentBulkPay: function () {
	this.view.customheader.forceCloseHamburger();	  
    this.view.flxMainBulkPay.setVisibility(true);
    this.view.flxContainer.setVisibility(false);
  },
  preShowFrmAcknowledgmentSinglePay: function () {
    this.view.customheader.forceCloseHamburger();  
    this.view.flxMainBulkPay.setVisibility(false);
    this.view.flxContainer.setVisibility(true);
  },
  customizeUIForTransferAcknowledege : function() {
    this.view.flxMainBulkPay.setVisibility(false);
	  this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(false);     
    this.view.flxContainer.setVisibility(true);
    this.view.btnSavePayee.setVisibility(false);
    this.view.lblBillPayAcknowledgement.text = "Transfer Money- Acknowledgement";
    this.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
        }, {
          text: kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement")
        }]);
        this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfers");
        this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");		
        this.view.btnMakeTransfer.text = "MAKE ANOTHER TRANSFER";
          this.view.btnAddAnotherAccount.text = "VIEW TRANSFER ACTIVITY";
          this.view.lblBillPayAcknowledgement.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgementlbl");
          this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.acknowledgmentMsg");
  },
  customizeUIForBillPayAcknowledege : function() {
    this.view.flxMainBulkPay.setVisibility(false);
    this.view.flxContainer.setVisibility(true);
    this.view.acknowledgmentModify.setVisibility(false);
    this.view.acknowledgment.setVisibility(true);     
    this.view.Balance.setVisibility(true);
    this.view.lblBillPayAcknowledgement.text = "Bill Pay - Acknowledgement";
    this.view.breadcrumb.setBreadcrumbData([{
          text: "BILL PAY"
        }, {
          text: "ACKNOWLEDGEMENT"
        }]);
//     this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfers");
//     this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");				
    this.view.btnMakeTransfer.text = "MAKE ANOTHER PAYMENT";
    this.view.btnAddAnotherAccount.text = "VIEW PAYMENT ACTIVITY";
    this.view.lblBillPayAcknowledgement.text = kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay");
    this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.acknowledgmentMsg");
    this.view.forceLayout();
  },
  postShowFrmAcknowledgement: function () {
    this.view.confirmDialogAccounts.confirmButtons.setVisibility(false);
    this.customiseHeaderComponentforAcknowledgement();
    this.AdjustScreen();
    // this.bindDataToValuesInTransactionDeatils();
  },
   //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  },       

  /* Dummy data for testing 
  To be replaced by actual data from service calls */
  bindDataToValuesInTransactionDeatils: function () {
    this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = "Personal Checking....1234";
    this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = "Lake Jackson Water Corporation";
    this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = "$600";
    this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = "15-May-2017";
    this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text = "Once";
    this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = "Water Bill for the month of May";
    this.view.forceLayout();
  },

  customiseHeaderComponentforAcknowledgement: function () {
    this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
    this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
  },

  updateHamburgerMenu: function(sideMenuModel) {
        if (this.presenter && this.presenter.loadHamburgerTransfers) {
            this.view.customheader.initHamburger(sideMenuModel, "Transfers", "Transfer Money");
        }
        if (this.presenter && this.presenter.loadHamburgerBillPay) {
            this.view.customheader.initHamburger(sideMenuModel, "Bill Pay", "Pay A Bill");
        }
    },
  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);    
  } 

}});
