define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmStopPayments **/
    AS_Form_f3e78a5e6a0342a7b9f626ade63f074d: function AS_Form_f3e78a5e6a0342a7b9f626ade63f074d(eventobject) {
        var self = this;
        this.frmStopPaymentsInitAction();
    },
    /** postShow defined for frmStopPayments **/
    AS_Form_f72b4190d92c4539a0ddde4bfa559352: function AS_Form_f72b4190d92c4539a0ddde4bfa559352(eventobject) {
        var self = this;
        this.postShowfrmStopCheckPayments();
    }
});