define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities, OLBConstants) {

    return {

        /**
         * frmStopPaymentsInitAction : Form Init action handler
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        frmStopPaymentsInitAction: function(){
            this.setFormActions();
            this.setHeaderActions();
            this.setFooter();
            this.clearData();
            this.view.StopCheckPaymentSeriesMultiple.lblSingleMultipleChecks.text = kony.i18n.getLocalizedString("i18n.StopPayments.SingleCheck"); 
            this.view.MyRequestsTabs.lblStatusDC.text = kony.i18n.getLocalizedString("i18n.StopPayments.Status");
            this.view.ConfirmDetailsSingleMultiple.btnViewRequests.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewRequests"));
            this.view.ConfirmDetailsSingleMultiple.btnBackToAccoutnDetails.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.ViewStatements.BackToAccountDetails"));
        },

        /**
        * getPageHeight : Return Page height.
        * @member of {frmAccountsLandingController}
        * @param {} 
        * @return {} 
        * @throws {}
        */
        getPageHeight: function () {
            return this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height;
        },

        /**
         * clearData : clear dummy/sample data.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        clearData: function(){
            var scopeObj = this;
            scopeObj.view.MyRequestsTabs.segTransactions.setData([]);
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setData([]);
        },
         /**
         * setFooter : set form footer.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        setFooter: function(){
            this.view.flxFooter.setVisibility(true);
        },

         /**
         * postShowfrmStopCheckPayments : Form postShow action handler
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        postShowfrmStopCheckPayments: function () {
            this.AdjustScreen();
            this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "Stop Payment Requests");
        },

        
        /**
         * AdjustScreen : Ui team proposed method to handle screen aligment
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        AdjustScreen: function () {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                }
                else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
        },

        /** 
        * setBreadcrumb :Resets Bread crumb state
        * @member  {frmStopPaymentsController}
        * @param {Array} viewModel , Breadcrumb lables object array { lable , toolTip}
        * @returns
        * @throws
        */
        setBreadcrumb: function (viewModel) {
            var scopeObj = this;
            if(viewModel && viewModel.length > 1) {
                scopeObj.view.breadcrumb.setBreadcrumbData([{
                    text: viewModel[0].label
                }, {
                    text: viewModel[1].label
                }]);
                scopeObj.view.breadcrumb.btnBreadcrumb1.toolTip = viewModel[0].toolTip;
                scopeObj.view.breadcrumb.lblBreadcrumb2.toolTip = viewModel[1].toolTip;
            }
        },
        
        /**
         * setFormActions : Method to bind all action in form
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        setFormActions: function () {
            var scopeObj = this;
            
            scopeObj.view.btnViewDisputedTransactions.onClick = scopeObj.btnViewDisputedTransactionsClickHandler.bind(scopeObj);
            scopeObj.view.btnViewDisputedTransactions.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewDisputedTransactions"));
	        scopeObj.view.btnViewDisputedChecks.onClick = scopeObj.btnViewDisputedChecks.bind(scopeObj);
            scopeObj.view.btnViewDisputedChecks.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewDisputedChecks"));
            
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.onClick = scopeObj.onSingleOrMultipleRadioButtonClick.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.onClick = scopeObj.onSeriesRadioButtonClick.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxTCContentsCheckbox.onClick = scopeObj.onTCCClickHanlder.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.btnTermsAndConditions.onClick = scopeObj.onTandCBtnClickHandler.bind(scopeObj);
            scopeObj.view.flxTCContentsCheckbox.onClick = scopeObj.onTandContentCheckBoxClickHandler.bind(scopeObj);
            scopeObj.view.btnSave.onClick = scopeObj.onTandContentSaveClickHandler.bind(scopeObj);
            scopeObj.view.btnCancel.onClick = scopeObj.setTermsAndConditionsPopupState.bind(scopeObj, false);
            scopeObj.view.flxClose.onClick = scopeObj.setTermsAndConditionsPopupState.bind(scopeObj, false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.onTextChange = scopeObj.onSeriesLastCheck2TextChange.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.onTextChange = scopeObj.onSeriesLastCheckTextChange.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.onTextChange = scopeObj.onSeriesLastCheck2TextChange.bind(scopeObj);

            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);

            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.onKeyUp = scopeObj.updateStopChekFormContinueBtnState.bind(scopeObj);
            CommonUtilities.bindEventsForAmountField(scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount);

            scopeObj.view.imgCloseDowntimeWarning.onTouchEnd = function(){
                scopeObj.setServerError(false);
            };
            this.view.CancelStopCheckPayments.flxCross.onClick = function () {
                scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
            };
            this.view.CancelStopCheckPayments.btnNo.onClick = function () {
                scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
            };
            this.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.onClick = function () {
                scopeObj.noOfClonedChecks++;
                var newCheck = scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.clone(scopeObj.noOfClonedChecks);
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxStopCheckPayments.addAt(newCheck, 3);
            };
        },

        /**
         * setDisputedTransactionTabUI: Method to set My requests Disputed Transactions tab UI
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        setDisputedTransactionTabUI: function () {
            var scopeObj = this;
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.skin = OLBConstants.SKINS.STOPPAYMENTS_SELECT_TAB;
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.skin = OLBConstants.SKINS.STOPPAYMENTS_UNSELECT_TAB;
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.hoverSkin = OLBConstants.SKINS.STOPPAYMENTS_UNSELECT_HOVER;
            scopeObj.ShowAllSeperators();
            scopeObj.view.MyRequestsTabs.flxTabsSeperator3.setVisibility(false);
            scopeObj.view.MyRequestsTabs.flxSort.setVisibility(true);
            scopeObj.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(false);
            scopeObj.view.forceLayout();
        },

        /**
         * setDisputedChecksTabUI: Method to set My requests Disputed Checks tab UI
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        setDisputedChecksTabUI: function () {
            var scopeObj = this;
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.skin = OLBConstants.SKINS.STOPPAYMENTS_UNSELECT_TAB;
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.hoverSkin = OLBConstants.SKINS.STOPPAYMENTS_UNSELECT_HOVER;
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.skin = OLBConstants.SKINS.STOPPAYMENTS_SELECT_TAB;
            scopeObj.ShowAllSeperators();
            scopeObj.view.MyRequestsTabs.flxTabsSeperator1.setVisibility(false);
            scopeObj.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(true);
            scopeObj.view.MyRequestsTabs.flxSort.setVisibility(false);
            scopeObj.view.forceLayout();
        },
        ShowAllSeperators: function () {
            this.view.MyRequestsTabs.flxTabsSeperator1.setVisibility(true);
            this.view.MyRequestsTabs.flxTabsSeperator2.setVisibility(true);
            this.view.MyRequestsTabs.flxTabsSeperator3.setVisibility(true);
            this.view.forceLayout();
        },
 
        /**
         * showDisputeTransactionDetail : update flexes visibility w.r.t dispute transaction form
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        showDisputeTransactionDetail: function () {
            this.hideAll();

            this.setBreadcrumb([
                {
                label: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle"),
                toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                },
                {
                label: kony.i18n.getLocalizedString("i18n.accounts.disputeTransaction"),
                toolTip: kony.i18n.getLocalizedString("i18n.accounts.disputeTransaction")
                },
                ]);
            this.view.flxDisputedTransactionDetail.setVisibility(true);
            this.view.flxActionsDisputeTransactionDetails.setVisibility(true);
            this.view.flxDisputedTransactionDetails.setVisibility(true);
            this.view.DisputeTransactionDetail.setVisibility(true);
            this.view.StopCheckPaymentSeriesMultiple.setVisibility(false);
            this.view.DisputeTransactionDetail.confirmButtons.btnModify.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.CANCEL");
            this.view.DisputeTransactionDetail.confirmButtons.btnConfirm.text = kony.i18n.getLocalizedString("i18n.common.continue");
            this.view.DisputeTransactionDetail.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.DisputeTransactionDetails");
            this.view.DisputeTransactionDetail.confirmButtons.btnModify.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.CANCEL"));
            this.view.DisputeTransactionDetail.confirmButtons.btnConfirm.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.common.continue"));

            this.AdjustScreen();
        },

         /**
         * showStopCheckPaymentsSeriesMultiple : update flexes visibility w.r.t stop checks form
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        showStopCheckPaymentsSeriesMultiple: function () {
            var scopeObj = this;
            scopeObj.hideAll();
            scopeObj.view.flxDisputedTransactionDetail.setVisibility(true);
            scopeObj.view.flxDisputedTransactionDetails.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.setVisibility(true);
            scopeObj.view.flxActionsDisputeTransactionDetails.setVisibility(true);
            scopeObj.view.DisputeTransactionDetail.setVisibility(false);
            scopeObj.setValidationErrorMessageState(false);
            scopeObj.view.forceLayout();
        },

        /**
         * hideAll : Method to show hide all flexes in form
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        hideAll: function () {
            this.view.flxDowntimeWarning.setVisibility(false);
            this.view.flxDisputedTransactionDetail.setVisibility(false);
            this.view.flxMyRequestsTabs.setVisibility(false);
            this.view.flxSinglePayConfirm.setVisibility(false);
            this.view.flxAcknowledgement.setVisibility(false);
            this.view.forceLayout();
        },

        /**
         * shouldUpdateUI : Method to decide whether view model is defined or not 
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel, view model object
         * @return {bolean} true/false , whether view model is defined or not 
         * @throws {} 
         */
        shouldUpdateUI: function (viewModel) {
            return viewModel !== undefined && viewModel !== null;
        },

        /**
         * willUpdateUI : Method to update form UI w.r.t view Model data called by presentUserInterface
         * @member of {frmStopPaymentsController}
         * @param {obejct} viewModel, view model object
         * @return {}
         * @throws {} 
         */
        willUpdateUI: function (viewModel) {
            var scopeObj = this;
            if (viewModel.progressBar !== undefined) {
                if (viewModel.progressBar) {
                    scopeObj.setServerError(false);
                    CommonUtilities.showProgressBar(scopeObj.view);
                } else {
                    CommonUtilities.hideProgressBar(scopeObj.view);
                }
            } else {

                if (viewModel.serverError) {
                    scopeObj.setServerError(viewModel.serverError);
                } else if (viewModel.sideMenu) {
                    scopeObj.updateHamburgerMenu(viewModel.sideMenu);
                } else if (viewModel.topBar) {
                    scopeObj.updateTopBar(viewModel.topBar);
                } else if(viewModel.stopChecksFormData) {
                    scopeObj.showStopChecksForm(viewModel.stopChecksFormData);
                } else if(viewModel.stopChecksFormAccounts) {
                    scopeObj.setStopChecksFormAccountsDropdown( {
                        "ddnList": viewModel.stopChecksFormAccounts
                    });
                } else if (viewModel.disputeTransactionObject){
                    scopeObj.showDisputeTransactionDetailPage(viewModel.disputeTransactionObject);
                } else if(viewModel.successStopCheckRequest) {
                    scopeObj.showStopChekRequestAcknowledgment(viewModel.successStopCheckRequest);
                } else if (viewModel.disputeTransactionResponse) {
                    scopeObj.showConfirmationDisputeTransaction(viewModel.disputeTransactionResponse);
                } else if (viewModel.myRequests) {
                    scopeObj.showMyRequests(viewModel.myRequests);
                } else if (viewModel.stopCheckRequestsViewModel) {
                    scopeObj.showStopCheckRequests(viewModel.stopCheckRequestsViewModel);
                } else if (viewModel.viewDisputedRequestsResponse) {
                    scopeObj.showDisputeTransactionsRequests(viewModel.viewDisputedRequestsResponse);
                } else if (viewModel.cancelStopCheckAction) {
                    scopeObj.showCancelStopRequestUI(viewModel.cancelStopCheckAction);
                }
            }
            scopeObj.AdjustScreen();
        },

        /**
         * bindDisputeTransactionRequestsData : set widget data map for dispute transactions view requests
         * @member of {frmStopPaymentsController}
         * @param {Array} dispute transactions view model
         * @return {} 
         * @throws {} 
         */
        bindDisputeTransactionRequestsData: function (disputeTransactionRequests) {
            var scopeObj = this;
            var widgetDataMap = {
                "lblIdentifier": "lblIdentifier",
                "lblSeparator2": "lblSeparator2",
                "lblSeperator2": "lblSeperator2",
                "lblSeparatorActions": "lblSeparatorActions",
                "imgDropDown": "imgDropDown",
                "lblDate": "lblDate", 
                "btnCancelRequests": "btnCancelRequests", 
                "btnSendAMessage": "btnSendAMessage",
                "lblDescription": "lblDescription", 
                "lblFromAccount": "lblFromAccount", 
                "lblFromAccountData": "lblFromAccountData", 
                "lblDateOfDescriptionKey": "lblDateOfDescriptionKey",
                "lblDateOfDescriptionValue": "lblDateOfDescriptionValue",
                "lblReasonKey": "lblReasonKey",
                "lblReasonValue": "lblReasonValue",
                "lblReferenceNo": "lblReferenceNo", 
                "lblToAccount": "lblToAccount", 
                "lblToAccountData": "lblToAccountData",
                "CopylblFrequencyTitle0c4e7bef1ab0c44": "CopylblFrequencyTitle0c4e7bef1ab0c44", 
                "lblTransactionTypeValue": "lblTransactionTypeValue", 
                "lblDesciptionKey": "lblDesciptionKey",
                "lblDescriptionValue": "lblDescriptionValue", 
                "lblAmount": "lblAmount", 
                "lblStatus": "lblStatus", 
                "flxDropdown":"flxDropdown",


            };
            var dataMap = disputeTransactionRequests.map(function (requestObj) {
                return {
                    "template": "flxDisputedTransactionsUnSelected",
                    "imgDropDown": OLBConstants.IMAGES.ARRAOW_DOWN,
                    "flxDropdown":"flxDropdown",
                    "lblDate":requestObj.disputeDate,
                    
                    "btnSendAMessage": {
                        "text": kony.i18n.getLocalizedString("i18n.StopCheckPayments.SENDMessage"),
                        "toolTip": CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.SENDMessage")),
                        "onClick": requestObj.onSendMessageAction ? requestObj.onSendMessageAction : null,
                        "isVisible": requestObj.onSendMessageAction ? true : false,
                    },
                    
                    "btnCancelRequests": {
                        "isVisible": requestObj.onCancelRequest ? true : false ,
                        "text":  kony.i18n.getLocalizedString("i18n.StopPayments.CANCELREQUEST"),
                        "toolTip": CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopPayments.CANCELREQUEST")),
                        "onClick": requestObj.onCancelRequest,

                    },
                    "lblDescription": requestObj.transactionDesc,
                    "lblIdentifier": "lblIdentifier",
                    "lblSeparator2": "lblSeparator2",
                    "lblSeperator2": "lblSeperator2",
                    "lblSeparatorActions": "lblSeparatorActions",
                    
                    "lblFromAccount": kony.i18n.getLocalizedString("i18n.StopCheckPayments.FromAccount"), 
                    "lblFromAccountData": requestObj.fromAccount,
                    "lblDateOfDescriptionKey": kony.i18n.getLocalizedString("i18n.StopCheckPayments.DateOfTransaction"),
                    "lblDateOfDescriptionValue": requestObj.transactionDate,
                    "lblReasonKey": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Reason"),
                    "lblReasonValue": requestObj.disputeReason,
                    "lblReferenceNo": requestObj.transactionId, 
                    "lblToAccount": kony.i18n.getLocalizedString("i18n.StopPayments.checkRequest.toAccount"), 
                    "lblToAccountData": requestObj.toAccountName,
                    "CopylblFrequencyTitle0c4e7bef1ab0c44": kony.i18n.getLocalizedString("i18n.StopCheckPayments.TransactionType"),
                    "lblTransactionTypeValue": requestObj.transactionType, 
                    "lblDesciptionKey": kony.i18n.getLocalizedString("i18n.StopPayments.Description"),
                    "lblDescriptionValue": requestObj.disputeDescription, 
                    "lblAmount": requestObj.amount, 
                    "lblStatus": requestObj.disputeStatus, 
                    

                    
                };
            });
            scopeObj.view.MyRequestsTabs.segTransactions.widgetDataMap = widgetDataMap;
            scopeObj.view.MyRequestsTabs.segTransactions.setData(dataMap);
           
            scopeObj.view.MyRequestsTabs.segTransactions.setVisibility(true);
            scopeObj.view.forceLayout();
            scopeObj.AdjustScreen();
        },




       /**
         * bindDisputeTransactionReasonsDropdown : Method to bind Stop checks form reasons dropdown
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - dropdown view model
         * @return {}
         * @throws {} 
         */
        bindDisputeTransactionReasonsDropdown: function(viewModel){
            var scopeObj = this;
            if(viewModel && viewModel.list.length) {
                scopeObj.view.DisputeTransactionDetail.lstBoxSelectReasonForDispute.masterData = viewModel.list.map(function (reason) {
                    return [reason.id, reason.name];
                });
            }
        },

         /**
         * setDisputeTransactionReasonsDropdown : Method to set dispute transaction reasons dropdown 
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - dropdown view model
         * @return {}
         * @throws {} 
         */
        setDisputeTransactionReasonsDropdown: function(viewModel) {
            var scopeObj = this;
            if(viewModel && viewModel.ddnList && viewModel.ddnList.length) {
                scopeObj.bindDisputeTransactionReasonsDropdown( { 
                    list : viewModel.ddnList
                });
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lstBoxSelectReasonForDispute.selectedKey =  viewModel.selectedValue || scopeObj.savedSeriesCheckReason || scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.masterData[0][0];
        },

        /**
                * showConfirmationDisputeTransaction : Method to show showConfirmationDisputeTransaction
                * @member of {frmStopPaymentsController}
                * @param {object} backend response
                * @return {} none
                * @throws {} none
                */
        showConfirmationDisputeTransaction: function (viewModel) {

                this.hideAll();
                
                this.view.flxAcknowledgement.setVisibility(true);
                this.view.flxAcknowledgementMain.setVisibility(true);
                this.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.CardManagement.Acknowledgement");
                this.view.confirmDialog.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.YourTransactionDetails");
                this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.ViewStatements.BackToAccountDetails");
                this.view.btnAddAnotherAccount.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewRequests");
                this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.ViewStatements.BackToAccountDetails");
                this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewRequests");
                this.view.acknowledgmentMyRequests.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.CardManagement.Acknowledgement");
                this.view.lblBillPayAcknowledgement.text = kony.i18n.getLocalizedString("i18n.StoCheckPayment.DisputeTransactionAck");

                this.view.acknowledgmentMyRequests.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage");
                this.setBreadcrumb([
                    {
                        label: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle"),
                        toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                    },
                    {
                        label: kony.i18n.getLocalizedString("i18n.StoCheckPayment.DisputeTransactionAck"),
                        toolTip: kony.i18n.getLocalizedString("i18n.StoCheckPayment.DisputeTransactionAck")
                    },
                ]);


                this.view.confirmDialog.keyValueRoutingNumber.lblValue.text = viewModel.values.amount || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.confirmDialog.flxDescription.lblValue.text = viewModel.values.description || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.confirmDialog.keyValueAccountNumber.lblValue.text = viewModel.values.notes || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.confirmDialog.keyValueBankName.lblValue.text = viewModel.values.fromAccountNumber || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.confirmDialog.keyValueAccountNickName.lblValue.text = viewModel.values.referenceNumber || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.confirmDialog.flxDileveryBy.lblValue.text = viewModel.values.reason || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.confirmDialog.keyValueCountryName.lblValue.text = viewModel.values.toAccount || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.confirmDialog.keyValueAccountType.lblValue.text = viewModel.values.date || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.confirmDialog.keyValueBenificiaryName.lblValue.text = viewModel.values.types || kony.i18n.getLocalizedString("i18n.common.none");
                this.view.btnMakeTransfer.onClick = viewModel.values.onBacktoAccountDetails;
                var self = this;
                this.view.btnAddAnotherAccount.onClick = function () {
                   var viewModel = {selectTab : OLBConstants.DISPUTED_TRANSACTIONS };
                   self.presenter.showMyRequests(viewModel);
                   
                };
            
        },
        /**
         * showDisputeTransactionDetailPage : Method to show showDisputeTransactionDetailPage
         * @member of {frmStopPaymentsController}
         * @param {object} transaction object
         * @return {} none
         * @throws {} none
         */
        showDisputeTransactionDetailPage: function (viewModel) {
            var self = this;
            this.showDisputeTransactionDetail();

                this.setDisputeTransactionReasonsDropdown({
                    ddnList: self.presenter.getdisputeTransactionReasonsListViewModel(),
                    selectedValue : viewModel.checkReason || null
                });
            this.view.DisputeTransactionDetail.txtBoxDescription.text = "";
            this.view.DisputeTransactionDetail.keyValue1.lblValue.text = viewModel.data.fromAccountNumber || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue2.lblValue.text = viewModel.data.toAccount || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue3.lblValue.text = viewModel.data.amount || kony.i18n.getLocalizedString("i18n.common.none");
            var date = viewModel.data.date;
            this.view.DisputeTransactionDetail.keyValue4.lblValue.text = date || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue5.lblValue.text = viewModel.data.types || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue6.lblValue.text = viewModel.data.referenceNumber || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.keyValue7.lblValue.text = viewModel.data.notes || kony.i18n.getLocalizedString("i18n.common.none");
            this.view.DisputeTransactionDetail.txtBoxDescription.maxTextLength =  OLBConstants.NOTES_MAX_LENGTH ;
           
            this.view.DisputeTransactionDetail.confirmButtons.btnModify.onClick = viewModel.onCancel;
            
            this.view.DisputeTransactionDetail.confirmButtons.btnConfirm.onClick = function () {
                
                var input = {
                    fromAccountNumber: viewModel.data.fromAccountNumber ,
                    toAccount: viewModel.data.toAccount,
                    amount: viewModel.data.amount,
                    date: viewModel.data.date,
                    types: viewModel.data.types || kony.i18n.getLocalizedString("i18n.common.none"),
                    referenceNumber: viewModel.data.referenceNumber,
                    notes: viewModel.data.notes || kony.i18n.getLocalizedString("i18n.common.none"),
                    reason: self.view.DisputeTransactionDetail.lstBoxSelectReasonForDispute.selectedkeyvalue[1],
                    description: self.view.DisputeTransactionDetail.txtBoxDescription.text,
                    onBacktoAccountDetails: viewModel.onBacktoAccountDetails || null,
                };
                self.onBtnContinueDisputeTransaction(input, viewModel.onCancel);
                self.AdjustScreen();
            };
        },
        /**
         * onBtnContinueDisputeTransaction : Method to show confirmation page for dispute a transaction
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel
         * @return {} none
         * @throws {} none
         */
        onBtnContinueDisputeTransaction: function (input, onCancelAction) {
            this.view.flxDisputedTransactionDetail.setVisibility(false);
            this.view.flxSinglePayConfirm.setVisibility(true);
            this.view.flxTopHeader.setVisibility(true);
            this.view.flxSuccessMessageStopCheck.setVisibility(false);
            this.view.ConfirmDetailsSingleMultiple.setVisibility(true);
            this.view.ConfirmDetailsSingleMultiple.lblReason.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.Reason");
            this.view.ConfirmDetailsSingleMultiple.lblDescription.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.Description");
            this.view.ConfirmDetailsSingleMultiple.lblConfirmHeader.text =  kony.i18n.getLocalizedString("i18n.StopCheckPayments.DisputeTransactionDetails");
            this.view.ConfirmDetailsSingleMultiple.flxAckButtons.setVisibility(false);
            this.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle"),
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StopCheckPayments.ConfirmDisputeTransaction"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopCheckPayments.ConfirmDisputeTransaction")
                },
            ]);
            this.view.ConfirmDetailsSingleMultiple.rtxReason.text = input.reason;
            this.view.ConfirmDetailsSingleMultiple.rtxDescription.text = input.description;
            this.view.flxPrintACK.setVisibility(false);
            this.view.flxDownloadACK.setVisibility(false);
            this.view.lblHeader.text= kony.i18n.getLocalizedString("i18n.StopCheckPayments.ConfirmDisputeTransaction");
            var dataMap = {
                "rtxFrom1": "rtxFrom1",
                "rtxPayee1": "rtxPayee1",
                "rtxDate1": "rtxDate1",
                "rtxAmount1": "rtxAmount1",
                "rtxNotes1": "rtxNotes1",
                "rtxType1": "rtxType1",
                "rtxReferenceNumber1": "rtxReferenceNumber1",
                "lblFrom1": "lblFrom1",
                "lblPayee1": "lblPayee1",
                "lblDate1": "lblDate1",
                "lblAmount1": "lblAmount1",
                "lblNotes1": "lblNotes1",
                "lblType1": "lblType1",
                "lblReferenceNumber1": "lblReferenceNumber1",
            };
            var data = [];
            var segData = {
                "rtxFrom1": input.fromAccountNumber,
                "rtxPayee1": input.toAccount,
                "rtxDate1": input.date,
                "rtxAmount1": input.amount,
                "rtxNotes1": input.notes,
                "rtxType1": input.types,
                "rtxReferenceNumber1": input.referenceNumber,
                "lblFrom1": kony.i18n.getLocalizedString("i18n.StopCheckPayment.From"),
                "lblPayee1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Payee"),
                "lblDate1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.DatesDC"),
                "lblAmount1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Amount($)"),
                "lblNotes1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Notes"),
                "lblType1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Types"),
                "lblReferenceNumber1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Referenceno"),
            };
            data.push(segData);
            this.view.ConfirmDetailsSingleMultiple.segConfirmDetails.widgetDataMap = dataMap;
            this.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setData(data);
            
            var self = this;
            this.view.ConfirmDetailsSingleMultiple.setVisibility(true);
            this.view.ConfirmDetailsSingleMultiple.flxStep2Buttons.setVisibility(true);
            this.view.ConfirmDetailsSingleMultiple.btnCancel.onClick = onCancelAction;
            this.view.ConfirmDetailsSingleMultiple.btnModify.onClick = function () {
                var viewModel = { data: input };
                self.showDisputeTransactionDetail(viewModel);
            };
            this.view.ConfirmDetailsSingleMultiple.btnConfirm.onClick = function () {
                var params = {
                    transactionId: input.referenceNumber,
                    disputeReason: input.reason,
                    disputeDescription: input.description
                };
                self.presenter.createDisputeTransaction(params, input);
            };
            self.AdjustScreen();
        },




        /**
         * showStopChecksForm : Method to show stop cheks form view
         * @member of {frmStopPaymentsController}
         * @param {object} formDataVieModel - form prepopulated view model
         * @return {}
         * @throws {} 
         */
        showStopChecksForm: function(formDataVieModel) {
            var scopeObj = this;
            scopeObj.stopCheckRequestData = {};
            scopeObj.setBreadcrumb([
                { 
                    label : kony.i18n.getLocalizedString("i18n.accounts.accountsTitle") ,
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                },
                { 
                    label : kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS")
                },
            ]);
            if(formDataVieModel.showStopPaymentServiceFeesAndValidity) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblThisServicesIsChargeble.text = formDataVieModel.serviceChargableText || "";
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblThisServicesIsChargeble.setVisibility(true);
            } else {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblThisServicesIsChargeble.setVisibility(false);
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.dateformat = CommonUtilities.getConfiguration('frontendDateFormat');
            scopeObj.showStopCheckPaymentsSeriesMultiple();
            scopeObj.setStopChecksFormData(formDataVieModel);
            scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed.onClick = scopeObj.onStopCheckFormConfirmHandler.bind(scopeObj);
            scopeObj.onStopCheckCancel = formDataVieModel.onCancel || null;
            scopeObj.view.StopCheckPaymentSeriesMultiple.btnCancel.onClick = scopeObj.onStopCheckCancel;
        },

        /**
         * onStopCheckFormConfirmHandler : Stop check request confirm button handler
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        onStopCheckFormConfirmHandler: function () {
            var scopeObj = this;
            var hasInvaidInputs = true;
            if(CommonUtilities.isRadioBtnSelected(scopeObj.view.StopCheckPaymentSeriesMultiple.imgRadioBtn1)) {
                if (scopeObj.isValidStopCheckForm(OLBConstants.CHECK_REQUEST_TYPES.SINGLE)){ 
                    hasInvaidInputs = false;
                    scopeObj.setSingleOrMultipleCheckRequestConfirmPage();
                } 
            } else {
                if (scopeObj.isValidStopCheckForm(OLBConstants.CHECK_REQUEST_TYPES.SERIES)) {
                    hasInvaidInputs = false;
                    scopeObj.setSeriesCheckRequestConfirmPage();
                }
            }
            if(hasInvaidInputs) {
                scopeObj.setValidationErrorMessageState(true);
            }
            scopeObj.AdjustScreen();
        },
        
        /**
         * setValidationErrorMessageState : Set Stop checks Error message
         * @member of {frmStopPaymentsController}
         * @param {boolean} visible show or hide flag
         * @param {string} i18nKey, i18n key
         * @return {}
         * @throws {} 
         */
        setValidationErrorMessageState: function(visible, i18nKey){
            var scopeObj = this;
            if (visible) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblWarning.text = kony.i18n.getLocalizedString(i18nKey || scopeObj.validationErrorMsgI18nKey || "i18n.StopPayments.errormessages.InvalidDetails");
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lblWarning.setVisibility(visible);
            scopeObj.AdjustScreen();
        },
        /**
         * isValidStopCheckForm : Stop check request form vaidation handler
         * @member of {frmStopPaymentsController}
         * @param {string} requestType, check request type single/series
         * @return {boolean} isvalid form or not.
         * @throws {} 
         */
        isValidStopCheckForm: function(requestType){
            var scopeObj = this;
            scopeObj.validationErrorMsgI18nKey = "";
            var isValidCheckNumber = function(checkumber) {
                var checkNumberReg = /^\d+$/;
                return checkNumberReg.test(checkumber);
            };
            var payeeName = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.text;
            if(CommonUtilities.isEmptyString(payeeName.toString())){
                scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidPayeeName";
                return false;
            }
           
            switch(requestType){
                case OLBConstants.CHECK_REQUEST_TYPES.SINGLE:
                    var amount = CommonUtilities.deFormatAmount(scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.text);
                    var checkNumber1 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.text;
                    var checkDescription = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.text;
                    
                    if (CommonUtilities.isEmptyString(amount.toString()) || !CommonUtilities.isValidAmount(amount) || amount <= 0 ){
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidAmount";
                        return false;
                    }
                    if(!isValidCheckNumber(checkNumber1)) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidCheckNumber";
                        return false;
                    }
                    if(CommonUtilities.isEmptyString(checkDescription.toString())) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidDescription";
                        return false;
                    }
                break;
                case OLBConstants.CHECK_REQUEST_TYPES.SERIES:
                    var serCheckNumber1 =  scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text;
                    var sercheckNumber2 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text;
                    var serDescription = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.text;
                    if(!isValidCheckNumber(serCheckNumber1) || !isValidCheckNumber(sercheckNumber2)) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidCheckNumber";
                        return false;
                    }
                    var noOfChecks = scopeObj.getSeriesCheckNumbersCount();
                    if(noOfChecks <= 1 || noOfChecks > OLBConstants.MAX_CHECKS_COUNT) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidSeriesCheckNumbers";
                        return false;
                    }
                    if(CommonUtilities.isEmptyString(serDescription.toString())) {
                        scopeObj.validationErrorMsgI18nKey = "i18n.StopPayments.errormessages.InvalidDescription";
                        return false;
                    }
                break;
                default:
                    CommonUtilities.ErrorHandler.onError("Invalid Check request type : " + requestType);
                    return false;
            }
            return true;
        },

        /**
         * showStopCheckRequestCofirmPage : Method to show Stop check single request confirmation form.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        showStopCheckRequestCofirmPage: function(){
            var scopeObj = this;
            scopeObj.hideAll();
            scopeObj.view.flxSinglePayConfirm.setVisibility(true);
            scopeObj.view.flxTopHeader.setVisibility(true);
            scopeObj.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment");
            scopeObj.view.flxPrintACK.setVisibility(false);
            scopeObj.view.flxDownloadACK.setVisibility(false);
            scopeObj.view.flxSuccessMessageStopCheck.setVisibility(false);

            var confirmDetailsSingleMultiple =scopeObj.view.ConfirmDetailsSingleMultiple;
            confirmDetailsSingleMultiple.setVisibility(true);
            confirmDetailsSingleMultiple.lblConfirmHeader.text = kony.i18n.getLocalizedString("i18n.StopPayments.stopCheckPaymentDetails");
            confirmDetailsSingleMultiple.flxTransactionDetails.setVisibility(true);
            confirmDetailsSingleMultiple.flxTransactionDetailsRow1.setVisibility(true);
            confirmDetailsSingleMultiple.lblReason.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.from");
            scopeObj.stopCheckRequestData.fromAccountNumber = scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.selectedKeyValue[0];
            confirmDetailsSingleMultiple.rtxReason.text = scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.selectedKeyValue[1];

            confirmDetailsSingleMultiple.flxTransactionDetailsRow2.setVisibility(true);
            confirmDetailsSingleMultiple.lblDescription.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.Payee");
            scopeObj.stopCheckRequestData.payeeName = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.text.toString().trim();
            confirmDetailsSingleMultiple.rtxDescription.text = scopeObj.stopCheckRequestData.payeeName;
            confirmDetailsSingleMultiple.flxTransactionDetailsRow3.setVisibility(false);
            confirmDetailsSingleMultiple.flxStep2Buttons.setVisibility(true);
            confirmDetailsSingleMultiple.flxAckButtons.setVisibility(false);
            confirmDetailsSingleMultiple.btnModify.onClick = scopeObj.onStopCheckModifyClickHandler.bind(scopeObj);
            confirmDetailsSingleMultiple.btnCancel.onClick = scopeObj.onStopCheckCancel || scopeObj.onStopCheckModifyClickHandler.bind(scopeObj);
            scopeObj.AdjustScreen();
        },

        /**
         * onStopCheckModifyClickHandler : Stop check request confim page Modify click handler.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        onStopCheckModifyClickHandler: function(){
            var scopeObj = this;
            scopeObj.setBreadcrumb([
                { 
                    label : kony.i18n.getLocalizedString("i18n.accounts.accountsTitle") ,
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                },
                { 
                    label : kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS") ,
                    toolTip: kony.i18n.getLocalizedString("i18n.StopcheckPayments.STOPCHECKPAYMENTS")
                },
            ]);
            scopeObj.showStopCheckPaymentsSeriesMultiple();
            scopeObj.view.flxSinglePayConfirm.setVisibility(false);
            scopeObj.view.flxTopHeader.setVisibility(false);
            scopeObj.view.flxPrintACK.setVisibility(false);
            scopeObj.view.flxDownloadACK.setVisibility(false);
            scopeObj.view.flxSuccessMessageStopCheck.setVisibility(false);
            scopeObj.view.ConfirmDetailsSingleMultiple.setVisibility(false);  
            scopeObj.AdjustScreen();
        },
        
        /**
         * showStopCheckRequestCofirmPage : Method to set single or mutliple check request page
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        setSingleOrMultipleCheckRequestConfirmPage: function(){
            var scopeObj = this;
            scopeObj.setBreadcrumb([
                { 
                    label : kony.i18n.getLocalizedString("i18n.accounts.accountsTitle") ,
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                },
                { 
                    label : kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment")
                },
            ]);
            scopeObj.showStopCheckRequestCofirmPage();
            scopeObj.view.ConfirmDetailsSingleMultiple.btnConfirm.onClick = scopeObj.onSingleOrMultipeCheckRequestConfirmBtnClick.bind(scopeObj);
            scopeObj.setStopCheckSingleOrMutlipleTranSegment();
        },

        /**
         * setStopCheckSingleOrMutlipleTranSegment : Method to set single or mutliple check request transaction deatils in Stop payment confimatin
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        setStopCheckSingleOrMutlipleTranSegment: function(){
            var scopeObj = this;
            var dataMap = {
                "flxRecipientDetails1": "flxRecipientDetails1",
                "flxFrom1": "flxFrom1",
                "lblFrom1": "lblFrom1",
                "rtxFrom1": "rtxFrom1",
                "flxPayee1": "flxPayee1",
                "lblPayee1": "lblPayee1",
                "rtxPayee1": "rtxPayee1",
                "flxDate1": "flxDate1",
                "lblDate1": "lblDate1",
                "rtxDate1": "rtxDate1",
                "flxAmount1": "flxAmount1",
                "lblAmount1": "lblAmount1",
                "rtxAmount1": "rtxAmount1",
                "flxNotes1": "flxNotes1",
                "lblNotes1": "lblNotes1",
                "rtxNotes1": "rtxNotes1"
            };

            scopeObj.stopCheckRequestData.checkNumber1 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.text;
            scopeObj.stopCheckRequestData.amount = CommonUtilities.deFormatAmount(scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.text);
            scopeObj.stopCheckRequestData.checkDateOfIssue = scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.formattedDate;
            scopeObj.stopCheckRequestData.checkReason = scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.selectedKeyValue[0];
            scopeObj.stopCheckRequestData.description = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.text;

            
            var viewModel= [{
                "lblFrom1": kony.i18n.getLocalizedString("i18n.StopPayments.Checkno"),
                "rtxFrom1": scopeObj.stopCheckRequestData.checkNumber1,
                "lblPayee1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Amount($)"),
                "rtxPayee1": CommonUtilities.formatCurrencyWithCommas(scopeObj.stopCheckRequestData.amount, true),
                "lblDate1": kony.i18n.getLocalizedString("i18n.StopPayments.DateOfIssueWithColon"),
                "rtxDate1": scopeObj.stopCheckRequestData.checkDateOfIssue,
                "lblAmount1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Reason"),
                "rtxAmount1": scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.selectedKeyValue[1],
                "lblNotes1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Description"),
                "rtxNotes1": {
                    "bottom": "20dp",
                    "text": scopeObj.stopCheckRequestData.description
                }
            }];

            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setVisibility(true);
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.widgetDataMap = dataMap;
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setData(viewModel);
            scopeObj.view.forceLayout();
            scopeObj.AdjustScreen();
        },

        /**
         * onSingleOrMultipeCheckRequestConfirmBtnClick : Stop checks single/ multiple check confirmation page confirm button click handler
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        onSingleOrMultipeCheckRequestConfirmBtnClick: function() {
            var scopeObj = this;
            var requestModel = {
                transactionType : OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST,
                fromAccountNumber: scopeObj.stopCheckRequestData.fromAccountNumber,
                payeeName: scopeObj.stopCheckRequestData.payeeName,
                checkNumber1: scopeObj.stopCheckRequestData.checkNumber1,
                amount: scopeObj.stopCheckRequestData.amount,
                checkDateOfIssue: scopeObj.stopCheckRequestData.checkDateOfIssue,
                checkReason: scopeObj.stopCheckRequestData.checkReason,
                description: scopeObj.stopCheckRequestData.description         
            };
            scopeObj.presenter.stopCheckRequest(requestModel);
        },

        /**
         * onSeriesCheckRequestConfirmBtnClick : Stop checks series check confirmation page confirm button click handler
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        onSeriesCheckRequestConfirmBtnClick: function(){
            var scopeObj = this;
            var requestModel = {
                transactionType : OLBConstants.TRANSACTION_TYPE.STOPCHECKPAYMENTREQUEST,
                fromAccountNumber: scopeObj.stopCheckRequestData.fromAccountNumber,
                payeeName: scopeObj.stopCheckRequestData.payeeName,
                checkNumber1: scopeObj.stopCheckRequestData.checkNumber1,
                checkNumber2: scopeObj.stopCheckRequestData.checkNumber2,
                checkReason: scopeObj.stopCheckRequestData.checkReason,
                description: scopeObj.stopCheckRequestData.description         
            };
            scopeObj.presenter.stopCheckRequest(requestModel);
        },

        /**
         * setSeriesCheckRequestConfirmPage : Method to set series check request page
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        setSeriesCheckRequestConfirmPage: function(){
            var scopeObj = this;
            scopeObj.setBreadcrumb([
                { 
                    label : kony.i18n.getLocalizedString("i18n.accounts.accountsTitle") ,
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                },
                { 
                    label : kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopPayments.confirmStopCheckPayment")
                },
            ]);
            scopeObj.showStopCheckRequestCofirmPage();
            scopeObj.setStopCheckSeriesTranSegment();
            scopeObj.view.ConfirmDetailsSingleMultiple.btnConfirm.onClick = scopeObj.onSeriesCheckRequestConfirmBtnClick.bind(scopeObj);
        },

        /**
         * setStopCheckSeriesTranSegment : Method to set series check request transaction deatils in Stop payment confimatin
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        setStopCheckSeriesTranSegment: function(){
            var scopeObj = this;
            var dataMap = {
                "flxRecipientDetails1": "flxRecipientDetails1",
                "flxFrom1": "flxFrom1",
                "lblFrom1": "lblFrom1",
                "rtxFrom1": "rtxFrom1",
                "flxPayee1": "flxPayee1",
                "lblPayee1": "lblPayee1",
                "rtxPayee1": "rtxPayee1",
                "flxDate1": "flxDate1",
                "lblDate1": "lblDate1",
                "rtxDate1": "rtxDate1"
            };

            scopeObj.stopCheckRequestData.checkNumber1 =  scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text;
            scopeObj.stopCheckRequestData.checkNumber2 = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text;
            scopeObj.stopCheckRequestData.checkReason = scopeObj.view.StopCheckPaymentSeriesMultiple.lstBoxSelectReasonForDispute.selectedKeyValue[0];
            scopeObj.stopCheckRequestData.description = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.text;

            var viewModel= [{
                "lblFrom1": kony.i18n.getLocalizedString("i18n.StopPayments.Checkno"),
                "rtxFrom1": scopeObj.getSeriesCheckNumber() + "&nbsp;&nbsp;&nbsp;&nbsp;" + scopeObj.getSeriesCheckNumbersCount() +" "+ kony.i18n.getLocalizedString("i18n.StopPayments.ChecksSelected"),
                "lblPayee1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Reason"),
                "rtxPayee1": scopeObj.stopCheckRequestData.checkReason,
                "lblDate1": kony.i18n.getLocalizedString("i18n.StopCheckPayments.Description"),
                "rtxDate1":  {
                    "bottom": "20dp",
                    "text": scopeObj.stopCheckRequestData.description
                }
            }];
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setVisibility(true);
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.widgetDataMap = dataMap;
            scopeObj.view.ConfirmDetailsSingleMultiple.segConfirmDetails.setData(viewModel);
            scopeObj.view.forceLayout();
            scopeObj.AdjustScreen();
        },

        /**
         * getSeriesCheckNumber : Method to create and return series check number 
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {String} formatted check number for series
         * @throws {} 
         */
        getSeriesCheckNumber : function(){
            return this.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text + " " + OLBConstants.CHECK_SERIES_SEPARATOR + " " + this.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text;
        },

        /**
         * getSeriesCheckNumbersCount : Method to return total check number in series
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {Number} total checks count
         * @throws {} 
         */
        getSeriesCheckNumbersCount : function(){
            var firstCheckNumber = Number(this.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text);
            var lastCheckNumber = Number(this.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text);
            var count = (lastCheckNumber - firstCheckNumber) + 1 ;
            if(isNaN(count)) {
                
                return -1;
            }
            return count;
        },

         /**
         * setStopChecksFormData : Method to set Stop checks form input values
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - form prepopulated view model
         * @return {}
         * @throws {} 
         */
        setStopChecksFormData: function(viewModel){
            var scopeObj = this;
            scopeObj.setStopChecksFormAccountsDropdown({
                selectedValue : viewModel.accountID
            });
            scopeObj.savedSelectedAccount = viewModel.accountID;

            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.text = viewModel.payeeName || "";
            if(viewModel.isSeriesChecks) {
                scopeObj.onSeriesRadioButtonClick();
                scopeObj.setStopChecksFormSingleOrMultipleReasonsDropdown({
                    selectedValue : viewModel.checkReason
                });
                scopeObj.savedSeriesCheckReason = viewModel.checkReason;
                scopeObj.setSeriesChecksFormValues(viewModel);
            } else {
                scopeObj.onSingleOrMultipleRadioButtonClick();
                scopeObj.setStopChecksFormSeriesChecksReasonsDropdown({
                    selectedValue : viewModel.checkReason
                });
                scopeObj.savedSingleCheckReason = viewModel.checkReason;
                scopeObj.setSingleOrMultipleChecksFormValues(viewModel);
            }
        },

         /**
         * setStopChecksFormAccountsDropdown : Method to set Stop checks form accounts dropdown 
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - dropdown view model
         * @return {}
         * @throws {} 
         */
        setStopChecksFormAccountsDropdown: function(viewModel) {
            var scopeObj = this;
            if(viewModel && viewModel.ddnList && viewModel.ddnList.length) {
                scopeObj.bindStopChecksFormAccountsDropdown( { 
                    list : viewModel.ddnList
                });
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.selectedKey = viewModel.selectedValue || scopeObj.savedSelectedAccount || scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.masterData[0][0];
        },


        /**
         * onSingleOrMultipleRadioButtonClick : Stop Payments signle or multiple radion buttn click hanlder
         * @member of {frmStopPaymentsController}
         * @param {object} event, click event JS  object
         * @return {}
         * @throws {} 
         */
        onSingleOrMultipleRadioButtonClick: function(event){
            var scopeObj = this;
            if (scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.imgRadioBtn1.src === OLBConstants.IMAGES.ICON_RADIOBTN) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.imgRadioBtn1.src = OLBConstants.IMAGES.RADIOBTN_ACTIVE_SMALL;
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.imgRadioBtn2.src = OLBConstants.IMAGES.ICON_RADIOBTN;
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(false);
                scopeObj.setSingleOrMultipleChecksFormValues();
                scopeObj.setValidationErrorMessageState(false);
                scopeObj.AdjustScreen();
            }
        },

        /**
         * onSeriesRadioButtonClick : Stop Payments series radion buttn click hanlder
         * @member of {frmStopPaymentsController}
         * @param {object} event, click event JS  object
         * @return {}
         * @throws {} 
         */
        onSeriesRadioButtonClick: function(event){
            var scopeObj = this;
            if (scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.imgRadioBtn2.src === OLBConstants.IMAGES.ICON_RADIOBTN) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.imgRadioBtn2.src = OLBConstants.IMAGES.RADIOBTN_ACTIVE_SMALL;
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.imgRadioBtn1.src = OLBConstants.IMAGES.ICON_RADIOBTN;
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(false);
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(false);
                scopeObj.setSeriesChecksFormValues();
                scopeObj.setValidationErrorMessageState(false);
                scopeObj.AdjustScreen();
            }
        },

         /**
         * resetTCC: Stop Payments Term and Condition check box click handler
         * @member of {frmStopPaymentsController}
         * @param {object} event, click event JS  object
         * @return {}
         * @throws {} 
         */
        resetTCC : function(){
            var scopeObj = this;
            CommonUtilities.setCheckboxState(false, scopeObj.view.StopCheckPaymentSeriesMultiple.imgTCContentsCheckbox);
        },

        /**
         * updateStopChekFormContinueBtnState: Update Stop check request Continue button state w.r.t all mandatory fields and T&C Checkbox state
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        updateStopChekFormContinueBtnState : function(){
            var scopeObj = this;
            var tccCheckBox = scopeObj.view.StopCheckPaymentSeriesMultiple.imgTCContentsCheckbox;
            var tbxPayee = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxPayee.text.toString();
            var tbxFirstCheckNo = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text.toString();
            var tbxLastCheckNo = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.text.toString();
            var tbxCheckNo = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text.toString(); 
            var tbxDescription = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.text.toString();
            var tbxCheckNumber = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.text.toString();
            var tbxAmount = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.text.toString();
            var tbxDescriptionNew = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.text.toString();

            if(CommonUtilities.isRadioBtnSelected(scopeObj.view.StopCheckPaymentSeriesMultiple.imgRadioBtn1)) {
                if (CommonUtilities.isChecked(tccCheckBox) && 
                    !CommonUtilities.isEmptyString(tbxPayee) && 
                    !CommonUtilities.isEmptyString(tbxCheckNumber) && 
                    !CommonUtilities.isEmptyString(tbxAmount) && 
                    !CommonUtilities.isEmptyString(tbxDescriptionNew)) {
                    CommonUtilities.enableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
                } else {
                    CommonUtilities.disableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
                }
            } else {
                if (CommonUtilities.isChecked(tccCheckBox) && 
                    !CommonUtilities.isEmptyString(tbxPayee) && 
                    !CommonUtilities.isEmptyString(tbxFirstCheckNo) && 
                    ! (CommonUtilities.isEmptyString(tbxLastCheckNo)  || CommonUtilities.isEmptyString(tbxCheckNo)) && 
                    !CommonUtilities.isEmptyString(tbxDescription)) {
                    CommonUtilities.enableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
                } else {
                    CommonUtilities.disableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
                }
            }
        },
        /**
         * onTCCClickHanlder : Stop Payments Term and Condition check box click handler
         * @member of {frmStopPaymentsController}
         * @param {object} event, click event JS  object
         * @return {}
         * @throws {} 
         */
        onTCCClickHanlder: function () {
            var scopeObj = this;
            var tccCheckBox = scopeObj.view.StopCheckPaymentSeriesMultiple.imgTCContentsCheckbox;
            CommonUtilities.toggleCheckBox(tccCheckBox);
            scopeObj.updateStopChekFormContinueBtnState();
        },

        /**
         * setSingleOrMultipleChecksFormValues : Method to set Stop payments single/mutliple checks form values
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel, view model object
         * @return {}
         * @throws {} 
         */
        setSingleOrMultipleChecksFormValues: function(viewModel){
            var scopeObj = this;
            viewModel = viewModel || {};
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNumber.text = viewModel.checkNumber1 || "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.date = viewModel.checkDateOfIssue || CommonUtilities.getFrontendDateString(new Date(), CommonUtilities.getConfiguration("frontendDateFormat"));
            scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.dateComponents = scopeObj.view.StopCheckPaymentSeriesMultiple.calDateOfIssue.dateComponents; //Platform hack with date set.
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxAmount.text = viewModel.checkAmount ? CommonUtilities.formatCurrencyWithCommas(viewModel.checkAmount, true) : "";

            scopeObj.setStopChecksFormSingleOrMultipleReasonsDropdown({
                ddnList: scopeObj.presenter.getCheckReasonsListViewModel(),
                selectedValue : viewModel.checkReason || null
            });
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.placeholder = "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.maxTextLength = viewModel.maxDesriptionLength || OLBConstants.NOTES_MAX_LENGTH;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescriptionNew.text = viewModel.description || "";
            scopeObj.resetTCC();
            scopeObj.updateStopChekFormContinueBtnState();
        },

        /**
         * setSeriesChecksFormValues : Method to set Stop payments series checks form values
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel, view model object
         * @return {}
         * @throws {} 
         */
        setSeriesChecksFormValues : function(viewModel){
            var scopeObj = this;
            viewModel = viewModel || {};
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxFirstCheckNo.text = viewModel.checkNumber1 || "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.text = viewModel.checkNumber2 || "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text = viewModel.checkNumber2 || "";
            var noOfChecks = scopeObj.getSeriesCheckNumbersCount();
            if(noOfChecks > 1) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = noOfChecks +" "+ kony.i18n.getLocalizedString("i18n.StopPayments.ChecksSelected");
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.setVisibility(false);
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.setVisibility(true);
            } else {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = "";
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.setVisibility(false);
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.setVisibility(false);
            }
            scopeObj.setStopChecksFormSeriesChecksReasonsDropdown({
                ddnList: scopeObj.presenter.getCheckReasonsListViewModel(),
                selectedValue : viewModel.checkReason || null
            });
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.placeholder = "";
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.placeholder = kony.i18n.getLocalizedString("i18n.StopcheckPayments.LastCheckNumber");
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.maxTextLength = viewModel.maxDesriptionLength || OLBConstants.NOTES_MAX_LENGTH;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxDescription.text = viewModel.description || "";
            scopeObj.resetTCC();
            scopeObj.updateStopChekFormContinueBtnState();
        },

        /**
         * bindStopChecksFormAccountsDropdown : Method to bind Stop checks form accounts dropdown
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - dropdown view model
         * @return {}
         * @throws {} 
         */
        bindStopChecksFormAccountsDropdown: function(viewModel){
            var scopeObj = this;
            if(viewModel && viewModel.list.length) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lbxFrom.masterData = viewModel.list.map(function (account) {
                    return [account.accountID, account.accountName];
                });
            }
        },

         /**
         * setStopChecksFormSingleOrMultipleReasonsDropdown : Method to set Stop checks form single or multiple checks reasons dropdown 
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - dropdown view model
         * @return {}
         * @throws {} 
         */
        setStopChecksFormSingleOrMultipleReasonsDropdown: function(viewModel) {
            var scopeObj = this;
            if(viewModel && viewModel.ddnList && viewModel.ddnList.length) {
                scopeObj.bindStopChecksFormSingleOrMultipleReasonsDropdown( { 
                    list : viewModel.ddnList
                });
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.selectedKey = viewModel.selectedValue || scopeObj.savedSingleCheckReason || scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.masterData[0][0];
        },

        /**
         * bindStopChecksFormSingleOrMultipleReasonsDropdown : Method to bind Stop checks form reasons dropdown
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - dropdown view model
         * @return {}
         * @throws {} 
         */
        bindStopChecksFormSingleOrMultipleReasonsDropdown: function(viewModel){
            var scopeObj = this;
            if(viewModel && viewModel.list.length) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.masterData = viewModel.list.map(function (reason) {
                   return [reason.id, reason.name];
                });
            }
        },

        /**
         * setStopChecksFormSingleOrMultipleReasonsDropdown : Method to set Stop checks form single or multiple checks reasons dropdown 
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - dropdown view model
         * @return {}
         * @throws {} 
         */
        setStopChecksFormSeriesChecksReasonsDropdown: function(viewModel) {
            var scopeObj = this;
            if(viewModel && viewModel.ddnList && viewModel.ddnList.length) {
                scopeObj.bindStopChecksFormSeriesChecksReasonsDropdown( { 
                    list : viewModel.ddnList
                });
            }
            scopeObj.view.StopCheckPaymentSeriesMultiple.lstBoxSelectReasonForDispute.selectedKey =  viewModel.selectedValue || scopeObj.savedSeriesCheckReason || scopeObj.view.StopCheckPaymentSeriesMultiple.lbxSelectReasonNew.masterData[0][0];
        },

        /**
         * bindStopChecksFormSingleOrMultipleReasonsDropdown : Method to bind Stop checks form reasons dropdown
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel - dropdown view model
         * @return {}
         * @throws {} 
         */
        bindStopChecksFormSeriesChecksReasonsDropdown: function(viewModel){
            var scopeObj = this;
            if(viewModel && viewModel.list.length) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lstBoxSelectReasonForDispute.masterData = viewModel.list.map(function (reason) {
                    return [reason.id, reason.name];
                });
            }
        },

        /**
         * onTandCBtnClickHandler : Terms and Conditions button click handler.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        onTandCBtnClickHandler: function(){
            var scopeObj = this;
            scopeObj.setTermsAndConditionsPopupState({
                visible: true
            });
        },

        /**
         * setTermsAndConditionsPopupState : set Terms and Conditions popup.
         * @member of {frmStopPaymentsController}
         * @param {boolean} visible  is visibility true/false
         * @return {}
         * @throws {} 
         */
        setTermsAndConditionsPopupState: function(visible){
            var scopeObj = this;
            visible = visible || false;
            scopeObj.view.flxTermsAndConditions.height = scopeObj.getPageHeight() + "dp";
            scopeObj.view.flxTermsAndConditions.setVisibility(visible);
            scopeObj.view.flxClose.setFocus(true);
            CommonUtilities.setCheckboxState(CommonUtilities.isChecked(scopeObj.view.StopCheckPaymentSeriesMultiple.imgTCContentsCheckbox), scopeObj.view.imgTCContentsCheckbox);
        },

        /**
         * onTandContentSaveClickHandler : Terms and Conditions popup Save button click handler.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        onTandContentSaveClickHandler: function(){
            var scopeObj = this;
            CommonUtilities.setCheckboxState(CommonUtilities.isChecked(scopeObj.view.imgTCContentsCheckbox), scopeObj.view.StopCheckPaymentSeriesMultiple.imgTCContentsCheckbox);
            scopeObj.updateStopChekFormContinueBtnState();
            scopeObj.setTermsAndConditionsPopupState(false);
        },

        /**
         * onTandContentCheckBoxClickHandler : Terms and Conditions popup checkbox click handler.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        onTandContentCheckBoxClickHandler: function(){
            CommonUtilities.toggleCheckBox(this.view.imgTCContentsCheckbox);
        },

        /**
         * onSuccessCreateStopCheckRequest : Method to handle successful stop check request.
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel, success request view model
         * @return {}
         * @throws {} 
         */
        onSuccessCreateStopCheckRequest: function(viewModel) {
            var scopeObj = this;
            scopeObj.showStopChekRequestAcknowledgment({
                referenceNumber : viewModel.referenceNumber
            });
        },

        /**
         * showStopChekRequestAcknowledgment : Method to show Stop check request Acknowledgement form.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        showStopChekRequestAcknowledgment: function(viewModel) {
            var scopeObj = this;
            var confirmDetailsSingleMultiple =scopeObj.view.ConfirmDetailsSingleMultiple;
            scopeObj.setBreadcrumb([
                { 
                    label : kony.i18n.getLocalizedString("i18n.accounts.accountsTitle") ,
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                },
                { 
                    label : kony.i18n.getLocalizedString("i18n.CustomerFeedback.Acknowledgement"),
                    toolTip: kony.i18n.getLocalizedString("i18n.CustomerFeedback.Acknowledgement")
                },
            ]);
            scopeObj.view.lblReferenceNumber2.text = viewModel.referenceNumber;

            scopeObj.view.lblHeader.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.StopCheckPaymentAck");
            confirmDetailsSingleMultiple.setVisibility(true);
            confirmDetailsSingleMultiple.lblConfirmHeader.text = kony.i18n.getLocalizedString("i18n.StopPayments.stopCheckPaymentDetails");
            scopeObj.view.flxSuccessMessageStopCheck.setVisibility(true);
            confirmDetailsSingleMultiple.flxStep2Buttons.setVisibility(false);
            confirmDetailsSingleMultiple.flxAckButtons.setVisibility(true);
            scopeObj.view.ConfirmDetailsSingleMultiple.btnBackToAccoutnDetails.onClick = viewModel.onBacktoAccountDetails;
            scopeObj.view.ConfirmDetailsSingleMultiple.btnViewRequests.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.ViewRequests");
            scopeObj.view.ConfirmDetailsSingleMultiple.btnViewRequests.onClick = viewModel.onMyRequestAction || scopeObj.onViewRequestsBtnClick.bind(scopeObj);
        },

        /**
         * onViewRequestsBtnClick : Stop Payment Acknowledgemtn My request button click hanlder.
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        onViewRequestsBtnClick: function(){
            var scopeObj = this;
            scopeObj.presenter.showMyRequests({
                selectTab : OLBConstants.DISPUTED_CHECKS
            });
        },

        /**
         * onSeriesLastCheckTextChange : Method to handle Series last check number stop editing
         * @member of {frmStopPaymentsController}
         * @param {} 
         * @return {}
         * @throws {} 
         */
        onSeriesLastCheckTextChange: function(){
            var scopeObj = this;
            scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.text = scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.text;
            var noOfChecks = scopeObj.getSeriesCheckNumbersCount();
            if(noOfChecks > 1) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = noOfChecks +" "+ kony.i18n.getLocalizedString("i18n.StopPayments.ChecksSelected");
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxLastCheckNo.setVisibility(false);
                scopeObj.view.StopCheckPaymentSeriesMultiple.tbxCheckNo.setVisibility(true);
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.setVisibility(true);
                scopeObj.view.forceLayout();
            }
        },

        /**
         * onSeriesLastCheck2TextChange : Method to handle Series last check number first / 2nd text box stop editing
         * @member of {frmStopPaymentsController}
         * @param {} 
         * @return {}
         * @throws {} 
         */
        onSeriesLastCheck2TextChange: function(){
            var scopeObj = this;
            var checksCount = scopeObj.getSeriesCheckNumbersCount();
            if (checksCount > 1 && checksCount <= OLBConstants.MAX_CHECKS_COUNT) {
                scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = scopeObj.getSeriesCheckNumbersCount() + " " + kony.i18n.getLocalizedString("i18n.StopPayments.ChecksSelected");
                scopeObj.setValidationErrorMessageState(false);
            }
            else {
                if(scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.isVisible) {
                    scopeObj.view.StopCheckPaymentSeriesMultiple.lblChecksSelected.text = "";
                    scopeObj.setValidationErrorMessageState(true, "i18n.StopPayments.errormessages.InvalidSeriesCheckNumbers");
                }
            }
        },

        /**
         * showMyRequests : Method to show My Request View
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel, view model object
         * @return {}
         * @throws {} 
         */
        showMyRequests: function (viewModel) {
            var scopeObj = this;
            if(viewModel) {
                scopeObj.setMyRequestsUI(viewModel);
            } else {
                CommonUtilities.ErrorHandler.onError("showMyRequests - Invalid View Model : " + viewModel);
            }  
        },

        /**
         * setMyRequestsUI : Method to set flexes visibility for My Requests UI
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel, view model object
         * @return {}
         * @throws {} 
         */
        setMyRequestsUI: function (viewModel) {
            var scopeObj = this;

            scopeObj.setBreadcrumb([
                {
                    label: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle"),
                    toolTip: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
                },
                {
                    label: kony.i18n.getLocalizedString("i18n.StopCheckPayments.MyRequests"),
                    toolTip: kony.i18n.getLocalizedString("i18n.StopCheckPayments.MyRequests")
                },
            ]);

            var selectTab = viewModel.selectTab || OLBConstants.DISPUTED_TRANSACTIONS;

            scopeObj.hideAll();
            scopeObj.view.flxMyRequestsTabs.setVisibility(true);
            scopeObj.view.MyRequestsTabs.flxMyRequestsHeader.setVisibility(true);
            scopeObj.view.MyRequestsTabs.flxMyRequestsHeader.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.MyRequests");
            scopeObj.view.MyRequestsTabs.flxTabs.setVisibility(true);
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.setVisibility(true);
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.DisputedTransactions");
            scopeObj.view.MyRequestsTabs.btnDisputedTrnsactions.onClick = scopeObj.disputedTransactionsTabClickHandler.bind(scopeObj);
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.DisputedChecks"); 
            scopeObj.view.MyRequestsTabs.btnDisputedChecks.onClick = scopeObj.disputedChecksTabClickHandler.bind(scopeObj);
            scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.setVisibility(true);

            if(viewModel && viewModel.addNewStopCheckRequestAction) {
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.text = viewModel.addNewStopCheckRequestAction.displayName;
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.toolTip = CommonUtilities.changedataCase(viewModel.addNewStopCheckRequestAction.displayName);
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.onClick = viewModel.addNewStopCheckRequestAction.action;
            } else {
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.text = kony.i18n.getLocalizedString("i18n.StopCheckPayments.AddNewStopCheckRequest");
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.AddNewStopCheckRequest"));
                scopeObj.view.MyRequestsTabs.btnAddNewStopCheckPayments.onClick = scopeObj.btnAddNewStopCheckPaymentsClickHandler.bind(scopeObj);
            }
            switch(selectTab) {
                case OLBConstants.DISPUTED_TRANSACTIONS: 
                    scopeObj.setDisputedTransactionTabUI();
                break;
                case OLBConstants.DISPUTED_CHECKS: 
                    scopeObj.setDisputedChecksTabUI();
                break;
                default:
                    scopeObj.setDisputedTransactionTabUI();
            }
            scopeObj.AdjustScreen();
        },

        /**
         * showStopCheckRequests : Method to show Stop check requests
         * @member of {frmStopPaymentsController}
         * @param {Array} stopRequests, stop check requests array
         * @return {}
         * @throws {} 
         */
        showStopCheckRequests: function(stopCheckRequestsViewModel){
            var scopeObj = this;
            scopeObj.setDisputedChecksTabUI();
            if (stopCheckRequestsViewModel && stopCheckRequestsViewModel.stopchecksRequests && stopCheckRequestsViewModel.stopchecksRequests.length) {
                scopeObj.bindStopCheckRequestsData(stopCheckRequestsViewModel.stopchecksRequests);
                var sortMap = [{
                    name: 'transactionDate',
                    imageFlx: this.view.MyRequestsTabs.imgSortDateDC,
                    clickContainer: this.view.MyRequestsTabs.flxSortDateDC
                },
                {
                    name: 'amount',
                    imageFlx: this.view.MyRequestsTabs.imgSortAmountDC,
                    clickContainer: this.view.MyRequestsTabs.flxSortAmountDC
                },
                {
                    name: 'statusDesc',
                    imageFlx: this.view.MyRequestsTabs.imgStatusDC,
                    clickContainer: this.view.MyRequestsTabs.flxStatusDC
                }];
                CommonUtilities.Sorting.setSortingHandlers(sortMap, scopeObj.onDistiputedChecksSorting.bind(scopeObj), scopeObj);
                CommonUtilities.Sorting.updateSortFlex(sortMap, stopCheckRequestsViewModel.config);
                scopeObj.view.MyRequestsTabs.flxNoTransactions.setVisibility(false);
            } else {
                scopeObj.showNoTransactionsUI({
                    noTransactionsMessageI18Key: "i18n.StopPayments.NoStopPaymentRequests"
                });
            }
            scopeObj.AdjustScreen();
        },


        /**
         * showDisputeTransactionsRequests : Method to show dispute transaction requests
         * @member of {frmStopPaymentsController}
         * @param {Array} Dispute transactions view model
         * @return {}
         * @throws {} 
         */
        showDisputeTransactionsRequests: function(disputeTransactionsRequestsViewModel){
            var scopeObj = this;
            scopeObj.setDisputedTransactionTabUI();
            if (disputeTransactionsRequestsViewModel && disputeTransactionsRequestsViewModel.stopDisputedRequests && disputeTransactionsRequestsViewModel.stopDisputedRequests.length) {
                scopeObj.bindDisputeTransactionRequestsData(disputeTransactionsRequestsViewModel.stopDisputedRequests);
                var sortMap = [{
                    name: 'disputeDate',
                    imageFlx: this.view.MyRequestsTabs.imgSortDate,
                    clickContainer: this.view.MyRequestsTabs.flxSortDate
                },
                {
                    name: 'amount',
                    imageFlx: this.view.MyRequestsTabs.imgSortAmount,
                    clickContainer: this.view.MyRequestsTabs.flxSortAmount
                },
                {
                    name: 'disputeStatus',
                    imageFlx: this.view.MyRequestsTabs.imgStatus,
                    clickContainer: this.view.MyRequestsTabs.flxStatus
                }];
                CommonUtilities.Sorting.setSortingHandlers(sortMap, scopeObj.onDistiputedTransactionsSorting.bind(scopeObj), scopeObj);
                CommonUtilities.Sorting.updateSortFlex(sortMap, disputeTransactionsRequestsViewModel.config);
                scopeObj.view.MyRequestsTabs.flxNoTransactions.setVisibility(false);
            } else {
                scopeObj.showNoTransactionsUI({
                    noTransactionsMessageI18Key: "i18n.StopPayments.NoStopPaymentRequests"
                });
            }
            scopeObj.AdjustScreen();
        },


        /**
         * onDistiputedTransactionsSorting: Handler for Make Transfer tab Sorting
         * @member {frmStopPaymentsController}
         * @param {object} event - Event Object
         * @param {object} data - Updated Config 
         * @returns {}
         * @throws {}
         */
        onDistiputedTransactionsSorting: function (event, data) {
            this.presenter.showDisputeTransactionRequests(data);
        },
        /**
         * onDistiputedChecksSorting: Handler for Make Transfer tab Sorting
         * @member {frmStopPaymentsController}
         * @param {object} event - Event Object
         * @param {object} data - Updated Config 
         * @returns {}
         * @throws {}
         */
        onDistiputedChecksSorting: function (event, data) {
            this.presenter.showDisputeCheckRequests(data);
        },

         /**
         * bindStopCheckRequestsData : Method to bind Stop check requests data to Segments
         * @member of {frmStopPaymentsController}
         * @param {Array} stopRequests, stop check requests array
         * @return {}
         * @throws {} 
         */
        bindStopCheckRequestsData:  function(stopCheckRequests) {
            var scopeObj = this;
            var widgetDataMap = {
                "flxDisputedChecksRowWrapper": "flxDisputedChecksRowWrapper",
                "flxSegDisputedChecksRowWrapper": "flxSegDisputedChecksRowWrapper",
                "flxIdentifier": "flxIdentifier",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxSegDisputedTransactionRowWrapper": "flxSegDisputedTransactionRowWrapper",
                "flxDropdown": "flxDropdown",
                "flxSegDisputedTransactionRowWrappers": "flxSegDisputedTransactionRowWrappers",
                "flxWrapper": "flxWrapper",
                "flxLeft": "flxLeft",
                "flxRight": "flxRight",
                "flxDate": "flxDate",
                "flxReferenceNo": "flxReferenceNo",
                "flxAmount": "flxAmount",
                "flxStatus": "flxStatus",
                "lblSeparator": "lblSeparator",
                "lblIdentifier": "lblIdentifier",
				"lblSeparator2": "lblSeparator2",
				"imgDropDown" : "imgDropDown",
				"lblDate": "lblDate",
				"lblDescription":"lblDescription",
                "lblReferenceNo":"lblReferenceNo",
                "lblAmount":"lblAmount",
                "lblStatus":"lblStatus",
                "lblSepeartor3":"lblSepeartor3",
                "btnCancelRequests":"btnCancelRequests",
                "btnSendAMessage":"btnSendAMessage",
                "lblFromAccount":"lblFromAccount",
                "lblToAccount":"lblToAccount",
                "lblExpiresOnKey":"lblExpiresOnKey",
                "lblFromAccountData":"lblFromAccountData",
                "lblToAccountData":"lblToAccountData",
                "lblExpiresOnData":"lblExpiresOnData",
                "lblDateOfDescriptionKey":"lblDateOfDescriptionKey",
                "CopylblFrequencyTitle0c4e7bef1ab0c44":"CopylblFrequencyTitle0c4e7bef1ab0c44", 
                "lblDateOfDescriptionValue":"lblDateOfDescriptionValue",
                "lblTransactionTypeValue":"lblTransactionTypeValue"
            };
            var dataMap = stopCheckRequests.map(function(requestObj) {
                return {
                    "template": "flxDisputedChecksUnSelectedWrapper",
                    "lblSeparator": "lblSeparator",
                    "lblIdentifier": "lblIdentifier",
                    "lblSeparator2": "lblSeparator2",
                    "imgDropDown" : OLBConstants.IMAGES.ARRAOW_DOWN,
                    "lblDate": requestObj.transactionDate,
                    "lblDescription": requestObj.payeeName,
                    "lblReferenceNo": requestObj.checkNumber,
                    "lblAmount": requestObj.amount,
                    "lblStatus": requestObj.statusDescription,
                    "lblSepeartor3":"",
                    "lblFromAccount": kony.i18n.getLocalizedString("i18n.StopCheckPayments.FromAccount"),
                    "lblToAccount": kony.i18n.getLocalizedString("i18n.StopCheckPayments.DateOnCheck"),
                    "lblExpiresOnKey": kony.i18n.getLocalizedString("i18n.StopCheckPayments.ExpiresOn"),
                    "lblFromAccountData": requestObj.fromAccount,
                    "lblToAccountData": requestObj.checkDateOfIssue,
                    "lblExpiresOnData": requestObj.requestValidity,
                    "lblDateOfDescriptionKey": kony.i18n.getLocalizedString("i18n.StopPayments.Reason"),
                    "CopylblFrequencyTitle0c4e7bef1ab0c44": kony.i18n.getLocalizedString("i18n.StopPayments.Description"),
                    "lblDateOfDescriptionValue": requestObj.checkReason,
                    "lblTransactionTypeValue": requestObj.transactionsNotes,
                    "btnSendAMessage":  {
                        "text" : kony.i18n.getLocalizedString("i18n.StopCheckPayments.SENDMessage"),
                        "toolTip" : CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.StopCheckPayments.SENDMessage")),
                        "onClick" : requestObj.onSendMessageAction ? requestObj.onSendMessageAction : null,
                        "isVisible" : requestObj.onSendMessageAction ? true : false,
                    },
                    "btnCancelRequests" :{
                        "text" : requestObj.onCancelRequest ? kony.i18n.getLocalizedString("i18n.StopPayments.CANCELREQUEST") : kony.i18n.getLocalizedString("i18n.StopPayments.RENEWREQUEST"),
                        "toolTip" : CommonUtilities.changedataCase(requestObj.onCancelRequest ? kony.i18n.getLocalizedString("i18n.StopPayments.CANCELREQUEST") : kony.i18n.getLocalizedString("i18n.StopPayments.RENEWREQUEST")),
                        "onClick" : requestObj.onCancelRequest ? requestObj.onCancelRequest : requestObj.onReNewRequest,
                        "isVisible" : requestObj.onCancelRequest || requestObj.onReNewRequest ? true : false,
                    }
                };
            });
            scopeObj.view.MyRequestsTabs.segTransactions.widgetDataMap = widgetDataMap;
            scopeObj.view.MyRequestsTabs.segTransactions.setData(dataMap);
            scopeObj.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(true);
            scopeObj.view.MyRequestsTabs.segTransactions.setVisibility(true);
            scopeObj.view.forceLayout();
            scopeObj.AdjustScreen();
        },
        
        /**
         * showNoTransactionsUI : Method to handle Empty requests/transactions.
         * @member of {frmStopPaymentsController}
         * @param {object} viewModel,view model
         * @return {}
         * @throws {} 
         */
        
         
        showNoTransactionsUI: function (viewModel) {
            var scopeObj = this;
            viewModel = viewModel || {};
            scopeObj.view.MyRequestsTabs.flxSort.setVisibility(false);
            scopeObj.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(false);
            scopeObj.view.MyRequestsTabs.segTransactions.setVisibility(false);
            scopeObj.view.MyRequestsTabs.flxNoTransactions.setVisibility(true);
            scopeObj.view.MyRequestsTabs.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString(viewModel.noTransactionsMessageI18Key || "i18n.StopPayments.NoStopPaymentRequests");
        },

        /**
         * btnAddNewStopCheckPaymentsClickHandler : My Requests Add New Stop Check Payments button click handler
         * @member of {frmStopPaymentsController}
         * @param {} 
         * @return {}
         * @throws {} 
         */
        btnAddNewStopCheckPaymentsClickHandler: function(){
            this.presenter.showStopChecksForm();
        },

         /**
         * btnViewDisputedTransactionsClickHandler : View Disputed Transactions button Click Handler
         * @member of {frmStopPaymentsController}
         * @param {} 
         * @return {}
         * @throws {} 
         */
        btnViewDisputedTransactionsClickHandler: function(){
            this.presenter.showMyRequests({
                selectTab: OLBConstants.DISPUTED_TRANSACTIONS
            });
        },


         /**
         * btnViewDisputedChecks : View Disputed Checks button click handler
         * @member of {frmStopPaymentsController}
         * @param {} 
         * @return {}
         * @throws {} 
         */
        btnViewDisputedChecks: function(){
            var scopeObj = this;
            scopeObj.presenter.showMyRequests({
                selectTab: OLBConstants.DISPUTED_CHECKS
            });
        },
        
        /**
         * disputedTransactionsTabClickHandler : My Requests Disputed Transactions Tab Click Handler
         * @member of {frmStopPaymentsController}
         * @param {} 
         * @return {}
         * @throws {} 
         */
        disputedTransactionsTabClickHandler: function(){
            var scopeObj = this;
            scopeObj.setDisputedTransactionTabUI();
            scopeObj.presenter.showDisputeTransactionRequests({
                resetSorting: true
            });
        },

        /**
         * disputedChecksTabClickHandler : My Requests Disputed Checks Tab Click Handler
         * @member of {frmStopPaymentsController}
         * @param {} 
         * @return {}
         * @throws {} 
         */
        disputedChecksTabClickHandler: function(){
            var scopeObj = this;
            scopeObj.setDisputedChecksTabUI();
            scopeObj.presenter.showDisputeCheckRequests({
                resetSorting: true
            });
        },

        /**
         * setServerError : Method to handle Server error 
         * @member of {frmStopPaymentsController}
         * @param {boolean} isError, error flag to show/hide error flex.
         * @return {}
         * @throws {} 
         */
        setServerError: function (isError) {
            var scopeObj = this;
            scopeObj.view.flxDowntimeWarning.setVisibility(isError);
            if (isError) {
                scopeObj.view.lblDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
            }
            scopeObj.view.forceLayout();
        },

        /** 
         * showCancelStopRequestUI : show cancel stop request UI
         * @member  {frmStopPaymentsController}
         * @param {object} viewModel , view model object.
         * @returns {}
         * @throws {}
         */
        showCancelStopRequestUI: function(viewModel){
            var scopeObj = this;
            var cancelStopCheckPopup = scopeObj.view.CancelStopCheckPayments;
            var height = scopeObj.getPageHeight();
            cancelStopCheckPopup.lblHeading.text = viewModel.headerText;
            cancelStopCheckPopup.lblPopupMessage.text = viewModel.message;
            if(viewModel.showStopPaymentServiceFeesAndValidity) {
                cancelStopCheckPopup.lblThisServicesIsChargeble.text = viewModel.serviceChargableText || "";
                cancelStopCheckPopup.flxPleaseNoteTheFollowingPoints.setVisibility(true);
            } else {
                cancelStopCheckPopup.flxPleaseNoteTheFollowingPoints.setVisibility(false);
            }

            scopeObj.view.flxLogoutStopCheckPayment.height = height + "dp";
            scopeObj.view.flxLogoutStopCheckPayment.left = "0%";
            scopeObj.view.flxLogoutStopCheckPayment.setVisibility(true);
            scopeObj.view.flxLogoutStopCheckPayment.setFocus(true);


            cancelStopCheckPopup.btnYes.onClick = function () {
                scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
                viewModel.confirmCancelAction();
            };
        },

        /** setHeaderActions : Binds Action for Logout and other header functionalities
         * @member  {frmStopPaymentsController}
         * @param {}
         * @returns {}
         * @throws {}
         */
        setHeaderActions: function () {
            var scopeObj = this;
            scopeObj.view.customheader.headermenu.btnLogout.onClick = function () {
                scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
                scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
                scopeObj.view.flxLogout.left = "0%";
                scopeObj.view.flxLogout.setVisibility(true);
                
                var mainheight = scopeObj.getPageHeight();
                scopeObj.view.flxLogout.height = mainheight + "dp";
                
            };
            scopeObj.view.CustomPopup.btnYes.onClick = function () {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.presentationController.doLogout({
                    "action": "Logout"
                });
                scopeObj.view.flxLogout.left = "-100%";
            };
            scopeObj.view.CustomPopup.btnNo.onClick = function () {
                scopeObj.view.flxLogout.left = "-100%";
            };
            scopeObj.view.CustomPopup.flxCross.onClick = function () {
                scopeObj.view.flxLogout.left = "-100%";
            };
        },

        /**
         * updateHamburgerMenu : Methiod to handle Hamburger Menu
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        updateHamburgerMenu: function (sideMenuModel) {
            this.view.customheader.initHamburger(sideMenuModel);
        },

        /**
         * updateTopBar : Method to handle top bar menu
         * @member of {frmStopPaymentsController}
         * @param {}
         * @return {}
         * @throws {} 
         */
        updateTopBar: function (topBarModel) {
            this.view.customheader.initTopBar(topBarModel);
        }
    };

});