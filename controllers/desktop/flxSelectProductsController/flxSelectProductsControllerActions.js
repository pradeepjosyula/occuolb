define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_fd12cc78441f492bb30761e9e51bb3be: function AS_FlexContainer_fd12cc78441f492bb30761e9e51bb3be(eventobject, context) {
        var self = this;
        this.toggleCheckBox();
    },
    /** onClick defined for btnKnowMore **/
    AS_Button_g413108cbdaf4439928dcef97cd9d84f: function AS_Button_g413108cbdaf4439928dcef97cd9d84f(eventobject, context) {
        var self = this;
        this.KnowMoreAction();
    }
});