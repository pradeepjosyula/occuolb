define({ 
	
  showSelectedRow: function(){
    var previousIndex
    var index = kony.application.getCurrentForm().tableView.segP2P.selectedIndex;
    var rowIndex = index[1];
    var data = kony.application.getCurrentForm().tableView.segP2P.data;
    //data[rowIndex].template = "flxManageRecipientSelected";
      for(i=0;i<data.length;i++)
         {
           if(i==rowIndex)
             {
               data[i].imgDropdown = "arrow_up.png";
               data[i].template = "flxManageRecipientSelected";
             }
           else
             {
               data[i].imgDropdown = "arrow_down.png";
               data[i].template = "flxManageRecipient";
             }
         }  
  //  kony.application.getCurrentForm().tableView.segP2P.setDataAt(data[rowIndex], rowIndex);
     kony.application.getCurrentForm().tableView.segP2P.setData(data);
      this.AdjustScreen(150);
  },
  //UI Code
  AdjustScreen: function(data) {
    var currForm = kony.application.getCurrentForm();
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = currForm.customheader.frame.height + currForm.flxContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - currForm.flxFooter.frame.height;
        if (diff > 0) 
            currForm.flxFooter.top = mainheight + diff + data + "dp";
        else
            currForm.flxFooter.top = mainheight + data + "dp";        
     } else {
        currForm.flxFooter.top = mainheight + data + "dp";
     }
    currForm.forceLayout();
  },  


 });

