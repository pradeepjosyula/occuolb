define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_acd6ec49e49343b1ad50b0527e34607c: function AS_FlexContainer_acd6ec49e49343b1ad50b0527e34607c(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for btnCancelRequests **/
    AS_Button_c675ea729ef1437bac51c94544f2f8f7: function AS_Button_c675ea729ef1437bac51c94544f2f8f7(eventobject, context) {
        var self = this;
        var currForm = kony.application.getCurrentForm();
        currForm.flxLogoutStopCheckPayment.setVisibility(true);
        currForm.forceLayout();
    },
    /** onClick defined for btnSendAMessage **/
    AS_Button_e72108dde6484a1ba64b45b45062952f: function AS_Button_e72108dde6484a1ba64b45b45062952f(eventobject, context) {
        var self = this;
        var nav = new kony.mvc.Navigation("frmNotificationsAndMessages");
        nav.navigate();
    }
});