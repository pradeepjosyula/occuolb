define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_fb4dd8ac745e46b98cd0692ca6cd7a01: function AS_FlexContainer_fb4dd8ac745e46b98cd0692ca6cd7a01(eventobject, context) {
        var self = this;
        this.showUnselectedRow();
    },
    /** onClick defined for btnAction **/
    AS_Button_ge0638ee56e54583bc9220d9af4d92e6: function AS_Button_ge0638ee56e54583bc9220d9af4d92e6(eventobject, context) {
        var self = this;
        this.executeOnParent("viewTransactionReport");
    },
    /** onClick defined for btnEdit **/
    AS_Button_b569dbfb367946b9beda2396fd113363: function AS_Button_b569dbfb367946b9beda2396fd113363(eventobject, context) {
        var self = this;
        this.executeOnParent("repeatTransaction");
    },
    /** onClick defined for btnRepeat **/
    AS_Button_j9c2da4f2c9e437c82f17f82638e9aad: function AS_Button_j9c2da4f2c9e437c82f17f82638e9aad(eventobject, context) {
        var self = this;
        this.executeOnParent("repeatTransaction");
    }
});