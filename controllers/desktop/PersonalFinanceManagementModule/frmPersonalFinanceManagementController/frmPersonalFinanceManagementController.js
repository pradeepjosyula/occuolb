define(['CommonUtilities'], function (CommonUtilities) {

    return {
        shouldUpdateUI: function (viewModel) {
            return viewModel !== undefined && viewModel !== null;
        },
        willUpdateUI: function (viewModel) {
            if (viewModel.showProgressBar) {
                CommonUtilities.showProgressBar(this.view);
            } else if (viewModel.hideProgressBar) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewModel.onServerDownError) {
                this.showServerDownMessage(true);
            }else{
               this.showServerDownMessage(false);
            }
            if (viewModel.sideMenu) {
                this.updateHamburgerMenu(viewModel.sideMenu);
            }
            if (viewModel.topBar) {
                this.updateTopBar(viewModel.topBar);
            }
            if (viewModel.getMonths) {
                CommonUtilities.hideProgressBar(this.view);
                this.addMonthsToDonutDropDown(viewModel.getMonths);
            }
            if (viewModel.getYears) {
                this.addYearsToBarChartDropDown(viewModel.getYears);
            }
            if (viewModel.showMonthlyDonutChart) {
                this.addDonut(viewModel.showBothDonutCharts,viewModel.monthlySpending, viewModel.totalCashSpent);
            }
			if(viewModel.hideflex)
			{
				this.naviagetToPFMLandingForm();
			}
            if (viewModel.showPFMAccounts) {
                this.setPFMAccounts(viewModel.pfmAccounts);
            }
            if (viewModel.showYearlyBarChart) {
                CommonUtilities.hideProgressBar(this.view);
                this.addBarChart(viewModel.yearlySpending);
            }
            if (viewModel.showYearlyBudgetChart) {
                CommonUtilities.hideProgressBar(this.view);
                this.addStackedBarChart(viewModel.yearlyBudgetData);
            }
            if (viewModel.showMonthlyCategorizedTransactions) {
                CommonUtilities.hideProgressBar(this.view);
                this.setMonthlyCategorizedTransactions(viewModel.monthlyCategorizedTransactions);
            }
            if (viewModel.unCategorizedIdTransactionList) {
                this.setDataForUnCategorizedTransactions(viewModel.unCategorizedIdTransactionList.data, viewModel.unCategorizedIdTransactionList.config);
            }
            if (viewModel.bulkUpdateTransactionList) {
                this.setBulkUpdateTransactionData(viewModel.bulkUpdateTransactionList);
            }
            if (viewModel.categoryList) {
                this.setCategoryList(viewModel.categoryList);
            }
        },
        /**
         * showServerDownMessage - function to handle serverdown message
         * @member of {frmPayAPersonController}  
         * @param {boolean} toShow- true : display message, false: hide message
         * @returns {} 
         * @throws {}
         */
        showServerDownMessage: function (toShow) {
            if (toShow) {
                CommonUtilities.hideProgressBar(this.view);
                this.view.flxDowntimeWarning.setVisibility(true);
                this.view.rtxDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
                this.view.forceLayout();
            } else if (this.view.flxDowntimeWarning.isVisible === true) {
                this.view.flxDowntimeWarning.setVisibility(false);
            }
        },

        /**
   * setP2PBreadcrumbData - sets the breadcrumb value across the pay a person .
   * @member of {frmPayAPersonController}  
   * @param {array} an array of JSONs of breadcrums with text and callback as fields. (can send an array of length 3 at max).
   * @returns {} 
   * @throws {}
   */
        setPFMBreadcrumbData: function (breadCrumbArray) {
            var self = this;
            self.view.breadcrumb.setBreadcrumbData(breadCrumbArray);
        },

        /**
       * naviagetToPFMLandingForm - used to navigate the pfmLanding Form.
       * @member of {frmPayAPersonController}  
       * @param {array} an array of JSONs of breadcrums with text and callback as fields. (can send an array of length 3 at max).
       * @returns {} 
       * @throws {}
       */

        naviagetToPFMLandingForm: function () {
            var self = this;
            self.setPFMBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.pfm.accounts"),
                skin: "sknBtnLato3343A813PxBg0",
                toolTip : kony.i18n.getLocalizedString("i18n.pfm.accounts"),
                callback: function () {
                    CommonUtilities.showProgressBar(self.view);
                    self.presenter.naviageteToAccountLandingPage();
                }
            }
                , {
                text: kony.i18n.getLocalizedString("i18n.pfm.personalfinancemanagement"),
                skin: "skna0a0a0op100breadcrumb2",
                toolTip : kony.i18n.getLocalizedString("i18n.pfm.personalfinancemanagement")
            }]);
            self.view.flxPFMContainers.setVisibility(true);
            self.view.flexCategorizedMonthlySpending.setVisibility(false);
            this.view.flxUncategorizedTransactions.setVisibility(false);
			self.view.forceLayout();
			self.AdjustScreen();
        },

        /**
         * Initialize HamburgerMenu
         * @param: sideMenuModel
         */
        updateHamburgerMenu: function (sideMenuModel) {
            this.view.customheader.initHamburger(sideMenuModel);
        },
        /**
         * Initialize TopBar
         * @param: topBarModel
         */
        updateTopBar: function (topBarModel) {
            this.view.customheader.initTopBar(topBarModel);
        },

        /**
        * setMonthlyCategorizedTransactions : used to set the monthly transactions.
        * @member of {frmPersonalFinanceManagementController}
        * @param {transactions} /get All transactions data
        * @returns {}
        * @throws {} 
        */
        setMonthlyCategorizedTransactions: function (transactions) {
			var self =this;
            this.view.flexCategorizedMonthlySpending.setVisibility(true);
            this.view.flxPFMContainers.setVisibility(false);
            if (transactions) {
                var dataMap = {
                    "lblIdentifier": "lblIdentifier",
                    "lblSeparator": "lblSeparator",
                    "lblTotal": "lblTotal",
                    "lblTotalValue": "lblTotalValue",
                    "lblTransactionHeader": "lblTransactionHeader",
                    "imgCategoryDropdown": "imgCategoryDropdown",
                    "imgDropdown": "imgDropdown",
                    "lblAmount": "lblAmount",
                    "lblCategory": "lblCategory",
                    "lblDate": "lblDate",
                    "lblDescription": "lblDescription",
                    "lblSeparator2": "lblSeparator2",
                    "lblFrom": "lblFrom",
                    "lblFromValue": "lblFromValue",
                    "lblNoteTitle": "lblNoteTitle",
                    "lblNoteValue": "lblNoteValue",
                    "lblToValuetitle": "lblToValuetitle",
                    "lblToValue": "lblToValue",
					"flxActions" : "flxActions",
					"flxIdentifier" : "flxIdentifier",
                    "lblIdentifier"  :"lblIdentifier"
                }
                var categoryTransactionsData = [];
                var categoryTransactions = transactions.map(function (dataItem) {
                    var categoryMainTransactions = [];
                    var category = {
                        "lblIdentifier": {
                            "skin": self.getCategorySkin(dataItem.header.categoryName).skin
                        },
                        "lblSeparator": "Label",
                        "lblTotal": kony.i18n.getLocalizedString("i18n.CheckImages.Total"),
                        "lblTotalValue": dataItem.header.totalAmount,
                        "lblTransactionHeader": dataItem.header.categoryName
                    };
                    categoryMainTransactions.push(category);
                    var categoryTransactionsArray = dataItem.transactionobj.map(function (trans) {
                        return {
                            "imgCategoryDropdown": "arrow_down.png",
							"flxActions" : "flxActions",
                            "imgDropdown": "arrow_down.png",
                            "lblAmount": trans.displayAmount,
                            "lblCategory": trans.categoryName,
                            "lblDate": trans.transactionDate,
                            "lblDescription": trans.transactionDescription,
                            "lblSeparator": "a",
                            "lblSeparator2": " a",
                            "lblFrom": kony.i18n.getLocalizedString("i18n.PayAPerson.From"),
                            "lblFromValue": trans.fromAccountName,
                            "lblNoteTitle": kony.i18n.getLocalizedString("i18n.transfers.note"),
                            "lblNoteValue": trans.transactionNotes,
                            "lblToValuetitle": kony.i18n.getLocalizedString("i18n.StopCheckPayments.To"),
                            "lblToValue": trans.toAccountName ? trans.toAccountName : "",
							"flxIdentifier" : "flxIdentifier",
							"lblIdentifier"  :"lblIdentifier"
                        }
                    });
                    categoryMainTransactions.push(categoryTransactionsArray);
                    categoryTransactionsData.push(categoryMainTransactions);
                });
                this.view.CategorizedMonthlySpending.segTransactions.setVisibility(true);
                this.view.CategorizedMonthlySpending.flxNoTransactions.setVisibility(false);
                this.view.CategorizedMonthlySpending.segTransactions.widgetDataMap = dataMap;
                this.view.CategorizedMonthlySpending.segTransactions.setData(categoryTransactionsData);
				self.AdjustScreen();
            } else {
                this.view.CategorizedMonthlySpending.segTransactions.setVisibility(false);
                this.view.CategorizedMonthlySpending.flxNoTransactions.setVisibility(true);
				self.AdjustScreen();
            }
        },

        /**
         * setPFMAccounts : used to update the accounts segment.
         * @member of {frmPersonalFinanceManagementController}
         * @param {accounts} /get All accounts data
         * @returns {}
         * @throws {} 
         */
        setPFMAccounts: function(accounts) {
            var self = this;
            if (accounts) {
                if (accounts.length !== 0) {
                    var dataMap = {
                        "lblIdentifier": "lblIdentifier",
                        "lblAccountName": "lblAccountName",
                        "lblAccountNumber": "lblAccountNumber",
                        "lblAvailableBalanceTitle": "lblAvailableBalanceTitle",
                        "lblAvailableBalanceValue": "lblAvailableBalanceValue",
                        "lblCreditKey": "lblCreditKey",
                        "lblCreditValue": "lblCreditValue",
                        "lblDebitKey": "lblDebitKey",
                        "lblDebitValue": "lblDebitValue",
                        "lblSeparator": "lblSeparator",
                        "lblSepeartor2": "lblSepeartor2",
                        "lblSeperator3": "lblSeperator3"
                    };
                    accounts = accounts.map(function(dataItem) {
                        var balanceKey = self.getAccountSkinAndTitle(dataItem).balanceKey;
                        return {
                            "lblIdentifier": {
                                "skin": self.getAccountSkinAndTitle(dataItem).skin,
                                "text":""
                            },
                            "lblAccountName": dataItem.accountName,
                            "lblAccountNumber": dataItem.accountID,
                            "lblAvailableBalanceTitle": {
                                "text": self.getAccountSkinAndTitle(dataItem).balanceTitle
                            },
                            "lblAvailableBalanceValue": dataItem[balanceKey],
                            "lblCreditKey": kony.i18n.getLocalizedString("i18n.pfm.totalCredit"),
                            "lblCreditValue": dataItem.totalCredits,
                            "lblDebitKey": kony.i18n.getLocalizedString("i18n.pfm.totalDebit"),
                            "lblDebitValue": dataItem.totalDebits,
                            "lblSeparator": "a",
                            "lblSepeartor2": "a",
                            "lblSeperator3": "a"
                        }
                    });
                    this.view.segAccounts.widgetDataMap = dataMap;
                    this.view.flxMySpendingWrapperAccounts.setVisibility(true);
                    this.view.flxAccounts.setVisibility(false);
                    this.view.segAccounts.setVisibility(true);
                    this.view.segAccounts.setData(accounts);
                } else {
                    this.view.flxMySpendingWrapperAccounts.setVisibility(false);
                    this.view.flxAccounts.flxNoAccounts.lblNoAccounts.text = kony.i18n.getLocalizedString("i18n.pfm.nopfmAccounts");
                    this.view.flxAccounts.setVisibility(true);
                }
            }
        },
		/**
         * getCategorySkin : Method for used to get category skin.
         * @member of {frmPersonalFinanceManagementController}
         * @param {categoryName} 
         * @returns {}
         * @throws {} 
         */
		 getCategorySkin: function(categoryName) {
			  var categoryTypeConfig = {
				  'Home' : {
					  skin: "sknFEDB64PFM"
				  },
				  'Auto & Transport' : {
					  skin: "skn3645A7PFM"
				  },
				  'Financial' : {
					  skin: "skn6753ECPFM"
				  },
				  'Food & Dining' : {
					  skin: "sknD6B9EAPFM"
				  },
				  'Bills & Utilities' : {
					  skin: "sknE87C5EPFM"
				  },
				  'Travel' : {
					  skin: "skn8ED174PFM"
				  },
				  'Health & Fitness' : {
					  skin: "skn04B6DFPFM"
				  },
				  'Education' : {
					  skin: "sknE8A75EPFM"
				  },
				  'Other' : {
					  skin: "sknB160DCPFM"
				  },
				  'Default' : {
					  skin: "skn23A8B1PFM"
				  }
			  }
			if (categoryTypeConfig[categoryName]) {
                return categoryTypeConfig[categoryName];
            } else {
                return categoryTypeConfig.Default;
            }
			  
		 },
        /**
         * getAccountSkinAndTitle : Method for used to get Account skin and balance title.
         * @member of {frmPersonalFinanceManagementController}
         * @param {monthsWithYear} /Month Array
         * @returns {} get all list of Years.
         * @throws {} 
         */
        getAccountSkinAndTitle: function(account) {
            var accountTypeConfig = {
                'Savings': {
                    skin: "sknFlx26D0CE",
                    balanceKey: 'availableBalance',
                    balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
                },
                'Checking': {
                    skin: "sknFlx9060b7",
                    balanceKey: 'availableBalance',
                    balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
                },
                'CreditCard': {
                    skin: "sknFlxF4BA22",
                    balanceKey: 'currentBalance',
                    balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
                },
                'Deposit': {
                    skin: "sknFlx4a90e2Border1pxRound",
                    balanceKey: 'currentBalance',
                    balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
                },
                'Mortgage': {
                    skin: 'sknFlx4a90e2Border1pxRound',
                    balanceKey: 'currentBalance',
                    balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
                },
                'Loan': {
                    skin: 'sknFlx8D6429',
                    balanceKey: 'outstandingBalance',
                    balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.outstandingBalance'),
                },
                'Default': {
                    skin: 'sknFlx26D0CE',
                    balanceKey: 'availableBalance',
                    balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
                }
            };
            if (accountTypeConfig[account.accountType]) {
                return accountTypeConfig[account.accountType];
            } else {
                return accountTypeConfig.Default;
            }
        },
        /**
         * addMonthsToDonutDropDown : Method for used to add All Months based on year selection.
         * @member of {frmPersonalFinanceManagementController}
         * @param {monthsWithYear} /Month Array
         * @returns {} get all list of Years.
         * @throws {} 
         */
        addMonthsToDonutDropDown: function (monthsWithYear) {
            var year = monthsWithYear.year;

            function convertMonthToKeyValue(month) {
                var monthWithYear = month;
                return [monthsWithYear.pfmMonths.indexOf(month) + 1, month + "    " + year];
            }
            var monthsObj = monthsWithYear.pfmMonths;
            monthsWithYear.pfmMonths = Object.keys(monthsObj).map(function(e) {
			return monthsObj[e]
			});
            this.view.lstSelectMonth.masterData = monthsWithYear.pfmMonths.map(convertMonthToKeyValue).reverse();
            this.view.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lstSelectMonth.masterData = monthsWithYear.pfmMonths.map(convertMonthToKeyValue).reverse();
            this.view.lstSelectMonth.onSelection = this.onMonthDropSelect.bind(this);
            this.view.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lstSelectMonth.onSelection = this.onCategoryMonthDropSelect.bind(this);
        },
        /**
         * addYearsToBarChartDropDown : Method for used to add All Years.
         * @member of {frmPersonalFinanceManagementController}
         * @param {years} /Year Array
         * @returns {} get all list of Years.
         * @throws {} 
         */
        addYearsToBarChartDropDown: function (years) {
            function convertYearToKeyValue(year) {
                return [year, year];
            }
            this.view.lstSelectPeriod.masterData = years.map(convertYearToKeyValue);
            this.view.lstSelectPeriod.onSelection = this.onYearDropSelect.bind(this);
        },

        onCategoryMonthDropSelect: function () {
            this.presenter.getMonthlyCategorizedTransactions(this.view.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lstSelectMonth.selectedKey);
            this.presenter.getMonthlySpending(false,this.view.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);
        },
        /**
         * onMonthDropSelect : Method for used to update the monthly chart.
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}.
         * @throws {} 
         */
        onMonthDropSelect: function () {
            this.view.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lstSelectMonth.selectedKey = this.view.lstSelectMonth.selectedKey;
            this.presenter.getMonthlySpending(true,this.view.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);
            this.presenter.getPFMBudgetChart(this.view.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);
            this.presenter.getPFMRelatedAccounts(this.view.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);            
        },
        /**
         * onYearDropSelect : Method for used to update the yearly chart and monthly chart and
         * budget chart.
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}.
         * @throws {} 
         */
        onYearDropSelect: function () {
            this.presenter.getYearlySpending(this.view.lstSelectPeriod.selectedKey);
            this.presenter.selectYear(this.view.lstSelectPeriod.selectedKey);
            this.presenter.getMonthlySpending(true,this.view.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);
            this.presenter.getPFMBudgetChart(this.view.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);
            this.presenter.getPFMRelatedAccounts(this.view.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);
        },
        /**
         * initActions : Used to initlize the chart widgets.
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}.
         * @throws {} 
         */
        initActions: function () {
            this.initlizeDonutChart();
            this.initlizeDonutChartForCategory();
            this.initlizeBarChart();
            this.initlizeStackedBarChart();
            this.view.flxMySpendingWrapper.onclick = this.navigateToMonthlyTransactionsPage.bind(this,null);
            this.view.flxUncategorizedTransactions.setVisibility(false);
        },
        /**
         * presShow : Used to Preshow.
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}.
         * @throws {} 
         */
        preShow: function () {
            var scopeObj = this;
			this.view.customheader.forceCloseHamburger();
            this.view.flexCategorizedMonthlySpending.setVisibility(false);
            this.view.flxPFMContainers.setVisibility(true);
            this.view.flxUncategorizedTransactions.setVisibility(false);
			this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "PFM");
            this.view.CustomPopup.flxCross.onClick = function () {
                scopeObj.view.flxLogout.left = "-100%";
            };
            this.view.CustomPopup.btnYes.onClick = function () {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                var context = {
                    "action": "Logout"
                };
                authModule.presentationController.doLogout(context);
                scopeObj.view.flxLogout.left = "-100%";

            };
            this.view.CustomPopup.btnNo.onClick = function() {
                scopeObj.view.flxLogout.left = "-100%";
            };
            this.view.customheader.headermenu.btnLogout.onClick = function () {
                scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
                scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
                //var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainWrapper.frame.height;
                //scopeObj.view.flxLogout.height = height + "dp";
                scopeObj.view.flxLogout.left = "0%";
            };
        },
        /**
         * poshShow : Used to show Postshow.
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}.
         * @throws {} 
         */
        postShow: function () {
             this.view.CommonHeader.btnRequest.onClick = this.showUnCategorizedTransactions;
             this.view.CommonHeader.btnRequest.text = kony.i18n.getLocalizedString("i18n.PFM.ViewUncategorizedTransactions");
             this.view.CommonHeader.btnRequest.toolTip = kony.i18n.getLocalizedString("i18n.PFM.ViewUncategorizedTransactions");
            this.view.TransactionsUnCategorized.btnCancel.onClick = this.showUnCategorizedTransactions;
            var scopeObj = this;
            this.view.flxChart.onClick = function () {
                scopeObj.view.flexCategorizedMonthlySpending.setVisibility(true);
                scopeObj.setBreadCrumbData();
                scopeObj.view.CategorizedMonthlySpending.CommonHeader.btnRequest.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.flxDonutChart.onClick = function () {
                scopeObj.view.flexCategorizedMonthlySpending.setVisibility(true);
                scopeObj.setBreadCrumbData();
                scopeObj.view.CategorizedMonthlySpending.CommonHeader.btnRequest.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.TransactionsUnCategorized.CommonHeader.btnRequest.onClick = function () {
                scopeObj.view.flxUncategorizedTransactions.setVisibility(true);
                scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(false);
                scopeObj.view.TransactionsUnCategorized.flxSort.setVisibility(false);
                scopeObj.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(true);
                scopeObj.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
                scopeObj.view.TransactionsUnCategorized.flxPagination.setVisibility(false);
                scopeObj.view.TransactionsUnCategorized.flxSeparatorSort.setVisibility(true);
                scopeObj.view.TransactionsUnCategorized.flxButtons.setVisibility(true);
                scopeObj.presenter.showBulkUpdateTransaction();
                scopeObj.view.forceLayout();
            };
			if(CommonUtilities.isCSRMode()){
               this.view.TransactionsUnCategorized.btnDownload.onClick = CommonUtilities.disableButtonActionForCSRMode();
               this.view.TransactionsUnCategorized.btnDownload.skin = CommonUtilities.disableButtonSkinForCSRMode();
               this.view.TransactionsUnCategorized.btnDownload.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
               this.view.TransactionsUnCategorized.btnDownload.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
            }else{
            this.view.TransactionsUnCategorized.btnDownload.onClick = function () {
                scopeObj.view.flxPFMAssignCategory.setVisibility(true);
                scopeObj.view.forceLayout();
            };
			}
            this.view.AssignCategory.flxCross.onClick = function () {
                scopeObj.view.flxPFMAssignCategory.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.AssignCategory.btnCancel.onClick = function () {
                scopeObj.view.flxPFMAssignCategory.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.AssignCategory.btnDownload.onClick = function () {
                scopeObj.presenter.bulkUpdateCategory(scopeObj.getSelectedTransactions());
                scopeObj.view.flxPFMAssignCategory.setVisibility(false);                
                if(scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src === "checked_box.png"){
                    scopeObj.naviagetToPFMLandingForm();
                }else{
                    scopeObj.removedSelectedRows();
                }
            };
            this.view.TransactionsUnCategorized.flxCheckbox.onClick = function () {
                if (scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src === "checked_box.png") {
                    scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "unchecked_box.png";
                    var data = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.data;
                    for (var i = 0; i < data.length; i++) {
                        data[i].imgCheckBox = "unchecked_box.png";
                    }
                    scopeObj.disableButton(scopeObj.view.TransactionsUnCategorized.btnDownload);
                    kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.setData(data);
                } else {
                    var data1 = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.data;
                    scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "checked_box.png";
                    for (i = 0; i < data1.length; i++) {
                        data1[i].imgCheckBox = "checked_box.png";
                    }
					if(!CommonUtilities.isCSRMode()){
                    scopeObj.enableButton(scopeObj.view.TransactionsUnCategorized.btnDownload);
					}
                    kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.setData(data1);
                }
            };
			
			this.view.breadcrumb.btnBreadcrumb1.text = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
            this.view.breadcrumb.lblBreadcrumb2.setVisibility(false);
            this.view.breadcrumb.btnBreadcrumb2.setVisibility(true);
            this.view.breadcrumb.btnBreadcrumb2.text = kony.i18n.getLocalizedString("i18n.accounts.PersonalFinanceManagement");
			this.view.breadcrumb.btnBreadcrumb2.skin = "skna0a0a0op100breadcrumb2";
            this.view.breadcrumb.btnBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.accounts.PersonalFinanceManagement");
            this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
			this.view.breadcrumb.btnBreadcrumb2.onClick = null;
			this.view.customheader.topmenu.flxMenu.skin = "slFbox";
			this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
			this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
			this.view.customheader.topmenu.flxSeperator3.setVisibility(false);	
			this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
			
        },
		
        /**
         * setCategoryList : Method for used update list box with categories.
         * @member of {frmPersonalFinanceManagementController}
         * @param {categoryList} /category Array
         * @returns {}
         * @throws {} 
         */
        setCategoryList: function (categoryList) {
            var mapCategoryList = function (data) {
                if (data.categoryName !== "Uncategorized") {
                    return [data.categoryId, data.categoryName];
                }
            };
            categoryList = categoryList.filter(function (data) {
                if (data.categoryName !== "Uncategorized") {
                    return data;
                }
            });
            this.view.AssignCategory.lbxSelectFormat.masterData = categoryList.map(mapCategoryList);
        },
        /**
         * showUnCategorizedTransactions : Method for show UnCategorized Transactions widget
         * @member of {frmPersonalFinanceManagementController}
         * @param {} 
         * @returns {}
         * @throws {} 
         */
        showUnCategorizedTransactions: function () {
            CommonUtilities.showProgressBar(this.view);
            this.view.flexCategorizedMonthlySpending.setVisibility(false);
            this.view.flxUncategorizedTransactions.setVisibility(true);
            this.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(true);
            this.view.TransactionsUnCategorized.CommonHeader.btnRequest.text = kony.i18n.getLocalizedString("i18n.PFM.BulkUpdate");
            this.view.TransactionsUnCategorized.CommonHeader.btnRequest.toolTip = kony.i18n.getLocalizedString("i18n.PFM.BulkUpdate");
            this.view.TransactionsUnCategorized.flxSort.setVisibility(true);
            this.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(false);
            this.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
            this.view.TransactionsUnCategorized.flxPagination.setVisibility(false);
            this.view.TransactionsUnCategorized.flxSeparatorSort.setVisibility(false);
            this.view.TransactionsUnCategorized.flxButtons.setVisibility(false);
            this.view.flxPFMContainers.setVisibility(false);
            this.setBreadCrumbData();
            this.view.forceLayout();
            this.presenter.fetchUnCategorizedTransations(this.view.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);
        },
        /**
         * createViewModelForUnCategorizedTransaction : method for create datamap
         * @member of {frmPersonalFinanceManagementController}
         * @param {data} /array of transactions
         * @returns {}
         * @throws {} 
         */
        createViewModelForUnCategorizedTransaction: function (data) {
            return {
                lblAmount: data.transactionAmount,
                lblDate: data.transactionDate,
                lblDescription: data.transactionDescription,
                lblFromAccount: kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                lblFromAccountData: CommonUtilities.mergeAccountNameNumber(data.fromAccountName, data.fromAccountNumber),
                lblTo: data.toAccountName,
                lblToAccount: kony.i18n.getLocalizedString("i18n.PayAPerson.ToAccount"),
                lblToAccountData: data.toAccountName,
                template: "flxPFMUnCategorizedTransactions",
                "lblIdentifier": {
                    "skin": "sknflx4a902"
                },
                "lblCategory": "Select Category",
                "imgCategoryDropdown": "arrow_down.png",
                "imgDropdown": "arrow_down.png",
                "lblSeparator": "lblSeparator",
                "lblSeparator2": "lblSeparator2"
            }
        },
        /**
         * setBreadCrumbData : method for handle setBreadCrumbData for uncategorized transactions
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}
         * @throws {} 
         */
        setBreadCrumbData: function (categorizedFlow) {
            this.view.breadcrumb.btnBreadcrumb1.text = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
            this.view.breadcrumb.lblBreadcrumb2.setVisibility(false);
            this.view.breadcrumb.btnBreadcrumb2.setVisibility(true);
            this.view.breadcrumb.btnBreadcrumb2.text = kony.i18n.getLocalizedString("i18n.accounts.PersonalFinanceManagement");
            this.view.breadcrumb.btnBreadcrumb2.tooltip = kony.i18n.getLocalizedString("i18n.accounts.PersonalFinanceManagement");
            this.view.breadcrumb.btnBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.accounts.PersonalFinanceManagement");
            this.view.breadcrumb.btnBreadcrumb2.skin = "sknBtnLato3343A813PxBg0";
            this.view.breadcrumb.btnBreadcrumb2.onClick = this.showPFMLanding;
            this.view.breadcrumb.btnBreadcrumb2.onClick = this.naviagetToPFMLandingForm;
            this.view.breadcrumb.imgBreadcrumb2.setVisibility(true);
			if(categorizedFlow)
			{
			this.view.breadcrumb.lblBreadcrumb3.text = kony.i18n.getLocalizedString("i18n.pfm.categorizedMonthlySpending");
            this.view.breadcrumb.lblBreadcrumb3.toolTip = kony.i18n.getLocalizedString("i18n.pfm.categorizedMonthlySpending");
            this.view.breadcrumb.lblBreadcrumb3.setVisibility(true);
			}else{
			this.view.breadcrumb.lblBreadcrumb3.text = kony.i18n.getLocalizedString("i18n.PFM.UncategorizedTransactions");
            this.view.breadcrumb.lblBreadcrumb3.toolTip = kony.i18n.getLocalizedString("i18n.PFM.UncategorizedTransactions");
            this.view.breadcrumb.lblBreadcrumb3.setVisibility(true);
			}
        },
        /**
         * setDataForUnCategorizedTransactions : method to set data for uncategorized tranasactions
         * @member of {frmPersonalFinanceManagementController}
         * @param {Array} data - transactions data
         * @returns {}
         * @throws {} 
         */
        setDataForUnCategorizedTransactions: function (data , config) {
            var self =this;
            if (data.length === 0) {
                this.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(true);
                this.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(false);
                this.view.TransactionsUnCategorized.segTransactions.setVisibility(false);
                this.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(false);
                this.view.TransactionsUnCategorized.flxSort.setVisibility(false);
                CommonUtilities.hideProgressBar(this.view);
				self.AdjustScreen();
                return;
            } else {
                this.view.TransactionsUnCategorized.flxSort.setVisibility(true);
                this.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(false);
                this.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
                this.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(true);
                this.view.TransactionsUnCategorized.segTransactions.setVisibility(true);
            }
            var widgetMap = {
                "flxActions": "flxActions",
                "flxAmount": "flxAmount",
                "flxCategory": "flxCategory",
                "flxDate": "flxDate",
                "flxDescription": "flxDescription",
                "flxDetail": "flxDetail",
                "flxDetailData": "flxDetailData",
                "flxDetailHeader": "flxDetailHeader",
                "flxDropdown": "flxDropdown",
                "flxFromAccount": "flxFromAccount",
                "flxFromAccountData": "flxFromAccountData",
                "flxIdentifier": "flxIdentifier",
                "flxInformation": "flxInformation",
                "flxLeft": "flxLeft",
                "flxPFMUnCategorizedTransactionSelected": "flxPFMUnCategorizedTransactionSelected",
                "flxPFMUnCategorizedTransactionsSelected": "flxPFMUnCategorizedTransactionsSelected",
                "flxRight": "flxRight",
                "flxSegDisputedTransactionRowWrapper": "flxSegDisputedTransactionRowWrapper",
                "flxTo": "flxTo",
                "flxToAccount": "flxToAccount",
                "flxToAccountData": "flxToAccountData",
                "flxWrapper": "flxWrapper",
                "imgCategoryDropdown": "imgCategoryDropdown",
                "imgDropdown": "imgDropdown",
                "lblAmount": "lblAmount",
                "lblCategory": "lblCategory",
                "lblDate": "lblDate",
                "lblDescription": "lblDescription",
                "lblFromAccount": "lblFromAccount",
                "lblFromAccountData": "lblFromAccountData",
                "lblIdentifier": "lblIdentifier",
                "lblSeparator": "lblSeparator",
                "lblSeparator2": "lblSeparator2",
                "lblTo": "lblTo",
                "lblToAccount": "lblToAccount",
                "lblToAccountData": "lblToAccountData"
            };
            var data = data.map(this.createViewModelForUnCategorizedTransaction);
            this.view.TransactionsUnCategorized.segTransactions.widgetDataMap = widgetMap;
            this.view.TransactionsUnCategorized.segTransactions.setData(data);
            var sortMap = [{
                name: 'transactionDate',
                imageFlx: this.view.TransactionsUnCategorized.flxSort.flxSortSixColumn.flxLeft.flxSortDate.imgSortDate,
                clickContainer: this.view.TransactionsUnCategorized.flxSort.flxSortSixColumn.flxLeft.flxSortDate
            },
			{
                name: 'amount',
                imageFlx: this.view.TransactionsUnCategorized.flxSort.flxSortSixColumn.flxRight.flxSortAmount.imgSortAmount,
                clickContainer: this.view.TransactionsUnCategorized.flxSort.flxSortSixColumn.flxRight.flxSortAmount
            }
			];
            CommonUtilities.Sorting.setSortingHandlers(sortMap, self.onCategorizedTransactionsSorting.bind(self), self);
            CommonUtilities.Sorting.updateSortFlex(sortMap, config);
            CommonUtilities.hideProgressBar(this.view);
            this.view.forceLayout();
			self.AdjustScreen();
        },
		/**
         * onCategorizedTransactionsSorting: Handler for Make Categorized Transactions Sorting
         * @member {frmPersonalFinanceManagementController}
         * @param {object} event - Event Object
         * @param {object} data - Updated Config
         * @returns {}
         * @throws {}
         */
        onCategorizedTransactionsSorting: function(event, data) {
            this.presenter.fetchUnCategorizedTransations(null, null, data);
        },
        /**
         * AdjustScreen : method to adjust screen after load data
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}
         * @throws {} 
         */        
        AdjustScreen: function () {
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0)
                    this.view.flxFooter.top = mainheight + diff + "dp";
                else
                    this.view.flxFooter.top = mainheight + "dp";
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
        },
        /**
         * initlizeStackedBarChart : Method for used to Initilize the stacked Bar Chart Data.
         * @member of {frmPersonalFinanceManagementController}
         * @param {} /bar chart Data
         * @returns {} get all list of Years.
         * @throws {} 
         */
        initlizeStackedBarChart: function () {
            var data = [];
            var options = {
                displayAnnotations: true,
                colors: ['green', 'red'],
                isStacked: true,
                height: 400,
                legend: 'none',
                annotations: {
                    alwaysOutside: true,
                    startup: true,
                    highContrast: true
                },
            };
            var StackedBarChart = new kony.ui.CustomWidget({
                "id": "stackedBarchart",
                "isVisible": true,
                "left": "1dp",
                "top": "1dp",
                "width": "600dp",
                "height": "800dp",
                "zIndex": 1
            }, {
                    "padding": [0, 0, 0, 0],
                    "paddingInPixel": false
                }, {
                    "widgetName": "StackedBarChart",
                    "chartData": data,
                    "chartProperties": options,
                    "OnClickOfBar": function (data) {
                        //alert(data);
                    }
                });
            this.view.flxBudget2018WrapperdataUnavailable.setVisibility(false);
            this.view.flxMySpendingWrapperBudget.setVisibility(true);
            this.view.flxMySpendingWrapperBudget.add(StackedBarChart);
        },
        /**
         * initlizeBarChart : Method for used to Initilize the Bar Chart Data.
         * @member of {frmPersonalFinanceManagementController}
         * @param {} /bar chart Data
         * @returns {} get all list of Years.
         * @throws {} 
         */
        initlizeBarChart: function () {
			var self = this;
            var data = [];
            var options = {
                title: '',
                height: 500,
                width: 550,
				chartArea : 
				{
					top :20
				},
                legend: {
                    position: 'none'
                },
				annotations :
				{ 
				 alwaysOutside : 'true',
				  textStyle: {
					color:'#000000'
					}
				},
                bars: 'horizontal',
                hAxis: {
                    minValue: 0,
                    ticks: []
                },
                vAxis: {
                    basecolor: 'red',
                    minValue: 0,
                    ticks: [0, 45]
                },
                bar: {
                    groupWidth: "45%"
                },
                colors: ["#3284E5"]
            };
            var BarChart = new kony.ui.CustomWidget({
                "id": "barchart",
                "isVisible": true,
                "left": "1dp",
                "top": "1dp",
                "width": "800dp",
                "height": "400dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "BarChart",
                "chartData": data,
                "chartProperties": options,
                "OnClickOfBar": function(monthId) {
                    self.navigateToMonthlyTransactionsPage(monthId);
                }
            });
            this.view.flxChart.setVisibility(true);
            this.view.flxChart.add(BarChart);
        },
        initlizeDonutChartForCategory: function () {
			var self = this;
            var data = [];
            var options = {
				   legend: {
                    position: 'top',
					fontSize: 16,
					maxLines : 4		
                },
                height: 370,
                width: 370,
                title: '',
                pieHole: 0.5,
				pieSliceText: 'none',
				tooltip :
				{
					text : 'percentage'
				},
                colors: ["#FEDB64", "#E87C5E", "#6753EC", "#E8A75E", "#3645A7", "#04B6DF", "#8ED174", "#D6B9EA", "#B160DC", "#23A8B1"]
            };
            var donutChart = new kony.ui.CustomWidget({
                "id": "donutChart",
                "isVisible": true,
                "left": "1dp",
                "top": "0dp",
                "width": "800dp",
                "height": "390dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "DonutChart",
                "chartData": data,
                "chartProperties": options,
                "OnClickOfPie": function(data) {
                    
                }
            });
            this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.setVisibility(true);
            this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxDonutChart.add(donutChart);
        },
        /**
         * initlizeDonutChart : Method for used to Initilize the Donut Chart Data.
         * @member of {frmPersonalFinanceManagementController}
         * @param {} /bar chart Data
         * @returns {} get all list of Years.
         * @throws {} 
         */
        initlizeDonutChart: function () {
			var self = this;
            var data = [];
            var options = {
                height: 330,
                width: 680,
                position: 'top',
                title: '',
                pieHole: 0.6,
			    pieSliceText: 'none',
				tooltip :
				{
					text : 'percentage'
				},
                colors: ["#FEDB64", "#E87C5E", "#6753EC", "#E8A75E", "#3645A7", "#04B6DF", "#8ED174", "#D6B9EA", "#B160DC", "#23A8B1"]
            };
            var donut = new kony.ui.CustomWidget({
                "id": "donut",
                "isVisible": true,
                "left": "1dp",
                "top": "0dp",
                "width": "750dp",
                "height": "350dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "DonutChart",
                "chartData": data,
                "chartProperties": options,
                "OnClickOfPie": function(data) {
                    self.navigateToMonthlyTransactionsPage();
                }
            });
            this.view.flxMySpendingWrapper.flxDonutChart.add(donut);
        },

		/**
         * navigateToMonthlyTransactionsPage : Method for used to naviagate the monthly transactions page.
         * @member of {frmPersonalFinanceManagementController}
         * @param {} 
         * @returns {} get all list of Years.
         * @throws {} 
         */
        navigateToMonthlyTransactionsPage: function (monthId) {
            var self = this;
            this.setBreadCrumbData(true);
            this.view.flexCategorizedMonthlySpending.setVisibility(true);
            this.view.flxPFMContainers.setVisibility(false);
            this.view.CategorizedMonthlySpending.CommonHeader.btnRequest.onClick = this.showUnCategorizedTransactions;
			if(monthId)
			{
				this.view.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lstSelectMonth.selectedKey = monthId;
				this.presenter.getMonthlyCategorizedTransactions(monthId);
			    this.presenter.getMonthlySpending(false,monthId,this.view.lstSelectPeriod.selectedKey);
			}else{
				this.view.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lstSelectMonth.selectedKey = this.view.lstSelectMonth.selectedKey;
				this.presenter.getMonthlyCategorizedTransactions(this.view.lstSelectMonth.selectedKey);
			    this.presenter.getMonthlySpending(false,this.view.lstSelectMonth.selectedKey,this.view.lstSelectPeriod.selectedKey);
			}
			this.view.forceLayout();
        },

        /**
         * addBarChart : Method for used to update the stacked bar Chart Data.
         * @member of {frmPersonalFinanceManagementController}
         * @param {pfmBarChartData} /bar chart Data
         * @returns {} get all list of Years.
         * @throws {} 
         */
        addBarChart: function (pfmBarChartData) {
            if (pfmBarChartData.length != 0) {
                this.view.flxChart.setVisibility(true);
                this.view.flxChart.barchart.chartData = pfmBarChartData;
                this.view.flxNoTransactions.setVisibility(false);
            } else {
                this.view.flxChart.setVisibility(false);
                this.view.flxNoTransactions.setVisibility(true);
            }
            this.view.forceLayout();
        },
        /**
         * addStackedBarChart : Method for used to update the stacked bar Chart Data.
         * @member of {frmPersonalFinanceManagementController}
         * @param {pfmBudgetChartData} /bar chart Data
         * @returns {} get all list of Years.
         * @throws {} 
         */
        addStackedBarChart: function (pfmBudgetChartData) {
            this.view.lblHeaderBudget.text = kony.i18n.getLocalizedString("i18n.PFM.Budget2018") + "   " + this.view.lstSelectMonth.selectedkeyvalue[1];
            if (pfmBudgetChartData.length != 0) {
                this.view.flxMySpendingWrapperBudget.setVisibility(true);
                this.view.flxBudget2018WrapperdataUnavailable.setVisibility(false);
                this.view.flxMySpendingWrapperBudget.stackedBarchart.chartData = pfmBudgetChartData;
            } else {
                this.view.flxMySpendingWrapperBudget.setVisibility(false);
                this.view.flxBudget2018WrapperdataUnavailable.setVisibility(true);
                //this.view.flxMySpendingWrapperdataUnavailable.setVisibility(true);
            }
            this.view.forceLayout();
        },
        /**
         * addDonut : Method for used to update the  Donut Chart.
         * @member of {frmPersonalFinanceManagementController}
         * @param {pfmChartData} /bar chart Data
         * @returns {} get all list of Years.
         * @throws {} 
         */
        addDonut: function(showBothDonutCharts,pfmChartData, totalCashSpent) {
            if (pfmChartData.length != 0) {                
				if(showBothDonutCharts)
				{
				this.view.flxMySpendingWrapper.setVisibility(true);
                this.view.flxMySpendingWrapper.flxDonutChart.setVisibility(true);
                this.view.flxMySpendingWrapper.flxNoTransactionsMySpending.setVisibility(false);
                this.view.flxMySpendingWrapper.lblOverallSpendingMySpending.setVisibility(true);
                this.view.flxMySpendingWrapper.lblOverallSpendingAmount.setVisibility(true);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpendingAmount.setVisibility(true);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpending.setVisibility(true);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxMySpendingWrapperdataUnavailableSpending.setVisibility(false);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxDonutChart.setVisibility(true);
				this.view.flxMySpendingWrapper.lblOverallSpendingAmount.text = totalCashSpent;
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpendingAmount.text = totalCashSpent;
                this.view.flxMySpendingWrapper.flxDonutChart.donut.chartData = pfmChartData;
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxDonutChart.donutChart.chartData = pfmChartData;	
				}else{
				this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpendingAmount.setVisibility(true);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpending.setVisibility(true);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxMySpendingWrapperdataUnavailableSpending.setVisibility(false);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxDonutChart.setVisibility(true);	
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpendingAmount.text = totalCashSpent;
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxDonutChart.donutChart.chartData = pfmChartData;
				}
               
            } else {
				if(showBothDonutCharts)
				{
				this.view.flxMySpendingWrapper.flxNoTransactionsMySpending.setVisibility(true);
                this.view.flxMySpendingWrapper.flxDonutChart.setVisibility(false);
                this.view.flxMySpendingWrapper.lblOverallSpendingMySpending.setVisibility(false);
                this.view.flxMySpendingWrapper.lblOverallSpendingAmount.setVisibility(false);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpendingAmount.setVisibility(false);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpending.setVisibility(false);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxMySpendingWrapperdataUnavailableSpending.setVisibility(true);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxDonutChart.setVisibility(false);
				}else{
				this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpendingAmount.setVisibility(false);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.lblOverallSpending.setVisibility(false);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxMySpendingWrapperdataUnavailableSpending.setVisibility(true);
                this.view.flxMainContainer.flexCategorizedMonthlySpending.CategorizedMonthlySpending.flxDonutChart.setVisibility(false);
					
				}
                
            }
            this.view.forceLayout();
        },
        /**
         * createViewModelForBulkUpdate : Method for used to create view model for bulkupdate
         * @member of {frmPersonalFinanceManagementController}
         * @param {jsonobject} data - view model object
         * @returns {} get all list of Years.
         * @throws {} 
         */        
        createViewModelForBulkUpdate: function (data) {
            var self = this;
            return {
                imgCheckBox: "unchecked_box.png",
                template: "flxPFMBulkUpdateTransaction",
                lblAmount: data.transactionAmount,
                lblDate: data.transactionDate,
                lblDescription: data.transactionDescription,
                lblFromAccount: kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                lblFromAccountData: CommonUtilities.mergeAccountNameNumber(data.fromAccountName, data.fromAccountNumber),
                lblTo: data.toAccountName,
                lblToAccount: kony.i18n.getLocalizedString("i18n.PayAPerson.ToAccount"),
                lblToAccountData: data.toAccountName,
                lblIdentifier: {
                    skin: "sknflx4a902"
                },
                imgCategoryDropdown: "arrow_down.png",
                imgDropdown: "arrow_down.png",
                lblSeparator: "lblSeparator",
                lblSeparator2: "lblSeparator2",
                flxCheckbox: {
                    onClick: self.toggleCheckBox
                },
                transactionId: data.transactionId,
                categoryId: data.categoryId
            }
        },
        /**
         * setBulkUpdateTransactionData : Method for used to set bulk update the transaction
         * @member of {frmPersonalFinanceManagementController}
         * @param {Array} transaction List
         * @returns {}
         * @throws {} 
         */        
        setBulkUpdateTransactionData: function (transactionList) {
            var BulkTransactionData = {
                "flxAmount": "flxAmount",
                "flxCheckbox": "flxCheckbox",
                "flxDate": "flxDate",
                "flxDescription": "flxDescription",
                "flxDetail": "flxDetail",
                "flxDetailData": "flxDetailData",
                "flxDetailHeader": "flxDetailHeader",
                "flxDropdown": "flxDropdown",
                "flxFromAccount": "flxFromAccount",
                "flxFromAccountData": "flxFromAccountData",
                "flxIdentifier": "flxIdentifier",
                "flxInformation": "flxInformation",
                "flxLeft": "flxLeft",
                "flxPFMBulkUpdateTransactions": "flxPFMBulkUpdateTransactions",
                "flxPFMBulkUpdateTransactionsSelected": "flxPFMBulkUpdateTransactionsSelected",
                "flxRight": "flxRight",
                "flxSegDisputedTransactionRowWrapper": "flxSegDisputedTransactionRowWrapper",
                "flxTo": "flxTo",
                "flxToAccount": "flxToAccount",
                "flxToAccountData": "flxToAccountData",
                "flxWrapper": "flxWrapper",
                "imgCheckBox": "imgCheckBox",
                "imgDropdown": "imgDropdown",
                "lblAmount": "lblAmount",
                "lblDate": "lblDate",
                "lblDescription": "lblDescription",
                "lblFromAccount": "lblFromAccount",
                "lblFromAccountData": "lblFromAccountData",
                "lblIdentifier": "lblIdentifier",
                "lblSeparator": "lblSeparator",
                "lblSeparator2": "lblSeparator2",
                "lblTo": "lblTo",
                "lblToAccount": "lblToAccount",
                "lblToAccountData": "lblToAccountData"
            };
            this.view.TransactionsUnCategorized.segTransactions.widgetDataMap = BulkTransactionData;
            this.view.TransactionsUnCategorized.segTransactions.setData(transactionList.map(this.createViewModelForBulkUpdate));
            this.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "unchecked_box.png";
            this.disableButton(this.view.TransactionsUnCategorized.btnDownload);
            this.view.forceLayout();
        },
        /**
         * removedSelectedRows : Method used to removed Selected Rows
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}
         * @throws {} 
         */        
        removedSelectedRows: function () {
            var data = this.view.TransactionsUnCategorized.segTransactions.data;
            if (data.length === 0) return;
            for (var i = 0; i < data.length; i++) {
                if (data[i].imgCheckBox === "checked_box.png") {
                    this.view.TransactionsUnCategorized.segTransactions.removeAt(i);
                }
            }
        },        
        /**
         * toggleCheckBox : Method used to toggle check box
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {}
         * @throws {} 
         */        
        toggleCheckBox: function () {
            var index = this.view.TransactionsUnCategorized.segTransactions.selectedIndex[1];
            var data = this.view.TransactionsUnCategorized.segTransactions.data;
            if (data.length === 0) return;
            var selectedRowCount = 0;
            for (var i = 0; i < data.length; i++) {
                if (i == index) {
                    if (data[i].imgCheckBox === "unchecked_box.png") {
                        data[i].imgCheckBox = "checked_box.png";
                    }
                    else {
                        data[i].imgCheckBox = "unchecked_box.png";
                    }
                }
                if (data[i].imgCheckBox === "checked_box.png") {
                    selectedRowCount++;
                }
            }
            this.view.TransactionsUnCategorized.segTransactions.setDataAt(data[index], index);
            if (selectedRowCount === data.length) {
                this.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "checked_box.png";
            } else if (this.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src === "checked_box.png") {
                this.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "unchecked_box.png";
            }
            if (selectedRowCount > 0 && !CommonUtilities.isCSRMode()) {
                this.enableButton(this.view.TransactionsUnCategorized.btnDownload);
            } else {
                this.disableButton(this.view.TransactionsUnCategorized.btnDownload);
            }
        },
        /**
         * getSelectedTransactions : Method to get selected rows from segment
         * @member of {frmPersonalFinanceManagementController}
         * @param {}
         * @returns {Array} list of rows
         * @throws {} 
         */        
        getSelectedTransactions: function () {
            var categoryId = this.view.AssignCategory.lbxSelectFormat.selectedKey;
            var data = this.view.TransactionsUnCategorized.segTransactions.data;
            var selectedRows = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].imgCheckBox === "checked_box.png") {
                    selectedRows.push({
                        transactionId: data[i].transactionId,
                        categoryId: categoryId
                    });
                }
            }
            return selectedRows;
        },
        /**
        * disableButton - Disable button
        * @member frmPersonalFinanceManagementController
        * @param {Object} button
        * @returns {void} - None
        * @throws {void} - None
        */
        disableButton: function (button) {
            button.setEnabled(false);
            button.skin = "sknBtnBlockedLatoFFFFFF15Px";
            button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
            button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
        },
        /**
        * enableButton - Enable button
        * @member frmPersonalFinanceManagementController
        * @param {Object} button
        * @returns {void} - None
        * @throws {void} - None
        */
        enableButton: function (button) {
            button.setEnabled(true);
            button.skin = "sknbtnLatoffffff15px";
            button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
            button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
        }
    };
});