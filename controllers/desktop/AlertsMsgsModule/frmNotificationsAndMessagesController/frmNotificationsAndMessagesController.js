define(['CommonUtilities','CSRAssistUI'], function (CommonUtilities,CSRAssistUI) {
  return {
    willUpdateUI: function (alertsViewModel) {
      if (alertsViewModel.ProgressBar) {
        if (alertsViewModel.ProgressBar.show) {
          CommonUtilities.showProgressBar(this.view);
        } else {
          CommonUtilities.hideProgressBar(this.view);
        }
      }
      if (alertsViewModel.sideMenu) {
        this.updateHamburgerMenu(alertsViewModel.sideMenu);
      }
      if (alertsViewModel.topBar) {
        this.updateTopBar(alertsViewModel.topBar);
      }
      if (alertsViewModel.showAlertsViewModel) {
        this.showAlertsViewModel(alertsViewModel.showAlertsViewModel);
      }
      if (alertsViewModel.unreadNotificationCountViewModel) {
        this.showUnreadNotificationCount(alertsViewModel.unreadNotificationCountViewModel.count);
      }
      if (alertsViewModel.unreadMessagesCountViewModel) {
        this.showUnreadMessagesCount(alertsViewModel.unreadMessagesCountViewModel);
      }
      if (alertsViewModel.updateNotificationAsReadViewModel) {
        this.updateNotificationAsReadViewModel(alertsViewModel.updateNotificationAsReadViewModel);
      }
      if (alertsViewModel.dismissAlertsViewModel) {
        this.dismissAlertsViewModel(alertsViewModel.dismissAlertsViewModel);
      }
      if (alertsViewModel.searchAlertsViewModel) {
        this.showSearchAlertsViewModel(alertsViewModel.searchAlertsViewModel);
      }
      if (alertsViewModel.showRequestsViewModel) {
        this.showRequestsViewModel(alertsViewModel.showRequestsViewModel);
      }
      if (alertsViewModel.showMessagesViewModel) {
        this.showMessagesViewModel(alertsViewModel.showMessagesViewModel);
      }
      if (alertsViewModel.showDeletedRequestsViewModel) {
        this.showDeletedRequestsViewModel(alertsViewModel.showDeletedRequestsViewModel);
      }
      if (alertsViewModel.createNewRequestOrMessagesViewModel) {
        this.showNewMessage(alertsViewModel.createNewRequestOrMessagesViewModel);
      }
      if (alertsViewModel.createNewRequestError) {
        this.showNewRequestCreationError(alertsViewModel.createNewRequestError);
      }
      if (alertsViewModel.createNewMessageError) {
        this.showNewMessageCreationError(alertsViewModel.createNewMessageError);
      }
      if (alertsViewModel.updateMessageAsReadSuccessViewModel) {
        this.updateMessageAsReadSuccessViewModel(alertsViewModel.updateMessageAsReadSuccessViewModel.readCount);
      }
      if (alertsViewModel.searchRequestsViewModel) {
        this.showSearchRequestsViewModel(alertsViewModel.searchRequestsViewModel);
      }
      if (alertsViewModel.showSearchDeletedRequestsViewModel) {
        this.showSearchDeletedRequestsViewModel(alertsViewModel.showSearchDeletedRequestsViewModel);
      }
      this.AdjustScreen();
    },

    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel);
      this.view.forceLayout();
    },

    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },

    /**
    * disableButton :The function is used to disable the button using the disabled skin 
    * @member of {frmNotificationsAndMessagesController}
    * @param {button}  The Button for which the skin should be disabled
    * @return {} 
    * @throws {}
   */

    disableButton: function (button) {
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.setEnabled(false);
    },
    
    /**
    * enableButton :The function is used to enable the button using the enabled skin 
    * @member of {frmNotificationsAndMessagesController}
    * @param {button}  The Button for which the skin should be enabled
    * @return {} 
    * @throws {}
   */
    enableButton: function (button) {
      button.skin = "sknBtnNormalLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
      button.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.setEnabled(true);
    },
    
    /**
    * enableSendButton :The function is used to enable send button once the fields subject, Description and category are entered while creating a new message
    * @member of {frmNotificationsAndMessagesController}
    * @param {}  
    * @return {} 
    * @throws {}
   */
    enableSendButton: function () {
      var category = this.view.NotficationsAndMessages.listbxCategory.selectedkey;
      var subject = this.view.NotficationsAndMessages.tbxSubject.text.trim();
      var description = this.view.NotficationsAndMessages.textareaDescription.text.trim();
      if (category && subject.length > 0 && description.length > 0 && !(CommonUtilities.isCSRMode())) {
        this.enableButton(this.view.NotficationsAndMessages.btnNewMessageSend);
      } else {
        this.disableButton(this.view.NotficationsAndMessages.btnNewMessageSend);
      }
    },

    initFunction: function () {
      var css = document.createElement("loading");
      css.type = "text/css";
      css.innerHTML = "@-webkit-keyframes sk-bouncedelay {0 % , 80 % , 100 % {-webkit - transform: scale(0) } 40 % {-webkit - transform: scale(1.0) } } #frmNotificationsAndMessages_flxLoadingOne{ -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both; }";
      document.body.appendChild(css);

    },

    /**
    * preShowFunction :This function is executed on the preShow of the frmNotificationsAndMessages
    * @member of {frmNotificationsAndMessagesController}
    * @param {}  
    * @return {} 
    * @throws {}
   */
    preShowFunction: function () {
	    var scopeObj=this;	
      this.view.NotficationsAndMessages.txtSearch.text = "";
      this.view.NotficationsAndMessages.lblHeadingNotification.text = "";
      this.view.NotficationsAndMessages.RichText0c22ac14f53af45.text = "";
      this.view.NotficationsAndMessages.lblDateAndTime.text = "";
      this.view.NotficationsAndMessages.tbxSubject.text = "";
      this.view.NotficationsAndMessages.textareaDescription.text = "";
      this.view.NotficationsAndMessages.lblOfferLink.isVisible = false;
      this.view.NotficationsAndMessages.imgBanner.isVisible = false;
      this.view.NotficationsAndMessages.imgCross.setVisibility(true);
      this.view.NotficationsAndMessages.segMessageAndNotification.setData([]);
      this.view.NotficationsAndMessages.segMessages.setData([]);
      this.disableButton(this.view.NotficationsAndMessages.btnNewMessageSend);
      this.view.NotficationsAndMessages.tbxSubject.onKeyUp = this.enableSendButton.bind(this);
      this.view.NotficationsAndMessages.textareaDescription.onKeyUp = this.enableSendButton.bind(this);
      this.view.flxBottom.tablePagination.flxPagination.skin = "sknflxe9ebee";
      this.view.customheader.flxUserActions.isVisible = false;
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
      this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.forceCloseHamburger();
      scopeObj.view.NotficationsAndMessages.flxClearSearch.onClick = function () {
        scopeObj.view.NotficationsAndMessages.txtSearch.text = "";
        scopeObj.view.NotficationsAndMessages.flxClearSearch.setVisibility(false);
        scopeObj.view.forceLayout();
        scopeObj.view.NotficationsAndMessages.txtSearch.setFocus(true);
      };
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages")
      }, {
        text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts")
      }]);

      //FOR ALERTS: MANAGE SEARCH 
      this.searchAnimate();
      this.view.NotficationsAndMessages.btnSearch.isVisible = true;
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts");
      this.view.breadcrumb.setFocus(true);
      var css = document.createElement("loading");
      css.type = "text/css";
      css.innerHTML = "@-webkit-keyframes sk-bouncedelay {0 % , 80 % , 100 % {-webkit - transform: scale(0) } 40 % {-webkit - transform: scale(1.0) } } #frmNotificationsAndMessages_flxLoadingOne{ -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both; }";
      document.getElementsByTagName("head")[0].appendChild(css);

      this.view.NotficationsAndMessages.btnSendReply.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.replyButton");
      this.view.NotficationsAndMessages.btnSendReply.toolTip = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.replyButton");

      //setting the Actions to the tabs and buttons in the frmNotificationsAndMessaages
      this.view.NotficationsAndMessages.btnNotifications.onClick = function () {
        scopeObj.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages")
        }, {
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts")
        }]);
        scopeObj.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages");
        scopeObj.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts");
        scopeObj.view.NotficationsAndMessages.txtSearch.text = "";
        scopeObj.view.NotficationsAndMessages.flxClearSearch.setVisibility(false);
        scopeObj.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(true);
        scopeObj.view.NotficationsAndMessages.btnSearchCancel.setVisibility(false);
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.showAlertsPage();
        scopeObj.view.NotficationsAndMessages.imgMsgsSearch.onTouchEnd = scopeObj.onSearchClick.bind(scopeObj);
        scopeObj.view.NotficationsAndMessages.btnSearchCancel.onClick = scopeObj.onSearchClick.bind(scopeObj);
        scopeObj.view.NotficationsAndMessages.txtSearch.onDone = scopeObj.onSearchClick.bind(scopeObj);
      };
      this.view.NotficationsAndMessages.btnMyMessages.onClick = function () {
        scopeObj.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages")
        }, {
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages")
        }]);
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.showRequests();
        scopeObj.view.NotficationsAndMessages.btnSearch.isVisible = true;
        scopeObj.view.NotficationsAndMessages.txtSearch.text = "";
        scopeObj.view.NotficationsAndMessages.flxClearSearch.setVisibility(false);
        scopeObj.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(true);
        scopeObj.view.NotficationsAndMessages.btnSearchCancel.setVisibility(false);
        scopeObj.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages");
        scopeObj.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages");
      };

      this.view.NotficationsAndMessages.btnDeletedMessages.onClick = function () {
        scopeObj.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages")
        }, {
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.DeletedMessages")
        }]);
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.showDeletedRequests();
        scopeObj.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(true);
        scopeObj.view.NotficationsAndMessages.btnSearchCancel.setVisibility(false);
        scopeObj.view.NotficationsAndMessages.txtSearch.text = "";
        scopeObj.view.NotficationsAndMessages.flxClearSearch.setVisibility(false);
      };
      this.view.NotficationsAndMessages.btnNewMessage.onClick = function () {
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.view.NotficationsAndMessages.txtSearch.text = "";
        scopeObj.view.NotficationsAndMessages.tbxSubject.text = "";
        scopeObj.view.NotficationsAndMessages.textareaDescription.text = "";
        scopeObj.disableButton(scopeObj.view.NotficationsAndMessages.btnNewMessageSend);
        scopeObj.presenter.showRequests(null, "createNewMessage");
      };
      this.view.NotficationsAndMessages.flxSearchImage.onClick = function () {
        scopeObj.searchAnimate();
      };
      this.view.NotficationsAndMessages.btnDelete.onClick = function () {
        scopeObj.showSoftDeletePopup();
      };
      if(CommonUtilities.isCSRMode()){
          this.view.NotficationsAndMessages.btnDeleteForever.onClick = CommonUtilities.disableButtonActionForCSRMode();
          this.view.NotficationsAndMessages.btnDeleteForever.skin = CommonUtilities.disableButtonSkinForCSRMode();
      }
      else{
      this.view.NotficationsAndMessages.btnDeleteForever.onClick = function () {
        scopeObj.showHardDeletePopup();
      };
      }
      this.view.NotficationsAndMessages.imgAttachmentBLue2.onTouchStart = function () {
        scopeObj.browseFiles();
      };
      this.view.CustomPopup1.btnNo.onClick = function () {
        scopeObj.closeDismissPopup();
      };
      this.view.CustomPopup1.flxCross.onClick = function () {
        scopeObj.closeDismissPopup();
      };
      if(CommonUtilities.isCSRMode()){
          this.view.NotficationsAndMessages.btnSendReply.onClick = CommonUtilities.disableButtonActionForCSRMode();
          this.view.NotficationsAndMessages.btnSendReply.skin = CommonUtilities.disableButtonSkinForCSRMode();
          this.view.NotficationsAndMessages.btnSendReply.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
      }
      else{
      this.view.NotficationsAndMessages.btnSendReply.onClick = function () {
        scopeObj.showReplyView();
      };
      }
      if(CommonUtilities.isCSRMode()){
        this.view.NotficationsAndMessages.btnNewMessageSend.onClick = CommonUtilities.disableButtonActionForCSRMode();
        this.view.NotficationsAndMessages.btnNewMessageSend.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
        this.view.NotficationsAndMessages.btnNewMessageSend.skin = CommonUtilities.disableButtonSkinForCSRMode();
      }
      else{
      this.view.NotficationsAndMessages.btnNewMessageSend.onClick = function () {
        CommonUtilities.showProgressBar(scopeObj.view);
        var requestParam = {
          "files": scopeObj.fileObject,
          "categoryid": scopeObj.view.NotficationsAndMessages.listbxCategory.selectedkey,
          "subject": scopeObj.view.NotficationsAndMessages.tbxSubject.text,
          "description": scopeObj.view.NotficationsAndMessages.textareaDescription.text,
          "username": kony.mvc.MDAApplication.getSharedInstance().appContext.username
        };
        scopeObj.presenter.createNewRequestOrMessage(requestParam);
      };
      }
      if(CommonUtilities.isCSRMode()){
          this.view.NotficationsAndMessages.btnDismiss.onClick = CommonUtilities.disableButtonActionForCSRMode();
          this.view.NotficationsAndMessages.btnDismiss.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
      }else{
      this.view.NotficationsAndMessages.btnDismiss.onClick = function () {
        scopeObj.showDismissPopup();
      };
      }
      this.view.NotficationsAndMessages.btnCancel.onClick = function () {
        scopeObj.hideCreateMessageView();
      };
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        var context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";

      };
      this.view.CustomPopup.btnNo.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
	  if(CommonUtilities.isCSRMode())
	      CSRAssistUI.setCSRAssistConfigurations(scopeObj,'frmNotificationsAndMessages');

    },

    /**
    * showUnreadNotificationCount :This function is used to display the unread Notifications Count in the Tab
    * @member of {frmNotificationsAndMessagesController}
    * @param {data}  data consists of the unreadNotificationCount 
    * @return {} 
    * @throws {}
   */
    showUnreadNotificationCount: function (data) {
      this.unReadNotificationCount = data;
      this.view.NotficationsAndMessages.btnNotifications.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts") + " (" + data + ")";
      this.updateAlertsIcon();
      this.view.forceLayout();
    },

    showOrHideSearchCrossImage : function() {
      var searchString = this.view.NotficationsAndMessages.txtSearch.text;
      if(searchString && searchString.trim()) {
        this.view.NotficationsAndMessages.flxClearSearch.setVisibility(true);
      } else {
        this.view.NotficationsAndMessages.flxClearSearch.setVisibility(false);
      }
      this.view.forceLayout();
    },

    /**
    * showAlertsViewModel :This function is used to bind the alerts to the segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {viewModel}  viewModel consists list of Alerts Data 
    * @return {} 
    * @throws {}
   */
    showAlertsViewModel: function (viewModel) {
      var data = viewModel.data;
      var self = this;
      this.view.customheader.customhamburger.activateMenu("ALERTS & MESSAGES",  "Alerts");
      self.view.NotficationsAndMessages.imgMsgsSearch.onTouchEnd = self.onSearchClick.bind(self);
      self.view.NotficationsAndMessages.btnSearchCancel.onClick = self.onSearchClick.bind(self);
      self.view.NotficationsAndMessages.txtSearch.onDone = self.onSearchClick.bind(self);
      self.view.NotficationsAndMessages.txtSearch.onKeyUp = self.showOrHideSearchCrossImage.bind(self);
      this.view.NotficationsAndMessages.flxSearchAndSort.isVisible = true;
      if (data.length === 0) {
        this.view.NotficationsAndMessages.btnDismiss.isVisible = false;
        if(viewModel.errorMsg) {
          this.view.NotficationsAndMessages.rtxNoSearchResults.text = viewModel.errorMsg;
        } else {
          this.view.NotficationsAndMessages.rtxNoSearchResults.text = "No alerts available at this time";
        }
        this.view.NotficationsAndMessages.flxNoSearchResult.isVisible = true;
        this.view.NotficationsAndMessages.segMessageAndNotification.isVisible = false;
        this.view.NotficationsAndMessages.btnNewMessage.setVisibility(true);
        this.view.NotficationsAndMessages.flxRightNotifications.setVisibility(false);
        this.view.NotficationsAndMessages.flxRightMessages.setVisibility(false);
        this.view.NotficationsAndMessages.flxNewMessage.setVisibility(false);
        this.setSkinActive(this.view.NotficationsAndMessages.flxTabs.btnNotifications);
        this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnMyMessages);
        this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnDeletedMessages);
      } else {
        this.view.NotficationsAndMessages.btnDismiss.isVisible = true;
        this.view.NotficationsAndMessages.flxNoSearchResult.isVisible = false;
        this.view.NotficationsAndMessages.segMessageAndNotification.isVisible = true;
        this.setNotificationSegmentData(data); 
      }
      CommonUtilities.hideProgressBar(this.view);
    },
    setSkinActive: function (obj) {
      obj.skin = "sknBtnAccountSummarySelected";
    },
    setSkinInActive: function (obj) {
      obj.skin = "sknBtnAccountSummaryUnselected";
    },
   
   /**
    * postShowNotifications :This function is executed on the post Show of the frmNotificationsAndMessages
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    postShowNotifications: function () {
      var self = this;
      this.view.NotficationsAndMessages.flxInfo.onClick = function() {
         if(self.view.AllForms.isVisible === true)
            self.view.AllForms.isVisible = false;
         else
           self.view.AllForms.isVisible = true;
      };
      this.AdjustScreen();
    },
    //UI Code
    AdjustScreen: function () {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0)
          this.view.flxFooter.top = mainheight + diff + "dp";
        else
          this.view.flxFooter.top = mainheight + "dp";
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },
    
    searchAnimate: function () {
      //    this.view.NotficationsAndMessages.flxSort.setVisibility(false);
      this.view.NotficationsAndMessages.flxSearch.animate(
        kony.ui.createAnimation({
          "100": {
            "left": "0%",
            "stepConfig": {
              "timingFunction": kony.anim.EASE
            },
            "rectified": true
          }
        }), {
          "delay": 0,
          "iterationCount": 1,
          "fillMode": kony.anim.FILL_MODE_FORWARDS,
          "duration": 0.5
        }, {});

    },

    sortAnimate: function () {
      //    this.view.NotficationsAndMessages.flxSort.setVisibility(true);
      this.view.NotficationsAndMessages.flxSearch.animate(
        kony.ui.createAnimation({
          "100": {
            "left": "100%",
            "stepConfig": {
              "timingFunction": kony.anim.EASE
            },
            "rectified": true
          }
        }), {
          "delay": 0,
          "iterationCount": 1,
          "fillMode": kony.anim.FILL_MODE_FORWARDS,
          "duration": 0.5
        }, {});

    },

    /**
    * enableSendReplyButton :This function is used to enable/Disable the reply button while replying to the existing request
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
  enableSendReplyButton: function () {
      if (this.view.NotficationsAndMessages.txtAreaReply.text.trim() === "") {
        this.disableButton(this.view.NotficationsAndMessages.btnSendReply);
      } else {
        this.enableButton(this.view.NotficationsAndMessages.btnSendReply);
      }
    },

     /**
    * showReplyView :This function is used to show the reply view once we click on Reply button
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    showReplyView: function () {
      var self = this;
      this.disableButton(this.view.NotficationsAndMessages.btnSendReply);
      this.view.NotficationsAndMessages.flxReplyMessageButtons.setVisibility(false);
      this.view.NotficationsAndMessages.flxSendMessage.setVisibility(true);
      this.view.NotficationsAndMessages.flxImageAttachment.setVisibility(true);
      this.view.NotficationsAndMessages.imgAttachmentBLue.setVisibility(true);
      this.view.NotficationsAndMessages.lblWarningReplyMessage.setVisibility(false);
      this.view.NotficationsAndMessages.btnCancelReply.setVisibility(true);
      this.view.NotficationsAndMessages.btnSendReply.text = "Send";
      this.view.NotficationsAndMessages.btnCancelReply.text = "Cancel";
      this.view.NotficationsAndMessages.txtAreaReply.onKeyUp = this.enableSendReplyButton.bind(this);
      this.view.NotficationsAndMessages.flxReply.isVisible = true;
      this.view.NotficationsAndMessages.flxReplyWrapper.isVisible = true;
      this.view.NotficationsAndMessages.txtAreaReply.text = "";
      this.fileObject = [];
      this.view.NotficationsAndMessages.segAttachmentRightMessage.data = [];
      this.view.NotficationsAndMessages.flxImageAttachment.onClick = this.replyBrowseFiles.bind(this);
      this.view.NotficationsAndMessages.btnCancelReply.onClick = this.hideReplyView.bind(this);
      this.view.NotficationsAndMessages.btnSendReply.onClick = function () {
        CommonUtilities.showProgressBar(self.view);
        var requestParam = {
          "files": self.fileObject,
          "requestid": self.view.NotficationsAndMessages.segMessages.data[0].requestId,
          "description": self.view.NotficationsAndMessages.txtAreaReply.text,
          "username": kony.mvc.MDAApplication.getSharedInstance().appContext.username
        };
        self.presenter.createNewRequestOrMessage(requestParam);
      };
      this.view.forceLayout();
      this.view.NotficationsAndMessages.flxReplyHeader.setFocus(true);
      this.view.NotficationsAndMessages.txtAreaReply.setFocus(true);
    },

    /**
    * hideReplyView :This function is used to hide the reply view once we click on ReplyCancel button
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    hideReplyView: function () {
      this.enableButton(this.view.NotficationsAndMessages.btnSendReply);
      this.view.NotficationsAndMessages.flxSendMessage.isVisible = true;
      this.view.NotficationsAndMessages.flxReply.isVisible = false;
      this.view.NotficationsAndMessages.flxReplyWrapper.isVisible = false;
      this.view.NotficationsAndMessages.btnCancelReply.isVisible = false;
      this.view.NotficationsAndMessages.flxImageAttachment.setVisibility(false);
      this.view.NotficationsAndMessages.lblWarningReplyMessage.setVisibility(false);
      this.view.NotficationsAndMessages.btnSendReply.text = "Reply";
      this.view.NotficationsAndMessages.btnSendReply.onClick = this.showReplyView.bind(this);
      this.view.forceLayout();
    },

    /**
    * hideCreateMessageView :This function is used to hide the create Message Template once we click on CancelCreatemessage button
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    hideCreateMessageView: function () {
      this.view.NotficationsAndMessages.btnNewMessage.setVisibility(true);
      this.view.NotficationsAndMessages.flxRightNotifications.setVisibility(false);
      this.view.NotficationsAndMessages.flxNewMessage.setVisibility(false);
      this.setSkinActive(this.view.NotficationsAndMessages.flxTabs.btnMyMessages);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnNotifications);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnDeletedMessages);
      var dataMap = {
        "flxNotificationsAndMessages": "flxNotificationsAndMessages",
        "imgAttachment": "imgAttachment",
        "imgCurrentOne": "imgCurrentOne",
        "lblDateAndTime": "lblDateAndTime",
        "lblSegDescription": "lblSegDescription",
        "lblSegHeading": "lblSegHeading",
        "imgBulletIcon": "imgBulletIcon",
        "segNotificationsAndMessages": "segNotificationsAndMessages"
      };
      var data = [];
      if (this.view.NotficationsAndMessages.segMessageAndNotification.data && this.view.NotficationsAndMessages.segMessageAndNotification.data.length > 1) {
        //some rows are already present.. moving the data one step up.
        data = this.view.NotficationsAndMessages.segMessageAndNotification.data;
        data.splice(0, 1);
        data[0].flxNotificationsAndMessages.skin = "sknFlxf7f7f7";
        data[0].flxNotificationsAndMessages.hoverSkin = "sknFlxf7f7f7";
        this.view.NotficationsAndMessages.flxRightMessages.setVisibility(true);
      } else {
        // there is no data..
        this.view.NotficationsAndMessages.flxNoSearchResult.setVisibility(true);
        this.view.NotficationsAndMessages.segMessageAndNotification.setVisibility(false);
        this.view.NotficationsAndMessages.flxRightMessages.setVisibility(false);
      }
      this.view.NotficationsAndMessages.segMessageAndNotification.widgetDataMap = dataMap;
      this.view.NotficationsAndMessages.segMessageAndNotification.setData(data);
      if (data.length > 0 ) {
        this.OnMessageRowSelection();
      }
      this.view.forceLayout();
    },

    /**
    * showNewMessage :This function is used to show the create Message Template once we click on newMessage button
    * @member of {frmNotificationsAndMessagesController}
    * @param {viewModel}  viewModel consists of the list of categories data  
    * @return {} 
    * @throws {}
   */
    showNewMessage: function (viewModel) {
      this.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages")
        }, {
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages")
      }]);
      this.view.customheader.customhamburger.activateMenu("ALERTS & MESSAGES",  "New Message");
      this.view.NotficationsAndMessages.lblWarningNewMessage.setVisibility(false);
      this.fileObject = [];
      if (viewModel.data) {
        var requestCategories = viewModel.data.map(function (dataItem) {
          var keyValue = [];
          keyValue.push(dataItem.id);
          keyValue.push(dataItem.Name);
          return keyValue;
        });
        this.view.NotficationsAndMessages.listbxCategory.masterData = requestCategories;
      }
      this.view.NotficationsAndMessages.segAttachment.setData([]);
      this.view.NotficationsAndMessages.tbxSubject.text = "";
      this.view.NotficationsAndMessages.textareaDescription.text = "";
      this.view.NotficationsAndMessages.btnNewMessage.setVisibility(false);
      this.view.NotficationsAndMessages.flxRightMessages.setVisibility(false);
      this.view.NotficationsAndMessages.flxRightNotifications.setVisibility(false);
      this.view.NotficationsAndMessages.flxNewMessage.setVisibility(true);
      this.setSkinActive(this.view.NotficationsAndMessages.flxTabs.btnMyMessages);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnNotifications);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnDeletedMessages);
      var dataMap = {
        "flxNotificationsAndMessages": "flxNotificationsAndMessages",
        "imgAttachment": "imgAttachment",
        "imgCurrentOne": "imgCurrentOne",
        "lblDateAndTime": "lblDateAndTime",
        "lblSegDescription": "lblSegDescription",
        "lblSegHeading": "lblSegHeading",
        "imgBulletIcon": "imgBulletIcon",
        "segNotificationsAndMessages": "segNotificationsAndMessages"
      };
      var data = [];
      var newMessageData = {
        "imgCurrentOne": {
          "src": "accounts_sidebar_blue.png",
          "isVisible": true
        },
        "flxNotificationsAndMessages": {
          "skin": "sknFlxf7f7f7",
          "hoverSkin": "sknFlxf7f7f7"
        },
        "lblSegHeading": {
          "text": "New Message"
        }
      };
      if (this.view.NotficationsAndMessages.segMessageAndNotification.data && this.view.NotficationsAndMessages.segMessageAndNotification.data.length > 0) {
        //some rows are already present.. moving the data one step down.
        data = this.view.NotficationsAndMessages.segMessageAndNotification.data;
        data = data.map(function (dataItem) {
          dataItem.imgCurrentOne.isVisible = false;
          dataItem.flxNotificationsAndMessages.skin = "sknFlxffffff";
          dataItem.flxNotificationsAndMessages.hoverSkin = "sknFlxf7f7f7";
          return dataItem;
        });
        data.unshift(newMessageData);
      } else {
        // there is no data..
        this.view.NotficationsAndMessages.segMessageAndNotification.setVisibility(true);
        this.view.NotficationsAndMessages.flxNoSearchResult.setVisibility(false);
        data.push(newMessageData);
      }
      this.view.NotficationsAndMessages.segMessageAndNotification.widgetDataMap = dataMap;
      this.view.NotficationsAndMessages.segMessageAndNotification.setData(data);
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },

    /**
    * showNewRequestCreationError :This function is used to show the error when  the create New Request Fails
    * @member of {frmNotificationsAndMessagesController}
    * @param {errorMsg}    
    * @return {} 
    * @throws {}
   */
    showNewRequestCreationError : function (errorMsg) {
        this.view.NotficationsAndMessages.lblWarningNewMessage.text = errorMsg;
        this.view.NotficationsAndMessages.lblWarningNewMessage.setVisibility(true);
        CommonUtilities.hideProgressBar(this.view);
    },

    /**
    * showNewMessageCreationError :This function is used to show the error when  the create New Message Fails
    * @member of {frmNotificationsAndMessagesController}
    * @param {errorMsg}    
    * @return {} 
    * @throws {}
   */
    showNewMessageCreationError : function (errorMsg) {
        this.view.NotficationsAndMessages.lblWarningReplyMessage.text = errorMsg;
        this.view.NotficationsAndMessages.lblWarningReplyMessage.setVisibility(true);
        CommonUtilities.hideProgressBar(this.view);
    },

    /**
    * showDismissPopup :This function is used to show the dismiss PopUp once the user clicks on dismiss Noification
    * @member of {frmNotificationsAndMessagesController}
    * @param {}    
    * @return {} 
    * @throws {}
   */
    showDismissPopup: function () {
      this.view.FlxDismiss.setVisibility(true);
      this.view.CustomPopup1.lblHeading.text = "DISMISS";
      this.view.CustomPopup1.lblPopupMessage.text = "Are you sure you want to dismiss this Notification?";
    },

    /**
    * closeDismissPopup :This function is used to close the Dismiss Pop Up when clicked on "No" button in the Dismiss PopUp
    * @member of {frmNotificationsAndMessagesController}
    * @param {}    
    * @return {} 
    * @throws {}
   */
    closeDismissPopup: function () {
      this.view.FlxDismiss.setVisibility(false);
    },

    /**
    * setNotificationSegmentData :This function is used to bind the Alerts to the segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {data} data contains the Array of Alerts   
    * @return {} 
    * @throws {}
   */
    setNotificationSegmentData: function (data) {
      this.view.NotficationsAndMessages.segMessageAndNotification.onRowClick = this.onRowSelection.bind(this);
      this.view.NotficationsAndMessages.btnNewMessage.setVisibility(true);
      this.view.NotficationsAndMessages.flxRightNotifications.setVisibility(true);
      this.view.NotficationsAndMessages.flxRightMessages.setVisibility(false);
      this.view.NotficationsAndMessages.flxNewMessage.setVisibility(false);
      this.setSkinActive(this.view.NotficationsAndMessages.flxTabs.btnNotifications);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnMyMessages);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnDeletedMessages);
      var dataMap = {
        "flxNotificationsAndMessages": "flxNotificationsAndMessages",
        "imgCurrentOne": "imgCurrentOne",
        "lblDateAndTime": "lblDateAndTime",
        "lblSegDescription": "lblSegDescription",
        "lblSegHeading": "lblSegHeading",
        "imgBulletIcon": "imgBulletIcon",
        "imgAttachment": "imgAttachment",
        "segNotificationsAndMessages": "segNotificationsAndMessages",
        "userNotificationId": "userNotificationId"
      };
      var self = this;
      data = data.map(function (dataItem) {
        return {
          "imgCurrentOne": {
            "src": "accounts_sidebar_blue.png",
            "isVisible": false
          },
          "imgBulletIcon": {
            "isVisible": !self.isRead(dataItem.isRead)
          },
          "imgAttachment": {
            "src": "attachment_grey.png",
            "isVisible": false
          },
          "lblDateAndTime": {
            "text": CommonUtilities.getDateAndTime(dataItem.receivedDate, CommonUtilities.getConfiguration('frontendDateFormat'))
          },
          "lblSegDescription": {
            "text": self.labelData(dataItem.notificationText)
          },
          "flxNotificationsAndMessages": {
            "skin": "sknFlxffffff",
            "hoverSkin": "sknFlxf7f7f7"
          },
          "lblSegHeading": {
            "text": dataItem.notificationSubject
          },
          "userNotificationId": dataItem.userNotificationId

        };
      });
      this.view.NotficationsAndMessages.segMessageAndNotification.widgetDataMap = dataMap;
      this.view.NotficationsAndMessages.segMessageAndNotification.setData(data);
      this.onRowSelection();
      this.view.forceLayout();
    },

    /**
    * updateNotificationAsReadViewModel :This function is executed when the notification is marked as read
    * @member of {frmNotificationsAndMessagesController}
    * @param {viewModel}    viewModel consists of status whether it is success or failure and the unreadNotificationCount
    * @return {} 
    * @throws {}
   */
    updateNotificationAsReadViewModel: function (viewModel) {
      if(viewModel.status === "success") {
        this.unReadNotificationCount = this.unReadNotificationCount - 1; // Decrement by 1
        this.view.NotficationsAndMessages.btnNotifications.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts") + " (" + this.unReadNotificationCount + ")";
        if (this.unReadNotificationCount === 0 && this.unreadMessagesCount === 0) {
          this.view.customheader.headermenu.lblNewMessages.isVisible = false;
        } else {
          this.view.customheader.headermenu.lblNewMessages.isVisible = true;
        }
        var data = this.view.NotficationsAndMessages.segMessageAndNotification.data;
        var index = 0;
        if (this.view.NotficationsAndMessages.segMessageAndNotification.selectedIndex) {
          index = this.view.NotficationsAndMessages.segMessageAndNotification.selectedIndex[1];
        }
        data[index].imgBulletIcon.isVisible = false;
        this.view.NotficationsAndMessages.segMessageAndNotification.setDataAt(data[index], index);        
      } else {
        
      }
      this.view.forceLayout();
    },

    /**
    * showSoftDeletePopup :This function is used to show the SoftDeletePopUp when clicked on Delete button for a message
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    showSoftDeletePopup: function () {
      this.view.FlxDismiss.setVisibility(true);
      this.view.CustomPopup1.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
      this.view.CustomPopup1.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.NotificationMessages.DeleteMsg");
    },

    /**
    * showHardDeletePopup :This function is used to show the HardDelete PopUp when clicked on Delete button for a message in DeletedMessages tab
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    showHardDeletePopup: function () {
      this.view.FlxDismiss.setVisibility(true);
      this.view.CustomPopup1.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
      this.view.CustomPopup1.lblPopupMessage.text =  kony.i18n.getLocalizedString("i18n.NotificationMessages.DeleteMsg");
    },

    /**
    * labelData :This function is used to convert the html Data to normal String by removing the tags and assigning the text to a label
    * @member of {frmNotificationsAndMessagesController}
    * @param {data}   html Data 
    * @return {text} normal text after converting from html to text 
    * @throws {}
   */
    labelData: function (data) {
      var parser = new DOMParser();
      var res = parser.parseFromString(data, 'text/html');
      var html = res.body.textContent;
      var div = document.createElement("div");
      div.innerHTML = html;
      return div.innerText;

    },

    /**
    * dismissAlertsViewModel :This function is executed when the dismiss of the notification is successful
    * @member of {frmNotificationsAndMessagesController}
    * @param {viewModel}   viewModel consists of the Status whether it is success or failure
    * @return {text} normal text after converting from html to text 
    * @throws {}
   */
    dismissAlertsViewModel: function (viewModel) {
      if(viewModel.status === "success") {
        this.onSearchClick();
      } else {
        CommonUtilities.hideProgressBar(this.view);
      }
    },


    /**
    * isRead :This function is used to check whether the notification is read or not
    * @member of {frmNotificationsAndMessagesController}
    * @param {status}   status is either 0 or 1 indicating whether the  notification is read or not
    * @return {String} returns true or false depending on the value of notification is read or not
    * @throws {}
   */
    isRead: function (status) {
      if (status == 1)
        return true;
      else
        return false;
    },
    /**
    * onRowSelection :This function is used to display the details of the selected Alert on the right Hand Side
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    onRowSelection: function () {
      var scopeObj = this;
      var selectedUserNotificationId = this.view.NotficationsAndMessages.segMessageAndNotification.data[0].userNotificationId;
      var index = 0;
      this.view.NotficationsAndMessages.lblHeadingNotification.text = "";
      this.view.NotficationsAndMessages.RichText0c22ac14f53af45.text = "";
      this.view.NotficationsAndMessages.lblDateAndTime.text = "";
      this.view.NotficationsAndMessages.lblOfferLink.text = "";
      this.view.NotficationsAndMessages.imgBanner.src = "";
      this.view.NotficationsAndMessages.btnDismiss.isVisible = true;
      if (this.view.NotficationsAndMessages.segMessageAndNotification.selectedItems) {
        selectedUserNotificationId = this.view.NotficationsAndMessages.segMessageAndNotification.selectedItems[0].userNotificationId;
        index = this.view.NotficationsAndMessages.segMessageAndNotification.selectedIndex[1];
      }
      var data = this.view.NotficationsAndMessages.segMessageAndNotification.data;
      var response = this.presenter.getAlertsDetails(selectedUserNotificationId);
      this.view.NotficationsAndMessages.lblHeadingNotification.text = response.notificationSubject;
      this.view.NotficationsAndMessages.RichText0c22ac14f53af45.text = response.notificationText;
      this.view.NotficationsAndMessages.lblDateAndTime.text = CommonUtilities.getDateAndTime(response.receivedDate, CommonUtilities.getConfiguration('frontendDateFormat'));
      this.view.NotficationsAndMessages.RichText0c22ac14f53af45.isVisible = true;
      this.view.NotficationsAndMessages.txtareaNotification.isVisible = false;

      /**
       *Code to show interactive notifications based on configurations
       */
      if (CommonUtilities.getConfiguration("isInteractiveNotificationEnabled") === "true") {
        if (response.notificationActionLink !== "") {
          this.view.NotficationsAndMessages.lblOfferLink.text = response.notificationActionLink;
          this.view.NotficationsAndMessages.lblOfferLink.isVisible = true;
          var notificationActionLink = this.view.NotficationsAndMessages.lblOfferLink.text;
          if (notificationActionLink) {
            notificationActionLink = notificationActionLink.trim();
          }
          switch (notificationActionLink) {

            case "PAY BILL":
              this.view.NotficationsAndMessages.lblOfferLink.onTouchEnd = function () {
                var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
                billPayModule.presentationController.showBillPayData();
              };
              break;

            case "SEND MONEY":
              this.view.NotficationsAndMessages.lblOfferLink.onTouchEnd = function () {
                var p2pModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModule");
                p2pModule.presentationController.loadPayAPersonflx();
              };
              break;

            case "VIEW MY ACCOUNT":
              this.view.NotficationsAndMessages.lblOfferLink.onTouchEnd = function () {
                var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                accountsModule.presentationController.showAccountsDashboard();
              };
              break;

            case "TRANSFER MONEY":
              this.view.NotficationsAndMessages.lblOfferLink.onTouchEnd = function () {
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.presentationController.showTransferScreen();
              };
              break;

            default:
              this.view.NotficationsAndMessages.lblOfferLink.onTouchEnd = function () {
                window.open(response.notificationActionLink);
              };
              break;
          }
        } else {
          this.view.NotficationsAndMessages.lblOfferLink.isVisible = false;
        }
      } else {
        this.view.NotficationsAndMessages.lblOfferLink.isVisible = false;
      }
      response.imageURL = response.imageURL ? response.imageURL.trim() : "";
      if (response.imageURL !== "") {
        this.view.NotficationsAndMessages.imgBanner.src = response.imageURL;
        this.view.NotficationsAndMessages.imgBanner.isVisible = true;
      } else {
        this.view.NotficationsAndMessages.imgBanner.isVisible = false;
      }
      var prevIndex = 0;
      for (var i = 0; i < data.length; i++) {
        if (data[i].imgCurrentOne.isVisible) {
          prevIndex = i;
        }
        data[i].flxNotificationsAndMessages = {
          "skin": "sknFlxffffff"
        };
        data[i].imgCurrentOne.isVisible = false;
        data[i].imgBulletIcon.isVisible = false;
      }
      data[index].flxNotificationsAndMessages = {
        "skin": "sknFlxf7f7f7"
      };
      data[index].imgCurrentOne.isVisible = true;
      this.view.NotficationsAndMessages.segMessageAndNotification.setDataAt(data[index], index);
      this.view.NotficationsAndMessages.segMessageAndNotification.setDataAt(data[prevIndex], prevIndex);
      if (!this.isRead(response.isRead)) {
        this.presenter.updateNotificationAsRead(data[index].userNotificationId);
      }
      this.view.CustomPopup1.btnYes.onClick = function () {
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.closeDismissPopup();
        scopeObj.presenter.dismissNotification(selectedUserNotificationId);
      };
      this.view.forceLayout();
    },


    /**
    * onSearchClick :This function is executed on entering any search String in the Alerts Tab and click of "Go" or "Enter" and based on the search String the Alerts are displayed
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    onSearchClick: function () {
      var searchString = this.view.NotficationsAndMessages.txtSearch.text;
      CommonUtilities.showProgressBar(this.view);
      if(this.view.NotficationsAndMessages.imgMsgsSearch.isVisible) {
        if(searchString && searchString.trim()) {
          this.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(false);
          this.view.NotficationsAndMessages.btnSearchCancel.setVisibility(true);
          this.presenter.searchAlerts(searchString.trim());
        } else {
          this.presenter.showAlertsPage();
        }
      } else {
        this.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(true);
        this.view.NotficationsAndMessages.btnSearchCancel.setVisibility(false);
        this.view.NotficationsAndMessages.txtSearch.text = "";
        this.view.NotficationsAndMessages.flxClearSearch.setVisibility(false);
        this.presenter.showAlertsPage();
      }
    },

    /**
    * onSearchClick :This function is used to bind the search Result of the Alerts to the Segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {JSON} viewModel is a JSON which  contains data of the Alerts according to the Search String   
    * @return {} 
    * @throws {}
   */
    showSearchAlertsViewModel: function (viewModel) {
      var data = viewModel.data;
      var self = this;
      if (data.length === 0) {
        this.view.NotficationsAndMessages.btnDismiss.isVisible = false;
        if(viewModel.errorMsg) {
          this.view.NotficationsAndMessages.rtxNoSearchResults.text = viewModel.errorMsg;
        } else {
          this.view.NotficationsAndMessages.rtxNoSearchResults.text = "No search result found Please change the search criteria";
        }
        this.view.NotficationsAndMessages.flxNoSearchResult.isVisible = true;
        this.view.NotficationsAndMessages.lblHeadingNotification.text = "";
        this.view.NotficationsAndMessages.RichText0c22ac14f53af45.text = "";
        this.view.NotficationsAndMessages.lblDateAndTime.text = "";
        this.view.NotficationsAndMessages.lblOfferLink.isVisible = false;
        this.view.NotficationsAndMessages.imgBanner.isVisible = false;
        this.view.NotficationsAndMessages.segMessageAndNotification.setData([]);
        this.view.NotficationsAndMessages.segMessageAndNotification.isVisible = false;
      } else {
        this.view.NotficationsAndMessages.flxNoSearchResult.isVisible = false;
        this.view.NotficationsAndMessages.segMessageAndNotification.isVisible = true;
        //TODO: count of unread notification which is unread..
        var count = data.map(function (dataItem) {
          var unread = 1;
          if (self.isRead(dataItem.isRead)) {
            unread = 0;
          }
          return unread;
        }).reduce(function (a, b) {
          return a + b;
        }, 0);
        this.showUnreadNotificationCount(count);
        this.setNotificationSegmentData(data);
      }
      CommonUtilities.hideProgressBar(this.view);
      this.view.forceLayout();
    },


    /**
    * showRequestsViewModel :This function is exeecuted when the retrieval of the requests is Success which inturn calls the setMessagesSegmentData function which is used to bind the Requests to the Segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {JSON} viewModel is a JSON which  contains data of the Requests   
    * @return {} 
    * @throws {}
   */
    showRequestsViewModel: function (viewModel) {
      this.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages")
        }, {
          text: kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages")
      }]);
      this.view.NotficationsAndMessages.segMessageAndNotification.setData([]);
      this.view.customheader.customhamburger.activateMenu("ALERTS & MESSAGES",  "Messages");
      this.unreadMessagesCount = viewModel.unReadMessagesCount;
      this.view.NotficationsAndMessages.flxNewMessage.setVisibility(false);
      this.view.NotficationsAndMessages.btnDismiss.isVisible = true;
      this.view.NotficationsAndMessages.flxNoSearchResult.isVisible = false;
      this.view.NotficationsAndMessages.btnSendReply.text = "REPLY";
      this.view.NotficationsAndMessages.btnCancelReply.setVisibility(false);
      this.view.NotficationsAndMessages.flxImageAttachment.setVisibility(false);
      this.view.NotficationsAndMessages.lblWarningReplyMessage.setVisibility(false);
      this.view.NotficationsAndMessages.btnSendReply.onClick = this.showReplyView.bind(this);
      this.view.NotficationsAndMessages.btnSendReply.isVisible = true;
      this.view.NotficationsAndMessages.segMessageAndNotification.isVisible = true;
      this.view.NotficationsAndMessages.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
      this.view.NotficationsAndMessages.imgMsgsSearch.onTouchEnd = this.OnMessageSearchClick.bind(this);
      this.view.NotficationsAndMessages.btnSearchCancel.onClick = this.OnMessageSearchClick.bind(this);
      this.view.NotficationsAndMessages.txtSearch.onDone = this.OnMessageSearchClick.bind(this);
      this.view.NotficationsAndMessages.txtSearch.onKeyUp = this.showOrHideSearchCrossImage.bind(this);
      var searchString = this.view.NotficationsAndMessages.txtSearch.text;
      if (searchString && searchString.trim()) {
        this.OnMessageSearchClick();
      } else {
        this.setMessagesSegmentData(viewModel);
      }
    },

    /**
    * setMessagesSegmentData :This function is used to bind the Messages/Requests Data to the Segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {JSON} viewModel is a JSON which  contains data of the Requests/Messages
    * @return {} 
    * @throws {}
   */
    setMessagesSegmentData: function (viewModel) {
      var data = viewModel.data;
      this.view.NotficationsAndMessages.segMessageAndNotification.onRowClick = this.OnMessageRowSelection.bind(this);
      this.view.NotficationsAndMessages.btnNewMessage.setVisibility(true);
      this.view.NotficationsAndMessages.flxMainMessages.height = "430dp";
      this.view.NotficationsAndMessages.flxReply.setVisibility(false);
      this.view.NotficationsAndMessages.flxRightNotifications.setVisibility(false);
      this.view.NotficationsAndMessages.flxNewMessage.setVisibility(false);
      this.view.NotficationsAndMessages.flxDeletedMessagesBottom.setVisibility(false);
      this.view.NotficationsAndMessages.btnCancelReply.setVisibility(false);
      this.view.NotficationsAndMessages.lblWarningReplyMessage.setVisibility(false);
      this.view.NotficationsAndMessages.flxImageAttachment.setVisibility(false);
      this.setSkinActive(this.view.NotficationsAndMessages.flxTabs.btnMyMessages);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnNotifications);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnDeletedMessages);
      if (data && data.length > 0) {
        this.view.NotficationsAndMessages.flxRightMessages.setVisibility(true);
        this.view.NotficationsAndMessages.flxSendMessage.setVisibility(true);
        this.view.NotficationsAndMessages.flxRightMessages.flxMessagesHeader.btnDelete.setVisibility(true);
        this.view.NotficationsAndMessages.segMessageAndNotification.setVisibility(true);
        this.view.NotficationsAndMessages.flxNoSearchResult.setVisibility(false);
        var dataMap = this.processRequestsDataMap();
        data = this.processRequestsData(data);
        this.view.NotficationsAndMessages.segMessageAndNotification.widgetDataMap = dataMap;
        this.view.NotficationsAndMessages.segMessageAndNotification.setData(data);
        if (viewModel.selectedRequestId) {
          //set the selected Reqeuest in the segment
          var index = data.findIndex(function (dataItem) {
            return dataItem.requestId === viewModel.selectedRequestId;
          });
          if (index >= 0) {
            this.view.NotficationsAndMessages.segMessageAndNotification.selectedIndex = [0, index];
          }
        }
        if (!viewModel.createNewMessage) {
          this.OnMessageRowSelection();
        }
      } else {
        this.view.NotficationsAndMessages.flxRightMessages.setVisibility(false);
        this.view.NotficationsAndMessages.flxSendMessage.setVisibility(false);
        this.view.NotficationsAndMessages.flxRightMessages.flxMessagesHeader.btnDelete.setVisibility(false);
        this.view.NotficationsAndMessages.segMessageAndNotification.setVisibility(false);
        if(viewModel.errorMsg) {
          this.view.NotficationsAndMessages.rtxNoSearchResults.text = viewModel.errorMsg;
        } else {
          this.view.NotficationsAndMessages.rtxNoSearchResults.text = "No messages available at this time";    
        }
        this.view.NotficationsAndMessages.flxNoSearchResult.setVisibility(true);
        if (!viewModel.createNewMessage) {
          CommonUtilities.hideProgressBar(this.view);
        }
      }
      this.view.forceLayout();
    },

     /**
    * OnMessageRowSelection :This function is executed when we want to display the complete details of the Request selected
    * @member of {frmNotificationsAndMessagesController}
    * @param {} 
    * @return {} 
    * @throws {}
   */
    OnMessageRowSelection: function () {
      var self = this;
      this.view.NotficationsAndMessages.flxNewMessage.isVisible = false;
      this.view.NotficationsAndMessages.lblAccountRenewal.text = "";
      this.view.NotficationsAndMessages.segMessages.setData([]);
      this.view.NotficationsAndMessages.flxReply.isVisible = false;
      this.view.NotficationsAndMessages.flxSendMessage.isVisible = true;
      this.view.NotficationsAndMessages.flxRightMessages.isVisible = true;
      this.view.NotficationsAndMessages.btnNewMessage.setVisibility(true);
      if(!CommonUtilities.isCSRMode()){
      this.enableButton(this.view.NotficationsAndMessages.btnSendReply);
      } 
      CommonUtilities.showProgressBar(this.view);
      var index = 0;
      var data = this.view.NotficationsAndMessages.segMessageAndNotification.data;
      if (this.view.NotficationsAndMessages.segMessageAndNotification.selectedItems) {
        index = this.view.NotficationsAndMessages.segMessageAndNotification.selectedIndex[1];
      }
      var selectedRow = data[index];
      if (!data[0].requestId) {
        data.splice(0, 1);
        index = index - 1;
        this.view.NotficationsAndMessages.segMessageAndNotification.setData(data);
      }

      var response = this.presenter.getRequestsDetails(selectedRow.requestId);
      var prevIndex = 0;
      for (var i = 0; i < data.length; i++) {
        if (data[i].imgCurrentOne.isVisible) {
          prevIndex = i;
        }
        data[i].flxNotificationsAndMessages = {
          "skin": "sknFlxffffff"
        };
        data[i].imgCurrentOne.isVisible = false;
        data[i].imgBulletIcon.isVisible = false;
      }
      data[index].flxNotificationsAndMessages = {
        "skin": "sknFlxf7f7f7"
      };
      data[index].imgCurrentOne.isVisible = true;

      if (this.isReadMessage(response.unreadmsgs)) {
        var subject = data[index].lblSegHeading.text;
        data[index].lblSegHeading.text = subject.lastIndexOf(" \(") != -1 ? subject.substr(0, subject.indexOf(" \(")) : subject;
        this.presenter.updateMessageAsRead(data[index].requestId);
      }
      this.view.NotficationsAndMessages.segMessageAndNotification.setDataAt(data[index], index);
      this.view.NotficationsAndMessages.segMessageAndNotification.setDataAt(data[prevIndex], prevIndex);
      this.view.NotficationsAndMessages.lblAccountRenewal.text = data[index].lblSegHeading.text;
      if (selectedRow.softdeleteflag === "true") {
        if(CommonUtilities.isCSRMode()){
            this.view.NotficationsAndMessages.btnRestore.onClick = CommonUtilities.disableButtonActionForCSRMode();
            this.view.NotficationsAndMessages.btnRestore.skin = CommonUtilities.disableButtonSkinForCSRMode();
        }
        else{
        this.view.NotficationsAndMessages.btnRestore.onClick = function () {
          CommonUtilities.showProgressBar(self.view);
          self.presenter.restoreRequest(selectedRow.requestId);
        };
        }
        this.view.CustomPopup1.btnYes.onClick = function () {
          self.closeDismissPopup();
          CommonUtilities.showProgressBar(self.view);
          self.presenter.hardDeleteRequest(selectedRow.requestId);
        };
        this.view.CustomPopup1.btnNo.onClick = function () {
          self.closeDismissPopup();
        };
      } else {
        this.view.CustomPopup1.btnYes.onClick = function () {
          self.closeDismissPopup();
          CommonUtilities.showProgressBar(self.view);
          self.presenter.softDeleteRequest(selectedRow.requestId);
        };
        this.view.CustomPopup1.btnNo.onClick = function () {
          self.closeDismissPopup();
        };
      }
      this.presenter.showMessages(selectedRow.requestId);
    },

    /**
    * showUnreadMessagesCount :This function is used to display the unread messages Count in the "My Messages" tab
    * @member of {frmNotificationsAndMessagesController}
    * @param {data} data consists of the unreadMessagesCount which is to be displayed 
    * @return {} 
    * @throws {}
   */
    showUnreadMessagesCount: function (data) {
      this.unreadMessagesCount = data.unReadMessagesCount;
      this.view.NotficationsAndMessages.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
      this.updateAlertsIcon();
      this.view.forceLayout();
    },

     /**
    * updateAlertsIcon :This function is used to update the red Dot on the Alerts icon depending on whether there are any unread Messages/Notifications  
    * @member of {frmNotificationsAndMessagesController}
    * @param {}  
    * @return {} 
    * @throws {}
   */
    updateAlertsIcon: function () {
      var show = true;
      if (this.unreadMessagesCount !== undefined && this.unReadNotificationCount !== undefined) {
        if (parseInt(this.unreadMessagesCount) + parseInt(this.unReadNotificationCount) === 0) {
          show = false;
        }
      }
      kony.mvc.MDAApplication.getSharedInstance().appContext.hasUnreadMessagesOrNotifications = show;
      this.view.customheader.headermenu.lblNewMessages.setVisibility(show);
    },

    /**
    * showMessagesViewModel :This function is executed when all  the messages for the request are retrieved
    * @member of {frmNotificationsAndMessagesController}
    * @param {viewModel}  viewModel is a JSON which consists of array of all the messsages for the particular request
    * @return {} 
    * @throws {}
   */
    showMessagesViewModel: function (viewModel) {
      var widgetDataMap = this.processMessagesDataMap();
      var data = this.processMessagesData(viewModel.data);
      this.view.NotficationsAndMessages.segMessages.widgetDataMap = widgetDataMap;
      this.view.NotficationsAndMessages.segMessages.setData(data);
      CommonUtilities.hideProgressBar(this.view);
      this.view.forceLayout();
    },

    /**
    * showDeletedRequestsViewModel :This function is executed when all  the DeletedMessages  are retrieved
    * @member of {frmNotificationsAndMessagesController}
    * @param {viewModel}  viewModel is a JSON which consists of array of all the DeletedMessages 
    * @return {}
    * @throws {}
    */
    showDeletedRequestsViewModel: function (viewModel) {
      var self = this;
      self.view.NotficationsAndMessages.imgMsgsSearch.onTouchEnd = self.OnMessageDeleteSearchClick.bind(self);
      self.view.NotficationsAndMessages.btnSearchCancel.onClick = self.OnMessageDeleteSearchClick.bind(self);
      self.view.NotficationsAndMessages.txtSearch.onDone = self.OnMessageDeleteSearchClick.bind(self);
      self.view.NotficationsAndMessages.txtSearch.onKeyUp = self.showOrHideSearchCrossImage.bind(self);
      var searchString = this.view.NotficationsAndMessages.txtSearch.text.trim();
      if (searchString === "") {
        self.setDeletedMessagesSegmentData(viewModel);
      } else {
        self.OnMessageDeleteSearchClick();
      }
    },

    /**
    * setDeletedMessagesSegmentData :This function is used to bind the Deleted Messaages to the Segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {viewModel}  viewModel is a JSON which consists of array of all the DeletedMessages 
    * @return {}
    * @throws {}
    */
    setDeletedMessagesSegmentData: function (viewModel) {
      var data = viewModel.data;
      var self = this;
      this.setSkinActive(this.view.NotficationsAndMessages.flxTabs.btnDeletedMessages);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnNotifications);
      this.setSkinInActive(this.view.NotficationsAndMessages.flxTabs.btnMyMessages);
      this.view.NotficationsAndMessages.flxRightMessages.flxMessagesHeader.btnDelete.setVisibility(false);
      this.view.NotficationsAndMessages.flxNoSearchResult.setVisibility(false);
      this.view.NotficationsAndMessages.btnNewMessage.setVisibility(true);
      this.view.NotficationsAndMessages.flxMainMessages.height = "430dp";
      this.view.NotficationsAndMessages.flxReply.setVisibility(false);
      this.view.NotficationsAndMessages.flxNewMessage.setVisibility(false);
      this.view.NotficationsAndMessages.flxRightNotifications.setVisibility(false);
      this.view.NotficationsAndMessages.flxSendMessage.setVisibility(false);
      if (data && data.length > 0) {
        this.view.NotficationsAndMessages.segMessageAndNotification.onRowClick = this.OnMessageRowSelection.bind(this);
        this.view.NotficationsAndMessages.segMessageAndNotification.setVisibility(true);
        this.view.NotficationsAndMessages.flxDeletedMessagesBottom.setVisibility(true);
        this.view.NotficationsAndMessages.flxRightMessages.setVisibility(true);
        var dataMap = self.processRequestsDataMap();
        data = self.processRequestsData(data);
        this.view.NotficationsAndMessages.segMessageAndNotification.widgetDataMap = dataMap;
        this.view.NotficationsAndMessages.segMessageAndNotification.setData(data);
        this.OnMessageRowSelection();
      } else {
        this.view.NotficationsAndMessages.segMessageAndNotification.setVisibility(false);
        this.view.NotficationsAndMessages.flxDeletedMessagesBottom.setVisibility(false);
        this.view.NotficationsAndMessages.flxRightMessages.setVisibility(false);
        if(viewModel.errorMsg) {
          this.view.NotficationsAndMessages.rtxNoSearchResults.text = viewModel.errorMsg;
        } else {
          this.view.NotficationsAndMessages.rtxNoSearchResults.text = "No Records Available"; 
        }
        this.view.NotficationsAndMessages.flxNoSearchResult.setVisibility(true);
        CommonUtilities.hideProgressBar(this.view);
      }
      this.view.forceLayout();
    },

    /**
    * processRequestsData :This function is used for assigning  individual Request Data to the Segment 
    * @member of {frmNotificationsAndMessagesController}
    * @param {data}  data consists of all the details of the  requests which are to be mapped
    * @return {}
    * @throws {}
    */
    processRequestsData: function (data) {
      var self = this;
      data = data.map(function (dataItem) {
        return {
          "imgCurrentOne": {
            "src": "accounts_sidebar_blue.png",
            "isVisible": false
          },
          "imgAttachment": { //if it has attachments depending on the data
            "src": "attachment_grey.png",
            "isVisible": dataItem.totalAttachments > 0 ? true : false
          },
          "imgBulletIcon": {
            "isVisible": self.isReadMessage(dataItem.unreadmsgs)
          },
          "lblDateAndTime": {
            "text": dataItem.recentMsgDate ? CommonUtilities.getDateAndTime(dataItem.recentMsgDate, CommonUtilities.getConfiguration('frontendDateFormat')) : "" 
          },
          "lblSegDescription": {
             "text": self.labelData(dataItem.requestCreatedDate) ? self.labelData(dataItem.requestCreatedDate) : ""
          },
          "flxNotificationsAndMessages": {
            "skin": "sknFlxffffff",
            "hoverSkin": "sknFlxf7f7f7"
          },
          "lblSegHeading": {
            "text": dataItem.unreadmsgs > 0 ? dataItem.requestsubject + " (" + dataItem.unreadmsgs + ")" : dataItem.requestsubject
          },
          "requestId": dataItem.id,
          "softdeleteflag": dataItem.softdeleteflag
        };
      });
      return data;
    },

    /**
    * processRequestsDataMap :This function is used for binding the each individual Request Data to the Segment fields
    * @member of {frmNotificationsAndMessagesController}
    * @param {}  
    * @return {}
    * @throws {}
    */
    processRequestsDataMap: function () {
      return {
        "flxNotificationsAndMessages": "flxNotificationsAndMessages",
        "imgAttachment": "imgAttachment",
        "imgBulletIcon": "imgBulletIcon",
        "imgCurrentOne": "imgCurrentOne",
        "lblDateAndTime": "lblDateAndTime",
        "lblSegDescription": "lblSegDescription",
        "lblSegHeading": "lblSegHeading",
        "segNotificationsAndMessages": "segNotificationsAndMessages"
      };
    },

    /**
    * processMessagesDataMap :This function is used for binding the each individual messages Data to the Segment fields
    * @member of {frmNotificationsAndMessagesController}
    * @param {}  
    * @return {}
    * @throws {}
    */
    processMessagesDataMap: function () {
      return {
        "flxDummy": "flxDummy",
        "flxMessage": "flxMessage",
        "flxNameDate": "flxNameDate",
        "imgToolTip": "imgToolTip",
        "imgUser": "imgUser",
        "lblDate": "lblDate",
        "lblDummy": "lblDummy",
        "rtxMessage": "rtxMessage",
        "flxDocAttachment1": "flxDocAttachment1",
        "flxAttachmentName1": "flxAttachmentName1",
        "imgPDF1": "imgPDF1",
        "lblDocName1": "lblDocName1",
        "lblSize1": "lblSize1",
        "flxVerticalMiniSeparator1": "flxVerticalMiniSeparator1",
        "imgRemoveAttachment1": "imgRemoveAttachment1",
        "flxRemoveAttachment1": "flxRemoveAttachment1",
        "flxDownloadAttachment1": "flxDownloadAttachment1",
        "imgDownloadAttachment1": "imgDownloadAttachment1",
        "flxDocAttachment2": "flxDocAttachment2",
        "imgPDF2": "imgPDF2",
        "lblDocName2": "lblDocName2",
        "lblSize2": "lblSize2",
        "imgRemoveAttachment2": "imgRemoveAttachment2",
        "flxRemoveAttachment2": "flxRemoveAttachment2",
        "flxDownloadAttachment2": "flxDownloadAttachment2",
        "imgDownloadAttachment2": "imgDownloadAttachment2",
        "flxDocAttachment3": "flxDocAttachment3",
        "imgPDF3": "imgPDF3",
        "lblDocName3": "lblDocName3",
        "lblSize3": "lblSize3",
        "flxRemoveAttachment3": "flxRemoveAttachment3",
        "imgRemoveAttachment3": "imgRemoveAttachment3",
        "flxDownloadAttachment3": "flxDownloadAttachment3",
        "imgDownloadAttachment3": "imgDownloadAttachment3",
        "flxDocAttachment4": "flxDocAttachment4",
        "imgPDF4": "imgPDF4",
        "lblDocName4": "lblDocName4",
        "lblSize4": "lblSize4",
        "flxRemoveAttachment4": "flxRemoveAttachment4",
        "imgRemoveAttachment4": "imgRemoveAttachment4",
        "flxDownloadAttachment4": "flxDownloadAttachment4",
        "imgDownloadAttachment4": "imgDownloadAttachment4",
        "flxDocAttachment5": "flxDocAttachment5",
        "imgPDF5": "imgPDF5",
        "lblDocName5": "lblDocName5",
        "lblSize5": "lblSize5",
        "flxRemoveAttachment5": "flxRemoveAttachment5",
        "imgRemoveAttachment5": "imgRemoveAttachment5",
        "flxDownloadAttachment5": "flxDownloadAttachment5",
        "imgDownloadAttachment5": "imgDownloadAttachment5",
      };
    },

    /**
    * processMessagesData :This function is used for setting the messages data to the segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {data}  data consists of the array of the messages 
    * @return {}
    * @throws {}
    */
    processMessagesData: function (data) {
      var self = this;
      var username = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      data = data.map(function (dataItem) {
        var totalAttachments = parseInt(dataItem.totalAttachments);
        var hasFirstAttachment = self.hasFirstAttachment(totalAttachments);
        var hasSecondAttachment = self.hasSecondAttachment(totalAttachments);
        var hasThirdAttachment = self.hasThirdAttachment(totalAttachments);
        var hasFourthAttachment = self.hasFourthAttachment(totalAttachments);
        var hasFifthAttachment = self.hasFifthAttachment(totalAttachments);
        return {
          "requestId": dataItem.CustomerRequest_id,
          "template": dataItem.createdby != username ? "flxMessagesLeft" : "flxMessagesRight",
          "rtxMessage": dataItem.MessageDescription,
          "lblDate": CommonUtilities.getDateAndTime(dataItem.lastmodifiedts, CommonUtilities.getConfiguration('frontendDateFormat')),
          "flxDocAttachment1": {
            "isVisible": hasFirstAttachment ? true : false
          },
          "lblDocName1": hasFirstAttachment ? dataItem.attachments[0].Name : "",
          "lblSize1": hasFirstAttachment ? '(' + dataItem.attachments[0].Size.trim() + "KB" + ')' : "",
          "imgPDF1": hasFirstAttachment ? self.getImageByType(dataItem.attachments[0].type) : "pdf_image.png",
          "flxRemoveAttachment1": {
            "isVisible": false
          },
          "flxDownloadAttachment1": {
            "isVisible": true,
            "onClick": function () {
              var mediaId = dataItem.attachments[0].media_Id;
              var fileName = dataItem.attachments[0].Name;
              self.presenter.downloadAttachment(mediaId, fileName);
            }
          },
          "imgDownloadAttachment1": {
            "src": "download_blue.png",
          },
          "flxDocAttachment2": {
            "isVisible": hasSecondAttachment ? true : false
          },
          "lblDocName2": hasSecondAttachment ? dataItem.attachments[1].Name : "",
          "lblSize2": hasSecondAttachment ? '(' + dataItem.attachments[1].Size.trim() + "KB" + ')' : "",
          "imgPDF2": hasSecondAttachment ? self.getImageByType(dataItem.attachments[1].type) : "pdf_image.png",
          "flxRemoveAttachment2": {
            "isVisible": false
          },
          "flxDownloadAttachment2": {
            "isVisible": true,
            "onClick": function () {
              var mediaId = dataItem.attachments[1].media_Id;
              var fileName = dataItem.attachments[1].Name;
              self.presenter.downloadAttachment(mediaId, fileName);
            }
          },
          "imgDownloadAttachment2": {
            "src": "download_blue.png",
            "isVisible": true
          },
          "flxDocAttachment3": {
            "isVisible": hasThirdAttachment ? true : false
          },
          "lblDocName3": hasThirdAttachment ? dataItem.attachments[2].Name : "",
          "lblSize3": hasThirdAttachment ? '(' + dataItem.attachments[2].Size.trim() + "KB" + ')' : "",
          "imgPDF3": hasThirdAttachment ? self.getImageByType(dataItem.attachments[2].type) : "pdf_image.png",
          "flxRemoveAttachment3": {
            "isVisible": false
          },
          "flxDownloadAttachment3": {
            "isVisible": true,
            "onClick": function () {
              var mediaId = dataItem.attachments[2].media_Id;
              var fileName = dataItem.attachments[2].Name;
              self.presenter.downloadAttachment(mediaId, fileName);
            }
          },
          "imgDownloadAttachment3": {
            "src": "download_blue.png",
            "isVisible": true
          },
          "flxDocAttachment4": {
            "isVisible": hasFourthAttachment ? true : false
          },
          "lblDocName4": hasFourthAttachment ? dataItem.attachments[3].Name : "",
          "lblSize4": hasFourthAttachment ? '(' + dataItem.attachments[3].Size.trim() + "KB" + ')' : "",
          "imgPDF4": hasFourthAttachment ? self.getImageByType(dataItem.attachments[3].type) : "pdf_image.png",
          "flxRemoveAttachment4": {
            "isVisible": false
          },
          "flxDownloadAttachment4": {
            "isVisible": true,
            "onClick": function () {
              var mediaId = dataItem.attachments[3].media_Id;
              var fileName = dataItem.attachments[3].Name;
              self.presenter.downloadAttachment(mediaId, fileName);
            }
          },
          "imgDownloadAttachment4": {
            "src": "download_blue.png",
            "isVisible": true
          },
          "flxDocAttachment5": {
            "isVisible": hasFifthAttachment ? true : false
          },
          "lblDocName5": hasFifthAttachment ? dataItem.attachments[4].Name : "",
          "lblSize5": hasFifthAttachment ? '(' + dataItem.attachments[4].Size.trim() + "KB" + ')' : "",
          "imgPDF5": hasFifthAttachment ? self.getImageByType(dataItem.attachments[4].type) : "pdf_image.png",
          "flxRemoveAttachment5": {
            "isVisible": false
          },
          "flxDownloadAttachment5": {
            "isVisible": true,
            "onClick": function () {
              var mediaId = dataItem.attachments[4].media_Id;
              var fileName = dataItem.attachments[4].Name;
              self.presenter.downloadAttachment(mediaId, fileName);
            }
          },
          "imgDownloadAttachment5": {
            "src": "download_blue.png",
            "isVisible": true
          },
          "imgUser": dataItem.createdby != username ? "bank_circle_icon_mod.png" : "user_image.png",
          "imgToolTip": dataItem.createdby != username ? "reply_arrowtip.png" : "reply_arrowtip_right.png"
        };
      });
      return data;
    },

     /**
    * hasFirstAttachment :This function is used to check whether there is first Attachment or not
    * @member of {frmNotificationsAndMessagesController}
    * @param {totalAttachments}  totalAttachements  is the total number of Attachments which are uploaded
    * @return {boolean} retuns true if there is an attachment or false if there is no first Attachment
    * @throws {}
    */
    hasFirstAttachment: function (totalAttachments) {
      var has = false;
      has = totalAttachments > 0 ? true : false;
      return has;
    },

     /**
    * hasSecondAttachment :This function is used to check whether there is second Attachment or not
    * @member of {frmNotificationsAndMessagesController}
    * @param {totalAttachments}  totalAttachements  is the total number of Attachments which are uploaded
    * @return {boolean} retuns true if there is an attachment or false if there is no second Attachment
    * @throws {}
    */
    hasSecondAttachment: function (totalAttachments) {
      var has = false;
      has = totalAttachments > 1 ? true : false;
      return has;
    },
      /**
    * hasThirdAttachment :This function is used to check whether there is third Attachment or not
    * @member of {frmNotificationsAndMessagesController}
    * @param {totalAttachments}  totalAttachements  is the total number of Attachments which are uploaded
    * @return {boolean} retuns true if there is an attachment or false if there is no third Attachment
    * @throws {}
    */
    hasThirdAttachment: function (totalAttachments) {
      var has = false;
      has = totalAttachments > 2 ? true : false;
      return has;
    },

    /**
    * hasFourthAttachment :This function is used to check whether there is fourth Attachment or not
    * @member of {frmNotificationsAndMessagesController}
    * @param {totalAttachments}  totalAttachements  is the total number of Attachments which are uploaded
    * @return {boolean} retuns true if there is an attachment or false if there is no fourth Attachment
    * @throws {}
    */
    hasFourthAttachment: function (totalAttachments) {
      var has = false;
      has = totalAttachments > 3 ? true : false;
      return has;
    },

    /**
    * hasFifthAttachment :This function is used to check whether there is fifth Attachment or not
    * @member of {frmNotificationsAndMessagesController}
    * @param {totalAttachments}  totalAttachements  is the total number of Attachments which are uploaded
    * @return {boolean} retuns true if there is an attachment or false if there is no fifth Attachment
    * @throws {}
    */
    hasFifthAttachment: function (totalAttachments) {
      var has = false;
      has = totalAttachments > 4 ? true : false;
      return has;
    },

    /**
    * getImageByType :This function is used to get the image depending on the type of the document attached
    * @member of {frmNotificationsAndMessagesController}
    * @param {type}  type of the Attachment can be application/pdf, text/plain, image/jpeg, application/msword(DOC) or application/vnd.openxmlformats-officedocument.wordprocessingml.document(DOCX)
    * @return {image} returns the appropriate image based on type of the document
    * @throws {}
    */
    getImageByType: function (type) {
      var image;
      switch (type) {
        case "application/pdf":
          image = "pdf_image.png";
          break;
        case "text/plain":
          image = "txt_image.png";
          break;
        case "image/jpeg":
          image = "jpeg_image.png";
          break;
        case "application/msword":
          image = "doc_image.png";
          break;
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
          image = "docx_image.png";
          break;
         case "image/png":
              image = "png_image.png";
      }
      return image;
    },

    /**
    * getBrowseFilesConfig :This function is used to get the browser files config like which type of files to be uploaded
    * @member of {frmNotificationsAndMessagesController}
    * @param {}  
    * @return {config} returns the documents attached
    * @throws {}
    */
    getBrowseFilesConfig: function () {
      var config = {
        selectMultipleFiles: true,
        filter: ["image/png","application/msword", "image/jpeg", "application/pdf", "text/plain", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]
      };
      return config;
    },

    /**
    * replyBrowseFiles :This function displays the error message when more than five files are attached or invokes the konyAPI to browse the files
    * @member of {frmNotificationsAndMessagesController}
    * @param {}  
    * @return {}
    * @throws {}
    */
    replyBrowseFiles: function () {
      var config = this.getBrowseFilesConfig();
      this.view.NotficationsAndMessages.lblWarningReplyMessage.setVisibility(false);
      kony.io.FileSystem.browse(config, this.replyBrowseFilesCallback);
    },

    /**
    * browseFiles :This function is used for opening the browser for uploading the files on click of the Attachment icon
    * @member of {frmNotificationsAndMessagesController}
    * @param {}  
    * @return {}
    * @throws {}
    */
    browseFiles: function () {
      var config = this.getBrowseFilesConfig();
      this.view.NotficationsAndMessages.lblWarningNewMessage.setVisibility(false);
      kony.io.FileSystem.browse(config, this.browseFilesCallback);
    },

    /**
    * browseFilesCallback :This function executes once the files are uploded so that  the uploaded files are binded to the Segment while creating a new Message
    * @member of {frmNotificationsAndMessagesController}
    * @param {event,files}   files is the data of the files which are uploaded
    * @return {}
    * @throws {}
    */
    browseFilesCallback: function (event, files) {
      this.bindFilesToSegment(files, this.view.NotficationsAndMessages.segAttachment, this.view.NotficationsAndMessages.lblWarningNewMessage);
    },

    /**
    * replyBrowseFilesCallback :This function executes once the files are uploded so that  the uploaded files are binded to the Segment while replying to the request
    * @member of {frmNotificationsAndMessagesController}
    * @param {event,files}   files is the data of the files which are uploaded
    * @return {}
    * @throws {}
    */
    replyBrowseFilesCallback: function (event, files) {
      this.bindFilesToSegment(files, this.view.NotficationsAndMessages.segAttachmentRightMessage, this.view.NotficationsAndMessages.lblWarningReplyMessage);
    },

     isFileTypeSupported: function(files) {
                filetype = files[0].file.type;
                if (filetype == "image/png" || filetype == "application/msword" || filetype == "image/jpeg" || filetype == "application/pdf" || filetype == "text/plain" || filetype == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") return true;
            return false;
        },
     /**
    }
    * isFileSizeExceeds :This function is used to check if the size of the file exceeds more than 1MB
    * @member of {frmNotificationsAndMessagesController}
    * @param {files}   JSON which consists of the data of the files
    * @return {boolean} true if the size of the file exceeds false if the file size is less than 1MB
    * @throws {}
    */
    isFileSizeExceeds: function (files) {
      var temp = files.filter(function (file) {
        return file.size > 1048576;
      });
      return temp.length > 0;
    },

     /**
    * isFileAlreadyAdded :This function is used to check if the file is already added or not 
    * @member of {frmNotificationsAndMessagesController}
    * @param {files}   JSON which consists of the data of the files
    * @return {boolean} true if the same file is added again or  false if the new file is added
    * @throws {}
    */
    isFileAlreadyAdded: function (files) {
      var temp = this.fileObject.filter(function (file) {
        return files.filter(function (f) {
          return f.name == file.name;
        }).length > 0;
      });
      return temp.length > 0;
    },

    /**
    * bindFilesToSegment :This function binds the attachment data to the segment 
    * @member of {frmNotificationsAndMessagesController}
    * @param {files , segment,warningLabel}  files is a  JSON which consists of the data of the files attached ,segment for which the files data is to be binded and the warning label which shows different kinds o warnings
    * @return {} 
    * @throws {}
    */
    bindFilesToSegment: function (files, segment, warningLabel) {
      var self = this;
      this.fileObject = this.fileObject || [];
      if (this.fileObject.length + files.length > 5) {
        warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.Maximum5AttachmentsAllowed");
        warningLabel.setVisibility(true);
        return;
      } else if (this.isFileSizeExceeds(files)) {
        warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.SizeExceeds1MB");
        warningLabel.setVisibility(true);
        return;
      } else if (this.isFileAlreadyAdded(files)) {
        warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.AlreadyFileAdded");
        warningLabel.setVisibility(true);
        return;
      }
      else if(!this.isFileTypeSupported(files)){
         warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.InvalidFileType");
        warningLabel.setVisibility(true);
        return;
      } else {
        if (files && files.length > 0) {
          files.forEach(function (item) {
            self.fileObject.push(item);
          });
          var dataToAdd = files.map(function (dataItem) {
            return {
              "lblDocSize": "(" + dataItem.file.size + " Bytes)",
              "imgPDF": self.getImageByType(dataItem.file.type),
              "flxRemoveAttachment": {
                "onClick": function () {
                  var index = segment.selectedIndex;
                  var sectionIndex = index[0];
                  var rowIndex = index[1];
                  segment.removeAt(rowIndex, sectionIndex);
                  self.fileObject.splice(rowIndex, 1);
                  self.view.forceLayout();
                  self.view.NotficationsAndMessages.flxReplyMessageButtons.setFocus(true);
                }
              },
              "imgRemoveAttachment": {
                "src": "icon_close_grey.png"
              },
              "lblDOcName": dataItem.file.name
            };
          });
          var data = segment.data;
          if (data && data.length > 0) {
            dataToAdd.forEach(function (item) {
              data.push(item);
            });
          } else {
            data = dataToAdd;
          }
          var dataMap = {
            "lblDocSize": "lblDocSize",
            "flxAddAttachmentMain": "flxAddAttachmentMain",
            "flxAttachmentName": "flxAttachmentName",
            "flxDocAttachment": "flxDocAttachment",
            "flxRemoveAttachment": "flxRemoveAttachment",
            "flxVerticalMiniSeparator": "flxVerticalMiniSeparator",
            "imgPDF": "imgPDF",
            "imgRemoveAttachment": "imgRemoveAttachment",
            "lblDOcName": "lblDOcName"
          };
          segment.widgetDataMap = dataMap;
          segment.setData(data);
        }
      }
      this.view.forceLayout();
      this.view.NotficationsAndMessages.flxReplyHeader.setFocus(true);
      this.view.NotficationsAndMessages.flxReplyMessageButtons.setFocus(true);
    },

    /**
    * isReadMessage :This function checks whether there are any unread Messages 
    * @member of {frmNotificationsAndMessagesController}
    * @param {Status} status indicates the number of unread messages 
    * @return {boolean}  returns true if the count of the unread messages is greater than zero or false if there are no unread messages
    * @throws {}
    */
    isReadMessage: function (status) {
      if (status > 0) return true;
      else return false;
    },

    /**
    * updateMessageAsReadSuccessViewModel :This function is executed when the messages are  marked as read
    * @member of {frmNotificationsAndMessagesController}
    * @param {readCount}  readCount which represents the number of messages read
    * @return {} 
    * @throws {}
   */
    updateMessageAsReadSuccessViewModel: function (readCount) {
      this.unreadMessagesCount = this.unreadMessagesCount - readCount;
      this.view.NotficationsAndMessages.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
      if (this.unReadNotificationCount === 0 && this.unreadMessagesCount === 0) {
        this.view.customheader.headermenu.lblNewMessages.isVisible = false;
      } else {
        this.view.customheader.headermenu.lblNewMessages.isVisible = true;
      }
      this.view.forceLayout();
    },

    /**
    * OnMessageSearchClick :This function is executed on entering any search String in the My Messages Tab and click of "Go" or "Enter" and based on the search String the Requests are displayed.The search happens based on the subject field
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    OnMessageSearchClick: function () {
      CommonUtilities.showProgressBar(this.view);
      var searchString = this.view.NotficationsAndMessages.txtSearch.text;
      if(this.view.NotficationsAndMessages.imgMsgsSearch.isVisible) {
        if(searchString && searchString.trim()) {
          this.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(false);
          this.view.NotficationsAndMessages.btnSearchCancel.setVisibility(true);
          this.presenter.searchRequest(searchString);
        } else {
          this.presenter.showRequests();
        } 
      } else {
        this.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(true);
        this.view.NotficationsAndMessages.btnSearchCancel.setVisibility(false);
        this.view.NotficationsAndMessages.txtSearch.text = "";
        this.view.NotficationsAndMessages.flxClearSearch.setVisibility(false);
        this.presenter.showRequests();
      }
    },

     /**
    * showSearchRequestsViewModel :This function is used to bind the search Result of the Requests to the Segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {JSON} viewModel is a JSON which  contains data of the Requests according to the Search String   
    * @return {} 
    * @throws {}
   */
    showSearchRequestsViewModel: function (viewModel) {
      var data = viewModel.data;
      if (data.length === 0) {
        this.view.NotficationsAndMessages.btnDismiss.isVisible = false;
        this.view.NotficationsAndMessages.rtxNoSearchResults.text = "No search result found Please change the search criteria";
        this.view.NotficationsAndMessages.flxNoSearchResult.isVisible = true;
        this.view.NotficationsAndMessages.lblAccountRenewal.text = "";
        this.view.NotficationsAndMessages.segMessages.setData([]);
        this.view.NotficationsAndMessages.segMessageAndNotification.setData([]);
        this.view.NotficationsAndMessages.btnDelete.isVisible = false;
        this.view.NotficationsAndMessages.btnSendReply.isVisible = false;
        this.view.NotficationsAndMessages.btnCancelReply.setVisibility(false);
        this.view.NotficationsAndMessages.lblWarningReplyMessage.setVisibility(false);
        this.view.NotficationsAndMessages.flxImageAttachment.setVisibility(false);
        this.view.NotficationsAndMessages.segMessageAndNotification.isVisible = false;
        CommonUtilities.hideProgressBar(this.view);
      } else {
        this.unreadMessagesCount = viewModel.unreadSearchMessagesCount;
        this.view.NotficationsAndMessages.btnDismiss.isVisible = true;
        this.view.NotficationsAndMessages.btnDelete.isVisible = true;
        this.view.NotficationsAndMessages.flxNoSearchResult.isVisible = false;
        this.view.NotficationsAndMessages.segMessageAndNotification.isVisible = true;
        this.view.NotficationsAndMessages.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
        this.setMessagesSegmentData(viewModel);
      }
    },

   /**
    * OnMessageDeleteSearchClick :This function is executed on entering any search String in the DeletedMessages Tab and click of "Go" or "Enter" and based on the search String the Requests are displayed.The search happens based on the subject field
    * @member of {frmNotificationsAndMessagesController}
    * @param {}   
    * @return {} 
    * @throws {}
   */
    OnMessageDeleteSearchClick: function () {
      CommonUtilities.showProgressBar(this.view);
      var searchString = this.view.NotficationsAndMessages.txtSearch.text;
      if(this.view.NotficationsAndMessages.imgMsgsSearch.isVisible) {
        if(searchString && searchString.trim()) {
          this.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(false);
          this.view.NotficationsAndMessages.btnSearchCancel.setVisibility(true);
          this.presenter.searchDeletedRequests(searchString);
        } else {
          this.presenter.showDeletedRequests();
        } 
      } else {
        this.view.NotficationsAndMessages.imgMsgsSearch.setVisibility(true);
        this.view.NotficationsAndMessages.btnSearchCancel.setVisibility(false);
        this.view.NotficationsAndMessages.txtSearch.text = "";
        this.view.NotficationsAndMessages.flxClearSearch.setVisibility(false);
        this.presenter.showDeletedRequests();
      }
    },

     /**
    * showSearchDeletedRequestsViewModel :This function is used to bind the search Result of the Requests to the Segment
    * @member of {frmNotificationsAndMessagesController}
    * @param {JSON} viewModel is a JSON which  contains data of the Requests according to the Search String   
    * @return {} 
    * @throws {}
   */
    showSearchDeletedRequestsViewModel: function (viewModel) {
      var data = viewModel.data;
      if (data.length === 0) {
        this.view.NotficationsAndMessages.segMessageAndNotification.setVisibility(false);
        this.view.NotficationsAndMessages.flxDeletedMessagesBottom.setVisibility(false);
        this.view.NotficationsAndMessages.flxRightMessages.setVisibility(false);
        this.view.NotficationsAndMessages.rtxNoSearchResults.text = "No Search Result Available";
        this.view.NotficationsAndMessages.lblAccountRenewal.text = "";
        this.view.NotficationsAndMessages.segMessages.setData([]);
        this.view.NotficationsAndMessages.flxNoSearchResult.setVisibility(true);
        CommonUtilities.hideProgressBar(this.view);
      } else {
        this.view.NotficationsAndMessages.flxNoSearchResult.setVisibility(false);
        this.setDeletedMessagesSegmentData(viewModel);
      }
    }
  };
});