define(['CommonUtilities'],function(CommonUtilities){
  return{
  shouldUpdateUI: function (viewModel) {
    return viewModel !== undefined && viewModel !== null;
  },
  /**
   * Function to make changes to UI
   * Parameters: surveyViewModel {Object}
   */
  willUpdateUI: function (surveyViewModel) {
    if (surveyViewModel.showProgressBar) {
      CommonUtilities.showProgressBar(this.view);
    }
    else if (surveyViewModel.hideProgressBar) {
      CommonUtilities.hideProgressBar(this.view);
    }
    if (surveyViewModel.onServerDownError) {
      this.showServerDownForm(surveyViewModel.onServerDownError);
    }
    if (surveyViewModel.preLoginView) {
      this.showPreLoginView();
    }
    if (surveyViewModel.postLoginView) {
      this.showPostLoginView();
    }
    if (surveyViewModel.sideMenu) {
      this.updateHamburgerMenu(surveyViewModel.sideMenu);
    }
    if (surveyViewModel.topBar) {
      this.updateTopBar(surveyViewModel.topBar);
    }
    if (surveyViewModel.surveyQuestion) {
      this.setSurveyQuestionSegmentData(surveyViewModel.surveyQuestion.data.questions);
    }
    if (surveyViewModel.quetionsWithAnswers) {
      this.showSurveyAnswer(surveyViewModel.quetionsWithAnswers);
    }
    this.AdjustScreen();
  },

  /**
   * Methoid to handle Server errors.
   * Will navigate to serverdown page.
   */
  showServerDownForm: function (onServerDownError) {
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authModule.presentationController.navigateToServerDownScreen();
  },
  /**
   * Initialize HamburgerMenu
   * @param: sideMenuModel
   */
  updateHamburgerMenu: function (sideMenuModel) {
    this.view.customheader.initHamburger(sideMenuModel);
  },
  /**
   * Initialize TopBar
   * @param: topBarModel
   */
  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  },
	/**
     * Survey Pre-Login View UI 
     */
  showPreLoginView: function () {
    this.view.flxHeaderPreLogin.setVisibility(true);
    this.view.flxHeaderPostLogin.setVisibility(false);
  },
  /**
   * Survey Post-Login View UI 
   */
  showPostLoginView: function () {
    this.view.flxHeaderPreLogin.setVisibility(false);
    this.view.flxHeaderPostLogin.setVisibility(true);
  },







  //UI Code
  registerAction:function(){
	var scopeObj=this;
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      scopeObj.view.CustomPopupLogout.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopupLogout.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeaderPostLogin.frame.height + scopeObj.view.flxMainContainer.frame.height + scopeObj.view.flxFooter.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CustomPopupLogout.btnYes.onClick = function () {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopupLogout.btnNo.onClick = function () {
      scopeObj.view.flxLogout.left = "-100%";
    }
    this.view.CustomPopupLogout.flxCross.onClick = function () {
      scopeObj.view.flxLogout.left = "-100%";
    }
	this.view.btnLogin.onClick = function () {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.showLoginScreen();
    };
  },
   postShowCustomerFeedbackSurvey:function(){
      this.AdjustScreen();
   },
   //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  },       
 
  preShowCustomerFeedbackSurvey: function () {
	this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
	this.registerAction();
  this.view.FeedbackSurvey.confirmButtons.btnConfirm.skin = "sknbtnLatoffffff15px";
  this.view.FeedbackSurvey.confirmButtons.btnConfirm.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
  this.view.FeedbackSurvey.confirmButtons.btnConfirm.focusSkin = "sknBtnHoverLatoFFFFFF15Px";  
  this.view.FeedbackSurvey.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
  this.view.FeedbackSurvey.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.CustomerFeedback.Submit");
  this.view.FeedbackSurvey.confirmButtons.btnConfirm.onClick = this.btnSubmitAction;
	this.view.FeedbackSurvey.confirmButtons.btnModify.onClick=this.btnAckDoneAction;
  this.view.btnDone.onClick=this.btnAckDoneAction;
  },
  showRatingAction: function (val) {
    for (var i = 1; i <= val; i++) {
      this.view.FeedbackSurvey["imgRating" + i].src = "circle_blue_filled.png";
    }
    for (i = (val + 1); i <= 5; i++) {
      this.view.FeedbackSurvey["imgRating" + i].src = "circle_unfilled.png";
    }
    this.enableButton(this.view.FeedbackSurvey.confirmButtons.btnConfirm);
    this.view.forceLayout();
  },
  btnAckDoneAction: function () {  
    this.presenter.surveyDone();
  },
  btnSubmitAction: function () {
    var resAnswer = {};
    var disAnswers = {};
    var allQuest = kony.application.getCurrentForm().FeedbackSurvey.segSurveyQuestion1.data;
    var displayAnswer = [];
    for (var i = 0; i < allQuest.length; i++) {
      var quest = allQuest[i];
      if (quest.template === "flxRowSurvey") {
        resAnswer[quest.questionid] = quest.selectedRating;
      } else if (quest.template === "flxSurveyQuestion2") {
        resAnswer[quest.questionid] = this.getSelectedCheckBoxValue(quest);
      } else if (quest.template === "flxSurveyQuestion3") {
        resAnswer[quest.questionid] = quest.txtareaUserAdditionalComments;
      } else {
        return false;
      }
    }
    this.presenter.showSurveyAnswer(resAnswer);
  },

  showSurveyAnswer: function (data) {
    this.view.flxFeedbackSurveyContainer.setVisibility(false);
    this.view.flxFeedbackAcknowledgement.setVisibility(true);
    var dataMap = {
      "flxRowFeedbackSurveyQuestion": "flxRowFeedbackSurveyQuestion",
      "flxSurveyQuestionWrapper": "flxSurveyQuestionWrapper",
      "flxQuestion1": "flxQuestion1",
      "lblQuestion1": "lblQuestion1",
      "lblQuestionNo1": "lblQuestionNo1",
      "lblAnswer1": "lblAnswer1"
    };
    var numCounter = 1;
    var surveyData = data.map(function (dataItem) {
      return {
        "lblQuestionNo1": numCounter++,
        "lblQuestion1": dataItem.question,
        "lblAnswer1": dataItem.answerString,
        "template": "flxRowFeedbackSurveyQuestion"
      }
    });
    this.view.segSurveyQuestion.widgetDataMap = dataMap;
    this.view.segSurveyQuestion.setData(surveyData);
    this.view.forceLayout();
  },

  getSelectedCheckBoxValue: function (quest) {
    var selectedAns = [];
    for (var i = 1; quest["imgcheckbox" + i] != undefined; i++) {
      if (quest["imgcheckbox" + i] === "checked_box.png") {
        selectedAns.push(i - 1)
      }
    }
    return selectedAns;
  },

  btnCancelAction: function () {
    if (this.view.FeedbackSurvey.flxRating1.src === "circle_blue_filled.png" || this.view.FeedbackSurvey.flxRating2.src === "circle_blue_filled.png" || this.view.FeedbackSurvey.flxRating3.src === "circle_blue_filled.png" || this.view.FeedbackSurvey.flxRating4.src === "circle_blue_filled.png" || this.view.FeedbackSurvey.flxRating5.src === "circle_blue_filled.png") {
      this.view.FeedbackSurvey.flxRating1.src = "circle_unfilled.png";
      this.view.FeedbackSurvey.flxRating2.src = "circle_unfilled.png";
      this.view.FeedbackSurvey.flxRating3.src = "circle_unfilled.png";
      this.view.FeedbackSurvey.flxRating4.src = "circle_unfilled.png";
      this.view.FeedbackSurvey.flxRating5.src = "circle_unfilled.png";
      this.view.forceLayout();
    }
    else {
      var navObj = new kony.mvc.Navigation("frmLogin");
      navObj.navigate();
    }
  },
  toggleCheckBox: function (imgCheckBox) {
    if (imgCheckBox.src === "unchecked_box.png") {
      imgCheckBox.src = "checked_box.png";
    } else {
      imgCheckBox.src = "unchecked_box.png";
    }
  },
  /**
   *  Disable button.
   */
  disableButton: function (button) {
    button.setEnabled(false);
    button.skin = "sknBtnBlockedLatoFFFFFF15Px";
    button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
    button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
  },
  /**
   * Enable button.
   */
  enableButton: function (button) {
    button.setEnabled(true);
    button.skin = "sknbtnLatoffffff15px";
    button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
    button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
  },
  setSurveyQuestionSegmentData: function (data) {
    this.view.flxFeedbackSurveyContainer.setVisibility(true);
    this.view.flxFeedbackAcknowledgement.setVisibility(false);
    var dataMap = {
      "flxRating1": "flxRating1",
      "flxRating2": "flxRating2",
      "flxRating3": "flxRating3",
      "flxRating4": "flxRating4",
      "flxRating5": "flxRating5",
      "flxRatingimg": "flxRatingimg",
      "flxRowSurvey": "flxRowSurvey",
      "flxSurveyQuestion1Wrapper": "flxSurveyQuestion1Wrapper",
      "flxaddress": "flxaddress",
      "imgRating1": "imgRating1",
      "imgRating2": "imgRating2",
      "imgRating3": "imgRating3",
      "imgRating4": "imgRating4",
      "imgRating5": "imgRating5",
      "lblRateYourExpeience": "lblRateYourExpeience",
      "lblVeryEasy": "lblVeryEasy",
      "lblVeryHard": "lblVeryHard",

      "flxNotificationsmsgs": "flxNotificationsmsgs",
      "flxSecurityQuestionSetting": "flxSecurityQuestionSetting",
      "flxSurveyQuestion2": "flxSurveyQuestion2",
      "flxSurveyQuestion2Wrapper": "flxSurveyQuestion2Wrapper",
      "flxTransfers": "flxTransfers",
      "flxbillpay": "flxbillpay",
      "flxcheckbox1": "flxcheckbox1",
      "flxcheckbox2": "flxcheckbox2",
      "flxcheckbox3": "flxcheckbox3",
      "flxcheckbox4": "flxcheckbox4",
      "imgcheckbox1": "imgcheckbox1",
      "imgcheckbox2": "imgcheckbox2",
      "imgcheckbox3": "imgcheckbox3",
      "imgcheckbox4": "imgcheckbox4",
      "lblAddYourComments": "lblAddYourComments",
      "lblBillpay": "lblBillpay",
      "lblSecurityQuestionSettings": "lblSecurityQuestionSettings",
      "lblTranfers": "lblTranfers",
      "lblnotificationsmsgs": "lblnotificationsmsgs",

      "flxSurveyQuestion3": "flxSurveyQuestion3",
      "flxSurveyQuestion3Wrapper": "flxSurveyQuestion3Wrapper",
      "flxUserFeedback": "flxUserFeedback",
      "lblQuestion": "lblQuestion",
      "txtareaUserAdditionalComments": "txtareaUserAdditionalComments"
    };
    var surveyData = [];
    if (data.length > 0) {
      surveyData = data.map(function (dataItem) {
        var response;
        if (dataItem.inputType === "rating") {
          response = {
            "imgRating1": "circle_unfilled.png",
            "imgRating2": "circle_unfilled.png",
            "imgRating3": "circle_unfilled.png",
            "imgRating4": "circle_unfilled.png",
            "imgRating5": "circle_unfilled.png",
            "lblRateYourExpeience": dataItem.question,
            "lblVeryEasy": "VERY EASY",
            "lblVeryHard": "VERY HARD",

            "template": "flxRowSurvey"
          }
        }
        else if (dataItem.inputType === "mcq") {
          response = {
            "imgcheckbox1": "unchecked_box.png",
            "imgcheckbox2": "unchecked_box.png",
            "imgcheckbox3": "unchecked_box.png",
            "imgcheckbox4": "unchecked_box.png",
            "lblAddYourComments": dataItem.question,
            "lblBillpay": {
              "text": dataItem.questionInput[1],
              "isVisible": true,
            },
            "lblSecurityQuestionSettings": {
              "text": dataItem.questionInput[2],
              "isVisible": true,
            },
            "lblTranfers": {
              "text": dataItem.questionInput[0],
              "isVisible": dataItem.questionInput[0] ? true : false,
            },
            "lblnotificationsmsgs": {
              "text": dataItem.questionInput[3],
              "isVisible": dataItem.questionInput[3] ? true : false,
            },
            "template": "flxSurveyQuestion2"
          }
        }
        else if (dataItem.inputType === "text") {
          response = {
            "flxSurveyQuestion3": "flxSurveyQuestion3",
            "flxSurveyQuestion3Wrapper": "flxSurveyQuestion3Wrapper",
            "flxUserFeedback": "flxUserFeedback",
            "lblQuestion": dataItem.question,
            "txtareaUserAdditionalComments": " ",
            "template": "flxSurveyQuestion3"
          }
        }
        response.questionid = dataItem.questionid;
        return response;
      });
    }
    this.view.FeedbackSurvey.segSurveyQuestion1.widgetDataMap = dataMap;
    this.view.FeedbackSurvey.segSurveyQuestion1.setData(surveyData);
    this.view.forceLayout();
  }, 
  }
});