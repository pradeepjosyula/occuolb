define(['CommonUtilities','CommonUtilitiesOCCU'],function(CommonUtilities, CommonUtilitiesOCCU){

  return{

    /**
         * showUserNamesBasedOnlength :The function is used to show the list of masked usernames saved when the user clicks on the textbox if the value in the textbox is empty,once the user enters any characters the list is hidden
         * @member of {frmLoginController}
         * @param {}  
         * @return {} 
         * @throws {}
         */
    showUserNamesBasedOnlength: function() {
      var self = this;
      if (self.view.main.flxUserName.skin == "sknBorderFF0101") {
        self.credentialsMissingUIChangesAnti();
      }
      if (self.view.main.tbxUserName.text.trim().length === 0) self.showUserNames();
    },

    /**
         * hideUserNames :The function is used to hide the list of masked usernames saved when the user enters any characters and and function call to check whether to enable login button  if both username and password are entered
         * @member of {frmLoginController}
         * @param {}  
         * @return {} 
         * @throws {}
         */
    hideUserNames: function() {
      var self = this;
      self.enableLogin(self.view.main.tbxUserName.text.trim(), self.view.main.tbxPassword.text);
      if (self.view.main.tbxUserName.text) {
        self.hideUserNameSuggestions();
      }
    },

    /**
         * checkifUserNameContainsMaskCharacter :The function is used to check whether the username contains the mask Character "*" and shows/hides the username suggestions based on the text entered in the uusername field
         * @member of {frmLoginController}
         * @param {}  
         * @return {} 
         * @throws {}
         */
    checkifUserNameContainsMaskCharacter: function() {
      var self = this;
      self.removeSpecialCharacter();
      if (self.view.main.tbxUserName.text.trim().length > 0) {
        self.hideUserNameSuggestions();
      } else {
        self.showUserNames();
      }
      self.enableLogin(self.view.main.tbxUserName.text.trim(), self.view.main.tbxPassword.text);
    },

    /**
         * toEnableOrDisableLogin :The function is used to check whether both username and password are entered and enable the login button and on click of login button onLoginClick() function is called
         * @member of {frmLoginController}
         * @param {}  
         * @return {} 
         * @throws {}
         */
    toEnableOrDisableLogin: function() {
      var self = this;
      var username = self.view.main.tbxUserName.text.trim();
      var password = self.view.main.tbxPassword.text;
      if (username !== null && username !== "" && password !== null && password !== "" && password.length >= 1 && username.length >= 1) {
        self.onLoginClick();
      }

    },

    /**
  * Function to be called on pre show of the frmLogin
  */
    onPreShow : function () {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      this.attachToModule(authModule);
      this.restoreOriginalMainLoginUIChanges();
      this.preshowFrmLogin();
      //CommonUtilitiesOCCU.applySkinThemeToApp(); 
    },
    /**
  * Function to be called on post show of the frmLogin
  */
    onPostShow : function () {


      this.setFlowActions();
      this.setmainflexheight();
      this.loginConstructor();
      this.onLoadChangePointer();
      this.view.letsverify.lbxMonth.onSelection=this.showDates;
      this.view.letsverify.lbxYear.onSelection=this.showDates;
      this.view.main.imgRememberMe.src='checked_box.png';
      this.view.CustomFeedbackPopup.btnYes.toolTip = "TAKE SURVEY";
      this.view.CustomFeedbackPopup.btnNo.toolTip = "MAYBE LATER";
      this.disableButton(this.view.main.btnLogin);
      this.disableButton(this.view.letsverify.btnProceed);
      this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
      this.disableButton(this.view.ResetOrEnroll.btnNext);
      this.disableButton(this.view.newpasswordsetting.btnNext);
      this.view.newpasswordsetting.tbxNewPassword.onBeginEditing = this.passwordEditing;
      this.view.newpasswordsetting.tbxNewPassword.onKeyUp = this.newPwdKeyUp;
      this.view.newpasswordsetting.tbxMatchPassword.onKeyUp = this.matchPwdKeyUp;
    },
    setFlowActions: function() {
      var scopeObj = this;
      this.view.letsverify.flxWhatIsSSN.onClick = function() {
        if(scopeObj.view.AllForms.isVisible === false)
          scopeObj.view.AllForms.setVisibility(true);
        else
          scopeObj.view.AllForms.setVisibility(false);
      };
      this.view.AllForms.flxCross.onClick = function() {
        scopeObj.view.AllForms.setVisibility(false);
      };
    },
    newPwdKeyUp:function(){
      this.validateNewPassword(this.view.newpasswordsetting.tbxNewPassword.text);
    },
    matchPwdKeyUp:function(){
      if (this.view.newpasswordsetting.tbxMatchPassword.text !== "")
        this.validateConfirmPassword(this.view.newpasswordsetting.tbxMatchPassword.text); 
    },
    passwordEditing: function(){
      if(this.view.newpasswordsetting.lblErrorInfo.isVisible){
        this.reEnterNewPassword();
      } 
    },
    setmainflexheight: function()
    {
      var height = kony.os.deviceInfo().screenWidth * 0.5652;
      this.view.flxMain.height = height + "dp";
    },
    /**
  * Function to be called when the login button is clicked
  */
    onLoginClick : function () {
      var status=true;
      if(this.view.main.imgRememberMe.src=='unchecked_box.png') {
        status=false;
      }
      this.view.main.rtxErrorMsg.setVisibility(false);
      this.view.main.rtxErrorMsgUser.setVisibility(false);
      this.view.flxMain.flxLogoutMsg.setVisibility(false);
      this.view.lblLanguage.skin= "sknLabelLato3343A813Px";
      var enteredUserName = this.view.main.tbxUserName.text.trim();
      if (this.checkMasked(enteredUserName) === true) {
        var unMaskedUserName = this.getUnMaskedUserName(enteredUserName);
        if (unMaskedUserName !== null) {
          CommonUtilities.showProgressBar(this.view);
          this.presenter.onLogin({"username":unMaskedUserName,"password":this.view.main.tbxPassword.text,"rememberme":status},this.loginSuccessCallBack,this.loginFailureCallBack);     
        }
      } else {
        CommonUtilities.showProgressBar(this.view);
        this.presenter.onLogin({"username":enteredUserName,"password":this.view.main.tbxPassword.text,"rememberme":status},this.loginSuccessCallBack,this.loginFailureCallBack); 
      }    
    },

    /** 
   * function for login SuccessCallback depending on the status 
   *Parameters response,UserNamePasswordJSON
  */
    loginSuccessCallBack:function(response,UsernamePasswordJSON){
      kony.print("Success Authenticating to backend");
      var maskedUserName= this.maskUserName(UsernamePasswordJSON.username);
      kony.print("Success in getting BackendToken=>" + JSON.stringify(response)); 
      this.presenter.saveUserName(maskedUserName,UsernamePasswordJSON);
      kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged = true; 
      this.presenter.setIdleTimeout();
    },

    /**
    *function for login failure back depeding on the status of the Service
    *Parameters response
  */

    loginFailureCallBack:function(response){
      CommonUtilities.hideProgressBar(this.view);           
      var context = {
        errorMessage: response.data.message
      };
      kony.print("Failed Authenticating to backend");
      this.view.main.tbxPassword.text="";
      this.presenter.navigateToLoginErrorPage(context);
    },


    /**
  * Function to enable the button along with the enable button skin
  */
    enableButton : function (button) {
      button.skin = "sknBtnNormalLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
      button.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.setEnabled(true);
    },
    /**
  * Function to disable the button along with the disable button skin
  */
    disableButton : function (button) {
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.setEnabled(false);
    },
    /**
   * Function whether to update UI
   * Parameters: context
     */
    shouldUpdateUI : function(context) {
      return typeof context !== 'undefined' && context !== null;
    },
    /**
   * Function to make changes to UI
   * Parameters: context
    */
    willUpdateUI: function (context) {
      if(context.action === "Logout") {
        // when the user Clicked on Logout
        this.showLogoutPage();
      }else if (context.action === "SessionExpired") {
        // When the session is expired
        this.showSessionExpiredPage();
      } else if (context.action === "ServerDown") {
        //When there is a server down time
        this.showServerErrorPage(this.view.flxLogin);
      } else if (context.action === "hideProgressBar") {
        //When there is a server down time
        kony.olb.utils.hideProgressBar(this.view);
      } else  if (context.errorMessage){
        this.showErrorMessage(context.errorMessage);
      } 
    },
    /**
   * Function to display logout flex
     */
    showLogoutPage : function() {
      this.view.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.login.SuccessfullyLoggedOut");
      this.view.imgLogoutSuccess.src = "logout_tick_mark.png";
      this.restoreOriginalMainLoginUIChanges();
      this.changesAfterLogout();
    },
    /**
   * Function to show survey
   */
    showSurvey: function () {
      var scopeObj=this;
      var surveyEnabled = true;
      if (surveyEnabled) {
        this.view.flxFeedbackTakeSurvey.setVisibility(true);
        this.view.CustomFeedbackPopup.btnYes.onClick = function () {
          scopeObj.view.flxFeedbackTakeSurvey.setVisibility(false);
          var surveyModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SurveyModule");
          surveyModule.presentationController.showSurvey();
        }
        this.view.CustomFeedbackPopup.btnNo.onClick = function () {
          scopeObj.view.flxFeedbackTakeSurvey.setVisibility(false);
        }
        this.view.CustomFeedbackPopup.flxCross.onClick = function () {
          scopeObj.view.flxFeedbackTakeSurvey.setVisibility(false);
        }
      }
    },
    /**
   * Function to display the session expire flex
     */
    showSessionExpiredPage: function () {
      this.view.imgLogoutSuccess.src = "session_timeout_white.png";
      this.view.lblLoggedOut.text = kony.i18n.getLocalizedString("i18n.login.SessionExpired");
      this.restoreOriginalMainLoginUIChanges();
      this.changesAfterLogout();
    },
    /**
    * Common UI changes to be made after logout 
    */
    changesAfterLogout : function() {
      var scopeObj=this;
      this.view.flxLogoutMsg.setVisibility(true);
      this.view.lblLanguage.skin= "sknLabelLatoffffff13";
      this.view.flxMain.skin = "sknFlexLoginAfterLogout";
      this.view.flxLogin.top = "120dp";
      this.view.forceLayout();        
    },
    /**
   * Function to show the visibility of the Appropriate lable to true or false.
   * Parameters: errorMessage
     */
    showErrorMessage: function(errorMessage) {
      if(this.view.main.flxLoginUser.isVisible) {
        this.view.main.rtxErrorMsgUser.text = errorMessage;
        this.view.main.rtxErrorMsgUser.setVisibility(true);
      } else {
        this.view.main.rtxErrorMsg.text = errorMessage;
        this.view.main.rtxErrorMsg.setVisibility(true);
      } 
      this.view.forceLayout();
    },
    removeSpecialCharacter : function() {
      var str = this.view.main.tbxUserName.text;
      if(str) {
        if(str.charAt(str.length-1) === '*') {
          this.view.main.tbxUserName.text = str.substring(0,str.length-1);
        }
      }
    }, 
    /**
    *function to enable or disable the login depending on the text entered in username and password
    *parameters  username,password
   */
    enableLogin:function(username,password){
      if (username !== null && username !== "" && password !== null && password !== "" && password.length>=1 &&username.length>=1) {
        this.btnLoginToggle(true);
      } else {
        this.btnLoginToggle(false);
      }
    },

    /**
   * Function enables/disables the login button depending on the status.
   * Status ---true ---button enabled.
   * Status---False --- button disabled.
 **/   
    btnLoginToggle:function(status) {
      if(status === true) {
        this.enableButton(this.view.main.btnLogin);
      } else {
        this.disableButton(this.view.main.btnLogin);
      }
    },

    /**
  * function to show the username suggestions when the user clicks on the textbox
  */
    showUserNames:function(){
      var savedUserNames = JSON.parse(localStorage.getItem("olbNames"));
      if(savedUserNames === null || savedUserNames.length === 0) {
        this.view.main.flxUserDropdown.isVisible = false;
      } else {
        this.showUserNameSuggestions(JSON.parse(localStorage.getItem("olbNames")));  
      }
    },
    /** 
   * Function shows the usernames already stored in the local Storage as the suggestions when clicked on the flxUserName.
   * enteredText:Array of usernames which are stored in the localStorage.
  **/
    showUserNameSuggestions: function(enteredText) {
      if (enteredText !== null && enteredText !== undefined) {
        this.view.main.segUsers.removeAll();
        var x = [];
        for (var index in enteredText){
          for(var attr in enteredText[index]){
            var y = {
              "lblusers": enteredText[index][attr]
            };
            x.push(y);
          }
        }
        this.view.main.segUsers.addAll(x);
      }
      this.view.main.flxUserDropdown.isVisible = true;
      this.view.forceLayout();
    },

    /**
  *Function used when the user selects the usernames from the suggestions .
 **/
    selectUserName: function() {
      var val = this.view.main.segUsers.selectedItems[0].lblusers;
      this.view.main.tbxUserName.text = val;
      this.view.main.flxUserDropdown.isVisible = false;
      this.enableLogin(val, this.view.main.tbxPassword.text);
      this.view.forceLayout();
    },
    /**
 * Function used to hide the username suggestions once the user name is selected.
 **/
    hideUserNameSuggestions: function() {
      this.view.main.flxUserDropdown.isVisible = false;
      this.view.forceLayout();
    },
    /**
  * Function used to diplay the masked passsword with the help of the simple icon
  **/
    showPassword: function() {
      this.view.main.tbxPassword.secureTextEntry = false;
    },
    /**
       * function used to hide the masked passsword with the help of the simple icon
  **/
    hidePassword: function() {
      this.view.main.tbxPassword.secureTextEntry = true;
    },
    /**
    * function used to return the status of the remember me depending on checked or unchecked.
    **/
    chkRememberMe:function(){
      if(this.view.main.imgRememberMe.src == 'checked_box.png'){
        return true;
      }else{
        return false;
      }
    },

    /**
    * function used to show remember me
    **/
    rememberMe: function() {
      if (this.view.main.imgRememberMe.src == 'unchecked_box.png') {
        this.view.main.imgRememberMe.src = 'checked_box.png';
      } else {
        this.view.main.imgRememberMe.src = 'unchecked_box.png';
      }
      this.view.forceLayout();
    },
    /**
    * function used to show login page after time out
    **/

    preshowFrmLogin: function() {
      var scopeObj=this;
      //TODO: Refreshing the page after logout need to be done in a proper way...
      var OLBLogoutStatus = kony.store.getItem('OLBLogoutStatus');
      if(OLBLogoutStatus) {
        this.willUpdateUI(OLBLogoutStatus);
        kony.mvc.MDAApplication.getSharedInstance().appContext.username = OLBLogoutStatus.username;
        kony.store.removeItem('OLBLogoutStatus');
      } else {
        this.view.flxLogoutMsg.setVisibility(false);
      }
      if(this.chkUserInLocal()) {
        var username = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
        scopeObj.view.main.tbxUserName.text = this.maskUserName(username);
      } else {
        scopeObj.view.main.tbxUserName.text = "";
      }
      // After Login error from forgot password flow we need to persist the user name
      if(this.view.main.flxLoginUser.isVisible) {
        scopeObj.view.main.tbxUserName.text = this.view.main.lblVerifiedUser.text;
      }

      scopeObj.view.main.tbxPassword.text = "";
      if (this.view.flxEnrollOrServerError.isVisible || this.view.flxVerification.isVisible){
        scopeObj.view.flxLogin.setVisibility(false);
      } else {
        scopeObj.view.flxLogin.setVisibility(true);
      }
      this.view.main.btnOnlineAccessEnroll.onClick = function () {
        var context={"action" : "Navigate to Enroll"};
        //Load enroll module
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollModule.presentationController.showEnrollPage(context);
      };
//       this.view.btnFaqs.onClick = function(){
//         //          var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
//         //         informationContentModule.presentationController.showOnlineHelp();
//       };
//       this.view.btnPrivacy.onClick = function(){
//         //          var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
//         //         informationContentModule.presentationController.showPrivacyPolicyPage();
//       };
      this.view.btnLocateUs.onClick = function() {
        scopeObj.view.flxLogoutMsg.isVisible=false;
        var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
        locateUsModule.presentationController.showLocateUsPage();
      };
      function timerFunc() 
      {
        if(kony.os.deviceInfo().screenHeight>768){
          scopeObj.view.flxMain.height=kony.os.deviceInfo().screenHeight+"dp";
        }
        else
          scopeObj.view.flxMain.height="768dp";
        scopeObj.view.forceLayout();
      }
      kony.timer.schedule("screenHeightTimer",timerFunc, 0.01, true);

    },  /**
    * function used to show different languages 
    **/

    loginConstructor: function() {
      this.wrongCredentials = 0;
      this.languages =[{lblLang:kony.i18n.getLocalizedString("i18n.common.English"),lblSeparator:"lbl"},
                       {lblLang:kony.i18n.getLocalizedString("i18n.common.Spanish"),lblSeparator:"lbl"},
                       {lblLang:kony.i18n.getLocalizedString("i18n.common.German"),lblSeparator:"lbl"},
                       {lblLang:kony.i18n.getLocalizedString("i18n.common.Russian"),lblSeparator:"lbl"},
                       {lblLang:kony.i18n.getLocalizedString("i18n.common.Chinese"),lblSeparator:"lbl"},
                       {lblLang:kony.i18n.getLocalizedString("i18n.common.Japanese"),lblSeparator:"lbl"},
                       {lblLang:kony.i18n.getLocalizedString("i18n.common.French"),lblSeparator:"lbl"}];
    },
    /**
    * function used to show flxVerificationscreen 
    **/

    verifyUser: function() {
      this.view.flxLogoutMsg.setVisibility(false);
      this.view.flxMain.skin = "sknFlexLoginErrorBackgroundKA";
      this.view.flxLogin.isVisible = false;
      this.view.main.rtxErrorMsgUser.setVisibility(false);
      this.view.flxVerification.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * UI for reverting credentials Missing 
    **/

    credentialsMissingUIChangesAnti: function() {
      this.view.main.lblWelcome.isVisible = true;
      this.view.main.rtxErrorMsg.isVisible = false;
      this.view.main.flxPassword.skin = "sknBorderE3E3E3";
      this.view.main.flxUserName.skin = "sknBorderE3E3E3";
      this.enableButton(this.view.main.btnLogin);
      this.view.forceLayout();
    },
    /**
    * UI for credentials Missing 
    **/
    credentialsMissingUIChanges: function(container, msg) {
      this.view.main.lblWelcome.isVisible = true;
      this.view.main.rtxErrorMsg.isVisible = true;
      this.view.main.rtxErrorMsg.text = msg;
      container.skin = "sknBorderFF0101";
      this.view.main.btnLogin.setEnabled(false);
      this.disableButton(this.view.main.btnLogin);
      this.view.flxMain.skin = "sknFlexLoginErrorBackgroundKA";
      this.view.flxLogin.centerX = "50%";
      this.view.forceLayout();
    },
    /**
    * UI for wrong credentials
    **/
    credentialsWrongUIChanges: function() {
      this.view.flxMain.skin = 'sknFlexLoginErrorBackgroundKA';
      this.view.flxLogin.centerX = "50%";
      this.view.main.rtxErrorMsg.text = "You have only few attempts left.<br>You can use the \"Forgot\" option for assistance or contact us.";
      if (this.wrongCredentials < 4) {
        kony.print(this.wrongCredentials);
        this.view.main.lblWelcome.isVisible = true;
        this.view.main.rtxErrorMsg.isVisible = true;
        this.view.forceLayout();
      } else if (this.wrongCredentials == 4) {
        kony.print(this.wrongCredentials);
        this.view.main.rtxErrorMsg.text = "Looks like you might need our help.<br>Or you can try one more time";
        this.view.main.imgUser.src = 'help_error.png';
        this.view.forceLayout();

      } else {
        this.view.flxLogin.isVisible = false;
        this.view.flxBlocked.isVisible = true;
        this.view.forceLayout();
      }
    },
    /**
    * UI for reverting wrong credentials
    **/
    credentialsWrongUIChangesAnti: function() {
      this.view.main.lblWelcome.isVisible = true;
      this.view.main.rtxErrorMsg.isVisible = false;
      this.view.main.imgUser.src = 'default_username.png';
    },
    /**
    * UI for Login
    **/
    login: function() {

      this.view.flxLogoutMsg.setVisibility(false);
      if (this.view.main.tbxUserName.text && !(this.view.main.tbxPassword.text)) {
        this.credentialsMissingUIChanges(this.view.main.flxPassword, "Please enter your Password");
      } else if (!(this.view.main.tbxUserName.text) && !(this.view.main.tbxPassword.text)){

        this.credentialsMissingUIChanges(this.view.main.flxUserName, "Please enter your username and Password");
      } else if (!(this.view.main.tbxUserName.text) && this.view.main.tbxPassword.text){

        this.credentialsMissingUIChanges(this.view.main.flxUserName, "Please enter your username");
      }
      else{}

      function validationSuccessCall() {
        var ntf = new kony.mvc.Navigation("frmAccountsLanding");
        ntf.navigate();
        kony.timer.cancel("screenHeightTimer");
        this.view.forceLayout();
      }

      function validationErrorCall() {
        this.wrongCredentials++;
        this.credentialsWrongUIChanges();
      }
    },
    /**
    * Function used to validate user
    * Parameters: Response from getUserName command
    **/

    validateUser: function(success, failure) {
      if (this.view.main.tbxPassword.text == 'password' && this.view.main.tbxUserName.text == 'user') {
        success();
      } else {
        failure();
      }
    },
    /**
    * Function: used to show the welcomeBack screen.
    * Parameters: Response from getUserName command
    **/
    welcomeVerifiedUser: function(response) {
      this.emptyUserDetails();
      kony.mvc.MDAApplication.getSharedInstance().appContext.username = response.data.userName;
      this.view.flxVerification.isVisible = false;
      this.view.flxWelcomeBack.lblVerifiedUsername.text = response.data.userName;
      var buttonText =  this.view.AlterneteActionsSignIn.rtxCVV.text +" "+ response.data.userName;
      this.view.AlterneteActionsSignIn.rtxCVV.text = buttonText;
      this.view.flxWelcomeBack.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * UI for go To PasswordResetOptions Page and disable the CVV option
    **/
    hideCVVOption:function(){

      this.view.flxWelcomeBack.isVisible = false;
      this.view.flxResetPasswordOptions.isVisible = true;
      this.view.AlterneteActionsEnterCVV.isVisible=false;
      this.view.OrLineForCVVandPIN.isVisible=false;
      this.view.resetusingOTP.btnUseCVV.isVisible=false;
      this.view.resetusingOTP.orline.isVisible = false;
      this.view.forceLayout();
    },
    /**
    * UI for go To PasswordResetOptions Page and enable the CVV option
    **/
    showCVVOption:function(){
      this.view.flxWelcomeBack.isVisible = false;
      this.view.flxResetPasswordOptions.isVisible = true;
      this.view.resetusingOTP.btnUseCVV.isVisible = true;
      this.view.resetusingOTP.orline.isVisible = true;
      this.view.AlterneteActionsEnterCVV.isVisible = true;
      this.view.OrLineForCVVandPIN.isVisible = true;
      var buttonText = this.view.AlterneteActionsSignIn.rtxCVV.text;
      var position = buttonText.indexOf(kony.mvc.MDAApplication.getSharedInstance().appContext.username, 0);
      buttonText = buttonText.substring(0,position);
      this.view.AlterneteActionsSignIn.rtxCVV.text = buttonText.trim();
      this.view.flxResetPasswordOptions.isVisible = true
      this.view.forceLayout();
    },



    /**
    * UI for go To Reset Using OTP
    **/
    goToResetUsingOTP: function() {
      this.view.flxResetPasswordOptions.isVisible = false;
      this.view.flxSendOTP.isVisible = true;
      this.view.forceLayout();
    },

    /**
    * Function to resend OTP
    **/
    resendOTPValue: function() {
      CommonUtilities.showProgressBar(this.view);
      this.presenter.resendOTP(this.resendOTPResponseSuccess,this.resendOTPResponseFailure);
    },
    /**
  * function to handle success scenario of requestOTP  
*/
    resendOTPResponseSuccess:function(){
      kony.olb.utils.hideProgressBar(this.view);
      this.view.forceLayout();
    },

    /**
    *function to handle failure scenario of requestOTP
  */
    resendOTPResponseFailure:function(){
      kony.olb.utils.hideProgressBar(this.view);
      kony.print("Failed to request otp");
    },
    /**
    * Function to display enter OTP page
    **/
    showEnterOTPPage: function() {
      this.view.flxSendOTP.isVisible = false;
      this.view.resetusingOTPEnterOTP.flxCVV.isVisible = true;
      this.view.resetusingOTPEnterOTP.btnResendOTP.isVisible = true;
      this.view.resetusingOTPEnterOTP.btnResendOTP.onClick=this.resendOTPValue;
      this.view.resetusingOTPEnterOTP.btnNext.top = "3.52%";
      this.view.resetusingOTPEnterOTP.imgCVVOrOTP.top = "3.52%";
      this.view.resetusingOTPEnterOTP.btnUseCVV.top = "2.61%";
      this.view.resetusingOTPEnterOTP.btnNext.text = kony.i18n.getLocalizedString("i18n.common.next");
      this.view.flxResetUsingOTP.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * Function to display enter CVV page
    **/
    showEnterCVVPage: function() {
      this.view.flxResetPasswordOptions.isVisible = false;
      this.view.flxResetUsingCVV.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * Function to display cards available for the particualr user
	* Parameters the available cards ---cardsJSON
    **/
    showCVVCards: function(presentCards) {
      var x = [];
      this.view.ResetOrEnroll.lstbxCards.masterData=x;
      var count = 0;
      for (var index in presentCards) {
        for (var attr in presentCards[index]) {
          var value = presentCards[index][attr];
          var y = [];
          y.push( count, value);
          count++;
          x.push(y);
        }
      }
      this.view.ResetOrEnroll.lstbxCards.masterData=x;
    },
    /**
    * UI for re-enter CVV
    **/
    reEnterCVV:function(){
      this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
      // this.view.ResetOrEnroll.imgCVV.top = "8.8%";
      this.view.forceLayout();      
    },
    /**
    * UI for re-enter OTP
    **/
    reEnterOTP:function(){
      this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
      this.view.forceLayout();      
    },
    /**
    * Function to check if the password matches with the re-entered password
    **/    
    isPasswordValidAndMatchedWithReEnteredValue:function(){
      if(this.view.newpasswordsetting.tbxNewPassword.text && this.view.newpasswordsetting.tbxMatchPassword.text){
        if(this.view.newpasswordsetting.tbxNewPassword.text === this.view.newpasswordsetting.tbxMatchPassword.text){
          return true;
        }
      }
      return false;
    },
    /**
    * Function to re-enter new password
    **/
    reEnterNewPassword:function(){
      this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
      this.view.newpasswordsetting.flxNewPassword.top = "8.8%";      
    },
    /**
    * UI for recovered Username UI Changes
    **/
    recoveredUsernameUIChanges: function() {
      this.view.main.lblAppName.isVisible = false;
      this.view.main.lblWelcome.isVisible = false;
      this.view.main.rtxErrorMsg.isVisible = false;
      this.view.main.btnOnlineAccessEnroll.isVisible = false;
      this.view.main.flxLoginUser.isVisible = true;
      this.view.main.imgUserOutline.isVisible = true;
      this.view.main.imgUserName.src = 'login_username_icon.png';
      this.view.main.imgUser.src = 'user_image.png';
      // this.view.main.flxUserName.top = "7.04%";
      //this.view.main.flxUserDropdown.top = "45.8%";
      this.view.flxLogin.centerX = "50%";
    },
    /**
    * Function to login with verified userName
    **/
    loginWithVerifiedUserName: function() {
      this.recoveredUsernameUIChanges();
      this.view.flxWelcomeBack.isVisible = false;
      this.view.main.flxLoginUser.lblVerifiedUser.text = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      this.view.main.tbxUserName.text = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      this.view.main.tbxPassword.text = "";
      this.disableButton(this.view.main.btnLogin);
      var buttonText = this.view.AlterneteActionsSignIn.rtxCVV.text;
      var position = buttonText.indexOf(kony.mvc.MDAApplication.getSharedInstance().appContext.username, 0);
      buttonText = buttonText.substring(0,position);
      this.view.AlterneteActionsSignIn.rtxCVV.text = buttonText.trim();
      this.view.flxLogin.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * Function to restore original main login UI changes
    **/
    restoreOriginalMainLoginUIChanges: function() {
      this.view.flxMain.skin = 'sknFlexLoginMainBackgroundKA';
      this.view.flxLogin.centerX = "57.3%";
      this.view.main.imgUserOutline.isVisible = false;
      this.view.main.imgUser.src = 'default_username.png';
      this.view.main.lblAppName.isVisible = true;
      this.view.main.lblWelcome.isVisible = true;
      this.view.main.rtxErrorMsg.isVisible = false;
      this.view.main.flxLoginUser.isVisible = false;
      this.view.main.imgUserName.src = 'username.png';
      this.view.main.flxUserName.top = "9.68%";
      this.view.main.btnOnlineAccessEnroll.isVisible = true;
      this.view.flxResetUsingOTP.setVisibility(false);
      this.view.flxResetUsingCVV.setVisibility(false);
      this.view.flxResetPassword.setVisibility(false);
      this.view.flxResetSuccessful.setVisibility(false);
      this.view.flxWelcomeBack.setVisibility(false);
      this.view.flxLogin.setVisibility(true);
      this.view.flxVerification.setVisibility(false);
      this.view.flxResetPasswordOptions.setVisibility(false);
      this.view.flxSendOTP.setVisibility(false);
      this.view.flxBlocked.setVisibility(false);
      this.view.flxEnrollOrServerError.setVisibility(false);
      this.view.flxEnroll.setVisibility(false);
      this.view.main.flxLoginUser.setVisibility(false);
      this.view.imgDropdown.src="chevron_down_white.png";
      this.view.lblLanguage.skin= "sknLabelLato3343A813Px";
      this.view.newpasswordsetting.tbxNewPassword.text="";
      this.view.newpasswordsetting.tbxMatchPassword.text="";
      this.view.newpasswordsetting.imgValidPassword.isVisible=false;
      this.view.newpasswordsetting.imgPasswordMatched.isVisible=false;
      this.view.main.tbxUserName.text="";
      this.view.main.tbxPassword.text="";
      this.disableButton(this.view.main.btnLogin);
      this.disableButton(this.view.letsverify.btnProceed);
      this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
      this.disableButton(this.view.ResetOrEnroll.btnNext);
      this.disableButton(this.view.newpasswordsetting.btnNext);

      //this.view.main.flxUserDropdown.top = "48.2%";
    },
    /**
    * UI for re-login
    **/
    loginLater: function(currentFlex) {
      currentFlex.isVisible = false;
      this.restoreOriginalMainLoginUIChanges();
      this.view.flxLogin.top = "100dp";
      this.view.forceLayout();
    },
    /**
    * Function to dsplay server error page
    **/
    showServerErrorPage: function(currentFlex){
      currentFlex.isVisible = false;
      //this.restoreOriginalMainLoginUIChanges();
      this.view.flxEnrollOrServerError.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * UI to verify the blocked user
    **/
    letsGetStarted: function() {
      this.view.flxBlocked.isVisible = false;
      this.view.flxVerification.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * Function to use CVV for reset
    **/
    useCVVForReset: function() {
      this.view.flxResetUsingOTP.isVisible = false;
      this.view.flxSendOTP.isVisible = false;
      this.view.flxResetUsingCVV.isVisible = true;
      this.view.resetusingOTPEnterOTP.tbxCVV.text = "";
      this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
      this.view.forceLayout();
    },
    /**
    * Function to use OTP for reset
    **/
    useOTPForReset: function() {
      this.view.flxResetUsingCVV.isVisible = false;
      this.view.flxSendOTP.isVisible = true;
      this.view.ResetOrEnroll.tbxCVV.text = "";
      this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
      this.view.forceLayout();
    },
    /**
    * Function to on Load chnage pointer
    **/
    onLoadChangePointer: function() {
      var elems = document.querySelectorAll("input[kwidgettype='Button']");
      for (var i = 0, iMax = elems.length; i < iMax; i++) {
        elems[i].style.cursor = 'pointer';
      }
    },
    /**
    * Function to change Skin On Hover
    **/
    changeSkinOnHover: function() {
      this.view.passwordresetsucess.rtxDoneLoginlater.addEventListener("mouseover", function(event) {
        // highlight the mouseover target
        event.target.style.color = "orange";
        // reset the color after a short delay
        setTimeout(function() {
          event.target.style.color = "";
        }, 500);
      }, false);
    },
    /**
    * Function to display language options
    **/
    showLanguageOptions:function(){
      this.view.imgDropdown.src = "arrow_up.png";
      this.view.flxLanguagePicker.isVisible = true;
      this.view.forceLayout();  
    },
    /**
    * Function to swap languages
    **/
    interchangePlaces:function(val){
      var temp = this.languages[0];
      this.languages[0] = this.languages[val];
      this.languages[val] = temp;
    },
    /**
    * Function to select your language
    **/
    selectYourLanguage:function(){

      var val = this.view.segLanguagesList.selectedItems[0].lblLang;
      var selectedIndex = this.view.segLanguagesList.selectedRowIndex[1];
      kony.print(selectedIndex);
      this.interchangePlaces(selectedIndex + 1);
      kony.print(this.languages[0]);
      var lang = this.languages.slice(1,7);
      this.view.segLanguagesList.setData(lang);
      this.view.flxLanguagePicker.isVisible = false;
      this.view.lblLanguage.text = val;
      this.view.forceLayout();
    },
    /**
    * Function to hide opened menus
    **/
    hideOpenMenus:function(){
      this.view.flxLanguagesSegment.isVisible = false;


    },
    /**
    * Function to request OTP
    **/
    requestOTPValue : function() {
      this.presenter.requestOTP(this.requestOTPResponseSuccess,this.requestOTPResponseFailure);
    },

    /**
  * function to handle requestOTP successcallback 
*/
    requestOTPResponseSuccess:function(){
      this.showEnterOTPPage();
    },

    /**
    *function to handle requestOTP failureCallBack
  */
    requestOTPResponseFailure:function(){
      kony.print("Failed to request otp");
    },

    /**
    * Function to show error if CVV is invalid
    **/
    showErrorForCVV:function(){
      this.view.ResetOrEnroll.lblWrongCvv.isVisible = true;
      // this.view.ResetOrEnroll.imgCVV.top = "5.28%";
      this.view.forceLayout();
    },
    /**
    * Function to display reset password page
    **/
    showResetPasswordPage: function() {
      this.view.flxResetUsingCVV.isVisible = false;
      this.view.flxResetPassword.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * Function to show error if OTP is invalid
    **/ 
    showErrorForOTP:function(){
      this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * Function to show ResetPasswordPage From OTP
  **/
    showResetPasswordPageFromOTP: function() {   
      this.view.flxResetUsingOTP.isVisible = false;
      this.view.flxResetPassword.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * Function to reset password
  	**/
    showResetConfirmationPage: function() {
      var password = this.view.newpasswordsetting.tbxNewPassword.text;
      var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      CommonUtilities.showProgressBar(this.view);
      this.presenter.toResetPassword(userName,password,this.toResetPasswordResponse);

    },

    /**
   *function to handle ResetPassword SuccessCallBack
   *Parameters response from the command
  */
    toResetPasswordResponse:function(response){
      kony.olb.utils.hideProgressBar(this.view);
      this.view.flxResetPassword.setVisibility(false);
      if (response.status === kony.mvc.constants.STATUS_SUCCESS) {
        this.showResetConfirmationScreen();
      } else {
        this.presenter.navigateToServerDownScreen();
      }
      this.view.forceLayout();
    },
    /**
    * Function to show password reset successful screen
  **/
    showResetConfirmationScreen: function() {
      this.view.flxResetPassword.isVisible = false;
      this.view.flxResetSuccessful.isVisible = true;
      this.view.forceLayout();
    },
    /**
    * Function to check if password contains 9 consecutive digits
  **/
    hasConsecutiveDigits: function(input) {
      var i; 
      var count=0;
      for (i = 0; i < input.length; i++) {
        // alert(abc[i]);
        if(input[i] >= 0 && input[i] <= 9 ) 
          count++;
        else 
          count=0;
        if(count === 9)
          return true;
      }  
      return false; 
    },
    /**
    * Function to validate New Password
    **/
    validateNewPassword: function(enteredPassword) {
      if(this.isPasswordValid(enteredPassword)) {
        this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
        this.view.newpasswordsetting.imgValidPassword.isVisible = true;
        if(this.isPasswordValidAndMatchedWithReEnteredValue()) {
          this.view.newpasswordsetting.imgPasswordMatched.isVisible = true;
          this.enableButton(this.view.newpasswordsetting.btnNext);
        } else {
          this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
          this.disableButton(this.view.newpasswordsetting.btnNext);
        }
      } else {
        this.view.newpasswordsetting.lblErrorInfo.isVisible = true;
        this.view.newpasswordsetting.imgValidPassword.isVisible = false;
        this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
        this.disableButton(this.view.newpasswordsetting.btnNext);
      }
      this.view.forceLayout();
    },
    /**
  * Function to validate Confirm Password 
  */
    validateConfirmPassword : function (enteredPassword) {
      if(this.isPasswordValid(enteredPassword)) {
        if(this.isPasswordValidAndMatchedWithReEnteredValue()) {
          this.view.newpasswordsetting.lblErrorInfo.isVisible = false;
          this.view.newpasswordsetting.imgPasswordMatched.isVisible = true;
          this.enableButton(this.view.newpasswordsetting.btnNext);
        } else {
          this.view.newpasswordsetting.lblErrorInfo.isVisible = true;
          this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
          this.disableButton(this.view.newpasswordsetting.btnNext); 
        }
      } else {
        this.view.newpasswordsetting.lblErrorInfo.isVisible = true;
        this.view.newpasswordsetting.imgPasswordMatched.isVisible = false;
        this.disableButton(this.view.newpasswordsetting.btnNext);
      }
      this.view.forceLayout();
    },

    /**
   * password check between 8 to 20 characters which contain at least one lowercase letter, one uppercase letter, 
   * one numeric digit, and one special character
   */
    isPasswordValid : function(enteredPassword) {
      var password=  /^(?=.*\d)(?=.*[a-zA-Z])(?=.+[\!\#\$\%\(\*\)\+\,\-\;\=\?\@\[\\\]\^\_\'\{\}]).{8,20}$/;
      var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      if(enteredPassword.match(password)  && !this.hasConsecutiveDigits(enteredPassword)&& enteredPassword != userName){
        return true;
      }
      return false;
    },
    /**
  * Function to check if cvv matches the given criteria
  **/
    cvvCheck: function() {
      var input = this.view.ResetOrEnroll.tbxCVV.text;
      var cvv = /^\d{3}$/;
      if(!input.match(cvv)){
        if(input !== "") {
          this.view.ResetOrEnroll.lblWrongCvv.isVisible = true;
        } else {
          this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
        }
        this.view.ResetOrEnroll.lblWrongCvv.text = kony.i18n.getLocalizedString("i18n.login.IncorrectCVV");

        this.disableButton(this.view.ResetOrEnroll.btnNext);
      }
      else{
        this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
        this.enableButton(this.view.ResetOrEnroll.btnNext);
      }
    },
    /**
    * Function to check if the cvv is correct with input and username
    **/
    isCVVCorrect: function() {
      var maskedCardNumber = this.view.ResetOrEnroll.flxCards.lstbxCards.selectedKeyValue;
      var selCardNumber = JSON.stringify(maskedCardNumber);
      selCardNumber=selCardNumber.substring(selCardNumber.indexOf(",")+1,selCardNumber.length-1);
      var cvv = this.view.ResetOrEnroll.tbxCVV.text;
      var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      this.presenter.cvvValidate(selCardNumber,cvv, userName,this.CVVValidateResponseSuccess,this.CVVValidateResponseFailure);
    },

    /**
    *function to validate the CVV entered and if it is correct then  showResetPasswordPage
  */

    CVVValidateResponseSuccess:function(){
      this.showResetPasswordPage();
    } ,

    /**
    *function to validate the CVV entered and if it is not correct then showErrorCVVPage
  */

    CVVValidateResponseFailure:function(){
      this.showErrorForCVV();
    }, 
    /**
    * Function to check if otp matches the given criteria (client side validation)
    **/
    otpCheck: function() {
      var input = this.view.resetusingOTPEnterOTP.tbxCVV.text;
      var otp = /^\d{6}$/;
      if(!input.match(otp)){
        if(input !== "") {
          this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = true;
        } else {
          this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
        }
        this.view.resetusingOTPEnterOTP.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.login.incorrectOTP");

        this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
      } else{
        this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
        this.enableButton (this.view.resetusingOTPEnterOTP.btnNext);
      }
    },
    /**
    * Function to check if the otp is correct with input and username
    **/
    isOTPCorrect: function() {
      var otp = this.view.resetusingOTPEnterOTP.tbxCVV.text;
      var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      this.presenter.otpValidate(otp,userName,this.OTPValidateResponseSuccess,this.OTPValidateResponseFailure);
    },

    /**
    * Function to check if the otp is correct and if it is correct then show ResetPassword Page
   **/

    OTPValidateResponseSuccess:function(){
      this.view.resetusingOTPEnterOTP.tbxCVV.text="";
      this.showResetPasswordPageFromOTP();
    },

    /**
    * Function to check if the otp is correct or not and if it is not correct then show ErrorOTP Page
   **/

    OTPValidateResponseFailure:function(){
      this.view.resetusingOTPEnterOTP.tbxCVV.text="";
      this.showErrorForOTP();
    },
    /**
    * Function to display error for reset
    **/
    showErrorForReset:function(){
      this.view.flxResetPassword.isVisible = true;
      this.view.flxResetSuccessful.isVisible = false;
      this.view.forceLayout();
    },
    /**
    * Function to unmask CVV
    **/
    showCVV: function() {
      this.view.ResetOrEnroll.tbxCVV.secureTextEntry = false;
    },
    /**
    * Function to mask CVV
    **/
    hideCVV: function() {
      this.view.ResetOrEnroll.tbxCVV.secureTextEntry = true;
    },
    /**
    * Function to unmask OTP
    **/
    showOTP: function() {
      this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = false;
    },
    /**
    * Function to mask CVV
    **/
    hideOTP: function() {
      this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = true;
    },
    /**
    * Function to unmask OTP
    **/
    showSAC: function() {
      this.view.MFA.tbxEnterSACCode.secureTextEntry = false;
    },
    /**
    * Function to mask CVV
    **/
    hideSAC: function() {
      this.view.MFA.tbxEnterSACCode.secureTextEntry = true;
    },
    /**
    * Function to validate SSN
    **/
    ssnCheck :function(){
      var input = this.view.letsverify.tbxSSN.text;
      var ssn = /^\d{9}$/;
      if(!this.isSSNValid(input) && input !== ""){
        this.view.letsverify.lblWrongInfo.text = kony.i18n.getLocalizedString("i18n.login.incorrectSSN");
        this.view.letsverify.lblWrongInfo.isVisible=true;
      }
      else
        this.view.letsverify.lblWrongInfo.isVisible = false;
    },
    /**
 *Function to check the Valid SSN
 */
    isSSNValid: function(input) {
      var ssn = /^\d{9}$/;
      if(input.match(ssn) && input!=="") {
        return true;
      }
      return false;
    },
    /**
    * OnSelection Function for listboxes of Month and Year
    **/
    showDates: function() {
      this.setNoOfDays();
      this.allFieldsCheck();
    },
    /*
    function to set days in flxVerification basing on year and month given 
  */
    setNoOfDays:function(){
      var selectedMonth = this.view.letsverify.lbxMonth.selectedKey;
      var dayArray = [];

      dayArray = [{"daykey":"day0","dayvalue":"Day"},{"daykey":"day1","dayvalue":1},{"daykey":"day2","dayvalue":2},{"daykey":"day3","dayvalue":3},
                  {"daykey":"day4","dayvalue":4},{"daykey":"day5","dayvalue":5},{"daykey":"day6","dayvalue":6},
                  {"daykey":"day7","dayvalue":7},{"daykey":"day8","dayvalue":8},{"daykey":"day9","dayvalue":9},
                  {"daykey":"day10","dayvalue":10},{"daykey":"day11","dayvalue":11},{"daykey":"day12","dayvalue":12},
                  {"daykey":"day13","dayvalue":13},{"daykey":"day14","dayvalue":14},{"daykey":"day15","dayvalue":15},
                  {"daykey":"day16","dayvalue":16},{"daykey":"day17","dayvalue":17},{"daykey":"day18","dayvalue":18},
                  {"daykey":"day19","dayvalue":19},{"daykey":"day20","dayvalue":20},{"daykey":"day21","dayvalue":21},
                  {"daykey":"day22","dayvalue":22},{"daykey":"day23","dayvalue":23},{"daykey":"day24","dayvalue":24},
                  {"daykey":"day25","dayvalue":25},{"daykey":"day26","dayvalue":26},{"daykey":"day27","dayvalue":27},
                  {"daykey":"day28","dayvalue":28}
                 ];
      var dayV,dayK ;
      var dayObj;
      if(selectedMonth == 'm1' || selectedMonth == 'm3' || selectedMonth == 'm5' || selectedMonth == 'm7' || selectedMonth == 'm8' || selectedMonth == 'm10' || selectedMonth == 'm12')
      {

        for(var i = 29; i <=31; i++){
          dayK = "day"+i;
          dayV = i;
          dayObj = {"daykey":dayK,"dayvalue":dayV};
          dayArray.push(dayObj);
        }
      }
      else if(selectedMonth == 'm4' || selectedMonth == 'm6' || selectedMonth == 'm9' || selectedMonth == 'm11'){

        for(var j = 29; j <=30; j++){
          dayK = "day"+j;
          dayV = j;
          dayObj = {"daykey":dayK,"dayvalue":dayV};
          dayArray.push(dayObj);
        }             
      }
      else{
        if((this.view.letsverify.lbxYear.selectedKeyValue!==undefined)&&(this.view.letsverify.lbxYear.selectedKeyValue!==null))
        {  var thisYear = this.view.letsverify.lbxYear.selectedKeyValue[1];
         if(((thisYear % 4 === 0) && (thisYear % 100 !== 0)) || (thisYear % 400 === 0)){
           dayK = "day"+29;
           dayV = 29;
           dayObj = {"daykey":dayK,"dayvalue":dayV};
           dayArray.push(dayObj);                  
         }
        }
      }
      this.view.letsverify.lbxDate.masterDataMap = [dayArray,"daykey","dayvalue"];
    },

    /**
    * Function to check whether all fields are filled
    **/
    allFieldsCheck:function(){
      var year=[];
      var month=[];
      var date=[];
      var SSN = this.view.letsverify.tbxSSN.text;
      var lastName = this.view.letsverify.tbxLastName.text;
      year=this.view.letsverify.lbxYear.selectedKeyValue;
      month=this.view.letsverify.lbxMonth.selectedKeyValue;
      date=this.view.letsverify.lbxDate.selectedKeyValue;
      if(this.isSSNValid(SSN) &&lastName!==""&&year[1]!="Year"&&month[1]!="Month"&&date[1]!="Day"){
        this.enableButton(this.view.letsverify.btnProceed);
      } else {
        this.disableButton(this.view.letsverify.btnProceed);
      }
    },
    /**
 * Function to verify user details
 **/
    verifyUserDetails: function() {
      var year=[];
      var month=[];
      var date=[];
      var SSN = this.view.letsverify.tbxSSN.text;
      var LastName = this.view.letsverify.tbxLastName.text;
      year=this.view.letsverify.lbxYear.selectedKeyValue;
      month=this.view.letsverify.lbxMonth.selectedKeyValue;
      var mon = month[0].slice(1,3);
      if(mon.length<2)
        mon='0'+mon;
      date=this.view.letsverify.lbxDate.selectedKeyValue;
      var day=date[1];
      if(day<10)
        day='0'+day;
      var dateStr = year[1] + '-' + mon + '-' + day;
      this.presenter.fetchUserName({
        "ssn": SSN,
        "lastname": LastName,
        "dob": dateStr
      },this.userDetailsSuccessCallBack,this.userDetailsErrorCallBack);
    },

    /**
    function to handle the userDetails SuccessCallBack from the fetchUserName Command Status
  */
    userDetailsSuccessCallBack:function(response){
      if(response.data.userName) {
        this.welcomeVerifiedUser(response);
      } else {
        var userDetails = {
          "userDetails" : response.userDetails
        };
        this.showEnrollFlex(userDetails);
      }
    },

    /**
    function to handle the userDetails ErrorCallBack if the user is not in the records from the fetchUserName Command Status
  */
    userDetailsErrorCallBack:function(response){
      this.fetchUserNameErrResponse();
    },

    /**
  *Show Enroll Flex error Message
  */
    showEnrollFlex : function(userDetails) {
      var self = this;
      self.emptyUserDetails();
      self.view.flxVerification.setVisibility(false);
      self.view.flxEnroll.setVisibility(true);
      self.view.EnrollPromptScreen.btnEnroll.onClick = function () {
        var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
        enrollModule.presentationController.showEnrollPage(data);
      };
    },

    /**
  * Function to Navigate To Enroll Page
  */
    navigateToEnrollPage : function(data) {
      this.presenter.navigateToEnrollPage(data);
    },
    /**
  * Function to empty the details entered in the UI, that was used to get the user name
  */
    emptyUserDetails : function() {
      this.view.letsverify.tbxSSN.text = "";
      this.view.letsverify.tbxLastName.text = "";
      this.view.letsverify.lbxYear.selectedKey = 'key-1';
      this.view.letsverify.lbxMonth.selectedKey = 'm0';
      this.view.letsverify.lbxDate.selectedKey = "day0";
      this.view.ResetOrEnroll.tbxCVV.text="";
      this.view.letsverify.lblWrongInfo.setVisibility(false);
      this.disableButton(this.view.letsverify.btnProceed);
    },
    /**
  * Function to display error for fetch User name
  **/
    fetchUserNameErrResponse : function() {
      this.view.letsverify.lblWrongInfo.text = kony.i18n.getLocalizedString("i18n.login.wrongInfo");
      this.view.letsverify.lblWrongInfo.isVisible=true;
    },
    btnYesTakeSurvey: function() {
      var navObj = new kony.mvc.Navigation("frmCustomerFeedbackNotLogin");
      navObj.navigate();
    },
    btnNoTakeSurvey: function() {
      this.view.flxFeedbackTakeSurvey.setVisibility(false);
    },
    flxCrossTakeSurvey: function() {
      this.view.flxFeedbackTakeSurvey.setVisibility(false);
    },
    /**
 *Function to mask the user name
 */
    maskUserName : function(userName){
      var maskedData='';
      var firstThree = userName.substring(0, 3);
      var lastTwo = userName.substring(userName.length-2, userName.length);
      var xLength = userName.length-5;
      var maskString = '';
      for(i=0; i<xLength;i++)
      {
        maskString = maskString + '*';
      }
      maskedData=firstThree + maskString + lastTwo;
      return maskedData;
    },
    /**
 *Function to check whether the entered username is masked
 */
    checkMasked:function(enteredUserName){
      var userName = enteredUserName.substring(3,enteredUserName.length-2);
      for(i=0;i<userName.length;i++){
        if(userName.charAt(i) !== '*')
          return false;
      }
      return true; 
    },
    /**
  * Function to get the unmasked username from the local storage
  */
    getUnMaskedUserName:function(enteredUserName){
      var names = [];
      names = JSON.parse(localStorage.getItem("olbNames"));
      for (var key in names) {
        if (names.hasOwnProperty(key)) {
          var val = names[key];
          if(JSON.stringify(val).includes(enteredUserName))
          {  
            var pos = JSON.stringify(val).indexOf(':',1);
            return JSON.stringify(val).substring(2,pos-1);
          }
        }
      }
      return null;
    },

    /**
 * Function to check the username in local storage
 */
    chkUserInLocal: function() {
      var username = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      var usernames = localStorage.getItem('olbNames');
      usernames = JSON.parse(usernames);
      for (var index in usernames) {

        if (this.getUnMaskedUserName(usernames[index][username]) === username) {
          return true;
        }
      }
      return false;
    },
    /**
 * Function to mask the card Number
 * Parameter --- card number
 */
    maskCreditCardNumber:function(cardNumber){
      var maskedCreditNumber;
      var firstfour = cardNumber.substring(0, 4);
      var lastfour = cardNumber.substring(cardNumber.length-4, cardNumber.length);
      maskedCreditNumber=firstfour + "XXXXXXXX" + lastfour;
      return maskedCreditNumber;
    },

    /**
   *function to go to passwordResetOptions Page
  */

    goToPasswordResetOptionsPage:function(){
      this.presenter.goToPasswordResetOptionsPage(this.getCardsSuccessCallBack,this.getCardsErrorCallBack);
    },

    /**
    *function to handle whether to display the CVV Option  depending on the card details
  */

    getCardsSuccessCallBack:function(cardsJSON){
      if(cardsJSON.length!==0){
        this.showCVVOption();
        this.showCVVCards(cardsJSON);
      }
      else
      {
        this.hideCVVOption();
      }
    },

    /**
    *function to handle whether to display the error if no cards are available
    *parameters response
  */

    getCardsErrorCallBack:function(response){
      this.presenter.navigateToServerDownScreen(); 
    },
    
    navigateToNewUserOnBoarding: function () {
            var nuoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NUOModule");
            nuoModule.presentationController.showNewUserOnBoarding();
    }
  }
});