define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants){
    return {
        /**
          * willUpdateUI :This function is called if shouldUpdateUI returns true
          * @member of {frmEnrollNowController}
          * @param {context} depending on the context the appropriate function is executed
          * @return {} 
          * @throws {}
         */
        willUpdateUI: function (context) {
            if (context.userDetails) {
                if (context.action === "ServerDown") {
                    this.showServerErrorPage(this.view.flxVerification);
                }
                this.view.flxVerification.setVisibility(false);
                this.view.flxResetPasswordOptions.setVisibility(true);
            }
            if (context.action === "Navigate to Enroll") {
                this.verifyUser();
            }
            if (context.action === "Verify User Success") {
                this.verifyUserSuccessCallBack(context.data);
            }
            if (context.action === "Verify User Failure") {
                this.verifyUserErrorCallBack(context.data);
            }
            if (context.action === "Password Reset Success") {
                this.getCardsSuccessCallBack(context.data);
            }
            if (context.action === "Password Reset Failure") {
                this.getCardsErrorCallBack(context.data);
            }
            if (context.action === "CVV Validate Success") {
                this.CVVValidateResponseSuccess(context.data);
            }
            if (context.action === "CVV Validate Failure") {
                this.CVVValidateResponseFailure(context.data);
            }
            if (context.action === "OTP Validate Success") {
                this.OTPValidateResponseSuccess(context.data);
            }
            if (context.action === "OTP Validate Failure") {
                this.OTPValidateResponseFailure(context.data);
            }
            if (context.action === "OTP Response Success") {
                this.requestOTPResponseSuccess(context.data);
            }
            if (context.action === "OTP Response Failure") {
                this.requestOTPResponseFailure(context.data);
            }
            if (context.action === "OTP Resend Success") {
                this.resendOTPResponseSuccess(context.data);
            }
            if (context.action === "OTP Resend Failure") {
                this.resendOTPResponseFailure(context.data);
            }
            if (context.action === "Create User Success") {
                this.createUserPasswordResponseSuccess(context.data);
            }
            if (context.action === "Create User Failure") {
                this.createUserPasswordResponseFailure(context.data);
            }
            if (context.action === "Fetch Questions") {
                this.staticSetQuestions(context.data.context, context.data.response);
            }
            if (context.action === "Save Questions Success") {
                this.saveSQCallback(context.data);
            }
            if (context.action === "Save Questions Failure") {
                this.saveSQfailureCallback(context.data);
            }
            this.view.forceLayout();
        },
        /**
           * showDates :This function sets the Date Of Birth 
           * @member of {frmEnrollNowController}
           * @param {}   
           * @return {}  
           * @throws {}
          */
        showDates: function () {
            this.setNoOfDays();
            this.allFieldsCheck();
        },
        /**
           * securityQuesAnsProceed :This function sets the UI for securityQuestions flex  
           * @member of {frmEnrollNowController}
           * @param {}   
           * @return {}  
           * @throws {}
          */
      securityQuesAnsProceed: function () {
        var self = this;
        this.view.flxSecurityQuestionsAck.isVisible = false;
        this.view.flxVerification.isVisible = true;
        this.view.flxVerification.letsverify.isVisible=true;
        this.view.forceLayout();
        this.goToLogin();
      },


        /**
        * showServerErrorPage :This function is for showing the server DownTime Screen
        * @member of {frmEnrollNowController}
        * @param {currrentFlex}  
        * @return {}  
        * @throws {}
       */
        showServerErrorPage: function (currentFlex) {
            currentFlex.isVisible = false;
            this.view.flxEnrollOrServerError.isVisible = true;
            this.view.forceLayout();
        },

        /**
       * matchPwdKeyUp :The function is used to confirm whether the entered password and renter password are same or not
       * @member of {frmEnrollNowController}
       * @param {}  
       * @return {} 
       * @throws {}
      */
        matchPwdKeyUp: function () {
            if (this.view.newUsernamepasswordsetting.tbxMatchPassword.text !== "") {
                this.validateConfirmPassword(this.view.newUsernamepasswordsetting.tbxMatchPassword.text);
            }
        },

        /**
        * passwordEditing :The function is called when the user enters the wrong password and gives an error that password doesnot meet the required criteria 
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {} 
        * @throws {}
       */
        passwordEditing: function () {
            this.showRulesPassword();
            if (this.view.newUsernamepasswordsetting.lblErrorInfo.isVisible) {
                this.reEnterNewPassword();
            }
        },

        /**
            * newUsernameValidation :The function is internally calls other function which validates the user
            * @member of {frmEnrollNowController}
            * @param {}  
            * @return {} 
            * @throws {}
           */
        newUsernameValidation: function () {
            this.ValidateUserName(this.view.newUsernamepasswordsetting.tbxNewUserName.text);
        },

        /**
            * newPwdValidation :The function is internally calls other function which validates the password
            * @member of {frmEnrollNowController}
            * @param {}  
            * @return {} 
            * @throws {}
           */
        newPwdValidation: function () {
            this.validateNewPassword(this.view.newUsernamepasswordsetting.tbxNewPassword.text);
        },

        /**
         * Function to be called on post show of the frmEnrollNow
         */

        onPostShow: function () {
            this.setmainflexheight();
            this.loginConstructor();
            this.onLoadChangePointer();
            this.setFlowActions();
            this.view.btnLocateUs.onClick = function () {
                var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
                locateUsModule.presentationController.showLocateUsPage();
            };
            this.view.btnFaqs.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showOnlineHelp();
            };
            this.view.btnContactUs.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showContactUsPage();
            };
            this.view.btnPrivacy.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showPrivacyPolicyPage();
            };
            this.view.btnTermsAndConditions.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showTermsAndConditions();
            };
            this.view.flxClose.onClick = function() {
                var ntf = new kony.mvc.Navigation("frmLogin");
                ntf.navigate();
            }; 
            this.view.newUsernamepasswordsetting.tbxNewUserName.onBeginEditing = this.showRulesUsername;
            this.view.newUsernamepasswordsetting.tbxNewUserName.onKeyUp = this.newUsernameValidation;//ValidateUserName(this.view.newUsernamepasswordsetting.tbxNewUserName.text);
            this.view.newUsernamepasswordsetting.tbxNewPassword.onBeginEditing = this.passwordEditing;
            this.view.newUsernamepasswordsetting.tbxNewPassword.onKeyUp = this.newPwdValidation;//validateNewPassword(this.view.newpasswordsetting.tbxNewPassword.text);
            this.view.newUsernamepasswordsetting.tbxMatchPassword.onKeyUp = this.matchPwdKeyUp;
            this.view.newUsernamepasswordsetting.btnCreate.onClick = this.showResetConfirtoResetPasswordmationPage;
            this.view.letsverify.lbxMonth.onSelection = this.showDates;
            this.view.letsverify.lbxYear.onSelection = this.showDates;
            this.disableButton(this.view.letsverify.btnProceed);
            this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            this.disableButton(this.view.ResetOrEnroll.btnNext);
            this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            this.disableButton(this.view.SetSecurityQuestions.btnSetSecurityQuestionsProceed);
        },

        /**
        * setFlowActions :The function contains the list of actions
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {} 
        * @throws {}
       */
        setFlowActions: function () {
            var scopeObj = this;
            this.view.letsverify.flxWhatIsSSN.onClick = function () {
                if (scopeObj.view.AllForms.isVisible === false)
                    scopeObj.view.AllForms.setVisibility(true);
                else
                    scopeObj.view.AllForms.setVisibility(false);
            };
            this.view.AllForms.flxCross.onClick = function () {
                scopeObj.view.AllForms.setVisibility(false);
            };
        },
        setmainflexheight: function () {
            var height = kony.os.deviceInfo().screenWidth * 0.5652;
            this.view.flxMain.height = height + "dp";
        },
        loginConstructor: function () {
            this.wrongCredentials = 0;
            this.languages = [{
                lblLang: kony.i18n.getLocalizedString("i18n.common.English"),
                lblSeparator: "lbl"
            },
            {
                lblLang: kony.i18n.getLocalizedString("i18n.common.Spanish"),
                lblSeparator: "lbl"
            },
            {
                lblLang: kony.i18n.getLocalizedString("i18n.common.German"),
                lblSeparator: "lbl"
            },
            {
                lblLang: kony.i18n.getLocalizedString("i18n.common.Russian"),
                lblSeparator: "lbl"
            },
            {
                lblLang: kony.i18n.getLocalizedString("i18n.common.Chinese"),
                lblSeparator: "lbl"
            },
            {
                lblLang: kony.i18n.getLocalizedString("i18n.common.Japanese"),
                lblSeparator: "lbl"
            },
            {
                lblLang: kony.i18n.getLocalizedString("i18n.common.French"),
                lblSeparator: "lbl"
            }
            ];
        },

        /**
        * verifyUser :This function is called once the user clicks on enroll button then enroll form visiblilty is set to false and verification flex visibility is set to true
        * @member of {frmEnrollNowController}
        * @param {} 
        * @return {}  
        * @throws {}
       */
        verifyUser: function () {
            this.view.flxLogoutMsg.setVisibility(false);
            this.view.flxMain.skin = "sknFlexLoginErrorBackgroundKA";
            this.view.flxSecurityQuestionsAck.setVisibility(false);
            this.view.flxUsernameAndPasswordAck.setVisibility(false);
            this.view.flxVerification.setVisibility(true);
            this.view.flxVerification.letsverify.isVisible = true;
            this.view.forceLayout();
        },

       
        /**
          * goToResetUsingOTP :This function is for setting the visibility of the flxSendOTP as true on clicking of the "Reset using OTP" option
          * @member of {frmEnrollNowController}
          * @param {} 
          * @return {}  
          * @throws {}
         */
        goToResetUsingOTP: function () {
            this.view.flxResetPasswordOptions.isVisible = false;
            this.view.flxResetUsingCVV.isVisible = false;
            this.view.flxSendOTP.resetusingOTP.lblResendOTPMsg.text = "We are sending you the OTP on your registered mobile.You will not be charged for the same.";
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
            this.view.flxSendOTP.isVisible = true;
            this.view.ResetOrEnroll.tbxCVV.secureTextEntry = true; 
            this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = true;
            this.view.resetusingOTP.flxCVV.isVisible = false;
            this.view.resetusingOTP.btnNext.text = "SEND";
            this.view.forceLayout();
        },


        /**
        * showEnterOTPPage :This function shows the OTP flex once the user clicks on "Reset using the Secure Access Code" Option
        * @member of {frmEnrollNowController}
        * @param {} 
        * @return {}  
        * @throws {}
       */
        showEnterOTPPage: function () {
            var self=this;
            this.view.flxSendOTP.isVisible = false;
            this.view.resetusingOTPEnterOTP.flxCVV.isVisible = true;
            this.view.resetusingOTPEnterOTP.btnResendOTP.isVisible = true;
            this.view.resetusingOTPEnterOTP.lblResendOTPMsg.isVisible = false;
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = true;
            this.view.resetusingOTPEnterOTP.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.Enrollnow.ResendOTP");
            this.view.resetusingOTPEnterOTP.btnNext.top = "3.52%";
            this.view.resetusingOTPEnterOTP.btnUseCVV.top = "2.61%";
            this.view.resetusingOTPEnterOTP.btnNext.text = kony.i18n.getLocalizedString("i18n.common.next");
            this.view.flxResetUsingOTP.isVisible = true;
            self.view.resetusingOTPEnterOTP.btnResendOTP.setEnabled(false);
            self.view.resetusingOTPEnterOTP.btnResendOTP.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_DISABLED;
            self.view.resetusingOTPEnterOTP.btnResendOTP.hoverSkin = OLBConstants.SKINS.LOGIN_RESEND_OTP_DISABLED; 
            this.resendOtpTimer = setTimeout(
  		     function(){ 
        	  self.view.resetusingOTPEnterOTP.btnResendOTP.setEnabled(true);
          	  self.view.resetusingOTPEnterOTP.btnResendOTP.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_ENABLED;
              self.view.resetusingOTPEnterOTP.btnResendOTP.hoverSkin = OLBConstants.SKINS.LOGIN_RESEND_OTP_ENABLED;
        	}, 10000);    
            this.view.forceLayout();            
        },

        /**
        * showEnterCVVPage :This function shows the CVV flex once the user clicks on "Reset using the CVV" Option
        * @member of {frmEnrollNowController}
        * @param {} 
        * @return {}  
        * @throws {}
       */
        showEnterCVVPage: function () {
            this.view.flxResetPasswordOptions.isVisible = false;
            this.view.flxResetUsingCVV.isVisible = true;
            this.view.forceLayout();
        },

        /**
       * reEnterCVV :This function shows the error message if the user enters incorrect CVV
       * @member of {frmEnrollNowController}
       * @param {}  
       * @return {}  
       * @throws {}
      */
        reEnterCVV: function () {
            this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
            // this.view.ResetOrEnroll.imgCVV.top = "8.8%";
            this.view.forceLayout();
        },

        /**
        * showResetPasswordPage :This function shows Reset Password Flex from CVV
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        showResetPasswordPage: function () {
            if (!this.view.ResetOrEnroll.tbxCVV.text) {
                this.view.ResetOrEnroll.lblWrongCvv.isVisible = true;
                this.view.forceLayout();
            } else {
                this.view.flxResetUsingCVV.isVisible = false;
                this.view.flxUsernameAndPassword.isVisible = true;
                this.view.forceLayout();
            }
        },

         getUserNamePolicies:function(){
            var rules=[];
           var data=kony.mvc.MDAApplication.getSharedInstance().appContext.usernamepasswordrules;
          if(data){
            for (i = 0; i < data.length; i++) {
                var policyName = data[i].policyName;
                if (CommonUtilities.substituteforIncludeMethod(policyName, "Username Policy for End Customers")) {
                    rules.push(data[i].policyDescription);
                }
            }
            if (rules.length >= 0) {
                this.view.newUsernamepasswordsetting.rtxRulesUsername.text = rules;
            }
          }
           else{
             CommonUtilities.showServerDownScreen();
           } 
           
        },
      
      getPasswordPolicies:function(){
         var data=[];
         var rules = [];
        data=kony.mvc.MDAApplication.getSharedInstance().appContext.usernamepasswordrules;
        if(data){
           for (i = 0; i < data.length; i++) {
                var policyName = data[i].policyName;
                if (CommonUtilities.substituteforIncludeMethod(policyName, "Password Policy for End Customers")) {
                    rules.push(data[i].policyDescription);
                }
            }
            if (rules.length >= 0) {
               this.view.newUsernamepasswordsetting.rtxRulesPassword.text = rules;
            }
        }
        else{
             CommonUtilities.showServerDownScreen();
        }
      
      },   
        /**
        * showRulesUsername :This function shows the username rules on click of username textbox
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        showRulesUsername: function () {
            this.view.newUsernamepasswordsetting.flxMain.height = "500dp";
            this.view.newUsernamepasswordsetting.flxRulesUsername.setVisibility(true);
            this.view.newUsernamepasswordsetting.flxRulesPassword.setVisibility(false);
            this.view.forceLayout();
        },

        /**
        * showRulesPassword :This function shows the password rules on click of password textbox
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        showRulesPassword: function () {
            this.view.newUsernamepasswordsetting.flxMain.height = "500dp";
            this.view.newUsernamepasswordsetting.flxRulesUsername.setVisibility(false);
            this.view.newUsernamepasswordsetting.flxRulesPassword.setVisibility(true);
            this.view.forceLayout();
        },

        /**
        * reEnterOTP :This function shows the error message if the user enters incorrect OTP
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        reEnterOTP: function () {
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
            this.view.forceLayout();
        },

        /**
        * showResetPasswordPageFromOTP :This function shows ResetPassword flex from CVV
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        showResetPasswordPageFromOTP: function () {
            this.view.flxResetUsingOTP.isVisible = false;
            this.view.flxUsernameAndPassword.isVisible = true;
            this.view.forceLayout();
        },

        /**
       * isPasswordValidAndMatchedWithReEnteredValue :This function checks whether the entered password and reenter Password are matched or not
       * @member of {frmEnrollNowController}
       * @param {}  
       * @return {boolean} true if they match,false if they do not match  
       * @throws {}
      */
        isPasswordValidAndMatchedWithReEnteredValue: function () {
            if (this.view.newUsernamepasswordsetting.tbxNewPassword.text && this.view.newUsernamepasswordsetting.tbxMatchPassword.text) {
                if (this.view.newUsernamepasswordsetting.tbxNewPassword.text === this.view.newUsernamepasswordsetting.tbxMatchPassword.text) {
                    return true;
                }
            }
            return false;
        },

        /**
       * showSetSecurityQuestions :This function sets the visibilty of the security questions flex to true
       * @member of {frmEnrollNowController}
       * @param {}  
       * @return {}  
       * @throws {}
      */
        showSetSecurityQuestions: function () {
            this.view.flxUsernameAndPasswordAck.setVisibility(false);
            this.view.flxSecurityQuestions.setVisibility(true);
            this.view.forceLayout();
        },

        /**
        * showSecurityQuestionsAck :This function sets the visibilty of the security questions Acknowledgement flex to true
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        showSecurityQuestionsAck: function () {
            this.view.flxSecurityQuestions.setVisibility(false);
            this.view.flxSecurityQuestionsAck.setVisibility(true);
            this.view.flxVerification.setVisibility(false);
            this.view.forceLayout();
        },

        /**
       * showUsernamePasswordAck :This function sets the visibilty of the UserName and password  Acknowledgement flex to true
       * @member of {frmEnrollNowController}
       * @param {}  
       * @return {}  
       * @throws {}
      */
        showUsernamePasswordAck: function () {
            this.view.flxSecurityQuestions.setVisibility(false);
            this.view.flxUsernameAndPassword.setVisibility(false);
            this.view.flxUsernameAndPasswordAck.setVisibility(true);
            this.view.forceLayout();
        },

        /**
       * showResetConfirmationPage :This function checks whether to show the password Confirmation Screen 
       * @member of {frmEnrollNowController}
       * @param {}  
       * @return {}  
       * @throws {}
      */
        showResetConfirmationPage: function () {
            var isPasswordValidAndMatched = this.isPasswordValidAndMatchedWithReEnteredValue();
            if (!isPasswordValidAndMatched) {
                this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                this.view.newUsernamepasswordsetting.flxNewPassword.top = "3.52%";
            } else {
                this.view.flxUsernameAndPassword.isVisible = false;
                this.view.flxVerification.isVisible = false;
                this.view.passwordresetsuccess.lblReserSuccessMsg.skin = "sknLblLato42424217px";
                this.view.flxUsernameAndPasswordAck.isVisible = true;
                this.view.forceLayout();
            }
        },

        /**
        * useCVVForReset :This function is for selecting the CVV option from the OTP screen
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        useCVVForReset: function () {
            this.view.flxResetUsingOTP.isVisible = false;
            this.view.flxSendOTP.isVisible = false;
            this.view.flxResetUsingCVV.isVisible = true;
            this.view.resetusingOTPEnterOTP.tbxCVV.text = "";
            this.view.ResetOrEnroll.tbxCVV.secureTextEntry = true; 
            this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = true;
            this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
            this.view.forceLayout();
        },

        /**
          * useOTPForReset :This function is for selecting the OTP option from the CVV screen
          * @member of {frmEnrollNowController}
          * @param {}  
          * @return {}  
          * @throws {}
         */
        useOTPForReset: function () {
            this.view.flxResetUsingCVV.isVisible = false;
            this.view.flxSendOTP.isVisible = true;
            this.view.ResetOrEnroll.tbxCVV.text = "";
            this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
            this.disableButton(this.view.ResetOrEnroll.btnNext);
            this.view.forceLayout();
        },
        onLoadChangePointer: function () {
            var elems = document.querySelectorAll("input[kwidgettype='Button']");
            for (var i = 0, iMax = elems.length; i < iMax; i++) {
                elems[i].style.cursor = 'pointer';
            }
        },


        /**
           * verifyUserDetails :This function calls the function which fetches the user based on the details entered
           * @member of {frmEnrollNowController}
           * @param {}   
           * @return {}  
           * @throws {}
          */
        verifyUserDetails: function () {
            var year = [];
            var month = [];
            var date = [];
            var SSN = this.view.letsverify.tbxSSN.text;
            var LastName = CommonUtilities.changedataCase(this.view.letsverify.tbxLastName.text);
            year = this.view.letsverify.lbxYear.selectedKeyValue;
            month = this.view.letsverify.lbxMonth.selectedKeyValue;
            var mon = month[0].slice(1, 3);
            if (mon.length < 2)
                mon = '0' + mon;
            date = this.view.letsverify.lbxDate.selectedKeyValue;
            var day = date[1];
            if (day < 10)
                day = '0' + day;
            var dateStr = year[1] + '-' + mon + '-' + day;
            CommonUtilities.showProgressBar(this.view);
            this.presenter.verifyUser({
                "ssn": SSN,
                "userlastname": LastName,
                "dateOfBirth": dateStr
            });
            this.view.forceLayout();
        },

        /**
          * verifyUserSuccessCallBack :This function sets the visibility of the passwordResetOptionsPage to true if the fetching of the username is Successfull
          * @member of {frmEnrollNowController}
          * @param {response} response contains the data from the backend   
          * @return {}  
          * @throws {}
         */
        verifyUserSuccessCallBack: function (response) {
            CommonUtilities.hideProgressBar(this.view);
            this.goToPasswordResetOptionsPage();
        },

        /**
        * verifyUserErrorCallBack :This function sets the visibility of the passwordResetOptionsPage to false if the fetching of the username is Successfull
        * @member of {frmEnrollNowController}
        * @param {response} response contains the data from the backend   
        * @return {}  
        * @throws {}
       */
        verifyUserErrorCallBack: function (response) {
            CommonUtilities.hideProgressBar(this.view);
            this.view.letsverify.lblWrongInfo.text = kony.i18n.getLocalizedString("i18n.login.wrongInfo");
            this.view.letsverify.lblWrongInfo.isVisible = true;
        },


  	/**
    * welcomeVerifiedUser :This function is showing the flxResetUsingCVV once all the details are filled and proceed button is clicked
    * @member of {frmEnrollNowController}
    * @param {response} response is either success or failure dependding on the details entered 
    * @return {}  
    * @throws {}
   */
        welcomeVerifiedUser: function (response) {
            CommonUtilities.hideProgressBar(this.view);
            this.emptyUserDetails();
            this.view.flxResetUsingCVV.isVisible = true;
            this.view.forceLayout();
        },

        /**
        * emptyUserDetails :This function empties all the user details entered and disable the login button
        * @member of {frmEnrollNowController}
        * @param {}   
        * @return {}  
        * @throws {}
       */
        emptyUserDetails: function() {
            this.view.letsverify.tbxSSN.text = "";
            this.view.letsverify.tbxLastName.text = "";
            this.view.letsverify.lbxYear.selectedKey = 'key-1';
            this.view.letsverify.lbxMonth.selectedKey = 'm0';
            this.view.letsverify.lbxDate.selectedKey = "day0";
            this.view.ResetOrEnroll.tbxCVV.text = "";
            this.view.letsverify.lblWrongInfo.setVisibility(false);
            this.view.resetusingOTP.lblWrongOTP.setVisibility(false);
            this.view.resetusingOTP.tbxCVV.text="";
            this.view.resetusingOTPEnterOTP.lblWrongOTP.setVisibility(false);
            this.view.resetusingOTPEnterOTP.tbxCVV.text="";
            this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
            this.view.ResetOrEnroll.tbxCVV.text="";
            this.disableButton(this.view.ResetOrEnroll.btnNext);
            this.view.newpasswordsetting.tbxNewUserName.text="";
            this.view.newpasswordsetting.tbxNewPassword.text="";
            this.view.newpasswordsetting.tbxMatchPassword.text="";
            this.disableButton(this.view.newpasswordsetting.btnNext);
            this.view.newpasswordsetting.lblErrorInfo.setVisibility(false);
            this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.setVisibility(false);
            this.view.newUsernamepasswordsetting.tbxNewUserName.text="";
            this.view.newUsernamepasswordsetting.tbxNewPassword.text="";
            this.view.newUsernamepasswordsetting.tbxMatchPassword.text="";
            this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            this.disableButton(this.view.letsverify.btnProceed);
             var ntf = new kony.mvc.Navigation("frmLogin");
                ntf.navigate();
            this.view.forceLayout();
        },

        /**
         * disableButton :This function is used to set the disabled skin for a button
         * @member of {frmEnrollNowController}
         * @param {buttonName} 
         * @return {} 
         * @throws {}
        */
        disableButton: function (button) {
            button.skin = "sknBtnBlockedLatoFFFFFF15Px";
            button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
            button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
            button.setEnabled(false);
        },

        /**
         * enableButton :This function is used to set the enabled skin for a button
         * @member of {frmEnrollNowController}
         * @param {buttonName} 
         * @return {} 
         * @throws {}
        */
        enableButton: function (button) {
            button.skin = "sknBtnNormalLatoFFFFFF15Px";
            button.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
            button.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
            button.setEnabled(true);
        },

        /**
        * isSSNValid :This function validates whether the SSN is correct or not (9 digits and no characters etc..)
        * @member of {frmEnrollNowController}
        * @param {input} the SSN entered by the user  
        * @return {boolean} true-if SSN is valid ,false- if SSN is invalid 
        * @throws {}
       */
         isSSNValid: function(input) {
            var ssn = /^[0-9]*$/;
            if (input.match(ssn) && input !== "") {
                return true;
            }
            return false;
        },

        /**
          * allFieldsCheck :This function enables the Proceed button only if all the fields(SSN,DOB,lastName) are correct
          * @member of {frmEnrollNowController}
          * @param {}   
          * @return {}  
          * @throws {}
         */
        allFieldsCheck: function () {
            var year = [];
            var month = [];
            var date = [];
            var SSN = this.view.letsverify.tbxSSN.text;
            var lastName = this.view.letsverify.tbxLastName.text;
            year = this.view.letsverify.lbxYear.selectedKeyValue;
            month = this.view.letsverify.lbxMonth.selectedKeyValue;
            date = this.view.letsverify.lbxDate.selectedKeyValue;
            if (this.isSSNValid(SSN) && lastName !== "" && year[1] != "Year" && month[1] != "Month" && date[1] != "Day" && SSN.length == 9) {
                this.enableButton(this.view.letsverify.btnProceed);
            } else {
                this.disableButton(this.view.letsverify.btnProceed);
            }
        },

        /**
         * ssnCheck :This function used to check whether the SSN entered is correct or not ..If it is wrong the error message is shown
         * @member of {frmEnrollNowController}
         * @param {}  
         * @return {}  
         * @throws {}
        */
       ssnCheck: function() {
        var input = this.view.letsverify.tbxSSN.text;
          if(input.trim() != ""){
          if(this.isSSNValid(input.trim())) {
              if(input.length >9 ) {
            
                this.view.letsverify.lblWrongInfo.text = kony.i18n.getLocalizedString("i18n.login.incorrectSSN");
                this.view.letsverify.lblWrongInfo.isVisible = true; }
             else
                 this.view.letsverify.lblWrongInfo.isVisible = false;
          }
          else{
              this.view.letsverify.lblWrongInfo.text = kony.i18n.getLocalizedString("i18n.login.incorrectSSN");
              this.view.letsverify.lblWrongInfo.isVisible = true;
          }

      }
      else {
             this.view.letsverify.lblWrongInfo.isVisible = false; 
          }
           this.view.forceLayout();
        },

        /**
          * cvvCheck :This function checks whether the entered CVV  is valid or not
          * @member of {frmEnrollNowController}
          * @param {}  
          * @return {}  
          * @throws {}
         */
        cvvCheck: function() {
            var input = this.view.ResetOrEnroll.tbxCVV.text;
            var cvv = /^[0-9]*$/;
            if(input.trim() != ""){
                  if(input.match(cvv)) {
              if(input.length >3 ) {
                 
                 this.view.ResetOrEnroll.lblWrongCvv.text = kony.i18n.getLocalizedString("i18n.login.IncorrectCVV");
                 this.disableButton(this.view.ResetOrEnroll.btnNext);
                 this.view.ResetOrEnroll.lblWrongCvv.isVisible = true;
            }
             else {
                 this.view.ResetOrEnroll.lblWrongCvv.isVisible = false;
             
             if(input.length===3)
                 this.enableButton(this.view.ResetOrEnroll.btnNext); 
             
             else 
                 this.disableButton(this.view.ResetOrEnroll.btnNext);  
          }
      }
          else{
                this.view.ResetOrEnroll.lblWrongCvv.text = kony.i18n.getLocalizedString("i18n.login.IncorrectCVV");
                 this.disableButton(this.view.ResetOrEnroll.btnNext);
                 this.view.ResetOrEnroll.lblWrongCvv.isVisible = true;
          }
        }
              else {
              this.view.ResetOrEnroll.lblWrongCvv.isVisible = false; 
              this.disableButton(this.view.ResetOrEnroll.btnNext);
          }
          this.view.forceLayout();
        },
        /**
        * otpCheck :This function checks the OTP entered is correct or not and shows the error message if it is incorrect
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        otpCheck: function() {
             var input = this.view.resetusingOTPEnterOTP.tbxCVV.text;
            var otp = /^[0-9]*$/;
            if(input.trim() != ""){
                  if(input.match(otp)) {
              if(input.length >6 ) {
                 this.view.resetusingOTPEnterOTP.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.login.incorrectOTP");
                 this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
                 this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = true;
            }
             else {
                 this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false;
             
             if(input.length===6)
                 this.enableButton(this.view.resetusingOTPEnterOTP.btnNext); 
             
             else 
                 this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);  
          }
      }
          else{
                this.view.resetusingOTPEnterOTP.lblWrongOTP.text = kony.i18n.getLocalizedString("i18n.login.incorrectOTP");
                 this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
                 this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = true;
          }
        }
              else {
              this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = false; 
              this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
          }
          this.view.forceLayout();
        },

        /**
        * showCVV :This function shows the masked CVV on click of eye icon
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        showCVV: function () {
          if(this.view.ResetOrEnroll.tbxCVV.secureTextEntry === true){
            this.view.ResetOrEnroll.tbxCVV.secureTextEntry = false;
          }else{
            this.view.ResetOrEnroll.tbxCVV.secureTextEntry = true;
          }
            
        },

        

        /**
          * showOTP :This function shows the masked OTP on click of eye icon
          * @member of {frmEnrollNowController}
          * @param {}  
          * @return {}  
          * @throws {}
         */
        showOTP: function () {
            if( this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry === true){
                this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = false;
            }else{
                this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = true;
            }
            
        },

       


        /**
        * goToPasswordResetOptionsPage :This function calls the function to retrieve the cards for the user
        * @member of {frmEnrollNowController}
        * @param {} 
        * @return {}  
        * @throws {}
       */
        goToPasswordResetOptionsPage: function () {
            var year = [];
            var month = [];
            var date = [];
            var SSN = this.view.letsverify.tbxSSN.text;
            var LastName = this.view.letsverify.tbxLastName.text;
            year = this.view.letsverify.lbxYear.selectedKeyValue;
            month = this.view.letsverify.lbxMonth.selectedKeyValue;
            var mon = month[0].slice(1, 3);
            if (mon.length < 2)
                mon = '0' + mon;
            date = this.view.letsverify.lbxDate.selectedKeyValue;
            var day = date[1];
            if (day < 10)
                day = '0' + day;
            var dateStr = year[1] + '-' + mon + '-' + day;
            this.emptyUserDetails();
            this.presenter.goToPasswordResetOptionsPage({
                "ssn": SSN,
                "userlastname": LastName,
                "dateOfBirth": dateStr
            });
            this.view.forceLayout();
        },

        /**
           * setNoOfDays :This function sets the Date Of Birth (day,month and year)
           * @member of {frmEnrollNowController}
           * @param {}   
           * @return {}  
           * @throws {}
          */
        setNoOfDays: function () {
            var selectedMonth = this.view.letsverify.lbxMonth.selectedKey;
            var dayArray = [];

            dayArray = [{ "daykey": "day0", "dayvalue": "Day" }, { "daykey": "day1", "dayvalue": 1 }, { "daykey": "day2", "dayvalue": 2 }, { "daykey": "day3", "dayvalue": 3 },
            { "daykey": "day4", "dayvalue": 4 }, { "daykey": "day5", "dayvalue": 5 }, { "daykey": "day6", "dayvalue": 6 },
            { "daykey": "day7", "dayvalue": 7 }, { "daykey": "day8", "dayvalue": 8 }, { "daykey": "day9", "dayvalue": 9 },
            { "daykey": "day10", "dayvalue": 10 }, { "daykey": "day11", "dayvalue": 11 }, { "daykey": "day12", "dayvalue": 12 },
            { "daykey": "day13", "dayvalue": 13 }, { "daykey": "day14", "dayvalue": 14 }, { "daykey": "day15", "dayvalue": 15 },
            { "daykey": "day16", "dayvalue": 16 }, { "daykey": "day17", "dayvalue": 17 }, { "daykey": "day18", "dayvalue": 18 },
            { "daykey": "day19", "dayvalue": 19 }, { "daykey": "day20", "dayvalue": 20 }, { "daykey": "day21", "dayvalue": 21 },
            { "daykey": "day22", "dayvalue": 22 }, { "daykey": "day23", "dayvalue": 23 }, { "daykey": "day24", "dayvalue": 24 },
            { "daykey": "day25", "dayvalue": 25 }, { "daykey": "day26", "dayvalue": 26 }, { "daykey": "day27", "dayvalue": 27 },
            { "daykey": "day28", "dayvalue": 28 }
            ];
            var dayV, dayK;
            var dayObj;
            if (selectedMonth === 'm1' || selectedMonth === 'm3' || selectedMonth === 'm5' || selectedMonth === 'm7' || selectedMonth === 'm8' || selectedMonth === 'm10' || selectedMonth === 'm12') {

                for (var i = 29; i <= 31; i++) {
                    dayK = "day" + i;
                    dayV = i;
                    dayObj = { "daykey": dayK, "dayvalue": dayV };
                    dayArray.push(dayObj);
                }
            }
            else if (selectedMonth === 'm4' || selectedMonth === 'm6' || selectedMonth === 'm9' || selectedMonth === 'm11') {

                for (var j = 29; j <= 30; j++) {
                    dayK = "day" + j;
                    dayV = j;
                    dayObj = { "daykey": dayK, "dayvalue": dayV };
                    dayArray.push(dayObj);
                }
            }
            else {
                if ((this.view.letsverify.lbxYear.selectedKeyValue !== undefined) && (this.view.letsverify.lbxYear.selectedKeyValue !== null)) {
                    var thisYear = this.view.letsverify.lbxYear.selectedKeyValue[1];
                    if (((thisYear % 4 === 0) && (thisYear % 100 !== 0)) || (thisYear % 400 === 0)) {
                        dayK = "day" + 29;
                        dayV = 29;
                        dayObj = { "daykey": dayK, "dayvalue": dayV };
                        dayArray.push(dayObj);
                    }
                }
            }
            this.view.letsverify.lbxDate.masterDataMap = [dayArray, "daykey", "dayvalue"];
        },
        /**
         * showCVVCards :This function prepares the list of cards available for the user and stores in the form of key and value where the key is the actual card number and value is the masked card number
         * @member of {frmEnrollNowController}
         * @param {presentCards} list of cards available for th user 
         * @return {}  
         * @throws {}
        */
        showCVVCards: function (presentCards) {
            var x = [];
            this.view.ResetOrEnroll.lstbxCards.masterData = x;
            var count = 0;
            for (var index in presentCards) {
                for (var attr in presentCards[index]) {
                    var value = presentCards[index][attr];
                    var y = [];
                    y.push(count, value);
                    count++;
                    x.push(y);
                }
            }
            this.view.ResetOrEnroll.lstbxCards.masterData = x;
        },

        /**
          * showCVVOption :This function is for showing the CVV option if there are  cards available for the user
          * @member of {frmEnrollNowController}
          * @param {} 
          * @return {}  
          * @throws {}
         */
        showCVVOption: function () {
            this.view.flxResetPasswordOptions.isVisible = true;
            this.view.resetusingOTP.btnUseCVV.isVisible = true;
            this.view.resetusingOTP.orline.isVisible = true;
            this.view.AlterneteActionsEnterCVV.isVisible = true;
              this.getUserNamePolicies();
            this.view.OrLineForCVVandPIN.isVisible = true;
            this.view.flxResetPasswordOptions.isVisible = true;
            this.view.forceLayout();
        },

  	/**
    * hideCVVOption :This function is for hiding the CVV option if there are no cards available for the user
    * @member of {frmEnrollNowController}
    * @param {} 
    * @return {}  
    * @throws {}
   */
        hideCVVOption: function () {
            this.view.flxResetPasswordOptions.isVisible = true;
            this.view.AlterneteActionsEnterCVV.isVisible = false;
            this.view.OrLineForCVVandPIN.isVisible = false;
              this.getUserNamePolicies();
            this.view.resetusingOTP.btnUseCVV.isVisible = false;
            this.view.resetusingOTP.orline.isVisible = false;
            this.view.forceLayout();
        },
        /**
      * preshowFrmLogin :Pre show function of login pafe
      * @member of {frmEnrollNowController}
      * @param {} 
      * @return {}  
      * @throws {}
     */
      preshowFrmLogin: function() {
            var scopeObj = this;

            function timerFunc() {
                if (kony.os.deviceInfo().screenHeight > 768) {
                    scopeObj.view.flxMain.height = kony.os.deviceInfo().screenHeight + "dp";
                } else scopeObj.view.flxMain.height = "768dp";
                scopeObj.view.forceLayout();
            }
            kony.timer.schedule("screenHeightTimer", timerFunc, 0.01, true);
            this.view.flxVerification.isVisible = true;
            this.view.flxResetPasswordOptions.isVisible = false;
             this.view.newUsernamepasswordsetting.rtxRulesUsername.text="";
             this.view.newUsernamepasswordsetting.rtxRulesPassword.text="";
            this.view.flxSendOTP.isVisible = false;
            this.view.flxResetUsingOTP.isVisible = false;
            this.view.flxResetUsingCVV.isVisible = false;
            this.view.flxResetPassword.isVisible = false;
            this.view.flxUsernameAndPassword.isVisible = false;
            this.view.flxSecurityQuestions.isVisible = false;
            this.view.flxSecurityQuestionsAck.isVisible = false;
            this.view.flxUsernameAndPasswordAck.isVisible = false;
            this.view.flxusernamepassword.isVisible = false;
            this.view.flxResetSuccessful.isVisible = false;
            this.view.flxEnrollOrServerError.isVisible = false;
            this.view.imgClose.onTouchEnd = this.emptyUserDetails.bind(this);
            this.view.imgCloseSendOTP.onTouchEnd=this.emptyUserDetails.bind(this);
            this.view.imgCloseResetPassword.onTouchEnd=this.emptyUserDetails.bind(this);
            this.view.imgCloseResetUsingOTP.onTouchEnd=this.emptyUserDetails.bind(this);
            this.view.imgCloseResetUsingCVV.onTouchEnd=this.emptyUserDetails.bind(this);
            this.view.imgCloseResetPswd.onTouchEnd=this.emptyUserDetails.bind(this);
            this.view.imgCloseUsernamePassowrd.onTouchEnd=this.emptyUserDetails.bind(this);

            this.presenter.getUserNamePolicies();
            this.view.newUsernamepasswordsetting.flxNewUserName.onTouchStart = this.getUserNamePolicies.bind(this);
            this.view.newUsernamepasswordsetting.flxNewPassword.onTouchStart = this.getPasswordPolicies.bind(this);
        },
        /**
      * goToLogin :This function navigates to login page
      * @member of {frmEnrollNowController}
      * @param {} 
      * @return {}  
      * @throws {}
     */
        goToLogin: function () {
            var ntf = new kony.mvc.Navigation("frmLogin");
            ntf.navigate();
            kony.timer.cancel("screenHeightTimer");
            this.view.forceLayout();
        },
        /**
      * getCardsSuccessCallBack :This function retrieve the Cards of the user and calls the function to show the CVV option
      * @member of {frmEnrollNowController}
      * @param {} 
      * @return {}  
      * @throws {}
     */

        getCardsSuccessCallBack: function (cardsJSON) {
            CommonUtilities.hideProgressBar(this.view);
            if (cardsJSON.length !== 0) {
                this.showCVVOption();
                this.showCVVCards(cardsJSON);
            }
            else {
                this.hideCVVOption();
                this.view.forceLayout();
            }

        },

        /**
         * getCardsErrorCallBack :This function navigates to error screen if the cards are not fetched because of the server error
         * @member of {frmEnrollNowController}
         * @param {} 
         * @return {}  
         * @throws {}
        */

        getCardsErrorCallBack: function (response) {
            CommonUtilities.hideProgressBar(this.view);
            this.hideCVVOption();
            this.presenter.navigateToServerDownScreen();
        },

        /**
       * isCVVCorrect :This function checks whether the  CVV  is correct  or not for the selected card
       * @member of {frmEnrollNowController}
       * @param {}  
       * @return {}  
       * @throws {}
      */
        isCVVCorrect: function () {
            var maskedCardNumber = this.view.ResetOrEnroll.flxCards.lstbxCards.selectedKeyValue;
            var selCardNumber = JSON.stringify(maskedCardNumber);
            selCardNumber = selCardNumber.substring(selCardNumber.indexOf(",") + 1, selCardNumber.length - 1);
            var cvv = this.view.ResetOrEnroll.tbxCVV.text;
            CommonUtilities.showProgressBar(this.view);
            this.presenter.cvvValidate(selCardNumber, cvv);
        },

        /**
         * CVVValidateResponseSuccess :This function shows the Reset Password page if the entered cvv is valid and correct
         * @member of {frmEnrollNowController}
         * @param {}  
         * @return {}  
         * @throws {}
        */

        CVVValidateResponseSuccess: function () {
            CommonUtilities.hideProgressBar(this.view);
            this.view.letsverify.isVisible = false;
            this.showResetPasswordPage();
            this.view.forceLayout();
        },

        /**
         * CVVValidateResponseFailure :This function shows the error message if the entered cvv is incorrect
         * @member of {frmEnrollNowController}
         * @param {}  
         * @return {}  
         * @throws {}
        */

        CVVValidateResponseFailure: function () {
            CommonUtilities.hideProgressBar(this.view);
            this.view.letsverify.isVisible = false;
            this.showErrorForCVV();
            this.view.forceLayout();
        },

        /**
        * isOTPCorrect :This function calls the function which validates whether entered OTP is correct or not 
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        isOTPCorrect: function () {
            CommonUtilities.showProgressBar(this.view);
            var otp = this.view.resetusingOTPEnterOTP.tbxCVV.text;
            this.presenter.otpValidate(otp);
        },

        /**
         * OTPValidateResponseSuccess :This function shows the resetPasswordPage from the OTP page
         * @member of {frmEnrollNowController}
         * @param {}  
         * @return {}  
         * @throws {}
        */

        OTPValidateResponseSuccess: function () {
            CommonUtilities.hideProgressBar(this.view);
            this.view.letsverify.isVisible = false;
            this.view.resetusingOTPEnterOTP.tbxCVV.text = "";
            this.showResetPasswordPageFromOTP();
            this.view.forceLayout();
        },

        /**
         * OTPValidateResponseFailure :This function shows the error message if the entered OTP is wrong
         * @member of {frmEnrollNowController}
         * @param {}  
         * @return {}  
         * @throws {}
        */

        OTPValidateResponseFailure: function () {
            CommonUtilities.hideProgressBar(this.view);
            this.view.letsverify.isVisible = false;
            this.view.resetusingOTPEnterOTP.tbxCVV.text = "";
            this.showResetPasswordPageFromOTP();
            this.view.forceLayout();
        },

        /**
          * requestOTPValue :This function is for calling the other function in the presentation Controller for requesting OTP from the server
          * @member of {frmEnrolLNowController}
          * @param {}  
          * @return {}  
          * @throws {}
         */
        requestOTPValue: function () {
            CommonUtilities.showProgressBar(this.view);
            this.presenter.requestOTP();
        },

        /**
        * requestOTPResponseSuccess :This function is called once the OTP from the server is received i.e. success 
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        requestOTPResponseSuccess: function () {
            CommonUtilities.hideProgressBar(this.view);
            this.showEnterOTPPage();
            this.view.forceLayout();
        },

        /**
          * requestOTPResponseFailure :This function is called when there is request OTP is failed i.e. OTP is not received 
          * @member of {frmEnrollNowController}
          * @param {}  
          * @return {}  
          * @throws {}
         */
        requestOTPResponseFailure: function () {
            CommonUtilities.hideProgressBar(this.view);
        },

        /**
         * showErrorForOTP :This function shows error message if user enters the wrong OTP
         * @member of {frmEnrollNowController}
         * @param {}  
         * @return {}  
         * @throws {}
        */
        showErrorForOTP: function () {
            this.view.resetusingOTPEnterOTP.lblWrongOTP.isVisible = true;
            this.view.forceLayout();
        },

        /**
        * showForgotScreen :This function shows forgot Screen in login module
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        showForgotScreen: function () {
            var context = {
                "action": "fromEnrol"
            };
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.presentUserInterface("frmLogin", context);
        },

        /**
         * validateConfirmPassword :This function validates whether the entered password and the matched password are same or not
         * @member of {frmEnrollNowController}
         * @param {enteredPassword}  the password entered by the user
         * @return {}  
         * @throws {}
        */
        validateConfirmPassword: function (enteredPassword) {
            if (this.isPasswordValid(enteredPassword)) {

                if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                    if (!this.isUserNameValid(this.view.newUsernamepasswordsetting.tbxNewUserName.text)) {
                        this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = "Please choose a valid username";
                        this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                        this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
                    }
                    else {

                        this.enableButton(this.view.newUsernamepasswordsetting.btnCreate);
                    }
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = true;
                } else {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = "Password does not meet the required criteria";
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = true;
                    this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
                }
            } else {
                if (enteredPassword.length === 0) {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                }
                else {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = "Password does not meet the required criteria";
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                }
                this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            }
            this.view.forceLayout();
        },

        /**
       * validateUserName :This function validates whether the entered username is valid or not
       * @member of {frmEnrollNowController}
       * @param {enteredPassword}  the username entered by the user
       * @return {}  
       * @throws {}
      */
        ValidateUserName: function (enteredUserName) {
            if (this.isUserNameValid(enteredUserName)) {
                this.view.newUsernamepasswordsetting.imgCorrectUserName.isVisible = true;
                this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                if ((this.isPasswordValid(this.view.newUsernamepasswordsetting.tbxNewPassword.text)) && (this.isPasswordValid(this.view.newUsernamepasswordsetting.tbxMatchPassword.text))) {
                    if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                        this.enableButton(this.view.newUsernamepasswordsetting.btnCreate);
                    } else {
                        this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
                    }
                }
            } else {
                if (enteredUserName.length === 0) {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                }
                else {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = "Please choose a valid username";
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                }
                this.view.newUsernamepasswordsetting.imgCorrectUserName.isVisible = false;
                this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            }
            this.view.forceLayout();
        },

        /**
         * validateNewPassword :This function validates whether the entered password is correct or not
         * @member of {frmEnrollNowController}
         * @param {enteredPassword}  the password entered by the user
         * @return {}  
         * @throws {}
        */
        validateNewPassword: function (enteredPassword) {
            if (this.isPasswordValid(enteredPassword)) {
			    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
                this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = true;
                if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
                    if (!this.isUserNameValid(this.view.newUsernamepasswordsetting.tbxNewUserName.text)) {
                        this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = "Please choose a valid username";
                        this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                        this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
                    }
                    else {
                        this.enableButton(this.view.newUsernamepasswordsetting.btnCreate);
                    }
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = true;
                }
                else {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = "Password does not meet the required criteria";
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                    this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                    this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
                }
            }
            else {
                if (enteredPassword.length === 0) {
                    this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = false;
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;

                }
                else {
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = "Password does not meet the required criteria";
                    this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
                }
                this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = false;
                this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            }
            this.view.forceLayout();
        },
        /**
         * reEnterNewPassword :This function shows the error message if the entered password is wrong
         * @member of {frmEnrollNowController}
         * @param {}  
         * @return {}  
         * @throws {}
        */
        reEnterNewPassword: function () {
            this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = false;
            this.view.newUsernamepasswordsetting.flxNewPassword.top = "8.8%";
            this.view.forceLayout();

        },

        /**
         * isPasswordValid :This function checks whether the password is valid or not
         * @member of {frmEnrollNowController}
         * @param {enteredPassword}  the password entered by the user
         * @return {}  
         * @throws {}
        */
        isPasswordValid: function (enteredPassword) {
            var password = /^(?=.*\d)(?=.*[a-zA-Z])(?=.+[\!\#\$\%\(\*\)\+\,\-\;\=\?\@\[\\\]\^\_\'\{\}]).{8,20}$/;
            var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
            if (enteredPassword.match(password) && !this.hasConsecutiveDigits(enteredPassword) && enteredPassword != userName) {
                return true;
            }
            return false;
        },

        /**
       * hasConsecutiveDigits :This function checks whether there are 9 Consecutive Digits or not
       * @member of {frmEnrollNowController}
       * @param {input} input is the entered password  
       * @return {boolean}  true-- if there are no consecutive digits ,false-- if there are consecutive digits
       * @throws {}
      */
        hasConsecutiveDigits: function (input) {
            var i;
            var count = 0;
            for (i = 0; i < input.length; i++) {
                // alert(abc[i]);
                if (input[i] >= 0 && input[i] <= 9)
                    count++;
                else
                    count = 0;
                if (count === 9)
                    return true;
            }
            return false;
        },
        /**
       * checkDot :This function checks whether the username entered has dot or not
       * @member of {frmEnrollNowController}
       * @param {input} input is the entered password  
       * @return {boolean}  true-- if there are no consecutive digits ,false-- if there are consecutive digits
       * @throws {}
      */
        checkDot: function (enteredUserName) {
            if (enteredUserName.charAt(0) === "." || enteredUserName.charAt(enteredUserName.length - 1) === ".")
                return false;
            var index = enteredUserName.indexOf(".");
            for (var i = index + 1; i < enteredUserName.length - 1; i++) {
                if (enteredUserName[i] === ".") {
                    return false;
                }
            }
            return true;
        },


        /**
         * isUserNameValid :This function checks whether the username is valid or not
         * @member of {frmEnrollNowController}
         * @param {enteredUserName}  the username entered by the user
         * @return {boolean} true -- if the username is valid,false--if the username is not valid  
         * @throws {}
        */
        isUserNameValid: function (enteredUserName) {
            var userName = /^[a-zA-Z0-9.]+$/;
            //var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
            if ((enteredUserName.match(userName)) && (enteredUserName.length > 7) && (this.checkDot(enteredUserName))) {
                return true;
            }
            return false;
        },

        /**
          * showResetConfirtoResetPasswordmationPage :This function calls the function to create the username and password
          * @member of {frmEnrollNowController}
          * @param {}  
          * @return {}
          * @throws {}
         */
        showResetConfirtoResetPasswordmationPage: function () {
            var password = this.view.newUsernamepasswordsetting.tbxNewPassword.text;
            var userName = this.view.newUsernamepasswordsetting.tbxNewUserName.text;
            CommonUtilities.showProgressBar(this.view);
            this.presenter.createUser(userName, password);
            this.view.forceLayout();

        },

        /**
         * createUserPasswordResponseSuccess :This function is called when the creation of the user is successfull
         * @member of {frmEnrollNowController}
         * @param {response} response contains the userId   
         * @return {}
         * @throws {}
        */
        createUserPasswordResponseSuccess: function (response) {
            if (response.data.userId !== "") {
                this.view.flxUsernameAndPassword.setVisibility(false);
                if (CommonUtilities.getConfiguration("enrolSecurityQuestionsAvailable") === "true") {
                    this.view.flxUsernameAndPasswordAck.setVisibility(true);
                } else {
                    this.view.flxSecurityQuestionsAck.setVisibility(true);
                    this.view.securityAck.lblNotYetEnrolledOrError.text = kony.i18n.getLocalizedString("i18n.enrollNow.successfulEnrol");
                    this.view.securityAck.lblNotYetEnrolledOrError.isVisible = true;
                }
                this.view.newUsernamepasswordsetting.tbxNewUserName.text = "";
                this.view.newUsernamepasswordsetting.imgCorrectUserName.isVisible = false;
                this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = false;
                this.view.newUsernamepasswordsetting.tbxNewPassword.text = "";
                this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
                this.view.newUsernamepasswordsetting.tbxMatchPassword.text = "";
                this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
            } else {
                this.createUserPasswordResponseAlreadyEnrolled(response);
            }
            CommonUtilities.hideProgressBar(this.view);
            this.view.forceLayout();
        },

        /**
        * createUserPasswordResponseAlreadyEnrolled :This function is called when the creating the user whose is already enrolled
        * @member of {frmEnrollNowController}
        * @param {response} response contains the userId   
        * @return {}
        * @throws {}
       */
        createUserPasswordResponseAlreadyEnrolled: function (response) {
            this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.text = kony.i18n.getLocalizedString("i18n.enrollNow.wrongUserName");
            this.view.newUsernamepasswordsetting.lblPasswordDoesnotMatch.isVisible = true;
            this.view.newUsernamepasswordsetting.tbxNewUserName.text = "";
            this.view.newUsernamepasswordsetting.imgCorrectUserName.isVisible = false;
            this.view.newUsernamepasswordsetting.imgValidPassword.isVisible = false;
            this.view.newUsernamepasswordsetting.tbxNewPassword.text = "";
            this.view.newUsernamepasswordsetting.imgPasswordMatched.isVisible = false;
            this.view.newUsernamepasswordsetting.tbxMatchPassword.text = "";
            this.disableButton(this.view.newUsernamepasswordsetting.btnCreate);
        },

        /**
       * createUserPasswordResponseFailure :This function is called when the creation of the username is failed
       * @member of {frmEnrollNowController}
       * @param {response} response contains the userId   
       * @return {}
       * @throws {}
      */
        createUserPasswordResponseFailure: function (response) {
            this.presenter.navigateToServerDownScreen();
        },

        /**
        * showResetConfirmationScreen :This function  shows the password Confirmation Screen 
        * @member of {frmEnrollNowController}
        * @param {}  
        * @return {}  
        * @throws {}
       */
        showResetConfirmationScreen: function () {
            this.view.flxUsernameAndPassword.isVisible = false;
            this.view.flxUsernameAndPasswordAck.isVisible = true;
            this.view.forceLayout();

        },

        /**
         * resendOTPValue :This function is called when the user clicks on resend OTP button
         * @member of {frmEnrollNowController}
         * @param {} 
         * @return {}  
         * @throws {}
        */
        resendOTPValue: function () {
            CommonUtilities.showProgressBar(this.view);
            this.view.ResetOrEnroll.tbxCVV.secureTextEntry = true; 
            this.view.resetusingOTPEnterOTP.tbxCVV.secureTextEntry = true;
            this.presenter.resendOTP();
        },

        /**
        * resendOTPResponseSuccess :This function is called when the Resend OTP is Success
        * @member of {frmEnrollNowController}
        * @param {} 
        * @return {}  
        * @throws {}
       */
        resendOTPResponseSuccess: function () {
            var self=this;
            CommonUtilities.hideProgressBar(this.view);
            this.view.resetusingOTPEnterOTP.tbxCVV.text="";
            this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);            
            self.view.resetusingOTPEnterOTP.btnResendOTP.setEnabled(false);
            self.view.resetusingOTPEnterOTP.btnResendOTP.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_DISABLED;
            self.view.resetusingOTPEnterOTP.btnResendOTP.hoverSkin = OLBConstants.SKINS.LOGIN_RESEND_OTP_DISABLED;
            this.resendOtpTimer = setTimeout(
  		     function(){ 
        	  self.view.resetusingOTPEnterOTP.btnResendOTP.setEnabled(true);
          	  self.view.resetusingOTPEnterOTP.btnResendOTP.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_ENABLED;
              self.view.resetusingOTPEnterOTP.btnResendOTP.hoverSkin = OLBConstants.SKINS.LOGIN_RESEND_OTP_ENABLED;
        	}, 10000);    
            this.view.forceLayout();
        },

        /**
          * resendOTPResponseFailure :This function is called when the Resend OTP is failed
          * @member of {frmEnrollNowController}
          * @param {} 
          * @return {}  
          * @throws {}
         */
        resendOTPResponseFailure: function () {
            CommonUtilities.hideProgressBar(this.view);
        },

        selectedQuestions: {
            ques: ["Select a Question", "Select a Question", "Select a Question", "Select a Question", "Select a Question"],
            key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
        },
        selectedQuestionsTemp: {
            securityQuestions: [],
            flagToManipulate: []
        },
        responseBackend: [
            {
                question: "",
                SecurityID: ""
            }
        ],
        /**
       * updateSecurityQuestions :This function updates the security Questions
       * @member of {frmEnrollNowController}
       * @param {} 
       * @return {}  
       * @throws {}
      */
        updateSecurityQuestions: function () {
            var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
            CommonUtilities.showProgressBar(this.view);
            enrollModule.presentationController.fetchSecurityQuestions(this.selectedQuestionsTemp);
        },

        /**
      * staticSetQuestions :This function makes a service call which retrieves all the questions
      * @member of {frmEnrollNowController}
      * @param {response,data} 
      * @return {} 
      * @throws {}
     */
        staticSetQuestions: function (response, data) {
            CommonUtilities.hideProgressBar(this.view);
            this.responseBackend = data;
            this.successCallback(response);
            this.showSetSecurityQuestions();

        },

        /**
        * successCallback :This function sets the master Data of the  Security Questions to each list Box
        * @member of {frmEnrollNowController}
        * @param {response}  set of aall questions
        * @return {} 
        * @throws {}
       */
        successCallback: function (response) {
            var data = [];
            this.selectedQuestionsTemp = response;
            data = this.getQuestions(response);
            this.view.SetSecurityQuestions.lbxQuestion1.masterData = data;
            this.view.SetSecurityQuestions.lbxQuestion2.masterData = data;
            this.view.SetSecurityQuestions.lbxQuestion3.masterData = data;
            this.view.SetSecurityQuestions.lbxQuestion4.masterData = data;
            this.view.SetSecurityQuestions.lbxQuestion5.masterData = data;
            this.view.SetSecurityQuestions.tbxAnswer1.text = "";
            this.view.SetSecurityQuestions.tbxAnswer2.text = "";
            this.view.SetSecurityQuestions.tbxAnswer3.text = "";
            this.view.SetSecurityQuestions.tbxAnswer4.text = "";
            this.view.SetSecurityQuestions.tbxAnswer5.text = "";
            this.disableButton(this.view.SetSecurityQuestions.btnSetSecurityQuestionsProceed);
        },

        /**
        * flagManipulation :This function manipulates the data in the listbox after selection
        * @member of {frmEnrollNowController}
        * @param {data,selectedData} 
        * @return {tempData2} which is the new Data Set 
        * @throws {}
       */
        flagManipulation: function (data, selectedData) {
            var tempData1 = [],
                tempData2 = [];
            if (selectedData[0] !== "lb0") {
                tempData1[0] = selectedData;
                tempData2 = tempData1.concat(data);
            } else
                tempData2 = data;
            return tempData2;

        },

        /**
        * getQuestionsAfterSelected :This function manipulates the listbox after selection
        * @member of {frmEnrollNowController}
        * @param {data,selectedQues,key} 
        * @return {} 
        * @throws {}
       */
        getQuestionsAfterSelected: function (data, selectedQues, key) {
            var temp = 10;
            if (data[1] !== "Select a Question") {
                for (var i = 0; i < this.selectedQuestionsTemp.securityQuestions.length; i++) {
                    if (this.selectedQuestionsTemp.securityQuestions[i] === data[1]) {
                        if (this.selectedQuestionsTemp.flagToManipulate[i] === "false") {
                            this.selectedQuestionsTemp.flagToManipulate[i] = "true";
                            temp = i;
                        }
                    }
                    if (this.selectedQuestionsTemp.securityQuestions[i] === selectedQues.ques) {
                        if (this.selectedQuestionsTemp.flagToManipulate[i] === "true") {
                            this.selectedQuestionsTemp.flagToManipulate[i] = "false";
                            this.selectedQuestions.key[key] = "lb" + (i + 1);
                        }
                    }
                }
            }
            else {
                this.disableButton(this.view.SetSecurityQuestions.btnSetSecurityQuestionsProceed);
                if (key === 0) {
                    this.view.SetSecurityQuestions.tbxAnswer1.text = "";
                }
                else if (key === 1) {
                    this.view.SetSecurityQuestions.tbxAnswer2.text = "";
                }
                else if (key === 2) {
                    this.view.SetSecurityQuestions.tbxAnswer3.text = "";
                }
                else if (key === 3) {
                    this.view.SetSecurityQuestions.tbxAnswer4.text = "";
                }
                else if (key === 4) {
                    this.view.SetSecurityQuestions.tbxAnswer5.text = "";
                }
                for (var ij = 0; ij < this.selectedQuestionsTemp.securityQuestions.length; ij++) {
                    if (this.selectedQuestionsTemp.securityQuestions[ij] === selectedQues.ques) {
                        if (this.selectedQuestionsTemp.flagToManipulate[ij] === "true") {
                            this.selectedQuestionsTemp.flagToManipulate[ij] = "false";
                            this.selectedQuestions.key[key] = "lb" + (ij + 1);
                        }
                    }
                }
            }
            if (temp != 10) {
                this.selectedQuestions.ques[key] = this.selectedQuestionsTemp.securityQuestions[temp];
                this.selectedQuestions.key[key] = "lb" + (temp + 1);
            }
            else {
                this.selectedQuestions.ques[key] = "Select a Question";
                this.selectedQuestions.key[key] = "lb0";
            }
            var questions = [];
            questions = this.getQuestions(this.selectedQuestionsTemp);
            return questions;
        },

        /**
        * getQuestions :This function changes questions into key-value pairs basing on the flagManipulation
        * @member of {frmEnrollNowController}
        * @param {response} JSON of questions
        * @return {temp} array of Security Questions 
        * @throws {}
       */

        getQuestions: function (response) {
            var temp = [];
            temp[0] = ["lb0", "Select a Question"];
            for (var i = 0, j = 1; i < response.securityQuestions.length; i++) {
                var arr = [];
                if (response.flagToManipulate[i] === "false") {
                    arr[0] = "lb" + (i + 1);
                    arr[1] = response.securityQuestions[i];
                    temp[j] = arr;
                    j++;
                }
            }
            return temp;
        },

        /**
     * enableSecurityQuestions :This function sets the status whether to enable/disable the status button
     * @member of {frmEnrollNowController}
     * @param {question1, question2, question3, question4, question5} values of all five questions
     * @return {} 
     * @throws {}
    */
        enableSecurityQuestions: function (question1, question2, question3, question4, question5) {
            if (question1 !== null && question1 !== "" && question2 !== null && question2 !== "" && question3 !== null && question3 !== "" && question4 !== null && question4 !== "" && question5 !== null && question5 !== "") {
                this.btnSecurityQuestions(true);
            } else {
                this.btnSecurityQuestions(false);
            }
        },

        /**
     * btnSecurityQuestions :This function enables the proceed button if all the five questions are answered
     * @member of {frmEnrollNowController}
     * @param {status}  returns true-- if all the five questions are answered , false-- if any question is not answered
     * @return {} 
     * @throws {}
    */
        btnSecurityQuestions: function (status) {
            if (status === true) {
                this.enableButton(this.view.SetSecurityQuestions.btnSetSecurityQuestionsProceed);
            } else {
                this.disableButton(this.view.SetSecurityQuestions.btnSetSecurityQuestionsProceed);
            }
        },

        /**
      * setQuestionForListBox1 :This function sets the question for first listBox
      * @member of {frmEnrollNowController}
      * @param {} 
      * @return {} 
      * @throws {}
     */
        setQuestionForListBox1: function () {
            var value = [];
            value = this.view.SetSecurityQuestions.lbxQuestion1.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[0];
            selectedQues.key = this.selectedQuestions.key[0];
            var position = 0;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.SetSecurityQuestions.lbxQuestion2.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.SetSecurityQuestions.lbxQuestion2.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.SetSecurityQuestions.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.SetSecurityQuestions.lbxQuestion3.masterData = mainData2;
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.SetSecurityQuestions.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.SetSecurityQuestions.lbxQuestion4.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.SetSecurityQuestions.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.SetSecurityQuestions.lbxQuestion5.masterData = mainData4;
        },


        /**
        * setQuestionForListBox4 :This function sets the question for fourth listBox
        * @member of {frmEnrollNowController}
        * @param {} 
        * @return {} 
        * @throws {}
       */
        setQuestionForListBox4: function () {
            var value = [];
            value = this.view.SetSecurityQuestions.lbxQuestion4.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[3];
            selectedQues.key = this.selectedQuestions.key[3];
            var position = 3;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.SetSecurityQuestions.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.SetSecurityQuestions.lbxQuestion1.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.SetSecurityQuestions.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.SetSecurityQuestions.lbxQuestion3.masterData = mainData2;

            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.SetSecurityQuestions.lbxQuestion2.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.SetSecurityQuestions.lbxQuestion2.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.SetSecurityQuestions.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.SetSecurityQuestions.lbxQuestion5.masterData = mainData4;
        },


        /**
      * setQuestionForListBox3 :This function sets the question for third listBox
      * @member of {frmEnrollNowController}
      * @param {} 
      * @return {} 
      * @throws {}
     */
        setQuestionForListBox3: function () {
            var value = [];
            value = this.view.SetSecurityQuestions.lbxQuestion3.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[2];
            selectedQues.key = this.selectedQuestions.key[2];
            var position = 2;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.SetSecurityQuestions.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.SetSecurityQuestions.lbxQuestion1.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.SetSecurityQuestions.lbxQuestion2.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.SetSecurityQuestions.lbxQuestion2.masterData = mainData2;
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.SetSecurityQuestions.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.SetSecurityQuestions.lbxQuestion4.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.SetSecurityQuestions.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.SetSecurityQuestions.lbxQuestion5.masterData = mainData4;
        },

        /**
      * setQuestionForListBox2 :This function sets the question for second listBox
      * @member of {frmEnrollNowController}
      * @param {} 
      * @return {} 
      * @throws {}
     */
        setQuestionForListBox2: function () {
            var value = [];
            value = this.view.SetSecurityQuestions.lbxQuestion2.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[1];
            selectedQues.key = this.selectedQuestions.key[1];
            var position = 1;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.SetSecurityQuestions.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.SetSecurityQuestions.lbxQuestion1.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.SetSecurityQuestions.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.SetSecurityQuestions.lbxQuestion3.masterData = mainData2;
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.SetSecurityQuestions.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.SetSecurityQuestions.lbxQuestion4.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.SetSecurityQuestions.lbxQuestion5.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.SetSecurityQuestions.lbxQuestion5.masterData = mainData4;
        },

        /**
       * setQuestionForListBox5 :This function sets the question for fifth listBox
       * @member of {frmEnrollNowController}
       * @param {} 
       * @return {} 
       * @throws {}
      */
        setQuestionForListBox5: function () {
            var value = [];
            value = this.view.SetSecurityQuestions.lbxQuestion5.selectedKeyValue;
            var data = [];
            var selectedQues = {
                ques: "null",
                key: "null"
            };
            selectedQues.ques = this.selectedQuestions.ques[4];
            selectedQues.key = this.selectedQuestions.key[4];
            var position = 4;
            data = this.getQuestionsAfterSelected(value, selectedQues, position);
            var mainData = [],
                selectedData = [];
            selectedData = this.view.SetSecurityQuestions.lbxQuestion1.selectedKeyValue;
            mainData = this.flagManipulation(data, selectedData);
            this.view.SetSecurityQuestions.lbxQuestion1.masterData = mainData;
            var mainData2 = [],
                selectedData2 = [];
            selectedData2 = this.view.SetSecurityQuestions.lbxQuestion3.selectedKeyValue;
            mainData2 = this.flagManipulation(data, selectedData2);
            this.view.SetSecurityQuestions.lbxQuestion3.masterData = mainData2;
            var mainData3 = [],
                selectedData3 = [];
            selectedData3 = this.view.SetSecurityQuestions.lbxQuestion4.selectedKeyValue;
            mainData3 = this.flagManipulation(data, selectedData3);
            this.view.SetSecurityQuestions.lbxQuestion4.masterData = mainData3;
            var mainData4 = [],
                selectedData4 = [];
            selectedData4 = this.view.SetSecurityQuestions.lbxQuestion2.selectedKeyValue;
            mainData4 = this.flagManipulation(data, selectedData4);
            this.view.SetSecurityQuestions.lbxQuestion2.masterData = mainData4;
        },

        /**
       * onEditingAnswer1 :This function sets the minimum and maximum limitations on the first answer selected
       * @member of {frmEnrollNowController}
       * @param {} 
       * @return {} 
       * @throws {}
      */
        onEditingAnswer1: function () {
            var data = [];
            data = this.view.SetSecurityQuestions.lbxQuestion1.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.SetSecurityQuestions.tbxAnswer1.maxTextLength = 0;
            } else {
                this.view.SetSecurityQuestions.tbxAnswer1.maxTextLength = 50;
            }
        },

        /**
       * onEditingAnswer2 :This function sets the minimum and maximum limitations on the second answer selected
       * @member of {frmEnrollNowController}
       * @param {} 
       * @return {} 
       * @throws {}
      */
        onEditingAnswer2: function () {
            var data = [];
            data = this.view.SetSecurityQuestions.lbxQuestion2.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.SetSecurityQuestions.tbxAnswer2.maxTextLength = 0;
            } else {
                this.view.SetSecurityQuestions.tbxAnswer2.maxTextLength = 50;
            }
        },

        /**
        * onEditingAnswer3 :This function sets the minimum and maximum limitations on the third answer selected
        * @member of {frmEnrollNowController}
        * @param {} 
        * @return {} 
        * @throws {}
       */
        onEditingAnswer3: function () {
            var data = [];
            data = this.view.SetSecurityQuestions.lbxQuestion3.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.SetSecurityQuestions.tbxAnswer3.maxTextLength = 0;
            } else {
                this.view.SetSecurityQuestions.tbxAnswer3.maxTextLength = 50;
            }
        },

        /**
        * onEditingAnswer4 :This function sets the minimum and maximum limitations on the fourth answer selected
        * @member of {frmEnrollNowController}
        * @param {} 
        * @return {} 
        * @throws {}
       */
        onEditingAnswer4: function () {
            var data = [];
            data = this.view.SetSecurityQuestions.lbxQuestion4.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.SetSecurityQuestions.tbxAnswer4.maxTextLength = 0;
            } else {
                this.view.SetSecurityQuestions.tbxAnswer4.maxTextLength = 50;
            }
        },

        /**
       * onEditingAnswer5 :This function sets the minimum and maximum limitations on the fifth answer selected
       * @member of {frmEnrollNowController}
       * @param {} 
       * @return {} 
       * @throws {}
      */
        onEditingAnswer5: function () {
            var data = [];
            data = this.view.SetSecurityQuestions.lbxQuestion5.selectedKeyValue;
            if (data[1] === "Select a Question") {
                this.view.SetSecurityQuestions.tbxAnswer5.maxTextLength = 0;
            } else {
                this.view.SetSecurityQuestions.tbxAnswer5.maxTextLength = 50;
            }
        },

        /**
         * onSaveSecurityQuestions :This function sets the selected answer to the selected question
         * @member of {frmEnrollNowController}
         * @param {} 
         * @return {} 
         * @throws {}
        */
        onSaveSecurityQuestions: function () {
            var data = [{
                customerAnswer: "", questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }, {
                customerAnswer: "",
                questionId: ""
            }];
            var quesData = "";
            quesData = this.view.SetSecurityQuestions.lbxQuestion1.selectedKeyValue;
            data[0].customerAnswer = this.view.SetSecurityQuestions.tbxAnswer1.text;
            data[0].questionId = this.getQuestionID(quesData);
            quesData = this.view.SetSecurityQuestions.lbxQuestion2.selectedKeyValue;
            data[1].customerAnswer = this.view.SetSecurityQuestions.tbxAnswer2.text;
            data[1].questionId = this.getQuestionID(quesData);
            quesData = this.view.SetSecurityQuestions.lbxQuestion3.selectedKeyValue;
            data[2].customerAnswer = this.view.SetSecurityQuestions.tbxAnswer3.text;
            data[2].questionId = this.getQuestionID(quesData);
            quesData = this.view.SetSecurityQuestions.lbxQuestion4.selectedKeyValue;
            data[3].customerAnswer = this.view.SetSecurityQuestions.tbxAnswer4.text;
            data[3].questionId = this.getQuestionID(quesData);
            quesData = this.view.SetSecurityQuestions.lbxQuestion5.selectedKeyValue;
            data[4].customerAnswer = this.view.SetSecurityQuestions.tbxAnswer5.text;
            data[4].questionId = this.getQuestionID(quesData);
            return data;
        },

        /**
          * getQuestionID :This function gets the unique question ID for each question
          * @member of {frmEnrollNowController}
          * @param {quesData} Array of questions
          * @return {qData} Questionss IDs  
          * @throws {}
         */
        getQuestionID: function (quesData) {
            var qData;
            for (var i = 0; i < this.responseBackend.length; i++) {
                if (quesData[1] === this.responseBackend[i].SecurityQuestion) {
                    qData = this.responseBackend[i].SecurityQuestion_id;
                }
            }
            return qData;
        },

        /**
          * onProceedSQ :This function is called when the server fails to save the security questions and navigates to server down time
          * @member of {frmEnrollNowController}
          * @param {} 
          * @return {}  
          * @throws {}
         */
        onProceedSQ: function () {
            CommonUtilities.showProgressBar(this.view);
            var data = this.onSaveSecurityQuestions();
            this.presenter.saveSecurityQuestions(data);
        },

        /**
          * saveSQCallback :This function is called when the server fails to save the security questions and navigates to server down time
          * @member of {frmEnrollNowController}
          * @param {} 
          * @return {}  
          * @throws {}
         */
        saveSQCallback: function () {
            CommonUtilities.hideProgressBar(this.view);
            this.showSecurityQuestionsAck();
            this.view.securityAck.lblNotYetEnrolledOrError.text = kony.i18n.getLocalizedString("i18n.enrollNow.successfulEnrol");
        },

        /**
          * saveSQfailureCallback :This function is called when the server fails to save the security questions and navigates to server down time
          * @member of {frmEnrollNowController}
          * @param {} 
          * @return {}  
          * @throws {}
         */
        saveSQfailureCallback: function () {
            CommonUtilities.hideProgressBar(this.view);
            this.presenter.navigateToServerDownScreen();

        },
        /**
          * onCancelSecurityQuestions :This function when user dont want to enter the security questions and click on cancel
          * @member of {frmEnrollNowController}
          * @param {} 
          * @return {}  
          * @throws {}
         */
        onCancelSecurityQuestions: function () {
            this.selectedQuestions = {
                ques: ["Select a Question", "Select a Question", "Select a Question", "Select a Question", "Select a Question"],
                key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
            };
            this.selectedQuestionsTemp = {
                securityQuestions: [],
                flagToManipulate: []
            };
            this.showUsernamePasswordAck();
        },
    };
});