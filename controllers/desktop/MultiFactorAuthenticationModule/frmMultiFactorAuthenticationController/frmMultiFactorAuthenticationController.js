define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants){
	return{
    shouldUpdateUI: function (viewModel) {
      return viewModel !== undefined && viewModel !== null;
    },
    willUpdateUI: function(mfaViewModel){
      if(mfaViewModel.progressBar === true){
        CommonUtilities.showProgressBar(this.view);
      }else if (mfaViewModel.progressBar === false){
        CommonUtilities.hideProgressBar(this.view);
      }
      if(mfaViewModel.serverError){
        CommonUtilities.hideProgressBar(this.view);
        this.showServerError(mfaViewModel.serverError);
      }else{
        this.hideServerError();
      }
      if(mfaViewModel.sideMenu) {
        this.updateHamburgerMenu(mfaViewModel.sideMenu);
      }
      if(mfaViewModel.topBar) {
        this.updateTopBar(mfaViewModel.topBar);
      }
      if(mfaViewModel.hamburgerSelection) {
        this.highlightHamburgerSelections(mfaViewModel.hamburgerSelection);
      }
      if(mfaViewModel.breadCrumbData){
        this.showBreadcrumbForMFA(mfaViewModel.breadCrumbData);
      }
      if(mfaViewModel.termsAndConditions){
        this.setTermsAndConditionsData(mfaViewModel.termsAndConditions);
      }
	  if(mfaViewModel.secureAccessCodeOptions){
        this.showSecureAccessCodeOptions(mfaViewModel.secureAccessCodeOptions);
      }
      if(mfaViewModel.secureAccessCodeSent){
        this.showEnterSecureAccessCodeScreen();
      }
      if(mfaViewModel.incorrectSecureAccessCode){
        this.showIncorrectSecureAccessCodeWarning();
      }
      if(mfaViewModel.cvvCardSelection){
        this.showCVVScreen(mfaViewModel.cards);
      }
      if(mfaViewModel.incorrectCVV){
        this.showIncorrectCVVWarning();
      }
      if(mfaViewModel.securityQuestions){
        this.showSecurityQuestionsScreen(mfaViewModel.securityQuestions);
      }
      if(mfaViewModel.incorrectSecurityAnswers){
        this.showIncorrectSecurityAnswersWarning();
      }
      this.AdjustScreen();
    },
    
    /**
     * AdjustScreen - Method that sets the height of footer properly.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */
    AdjustScreen: function() {
        var mainheight;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - this.view.flxFooter.frame.height;
            if (diff > 0) 
                this.view.flxFooter.top = mainheight + diff + 40 + "dp";
            else
                this.view.flxFooter.top = mainheight + 40 + "dp";        
         } else {
            this.view.flxFooter.top = mainheight + 40 + "dp";
         }
        this.view.forceLayout();
    }, 
      
    /**
     * frmMultiFactorAuthenticationPreShow - Pre show for this form.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */  
    frmMultiFactorAuthenticationPreShow: function(){
      this.hideAllMFAViews();
      this.view.breadcrumb.setVisibility(false);
      this.view.customheader.forceCloseHamburger();
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.setLogoutEvent();
      this.AdjustScreen();
    },  
    
    /**
     * setLogoutEvent - Method to set logout event.
     * @member of {frmCardManagementController}
     * @param {} - None.
     * @returns {VOID}
     * @throws {}
     */
   setLogoutEvent:function(){
      var scopeObj=this;
      this.view.customheader.headermenu.btnLogout.onClick = function(){  
        scopeObj.view.CustomPopup.lblHeading.text=kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text=kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height=scopeObj.view.flxHeader.frame.height+scopeObj.view.flxMain.frame.height+scopeObj.view.flxFooter.frame.height;
        scopeObj.view.flxLogout.height=height+"dp";
        scopeObj.view.flxLogout.left="0%";     
      };
      this.view.CustomPopup.btnYes.onClick = function(){
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        var context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";      

      };
      this.view.CustomPopup.btnNo.onClick = function(){ 
        scopeObj.view.flxLogout.left="-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function(){  
        scopeObj.view.flxLogout.left="-100%";
       };
    },  
      
      
    /**
     * showBreadcrumbForMFA - Enables breadcrumb and sets the text based on context.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {String} - Breadcrumb text.
     * @returns {VOID}
     * @throws {}
     */
    showBreadcrumbForMFA: function(breadCrumb1){
      var self = this;
      var breadCrumbData = [
        {
          'text': breadCrumb1,
          'callback': function(){
            CommonUtilities.showProgressBar(self.view);
            self.presenter.cancelCallback();
          }
        },{
          'text': kony.i18n.getLocalizedString("i18n.MFA.authentication"),
          'callback': null
        }
      ];
      this.view.breadcrumb.setVisibility(true);
      this.view.breadcrumb.setBreadcrumbData(breadCrumbData);
    },
    /**
     * showServerError - Method to show error flex.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {String} - Error message to be displayed.
     * @returns {VOID}
     * @throws {}
     */
    showServerError: function(errorMsg){
       this.view.rtxDowntimeWarning.text = errorMsg;
       this.view.flxDowntimeWarning.setVisibility(true);
       this.view.forceLayout();
    },

    /**
     * hideServerError - Method to hide error flex.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - None.
     * @returns {VOID}
     * @throws {}
     */
    hideServerError: function(){
       this.view.flxDowntimeWarning.setVisibility(false);
       this.view.forceLayout();
    },
      
    /**
     * showSecureAccessCodeOptions - Entry point for secure access code flow. Shows the options for receiving a code.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */  
    showSecureAccessCodeOptions: function(secureAccessCodeOptions){
      var self = this;
      this.hideAllMFAViews();
      this.view.flxOptionToRecieveAccessCode.setVisibility(true);
      if(secureAccessCodeOptions.isEmailEnabled === "true"){
	      this.view.LetsAuthenticateSecureAccessCode.imgCheckboxEmailId.src = OLBConstants.IMAGES.CHECKED_IMAGE;
      }else{
          this.view.LetsAuthenticateSecureAccessCode.imgCheckboxEmailId.src = OLBConstants.IMAGES.UNCHECKED_IMAGE;
      }
      this.view.LetsAuthenticateSecureAccessCode.flxCheckboxEmailId.onTouchEnd = function(){
        CommonUtilities.toggleCheckBox(self.view.LetsAuthenticateSecureAccessCode.imgCheckboxEmailId);
        if(CommonUtilities.isChecked(self.view.LetsAuthenticateSecureAccessCode.imgCheckboxEmailId) || CommonUtilities.isChecked(self.view.LetsAuthenticateSecureAccessCode.imgCheckBoxPhoneNo)){
          CommonUtilities.enableButton(self.view.LetsAuthenticateSecureAccessCode.btnProceed);
        }else{
          CommonUtilities.disableButton(self.view.LetsAuthenticateSecureAccessCode.btnProceed);
        }
      };
      if(secureAccessCodeOptions.isPhoneEnabled === "true"){
	      this.view.LetsAuthenticateSecureAccessCode.imgCheckBoxPhoneNo.src = OLBConstants.IMAGES.CHECKED_IMAGE;
      }else{
          this.view.LetsAuthenticateSecureAccessCode.imgCheckBoxPhoneNo.src = OLBConstants.IMAGES.UNCHECKED_IMAGE;
      }
      this.view.LetsAuthenticateSecureAccessCode.flxCheckBoxPhoneNo.onTouchEnd = function(){
        CommonUtilities.toggleCheckBox(self.view.LetsAuthenticateSecureAccessCode.imgCheckBoxPhoneNo);
        if(CommonUtilities.isChecked(self.view.LetsAuthenticateSecureAccessCode.imgCheckboxEmailId) || CommonUtilities.isChecked(self.view.LetsAuthenticateSecureAccessCode.imgCheckBoxPhoneNo)){
          CommonUtilities.enableButton(self.view.LetsAuthenticateSecureAccessCode.btnProceed);
        }else{
          CommonUtilities.disableButton(self.view.LetsAuthenticateSecureAccessCode.btnProceed);
        }
      };
      this.view.LetsAuthenticateSecureAccessCode.lblRegisteredEmailId.text = self.maskEmail(CommonUtilities.getPrimaryContact(kony.mvc.MDAApplication.getSharedInstance().appContext.emailids));
      this.view.LetsAuthenticateSecureAccessCode.lblPhoneNo.text = self.maskPhoneNumber(CommonUtilities.getPrimaryContact(kony.mvc.MDAApplication.getSharedInstance().appContext.contactNumbers));
      this.view.LetsAuthenticateSecureAccessCode.btnProfileSetting.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.profilesettings");
      this.view.LetsAuthenticateSecureAccessCode.btnProfileSetting.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.profilesettings");
      this.view.LetsAuthenticateSecureAccessCode.btnProfileSetting.onClick = function(){
        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
        profileModule.presentationController.showSecureAccessSettings();
      };
      this.view.LetsAuthenticateSecureAccessCode.btnCancel.onClick = function(){
        CommonUtilities.showProgressBar(self.view);
        self.presenter.cancelCallback();
      };
      this.view.LetsAuthenticateSecureAccessCode.btnProceed.onClick = function(){
        var options = {};
        options.email = CommonUtilities.isChecked(self.view.LetsAuthenticateSecureAccessCode.imgCheckboxEmailId);
        options.phone = CommonUtilities.isChecked(self.view.LetsAuthenticateSecureAccessCode.imgCheckBoxPhoneNo);
      	CommonUtilities.showProgressBar(self.view);
        self.presenter.sendSecureAccessCode(options);
      };
      this.view.forceLayout();
      this.AdjustScreen();
      CommonUtilities.hideProgressBar(this.view);
    },
     
    /**
     * showEnterSecureAccessCodeScreen - Shows the screen where user enters the code.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */
    showEnterSecureAccessCodeScreen: function(){
      var self = this;
      this.hideAllMFAViews();
      this.view.flxSecureAccessCode.setVisibility(true);
      this.view.SecureAccessCode.flxWarning.setVisibility(false);
      this.view.SecureAccessCode.tbxEnterCVVCode.text = "";
      this.view.SecureAccessCode.tbxEnterCVVCode.secureTextEntry = true;
      CommonUtilities.disableButton(self.view.SecureAccessCode.btnProceed);
      this.view.SecureAccessCode.tbxEnterCVVCode.onKeyUp = function(){
        if(!self.isValidSecureAccessCode(self.view.SecureAccessCode.tbxEnterCVVCode.text)){
          CommonUtilities.disableButton(self.view.SecureAccessCode.btnProceed);
        }else{
          CommonUtilities.enableButton(self.view.SecureAccessCode.btnProceed);
        }
      };
      this.view.SecureAccessCode.imgViewCVVCode.onTouchEnd = function(){
        self.toggleMaskingForTextBox(self.view.SecureAccessCode.tbxEnterCVVCode);
      };
      var resendOtpTimer;
      this.view.SecureAccessCode.btnCancel.onClick = function(){
        CommonUtilities.showProgressBar(self.view);
        clearTimeout(resendOtpTimer);
        self.presenter.cancelCallback();
      };
      self.view.SecureAccessCode.lblResendOption2.setEnabled(false);
      self.view.SecureAccessCode.lblResendOption2.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_DISABLED;
      resendOtpTimer = setTimeout(
        function(){ 
          self.view.SecureAccessCode.lblResendOption2.setEnabled(true);
          self.view.SecureAccessCode.lblResendOption2.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_ENABLED;
        }, 5000);
      this.view.SecureAccessCode.lblResendOption2.onClick = function(){
        self.view.SecureAccessCode.tbxEnterCVVCode.text = "";
        CommonUtilities.disableButton(self.view.SecureAccessCode.btnProceed);
        self.view.SecureAccessCode.flxWarning.setVisibility(true);
        self.view.SecureAccessCode.lblWarning.text = kony.i18n.getLocalizedString("i18n.MFA.ResentOTPMessage");
        self.view.SecureAccessCode.lblResendOption2.setEnabled(false);
        self.view.SecureAccessCode.lblResendOption2.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_DISABLED;
        resendOtpTimer = setTimeout(
          function(){ 
            self.view.SecureAccessCode.lblResendOption2.setEnabled(true);
            self.view.SecureAccessCode.lblResendOption2.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_ENABLED;
          }, 5000);
      };
      this.view.SecureAccessCode.btnProceed.onClick = function(){
        clearTimeout(resendOtpTimer);
        self.view.SecureAccessCode.lblResendOption2.setEnabled(false);
        self.view.SecureAccessCode.lblResendOption2.skin = OLBConstants.SKINS.LOGIN_RESEND_OTP_DISABLED;
        var params = {};
        params.enteredAccessCode = self.view.SecureAccessCode.tbxEnterCVVCode.text;
        CommonUtilities.showProgressBar(self.view);
        self.view.SecureAccessCode.flxWarning.setVisibility(false);
	    self.presenter.verifySecureAccessCode(params);
      };
      this.view.forceLayout();
      this.AdjustScreen();
      CommonUtilities.hideProgressBar(this.view);
    },
   	
    /**
     * isValidSecureAccessCode - Validates given secure access code.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */
    isValidSecureAccessCode: function(secureaccesscode){
      if(secureaccesscode.length !== OLBConstants.OTPLength) return false;  	
   	  var regex = new RegExp('^[0-9]+$');
      return regex.test(secureaccesscode);
    }, 
      
    /**
     * showIncorrectSecureAccessCodeWarning - Shows the "incorrect code entered" warning.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */  
    showIncorrectSecureAccessCodeWarning: function(){
      this.view.SecureAccessCode.flxWarning.setVisibility(true);
      this.view.SecureAccessCode.lblWarning.text = kony.i18n.getLocalizedString("i18n.MFA.EnteredSecureAccessCodeDoesNotMatch");
      CommonUtilities.hideProgressBar(this.view);
      this.view.forceLayout();
      this.AdjustScreen();
    },
      
      
    /**
     * maskEmail - Masks a given email. Eg: 'abcdef.a@kony.com" will be masked to 'a******a@kony.com'
     * @member of {frmMultiFactorAuthenticationController}
     * @param {String} - email.
     * @returns {String} - masked email. 
     * @throws {}
     */
    maskEmail: function(email){
      var eArr = email.split('@');
      var maskedEmail = "";
      maskedEmail += eArr[0].charAt(0);
      for(var i=0; i<(eArr[0].length-2); i++ )
        maskedEmail += "*";
      maskedEmail += eArr[0].charAt(eArr[0].length-1) + '@' + eArr[1];
      return maskedEmail;
    },
    
    /**
     * maskPhoneNumber - Masks a given phone number. Eg: '1234567890" will be masked to '******7890'
     * @member of {frmMultiFactorAuthenticationController}
     * @param {String} - phone number.
     * @returns {String} - masked phone number. 
     * @throws {}
     */  
    maskPhoneNumber: function(phone){
      var maskedPhoneNumber = "";
      for(var i=0; i<phone.length-4; i++)
        maskedPhoneNumber += "*";
      maskedPhoneNumber += phone.slice(-4);
      return maskedPhoneNumber; 
    },
      
      
    /**
     * toggleMaskingForTextBox - Toggles the secure text property for a given textbox
     * @member of {frmMultiFactorAuthenticationController}
     * @param {String} - widgetId.
     * @returns {VOID}  
     * @throws {}
     */ 
    toggleMaskingForTextBox: function(widgetId){
      widgetId.secureTextEntry = !widgetId.secureTextEntry;
    },
      
    /**
     * showCVVScreen - Sets the UI for CVV flow.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}  
     * @throws {}
     */ 
    showCVVScreen: function(cards){
      var self = this;
      this.hideAllMFAViews();
      this.view.flxEnterCVVCode.setVisibility(true);
      var cardsMasterData = this.getCardsDataForListbox(cards);
      this.view.EnterCVVModule.lbxDefaultAccountForSending.masterData = cardsMasterData;
      this.view.EnterCVVModule.lbxDefaultAccountForSending.onSelection = function(){
      	self.view.EnterCVVModule.tbxEnterCVVCode.text = "";  
      };
      this.view.EnterCVVModule.tbxEnterCVVCode.text = "";
      this.view.EnterCVVModule.flxDefaultAccountForSendingInfo.onClick= function() {
        self.view.InfoPopUpIcon.isVisible = true;
        self.view.InfoPopUpIcon.left = "183dp";
        self.view.InfoPopUpIcon.top = "240dp";
      };
      this.view.EnterCVVModule.flxEnterCVVCodeInfo.onClick = function() {
        self.view.InfoPopUpIcon.isVisible = true;
        self.view.InfoPopUpIcon.left = "100dp";
        self.view.InfoPopUpIcon.top = "300dp";
      };
      this.view.InfoPopUpIcon.flxCross.onClick = function() {
        self.view.InfoPopUpIcon.isVisible = false;
      };
      CommonUtilities.disableButton(this.view.EnterCVVModule.btnProceed);
      this.view.EnterCVVModule.tbxEnterCVVCode.onKeyUp = function(){
        if(!self.isValidCVV(self.view.EnterCVVModule.tbxEnterCVVCode.text)){
          CommonUtilities.disableButton(self.view.EnterCVVModule.btnProceed);
        }else{
          CommonUtilities.enableButton(self.view.EnterCVVModule.btnProceed);
        }
      };
      this.view.EnterCVVModule.tbxEnterCVVCode.secureTextEntry = true;
      this.view.EnterCVVModule.imgViewCVVCode.onTouchEnd = function(){
        self.toggleMaskingForTextBox(self.view.EnterCVVModule.tbxEnterCVVCode);
      };
      this.view.EnterCVVModule.flxWarningEnterCVV.setVisibility(false);
      this.view.EnterCVVModule.btnCancel.onClick = function(){
        CommonUtilities.showProgressBar(self.view);
        self.presenter.cancelCallback();
      };
      this.view.EnterCVVModule.btnProceed.onClick = function(){
        self.view.EnterCVVModule.flxWarningEnterCVV.setVisibility(false);
        var options = {};
        options.cardId = self.view.EnterCVVModule.lbxDefaultAccountForSending.selectedKey;
        options.cvv = self.view.EnterCVVModule.tbxEnterCVVCode.text;
        self.presenter.verifyCVV(options);
      };
      this.view.forceLayout();
      this.AdjustScreen();
      CommonUtilities.hideProgressBar(this.view);
    },  
    
    /**
     * getCardsDataForListbox - Returns the master data for cards list box.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {Array} - cards array.
     * @returns {Array} - Master Data for cards.
     * @throws {}
     */   
    getCardsDataForListbox: function(cards){
      var masterData = [];
      for(var i in cards){
        masterData.push([cards[i].cardId, this.getDisplayNameForCard(cards[i])]);
      }
      return masterData;
    }, 
      
    /**
     * getDisplayNameForCard - Returns the display value for card. (Combination of card name and number)
     * @member of {frmMultiFactorAuthenticationController}
     * @param {String} - card object.
     * @returns {String} - display name.
     * @throws {}
     */  
    getDisplayNameForCard: function(card){
      return card.cardProductName + "... " + card.maskedCardNumber.slice(-8); 
    },
    /**
     * isValidCVV - Validates given cvv.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */  
    isValidCVV: function(cvv){
      if(cvv.length !== 3) return false;
      var regex = new RegExp('^[0-9]+$');
      return regex.test(cvv);
    },
    /**
     * showIncorrectCVVWarning - Shows the "incorrect cvv entered" warning.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */  
    showIncorrectCVVWarning: function(){
      this.view.EnterCVVModule.flxWarningEnterCVV.setVisibility(true);
      this.view.EnterCVVModule.lblWarningEnterCVV.text = kony.i18n.getLocalizedString("i18n.MFA.IncorrectCVV");
      CommonUtilities.hideProgressBar(this.view);
      this.view.forceLayout();
      this.AdjustScreen();
    },
    
    /**
     * showSecurityQuestionsScreen - Sets the UI for Security Questions flow. 
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}  
     * @throws {}
     */   
    showSecurityQuestionsScreen: function(securityQuestions){
      var self = this;
      this.hideAllMFAViews();
      this.view.flxAnswerSecurityQuestions.setVisibility(true);
      this.view.AnswerSecurityQuestionsModule.flxWarningSecurityQuestions.setVisibility(false);
      this.view.AnswerSecurityQuestionsModule.lblAnswerSecurityQuestion1.text = securityQuestions[0].Question;
      this.view.AnswerSecurityQuestionsModule.lblAnswerSecurityQuestion2.text = securityQuestions[1].Question;
      this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text = "";
      this.view.AnswerSecurityQuestionsModule.tbxAnswers1.onKeyUp = function(){
        if(self.view.AnswerSecurityQuestionsModule.tbxAnswers1.text === "" || self.view.AnswerSecurityQuestionsModule.tbxAnswers2.text === ""){
          CommonUtilities.disableButton(self.view.AnswerSecurityQuestionsModule.btnProceed);
        }else{
          CommonUtilities.enableButton(self.view.AnswerSecurityQuestionsModule.btnProceed);
        }
      };
      this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text = "";
      this.view.AnswerSecurityQuestionsModule.tbxAnswers2.onKeyUp = function(){
        if(self.view.AnswerSecurityQuestionsModule.tbxAnswers1.text === "" || self.view.AnswerSecurityQuestionsModule.tbxAnswers2.text === ""){
          CommonUtilities.disableButton(self.view.AnswerSecurityQuestionsModule.btnProceed);
        }else{
          CommonUtilities.enableButton(self.view.AnswerSecurityQuestionsModule.btnProceed);
        }
      };
      CommonUtilities.disableButton(this.view.AnswerSecurityQuestionsModule.btnProceed);
      this.view.AnswerSecurityQuestionsModule.btnCancel.onClick = function(){
        CommonUtilities.showProgressBar(self.view);
        self.presenter.cancelCallback();
      };
      this.view.AnswerSecurityQuestionsModule.btnProceed.onClick = function(){
        self.view.AnswerSecurityQuestionsModule.flxWarningSecurityQuestions.setVisibility(false);
        var questionAnswers = [];
        questionAnswers.push({
          'questionId': securityQuestions[0].SecurityQuestion_id,
          'customerAnswer': self.view.AnswerSecurityQuestionsModule.tbxAnswers1.text
        }, {
          'questionId': securityQuestions[1].SecurityQuestion_id,
          'customerAnswer': self.view.AnswerSecurityQuestionsModule.tbxAnswers2.text
        });
        CommonUtilities.showProgressBar(self.view);
	    self.presenter.verifySecurityQuestionAnswers(questionAnswers);
      };
      CommonUtilities.hideProgressBar(self.view);
      this.view.forceLayout();
      this.AdjustScreen();
    }, 
    
    /**
     * showIncorrectSecurityAnswersWarning - Shows the "incorrect security answers" warning.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */   
    showIncorrectSecurityAnswersWarning: function(){
      this.view.AnswerSecurityQuestionsModule.flxWarningSecurityQuestions.setVisibility(true);
      this.view.AnswerSecurityQuestionsModule.lblWarningSecurityQuestions.text = kony.i18n.getLocalizedString("i18n.MFA.EnteredSecurityQuestionsDoesNotMatch");
      this.view.forceLayout();
      this.AdjustScreen();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
     * hideAllMFAViews - Hides all the flexes in MFA Screen
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE.
     * @returns {VOID}  
     * @throws {}
     */ 
    hideAllMFAViews: function(){
      this.view.flxOptionToRecieveAccessCode.setVisibility(false);
      this.view.flxSecureAccessCode.setVisibility(false);
      this.view.flxEnterCVVCode.setVisibility(false);
      this.view.flxAnswerSecurityQuestions.setVisibility(false);
    },
      
    /**
     * updateHamburgerMenu - Method that updates Hamburger Menu.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */
	updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel, "" ,"");
    },

    /**
     * updateTopBar - Method that updates Top Menu.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },
      
    /**
     * highlightHamburgerSelections - Method that updates Top Menu.
     * @member of {frmMultiFactorAuthenticationController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */
    highlightHamburgerSelections: function(hamburgerSelection){
      this.view.customheader.customhamburger.activateMenu(hamburgerSelection.selection1, hamburgerSelection.selection2);
    },
      
    setTermsAndConditionsData: function(termsAndConditions){
      this.view.TermsAndConditions.lblTermsAndConditions.text = termsAndConditions;
    }  
	};      
 });