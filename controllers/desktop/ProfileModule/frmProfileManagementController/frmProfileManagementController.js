define(['CommonUtilities'], function (CommonUtilities) {


  var flag = 0;
  var update = "";
  var finalData = [];
  var widgetsMap = [{
    menu: "flxProfileSettings",
    subMenu: {
      parent: "flxProfileSettingsSubMenu",
      children: [
        {
          "configuration": "profileSettingsEnable",
          "widget":"flxProfile"
        }, 
        {
          "configuration": "phoneSettingsEnable",
          "widget":"flxPhone"
        }, 
        {
          "configuration": "emailSettingsEnable",
          "widget":"flxEmail"
        }, 
        {
          "configuration": "addressSettingsEnable",
          "widget":"flxAddress"
        },
        {
          "configuration": "usernameAndPasswordSettingsEnable",
          "widget":"flxUsernameAndPassword"
        }
      ]
    },
    image: "imgProfileSettingsCollapse"
  },
                    {
                      menu: "flxSecuritySettings",
                      subMenu: {
                        parent: "flxSecuritySettingsSubMenu",
                        children: 
                        [
                          {
                            "configuration": "securityQuestionsSettingsEnable",
                            "widget":"flxSecurityQuestions"
                          }, 
                          {
                            "configuration": "secureAccessCodeSettingsEnable",
                            "widget":"flxSecureAccessCode"
                          }
                        ]
                      },
                      image: "imgSecuritySettingsCollapse"
                    },
                    {
                      menu: "flxAccountSettings",
                      subMenu: {
                        parent: "flxAccountSettingsSubMenu",
                        children: [
                          {
                            "configuration": "enablePreferredAccounts",
                            "widget":"flxAccountPreferences"
                          }, 
                          {
                            "configuration": "enableDefaultAccounts",
                            "widget":"flxSetDefaultAccount"
                          }
                        ]
                      },
                      image: "imgAccountSettingsCollapse"
                    },
                    {
                      menu: "flxAlerts",
                      subMenu: {
                        parent: "flxAlertsSubMenu",
                        children: [
                          {
                            "configuration":"",
                            "widget":"flxTransactionAndPaymentsAlerts"
                          },
                          {
                            "configuration":"",
                            "widget":"flxSecurityAlerts"
                          },
                          {
                            "configuration":"",
                            "widget":"flxPromotionalAlerts"
                          },
                        ]
                      },
                      image: "imgAlertsCollapse"
                    }
                   ];
  return {

    willUpdateUI: function (viewModel) {
      if (viewModel == undefined) {} else {
        if (viewModel.isLoading !== undefined) this.changeProgressBarState(viewModel.isLoading);
        if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
        if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
        if (viewModel.showDefautUserAccounts) this.showDefaultUserAccount(viewModel.showDefautUserAccounts);
        if (viewModel.getAccountsList) this.showAccountsList(viewModel.getAccountsList);
        if (viewModel.getPreferredAccountsList) this.showPreferredAccountsList(viewModel.getPreferredAccountsList);
        if (viewModel.errorEditPrefferedAccounts) this.onPreferenceAccountEdit(viewModel.errorEditPrefferedAccounts);
        if (viewModel.userProfile) this.updateUserProfileSetttingsView(viewModel.userProfile);
        if (viewModel.emailList) this.updateEmailList(viewModel.emailList);
        if (viewModel.emails) this.setEmailsToLbx(viewModel.emails);
        if (viewModel.phoneList) this.updatePhoneList(viewModel.phoneList);
        if (viewModel.addressList) this.updateAddressList(viewModel.addressList);
        if (viewModel.addPhoneViewModel) this.updateAddPhoneViewModel(viewModel.addPhoneViewModel);
        if (viewModel.editPhoneViewModel) this.updateEditPhoneViewModel(viewModel.editPhoneViewModel);      
        if (viewModel.showVerificationByChoice) {this.showUsernameVerificationByChoice();
                                                 CommonUtilities.hideProgressBar(this.view);}
        if (viewModel.requestOtp) this.enterOtp();
        if (viewModel.verifyOtp) this.updateRequirements();
        if (viewModel.verifyQuestion) this.updateRequirements();
        if (viewModel.update) this.acknowledge();
        if (viewModel.securityAccess) this.showSecureAccessOptions(viewModel.securityAccess);
        if (viewModel.secureAccessOption) this.presenter.checkSecureAccess();
        if (viewModel.SecurityQuestionExists) this.showSecurityQuestions(viewModel.SecurityQuestionExists);
        if (viewModel.answerSecurityQuestion) this.showSecurityVerification(viewModel.answerSecurityQuestion);
        if (viewModel.passwordExists) this.showExistingPasswordError();
        if (viewModel.requestOtpError) this.showRequestOtpError();
        if (viewModel.verifyOtpServerError) this.showVerifyOtpServerError();
        if (viewModel.verifyQuestionAnswerError) this.showVerifyQuestionAnswerError();
        if (viewModel.verifyQuestionServerAnswerError) this.showVerifyQuestionAnswerServerError();
        if (viewModel.getAnswerSecurityQuestionError) this.showGetAnswerSecurityQuestionError();
        if (viewModel.updateUsernameError) this.showUpdateUsernameError();
        if (viewModel.updateUsernameServerError) this.showUpdateUsernameServerError();
        if (viewModel.SecurityQuestionExistsError) this.showSecurityQuestionExistsError();
        if (viewModel.updateSecurityQuestionError) this.showUpdateSecurityQuestionError();
        if (viewModel.fetchSecurityQuestionsError) this.showFetchSecurityQuestionsError();
        if (viewModel.checkSecurityAccessError) this.showCheckSecurityAccessError();
        if (viewModel.verifyOtpError) this.showVerifyOtpError();
        if (viewModel.addNewAddress) this.showAddNewAddressForm(viewModel.addNewAddress);      
        if (viewModel.editAddress) this.showEditAddressForm(viewModel.editAddress);
        if (viewModel.secureAccessOptionError) this.showSecureAccessOptionError();
        if (viewModel.passwordExistsServerError) this.showPasswordExistsServerError();
        if (viewModel.passwordServerError) this.showPasswordServerError();
        if (viewModel.emailError) this.showEmailError(viewModel.emailError);
        if (viewModel.phoneDetails) this.showPhoneDetails(viewModel.phoneDetails);    
        if(viewModel.editEmailError) this.showEditEmailError(viewModel.editEmailError);  
        this.AdjustScreen();
        this.view.forceLayout();
      }
    },

    /**
  * Method to hide all the flex of main body
  * @member of frmProfileManagementController
  * @param {JSON} addAddressViewModel - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showAddNewAddressForm: function (addAddressViewModel) {
      this.showAddNewAddress();
      if (addAddressViewModel.serverError) {
        this.view.settings.flxErrorAddNewEmail.setVisibility(true);
        this.view.settings.CopylblError0e8bfa78d5ffc48.text = addAddressViewModel.serverError;
      }
      else {
        this.view.settings.flxErrorAddNewEmail.setVisibility(false);

      }
      this.view.settings.lbxType.masterData = addAddressViewModel.addressTypes;
      this.view.settings.lbxType.selectedKey = addAddressViewModel.addressTypeSelected;
      this.view.settings.tbxCity.text = addAddressViewModel.city;
      this.view.settings.tbxAddressLine1.text = addAddressViewModel.addressLine1;
      this.view.settings.tbxAddressLine2.text = addAddressViewModel.addressLine2;
      this.view.settings.lbxCountry.masterData = addAddressViewModel.countries;
      this.view.settings.lbxCountry.selectedKey = addAddressViewModel.countrySelected;
      this.view.settings.tbxZipcode.text = addAddressViewModel.zipcode;
      this.view.settings.imgSetAsPreferredCheckBox.src = addAddressViewModel.isPreferredAddress ? "checked_box.png" : "unchecked_box.png";
      this.checkNewAddressForm();
      this.view.settings.btnAddNewAddressAdd.onClick = function () {
        this.presenter.userProfile.saveAddress(this.getNewAddressFormData());
      }.bind(this);   
    } ,

    /**
  * Method to hide all the flex of main body
  * @member of frmProfileManagementController
  * @param {JSON} editAddressViewModel - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showEditAddressForm: function (editAddressViewModel) {
      this.showEditAddress();
      this.view.settings.lbxEditType.masterData = editAddressViewModel.addressTypes;
      this.view.settings.lbxEditType.selectedKey = editAddressViewModel.addressTypeSelected;
      this.view.settings.tbxEditAddressLine1.text = editAddressViewModel.addressLine1;
      this.view.settings.tbxEditAddressLine2.text = editAddressViewModel.addressLine2 || "";
      this.view.settings.lbxEditCountry.masterData = editAddressViewModel.countries;
      this.view.settings.lbxEditCountry.selectedKey = editAddressViewModel.countrySelected;
      this.view.settings.tbxEditCity.text = editAddressViewModel.city;
      this.view.settings.tbxEditZipcode.text = editAddressViewModel.zipcode;
      this.view.settings.imgEditSetAsPreferredCheckBox.src = editAddressViewModel.isPreferredAddress ? "checked_box.png" : "unchecked_box.png";
      this.view.settings.flxEditSetAsPreferred.setVisibility(!editAddressViewModel.hidePrefferedAddress);
      this.checkUpdateAddressForm();    
      this.view.settings.btnEditAddressSave.onClick = function () {
        var data = this.getUpdateAddressData();
        data.addressId = editAddressViewModel.addressId
        this.presenter.userProfile.updateAddress(data);
      }.bind(this);
    } ,

    /**
  * Method to show server error while editing username
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showUpdateUsernameServerError: function(){
      this.view.settings.lblError0.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
      this.view.settings.flxErrorEditUsername.setVisibility(true);
      this.view.settings.tbxUsername.text = "";
      this.showEditUsername();
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while requestind OTP
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns{void} - None
  * @throws {void} -None
  */
    showRequestOtpError: function(){
      //warning flex not there
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while verifying OTP
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showVerifyOtpServerError: function(){
      this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(true);
      this.view.settings.lblErrorSecuritySettingsVerification.text=kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while verifying Question and answer
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showVerifyQuestionAnswerError: function(){
      this.view.settings.flxErrorEditSecurityQuestions.setVisibility(true);
      this.view.settings.lblErrorSecurityQuestions.text=kony.i18n.getLocalizedString("i18n.ProfileManagement.invalidAnswers");
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show server error while displaying Question and answer
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showVerifyQuestionAnswerServerError: function(){
      this.view.settings.flxErrorEditSecurityQuestions.setVisibility(true);
      this.view.settings.lblErrorSecurityQuestions.text=kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while fetching Question and answer
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showGetAnswerSecurityQuestionError: function(){
      //warning flex not there for 2 questions
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error updating the username when service is hit
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showUpdateUsernameError: function(){
      this.view.settings.lblError0.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.usernameAlreadyExists");
      this.view.settings.flxErrorEditUsername.setVisibility(true);
      this.view.settings.tbxUsername.text = "";
      this.showEditUsername();
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while displaying Question and answer
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showSecurityQuestionExistsError: function(){
      this.view.settings.lblErrorSecuritySettings.text =kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
      this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
      this.showEditSecuritySettings();
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while updating Question and answer at the service side
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showUpdateSecurityQuestionError: function(){
      this.view.settings.lblErrorSecuritySettings.text =kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
      this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
      this.showEditSecuritySettings();
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while fetching Question and answer from backend
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showFetchSecurityQuestionsError: function(){
      this.view.settings.lblErrorSecuritySettings.text =kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");
      this.view.settings.flxErrorEditSecuritySettings.setVisibility(true);
      this.showViews(["flxEditSecuritySettingsWrapper"]);
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while fetching the secure access
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showCheckSecurityAccessError: function(){
      //error flx not there
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while verifying OTP from backend
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showVerifyOtpError: function(){
      this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(true);
      this.view.settings.lblErrorSecuritySettingsVerification.text=kony.i18n.getLocalizedString("i18n.ProfileManagement.invalidCode");
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while updating the secure access options
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showSecureAccessOptionError: function(){
      //error flx not there
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while updating the password and server is down
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showPasswordExistsServerError: function(){
      this.view.settings.flxErrorEditPassword.setVisibility(true);
      this.view.settings.tbxExistingPassword.text = "";
      this.view.settings.tbxNewPassword.text = "";
      this.view.settings.tbxConfirmPassword.text = "";
      this.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError");  
      this.showViews(["flxEditPasswordWrapper"]);
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show error while updating the password and ad password is not same for the existing password entered by user
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showExistingPasswordError: function(){
      this.view.settings.flxErrorEditPassword.setVisibility(true);
      this.view.settings.tbxExistingPassword.text = "";
      this.view.settings.tbxNewPassword.text = "";
      this.view.settings.tbxConfirmPassword.text = "";
      this.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.passwordExists");
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to convert question and answer to JSON 
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {JSON} data- returns the JSON of security questions while verifying the security answers
  * @throws {void} -None
  */
    onSaveAnswerSecurityQuestions: function(){
      var data=[{
        answer:"", question_id:""
      },{
        answer: "",
        question_id: ""
      }];
      var quesData="";
      quesData=this.view.settings.lblAnswerSecurityQuestion1.text;
      data[0].answer=this.view.settings.tbxAnswers1.text;
      data[0].question_id=this.getQuestionIDForAnswer(quesData);
      quesData=this.view.settings.lblAnswerSecurityQuestion2.text;
      data[1].answer=this.view.settings.tbxAnswers2.text;
      data[1].question_id=this.getQuestionIDForAnswer(quesData);

      return data;
    },
    /**
  * Method to return the ID of question sent from backend
  * @member of frmProfileManagementController
  * @param {String} quesData- it is the question who's ID is to be searched
  * @returns {Int} qData- returns the ID of the Question sent from backend
  * @throws {void} -None
  */
    getQuestionIDForAnswer:function(quesData){
      var qData;
      for(var i=0;i<this.responseBackend.length;i++){
        if(quesData===this.responseBackend[i].question){
          qData=this.responseBackend[i].question_id;
        }
      }
      return qData;
    },
    /**
  * Method to show security questions for verification
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- Set of questions to be shown
  * @returns {void} - None
  * @throws {void} -None
  */
    showSecurityVerification: function(viewModel){
      CommonUtilities.showProgressBar(this.view);
      this.responseBackend = viewModel.data;
      var self = this;
      this.view.settings.lblAnswerSecurityQuestion1.text = viewModel.data[0].question;
      this.view.settings.lblAnswerSecurityQuestion2.text = viewModel.data[1].question;
      this.view.settings.btnBackAnswerSecurityQuestions.onClick = function(){
        self.showUsernameVerificationByChoice();
      };
      this.showAnswerSecurityQuestions();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to handle the configurations at account settings
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */  
    configurationAccountSettings: function(){
      if(CommonUtilities.getConfiguration('enableAccountSettings')==="true"){
        this.view.settings.flxAccountSettings.setVisibility(true);
        this.view.settings.flxAccountSettingsSubMenu.setVisibility(true); 
      }else{
        this.view.settings.flxAccountSettings.setVisibility(false);
        this.view.settings.flxAccountSettingsSubMenu.setVisibility(false);  
      }

      if(CommonUtilities.getConfiguration('enableDefaultAccounts')==="true"){
        this.view.settings.flxSetDefaultAccount.setVisibility(true);
      }else{
        this.view.settings.flxSetDefaultAccount.setVisibility(false);
      }

      if(CommonUtilities.getConfiguration('enablePreferredAccounts')==="true"){
        this.view.settings.flxAccountPreferences.setVisibility(true);
      }else{
        this.view.settings.flxAccountPreferences.setVisibility(false);
      }

      if(CommonUtilities.getConfiguration('enablePreferredAccounts')==="false" && CommonUtilities.getConfiguration('enableDefaultAccounts')==="false"){
        this.view.settings.flxAccountSettings.setVisibility(false);
        this.view.settings.flxAccountSettingsSubMenu.setVisibility(false); 
      }else{
        this.view.settings.flxAccountSettings.setVisibility(true);
        this.view.settings.flxAccountSettingsSubMenu.setVisibility(true);  
      }

    },
    /**
  * Method to manupulate the UI of the screen
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    postShowProfileManagement: function(){
      this.setFlowActions();
      this.setAlertsFlowActions();
      this.disableButton(this.view.settings.btnEditAccountsSave);
      this.AdjustScreen();
    },
    //UI Code
    AdjustScreen: function() {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
          this.view.flxFooter.top = mainheight + diff + "dp";
        else
          this.view.flxFooter.top = mainheight + "dp";        
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },   
    setSeperatorHeight:function(){
      this.view.settings.flxSeperator3.height=this.view.settings.flxRight.frame.height+"px";
      this.view.forceLayout();
    },
    setAlertsFlowActions:function(){
      //functions for menu flow
      var scopeObj=this;
      this.view.settings.flxTransactionAndPaymentsAlerts.onClick=function(){
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.text=kony.i18n.getLocalizedString("i18n.Alerts.TransactionAndPaymentAlertSettings");
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info={"currentScreen":"disabled","isEnabled":false,"settings":"tAndP"};
        scopeObj.showViews(["flxTransactionalAndPaymentsAlertsWrapper"]);
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.flxSecurityAlerts.onClick=function(){
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.text=kony.i18n.getLocalizedString("i18n.Alerts.SecurityAlertSettings");
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info={"currentScreen":"enabled","isEnabled":true,"settings":"security"};
        scopeObj.view.settings.securityAlerts.rtxAlertsWarning.text="We are providing these messages as mandatory alerts. From security point of view you can not switch it off.";
        scopeObj.view.settings.securityAlerts.flxAlertsWarningWrapper.left="20px";
        scopeObj.showViews(["flxSecurityAlertsWrapper"]);
        scopeObj.showSecurityAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.flxPromotionalAlerts.onClick=function(){
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.text=kony.i18n.getLocalizedString("i18n.Alerts.PromotionalAlertSettings");
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info={"currentScreen":"enabled","isEnabled":true,"settings":"promotional"};
        scopeObj.showViews(["flxPromotionalAlertsWrapper"]);
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };

      //transcactional and payment alerts button actions
      this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.onClick=function(){
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.btnSave.onClick=function(){
        if (scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen==="confirm disable") {
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled=false;
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="disabled";
        } else {
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="enabled";
        }
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.btnEnableAlerts.onClick=function(){
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled=true;
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.btnCancel.onClick=function(){
        if (scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled===false) {
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="disabled";
        } else {
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="enabled";
        }
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.flxEnableAlertsCheckBox.onClick=function(){
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="confirm disable";
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.flxChannel1.onClick=function(){
        scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.transactionalAndPaymentsAlerts);
      };
      this.view.settings.transactionalAndPaymentsAlerts.flxChannel2.onClick=function(){
        scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.transactionalAndPaymentsAlerts);
      };
      this.view.settings.transactionalAndPaymentsAlerts.flxChannel3.onClick=function(){
        scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.transactionalAndPaymentsAlerts);
      };

      //security alerts button actions
      this.view.settings.securityAlerts.btnModifyAlerts.onClick=function(){
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showSecurityAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.securityAlerts.btnSave.onClick=function(){
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info.currentScreen="enabled";
        scopeObj.showSecurityAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.securityAlerts.btnCancel.onClick=function(){
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info.currentScreen="enabled";
        scopeObj.showSecurityAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.securityAlerts.flxChannel1.onClick=function(){
        scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.securityAlerts);
      };
      this.view.settings.securityAlerts.flxChannel2.onClick=function(){
        scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.securityAlerts);
      };
      this.view.settings.securityAlerts.flxChannel3.onClick=function(){
        scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.securityAlerts);
      };

      //promotional alerts button actions
      this.view.settings.promotionalAlerts.btnModifyAlerts.onClick=function(){
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.btnSave.onClick=function(){
        if (scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen==="confirm disable") {
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled=false;
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="disabled";
        } else {
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="enabled";
        }
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.btnEnableAlerts.onClick=function(){
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled=true;
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.btnCancel.onClick=function(){
        if (scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled===false) {
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="disabled";
        } else {
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="enabled";
        }
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.flxEnableAlertsCheckBox.onClick=function(){
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="confirm disable";
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.flxChannel1.onClick=function(){
        scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.promotionalAlerts);
      };
      this.view.settings.promotionalAlerts.flxChannel2.onClick=function(){
        scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.promotionalAlerts);
      };
      this.view.settings.promotionalAlerts.flxChannel3.onClick=function(){
        scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.promotionalAlerts);
      };
    },

    hideAllWidgetsAlerts:function(alertWidget){
      alertWidget.btnModifyAlerts.setVisibility(false);
      alertWidget.flxWhiteSpace.setVisibility(false);
      alertWidget.flxClickBlocker.setVisibility(false);

      alertWidget.flxAlertsStatusCheckbox.setVisibility(false);
      alertWidget.flxAlertsRow1.setVisibility(false);
      alertWidget.flxAlertsRow2.setVisibility(false);
      alertWidget.flxAlertsSegment.setVisibility(false);

      alertWidget.flxButtons.setVisibility(false);
      alertWidget.btnEnableAlerts.setVisibility(false);
      alertWidget.btnCancel.setVisibility(false);
      alertWidget.btnSave.setVisibility(false);
    },
    showTAndPAlerts:function(){
      this.hideAllWidgetsAlerts(this.view.settings.transactionalAndPaymentsAlerts);
      if (this.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen==="enabled" && this.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled===true) {
        this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxClickBlocker.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsRow2.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsSegment.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsStatusCheckbox.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.rtxAlertsWarning.text="Alerts is Enabled. You will receive Alerts for Balance & spending related activities. You can select the Alerts and the channels.";
        this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel1.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel2.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel3.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text=kony.i18n.getLocalizedString("i18n.Alerts.Enabled");
        this.setAlertsSegmentData(this.view.settings.transactionalAndPaymentsAlerts);
      }
      else if (this.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen==="modify" && this.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled===true) {
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsRow2.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsSegment.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsStatusCheckbox.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxButtons.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.btnCancel.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.btnSave.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.rtxAlertsWarning.text="Alerts is Enabled. You will receive Alerts for Balance & spending related activities. You can select the Alerts and the channels.";
        this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatusCheckBox.skin="skn3343a820pxolbfonticons";
        this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel1.skin="skn3343a820pxolbfonticons";
        this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel2.skin="skn3343a820pxolbfonticons";
        this.view.settings.transactionalAndPaymentsAlerts.lblCheckBoxChannel3.skin="skn3343a820pxolbfonticons";
        this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text=kony.i18n.getLocalizedString("i18n.Alerts.Enabled");
        this.setAlertsSegmentData(this.view.settings.transactionalAndPaymentsAlerts);
      }
      else if (this.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen==="confirm disable" && this.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled===true) {
        this.view.settings.transactionalAndPaymentsAlerts.flxWhiteSpace.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxButtons.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.btnSave.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.btnCancel.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text=kony.i18n.getLocalizedString("i18n.Alerts.Disabled");
        this.view.settings.transactionalAndPaymentsAlerts.rtxAlertsWarning.text="Alerts is disabled. You will not receive any Alerts for Balance & spending related activities.<br>Click Enable to start receiving Alerts.";
      }
      else if (this.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled===false) {
        this.view.settings.transactionalAndPaymentsAlerts.flxWhiteSpace.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.flxButtons.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.btnEnableAlerts.setVisibility(true);
        this.view.settings.transactionalAndPaymentsAlerts.lblAlertsStatus.text=kony.i18n.getLocalizedString("i18n.Alerts.Disabled");
        this.view.settings.transactionalAndPaymentsAlerts.rtxAlertsWarning.text="Alerts is disabled. You will not receive any Alerts for Balance & spending related activities.<br>Click Enable to start receiving Alerts.";
      }
    },
    showPromotionalAlerts:function(){
      this.hideAllWidgetsAlerts(this.view.settings.promotionalAlerts);
      if (this.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen==="enabled" && this.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled===true) {
        this.view.settings.promotionalAlerts.btnModifyAlerts.setVisibility(true);
        this.view.settings.promotionalAlerts.flxClickBlocker.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsRow2.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsSegment.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsStatusCheckbox.setVisibility(true);
        this.view.settings.promotionalAlerts.lblAlertsStatus.text=kony.i18n.getLocalizedString("i18n.Alerts.Enabled");
        this.view.settings.promotionalAlerts.lblAlertsStatusCheckBox.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.promotionalAlerts.lblCheckBoxChannel1.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.promotionalAlerts.lblCheckBoxChannel2.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.promotionalAlerts.lblCheckBoxChannel3.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.promotionalAlerts.rtxAlertsWarning.text="Alerts is Enabled. You will receive Alerts for Balance & spending related activities. You can select the Alerts and the channels.";
        this.setAlertsSegmentData(this.view.settings.promotionalAlerts);
      }
      else if (this.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen==="modify" && this.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled===true) {
        this.view.settings.promotionalAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsRow2.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsSegment.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsStatusCheckbox.setVisibility(true);
        this.view.settings.promotionalAlerts.flxButtons.setVisibility(true);
        this.view.settings.promotionalAlerts.btnCancel.setVisibility(true);
        this.view.settings.promotionalAlerts.btnSave.setVisibility(true);
        this.view.settings.promotionalAlerts.lblAlertsStatus.text=kony.i18n.getLocalizedString("i18n.Alerts.Enabled");
        this.view.settings.promotionalAlerts.lblAlertsStatusCheckBox.skin="skn3343a820pxolbfonticons";
        this.view.settings.promotionalAlerts.lblCheckBoxChannel1.skin="skn3343a820pxolbfonticons";
        this.view.settings.promotionalAlerts.lblCheckBoxChannel2.skin="skn3343a820pxolbfonticons";
        this.view.settings.promotionalAlerts.lblCheckBoxChannel3.skin="skn3343a820pxolbfonticons";
        this.view.settings.promotionalAlerts.rtxAlertsWarning.text="Alerts is Enabled. You will receive Alerts for Balance & spending related activities. You can select the Alerts and the channels.";
        this.setAlertsSegmentData(this.view.settings.promotionalAlerts);
      }
      else if (this.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen==="confirm disable" && this.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled===true) {
        this.view.settings.promotionalAlerts.flxWhiteSpace.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.promotionalAlerts.flxButtons.setVisibility(true);
        this.view.settings.promotionalAlerts.btnSave.setVisibility(true);
        this.view.settings.promotionalAlerts.btnCancel.setVisibility(true);
        this.view.settings.promotionalAlerts.lblAlertsStatus.text=kony.i18n.getLocalizedString("i18n.Alerts.Disabled");
        this.view.settings.promotionalAlerts.rtxAlertsWarning.text="Alerts is disabled. You will not receive any Alerts for Balance & spending related activities.<br>Click Enable to start receiving Alerts.";
      }
      else if (this.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled===false) {
        this.view.settings.promotionalAlerts.flxWhiteSpace.setVisibility(true);
        this.view.settings.promotionalAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.promotionalAlerts.flxButtons.setVisibility(true);
        this.view.settings.promotionalAlerts.btnEnableAlerts.setVisibility(true);
        this.view.settings.promotionalAlerts.lblAlertsStatus.text=kony.i18n.getLocalizedString("i18n.Alerts.Disabled");
        this.view.settings.promotionalAlerts.rtxAlertsWarning.text="Alerts is disabled. You will not receive any Alerts for Balance & spending related activities.<br>Click Enable to start receiving Alerts.";
      }
    },
    showSecurityAlerts:function(){
      this.hideAllWidgetsAlerts(this.view.settings.securityAlerts);
      if (this.view.settings.securityAlerts.lblAlertsHeading.info.currentScreen==="enabled" && this.view.settings.securityAlerts.lblAlertsHeading.info.isEnabled===true) {
        this.view.settings.securityAlerts.btnModifyAlerts.setVisibility(true);
        this.view.settings.securityAlerts.flxClickBlocker.setVisibility(true);
        this.view.settings.securityAlerts.flxAlertsRow2.setVisibility(true);
        this.view.settings.securityAlerts.flxAlertsSegment.setVisibility(true);
        this.view.settings.securityAlerts.flxAlertsStatusCheckbox.setVisibility(true);
        this.view.settings.securityAlerts.lblAlertsStatusCheckBox.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.securityAlerts.lblCheckBoxChannel1.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.securityAlerts.lblCheckBoxChannel2.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.view.settings.securityAlerts.lblCheckBoxChannel3.skin="skna0a0a0Bgf7f7f720pxolbfonticons";
        this.setAlertsSegmentData(this.view.settings.securityAlerts);
      }
      else if (this.view.settings.securityAlerts.lblAlertsHeading.info.currentScreen==="modify" && this.view.settings.securityAlerts.lblAlertsHeading.info.isEnabled===true) {
        this.view.settings.securityAlerts.flxAlertsRow1.setVisibility(true);
        this.view.settings.securityAlerts.flxAlertsRow2.setVisibility(true);
        this.view.settings.securityAlerts.flxAlertsSegment.setVisibility(true);
        this.view.settings.securityAlerts.flxAlertsStatusCheckbox.setVisibility(true);
        this.view.settings.securityAlerts.flxButtons.setVisibility(true);
        this.view.settings.securityAlerts.btnCancel.setVisibility(true);
        this.view.settings.securityAlerts.btnSave.setVisibility(true);
        this.view.settings.securityAlerts.lblAlertsStatusCheckBox.skin="skn3343a820pxolbfonticons";
        this.view.settings.securityAlerts.lblCheckBoxChannel1.skin="skn3343a820pxolbfonticons";
        this.view.settings.securityAlerts.lblCheckBoxChannel2.skin="skn3343a820pxolbfonticons";
        this.view.settings.securityAlerts.lblCheckBoxChannel3.skin="skn3343a820pxolbfonticons";
        this.setAlertsSegmentData(this.view.settings.securityAlerts);
      }
    },
    toggleChannel1Checkboxes:function(alertWidget){
      if(alertWidget.lblCheckBoxChannel1.text==="C")
        alertWidget.lblCheckBoxChannel1.text="D";
      else
        alertWidget.lblCheckBoxChannel1.text="C";
      var newData=alertWidget.segAlerts.data;
      for (var i = 0; i < newData.length; i++) {
        newData[i].lblCheckBoxChannel1.text=alertWidget.lblCheckBoxChannel1.text;
      }
      alertWidget.segAlerts.setData(newData);
    },
    toggleChannel2Checkboxes:function(alertWidget){
      if(alertWidget.lblCheckBoxChannel2.text==="C")
        alertWidget.lblCheckBoxChannel2.text="D";
      else
        alertWidget.lblCheckBoxChannel2.text="C";
      var newData=alertWidget.segAlerts.data;
      for (var i = 0; i < newData.length; i++) {
        newData[i].lblCheckBoxChannel2.text=alertWidget.lblCheckBoxChannel2.text;
      }
      alertWidget.segAlerts.setData(newData);
    },
    toggleChannel3Checkboxes:function(alertWidget){
      if(alertWidget.lblCheckBoxChannel3.text==="C")
        alertWidget.lblCheckBoxChannel3.text="D";
      else
        alertWidget.lblCheckBoxChannel3.text="C";
      var newData=alertWidget.segAlerts.data;
      for (var i = 0; i < newData.length; i++) {
        newData[i].lblCheckBoxChannel3.text=alertWidget.lblCheckBoxChannel3.text;
      }
      alertWidget.segAlerts.setData(newData);
    },
    setAlertsSegmentData:function(alertWidget){
      var scopeObj=this;
      var rowData;
      var dataMap={
        "flxAlerts": "flxAlerts",
        "flxAlertsCheckBoxChannels": "flxAlertsCheckBoxChannels",
        "flxAlertsStatusCheckbox": "flxAlertsStatusCheckbox",
        "flxChannel1": "flxChannel1",
        "flxChannel2": "flxChannel2",
        "flxChannel3": "flxChannel3",
        "flxRow1": "flxRow1",
        "flxRow2": "flxRow2",
        "flxSeperator": "flxSeperator",
        "flxTextbox": "flxTextbox",
        "lblAlertsStatusCheckBox": "lblAlertsStatusCheckBox",
        "lblChannel1": "lblChannel1",
        "lblChannel2": "lblChannel2",
        "lblChannel3": "lblChannel3",
        "lblCheckBoxChannel1": "lblCheckBoxChannel1",
        "lblCheckBoxChannel2": "lblCheckBoxChannel2",
        "lblCheckBoxChannel3": "lblCheckBoxChannel3",
        "lblCurrencySymbol": "lblCurrencySymbol",
        "lblEnableAlerts": "lblEnableAlerts",
        "lblSelectMethods": "lblSelectMethods",
        "lblSeperator": "lblSeperator",
        "tbxAmount": "tbxAmount"
      };
      data=[
        {
          "flxAlertsStatusCheckbox": {
            "isVisible":alertWidget.lblAlertsHeading.info.settings==="security"?false:true,
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblAlertsStatusCheckBox.text=(rowData.lblAlertsStatusCheckBox.text==="C" ?"D":"C");
              rowData.flxRow2.isVisible=(rowData.lblAlertsStatusCheckBox.text==="C"?true:false);
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel1": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel1.text=(rowData.lblCheckBoxChannel1.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel2": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel2.text=(rowData.lblCheckBoxChannel2.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel3": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel3.text=(rowData.lblCheckBoxChannel3.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxRow1": "flxRow1",
          "flxRow2": {"isVisible":true},
          "flxTextbox":{"isVisible":true},
          "lblAlertsStatusCheckBox": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel1": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel2": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel3": {
            "text":"D",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblChannel1": kony.i18n.getLocalizedString("i18n.ProfileManagement.Phone"),
          "lblChannel2": kony.i18n.getLocalizedString("i18n.Alerts.Email"),
          "lblChannel3": kony.i18n.getLocalizedString("i18n.Alerts.Notifications"),
          "lblCurrencySymbol": kony.i18n.getLocalizedString("i18n.Alerts.currencySymbol$"),
          "lblEnableAlerts": "When Bill Payment exceeds",
          "lblSelectMethods": kony.i18n.getLocalizedString("i18n.Alerts.SelectedMethodForThisAlert"),
          "lblSeperator": "'",
          "tbxAmount": {
            "text":"1,000.00"
          }
        },
        {
          "flxAlertsStatusCheckbox": {
            "isVisible":alertWidget.lblAlertsHeading.info.settings==="security"?false:true,
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblAlertsStatusCheckBox.text=(rowData.lblAlertsStatusCheckBox.text==="C" ?"D":"C");
              rowData.flxRow2.isVisible=(rowData.lblAlertsStatusCheckBox.text==="C"?true:false);
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel1": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel1.text=(rowData.lblCheckBoxChannel1.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel2": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel2.text=(rowData.lblCheckBoxChannel2.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel3": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel3.text=(rowData.lblCheckBoxChannel3.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxRow1": "flxRow1",
          "flxRow2": {"isVisible":true},
          "flxTextbox":{"isVisible":false},
          "lblAlertsStatusCheckBox": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel1": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel2": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel3": {
            "text":"D",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblChannel1": kony.i18n.getLocalizedString("i18n.ProfileManagement.Phone"),
          "lblChannel2": kony.i18n.getLocalizedString("i18n.Alerts.Email"),
          "lblChannel3": kony.i18n.getLocalizedString("i18n.Alerts.Notifications"),
          "lblCurrencySymbol": kony.i18n.getLocalizedString("i18n.Alerts.currencySymbol$"),
          "lblEnableAlerts": "Alert me on special offers",
          "lblSelectMethods": kony.i18n.getLocalizedString("i18n.Alerts.SelectedMethodForThisAlert"),
          "lblSeperator": "'",
          "tbxAmount": {
            "text":"1,000.00"
          }
        },
        {
          "flxAlertsStatusCheckbox": {
            "isVisible":alertWidget.lblAlertsHeading.info.settings==="security"?false:true,
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblAlertsStatusCheckBox.text=(rowData.lblAlertsStatusCheckBox.text==="C" ?"D":"C");
              rowData.flxRow2.isVisible=(rowData.lblAlertsStatusCheckBox.text==="C"?true:false);
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel1": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel1.text=(rowData.lblCheckBoxChannel1.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel2": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel2.text=(rowData.lblCheckBoxChannel2.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel3": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel3.text=(rowData.lblCheckBoxChannel3.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxRow1": "flxRow1",
          "flxRow2": {"isVisible":true},
          "flxTextbox":{"isVisible":false},
          "lblAlertsStatusCheckBox": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel1": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel2": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel3": {
            "text":"D",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblChannel1": kony.i18n.getLocalizedString("i18n.ProfileManagement.Phone"),
          "lblChannel2": kony.i18n.getLocalizedString("i18n.Alerts.Email"),
          "lblChannel3": kony.i18n.getLocalizedString("i18n.Alerts.Notifications"),
          "lblCurrencySymbol": kony.i18n.getLocalizedString("i18n.Alerts.currencySymbol$"),
          "lblEnableAlerts": "Alert me on special offers",
          "lblSelectMethods": kony.i18n.getLocalizedString("i18n.Alerts.SelectedMethodForThisAlert"),
          "lblSeperator": "'",
          "tbxAmount": {
            "text":"1,000.00"
          }
        },
        {
          "flxAlertsStatusCheckbox": {
            "isVisible":alertWidget.lblAlertsHeading.info.settings==="security"?false:true,
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblAlertsStatusCheckBox.text=(rowData.lblAlertsStatusCheckBox.text==="C" ?"D":"C");
              rowData.flxRow2.isVisible=(rowData.lblAlertsStatusCheckBox.text==="C"?true:false);
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel1": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel1.text=(rowData.lblCheckBoxChannel1.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel2": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel2.text=(rowData.lblCheckBoxChannel2.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxChannel3": {
            "onClick":function(){
              rowData=alertWidget.segAlerts.data[alertWidget.segAlerts.selectedIndex[1]];
              rowData.lblCheckBoxChannel3.text=(rowData.lblCheckBoxChannel3.text==="C" ?"D":"C");
              alertWidget.segAlerts.setDataAt(rowData,alertWidget.segAlerts.selectedIndex[1]);
            }
          },
          "flxRow1": "flxRow1",
          "flxRow2": {"isVisible":true},
          "flxTextbox":{"isVisible":false},
          "lblAlertsStatusCheckBox": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel1": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel2": {
            "text":"C",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblCheckBoxChannel3": {
            "text":"D",
            "skin":alertWidget.lblAlertsHeading.info.currentScreen==="modify"?"skn3343a820pxolbfonticons":"skna0a0a0Bgf7f7f720pxolbfonticons"},
          "lblChannel1": kony.i18n.getLocalizedString("i18n.ProfileManagement.Phone"),
          "lblChannel2": kony.i18n.getLocalizedString("i18n.Alerts.Email"),
          "lblChannel3": kony.i18n.getLocalizedString("i18n.Alerts.Notifications"),
          "lblCurrencySymbol": kony.i18n.getLocalizedString("i18n.Alerts.currencySymbol$"),
          "lblEnableAlerts": "Alert me on special offers",
          "lblSelectMethods": kony.i18n.getLocalizedString("i18n.Alerts.SelectedMethodForThisAlert"),
          "lblSeperator": "'",
          "tbxAmount": {
            "text":"1,000.00"
          }
        }
      ];
      alertWidget.segAlerts.widgetDataMap=dataMap;
      alertWidget.segAlerts.setData(data);
    },
    /**
  * Method to navigate to terms and Conditions
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    NavToTermsAndConditions: function() {
      this.view.flxTermsAndConditions.setVisibility(true);
    },
    /**
  * Method to close Terms and Conditions
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    CloseTermsAndConditions: function() {
      this.view.flxTermsAndConditions.setVisibility(false);
    },
    /**
  * Method to enable/disable the terms and condition button
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    SaveTermsAndConditions: function() {
      if(this.view.imgTCContentsCheckbox.src === "checked_box.png") {
        this.view.settings.imgTCContentsCheckbox.src = "checked_box.png";
        this.enableButton(this.view.settings.btnEditAccountsSave);  
      }  
      else {
        this.view.settings.imgTCContentsCheckbox.src = "unchecked_box.png";
        this.disableButton(this.view.settings.btnEditAccountsSave);    
      }
      this.view.flxTermsAndConditions.setVisibility(false);  
    },
    /**
  * Method to show all the security questions screen after fetching from backend
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- None
  * @returns {void} - None
  * @throws {void} -None
  */  
    showSecurityQuestions: function (viewModel){
      CommonUtilities.showProgressBar(this.view);
      var self = this;
      if(viewModel.data.result != "Questions Exist"){
        flag =0;
        this.showEditSecuritySettings();
        this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
        this.adjustScreen();
      }
      else{
        this.showViews(["flxEditSecuritySettingsWrapper"]);
        this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(false);
        this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(true);
        this.view.settings.flxSecurityQuestionSet.setVisibility(false);
        this.view.settings.flxEditSecuritySettingsButtons.top = "340dp";
        this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
        this.view.settings.btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ResetQuestions");
        this.enableButton(this.view.settings.btnEditSecuritySettingsProceed);
      }
      this.view.settings.btnEditSecuritySettingsProceed.onclick= function(){
        self.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
        flag = 0;
        if (self.view.settings.btnEditSecuritySettingsProceed.text == kony.i18n.getLocalizedString("i18n.ProfileManagement.ResetQuestions")) {
          self.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(true);
          self.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(false);
          self.view.settings.flxSecurityQuestionSet.setVisibility(false);
          self.view.settings.flxEditSecuritySettingsButtons.top = "0dp";
          self.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(true);
          self.view.settings.flxSecurityQASet1.setVisibility(true);
          self.view.settings.flxSecurityQASet2.setVisibility(true);
          self.view.settings.flxSecurityQASet3.setVisibility(true);
          self.view.settings.flxSecurityQASet4.setVisibility(true);
          self.view.settings.flxSecurityQASet5.setVisibility(true);
          self.view.settings.btnEditSecuritySettingsCancel.setVisibility(true);
          self.view.settings.btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");
          self.showEditSecuritySettings();
          self.adjustScreen();
        } else {
          self.onProceedSQ();
          self.selectedQuestions = {
            ques: ["None", "None", "None", "None", "None"],
            key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
          };
          self.selectedQuestionsTemp= {
            securityQuestions: [],
            flagToManipulate: []
          };
          update = "security questions";
          self.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SendingYouTheOtp");
          self.view.settings.lblSendingOTPMessage.text =kony.i18n.getLocalizedString("i18n.common.sendingOTP");
          self.view.settings.flxOTPtextbox.setVisibility("false");
          //scopeObj.enableButton(scopeObj.view.settings.btnUsernameVerificationSend);
          self.view.settings.btnSecuritySettingVerificationCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
          self.view.settings.btnSecuritySettingVerificationSend.text = kony.i18n.getLocalizedString("i18n.common.send");
          self.enableButton(self.view.settings.btnSecuritySettingVerificationSend);
          self.view.settings.btnSecuritySettingVerificationCancel.onClick = function(){
            self.presenter.checkSecurityQuestions();
            self.setSelectedSkin("SecurityQuestions");
            self.view.forceLayout();
          }
          self.showSecuritySettingVerification();
        }
      };
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);

    },
    /**
  * Method to change the the progress bar state
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- None
  * @returns {void} - None
  * @throws {void} -None
  */  
    changeProgressBarState: function (isLoading) {
      if (isLoading) {
        CommonUtilities.showProgressBar(this.view);
      } else {
        CommonUtilities.hideProgressBar(this.view);
      }
    },
    /**
  * Render The Profile Settings View
  * @param {Object} userProfielViewModel 
  * Values : name, dob, maskedSSN
  */

    updateUserProfileSetttingsView: function (userProfileViewModel) {
      this.showProfile();
      this.collapseAll();
      this.setSelectedSkin("Profile");
      this.expandWithoutAnimation(this.view.settings.flxProfileSettingsSubMenu);
      this.view.settings.lblNameValue.text = userProfileViewModel.name;
      this.view.settings.lblDOBValue.text = userProfileViewModel.dob;
      this.view.settings.lblSocialSecurityValue.text = userProfileViewModel.maskedSSN;
      this.view.settings.imgProfile.src = userProfileViewModel.userImage
    },
    /**
  * Method to to update the hamburger menu
  * @member of frmProfileManagementController
  * @param {Object} sideMenuModel- None
  * @returns {void} - None
  * @throws {void} -None
  */
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel);
    },
    /**
  * Method to change the the progress bar state
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- None
  * @returns {void} - None
  * @throws {void} -None
  */
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },
    /**
  * Method to update the list of emails
  * @member of frmProfileManagementController
  * @param {JSON} emailListViewModel- list of emails
  * @returns {void} - None
  * @throws {void} -None
  */
    updateEmailList: function (emailListViewModel) {
      this.showEmail();
      if (emailListViewModel.length  >= 3) {
        this.view.settings.btnAddNewEmail.setVisibility(false);
        this.view.settings.btnEditAddNewEmail.setVisibility(false);
      }
      else {
        this.view.settings.btnAddNewEmail.setVisibility(true);
        this.view.settings.btnEditAddNewEmail.setVisibility(true);
      }
      this.setEmailSegmentData(emailListViewModel);
      this.setSelectedSkin("Email");
    },
    /**
  * Method to update the list of address
  * @member of frmProfileManagementController
  * @param {JSON} addressListViewModel- list of addresses
  * @returns {void} - None
  * @throws {void} -None
  */
    updateAddressList: function (addressListViewModel) {
      this.showAddresses();
      if (addressListViewModel.length >= 3) {
        this.view.settings.btnAddNewAddress.setVisibility(false);      
      }
      else {
        this.view.settings.btnAddNewAddress.setVisibility(true);            
      }
      this.setAddressSegmentData(addressListViewModel);
      this.setSelectedSkin("Address");
    },
    /**
  * Method to update the list of phone number
  * @member of frmProfileManagementController
  * @param {JSON} phoneListViewModel- list of phone numbers
  * @returns {void} - None
  * @throws {void} -None
  */
    updatePhoneList: function (phoneListViewModel) {
      this.showPhoneNumbers();
      if (phoneListViewModel.length >= 3) {
        this.view.settings.btnAddNewNumber.setVisibility(false);
      }
      else {
        this.view.settings.btnAddNewNumber.setVisibility(true);
      }
      this.setPhoneSegmentData(phoneListViewModel);
      this.setSelectedSkin("Phone");
    },
    /**
  * Method to update the module while adding a new phone number
  * @member of frmProfileManagementController
  * @param {JSON} addPhoneViewModel- responce from backend after fetching phone number
  * @returns {void} - None
  * @throws {void} -None
  */
    updateAddPhoneViewModel: function (addPhoneViewModel) {
      if (addPhoneViewModel.serverError) {
        this.view.settings.flxErrorAddPhoneNumber.setVisibility(true);
        this.view.settings.CopylblError0f2f036aaf7534c.text = addPhoneViewModel.serverError;
      }
      else {
        this.showAddPhonenumber();
        this.view.settings.flxErrorAddPhoneNumber.setVisibility(false);    
        this.view.settings.imgAddRadioBtnUS.src = addPhoneViewModel.countryType === 'domestic' ?  "icon_radiobtn.png": "icon_radiobtn_active.png" ;
        this.showPhoneRadiobtn(this.view.settings.imgAddRadioBtnUS, this.view.settings.imgAddRadioBtnInternational);
        this.view.settings.lbxAddPhoneNumberType.masterData = addPhoneViewModel.phoneTypes;
        this.view.settings.lbxAddPhoneNumberType.selectedKey = addPhoneViewModel.phoneTypeSelected;
        this.view.settings.tbxAddPhoneNumber.text = addPhoneViewModel.phoneNumber;
        this.view.settings.tbxAddExtension.text = addPhoneViewModel.ext;
        this.view.settings.imgAddCheckBox3.src = addPhoneViewModel.isPrimary ? "checked_box.png" : "unchecked_box.png";
        this.view.settings.imgAddCheckBox4.src = addPhoneViewModel.recievePromotions ? "checked_box.png" : "unchecked_box.png";
        this.setAddPhoneServicesData(addPhoneViewModel.services);
        this.view.settings.btnAddPhoneNumberCancel.onClick = function () {
          this.showPhoneNumbers();
        }.bind(this);
        this.view.settings.btnAddPhoneNumberSave.onClick = function () {
          if(this.validateAddPhoneNumberForm()) {
            this.presenter.userProfile.savePhoneNumber(this.getNewPhoneFormData());
          }
        }.bind(this);
      }
    },
    /**
  * Method to update add phone number UI based on the error or success senario
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    validateAddPhoneNumberForm: function () {
      var phoneData = this.getNewPhoneFormData();
      if (!this.isPhoneNumberValid(phoneData.phoneNumber)) {
        this.view.settings.flxErrorAddPhoneNumber.setVisibility(true);
        this.view.settings.CopylblError0f2f036aaf7534c.text = kony.i18n.getLocalizedString("i18n.profile.notAValidPhoneNumber");
        return false;
      }
      else {
        this.view.settings.flxErrorAddPhoneNumber.setVisibility(false);
        return true;
      }
    },
    /**
  * Method to update the list of emails
  * @member of frmProfileManagementController
  * @param {Int} phoneNumber - phone number entered by user
  * @returns {void} - None
  * @throws {void} -None
  */
    isPhoneNumberValid: function (phoneNumber) {
      if (phoneNumber == "" || phoneNumber == null || phoneNumber == undefined)
        return false;
      else {
        var regex = new RegExp("^[0-9]");
        if (regex.test(phoneNumber) && phoneNumber.length == 10) {
          return true;
        } else
          return false;
      }
    },
    /**
  * Method to update edit phone number UI based on the responce form backend
  * @member of frmProfileManagementController
  * @param {JSON} addPhoneViewModel - responce from backend after fetching phone number
  * @returns {void} - None
  * @throws {void} -None
  */
    updateEditPhoneViewModel: function (addPhoneViewModel) {
      if (addPhoneViewModel.serverError) {
        this.view.settings.flxError.setVisibility(true);
        this.view.settings.lblError.text = addPhoneViewModel.serverError;
      }
      else {
        this.view.settings.flxErrorAddPhoneNumber.setVisibility(false);
        this.showEditPhonenumber();
        this.view.settings.flxError.setVisibility(false);    
        this.view.settings.imgRadioBtnUS.src = addPhoneViewModel.countryType === 'domestic' ?"icon_radiobtn.png": "icon_radiobtn_active.png" ;
        this.showPhoneRadiobtn(this.view.settings.imgRadioBtnUS, this.view.settings.imgRadioBtnInternational);
        this.view.settings.lbxPhoneNumberType.masterData = addPhoneViewModel.phoneTypes;
        this.view.settings.lbxPhoneNumberType.selectedKey = addPhoneViewModel.phoneTypeSelected;
        this.view.settings.tbxPhoneNumber.text = addPhoneViewModel.phoneNumber;
        this.view.settings.tbxAddExtensionEditPhoneNumber.text = addPhoneViewModel.ext || "";
        this.view.settings.flxOption3.setVisibility(!addPhoneViewModel.isPrimary);
        this.view.settings.flxWarning.setVisibility(!addPhoneViewModel.isPrimary);
        this.view.settings.imgCheckBox3.src = addPhoneViewModel.isPrimary ?  "checked_box.png": "unchecked_box.png";
        this.view.settings.flxCheckBox3.onClick = function () {
          this.toggleCheckBox(this.view.settings.imgCheckBox3);
        }.bind(this);
        this.view.settings.flxCheckBox4.onClick = function () {
          this.toggleCheckBox(this.view.settings.imgCheckBox4);
        }.bind(this);
        this.view.settings.btnEditPhoneNumberCancel.onClick = addPhoneViewModel.onBack;    
        this.view.settings.imgCheckBox4.src = addPhoneViewModel.recievePromotions ? "checked_box.png" : "unchecked_box.png";
        this.view.settings.btnEditPhoneNumberSave.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");

        this.view.settings.btnEditPhoneNumberSave.onClick = function () {
          if (this.validateEditPhoneNumberForm()) {
            this.updatePhoneNumber(addPhoneViewModel, this.getEditPhoneFormData());
          }
        }.bind(this);
        this.checkUpdateEditPhoneForm();
        this.setEditPhoneServicesData(addPhoneViewModel.services);
      }
    },

    updatePhoneNumber: function (editPhoneViewModel, formData) {
      var servicesToSend = [];
      editPhoneViewModel.services.forEach(function (account) {
        if (!account.selected && formData.services[account.id]) {
          servicesToSend.push({id: account.id, selected: true})
        }
        else if (account.selected  && !formData.services[account.id]) {
          servicesToSend.push({id: account.id, selected: false})
        }

      })
      formData.services = servicesToSend;
      this.presenter.userProfile.editPhoneNumber(editPhoneViewModel.id, formData);    
    },

    /**
  * Method to validate the phone umber after editing
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    validateEditPhoneNumberForm: function () {
      var phoneData = this.getEditPhoneFormData();
      if (!this.isPhoneNumberValid(phoneData.phoneNumber)) {
        this.view.settings.flxError.setVisibility(true);
        this.view.settings.lblError.text = kony.i18n.getLocalizedString("i18n.profile.notAValidPhoneNumber");
        return false;
      }
      else {
        this.view.settings.flxError.setVisibility(false);
        return true;
      }
    },
    /**
  * Method to show error while updating email
  * @member of frmProfileManagementController
  * @param {String} errorMessage- Message to be shown
  * @returns {void} - None
  * @throws {void} -None
  */
    showEmailError: function (errorMessage) {
      this.view.settings.flxErrorAddNewEmail.setVisibility(true);
      this.view.settings.CopylblError0e8bfa78d5ffc48.text =  errorMessage; 
    },
    /**
  * Method to error while updating the edit email scenario
  * @member of frmProfileManagementController
  * @param {String} errorMessage- Message to be shown
  * @returns {void} - None
  * @throws {void} -None
  */
    showEditEmailError: function (errorMessage) {
      this.view.settings.flxErrorEditEmail.setVisibility(false);
      this.view.settings.CopylblError0e702a95b68d041.text = errorMessage;
    },
    /**
  * Method to show data related to phone numbers
  * @member of frmProfileManagementController
  * @param {JSON} - which sets the data
  * @returns {void} - None
  * @throws {void} -None
  */
    getNewPhoneFormData: function () {
      return {
        type: this.view.settings.lbxAddPhoneNumberType.selectedKey,
        countryType: this.view.settings.imgAddRadioBtnUS.src === 'icon_radiobtn_active.png' ? 'domestic' : 'international',
        phoneNumber: this.view.settings.tbxAddPhoneNumber.text.trim(),
        extension: this.view.settings.tbxAddExtension.text.trim(),
        isPrimary: this.view.settings.imgAddCheckBox3.src === "checked_box.png",
        services: this.view.settings.segAddPhoneNumbersOption1.data
        .filter(function (data){ return data.imgCheckBox === "checked_box.png"})
        .map(function(data) {return data.id}),
        receivePromotions:false,
      }
    },
    /**
  * Method to show data related to edit phone numbers scenario
  * @member of frmProfileManagementController
  * @param {JSON} - which sets the data
  * @returns {void} - None
  * @throws {void} -None
  */
    getEditPhoneFormData: function () {
      var data = {
        type: this.view.settings.lbxPhoneNumberType.selectedKey,
        countryType: this.view.settings.imgRadioBtnUS.src === 'icon_radiobtn_active.png' ? 'domestic' : 'international',
        phoneNumber: this.view.settings.tbxPhoneNumber.text.trim(),
        isPrimary: this.view.settings.imgCheckBox3.src === "checked_box.png",
        receivePromotions: this.view.settings.imgCheckBox4.src === "checked_box.png",
        extension: this.view.settings.tbxAddExtensionEditPhoneNumber.text.trim()
      }
      var services = {};
      this.view.settings.segEditPhoneNumberOption1.data.forEach(function (data) {
        services[data.id] = data.imgCheckBox === "checked_box.png"
      })
      data.services = services;
      return data;
    },
    /**
  * Method to show data related to New Address scenario
  * @member of frmProfileManagementController
  * @param {JSON} - which sets the data
  * @returns {void} - None
  * @throws {void} -None
  */  
    getNewAddressFormData: function () {
      return {
        addressLine1: this.view.settings.tbxAddressLine1.text.trim(),
        addressLine2: this.view.settings.tbxAddressLine2.text.trim(),
        countrySelected: this.view.settings.lbxCountry.selectedKey,
        city:this.view.settings.tbxCity.text.trim(),
        zipcode: this.view.settings.tbxZipcode.text.trim(),
        isPreferredAddress: this.view.settings.imgSetAsPreferredCheckBox.src === "checked_box.png",
        addressTypeSelected: this.view.settings.lbxType.selectedKey
      };
    },
    /**
  * Method to show data after updating the New Address scenario
  * @member of frmProfileManagementController
  * @param {JSON} - which sets the data
  * @returns {void} - None
  * @throws {void} -None
  */
    getUpdateAddressData: function () {
      return {
        addressLine1: this.view.settings.tbxEditAddressLine1.text.trim(),
        addressLine2: this.view.settings.tbxEditAddressLine2.text.trim(),
        countrySelected:this.view.settings.lbxEditCountry.selectedKey,
        city: this.view.settings.tbxEditCity.text.trim(),
        zipcode: this.view.settings.tbxEditZipcode.text.trim(),
        isPreferredAddress: this.view.settings.imgEditSetAsPreferredCheckBox.src === "checked_box.png",
        addressTypeSelected: this.view.settings.lbxEditType.selectedKey
      };
    },
    /**
  * Method to show data related to New Email scenario
  * @member of frmProfileManagementController
  * @param {JSON} - which sets the data
  * @returns {void} - None
  * @throws {void} -None
  */
    getNewEmailData: function () {
      return {
        emailAddress: this.view.settings.tbxEmailId.text,
        isPrimary: this.view.settings.imgMarkAsPrimaryEmailCheckBox.src === "checked_box.png" 
      };
    },
    /**
  * Method to reset the fields while adding email
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    resetAddEmailForm: function () {
      this.view.settings.tbxEmailId.text = "";
      this.view.settings.imgMarkAsPrimaryEmailCheckBox.src = "unchecked_box.png";
      this.view.settings.flxErrorAddNewEmail.setVisibility(false);
      this.disableButton(this.view.settings.btnAddEmailIdAdd);
    },
    /**
  * Method to enable/disable the button of terms and condition
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showTermsAndConditions:function(){
      this.enableButton(this.view.btnSave);
      this.view.flxTermsAndConditions.isVisible=true;
      if (CommonUtilities.isChecked(this.view.settings.imgEnableEStatementsCheckBox) === true &&  CommonUtilities.isChecked(this.view.settings.imgTCContentsCheckbox) === true) {
        if(CommonUtilities.isChecked(this.view.imgTCContentsCheckbox) === false){
          CommonUtilities.toggleCheckBox(this.view.imgTCContentsCheckbox);
        }    
      }
      else {
        if(CommonUtilities.isChecked(this.view.imgTCContentsCheckbox) === true){
          CommonUtilities.toggleCheckBox(this.view.imgTCContentsCheckbox);
        }
      }
    },
    /**
  * Method to enable/disable the button of terms and condition
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    closeTermsAndConditions:function(){
      this.view.flxTermsAndConditions.isVisible=false;
    },
    /**
  * Method to toggle the buttons of terms and condition
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    toggleTC: function(){
      CommonUtilities.toggleCheckBox(this.view.imgTCContentsCheckbox);
      this.enableButton(this.view.btnSave);
    },
    /**
  * Method on Click of button Save of TnC Popup
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onSaveTnC : function()
    {
      var isEnabledTnCPop = CommonUtilities.isChecked(this.view.imgTCContentsCheckbox);
      var isEnabledTnC = CommonUtilities.isChecked(this.view.settings.imgTCContentsCheckbox);
      if(isEnabledTnC !== isEnabledTnCPop){
        CommonUtilities.toggleCheckBox(this.view.settings.imgTCContentsCheckbox);
      }
      if(isEnabledTnCPop == true){
        this.enableButton(this.view.settings.btnEditAccountsSave);
      }
      else{
        this.disableButton(this.view.settings.btnEditAccountsSave);
      }
      this.view.flxTermsAndConditions.isVisible=false;
    },
    /**
  * Method to manupulate the UI of profile management on post show
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */

    postShowProfileManagement: function () {
      this.setAlertsFlowActions();
      this.view.flxTCContentsCheckbox.onClick=this.toggleTC;
      this.view.flxClose.onClick=this.closeTermsAndConditions;
      this.view.btnCancel.onClick=this.closeTermsAndConditions;
      this.view.btnSave.onClick=this.onSaveTnC;
      if (kony.os.deviceInfo().screenHeight >= "900") {
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = this.view.customheader.frame.height;
        mainheight = mainheight + this.view.flxContainer.frame.height;
        var diff = screenheight - mainheight;

        if (diff > 0)
          this.view.flxFooter.top = diff + "dp";
      }
      this.view.settings.imgAddRadioBtnUS.src = "icon_radiobtn_active.png";
      this.view.settings.imgAddRadioBtnInternational.src = "icon_radiobtn.png";
      this.presenter.loadHamburger('frmProfileManagement');
    },
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel);
    },
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },

    /**
  * Method to show the profile management wrapper
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showProfile: function () {
      this.showViews(["flxProfileWrapper"]);
    },
    /**
  * Method to the UI to change the Name
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showNameChangeRequest: function () {
      this.showViews(["flxNameChangeRequestWrapper"]);
      this.view.flxSettingsWrapper.height = "790dp";
      this.view.settings.height = "720dp";
    },
    /**
  * Method to show phone Numbers UI
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showPhoneNumbers: function () {
      this.showViews(["flxPhoneNumbersWrapper"]);
    },
    /**
  * Method to show the UI of Add Phone Number
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showAddPhonenumber: function () {
      this.showViews(["flxAddPhoneNumbersWrapper"]);
      this.view.settings.flxAddOption2.setVisibility(false);
      this.view.settings.flxAddOption4.setVisibility(false);
      // this.view.settings.flxAddOptions.height = "210dp";
    },
    /**
  * Method to show the details at the phone number flex
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showDetailPhoneNumber: function () {
      // this.setEditPhoneNoOption2SegmentData();
      this.view.settings.lblEditPhoneNumberHeading.text = kony.i18n.getLocalizedString("i18n.profilemanagement.phoneNumberDetail");
      this.view.settings.flxOption2.setVisibility(false);
      this.showViews(["flxEditPhoneNumbersWrapper"]);

      this.view.settings.btnDelete.isVisible = true;
      this.view.settings.btnEdit.isVisible = true;
      this.view.settings.lblExtensionUnEditable.isVisible = true;
      this.view.settings.lblCountryValue.isVisible = true;
      this.view.settings.lblTypeValue.isVisible = true;
      this.view.settings.lblPhoneNumberValue.isVisible = true;

      this.view.settings.flxRadioBtnUS.isVisible = false;
      this.view.settings.flxRadioBtnInternational.isVisible = false;
      this.view.settings.tbxPhoneNumber.isVisible = false;
      this.view.settings.lbxPhoneNumberType.isVisible = false;
      this.view.settings.lblRadioInternational.isVisible = false;
      this.view.settings.lblRadioUS.isVisible = false;
      this.view.settings.tbxAddExtensionEditPhoneNumber.isVisible = false;
      this.view.settings.flxWarning.isVisible = false;
      this.view.settings.btnEditPhoneNumberCancel.isVisible = false;
      this.view.settings.btnEditPhoneNumberSave.text = kony.i18n.getLocalizedString('i18n.ProfileManagement.BACK');
    },
    /**
  * Method to show the details while editing the phone number
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showEditPhonenumber: function () {
      this.view.settings.lblEditPhoneNumberHeading.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.EditPhoneNumber");    
      this.setEditPhoneNoOption2SegmentData();
      this.showViews(["flxEditPhoneNumbersWrapper"]);

      this.view.settings.btnDelete.isVisible = false;
      this.view.settings.btnEdit.isVisible = false;
      this.view.settings.lblExtensionUnEditable.isVisible = false;  
      this.view.settings.lblCountryValue.isVisible = false;
      this.view.settings.lblTypeValue.isVisible = false;
      this.view.settings.lblPhoneNumberValue.isVisible = false;

      this.view.settings.flxRadioBtnUS.isVisible = true;
      this.view.settings.flxRadioBtnInternational.isVisible = true;
      this.view.settings.tbxPhoneNumber.isVisible = true;
      this.view.settings.tbxAddExtensionEditPhoneNumber.isVisible = true;    
      this.view.settings.lbxPhoneNumberType.isVisible = true;
      this.view.settings.lblRadioInternational.isVisible = true;
      this.view.settings.lblRadioUS.isVisible = true;
      this.view.settings.flxOption2.setVisibility(false);
      //TODO: warning visibility subjective
      this.view.settings.flxWarning.isVisible = true;
      this.view.settings.btnEditPhoneNumberCancel.isVisible = true;
      this.view.settings.btnEditPhoneNumberSave.text = kony.i18n.getLocalizedString('i18n.ProfileManagement.SAVE');    
    },
    showEmail: function () {
      this.showViews(["flxEmailWrapper"]);
    },
    showEditEmail: function () {
      this.showViews(["flxEditEmailWrapper"]);
    },
    showAddresses: function () {
      this.showViews(["flxAddressesWrapper"]);
    },
    showAddNewAddress: function () {
      this.showViews(["flxAddNewAddressWrapper"]);
    },
    showEditAddress: function () {
      this.showViews(["flxEditAddressWrapper"]);
    },
    showAccounts: function () {
      this.showViews(["flxAccountsWrapper"]);
    },
    showEditAccounts: function () {
      this.showViews(["flxEditAccountsWrapper"]);
    },
    showUsernameAndPassword: function () {
      scopeObj.view.settings.lblUsernameValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      this.showViews(["flxUsernameAndPasswordWrapper"]);
    },
    showAcknowledgement: function () {
      this.showViews(["flxAcknowledgementWrapper"]);
    },
    showEditPassword: function () {
      this.showViews(["flxEditPasswordWrapper"]);
    },
    showSecuritySettingVerification: function () {
      this.showViews(["flxSecuritySettingVerificationWrapper"]);
    },
    showUsernameVerification: function () {
      this.showViews(["flxUsernameVerificationWrapper2"]);
    },
    showUsernameVerificationByChoice: function () {
      this.showViews(["flxUsernameVerificationWrapper"]);
    },
    showAnswerSecurityQuestions: function () {
      this.showViews(["flxAnswerSecurityQuestionsWrapper"]);
    },
    showSecureAccessCode: function () {
      this.showViews(["flxSecureAccessCodeWrapper"]);
      //       this.view.settings.flxRight.height = "1000dp";
      //       this.view.settings.height = "1020dp";
      //       this.view.flxSettingsWrapper.height = "1090dp";
      flag = 1;
    },
    /**
  * Method to change the image of radio button
  * @member of frmProfileManagementController
  * @param {String} imgRadio1- path of the first radio button
  * @param {String} imgRadio2- path of the second radio button
  * @returns {void} - None
  * @throws {void} -None
  */
    showPhoneRadiobtn: function (imgRadio1, imgRadio2) {
      if (imgRadio1.src === "icon_radiobtn_active.png") {
        imgRadio1.src = "icon_radiobtn.png";
        imgRadio2.src = "icon_radiobtn_active.png";
      } else {
        imgRadio1.src = "icon_radiobtn_active.png";
        imgRadio2.src = "icon_radiobtn.png";
      }
      this.view.forceLayout();
    },
    /**
  * Method to show a particular view among all
  * @member of frmProfileManagementController
  * @param {String} views - ID of the view to be shown
  * @returns {void} - None
  * @throws {void} -None
  */
    showViews: function (views) {
      //       this.view.settings.flxRight.height = "760px";
      //       this.view.settings.height = "700px";
      //       this.view.flxSettingsWrapper.height = "760dp";
      this.view.settings.flxProfileWrapper.setVisibility(false);
      this.view.settings.flxNameChangeRequestWrapper.setVisibility(false);
      this.view.settings.flxPhoneNumbersWrapper.setVisibility(false);
      this.view.settings.flxEditPhoneNumbersWrapper.setVisibility(false);
      this.view.settings.flxAddPhoneNumbersWrapper.setVisibility(false);
      this.view.settings.flxEmailWrapper.setVisibility(false);
      this.view.settings.flxEditEmailWrapper.setVisibility(false);
      this.view.settings.flxEditUsernameWrapper.setVisibility(false);
      this.view.settings.flxAddNewEmailWrapper.setVisibility(false);
      this.view.settings.flxAddressesWrapper.setVisibility(false);
      this.view.settings.flxAddNewAddressWrapper.setVisibility(false);
      this.view.settings.flxEditAddressWrapper.setVisibility(false);
      this.view.settings.flxAccountsWrapper.setVisibility(false);
      this.view.settings.flxEditAccountsWrapper.setVisibility(false);
      this.view.settings.flxDefaultTransactionAccountWrapper.setVisibility(false);
      this.view.settings.flxUsernameAndPasswordWrapper.setVisibility(false);
      this.view.settings.flxAcknowledgementWrapper.setVisibility(false);
      this.view.settings.flxEditPasswordWrapper.setVisibility(false);
      this.view.settings.flxSecuritySettingVerificationWrapper.setVisibility(false);
      this.view.settings.flxUsernameVerificationWrapper.setVisibility(false);
      this.view.settings.flxUsernameVerificationWrapper2.setVisibility(false);
      this.view.settings.flxAnswerSecurityQuestionsWrapper.setVisibility(false);
      this.view.settings.flxSecureAccessCodeWrapper.setVisibility(false);
      this.view.settings.flxEditSecuritySettingsWrapper.setVisibility(false);
      this.view.settings.flxTransactionalAndPaymentsAlertsWrapper.setVisibility(false);
      this.view.settings.flxSecurityAlertsWrapper.setVisibility(false);
      this.view.settings.flxPromotionalAlertsWrapper.setVisibility(false);
      if(views){
        for (var i = 0; i < views.length; i++) {
          this.view.settings[views[i]].isVisible = true;
        }
      }  
      this.view.settings.forceLayout();
    },

    //all flex show methods
    showProfile: function () {
      this.showViews(["flxProfileWrapper"]);
    },
    showNameChangeRequest: function () {
      this.showViews(["flxNameChangeRequestWrapper"]);
    },
    showPhoneNumbers: function () {
      this.showViews(["flxPhoneNumbersWrapper"]);
    },
    // showEditPhonenumber: function () {
    //   this.showViews(["flxEditPhoneNumbersWrapper"]);
    // },
    showEmail: function () {
      this.showViews(["flxEmailWrapper"]);
    },
    /**
  * Method to reset fields in the UI of Email module
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    resetUpdateEmailForm: function () {
      this.view.settings.tbxEditEmailId.text = "";
      this.view.settings.imgEditMarkAsPrimaryEmailCheckBox.src = "unchecked_box.png";
      this.disableButton(this.view.settings.btnEditEmailIdSave);
      this.view.settings.flxErrorEditEmail.setVisibility(false);
    },
    /**
  * Method to reset fields in the UI of Email module and show the module
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showEditEmail: function () {
      this.resetUpdateEmailForm();
      this.showViews(["flxEditEmailWrapper"]);
    },
    /**
  * Method to Disable a button
  * @member of frmProfileManagementController
  * @param {String} button - ID of the button to be disabled
  * @returns {void} - None
  * @throws {void} -None
  */
    disableButton: function (button) {
      button.setEnabled(false);
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
    },
    /**
  * Method to Enable a button
  * @member of frmProfileManagementController
  * @param {String} button - ID of the button to be enabled
  * @returns {void} - None
  * @throws {void} -None
  */
    enableButton: function (button) {
      button.setEnabled(true);
      button.skin = "sknbtnLatoffffff15px";
      button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
    },
    /**
  * Method to Check Dots in Username Entered by the User
  * @member of frmProfileManagementController
  * @param {String} enteredUserName - username entered by the User
  * @returns {void} - None
  * @throws {void} -None
  */
    checkDot: function(enteredUserName) {
      var count = 0;
      if (enteredUserName.charAt(0) == "." || enteredUserName.charAt(enteredUserName.length - 1) == ".") return false;
      var index = enteredUserName.indexOf(".");
      for (var i = index + 1; i < enteredUserName.length - 1; i++) {
        if (enteredUserName[i] == ".") {
          return false;
        }
      }
      return true;
    },
    /**
  * Method to check whether the username is valid or not
  * @member of frmProfileManagementController
  * @param {String} enteredUserName - username entered by the User
  * @returns {void} - None
  * @throws {void} -None
  */
    isUserNameValid: function(enteredUserName) {
      var userName = /^[a-zA-Z0-9.]+$/;
      if ((enteredUserName.match(userName)) && (enteredUserName.length > 7) && (enteredUserName.length < 25) && (enteredUserName != kony.mvc.MDAApplication.getSharedInstance().appContext.username)) {
        if(this.checkDot(enteredUserName))
          return true;
      }
      return false;
    },
    /**
  * Method to Check whether the password is valid or not
  * @member of frmProfileManagementController
  * @param {String} enteredPassword - password entered by the User
  * @returns {void} - None
  * @throws {void} -None
  */
    isPasswordValid: function (enteredPassword) {
      var password = /^(?=.*\d)(?=.*[a-zA-Z])(?=.+[\!\#\$\%\(\*\)\+\,\-\;\=\?\[\\\]\^\_\'\{\}]).{8,20}$/;
      var userName = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      if (enteredPassword.match(password) && !this.hasConsecutiveDigits(enteredPassword) && enteredPassword != userName && this.view.settings.tbxExistingPassword.text.length > 0) {
        return true;
      }
      return false;
    },
    /**
  * Method to Check whether the entered text has consecutive digits or not
  * @member of frmProfileManagementController
  * @param {Int} input - field entered by the User
  * @returns {void} - None
  * @throws {void} -None
  */
    hasConsecutiveDigits: function (input) {
      var i;
      var count = 0;
      for (i = 0; i < input.length; i++) {
        // alert(abc[i]);
        if (input[i] >= 0 && input[i] <= 9)
          count++;
        else
          count = 0;
        if (count === 9)
          return true;
      }
      return false;
    },
    /**
  * Method to Check whether the password is valid and matches with the re entered password
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    isPasswordValidAndMatchedWithReEnteredValue: function () {
      if (this.view.settings.tbxNewPassword.text && this.view.settings.tbxConfirmPassword.text) {
        if (this.view.settings.tbxNewPassword.text === this.view.settings.tbxConfirmPassword.text) {
          return true;
        }
      }
      return false;
    },
    /**
  * Method to Check whether the username is valid or not and if valid then enable/disable the button
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    ValidateUsername: function () {
      if (this.isUserNameValid(this.view.settings.tbxUsername.text)) {
        this.enableButton(this.view.settings.btnEditUsernameProceed);
      } else {
        this.disableButton(this.view.settings.btnEditUsernameProceed);
      }
    },
    /**
  * Method to Check whether the password is valid and matches with the re entered password
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    ValidatePassword: function () {

      if ((this.isPasswordValid(this.view.settings.tbxNewPassword.text)) && (this.isPasswordValid(this.view.settings.tbxConfirmPassword.text))) {
        if (this.isPasswordValidAndMatchedWithReEnteredValue()) {
          this.enableButton(this.view.settings.btnEditPasswordProceed);
        } else {
          this.disableButton(this.view.settings.btnEditPasswordProceed);
        }
      } else {

        this.disableButton(this.view.settings.btnEditPasswordProceed);
      }
      this.view.forceLayout();
    },

    showEditUsername : function(){
      this.showViews(["flxEditUsernameWrapper"]);
    },
    showAddNewEmail : function(){
      this.resetAddEmailForm();
      this.showViews(["flxAddNewEmailWrapper"]);
    },
    showAddresses : function(){
      this.showViews(["flxAddressesWrapper"]);
    },
    showAddNewAddress : function(){
      this.showViews(["flxAddNewAddressWrapper"]);
    },
    showEditAddress: function(){
      this.showViews(["flxEditAddressWrapper"]);
    },
    showAccounts : function(){
      this.showViews(["flxAccountsWrapper"]);
    },
    showEditAccounts : function(){
      this.showViews(["flxEditAccountsWrapper"]);
    },
    /**
  * Method to show UI according to the default account
  * @member of frmProfileManagementController
  * @param {JSON} viewModel - JSON of Default Accounts
  * @returns {void} - None
  * @throws {void} -None
  */
    showDefaultUserAccount: function(viewModel) {
      this.showViews(["flxDefaultTransactionAccountWrapper"]);
      if (viewModel.errorCase == true) {
        this.view.settings.flxDefaultTransactionButtons.setVisibility(false);
        this.view.settings.flxDefaultTransctionAccounts.setVisibility(false);
        this.view.settings.flxDefaultTransactionAccountWrapper.setVisibility(true);
        this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
        this.view.settings.txtDefaultTransactionAccountWarning.text = "Network error!";
        this.view.settings.flxDefaultAccountsSelected.setVisibility(false);
      } else {
        this.view.settings.flxDefaultTransactionButtons.setVisibility(true);
        this.view.settings.flxDefaultTransctionAccounts.setVisibility(true);
        this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
        this.view.settings.txtDefaultTransactionAccountWarning.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.YouhaveSelectedTheFollowingAccounts");
        this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
        this.showDefaultTransctionAccount(viewModel);
        this.setSelectedSkin("SetDefaultAccount");
      }
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to show UI according to the default account
  * @member of frmProfileManagementController
  * @param {JSON} viewModel - JSON of Default Transaction
  * @returns {void} - None
  * @throws {void} -None
  */
    showDefaultTransctionAccount : function(viewModel){
      this.showViews(["flxDefaultTransactionAccountWrapper"]);
      var count=0;
      for(var keys in viewModel){
        if(viewModel[keys]!='None')
          count++;
      }
      if(count!==0){
        this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
        this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
      }else{
        this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
        this.view.settings.flxDefaultAccountsSelected.setVisibility(false);
      }
      this.view.settings.lblTransfersValue.text=viewModel.defaultTransferAccount;
      this.view.settings.lblBillPayValue.text=viewModel.defaultBillPayAccount;
      this.view.settings.lblPayAPersonValue.text=viewModel.defaultP2PAccount;
      this.view.settings.lblCheckDepositValue.text=viewModel.defaultCheckDepositAccount;
    },
    /**
  * Method to show the list of accounts
  * @member of frmProfileManagementController
  * @param {JSON} viewModel - JSON of Accounts
  * @returns {void} - None
  * @throws {void} -None
  */
    showAccountsList: function(viewModel) {
      if (viewModel == "errorCase") {
        this.view.settings.flxDefaultAccountsSelected.setVisibility(false);
        this.view.settings.flxDefaultTransctionAccounts.setVisibility(false);
        this.view.settings.flxDefaultTransactionButtons.setVisibility(false);
        this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(true);
        //this.view.settings.txtDefaultTransactionAccountWarning.setVisibility(true);
        this.view.settings.txtDefaultTransactionAccountWarning.text = "Network error!";
      } else {
        this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
        this.view.settings.flxDefaultTransctionAccounts.setVisibility(true);
        this.view.settings.flxDefaultTransactionButtons.setVisibility(true);
        this.view.settings.txtDefaultTransactionAccountWarning.setVisibility(false);
        this.view.settings.lbxTransfers.setVisibility(true);
        this.view.settings.lbxBillPay.setVisibility(true);
        this.view.settings.lbxPayAPreson.setVisibility(true);
        this.view.settings.lbxCheckDeposit.setVisibility(true);
        this.view.settings.lbxTransfers.masterData = viewModel.TransfersAccounts;
        this.view.settings.lbxTransfers.selectedKey = viewModel.defaultTransfersAccounts;
        this.view.settings.lbxBillPay.masterData = viewModel.BillPayAccounts;
        this.view.settings.lbxBillPay.selectedKey = viewModel.defaultBillPayAccounts;
        this.view.settings.lbxPayAPreson.masterData = viewModel.P2PAccounts;
        this.view.settings.lbxPayAPreson.selectedKey = viewModel.defaultP2PAccounts;
        this.view.settings.lbxCheckDeposit.masterData = viewModel.CheckDepositAccounts;
        this.view.settings.lbxCheckDeposit.selectedKey = viewModel.defaultCheckDepositAccounts;     
      }
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    }, 
    /**
  * Method to show UI according to the Prefered account
  * @member of frmProfileManagementController
  * @param {JSON} viewModel - JSON of Prefered Accounts
  * @returns {void} - None
  * @throws {void} -None
  */
    showPreferredAccountsList:function(viewModel){
      this.showAccounts();
      this.collapseAll();
      this.expandWithoutAnimation(this.view.settings.flxAccountSettingsSubMenu);
      if(viewModel.errorCase===true){

        CommonUtilities.hideProgressBar(this.view);
      }else{

        this.setAccountsSegmentData(viewModel);
        this.setSelectedSkin("AccountPreferences");
      }
    },
    showUsernameAndPassword : function(){
      this.showViews(["flxUsernameAndPasswordWrapper"]);
    },
    showAcknowledgement : function(){
      this.showViews(["flxAcknowledgementWrapper"]);
    },
    /**
  * Method to show UI to edit password
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showEditPassword : function(){
      this.view.settings.tbxExistingPassword.text="";
      this.view.settings.tbxNewPassword.text="";
      this.view.settings.tbxConfirmPassword.text="";
      this.disableButton(this.view.settings.btnEditPasswordProceed);
      this.showViews(["flxEditPasswordWrapper"]);
    },
    /**
  * Method to verify OTP by hitting backend
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    verifyOtp: function() {
      if (this.view.settings.tbxEnterOTP.text != "" && this.view.settings.tbxEnterOTP.text != null) {
        CommonUtilities.showProgressBar(this.view);
        this.presenter.verifyOtp(this.view.settings.tbxEnterOTP.text);
        //CommonUtilities.hideProgressBar(this.view);
      }
    },
    showAddNewEmail: function () {
      this.showViews(["flxAddNewEmailWrapper"]);
    },
    showAddresses: function () {
      this.showViews(["flxAddressesWrapper"]);
    },
    showAddNewAddress: function () {
      this.showViews(["flxAddNewAddressWrapper"]);
    },
    showEditAddress: function () {
      this.showViews(["flxEditAddressWrapper"]);
    },
    showAccounts: function () {
      this.showViews(["flxAccountsWrapper"]);
    },
    showEditAccounts: function () {
      this.showViews(["flxEditAccountsWrapper"]);
    },
    showUsernameAndPassword: function () {
      this.view.settings.lblUsernameValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
      this.showViews(["flxUsernameAndPasswordWrapper"]);
    },
    showAcknowledgement: function () {
      this.showViews(["flxAcknowledgementWrapper"]);
    },
    showEditPassword: function () {
      this.view.settings.flxErrorEditPassword.setVisibility(false);
      this.showViews(["flxEditPasswordWrapper"]);
    },
    /**
  * Method to Update the next step according to the the flag UPDATE
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    updateRequirements: function () {
      this.view.settings.flxErrorSecuritySettingsVerification.setVisibility(false);
      this.view.settings.flxErrorEditSecurityQuestions.setVisibility(false);
      var self = this;
      CommonUtilities.showProgressBar(self.view);
      if(update === "password")
        this.presenter.updatePassword(self.view.settings.tbxNewPassword.text);
      else if(update === "username")
        this.presenter.updateUsername(self.view.settings.tbxUsername.text);
      else
        this.presenter.updateSecurityQuestions(self.onSaveSecurityQuestions());
      //CommonUtilities.hideProgressBar(self.view);

    },
    /**
  * Method to Update the acknowledgment according to the the flag UPDATE
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    acknowledge: function (viewModel) {
      var scopeObj = this;
      CommonUtilities.showProgressBar(scopeObj.view);
      if (update === "password"){
        this.view.settings.btnAcknowledgementDone.onClick = function(){
          scopeObj.showUsernameAndPassword();
          scopeObj.setSelectedSkin("UsernameAndPassword");
        }
        scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.passwordUpdatedSuccessfully");
      } 
      else if (update === "username") {
        this.view.settings.btnAcknowledgementDone.onClick = function(){
          scopeObj.showUsernameAndPassword();
          scopeObj.setSelectedSkin("UsernameAndPassword");
        }
        kony.mvc.MDAApplication.getSharedInstance().appContext.username = this.view.settings.tbxUsername.text;
        scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.usernameUpdatedSuccess");
      } else {
        this.view.settings.btnAcknowledgementDone.onClick = function(){
          scopeObj.presenter.checkSecurityQuestions();
          scopeObj.setSelectedSkin("SecurityQuestions");
        }
        scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SecurityQuesUpdatedSuccess");
      }
      scopeObj.showAcknowledgement();
      update = "";
      CommonUtilities.hideProgressBar(scopeObj.view);
    },
    showOTP: function(){
      this.view.settings.tbxEnterOTP.secureTextEntry = false;
    },
    hideOTP: function(){
      this.view.settings.tbxEnterOTP.secureTextEntry = true;
    },
    /**
  * Method to show secure access options
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showSecureAccessOptions: function (viewModel){
      var scopeObj = this;
      CommonUtilities.showProgressBar(scopeObj.view);
      if(viewModel.data[0].isPhoneEnabled === "true")
        scopeObj.view.settings.imgPhoneUnchecked.src="checked_box.png";
      else
        scopeObj.view.settings.imgPhoneUnchecked.src="unchecked_box.png";
      if(viewModel.data[0].isEmailEnabled === "true")
        scopeObj.view.settings.imgemailunchecked.src="checked_box.png";
      else
        scopeObj.view.settings.imgemailunchecked.src="unchecked_box.png";
      scopeObj.view.settings.flxPhoneUnchecked.onClick = function(){};
      scopeObj.view.settings.flxemailunchecked.onClick = function(){};
      scopeObj.view.settings.flxPhoneUnchecked.setEnabled = false;
      scopeObj.view.settings.flxemailunchecked.setEnabled = false;
      scopeObj.view.settings.imgPhoneUnchecked.setEnabled = false;
      scopeObj.view.settings.imgemailunchecked.setEnabled = false;
      scopeObj.showSecureAccessCode();
      scopeObj.setSelectedSkin("SecureAccessCode");
      scopeObj.view.forceLayout();
      CommonUtilities.hideProgressBar(scopeObj.view);
    },
    /**
  * Method to handle the Enter OTP flow
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    enterOtp: function() {
      var scopeObj = this;
      CommonUtilities.showProgressBar(scopeObj.view);
      if (scopeObj.view.settings.btnSecuritySettingVerificationSend.text === "VERIFY") {
        scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SecurityQuesUpdatedSuccess");
        scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SendingYouTheOtp");
        scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.common.sendingOTP");
        scopeObj.view.settings.flxOTPtextbox.setVisibility("false");
        //scopeObj.enableButton(scopeObj.view.settings.btnUsernameVerificationSend);
        scopeObj.view.settings.btnSecuritySettingVerificationCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
        scopeObj.view.settings.btnSecuritySettingVerificationSend.text = kony.i18n.getLocalizedString("i18n.common.send");
        scopeObj.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
          scopeObj.presenter.requestOtp();
        }
        scopeObj.verifyOtp();
      } else {
        scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function() {
          scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SendingYouTheOtp");
          scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.common.sendingOTP");
          scopeObj.view.settings.flxOTPtextbox.setVisibility("false");
          scopeObj.view.settings.btnSecuritySettingVerificationCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
          scopeObj.view.settings.btnSecuritySettingVerificationSend.text = kony.i18n.getLocalizedString("i18n.common.send");
          scopeObj.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
            scopeObj.presenter.requestOtp();
          }
          scopeObj.enableButton(scopeObj.view.settings.btnSecuritySettingVerificationSend);
          scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function() {
            if (update == "username" || update == "password") {
              scopeObj.showUsernameAndPassword();
              scopeObj.setSelectedSkin("UsernameAndPassword");
              scopeObj.view.forceLayout();
            } else {
              scopeObj.presenter.checkSecurityQuestions();
              scopeObj.setSelectedSkin("SecurityQuestions");
              scopeObj.view.forceLayout();
            }
          }
          scopeObj.showSecuritySettingVerification();

        };
        scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.Enrollnow.EnterOTPMsg");
        scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.IfYouHavenotRecievedOTP");
        scopeObj.view.settings.flxOTPtextbox.setVisibility(true);
        scopeObj.view.settings.tbxEnterOTP.text = "";
        scopeObj.view.settings.btnSecuritySettingVerificationCancel.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Re-sendOTP");
        scopeObj.view.settings.btnSecuritySettingVerificationSend.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
        scopeObj.view.settings.btnSecuritySettingVerificationSend.onClick = function() {
          scopeObj.enterOtp();
        }
        scopeObj.disableButton(scopeObj.view.settings.btnSecuritySettingVerificationSend);
        CommonUtilities.hideProgressBar(scopeObj.view);
      }
      scopeObj.view.forceLayout();
      //CommonUtilities.hideProgressBar(scopeObj.view);
    },
    /**
  * Method to check the validation of username
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    checkUsername: function(){
      var self = this ;
      if(this.view.settings.tbxUsername.text.length >=  7 && this.isUserNameValid(this.view.settings.tbxUsername.text) && (this.view.settings.tbxUsername.text != kony.mvc.MDAApplication.getSharedInstance().appContext.username)) 
        this.enableButton(self.view.settings.btnEditUsernameProceed);
      else
        this.disableButton(self.view.settings.btnEditUsernameProceed);
    },
    /**
  * Method to check the validation of OTP
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    checkOTP: function() {
      if (this.view.settings.tbxEnterOTP.text.length >= 6) this.enableButton(this.view.settings.btnSecuritySettingVerificationSend);
      //scopeObj.enableButton(scopeObj.view.settings.btnSecuritySettingVerificationSend);
    },
    showSecuritySettingVerification : function(){
      this.showViews(["flxSecuritySettingVerificationWrapper"]);
    },
    showUsernameVerification: function(){
      this.showViews(["flxUsernameVerificationWrapper2"]);
    },
    showAnswerSecurityQuestions: function(){
      this.showViews(["flxAnswerSecurityQuestionsWrapper"]);
    },

    showSecureAccessCode: function(){
      this.showViews(["flxSecureAccessCodeWrapper"]);        
      //       this.view.settings.flxRight.height = "1000dp";
      //       this.view.settings.height = "1020dp";
      //       this.view.flxSettingsWrapper.height = "1090dp";
      flag = 1;
    },
    //showPhoneRadiobtn: function(imgRadio1,imgRadio2){
    //  if(imgRadio1.src === "icon_radiobtn_active.png" ){
    //    imgRadio1.src = "icon_radiobtn.png";
    //    imgRadio2.src = "icon_radiobtn_active.png";
    //  }
    //  else{
    //    imgRadio1.src = "icon_radiobtn_active.png";
    //    imgRadio2.src = "icon_radiobtn.png";
    //  }
    //},
    /**
  * Method to fetch 2 security question and show them
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    getAndShowAnswerSecurityQuestions: function(){
      CommonUtilities.showProgressBar(this.view);
      this.view.settings.tbxAnswers1.text = "";
      this.view.settings.tbxAnswers2.text = "";
      this.presenter.getAnswerSecurityQuestions();
      CommonUtilities.hideProgressBar(this.view);
    },

    showSecuritySettingVerification: function () {
      this.showViews(["flxSecuritySettingVerificationWrapper"]);
    },
    showUsernameVerification: function () {
      this.showViews(["flxUsernameVerificationWrapper2"]);
    },
    showAnswerSecurityQuestions: function () {
      this.showViews(["flxAnswerSecurityQuestionsWrapper"]);
    },
    showSecureAccessCode: function () {
      this.showViews(["flxSecureAccessCodeWrapper"]);
      //       this.view.settings.flxRight.height = "1000dp";
      //       this.view.settings.height = "1020dp";
      //       this.view.flxSettingsWrapper.height = "1090dp";
      flag = 1;
    },
    //showPhoneRadiobtn: function (imgRadio1, imgRadio2) {
    //  if(imgRadio1.src === "icon_radiobtn_active.png") {
    //    imgRadio1.src = "icon_radiobtn.png";
    //    imgRadio2.src = "icon_radiobtn_active.png";
    //  } else {
    //    imgRadio1.src = "icon_radiobtn_active.png";
    //    imgRadio2.src = "icon_radiobtn.png";
    //  }
    //},
    /**
  * Method to validate the Email
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {Boolean} - wheter the email is valid or not
  * @throws {void} -None
  */
    isValidEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    /**
  * Method to enable/disable button based on the email entered
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    checkAddEmailForm: function () {
      if (this.isValidEmail(this.view.settings.tbxEmailId.text)) {
        this.enableButton(this.view.settings.btnAddEmailIdAdd);
      }
      else {
        this.disableButton(this.view.settings.btnAddEmailIdAdd);

      }
    },
    /**
  * Method to enable/disable button based on the email entered after editing
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    checkUpdateEmailForm: function () {
      if (this.isValidEmail(this.view.settings.tbxEditEmailId.text)) {
        this.enableButton(this.view.settings.btnEditEmailIdSave);
      }
      else{
        this.disableButton(this.view.settings.btnEditEmailIdSave);      
      }
    },
    /**
  * Method to set the validation function while entering email
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setAddEmailValidationActions: function () {
      var scopeObj = this;
      this.disableButton(this.view.settings.btnAddEmailIdAdd);
      this.view.settings.tbxEmailId.onKeyUp = function () {
        scopeObj.checkAddEmailForm();
      }
    },
    /**
  * Method to set the validation function while editing email
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setUpdateEmailValidationActions: function () {
      var scopeObj = this;
      this.disableButton(this.view.settings.btnEditEmailIdSave);      
      this.view.settings.tbxEditEmailId.onKeyUp = function () {
        scopeObj.checkUpdateEmailForm();        
      }
    },
    /**
  * Method to validate the address entered
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    checkNewAddressForm: function () {
      var addAddressFormData = this.getNewAddressFormData();
      if (addAddressFormData.addressLine1 === '') {
        this.disableButton(this.view.settings.btnAddNewAddressAdd);
      }
      else if (addAddressFormData.zipcode === '') {
        this.disableButton(this.view.settings.btnAddNewAddressAdd);        
      }
      else if (addAddressFormData.city === '') {
        this.disableButton(this.view.settings.btnAddNewAddressAdd);        
      }
      else {
        this.enableButton(this.view.settings.btnAddNewAddressAdd);
      }
    },
    /**
  * Method to validate the edited address
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    checkUpdateAddressForm: function () {
      var addAddressFormData = this.getUpdateAddressData();
      if (addAddressFormData.addressLine1 === '') {
        this.disableButton(this.view.settings.btnEditAddressSave);
      }
      else if (addAddressFormData.zipcode === '') {
        this.disableButton(this.view.settings.btnEditAddressSave);        
      }
      else if (addAddressFormData.city === '') {
        this.disableButton(this.view.settings.btnEditAddressSave);        
      }
      else {
        this.enableButton(this.view.settings.btnEditAddressSave);
      }
    },
    /**
  * Method to enable/disable button based on the new phone number entered
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    checkAddNewPhoneForm: function () {
      var formdata = this.getNewPhoneFormData();
      if (formdata.phoneNumber === "") {
        this.disableButton(this.view.settings.btnAddPhoneNumberSave);
      }
      else {
        this.enableButton(this.view.settings.btnAddPhoneNumberSave);
      }
    },
    /**
  * Method to enable/disable button based on the edited phone number entered
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    checkUpdateEditPhoneForm: function () {
      var formdata = this.getEditPhoneFormData();
      if (formdata.phoneNumber === "") {
        this.disableButton(this.view.settings.btnEditPhoneNumberSave);
      }
      else {
        this.enableButton(this.view.settings.btnEditPhoneNumberSave);
      }
    },
    /**
  * Method to assign validation action on the address fields
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setNewAddressValidationActions: function () {
      this.disableButton(this.view.settings.btnAddNewAddressAdd);
      this.view.settings.tbxAddressLine1.onKeyUp = this.checkNewAddressForm.bind(this);
      this.view.settings.tbxAddressLine2.onKeyUp = this.checkNewAddressForm.bind(this);
      this.view.settings.tbxZipcode.onKeyUp = this.checkNewAddressForm.bind(this);   
      this.view.settings.tbxCity.onKeyUp = this.checkNewAddressForm.bind(this);   
    },
    /**
  * Method to assign validation action on the edit address fields
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setUpdateAddressValidationActions: function () {
      this.disableButton(this.view.settings.btnEditAddressSave);
      this.view.settings.tbxEditAddressLine1.onKeyUp = this.checkUpdateAddressForm.bind(this);
      this.view.settings.tbxEditAddressLine2.onKeyUp = this.checkUpdateAddressForm.bind(this);
      this.view.settings.tbxEditZipcode.onKeyUp = this.checkUpdateAddressForm.bind(this); 
      this.view.settings.tbxEditCity.onKeyUp = this.checkUpdateAddressForm.bind(this);
    },
    /**
  * Method to assign validation action on the edit address fields
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    getBase64: function (file, successCallback) {
      var reader = new FileReader();
      reader.onloadend = function() {
        successCallback(reader.result);
      }
      reader.readAsDataURL(file);
    },
    /**
  * Method to assign validation action on the new phone entered fields
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setAddNewPhoneValidationActions: function () {
      this.view.settings.tbxAddPhoneNumber.onKeyUp = this.checkAddNewPhoneForm.bind(this);
    },
    /**
  * Method to assign validation action while editing phone number  fields
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setEditNewPhoneValidationActions: function () {
      this.view.settings.tbxPhoneNumber.onKeyUp = this.checkAddNewPhoneForm.bind(this);
    },
    /**
  * Method to assign validation action on the edit address fields
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    selectedFileCallback: function (events, files) {
      var scopeObj = this;
      this.getBase64(files[0].file, function (base64String) {
        scopeObj.presenter.userProfile.userImageUpdate(base64String);
      })
    },
    /**
  * Method to assign the general flow action throughout the profile  module
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setFlowActions: function () {
      var scopeObj = this;
      CommonUtilities.showProgressBar(scopeObj.view);
      this.setAddEmailValidationActions();
      this.setUpdateEmailValidationActions();
      this.setNewAddressValidationActions();
      this.setUpdateAddressValidationActions();
      this.setAddNewPhoneValidationActions();
      this.setEditNewPhoneValidationActions();
      this.view.settings.btnAddPhoto.onClick = function () {
        var config = {
          selectMultipleFiles: true,        
          filter: ["image/png", "image/jpeg"]
        };
        kony.io.FileSystem.browse(config, this.selectedFileCallback.bind(this));      
      }.bind(this);
      //Menu flow
      this.view.settings.flxProfile.onClick = function () {
        scopeObj.presenter.userProfile.showUserProfile();
      };
      this.view.settings.flxPhone.onClick = function () {
        scopeObj.presenter.userProfile.showUserPhones();
      };
      this.view.settings.flxEmail.onClick = function () {
        scopeObj.presenter.userProfile.showUserEmail();
      };
      this.view.settings.flxAddress.onClick = function () {
        scopeObj.presenter.userProfile.showUserAddresses();
      };
      this.view.settings.flxUsernameAndPassword.onClick = function () {

        scopeObj.showUsernameAndPassword();
        scopeObj.setSelectedSkin("UsernameAndPassword");
      };
      this.view.settings.flxSecurityQuestions.onClick = function () {
        CommonUtilities.showProgressBar(this.view);
        scopeObj.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
        scopeObj.presenter.checkSecurityQuestions();
        scopeObj.setSelectedSkin("SecurityQuestions");
        scopeObj.view.settings.btnEditSecuritySettingsCancel.onClick = function(){
          scopeObj.view.settings.flxErrorEditSecuritySettings.setVisibility(false);
          scopeObj.presenter.checkSecurityQuestions();
          scopeObj.setSelectedSkin("SecurityQuestions");
          scopeObj.selectedQuestions = {
            ques: ["None", "None", "None", "None", "None"],
            key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
          };
          scopeObj.selectedQuestionsTemp= {
            securityQuestions: [],
            flagToManipulate: []
          };
        };
        scopeObj.view.settings.tbxEnterOTP.onKeyUp = function(){
          scopeObj.checkOTP();
        }
        scopeObj.view.settings.imgViewOTP.onTouchStart = function(){
          scopeObj.showOTP();
        };
        scopeObj.view.settings.imgViewOTP.onTouchEnd = function(){
          scopeObj.hideOTP();
        };
      };
      this.view.settings.flxSecureAccessCode.onClick = function () {
        scopeObj.view.settings.btnSecureAccessCodeModify.text = "MODIFY";
        scopeObj.view.settings.btnSecureAccessCodeModify.setVisibility(true);
        scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);

        scopeObj.view.settings.lblViewEmailSettings.onTouchEnd = function(){
          scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
          scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
          scopeObj.view.settings.imgSecuritySettingsCollapse.src = "arrow_down.png";
          scopeObj.view.settings.imgProfileSettingsCollapse.src = "arrow_up.png";
          scopeObj.presenter.userProfile.showUserEmail();
          scopeObj.view.forceLayout();
        }
        scopeObj.view.settings.lblViewPhoneSettings.onTouchEnd = function(){
          scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
          scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
          scopeObj.view.settings.imgSecuritySettingsCollapse.src = "arrow_down.png";
          scopeObj.view.settings.imgProfileSettingsCollapse.src = "arrow_up.png";
          scopeObj.presenter.userProfile.showUserPhones();
          scopeObj.view.forceLayout();
        }
        scopeObj.view.settings.btnSecureAccessCodeCancel.onClick = function(){
          scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Modify");
          scopeObj.view.settings.btnSecureAccessCodeModify.setVisibility(true);
          scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
          scopeObj.view.settings.lblViewEmailSettings.onClick = function() {
            scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
            scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
            scopeObj.view.settings.imgSecuritySettingsCollapse.src = "arrow_down.png";
            scopeObj.view.settings.imgProfileSettingsCollapse.src = "arrow_up.png";
            scopeObj.presenter.userProfile.showUserEmail();
            scopeObj.view.forceLayout();
          }
          scopeObj.view.settings.lblViewPhoneSettings.onClick = function() {
            scopeObj.expand(scopeObj.view.settings.flxProfileSettingsSubMenu);
            scopeObj.collapseWithoutAnimation(scopeObj.view.settings.flxSecuritySettingsSubMenu);
            scopeObj.view.settings.imgSecuritySettingsCollapse.src = "arrow_down.png";
            scopeObj.view.settings.imgProfileSettingsCollapse.src = "arrow_up.png";
            scopeObj.presenter.userProfile.showUserPhones();
            scopeObj.view.forceLayout();
          }
          scopeObj.presenter.checkSecureAccess();
          scopeObj.view.forceLayout();
        }
        scopeObj.presenter.checkSecureAccess();
        scopeObj.view.forceLayout();
        CommonUtilities.hideProgressBar(scopeObj.view);

      };
      this.view.settings.flxAccountPreferences.onClick = function () {
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.showPreferredAccounts();
      };
      this.view.settings.flxSetDefaultAccount.onClick = function () {
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.showDefaultScreen();
        scopeObj.presenter.getDefaultUserProfile();
        //scopeObj.showDefaultTransctionAccount();
        //scopeObj.setSelectedSkin("SetDefaultAccount");
      };

      //Profile Flow
      this.view.settings.btnNameChangeRequest.onClick = function () {
        scopeObj.showNameChangeRequest();
      };
      //this.view.settings.btnNameChangeRequestBack.onClick = function () {
      //  scopeObj.showProfile();
      //};

      //Phone Flow
      this.view.settings.btnAddNewNumber.onClick = function () {
        scopeObj.presenter.userProfile.getAddPhoneNumberViewModel();
      };
      this.view.settings.btnAddPhoneNumberCancel.onClick = function () {
        scopeObj.showPhoneNumbers();
      };
      this.view.settings.btnAddPhoneNumberSave.onClick = function () {
        //Add code for Saving the Phone Number
        scopeObj.showPhoneNumbers();
      };
      this.view.settings.btnEditPhoneNumberCancel.onClick = function () {
        scopeObj.showPhoneNumbers();
      };
      this.view.settings.btnEditPhoneNumberSave.onClick = function () {
        //Add code for saving the edited Phone Number
        scopeObj.showPhoneNumbers();
      };
      this.view.flxDeleteClose.onClick = function () {
        scopeObj.view.flxDelete.setFocus(true);
        scopeObj.view.flxDeletePopUp.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.btnDeleteNo.onClick = function () {
        scopeObj.view.flxDelete.setFocus(true);
        scopeObj.view.flxDeletePopUp.setVisibility(false);
        scopeObj.view.forceLayout();
      };

      this.view.settings.flxCheckBox3.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgCheckBox3);
      };
      this.view.settings.flxCheckBox4.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgCheckBox4);
      };

      this.view.settings.flxAddCheckBox3.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgAddCheckBox3);
      };
      this.view.settings.flxAddCheckBox4.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgAddCheckBox4);
      };
      this.view.settings.flxRadioBtnInternational.onClick = function () {
        scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgRadioBtnInternational, scopeObj.view.settings.imgRadioBtnUS);
      };
      this.view.settings.flxRadioBtnUS.onClick = function () {
        scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgRadioBtnUS, scopeObj.view.settings.imgRadioBtnInternational);
      };
      this.view.settings.flxAddRadioBtnInternational.onClick = function () {
        scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgAddRadioBtnInternational, scopeObj.view.settings.imgAddRadioBtnUS);
      };
      this.view.settings.flxAddRadioBtnUS.onClick = function () {
        scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgAddRadioBtnUS, scopeObj.view.settings.imgAddRadioBtnInternational);
      };

      //Email flow
      this.view.settings.btnAddNewEmail.onClick = function () {
        scopeObj.resetAddEmailForm();
        scopeObj.showAddNewEmail();
      };
      this.view.settings.btnAddEmailIdCancel.onClick = function () {
        scopeObj.showEmail();
      };
      this.view.settings.btnAddEmailIdAdd.onClick = function () {
        //add code to ADD new email
        scopeObj.presenter.userProfile.saveEmail(scopeObj.getNewEmailData());
        // scopeObj.showEmail();
      };

      this.view.settings.btnEditAddNewEmail.onClick = function () {
        scopeObj.showAddNewEmail();
      };
      this.view.settings.btnEditEmailIdCancel.onClick = function () {
        scopeObj.showEmail();
      };

      this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgEditMarkAsPrimaryEmailCheckBox);
      };
      this.view.settings.flxMarkAsPrimaryEmailCheckBox.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgMarkAsPrimaryEmailCheckBox);
      };

      //Address flow
      this.view.settings.btnAddNewAddress.onClick = function () {
        scopeObj.presenter.userProfile.getAddNewAddressViewModel();    
      };

      this.view.settings.btnAddNewAddressCancel.onClick = function () {
        scopeObj.showAddresses();
      };
      this.view.settings.btnAddNewAddressAdd.onClick = function () {
        //write code to ADD new Address
        scopeObj.showAddresses();
      };
      this.view.settings.btnEditAddressCancel.onClick = function () {
        scopeObj.showAddresses();
      };
      this.view.settings.btnEditAddressSave.onClick = function () {
        //Add code to save new address
        scopeObj.showAddresses();
      };
      this.view.settings.flxSetAsPreferredCheckBox.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgSetAsPreferredCheckBox);
      };
      this.view.settings.flxEditSetAsPreferredCheckBox.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgEditSetAsPreferredCheckBox);
      };


      //Username & Password
      this.view.settings.btnUsernameEdit.onClick= function(){
        scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
        scopeObj.view.settings.tbxUsername.onKeyUp = function(){
          scopeObj.ValidateUsername();
        }
        scopeObj.view.settings.tbxUsername.text = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
        scopeObj.view.settings.placeholder = "";
        scopeObj.ValidateUsername();
        scopeObj.view.settings.btnEditUsernameProceed.onClick = function() {
          scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
          scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function(){
            scopeObj.showUsernameAndPassword();
            scopeObj.setSelectedSkin("UsernameAndPassword");
            scopeObj.view.forceLayout();
          }
          scopeObj.view.settings.btnUsernameVerificationCancel.onClick = function(){
            scopeObj.showUsernameAndPassword();
            scopeObj.setSelectedSkin("UsernameAndPassword");
            scopeObj.view.forceLayout();
          }
          update = "username";
          scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SendingYouTheOtp");
          scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.common.sendingOTP");
          scopeObj.view.settings.flxOTPtextbox.setVisibility(false);
          //scopeObj.enableButton(scopeObj.view.settings.btnUsernameVerificationSend);
          scopeObj.showUsernameVerificationByChoice();
        };
        // scopeObj.disableButton(scopeObj.view.settings.btnEditUsernameProceed);
        scopeObj.showEditUsername();
      };
      this.view.settings.btnPasswordEdit.onClick= function(){
        scopeObj.view.settings.tbxExistingPassword.onKeyUp = function(){
          scopeObj.ValidatePassword();
        }
        scopeObj.view.settings.tbxNewPassword.onKeyUp = function(){
          scopeObj.ValidatePassword();
        }
        scopeObj.view.settings.tbxConfirmPassword.onKeyUp = function(){
          scopeObj.ValidatePassword();
        }
        scopeObj.view.settings.tbxExistingPassword.text = "";
        scopeObj.view.settings.tbxNewPassword.text = "";
        scopeObj.view.settings.tbxConfirmPassword.text = "";
        scopeObj.disableButton(scopeObj.view.settings.btnEditPasswordProceed);
        scopeObj.showEditPassword();
      };  
      this.view.settings.btnEditUsernameCancel.onClick= function(){
        scopeObj.showUsernameAndPassword();
      };
      this.view.settings.btnEditPasswordCancel.onClick= function(){
        scopeObj.showUsernameAndPassword();
      };
      this.view.settings.btnEditPasswordProceed.onClick= function(){
        if(scopeObj.view.settings.tbxExistingPassword.text === scopeObj.view.settings.tbxNewPassword.text){
          scopeObj.view.settings.lblError1.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.passUsernameNotSame");
          scopeObj.view.settings.flxErrorEditPassword.setVisibility(true);
        }
        else {
          scopeObj.view.settings.flxErrorEditPassword.setVisibility(false);
          scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function(){
            scopeObj.showUsernameAndPassword();
            scopeObj.setSelectedSkin("UsernameAndPassword");
            scopeObj.view.forceLayout();
          }
          scopeObj.view.settings.btnUsernameVerificationCancel.onClick = function(){
            scopeObj.showUsernameAndPassword();
            scopeObj.setSelectedSkin("UsernameAndPassword");
            scopeObj.view.forceLayout();
          }
          update = "password";
          CommonUtilities.showProgressBar(scopeObj.view);
          scopeObj.presenter.checkExistingPassword(scopeObj.view.settings.tbxExistingPassword.text);  
        }
      };
      this.view.settings.btnUsernameVerification2Proceed.onClick= function(){
        if(scopeObj.view.settings.btnUsernameVerification2Proceed.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")){
          scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.usernameUpdatedSuccess"); 
          scopeObj.showAcknowledgement();
        }
        else{
          scopeObj.view.settings.btnUsernameVerification2Cancel.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Re-sendOTP");
          scopeObj.view.settings.btnUsernameVerification2Proceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
          scopeObj.disableButton(scopeObj.view.settings.btnUsernameVerificationSend);
          scopeObj.view.settings.tbxEnterOTP.text = "";
          scopeObj.view.settings.lblUsernameVerification2SendingYouTheOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.EnterOTPsentOnYourMobilePhone");
          scopeObj.view.settings.lblUsernameVerificationtext2.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.IfYouHavenotRecievedOTP");
          scopeObj.view.settings.flxOTPtextbox.setVisibility(true);
        }
      };
      this.view.settings.btnUsernameVerificationCancel.onClick= function(){
        scopeObj.view.settings.tbxExistingPassword.text = "";
        scopeObj.view.settings.tbxNewPassword.text = "";
        scopeObj.view.settings.tbxConfirmPassword.text = "";
        scopeObj.view.settings.tbxUsername = "";
        scopeObj.disableButton(scopeObj.view.settings.btnEditPasswordProceed);
        scopeObj.presenter.showProfileSettings();
      };
      this.view.settings.btnUsernameVerificationProceed.onClick= function(){
        scopeObj.view.settings.imgViewOTP.onTouchStart = function(){
          scopeObj.showOTP();
        };
        scopeObj.view.settings.imgViewOTP.onTouchEnd = function(){
          scopeObj.hideOTP();
        };
        scopeObj.view.settings.flxErrorSecuritySettingsVerification.setVisibility(false);
        scopeObj.view.settings.flxErrorEditSecurityQuestions.setVisibility(false);
        if(scopeObj.view.settings.imgUsernameVerificationcheckedRadio.src == "icon_radiobtn_active.png"){
          scopeObj.view.settings.tbxEnterOTP.onKeyUp = function(){
            scopeObj.checkOTP();
          }
          scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SendingYouTheOtp");
          scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.common.sendingOTP");
          scopeObj.view.settings.flxOTPtextbox.setVisibility(false); 
          scopeObj.view.settings.btnSecuritySettingVerificationCancel.text =kony.i18n.getLocalizedString("i18n.transfers.Cancel");
          scopeObj.view.settings.btnSecuritySettingVerificationSend.text = kony.i18n.getLocalizedString("i18n.common.send");
          scopeObj.enableButton(scopeObj.view.settings.btnSecuritySettingVerificationSend);
          //scopeObj.enableButton(scopeObj.view.settings.btnUsernameVerificationSend);
          scopeObj.showViews(["flxSecuritySettingVerificationWrapper"]);
        }
        else
          scopeObj.getAndShowAnswerSecurityQuestions();
      };
      this.view.settings.btnAcknowledgementDone.onClick= function(){
        scopeObj.presenter.showProfileSettings();
      };
      this.view.settings.btnBackAnswerSecurityQuestions.onClick= function(){
        scopeObj.view.settings.tbxAnswers1.text = " ";
        scopeObj.view.settings.tbxAnswers2.text = " ";
      };
      this.view.settings.btnVerifyAnswerSecurityQuestions.onClick= function(){
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.verifyQuestionsAnswer(scopeObj.onSaveAnswerSecurityQuestions());
        //CommonUtilities.hideProgressBar(scopeObj.view);
      };

      //Secure Access Code 

      this.view.settings.btnSecureAccessCodeModify.onClick= function(){
        var isPhoneEnabled,isEmailenabled;
        var data=[];
        if(scopeObj.view.settings.btnSecureAccessCodeModify.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Save")){
          if(scopeObj.view.settings.imgPhoneUnchecked.src === "checked_box.png")
            data.isPhoneEnabled = "true";
          else
            data.isPhoneEnabled = "false";
          if(scopeObj.view.settings.imgemailunchecked.src === "checked_box.png")
            data.isEmailEnabled = "true";
          else
            data.isEmailEnabled = "false";


          scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
          scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Modify");
          CommonUtilities.showProgressBar(scopeObj.view);
          scopeObj.presenter.updateSecureAccessOptions(data);
          //CommonUtilities.hideProgressBar(scopeObj.view);
        }
        else{
          scopeObj.view.settings.flxPhoneUnchecked.onClick = function() {
            scopeObj.toggleCheckBox(scopeObj.view.settings.imgPhoneUnchecked);
          };
          scopeObj.view.settings.flxemailunchecked.onClick = function() {
            scopeObj.toggleCheckBox(scopeObj.view.settings.imgemailunchecked);
          };
          scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(true);
          scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
        }
      };
      this.view.settings.flxPhoneUnchecked.onClick= function(){
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgPhoneUnchecked);
      };
      this.view.settings.flxemailunchecked.onClick= function(){
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgemailunchecked);
      };

      //Security Settings Code
      //             this.view.settings.btnEditSecuritySettingsProceed.onClick= function(){
      //                 if(scopeObj.view.settings.btnEditSecuritySettingsProceed.text == "RESET QUESTIONS"){
      //                     scopeObj.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(true);
      //                     scopeObj.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(false);
      //                     scopeObj.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(true);
      //                     scopeObj.view.settings.flxSecurityQASet1.setVisibility(true);
      //                     scopeObj.view.settings.flxSecurityQASet2.setVisibility(true);
      //                     scopeObj.view.settings.flxSecurityQASet3.setVisibility(true);
      //                     scopeObj.view.settings.flxSecurityQASet4.setVisibility(true);
      //                     scopeObj.view.settings.flxSecurityQASet5.setVisibility(true);
      //                     scopeObj.view.settings.btnEditSecuritySettingsCancel.setVisibility(true);
      //                     scopeObj.view.settings.btnEditSecuritySettingsProceed.text = "PROCEED";
      //                 }
      //                 else{
      //                     scopeObj.view.settings.lblSendingOTP.text = "Sending you the OTP";
      //                     scopeObj.view.settings.lblSendingOTPMessage.text = "We are sending you the OTP on your registered mobile. You will not be charged for the same.";
      //                     scopeObj.view.settings.flxOTPtextbox.setVisibility("false");
      //                     //scopeObj.enableButton(scopeObj.view.settings.btnUsernameVerificationSend);
      //                     scopeObj.view.settings.btnSecuritySettingVerificationCancel.text = "CANCEL";
      //                     scopeObj.view.settings.btnSecuritySettingVerificationSend.text = "SEND";
      //                     scopeObj.showSecuritySettingVerification();
      //                 }
      //             };
      this.view.settings.btnSecuritySettingVerificationSend.onClick= function(){
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.requestOtp();
        //CommonUtilities.hideProgressBar(scopeObj.view);
      };
      this.view.settings.flxUsernameVerificationRadioOption1.onClick= function(){
        scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgUsernameVerificationcheckedRadio,scopeObj.view.settings.imgUsernameVerificationcheckedRadioOption2);
      };
      this.view.settings.flxUsernameVerificationRadioOption2.onClick= function(){
        scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgUsernameVerificationcheckedRadioOption2,scopeObj.view.settings.imgUsernameVerificationcheckedRadio);
      };
      //Accounts Flow

      this.view.settings.btnEditAccountsCancel.onClick= function(){
        scopeObj.showAccounts();
      };
      this.view.settings.btnEditAccountsSave.onClick= function(){
        //Write Code to SAVE the changes of accounts
        scopeObj.showAccounts();
      };
      this.view.settings.flxFavoriteEmailCheckBox.onClick = function() {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgFavoriteEmailCheckBox);
      };
      this.view.settings.flximgEnableEStatementsCheckBox.onClick = function() {
        scopeObj.toggleCheckBoxEditAccounts(scopeObj.view.settings.imgEnableEStatementsCheckBox);
      };
      this.view.settings.flxTCContentsCheckbox.onClick = function() {
        if (scopeObj.view.settings.imgTCContentsCheckbox.src === "unchecked_box.png") {
          scopeObj.view.settings.imgTCContentsCheckbox.src = "checked_box.png";
          scopeObj.enableButton(scopeObj.view.settings.btnEditAccountsSave);
        } else {
          scopeObj.view.settings.imgTCContentsCheckbox.src = "unchecked_box.png";
          scopeObj.disableButton(scopeObj.view.settings.btnEditAccountsSave);
        }
      };

      //Set Default Accounts

      this.view.settings.btnDefaultTransactionAccountEdit.onClick= function(){
        CommonUtilities.showProgressBar(scopeObj.view);
        if(scopeObj.view.settings.btnDefaultTransactionAccountEdit.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Save")){
          //Add code for saving the data entered
          var defaultAccounts={
            default_account_transfers:scopeObj.view.settings.lbxTransfers.selectedKey,
            default_account_billPay:scopeObj.view.settings.lbxBillPay.selectedKey,
            default_from_account_p2p:scopeObj.view.settings.lbxPayAPreson.selectedKey,
            default_account_deposit:scopeObj.view.settings.lbxCheckDeposit.selectedKey
          };
          scopeObj.presenter.saveDefaultAccounts(defaultAccounts);
          //           scopeObj.view.settings.lbxTransfers.setVisibility(false);
          //           scopeObj.view.settings.lbxBillPay.setVisibility(false);
          //           scopeObj.view.settings.lbxPayAPreson.setVisibility(false);
          //           scopeObj.view.settings.lbxCheckDeposit.setVisibility(false);
          //           scopeObj.view.settings.flxTransfersValue.setVisibility(true);
          //           scopeObj.view.settings.flxBillPayValue.setVisibility(true);
          //           scopeObj.view.settings.flxPayAPersonValue.setVisibility(true);
          //           scopeObj.view.settings.flxCheckDepositValue.setVisibility(true);
          //           scopeObj.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
          //           scopeObj.view.settings.flxDefaultAccountsSelected.setVisibility(true);
          //           scopeObj.view.settings.btnDefaultTransctionAccountsCancel.setVisibility(false);
          scopeObj.showDefaultScreen();
          scopeObj.view.settings.btnDefaultTransactionAccountEdit.text = "EDIT";
        }
        else{
          scopeObj.presenter.getAccountsList();
          scopeObj.view.settings.lbxTransfers.setVisibility(true);
          scopeObj.view.settings.flxTransfersValue.setVisibility(false);
          scopeObj.view.settings.lbxBillPay.setVisibility(true);
          scopeObj.view.settings.flxBillPayValue.setVisibility(false);
          scopeObj.view.settings.lbxPayAPreson.setVisibility(true);
          scopeObj.view.settings.flxPayAPersonValue.setVisibility(false);
          scopeObj.view.settings.lbxCheckDeposit.setVisibility(true);
          scopeObj.view.settings.flxCheckDepositValue.setVisibility(false);
          scopeObj.view.settings.btnDefaultTransactionAccountEdit.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
          scopeObj.view.settings.btnDefaultTransctionAccountsCancel.setVisibility(true);
        }
      };
      this.view.settings.btnDefaultTransctionAccountsCancel.onClick = function(){
        scopeObj.view.settings.btnDefaultTransactionAccountEdit.onClick();
        scopeObj.presenter.getDefaultUserProfile();
      };
      //Info Actions
      this.view.settings.flxImgInfoIcon.onClick = function() {
        var scrollpos=scopeObj.view.contentOffsetMeasured;
        if(scopeObj.view.settings.AllForms.isVisible === false) {
          scopeObj.view.settings.AllForms.isVisible = true;
        }
        else
          scopeObj.view.settings.AllForms.isVisible = false;
        scopeObj.view.forceLayout();
        scopeObj.view.setContentOffset(scrollpos);
      };
      this.view.settings.AllForms1.flxCross.onClick = function() {
        var scrollpos=scopeObj.view.contentOffsetMeasured;
        scopeObj.view.settings.AllForms1.isVisible = false;
        scopeObj.view.forceLayout();
        scopeObj.view.setContentOffset(scrollpos);
      };
      /*
    this.view.settings..onClick= function(){
      scopeObj.();
    };
    this.view.settings..onClick= function(){
      scopeObj.();
    };
    this.view.settings..onClick= function(){
      scopeObj.();
    };
    this.view.settings..onClick= function(){
      scopeObj.();
    };
    this.view.settings..onClick= function(){
      scopeObj.();
    };*/
    },
    /**
  * Method to show default screen
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showDefaultScreen:function(){
      this.view.settings.lbxTransfers.setVisibility(false);
      this.view.settings.lbxBillPay.setVisibility(false);
      this.view.settings.lbxPayAPreson.setVisibility(false);
      this.view.settings.lbxCheckDeposit.setVisibility(false);
      this.view.settings.flxTransfersValue.setVisibility(true);
      this.view.settings.flxBillPayValue.setVisibility(true);
      this.view.settings.flxPayAPersonValue.setVisibility(true);
      this.view.settings.flxCheckDepositValue.setVisibility(true);
      this.view.settings.flxDefaultTransactionAccountWarning.setVisibility(false);
      this.view.settings.flxDefaultAccountsSelected.setVisibility(true);
      this.view.settings.btnDefaultTransctionAccountsCancel.setVisibility(false);
      this.view.settings.btnDefaultTransactionAccountEdit.text = "EDIT";
    },
    showPassword: function() {
      this.view.settings.tbxEnterOTP.secureTextEntry = false;
    },
    hidePassword: function() {
      this.view.settings.tbxEnterOTP.secureTextEntry = true;
    },
    /**
  * Method to assign skin to the left side menu
  * @member of frmProfileManagementController
  * @param {String} name - name of the menu selected
  * @returns {void} - None
  * @throws {void} -None
  */
    setSelectedSkin: function(name){
      var scopeObj = this;
      var names=["Profile","Phone","Email","Address","UsernameAndPassword","SecurityQuestions","SecureAccessCode","AccountPreferences","SetDefaultAccount"];
      //Username & Password
      this.view.settings.btnUsernameEdit.onClick = function () {
        scopeObj.view.settings.flxErrorEditUsername.setVisibility(false);
        scopeObj.view.settings.tbxUsername.text = kony.mvc.MDAApplication.getSharedInstance().appContext.username;
        scopeObj.view.settings.placeholder = "";
        scopeObj.ValidateUsername();
        //scopeObj.disableButton(scopeObj.view.settings.btnEditUsernameProceed);
        scopeObj.showEditUsername();
      };
      this.view.settings.btnPasswordEdit.onClick = function () {
        scopeObj.showEditPassword();
      };
      this.view.settings.btnEditUsernameCancel.onClick = function () {
        scopeObj.showUsernameAndPassword();
      };
      this.view.settings.btnEditUsernameProceed.onClick = function () {
        this.view.settings.flxErrorEditUsername.setVisibility(false);
        this.view.settings.flxEditUsernameButtons.top = "293dp";
        scopeObj.view.settings.btnSecuritySettingVerificationCancel.onClick = function(){
          scopeObj.showUsernameAndPassword();
          scopeObj.setSelectedSkin("UsernameAndPassword");
          scopeObj.view.forceLayout();
        }
        scopeObj.view.settings.btnUsernameVerificationCancel.onClick = function(){
          scopeObj.showUsernameAndPassword();
          scopeObj.setSelectedSkin("UsernameAndPassword");
          scopeObj.view.forceLayout();
        }
        update = "username";
        scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SendingYouTheOtp");
        scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.common.sendingOTP");
        scopeObj.view.settings.flxOTPtextbox.setVisibility(false); 
        //scopeObj.enableButton(scopeObj.view.settings.btnUsernameVerificationSend);
        scopeObj.showUsernameVerificationByChoice();

      };
      this.view.settings.btnEditPasswordCancel.onClick = function () {
        scopeObj.showUsernameAndPassword();
      };
      this.view.settings.btnUsernameVerification2Cancel.onClick = function () {
        scopeObj.view.settings.tbxExistingPassword.text = "";
        scopeObj.view.settings.tbxNewPassword.text = "";
        scopeObj.view.settings.tbxConfirmPassword.text = "";
        scopeObj.view.settings.tbxUsername = "";
        scopeObj.disableButton(scopeObj.view.settings.btnEditPasswordProceed);
        scopeObj.presenter.showProfileSettings();
      };
      this.view.settings.btnUsernameVerification2Proceed.onClick = function () {
        if (scopeObj.view.settings.btnUsernameVerification2Proceed.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")) {
          scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.usernameUpdatedSuccess");
          scopeObj.showAcknowledgement();
        } else {
          scopeObj.view.settings.btnUsernameVerification2Cancel.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Re-sendOTP");
          scopeObj.view.settings.btnUsernameVerification2Proceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
          scopeObj.view.settings.lblUsernameVerification2SendingYouTheOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.EnterOTPsentOnYourMobilePhone");
          scopeObj.view.settings.lblUsernameVerificationtext2.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.IfYouHavenotRecievedOTP");
          scopeObj.view.settings.flxCVV.setVisibility(true);
        }
      };
      this.view.settings.btnUsernameVerificationCancel.onClick = function () {
        scopeObj.view.settings.tbxExistingPassword.text = "";
        scopeObj.view.settings.tbxNewPassword.text = "";
        scopeObj.view.settings.tbxConfirmPassword.text = "";
        scopeObj.view.settings.tbxUsername = "";
        scopeObj.disableButton(scopeObj.view.settings.btnEditPasswordProceed);
        scopeObj.presenter.showProfileSettings();
      };
      this.view.settings.btnAcknowledgementDone.onClick = function () {
        scopeObj.presenter.showProfileSettings();
      };
      this.view.settings.btnBackAnswerSecurityQuestions.onClick = function () {
        scopeObj.view.settings.tbxAnswers1.text = " ";
        scopeObj.view.settings.tbxAnswers2.text = " ";
      };
      this.view.settings.btnVerifyAnswerSecurityQuestions.onClick = function () {
        CommonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.verifyQuestionsAnswer(scopeObj.onSaveAnswerSecurityQuestions());
        //CommonUtilities.hideProgressBar(scopeObj.view);
      };

      //Secure Access Code 

      this.view.settings.btnSecureAccessCodeModify.onClick = function () {
        var isPhoneEnabled,isEmailenabled;
        var data=[];
        if(scopeObj.view.settings.btnSecureAccessCodeModify.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Save")){
          if(scopeObj.view.settings.imgPhoneUnchecked.src === "checked_box.png")
            data.isPhoneEnabled = "true";
          else
            data.isPhoneEnabled = "false";
          if(scopeObj.view.settings.imgemailunchecked.src === "checked_box.png")
            data.isEmailEnabled = "true";
          else
            data.isEmailEnabled = "false";


          scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(false);
          scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Modify");
          CommonUtilities.showProgressBar(scopeObj.view);

          scopeObj.presenter.updateSecureAccessOptions(data);
          //CommonUtilities.hideProgressBar(scopeObj.view);
        }
        else{
          scopeObj.view.settings.btnSecureAccessCodeCancel.setVisibility(true);
          scopeObj.view.settings.btnSecureAccessCodeModify.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Save");
        }
      };
      this.view.settings.flxPhoneUnchecked.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgPhoneUnchecked);
      };
      this.view.settings.flxemailunchecked.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgemailunchecked);
      };

      //Security Settings Code

      this.view.settings.btnEditSecuritySettingsCancel.onClick = function () {
        if (flag == 1) {
          scopeObj.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(false);
          scopeObj.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(true);
          scopeObj.view.settings.flxSecurityQuestionSet.setVisibility(false);
          scopeObj.view.settings.flxEditSecuritySettingsButtons.top = "340dp";
          scopeObj.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
          scopeObj.view.settings.btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ResetQuestions");
          scopeObj.enableButton(scopeObj.view.settings.btnEditSecuritySettingsProceed);
        } else {
          scopeObj.presenter.showProfileSettings();
        }
        scopeObj.selectedQuestions = {
          ques: ["None", "None", "None", "None", "None"],
          key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
        };
        scopeObj.selectedQuestionsTemp= {
          securityQuestions: [],
          flagToManipulate: []
        };
      };
      this.view.settings.btnSecuritySettingVerificationSend.onClick = function () {
        if (scopeObj.view.settings.btnSecuritySettingVerificationSend.text === kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify")) {
          scopeObj.view.settings.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.SecurityQuesUpdatedSuccess");
          scopeObj.showAcknowledgement();
        } else {
          scopeObj.view.settings.lblSendingOTP.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.EnterOTPsentOnYourMobilePhone");
          scopeObj.view.settings.lblSendingOTPMessage.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.IfYouHavenotRecievedOTP");
          scopeObj.view.settings.flxOTPtextbox.setVisibility(true);
          scopeObj.view.settings.btnSecuritySettingVerificationCancel.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Re-sendOTP");
          scopeObj.view.settings.btnSecuritySettingVerificationSend.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Verify");
        }
      };
      this.view.settings.flxUsernameVerificationRadioOption1.onClick = function () {
        scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgUsernameVerificationcheckedRadio, scopeObj.view.settings.imgUsernameVerificationcheckedRadioOption2);
      };
      this.view.settings.flxUsernameVerificationRadioOption2.onClick = function () {
        scopeObj.showPhoneRadiobtn(scopeObj.view.settings.imgUsernameVerificationcheckedRadioOption2, scopeObj.view.settings.imgUsernameVerificationcheckedRadio);
      };
      //Accounts Flow

      this.view.settings.btnEditAccountsCancel.onClick = function () {
        scopeObj.showAccounts();
      };
      this.view.settings.btnEditAccountsSave.onClick = function () {
        //Write Code to SAVE the changes of accounts
        scopeObj.showAccounts();
      };

      this.view.settings.flxFavoriteEmailCheckBox.onClick = function() {
        scopeObj.toggleCheckBox(scopeObj.view.settings.imgFavoriteEmailCheckBox);
      };
      this.view.settings.flximgEnableEStatementsCheckBox.onClick = function() {
        scopeObj.toggleCheckBoxEditAccounts(scopeObj.view.settings.imgEnableEStatementsCheckBox);
      };
      this.view.settings.flxTCContentsCheckbox.onClick = function() {
        if (scopeObj.view.settings.imgTCContentsCheckbox.src === "unchecked_box.png") {
          scopeObj.view.settings.imgTCContentsCheckbox.src = "checked_box.png";
          scopeObj.enableButton(scopeObj.view.settings.btnEditAccountsSave);
        } else {
          scopeObj.view.settings.imgTCContentsCheckbox.src = "unchecked_box.png";
          scopeObj.disableButton(scopeObj.view.settings.btnEditAccountsSave);
        }
      };
      t

      //Set Default Accounts


      /*
    this.view.settings..onClick= function(){
      scopeObj.();
    };
    this.view.settings..onClick= function(){
      scopeObj.();
    };
    this.view.settings..onClick= function(){
      scopeObj.();
    };
    this.view.settings..onClick= function(){
      scopeObj.();
    };
    this.view.settings..onClick= function(){
      scopeObj.();
    };*/
    },
    /**
  * Method to assign skin to the menu item selected
  * @member of frmProfileManagementController
  * @param {String} name - menu item selected
  * @returns {void} - None
  * @throws {void} -None
  */
    setSelectedSkin: function (name) {
      var names = ["Profile", "Phone", "Email", "Address", "UsernameAndPassword", "SecurityQuestions", "SecureAccessCode", "AccountPreferences", "SetDefaultAccount"];
      for (var i = 0; i < names.length; i++) {
        flxName = "flx" + names[i];
        imgName = "imgindicator" + names[i];
        var temp = this.view.settings[flxName].widgets();
        temp[0].setVisibility(false);
      }
      flxName = "flx" + name;
      imgName = "imgIndicator" + name;
      this.view.settings[flxName][imgName].setVisibility(true);
    },
    /**
  * Method to toggle arrows when menu is expanded
  * @member of frmProfileManagementController
  * @param {String} widget- widget ID
  * @param {String} imgArrow- ID of the arrow to be changed
  * @returns {void} -None
  * @throws {void} -None
  */
    toggle: function (widget, imgArrow) {
      var scope = this;
      if (widget.frame.height > 0) {
        this.collapseAll();
      } else {
        this.collapseAll();
        imgArrow.src = "arrow_up.png";
        this.expand(widget);
      }
    },
    /**
  * Method to assign images when checkbox is clicked
  * @member of frmProfileManagementController
  * @param {String} imgCheckBox- ID of the checkbox
  * @returns {void} - None
  * @throws {void} -None
  */
    toggleCheckBox: function (imgCheckBox) {
      if(imgCheckBox.src == "unchecked_box.png") {
        imgCheckBox.src = "checked_box.png";
      } else {
        imgCheckBox.src = "unchecked_box.png";
      }
    },
    /**
  * Method to assign images when checkbox is clicked in accounts
  * @member of frmProfileManagementController
  * @param {String} imgCheckBox- ID of the checkbox
  * @returns {void} - None
  * @throws {void} -None
  */

    toggleCheckBoxEditAccounts: function(imgCheckBox) {
      CommonUtilities.toggleCheckBox(imgCheckBox);
      if(CommonUtilities.isChecked(imgCheckBox) === true)
      {
        this.view.settings.flxTCCheckBox.setVisibility(true);
        this.view.settings.flxPleaseNoteTheFollowingPoints.height = "160dp";
        if(CommonUtilities.isChecked(this.view.settings.imgTCContentsCheckbox) === true){
          this.enableButton(this.view.settings.btnEditAccountsSave);
        }
        else{
          this.disableButton(this.view.settings.btnEditAccountsSave);
        }
      }
      else
      {
        this.view.settings.flxTCCheckBox.setVisibility(false);
        this.view.settings.flxPleaseNoteTheFollowingPoints.height = "120dp";
        this.enableButton(this.view.settings.btnEditAccountsSave);
        if(CommonUtilities.isChecked(this.view.settings.imgTCContentsCheckbox) === true){
          CommonUtilities.toggleCheckBox(this.view.settings.imgTCContentsCheckbox);
        }
      }
    },
    textBoxErrorSkinToggle: function (txtBox) {
      if (txtBox.skin == "skntxtLato424242Bordere3e3e3Op100Radius2px")
        txtBox.skin = "skntxtLato424242Bordere3e3e3Op100Radius2px";
      else
        txtBox.skin = "skntxtLato424242Bordere3e3e3Op100Radius2px";
    },
    /**
  * Method to assign animation to menu while collapsing
  * @member of frmProfileManagementController
  * @param {String} widget- ID of the widget
  * @returns {void} - None
  * @throws {void} -None
  */
    collapseWithoutAnimation: function (widget) {
      widget.height = 0;
      this.view.settings.forceLayout();
    },
    /**
  * Method to expand menu without animation
  * @member of frmProfileManagementController
  * @param {String} widget- ID of the widget
  * @returns {void} - None
  * @throws {void} -None
  */
    expandWithoutAnimation: function (widget) {
      widget.height = widget.widgets().length * 40;
      this.view.settings.forceLayout();
    },
    /**
  * Method to close all menu options without animation
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    collapseAll: function () {
      for (var i = 0; i < widgetsMap.length; i++) {
        var menuObject = widgetsMap[i];
        this.collapseWithoutAnimation(this.view.settings[menuObject.subMenu.parent]);
        this.view.settings[menuObject.image].src = "arrow_down.png";
      }
    },
    /**
  * Method to activate a menu item
  * @member of frmProfileManagementController
  * @param {String} parentIndex- parent to be exapnded
  * @param {String} childrenIndex- child to be expanded in parent
  * @returns {void} - None
  * @throws {void} -None
  */
    activateMenu: function (parentIndex, childrenIndex) {
      var menuObject = widgetsMap[parentIndex];
      this.collapseAll();
      this.expandWithoutAnimation(this.view.settings[menuObject.subMenu.parent]);
      this.view.settings[menuObject.subMenu.children[childrenIndex]].widget.skin = "sknMenuSelected";
      this.view.settings[menuObject.subMenu.children[childrenIndex]].widget.hoverSkin = "sknMenuSelected";
    },
    /**
  * Method to assign initial action on onclick of widgets
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    initProfileSettingsMenu: function () {
      this.view.customheader.forceCloseHamburger();
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        kony.print("btn logout pressed");
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.frame.height;
        scopeObj.view.flxLogout.height = height + "dp";
        scopeObj.view.flxLogout.left = "0%";
        scopeObj.view.flxLogout.setVisibility(true);
        scopeObj.view.forceLayout();
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      if (CommonUtilities.getConfiguration("editPassword") == "false") {
        this.view.settings.btnPasswordEdit.setVisibility(false);
      } else
        this.view.settings.btnPasswordEdit.setVisibility(true);
      if (CommonUtilities.getConfiguration("editUsername") == "false") {
        this.view.settings.btnUsernameEdit.setVisibility(false);
      } else
        this.view.settings.btnUsernameEdit.setVisibility(true);


      // Resetting Top Menu Skin
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
      this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false); 
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox"; 
      // this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";

      function applyConfigurations (menuObject) {
        var children = menuObject.subMenu.children
        var invisibleCount = children.reduce (function (count, child) {
          var configuration = CommonUtilities.getConfiguration(child.configuration);
          if (configuration === "false") {
            count +=1;
            scopeObj.view.settings[child.widget].setVisibility(false);
          }
          return count;          
        }, 0)
        var visibleCount = children.length - visibleCount;
        scopeObj.view.settings[menuObject.subMenu.parent].height = 40 * visibleCount + "px";
        if (invisibleCount === children.length) {
          scopeObj.view.settings[menuObject.menu].setVisibility(false);          
        }
      }
      var scopeObj = this;
      var component = this.view.settings;
      for (var i = 0; i < widgetsMap.length; i++) {
        var menuObject = widgetsMap[i];
        applyConfigurations(menuObject);
        scopeObj.view.settings.forceLayout();
        scopeObj.view.settings[menuObject.menu].onClick = this.getMenuHandler(this.view.settings[menuObject.subMenu.parent], this.view.settings[menuObject.image]);
      }
      this.setFlowActions();
      this.collapseAll();
      this.showViews();
      this.configurationContactSettings();
      //this.resetSkins();
    },
    /**
  * Method to manage Configuration of Address fields and phone number fields
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    configurationContactSettings: function () {
      this.view.settings.btnAddNewAddress.setVisibility(CommonUtilities.getConfiguration('additionalAddressAllowed') === "true");
      this.view.settings.btnAddNewNumber.setVisibility(CommonUtilities.getConfiguration('additionalPhoneAllowed') === "true")
    },
    /*resetSkins: function () {
    for (var i = 0; i < widgetsMap.length; i++) {
      var menuObj = widgetsMap[i];
      for (var j = 0; j < menuObj.subMenu.children.length; j++) {
        var element = menuObj.subMenu.children[j];
        this.view.settings[element].skin = "sknFlxf6f6f6BottomBorder";
        //this.view.settings[element].hoverSkin = "sknFlxHoverHamburger";
      }

    }
  },
  getLayoutProperties: function () {
    kony.print(this.view.settings.flxLogout.frame);
    this.logoutY = this.view.settings.flxLogout.frame.y;
  },*/
    /**
  * Method to highlight the menu item and collapse others on the left side
  * @member of frmProfileManagementController
  * @param {String} submenu- Item of the menu to be highlighted
  * @param {String} collapseImage- Arrow to be expanded
  * @returns {void} - None
  * @throws {void} -None
  */
    getMenuHandler: function (subMenu, collapseImage) {
      return function () {
        this.toggle(subMenu, collapseImage);
      }.bind(this);
    },
    /**
  * Method to expand a menu item with animation
  * @member of frmProfileManagementController
  * @param {String} widget- ID of the widget to be expanded
  * @returns {void} - None
  * @throws {void} -None
  */
    expand: function (widget) {
      var scope = this;
      function getVisibleChildrenLength (widget) {
        var children = widget.widgets();
        var visible  = children.reduce(function (count, child) {
          if (child.isVisible) {
            count+=1
          }
          return count;
        }, 0)
        return visible;
      }
      var animationDefinition = {
        100: {
          "height": getVisibleChildrenLength(widget) * 40
        }
      };
      var animationConfiguration = {
        duration: 0.5,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function () {
          // scope.checkLogoutPosition();
          scope.view.forceLayout();
        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      widget.animate(animationDef, animationConfiguration, callbacks);
    },
    /*fixHamburgerheight: function () {
    this.view.settings.height = kony.os.deviceInfo().screenHeight;
  },*/
    /**
  * Method to collapse the menu item with animation
  * @member of frmProfileManagementController
  * @param {String} widget- ID of the widget to be collapsed
  * @returns {void} - None
  * @throws {void} -None
  */
    collapse: function (widget) {
      var scope = this;
      var animationDefinition = {
        100: {
          "height": 0
        }
      };
      var animationConfiguration = {
        duration: 0.5,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function () {
          // scope.checkLogoutPosition();
          scope.view.forceLayout();

        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      widget.animate(animationDef, animationConfiguration, callbacks);
    },
    /**
  * Method to edit the primary Email of the User
  * @member of frmProfileManagementController
  * @param {JSON} emailObj- JSON object with email and its ID
  * @returns {void} - None
  * @throws {void} -None
  */
    editPrimaryEmail: function (emailObj) {
      this.view.settings.lblEditMarkAsPrimaryEmail.setVisibility(false);
      this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.setVisibility(false);
      this.view.forceLayout();
      var scopeObj = this;
      scopeObj.showEditEmail();
      scopeObj.view.settings.tbxEditEmailId.text = emailObj.email;
      this.checkUpdateEmailForm();
      scopeObj.view.settings.btnEditEmailIdSave.onClick = function () {
        scopeObj.presenter.userProfile.editEmail({
          id: emailObj.id,
          email: scopeObj.view.settings.tbxEditEmailId.text,
          isPrimary: true
        });
      };
      scopeObj.checkUpdateEmailForm();
    },
    /**
  * Method to edit the email which is already set
  * @member of frmProfileManagementController
  * @param {JSON} emailObj- JSON object of the email with all fields comminf from backend
  * @returns {void} - None
  * @throws {void} -None
  */
    editEmail: function (emailObj) {
      var scopeObj = this;
      this.view.settings.lblEditMarkAsPrimaryEmail.setVisibility(!emailObj.isPrimary);
      this.view.settings.flxEditMarkAsPrimaryEmailCheckBox.setVisibility(!emailObj.isPrimary);
      scopeObj.view.settings.imgEditMarkAsPrimaryEmailCheckBox.src = "unchecked_box.png";
      scopeObj.showEditEmail();
      this.view.forceLayout();    
      scopeObj.view.settings.tbxEditEmailId.text = emailObj.email;
      this.checkUpdateEmailForm();    
      scopeObj.view.settings.btnEditEmailIdSave.onClick = function () {
        scopeObj.presenter.userProfile.editEmail({
          id: emailObj.id,
          email: scopeObj.view.settings.tbxEditEmailId.text,
          isPrimary: scopeObj.view.settings.imgEditMarkAsPrimaryEmailCheckBox.src === "checked_box.png" ? true : false
        });
      };
      scopeObj.checkAddEmailForm();      
    },
    /**
  * Method to edit the address which is already set
  * @member of frmProfileManagementController
  * @param {JSON} address- All the fields of the Address
  * @returns {void} - None
  * @throws {void} -None
  */
    editAddress: function(address) {
      this.presenter.userProfile.getEditAddressViewModel(address);
    },
    /*postShowHamburger: function () {
    this.view.settings.flxMenu.height = kony.os.deviceInfo().screenHeight + "px";
    this.view.settings.forceLayout();
  }
  */
    /**
  * Method to set all the Data comming from backend to the email module
  * @member of frmProfileManagementController
  * @param {JSON} emailList - List of all the emails of the user
  * @returns {void} - None
  * @throws {void} -None
  */
    setEmailSegmentData: function (emailList) {
      var scopeObj = this;      
      function getDeleteEmailListener(emailObj) {
        return function () {
          var currForm = scopeObj.view;
          currForm.flxDeletePopUp.setVisibility(true);
          currForm.flxDeletePopUp.setFocus(true);
          currForm.lblDeleteHeader.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Delete");
          currForm.lblConfirmDelete.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.deleteEmail");
          currForm.forceLayout();
          currForm.btnDeleteYes.onClick = function () {
            scopeObj.presenter.userProfile.deleteEmail(emailObj);            
            scopeObj.view.flxDelete.setFocus(true);
            scopeObj.view.flxDeletePopUp.setVisibility(false);
            scopeObj.view.forceLayout();
          };
        }
      }

      function getPrimaryEmailEditListener(emailObj) {
        return function () {
          scopeObj.editPrimaryEmail(emailObj);
        }
      }

      function getEditEmailListener(emailObj) {
        return function () {
          scopeObj.editEmail(emailObj);
        }
      }

      var dataMap = {
        "btnDelete": "btnDelete",
        "btnEdit": "btnEdit",
        "flxDeleteAction": "flxDeleteAction",
        "flxEdit": "flxEdit",
        "flxEmailId": "flxEmailId",
        "flxEmail": "flxEmail",
        "flxPrimary": "flxPrimary",
        "flxProfileManagementEmail": "flxProfileManagementEmail",
        "flxRow": "flxRow",
        "flxInfo": "flxInfo",
        "lblSeperator": "lblSeperator",
        "lblSeperatorActions": "lblSeperatorActions",
        "imgInfo": "imgInfo",
        "lblEmailId": "lblEmailId",
        "lblPrimary": "lblPrimary",
        "lblEmail": "lblEmail"
      };

      var data = emailList.map(function (emailObj) {
        return {
          "lblEmail": emailObj.email,
          "lblEmailId": " ",
          "lblPrimary": emailObj.isPrimary ? "Primary" : " ",
          "btnDelete": {
            "text": emailObj.isPrimary ? " ": "DELETE",
            "onClick": getDeleteEmailListener(emailObj)
          },
          "flxInfo": {
            "onClick":scopeObj.toggleContextualMenuEmail.bind(scopeObj)
          },
          "btnEdit": {
            "text": "EDIT",
            "onClick": emailObj.isPrimary ? getPrimaryEmailEditListener(emailObj) :  getEditEmailListener(emailObj)
          },
          "imgInfo": emailObj.isPrimary ? "info_grey.png" : " ",
          "lblSeperator": "lblSeperator",
          "lblSeperatorActions": "lblSeperatorActions",
          "template": "flxProfileManagementEmail"
        }
      })
      this.view.settings.segEmailIds.widgetDataMap = dataMap;
      this.view.settings.segEmailIds.setData(data);
      this.view.forceLayout();
    },
    /**
  * Method to set all the data of the address module
  * @member of frmProfileManagementController
  * @param {JSON} userAddresses- List of all the addresses related to user
  * @returns {void} - None
  * @throws {void} -None
  */
    setAddressSegmentData: function (userAddresses) {
      var scopeObj = this;

      function getDeleteAddressEmailListener(address) {
        return function () {
          var currForm = scopeObj.view;
          currForm.flxDeletePopUp.setVisibility(true);
          currForm.flxDeletePopUp.setFocus(true);
          currForm.lblDeleteHeader.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Delete");
          currForm.lblConfirmDelete.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.deleteAddress");
          currForm.forceLayout();
          currForm.btnDeleteYes.onClick = function () {
            scopeObj.presenter.userProfile.deleteAddress(address);            
            scopeObj.view.flxDelete.setFocus(true);
            scopeObj.view.flxDeletePopUp.setVisibility(false);
            scopeObj.view.forceLayout();
          };

        }
      }
      function getEditAddressListener(address) {
        return function () {
          scopeObj.editAddress(address, userAddresses);
        };
      }

      var dataMap = {
        "btnEdit": "btnEdit",
        "btnDelete": "btnDelete",
        "flxAddress": "flxAddress",
        "flxAddressWrapper": "flxAddressWrapper",
        "flxCommunicationAddress": "flxCommunicationAddress",
        "flxRow": "flxRow",
        "flxInfo": "flxInfo",
        "lblSeperator": "lblSeperator",
        "imgCommunicationAddressInfo": "imgCommunicationAddressInfo",
        "lblAddessLine2": "lblAddessLine2",
        "lblAddressLine1": "lblAddressLine1",
        "lblAddressLine3": "lblAddressLine3",
        "lblAddressType": "lblAddressType",
        "lblSeperatorActions": "lblSeperatorActions",
        "lblCommunicationAddress": "lblCommunicationAddress"
      };
      var addressTypes = {
        'home': 'Home',
        'office': 'Work'
      }

      var data  = userAddresses.map(function (address) { 
        return  {
          "lblAddressType": addressTypes[address.addressType],
          "lblAddressLine1": address.addressLine1,
          "lblAddessLine2": address.addressLine2 ? address.addressLine2 : (address.city + ', ' + address.country + ', ' + address.zipcode) ,
          "lblAddressLine3": address.addressLine2 ? (address.city + ', ' + address.country + ', ' + address.zipcode) : "",
          "lblCommunicationAddress": address.isPreferredAddress === "1" ? "Communication Address" : "",
          "lblSeperatorActions": address.isPreferredAddress === "1" ?  "" : "lblSeperatorActions",
          "imgCommunicationAddressInfo": address.isPreferredAddress === "1" ? "info_grey.png" : "" ,
          "btnEdit": {
            text: "EDIT",
            onClick: getEditAddressListener(address)
          },
          "btnDelete": {
            text: address.isPreferredAddress === "1" ?  "" : "DELETE",
            onClick: getDeleteAddressEmailListener(address)
          },
          "flxInfo": {
            onClick: scopeObj.toggleContextualMenuAddress.bind(scopeObj)
          },
          "lblSeperator":"lblSeperator",
          "template": "flxRow"
        }
      }) 


      this.view.settings.segAddresses.widgetDataMap = dataMap;
      this.view.settings.segAddresses.setData(data);
      this.view.forceLayout();
    },
    /**
  * Method to assign all the data in the Accounts module
  * @member of frmProfileManagementController
  * @param {JSON} viewModel - JSON object which contains details of all the accounts of user
  * @returns {void} - None
  * @throws {void} -None
  */
    setAccountsSegmentData: function(viewModel){
      var self=this;
      var dataMap ={
        "btnEdit": "btnEdit",
        "flxAccountHolder": "flxAccountHolder",
        "flxContent":"flxContent",
        "flxAccountHolderKey": "flxAccountHolderKey",
        "flxAccountHolderValue": "flxAccountHolderValue",
        "flxAccountListItemWrapper": "flxAccountListItemWrapper",
        "flxAccountName": "flxAccountName",
        "flxAccountNumber": "flxAccountNumber",
        "flxAccountNumberKey": "flxAccountNumberKey",
        "flxAccountNumberValue": "flxAccountNumberValue",
        "flxAccountRow": "flxAccountRow",
        "flxAccountType": "flxAccountType",
        "flxAccountTypeKey": "flxAccountTypeKey",
        "flxAccountTypeValue": "flxAccountTypeValue",
        "flxEStatement": "flxEStatement",
        "flxEStatementCheckBox": "flxEStatementCheckBox",
        "flxFavoriteCheckBox": "flxFavoriteCheckBox",
        "flxLeft": "flxLeft",
        "flxMarkAsFavorite": "flxMarkAsFavorite",
        "flxMenu": "flxMenu",
        "flxRight": "flxRight",
        "imgEStatementCheckBox": "imgEStatementCheckBox",
        "imgFavoriteCheckBox": "imgFavoriteCheckBox",
        "flxIdentifier": "flxIdentifier",
        "imgMenu": "imgMenu",
        "lblAccountHolderColon": "lblAccountHolderColon",
        "lblAccountHolderKey": "lblAccountHolderKey",
        "lblAccountHolderValue": "lblAccountHolderValue",
        "lblAccountName": "lblAccountName",
        "lblAccountNumberColon": "lblAccountNumberColon",
        "lblAccountNumberKey": "lblAccountNumberKey",
        "lblAccountNumberValue": "lblAccountNumberValue",
        "lblAccountTypeColon": "lblAccountTypeColon",
        "lblAccountTypeKey": "lblAccountTypeKey",
        "lblAccountTypeValue": "lblAccountTypeValue",
        "lblEStatement": "lblEStatement",
        "lblMarkAsFavorite": "lblMarkAsFavorite",
        "lblSepeartorlast": "lblSepeartorlast",
        "lblSeperator": "lblSeperator",
        "lblSeperator2": "lblSeperator2",
        "lblseperator3":"lblseperator3",
        "lblsepeartorfirst": "lblsepeartorfirst", 
        "flxMoveUp":"flxMoveUp",
        "flxMoveDown":"flxMoveDown",
        "imgMoveUp":"imgMoveUp",
        "lblMove":"lblMove",
        "imgMoveDown":"imgMoveDown",
        "lblMarkAsFavouriteAccountCheckBoxIcon":"lblMarkAsFavouriteAccountCheckBoxIcon",
        "lblEstatementCheckBoxIcon":"lblEstatementCheckBoxIcon",
        "lblMoveUpIcon":"lblMoveUpIcon",
        "lblMoveDownIcon":"lblMoveDownIcon",
        "btn":"btn"
      };
      var mapViewModel=function(data,index,viewModel){
        return {
          "btn":{
            text:""
          },
          "accountPreference":data.accountPreference,
          "fullName":data.fullName,
          "nickName":data.nickName,
          "flxIdentifier":{
            "skin":data.flxIdentifier
          },
          "lblMarkAsFavouriteAccountCheckBoxIcon":{
            text:data.imgFavoriteCheckBox=="checked_box.png"?'C':'D',
          },
          "lblEstatementCheckBoxIcon":{
            text:data.imgEStatementCheckBox=="checked_box.png"?'C':'D',
          },
          "lblMoveUpIcon":index!==0?{
            text:'A',
            skin:"sknLabelLato3343A821px",
          }:{
            text:'A',
            skin:"sknC0C0C021px",
          },
          "lblMoveDownIcon":{
            text:'B',
            skin:"sknLabelLato3343A821px",
          },
          "lblAccountName":data.AccountName,
          "lblAccountHolderKey":"Account Holder",
          "lblAccountHolderColon": ": ",
          "lblAccountHolderValue":data.AccountHolder,
          "lblAccountNumberKey":"Account Number",
          "lblAccountNumberValue":data.AccountNumber,
          "lblAccountTypeKey":"Account Type",
          "lblAccountNumberColon": ": ",
          "lblAccountTypeValue":data.AccountType,
          "imgEStatementCheckBox":data.imgEStatementCheckBox,
          "lblAccountTypeColon": ": ",
          "imgFavoriteCheckBox":data.imgFavoriteCheckBox,
          "lblMarkAsFavorite":"Mark as Favorite Account",
          "lblEStatement":"E-Statement Enabled",
          "btnEdit":{
            text:"EDIT",
            onClick:self.onPreferenceAccountEdit
          },
          "flxMenu": {
            "skin":"CopyslFbox",
            "isVisible":CommonUtilities.getConfiguration('reOrderAccountPreferences')==="true"?true:false,
          },
          "imgMenu":data.imgMenu,
          "imgMoveUp":index!==0?"active_up_btn.png":"disable_up_btn.png",
          "lblMove":"Move",
          "imgMoveDown":"active_down_btn.png",
          "lblSepeartorlast": "lblSepeartorlast",
          "lblsepeartorfirst": "lblsepeartorfirst",
          "flxMoveUp":{
            onClick:function(){    
              if(index!==0){
                self.onBtnMoveUp(viewModel);
              }
            }
          },
          "flxMoveDown":{
            onClick:function(){
              self.onBtnMoveDown(viewModel);
            }
          },
          "lblSeperator": "lblSeperator",
          "lblSeperator2": "lblSeperator2",
          "lblseperator3":"lblseperator3",
          "template":"flxAccountRow",
          "email" : data.email,
        };
      };
      var data=viewModel.map(mapViewModel);
      var len=data.length;
      if(len!==0){
        data[data.length-1].imgMoveDown="disable_down_btn.png";
        data[data.length-1].lblMoveDownIcon={
          text:'B',
          skin:"sknC0C0C021px"
        };
        data[data.length-1].flxMoveDown=function(){};
      }
      this.view.settings.segAccounts.widgetDataMap=dataMap; 
      this.view.settings.segAccounts.setData(data);
      this.view.forceLayout();
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
  * Method to move a particular account upwards
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- JSON object of the account to be moved upwards
  * @returns {void} - None
  * @throws {void} -None
  */
    onBtnMoveUp:function(viewModel){
      var index=this.view.settings.segAccounts.selectedIndex[1];
      var updatedAccounts=[{
        accountNumber:viewModel[index].AccountNumber,
        accountPreference:viewModel[index-1].accountPreference.toString(),
      },{
        accountNumber:viewModel[index-1].AccountNumber,
        accountPreference:viewModel[index].accountPreference.toString(),
      }]
      //alert(JSON.stringify(updateAccounts));
      CommonUtilities.showProgressBar(this.view);
      this.presenter.setAccountsPreference(updatedAccounts);
    },
    /**
  * Method to move a particular account downwards
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- JSON object of the account to be moved downwards
  * @returns {void} - None
  * @throws {void} -None
  */
    onBtnMoveDown:function(viewModel){
      var index=this.view.settings.segAccounts.selectedIndex[1];
      var updatedAccounts=[{
        accountNumber:viewModel[index].AccountNumber,
        accountPreference:viewModel[index+1].accountPreference.toString(),
      },{
        accountNumber:viewModel[index+1].AccountNumber,
        accountPreference:viewModel[index].accountPreference.toString(),
      }]
      //alert(JSON.stringify(updateAccounts));
      CommonUtilities.showProgressBar(this.view);
      this.presenter.setAccountsPreference(updatedAccounts);
    },

    selectedQuestions: {
      ques: ["None", "None", "None", "None", "None"],
      key: ["lb0", "lb0", "lb0", "lb0", "lb0"]
    },
    selectedQuestionsTemp: {
      securityQuestions: [],
      flagToManipulate: []
    },
    responseBackend:[
      {question:"",
       SecurityID :""
      }
    ],
    /**
  * Method to show security questions which should get edit
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showEditSecuritySettings : function(){
      if(flag === 0){
        this.showViews(["flxEditSecuritySettingsWrapper"]);
        CommonUtilities.showProgressBar(this.view);
        this.presenter.fetchSecurityQuestions(this.selectedQuestionsTemp, this.staticSetQuestions.bind(this));
      }
      else{
        this.showViews(["flxEditSecuritySettingsWrapper"]);
        this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(false);
        this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(true);
        this.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(false);
        this.view.settings.flxSecurityQASet1.setVisibility(false);
        this.view.settings.flxSecurityQASet2.setVisibility(false);
        this.view.settings.flxSecurityQASet3.setVisibility(false);
        this.view.settings.flxSecurityQASet4.setVisibility(false);
        this.view.settings.flxSecurityQASet5.setVisibility(false);
        this.view.settings.btnEditSecuritySettingsCancel.setVisibility(false);
        this.view.settings. btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ResetQuestions");
        this.enableButton(scopeObj.view.settings.btnEditSecuritySettingsProceed);
      }
    },
    /**
  * Method to assign initial action on onclick of widgets
  * @member of frmProfileManagementController
  * @param {JSON} response- JSON of the questions fetched from MF
  * @param {JSON} data- JSON of the questions
  * @returns {void} - None
  * @throws {void} -None
  */
    staticSetQuestions: function(response,data) {
      if(!data.errmsg){
        CommonUtilities.hideProgressBar(this.view);
        this.responseBackend= data;
        this.successCallback(response);
        this.showSetSecurityQuestions();
        CommonUtilities.hideProgressBar(this.view);
      }
      else{
        this.showFetchSecurityQuestionsError();
      }


    },
    /**
  * Method to show all the security questions
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    showSetSecurityQuestions: function () {
      //this.view.flxUsernameAndPasswordAck.setVisibility(false);
      this.view.settings.flxSecuritySettingsResetQuestionsWarning.setVisibility(false);
      this.view.settings.flxSecurityQuestionSet.setVisibility(true);
      this.view.settings.flxSecuritySettingsQuestionsWarning.setVisibility(true);
      this.view.settings.lblSelectQuestionsAndAnswersSet.setVisibility(true);
      this.view.settings.flxSecurityQASet1.setVisibility(true);
      this.view.settings.flxSecurityQASet2.setVisibility(true);
      this.view.settings.flxSecurityQASet3.setVisibility(true);
      this.view.settings.flxSecurityQASet4.setVisibility(true);
      this.view.settings.flxSecurityQASet5.setVisibility(true);
      this.view.settings. btnEditSecuritySettingsProceed.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Proceed");
      this.view.forceLayout();
    },
    /**
  * Method to set data to all the list boxes initially
  * @member of frmProfileManagementController
  * @param {JSON} response- JSON of questions which should get set
  * @returns {void} - None
  * @throws {void} -None
  */
    successCallback: function(response) {
      var data = [];
      this.selectedQuestionsTemp = response;
      data = this.getQuestions(response);
      this.view.settings.lbxQuestion1.masterData = data;
      this.view.settings.lbxQuestion2.masterData = data;
      this.view.settings.lbxQuestion3.masterData = data;
      this.view.settings.lbxQuestion4.masterData = data;
      this.view.settings.lbxQuestion5.masterData = data;
      this.view.settings.tbxAnswer1.text="";
      this.view.settings.tbxAnswer2.text="";
      this.view.settings.tbxAnswer3.text="";
      this.view.settings.tbxAnswer4.text="";
      this.view.settings.tbxAnswer5.text="";
      this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);


    },
    /**
  * Method to manipulate data in listbox
  * @member of frmProfileManagementController
  * @param {JSON} data- JSON of all the questions
  * @returns {JSON} selectedData- JSON of seleted question and answer
  * @throws {void} -None
  */
    flagManipulation: function(data, selectedData) {
      var tempData1 = [],
          tempData2 = [];
      if (selectedData[0] !== "lb0") {
        tempData1[0] = selectedData;
        tempData2 = tempData1.concat(data);
      } else
        tempData2 = data;
      return tempData2;

    },
    /**
  * Method  to manipulate the flag of the question selected
  * @member of frmProfileManagementController
  * @param {void} data - JSON of all the questions with answers
  * @param {void} selectedQues - JSON of the selected question with its answer
  * @param {void} key - ID of the question selected
  * @returns {void} - None
  * @throws {void} -None
  */
    getQuestionsAfterSelected: function(data, selectedQues, key) {
      var response = [];
      var temp = 10;
      if (data[1] !== "None") {
        for (var i = 0; i < this.selectedQuestionsTemp.securityQuestions.length; i++) {
          if (this.selectedQuestionsTemp.securityQuestions[i] === data[1]) {
            if (this.selectedQuestionsTemp.flagToManipulate[i] === "false") {
              this.selectedQuestionsTemp.flagToManipulate[i] = "true";
              temp = i;
            }
          }
          if (this.selectedQuestionsTemp.securityQuestions[i] === selectedQues.ques) {
            if (this.selectedQuestionsTemp.flagToManipulate[i] === "true") {
              this.selectedQuestionsTemp.flagToManipulate[i] = "false";
              this.selectedQuestions.key[key] = "lb" + (i + 1);
            }
          }
        }
      }
      else{
        this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);
        if(key===0){
          this.view.settings.tbxAnswer1.text="";
        }
        else if(key===1){
          this.view.settings.tbxAnswer2.text="";
        }
        else if(key===2){
          this.view.settings.tbxAnswer3.text="";
        }
        else if(key===3){
          this.view.settings.tbxAnswer4.text="";
        }
        else if(key===4){
          this.view.settings.tbxAnswer5.text="";
        }
        for (var ij = 0; ij < this.selectedQuestionsTemp.securityQuestions.length; ij++) {
          if (this.selectedQuestionsTemp.securityQuestions[ij] === selectedQues.ques) {
            if (this.selectedQuestionsTemp.flagToManipulate[ij] === "true") {
              this.selectedQuestionsTemp.flagToManipulate[ij] = "false";
              this.selectedQuestions.key[key] = "lb" + (ij + 1);
            }
          }
        }
      }
      if (temp != 10) {
        this.selectedQuestions.ques[key] = this.selectedQuestionsTemp.securityQuestions[temp];
        this.selectedQuestions.key[key] = "lb" + (temp + 1);
      }
      else{ 
        this.selectedQuestions.ques[key] = "None";
        this.selectedQuestions.key[key] = "lb0";
      }
      var questions = [];
      questions = this.getQuestions(this.selectedQuestionsTemp);
      return questions;
    },
    /**
  * Method that changes questions into key-value pairs basing on the flagManipulation
  * @member of frmProfileManagementController
  * @param {JSON} response- JSON of all the questions 
  * @returns {JSON} temp- JSON of the particular question selected
  * @throws {void} -None
  */
    getQuestions: function(response) {
      var temp = [];
      temp[0] = ["lb0", "None"];
      for (var i = 0, j = 1; i < response.securityQuestions.length; i++) {
        var arr = [];
        if (response.flagToManipulate[i] === "false") {
          arr[0] = "lb" + (i + 1);
          arr[1] = response.securityQuestions[i];
          temp[j] = arr;
          j++;
        }
      }
      return temp;
    },
    /**
  * Method to enable Proceed button
  * @member of frmProfileManagementController
  * @param {String} question1- First question selected
  * @param {String} question1- Second question selected
  * @param {String} question1- Third question selected
  * @param {String} question1- Fourth question selected
  * @param {String} question1- Fifth question selected
  * @returns {void} - None
  * @throws {void} -None
  */
    enableSecurityQuestions: function(question1, question2, question3, question4, question5) {
      if (question1 !== null && question1 !== "" && question2 !== null && question2 !== "" && question3 !== null && question3 !== "" && question4 !== null && question4 !== "" && question5 !== null && question5 !== "") {
        this.btnSecurityQuestions(true);
      } else {
        this.btnSecurityQuestions(false);
      }
    },
    /**
  * Method to toggle the proceed button visibility
  * @member of frmProfileManagementController
  * @param {Boolean} status - Status of the button to be enabled or disabled
  * @returns {void} - None
  * @throws {void} -None
  */
    btnSecurityQuestions: function(status) {
      if (status === true) {
        this.enableButton(this.view.settings.btnEditSecuritySettingsProceed);
      } else {
        this.disableButton(this.view.settings.btnEditSecuritySettingsProceed);
      }
    },

    /**
  * Method to set Question for lbxQuestion1
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setQuestionForListBox1: function() {
      var value = [];
      value = this.view.settings.lbxQuestion1.selectedKeyValue;
      var data = [];
      var selectedQues = {
        ques: "null",
        key: "null"
      };
      selectedQues.ques = this.selectedQuestions.ques[0];
      selectedQues.key = this.selectedQuestions.key[0];
      var position = 0;
      data = this.getQuestionsAfterSelected(value, selectedQues, position);
      var mainData = [],
          selectedData = [];
      selectedData = this.view.settings.lbxQuestion2.selectedKeyValue;
      mainData = this.flagManipulation(data, selectedData);
      this.view.settings.lbxQuestion2.masterData = mainData;
      var mainData2 = [],
          selectedData2 = [];
      selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
      mainData2 = this.flagManipulation(data, selectedData2);
      this.view.settings.lbxQuestion3.masterData = mainData2;
      var mainData3 = [],
          selectedData3 = [];
      selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
      mainData3 = this.flagManipulation(data, selectedData3);
      this.view.settings.lbxQuestion4.masterData = mainData3;
      var mainData4 = [],
          selectedData4 = [];
      selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
      mainData4 = this.flagManipulation(data, selectedData4);
      this.view.settings.lbxQuestion5.masterData = mainData4;
    },

    /**
  * Method to set Question for lbxQuestion4
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setQuestionForListBox4: function() {
      var value = [];
      value = this.view.settings.lbxQuestion4.selectedKeyValue;
      var data = [];
      var selectedQues = {
        ques: "null",
        key: "null"
      };
      selectedQues.ques = this.selectedQuestions.ques[3];
      selectedQues.key = this.selectedQuestions.key[3];
      var position = 3;
      data = this.getQuestionsAfterSelected(value, selectedQues, position);
      var mainData = [],
          selectedData = [];
      selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
      mainData = this.flagManipulation(data, selectedData);
      this.view.settings.lbxQuestion1.masterData = mainData;
      var mainData2 = [],
          selectedData2 = [];
      selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
      mainData2 = this.flagManipulation(data, selectedData2);
      this.view.settings.lbxQuestion3.masterData = mainData2;

      var mainData3 = [],
          selectedData3 = [];
      selectedData3 = this.view.settings.lbxQuestion2.selectedKeyValue;
      mainData3 = this.flagManipulation(data, selectedData3);
      this.view.settings.lbxQuestion2.masterData = mainData3;
      var mainData4 = [],
          selectedData4 = [];
      selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
      mainData4 = this.flagManipulation(data, selectedData4);
      this.view.settings.lbxQuestion5.masterData = mainData4;
    },
    /**
  * Method to set Question for lbxQuestion3
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setQuestionForListBox3: function() {
      var value = [];
      value = this.view.settings.lbxQuestion3.selectedKeyValue;
      var data = [];
      var selectedQues = {
        ques: "null",
        key: "null"
      };
      selectedQues.ques = this.selectedQuestions.ques[2];
      selectedQues.key = this.selectedQuestions.key[2]
      var position = 2;
      data = this.getQuestionsAfterSelected(value, selectedQues, position);
      var mainData = [],
          selectedData = [];
      selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
      mainData = this.flagManipulation(data, selectedData);
      this.view.settings.lbxQuestion1.masterData = mainData;
      var mainData2 = [],
          selectedData2 = [];
      selectedData2 = this.view.settings.lbxQuestion2.selectedKeyValue;
      mainData2 = this.flagManipulation(data, selectedData2);
      this.view.settings.lbxQuestion2.masterData = mainData2;
      var mainData3 = [],
          selectedData3 = [];
      selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
      mainData3 = this.flagManipulation(data, selectedData3);
      this.view.settings.lbxQuestion4.masterData = mainData3;
      var mainData4 = [],
          selectedData4 = [];
      selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
      mainData4 = this.flagManipulation(data, selectedData4);
      this.view.settings.lbxQuestion5.masterData = mainData4;
    },
    /**
  * Method to set Question for lbxQuestion2
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setQuestionForListBox2: function() {
      var value = [];
      value = this.view.settings.lbxQuestion2.selectedKeyValue;
      var data = [];
      var selectedQues = {
        ques: "null",
        key: "null"
      };
      selectedQues.ques = this.selectedQuestions.ques[1];
      selectedQues.key = this.selectedQuestions.key[1]
      var position = 1;
      data = this.getQuestionsAfterSelected(value, selectedQues, position);
      var mainData = [],
          selectedData = [];
      selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
      mainData = this.flagManipulation(data, selectedData);
      this.view.settings.lbxQuestion1.masterData = mainData;
      var mainData2 = [],
          selectedData2 = [];
      selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
      mainData2 = this.flagManipulation(data, selectedData2);
      this.view.settings.lbxQuestion3.masterData = mainData2;
      var mainData3 = [],
          selectedData3 = [];
      selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
      mainData3 = this.flagManipulation(data, selectedData3);
      this.view.settings.lbxQuestion4.masterData = mainData3;
      var mainData4 = [],
          selectedData4 = [];
      selectedData4 = this.view.settings.lbxQuestion5.selectedKeyValue;
      mainData4 = this.flagManipulation(data, selectedData4);
      this.view.settings.lbxQuestion5.masterData = mainData4;
    },
    /**
  * Method to set Question for lbxQuestion5
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setQuestionForListBox5: function() {
      var value = [];
      value = this.view.settings.lbxQuestion5.selectedKeyValue;
      var data = [];
      var selectedQues = {
        ques: "null",
        key: "null"
      };
      selectedQues.ques = this.selectedQuestions.ques[4];
      selectedQues.key = this.selectedQuestions.key[4];
      var position = 4;
      data = this.getQuestionsAfterSelected(value, selectedQues, position);
      var mainData = [],
          selectedData = [];
      selectedData = this.view.settings.lbxQuestion1.selectedKeyValue;
      mainData = this.flagManipulation(data, selectedData);
      this.view.settings.lbxQuestion1.masterData = mainData;
      var mainData2 = [],
          selectedData2 = [];
      selectedData2 = this.view.settings.lbxQuestion3.selectedKeyValue;
      mainData2 = this.flagManipulation(data, selectedData2);
      this.view.settings.lbxQuestion3.masterData = mainData2;
      var mainData3 = [],
          selectedData3 = [];
      selectedData3 = this.view.settings.lbxQuestion4.selectedKeyValue;
      mainData3 = this.flagManipulation(data, selectedData3);
      this.view.settings.lbxQuestion4.masterData = mainData3;
      var mainData4 = [],
          selectedData4 = [];
      selectedData4 = this.view.settings.lbxQuestion2.selectedKeyValue;
      mainData4 = this.flagManipulation(data, selectedData4);
      this.view.settings.lbxQuestion2.masterData = mainData4;
    },
    /**
  * Method to assign onBeginEditing function of tbxAnswer1
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onEditingAnswer1: function() {
      var data = [];
      data = this.view.settings.lbxQuestion1.selectedKeyValue;
      if (data[1] === "None") {
        this.view.settings.tbxAnswer1.maxTextLength = 0;
      } else {
        this.view.settings.tbxAnswer1.maxTextLength = 50;
      }
    },
    /**
  * Method to assign onBeginEditing function of tbxAnswer2
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onEditingAnswer2: function() {
      var data = [];
      data = this.view.settings.lbxQuestion2.selectedKeyValue;
      if (data[1] === "None") {
        this.view.settings.tbxAnswer2.maxTextLength = 0;
      } else {
        this.view.settings.tbxAnswer2.maxTextLength = 50;
      }
    },
    /**
  * Method to assign onBeginEditing function of tbxAnswer3
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onEditingAnswer3: function() {
      var data = [];
      data = this.view.settings.lbxQuestion3.selectedKeyValue;
      if (data[1] === "None") {
        this.view.settings.tbxAnswer3.maxTextLength = 0;
      } else {
        this.view.settings.tbxAnswer3.maxTextLength = 50;
      }
    },
    /**
  * Method to assign onBeginEditing function of tbxAnswer4
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onEditingAnswer4: function() {
      var data = [];
      data = this.view.settings.lbxQuestion4.selectedKeyValue;
      if (data[1] === "None") {
        this.view.settings.tbxAnswer4.maxTextLength = 0;
      } else {
        this.view.settings.tbxAnswer4.maxTextLength = 50;
      }
    },
    /**
  * Method to assign onBeginEditing function of tbxAnswer5
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onEditingAnswer5: function() {
      var data = [];
      data = this.view.settings.lbxQuestion5.selectedKeyValue;
      if (data[1] === "None") {
        this.view.settings.tbxAnswer5.maxTextLength = 0;
      } else {
        this.view.settings.tbxAnswer5.maxTextLength = 50;
      }
    },
    /**
  * Method for saving security questions
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onSaveSecurityQuestions: function(){
      var data=[{
        answer:"", question_id:""
      },{
        answer: "",
        question_id: ""
      },{
        answer: "",
        question_id: ""
      },{
        answer: "",
        question_id: ""
      },{
        answer: "",
        question_id: ""
      }];
      var quesData="";
      quesData=this.view.settings.lbxQuestion1.selectedKeyValue;
      data[0].answer=this.view.settings.tbxAnswer1.text;
      data[0].question_id=this.getQuestionID(quesData);
      quesData=this.view.settings.lbxQuestion2.selectedKeyValue;
      data[1].answer=this.view.settings.tbxAnswer2.text;
      data[1].question_id=this.getQuestionID(quesData);
      quesData=this.view.settings.lbxQuestion3.selectedKeyValue;
      data[2].answer=this.view.settings.tbxAnswer3.text;
      data[2].question_id=this.getQuestionID(quesData);
      quesData=this.view.settings.lbxQuestion4.selectedKeyValue;
      data[3].answer=this.view.settings.tbxAnswer4.text;
      data[3].question_id=this.getQuestionID(quesData);
      quesData=this.view.settings.lbxQuestion5.selectedKeyValue;
      data[4].answer=this.view.settings.tbxAnswer5.text;
      data[4].question_id=this.getQuestionID(quesData);
      return data;
    },
    /**
  * Method to get questionID from question
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    getQuestionID:function(quesData){
      var qData;
      for(var i=0;i<this.responseBackend.length;i++){
        if(quesData[1]===this.responseBackend[i].question){
          qData=this.responseBackend[i].SecurityID;
        }
      }
      return qData;
    },
    /**
  * Method to assign OnClick function of button Proceed in SecurityQuestions
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onProceedSQ:function(){
      CommonUtilities.showProgressBar(this.view);
      finaldata = this.onSaveSecurityQuestions();
      CommonUtilities.hideProgressBar(this.view);
      //this.presenter.saveSecurityQuestions(data,this.saveSQCallback,this.saveSQfailureCallback);
    },
    /**
  * Method to set email got from backend
  * @member of frmProfileManagementController
  * @param {JSON} response - response of email JSON got from backend
  * @returns {void} - None
  * @throws {void} -None
  */

    setEmailsToLbx: function(response) {
      var data = [];
      var index = this.view.settings.segAccounts.selectedIndex[1];
      var accData = this.view.settings.segAccounts.data[index];
      var list = [];
      if (accData.email !== null && accData.email !== " " && accData.email !== undefined) {
        list.push("assignedEmail");
        list.push(accData.email);
        data.push(list);
      }
      for (var i = 0; i < response.length; i++) {
        list = [];
        if (accData.email !== response[i].email) {
          list.push(response[i].id);
          list.push(response[i].email);
          data.push(list);
        }
      }

      this.view.settings.lbxEmailForReceiving.masterData = data;
      if (accData.email == null || accData.email === " " || accData.email === undefined) this.view.settings.lbxEmailForReceiving.selectedKey = "email";
      else this.view.settings.lbxEmailForReceiving.selectedKey = "assignedEmail";
    },

    /**
  * Method to set the error flow while editing prefered account
  * @member of frmProfileManagementController
  * @param {Boolean} errorScenario - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onPreferenceAccountEdit: function(errorScenario) {
      if (errorScenario === true) {
        this.view.settings.flxErrorEditAccounts.setVisibility(true);
        this.view.settings.lblErrorEditAccounts.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.weAreUnableToProcess");
      } else {
        this.view.settings.flxErrorEditAccounts.setVisibility(false);
      }
      var self=this;
      var index=this.view.settings.segAccounts.selectedIndex[1];
      var data=this.view.settings.segAccounts.data[index];
      if(data.nickName==data.lblAccountHolderValue){
        this.view.settings.tbxAccountNickNameValue.text ="";
      }else{

        this.view.settings.tbxAccountNickNameValue.text = data.nickName;}

      this.view.settings.lblFullNameValue.text = data.lblAccountHolderValue;
      this.view.settings.lblAccountTypeValue.text = data.lblAccountTypeValue;
      this.view.settings.lblAccountNumberValue.text = data.lblAccountNumberValue;
      this.view.settings.imgFavoriteEmailCheckBox.src = data.imgFavoriteCheckBox;
      this.view.settings.imgFavoriteEmailCheckBox.onClick = function() {
        self.toggleCheckBox(self.view.settings.imgFavoriteEmailCheckBox);
      };
      this.view.settings.imgEnableEStatementsCheckBox.src = data.imgEStatementCheckBox;
      this.view.imgTCContentsCheckbox.src = "unchecked_box.png";
      this.view.settings.btnTermsAndConditions.onClick=this.showTermsAndConditions;  
      this.presenter.userProfile.getUserEmail();
      if(this.view.settings.imgEnableEStatementsCheckBox.src==="checked_box.png"){
        this.view.settings.flxTCCheckBox.setVisibility(true);
        this.view.settings.imgTCContentsCheckbox.src = "checked_box.png";
        this.view.settings.flxPleaseNoteTheFollowingPoints.height = "160dp";

      }
      else{
        this.view.settings.flxTCCheckBox.setVisibility(false);
        this.view.settings.imgTCContentsCheckbox.src = "unchecked_box.png";
        this.view.settings.flxPleaseNoteTheFollowingPoints.height = "120dp";
      }
      if(this.view.settings.imgEnableEStatementsCheckBox.src==="checked_box.png"  && this.view.settings.imgTCContentsCheckbox.src!=="checked_box.png"){
        this.disableButton(this.view.settings.btnEditAccountsSave);
        this.disableButton(this.view.btnSave);
      }
      else{
        this.enableButton(this.view.settings.btnEditAccountsSave);
        this.view.settings.flxTCCheckBox.setVisibility(true);
        this.enableButton(this.view.btnSave);
      }
      this.view.settings.flximgEnableEStatementsCheckBox.onClick = function(){
        self.toggleCheckBoxEditAccounts(self.view.settings.imgEnableEStatementsCheckBox);
      };
      this.view.settings.btnEditAccountsSave.onClick=this.onSaveAccountDetails;
      this.view.settings.flxEditAccountsWrapper.setVisibility(true);
      this.view.settings.flxAccountsWrapper.setVisibility(false);
      if(CommonUtilities.getConfiguration("editNickNameAccountSettings")==="true"){
        this.view.settings.flxAccountNickName.setVisibility(true);
      }
      else{
        this.view.settings.flxAccountNickName.setVisibility(false);
      }
    },
    /**
  * Method to save details on click of save button
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    onSaveAccountDetails : function() {
      var self=this;
      var index=self.view.settings.segAccounts.selectedIndex[1];
      var data=self.view.settings.segAccounts.data[index];
      if(self.view.settings.tbxAccountNickNameValue.text ==""||self.view.settings.tbxAccountNickNameValue.text == null){
        data.nickName=data.lblAccountHolderValue;
      }
      else{
        data.nickName=self.view.settings.tbxAccountNickNameValue.text;
      }
      var data = {
        accountID: self.view.settings.lblAccountNumberValue.text,
        nickName: data.nickName,
        favouriteStatus: self.view.settings.imgFavoriteEmailCheckBox.src == "checked_box.png" ? 1 : 0,
        eStatementEnable: self.view.settings.imgEnableEStatementsCheckBox.src == "checked_box.png" ? 1 : 0,
        email:  self.view.settings.imgEnableEStatementsCheckBox.src == "checked_box.png" ? self.view.settings.lbxEmailForReceiving.selectedKeyValue[1] :  " ",
      };
      CommonUtilities.showProgressBar(self.view);
      self.presenter.savePreferredAccountsData(data);
    },
    /**
  * Method to assign details of the phone number
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- JSON of the details of the phone number got from backend
  * @returns {void} - None
  * @throws {void} -None
  */
    showPhoneDetails: function(viewModel) {
      this.view.settings.flxError.setVisibility(false);
      var phoneModel = viewModel.phone;
      var scopeObj = this;
      this.showDetailPhoneNumber();
      this.view.settings.btnEdit.setVisibility(phoneModel.isPrimary !== "1"); 
      this.view.settings.btnDelete.text = phoneModel.isPrimary === "1" ?  kony.i18n.getLocalizedString("i18n.ProfileManagement.Edit"): kony.i18n.getLocalizedString("i18n.ProfileManagement.Delete") ;      
      this.view.settings.lblTypeValue.text = CommonUtilities.changedataCase(phoneModel.type);
      this.view.settings.lblCountryValue.text = phoneModel.countryType === "domestic" ? "Domestic" : "International";
      this.view.settings.lblPhoneNumberValue.text = phoneModel.phoneNumber;
      this.view.settings.imgCheckBox3.src = phoneModel.isPrimary === "1" ? 'checked_box.png' : 'unchecked_box.png';
      this.view.settings.imgCheckBox4.src = phoneModel.receivePromotions === "1" ? 'checked_box.png' : 'unchecked_box.png';
      this.view.settings.flxOption3.setVisibility(phoneModel.isPrimary !== "1");
      this.view.settings.flxWarning.setVisibility(phoneModel.isPrimary !== "1");
      this.view.settings.flxCheckBox3.onClick = null;
      this.view.settings.lblExtensionUnEditable.text = phoneModel.extension;
      this.view.settings.flxCheckBox4.onClick = null;
      this.view.settings.btnEditPhoneNumberSave.onClick = this.presenter.userProfile.showUserPhones;
      this.setViewPhoneServicesData(viewModel.services, viewModel.phone);
      this.view.settings.btnDelete.onClick = phoneModel.isPrimary === "1" ?scopeObj.editPhone.bind(scopeObj, phoneModel) : scopeObj.deletePhone.bind(scopeObj, phoneModel);
      this.view.settings.btnEdit.onClick = scopeObj.editPhone.bind(scopeObj, phoneModel);
    },
    /**
  * Method to delete a particular phone number
  * @member of frmProfileManagementController
  * @param {JSON} phoneObj- JSON object of the phone to be deleted
  * @returns {void} - None
  * @throws {void} -None
  */
    deletePhone: function(phoneObj) {
      var scopeObj = this;
      var currForm = scopeObj.view;
      currForm.flxDeletePopUp.setVisibility(true);
      currForm.flxDeletePopUp.setFocus(true);
      currForm.lblDeleteHeader.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.Delete");
      currForm.lblConfirmDelete.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.deletePhoneNum");
      currForm.forceLayout();
      currForm.btnDeleteYes.onClick = function () {
        scopeObj.presenter.userProfile.deletePhone(phoneObj);            
        scopeObj.view.flxDelete.setFocus(true);
        scopeObj.view.flxDeletePopUp.setVisibility(false);
        scopeObj.view.forceLayout();
      }
    },
    /**
  * Method to edit a phone number
  * @member of frmProfileManagementController
  * @param {JSON} phoneObj - JSON object of the phone number to be edited
  * @returns {void} - None
  * @throws {void} -None
  */
    editPhone : function (phoneObj) {
      this.presenter.userProfile.editPhone(phoneObj);
    },
    /**
  * Method to set data  of all the phone numbers
  * @member of frmProfileManagementController
  * @param {JSON} phoneListViewModel - List of all the phone numbers got from backend
  * @returns {void} - None
  * @throws {void} -None
  */
    setPhoneSegmentData: function (phoneListViewModel) {
      var scopeObj = this;
      var dataMap = {
        "btnViewDetail": "btnViewDetail",
        "flxCheckBox1": "flxCheckBox1",
        "flxCheckBox2": "flxCheckBox2",
        "flxCheckBox3": "flxCheckBox3",
        "flxCheckBox4": "flxCheckBox4",
        "flxCollapsible": "flxCollapsible",
        "flxDeleteAction": "flxDeleteAction",
        "flxEdit": "flxEdit",
        "flxHome": "flxHome",
        "flxOption1": "flxOption1",
        "flxOption2": "flxOption2",
        "flxOption3": "flxOption3",
        "flxOption4": "flxOption4",
        "flxOptions": "flxOptions",
        "flxPhoneNumber": "flxPhoneNumber",
        "flxPrimary": "flxPrimary",
        "flxRow": "flxRow",
        "flxSelectedPhoneNumbers": "flxSelectedPhoneNumbers",
        "imgCheckBox1": "imgCheckBox1",
        "imgCheckBox2": "imgCheckBox2",
        "imgCheckBox3": "imgCheckBox3",
        "imgCheckBox4": "imgCheckBox4",
        "imgCollapsible": "imgCollapsible",
        "imgInfo": "imgInfo",
        "flxInfo": "flxInfo",
        "lblHome": "lblHome",
        "lblOption1": "lblOption1",
        "lblOption2": "lblOption2",
        "lblOption3": "lblOption3",
        "lblOption4": "lblOption4",
        "lblOptionSeperator": "lblOptionSeperator",
        "lblPhoneNumber": "lblPhoneNumber",
        "lblPleaseChoose": "lblPleaseChoose",
        "lblPrimary": "lblPrimary",
        "lblSelectedSeperator": "lblSelectedSeperator",
        "lblSeperator": "lblSeperator",
        "lblSeperatorActions": "lblSeperatorActions"
      };

      function getShowDetailListener (phoneModel) {
        return function () {
          scopeObj.presenter.userProfile.getPhoneDetails(phoneModel);
        }
      }

      var data = phoneListViewModel.map(function(phoneModel) {
        return {
          "imgCollapsible": "arrow_down.png",
          "lblHome": CommonUtilities.changedataCase(phoneModel.type),
          "lblPhoneNumber": phoneModel.phoneNumber,
          "lblPrimary": phoneModel.isPrimary === "1" ? "Primary" : " ",
          "imgInfo": phoneModel.isPrimary === "1" ? "info_grey.png" : "",
          "flxInfo": {
            onClick: scopeObj.toggleContextualMenuPhoneNumbers.bind(scopeObj)
          },
          "btnViewDetail": {
            text: "VIEW DETAIL",
            onClick: getShowDetailListener(phoneModel)
          },
          "lblPleaseChoose": "Please choose what service you like to use this number for:",
          "lblOption1": "Joint Savings Ã¢â‚¬Â¦. 1234",
          "lblOption2": "Joint Checking Ã¢â‚¬Â¦. 1234",
          "lblOption3": "Make my primary phone number",
          "lblOption4": "Allow to recieve text messages",
          "imgCheckBox1": "checked_box.png",
          "imgCheckBox2": "checked_box.png",
          "imgCheckBox3": "checked_box.png",
          "imgCheckBox4": "checked_box.png",
          "lblSelectedSeperator": "lblSelectedSeperator",
          "lblSeperator": "lblSeperator",
          "lblSeperatorActions": "lblSeperatorActions",
          "lblOptionSeperator": "lblOptionSeperator",
          "template": "flxPhoneNumbers"
        };
      })
      this.view.settings.segPhoneNumbers.widgetDataMap = dataMap;
      this.view.settings.segPhoneNumbers.setData(data);
      this.view.forceLayout();
    },
    /**
  * Method to assign initial action on onclick of widgets
  * @member of frmProfileManagementController
  * @param {void} services - Array of accounts
  * @returns {void} phoneObj - JSON object of phone number
  * @throws {void} -None
  */
    setViewPhoneServicesData: function (services, phoneObj) {
      var scopeObj = this;
      function getServiceToggleListener (account, index) {
        return function (){
          var data = scopeObj.view.settings.segEditPhoneNumberOption1.data[index];
          data.imgCheckBox = data.imgCheckBox === "checked_box.png" ? "unchecked_box.png" : "checked_box.png";
          scopeObj.view.settings.segEditPhoneNumberOption1.setDataAt(data, index);
          // if (data.imgCheckBox === "checked_box.png") {
          //   scopeObj.presenter.userProfile.attachPhoneToAccount(account.id, phoneObj.id);
          // }
          // else {
          //   scopeObj.presenter.userProfile.removePhoneFromAccount(account.id);
          // }
        }
      }

      var dataMap = {    
        "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
        "flxCheckbox": "flxCheckbox",
        "imgCheckBox": "imgCheckBox",
        "lblAccounts": "lblAccounts"
      };

      var data = services.map(function (account, index) {
        return  {
          "id": account.id,
          "imgCheckBox": account.selected ? "checked_box.png" : "unchecked_box.png",
          "lblAccounts": account.name,
          "flxCheckbox": {
            // "onClick": getServiceToggleListener(account, index)
          }
        }
      }) 
      this.view.settings.segEditPhoneNumberOption1.widgetDataMap = dataMap;
      this.view.settings.segEditPhoneNumberOption1.setData(data);
      this.view.forceLayout();
    },
    /**
  * Method to set phone number according to the services
  * @member of frmProfileManagementController
  * @param {JSON} services - Array of accounts
  * @returns {void} - None
  * @throws {void} -None
  */
    setEditPhoneServicesData: function (services) {
      var scopeObj = this;
      function getServiceToggleListener (account, index) {
        return function (){
          var data = scopeObj.view.settings.segEditPhoneNumberOption1.data[index];
          data.imgCheckBox = data.imgCheckBox === "checked_box.png" ? "unchecked_box.png" : "checked_box.png";
          scopeObj.view.settings.segEditPhoneNumberOption1.setDataAt(data, index);
        }
      }

      var dataMap = {    
        "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
        "flxCheckbox": "flxCheckbox",
        "imgCheckBox": "imgCheckBox",
        "lblAccounts": "lblAccounts"
      };

      var data = services.map(function (account, index) {
        return  {
          "id": account.id,
          "imgCheckBox": account.selected ? "checked_box.png" : "unchecked_box.png",
          "lblAccounts": account.name,
          "flxCheckbox": {
            "onClick": getServiceToggleListener(account, index)
          }
        }
      }) 
      this.view.settings.segEditPhoneNumberOption1.widgetDataMap = dataMap;
      this.view.settings.segEditPhoneNumberOption1.setData(data);
      this.view.forceLayout();
    },
    toggleContextualMenuPhoneNumbers : function() {
      var index = this.view.settings.segPhoneNumbers.selectedIndex[1];
      var scrollpos=this.view.contentOffsetMeasured;
      var hgt = ((index)*51)+105;
      if(this.view.settings.AllForms1.isVisible === false)
        this.view.settings.AllForms1.isVisible = true;
      else
        this.view.settings.AllForms1.isVisible = false;
      this.view.settings.AllForms1.height = hgt + "dp";
      this.view.settings.AllForms1.left = "50%";
      this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoPhone");
      this.view.forceLayout();
      this.view.setContentOffset(scrollpos);
    },
    toggleContextualMenuEmail : function() {
      var index = this.view.settings.segEmailIds.selectedIndex[1];
      var scrollpos=this.view.contentOffsetMeasured;
      var hgt = ((index)*51)+105;
      if(this.view.settings.AllForms1.isVisible === false)
        this.view.settings.AllForms1.isVisible = true;
      else
        this.view.settings.AllForms1.isVisible = false;
      this.view.settings.AllForms1.height = hgt + "dp";
      this.view.settings.AllForms1.left = "50%";
      this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoEmail");
      this.view.forceLayout();
      this.view.setContentOffset(scrollpos);
    },
    toggleContextualMenuAddress : function() {
      var index = this.view.settings.segAddresses.selectedIndex[1];
      var scrollpos=this.view.contentOffsetMeasured;
      var hgt = ((index)*51)+105;
      if(this.view.settings.AllForms1.isVisible === false)
        this.view.settings.AllForms1.isVisible = true;
      else
        this.view.settings.AllForms1.isVisible = false;
      this.view.settings.AllForms1.height = hgt + "dp";
      this.view.settings.AllForms1.left = "60%";
      this.view.settings.AllForms1.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.msgInfoAddress");
      this.view.forceLayout();
      this.view.setContentOffset(scrollpos);
    },
    /**
  * Method to edit phone number options
  * @member of frmProfileManagementController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
    setEditPhoneNoOption2SegmentData: function () {
      var dataMap = {    
        "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
        "flxCheckbox": "flxCheckbox",
        "imgCheckBox": "imgCheckBox",
        "lblAccounts": "lblAccounts"
      };
      var data = [{
        "imgCheckBox": "checked_box.png",
        "lblAccounts": "Joint Checking ….4323  "
      },
                  {
                    "imgCheckBox": "unchecked_box.png",
                    "lblAccounts": "Joint Checking ….4323 "
                  },
                  {
                    "imgCheckBox": "unchecked_box.png",
                    "lblAccounts": "Joint Checking ….4323 "
                  }
                 ];
      this.view.settings.segEditPhoneNumbersOption2.widgetDataMap = dataMap;
      this.view.settings.segEditPhoneNumbersOption2.setData(data);
      this.view.forceLayout();
    },
    /**
  * Method to set the mapping to save phone data
  * @member of frmProfileManagementController
  * @param {JSON} services - Array of accounts
  * @returns {void} - None
  * @throws {void} -None
  */
    setAddPhoneServicesData: function (services) {
      var scopeObj = this;
      var dataMap = {    
        "flxAccountsPhoneNumbers": "flxAccountsPhoneNumbers",
        "flxCheckbox": "flxCheckbox",
        "imgCheckBox": "imgCheckBox",
        "lblAccounts": "lblAccounts"
      };

      function getCheckBoxListener (serviceObj, index) {
        return function () {
          var data = scopeObj.view.settings.segAddPhoneNumbersOption1.data[index];
          data.imgCheckBox = data.imgCheckBox === "checked_box.png" ? "unchecked_box.png" : "checked_box.png";
          scopeObj.view.settings.segAddPhoneNumbersOption1.setDataAt(data, index);
        }
      }
      var data = services.map (function (serviceObj, index) {
        return {
          "id": serviceObj.id,
          "imgCheckBox": "unchecked_box.png",
          "lblAccounts": serviceObj.name,
          "flxCheckbox": {
            "onClick": getCheckBoxListener(serviceObj,index)
          }
        }
      })
      this.view.settings.segAddPhoneNumbersOption1.widgetDataMap = dataMap;
      this.view.settings.segAddPhoneNumbersOption1.setData(data);
      this.view.forceLayout();
    },
  };
});