define({ 

   showSelectedRow: function(){
    var previousIndex;
    var index = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.selectedIndex;
    var rowIndex = index[1];
    var data = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.data;
      for(i=0;i<data.length;i++)
         {
           if(i==rowIndex)
             {
               data[i].imgDropdown = "arrow_up.png";
               data[i].template = "flxPFMBulkUpdateTransactionsSelected";
             }
           else
             {
               data[i].imgDropdown = "arrow_down.png";
               data[i].template = "flxPFMBulkUpdateTransaction";
             }
         }  
     kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.setData(data);
  },
   toggleCheckBox: function(){
        var index = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.selectedIndex[1];   
        var data = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.data;
        for(i=0;i<data.length;i++)
         {
           if(i==index)
             {
               if(data[i].imgCheckBox === "unchecked_box.png") {
                  data[i].imgCheckBox = "checked_box.png";
               }
               else {
                data[i].imgCheckBox = "unchecked_box.png";
               }
         }  
      	kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.setData(data);
        }   
    },
  

 });