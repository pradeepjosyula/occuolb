define(['CommonUtilities','CSRAssistUI'],function(CommonUtilities,CSRAssistUI){
  return { 
    willUpdateUI: function(viewModel){
      var self=this;
      if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
      if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
      if(viewModel.invalidCredential) this.errorWhileValidating();
      if(viewModel.errorVerifyAccount) this.errorWhileValidating(viewModel.errorVerifyAccount);
      if(viewModel.verifyAccounts) this.showVerifyAccountScreen(viewModel.verifyAccounts);
      if (viewModel.validateByCredential) this.showAcknowledgment();
      if (viewModel.validateByTrialDeposit) this.showNextSteps();
      for(var i=0;i<viewModel.length;i++){
        if (viewModel[i].sideMenu) this.updateHamburgerMenu(viewModel[i].sideMenu,viewModel);
        if (viewModel[i].topBar) this.updateTopBar(viewModel[i].topBar);        
        if(viewModel[i].internalAccount){    //To populate the infromation into the verifyAccount Form

          var Verifydata= viewModel[i].internalAccount;

          this.view.lblBillerValue.text = Verifydata.bankName;
          this.view.lblAccountTypeValue.text = Verifydata.accountType;
          this.view.lblAccountNumberValue.text = Verifydata.accountNumber;
          this.view.lblBeneficiaryNameValue.text =Verifydata.beneficiaryName;
          this.view.lblAccountNickNameValue.text = Verifydata.nickName ;
          this.view.rtxBillerAddress.isVisible=false;
          this.view.lblBankAddress.isVisible=false;
          this.view.flxsemicolon.isVisible=false;

          this.internalAccountAcknowledgement(Verifydata.beneficiaryName);//Call to set an Acknowledgement 
          this.view.acknowledgment.lblRefrenceNumberValue.text =Verifydata.referenceNo; //set the reference no which was retrieved from service
          this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountHeading');
          this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountAcknowledge')}]); 
          this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
          this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountAcknowledge");

          this.view.forceLayout();

        }
        if(viewModel[i].domesticAccount){    //To populate the infromation into the verifyAccount Form
          var Verifydata= viewModel[i].domesticAccount;

          this.view.lblBillerValue.text = Verifydata.bankName;
          this.view.lblAccountTypeValue.text = Verifydata.accountType;
          this.view.lblAccountNumberValue.text = Verifydata.accountNumber;
          this.view.lblBeneficiaryNameValue.text =Verifydata.beneficiaryName;
          this.view.lblAccountNickNameValue.text = Verifydata.nickName ;
          this.view.verifyByCredential.flxCheckbox.onClick=function(){
            CommonUtilities.toggleCheckBox(self.view.verifyByCredential.imgChecbox);
            self.checkSkin();
          };
          this.view.rtxBillerAddress.isVisible=false;
          this.view.lblBankAddress.isVisible=false;
          this.view.flxsemicolon.isVisible=false;
          this.view.btnConfirm.toolTip= kony.i18n.getLocalizedString("i18n.transfers.verifyAndAdd");
          this.normalSkin();
          this.view.btnConfirm.onClick = function () {
            self.checkVerificationOptions();
          };
          this.view.btnCancel.toolTip= kony.i18n.getLocalizedString("i18n.transfers.Cancel");
          this.view.btnCancel.onClick=function(){
            self.presenter.cancelTransaction(this);
          }
          this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount'); 
          if(Verifydata.ownerImage === "unchecked_box.png"){
            this.internalAccountAcknowledgement(Verifydata.beneficiaryName);//Call to set an Acknowledgement 
            this.view.acknowledgment.lblRefrenceNumberValue.text =Verifydata.referenceNo; //set the reference no which was retrieved from service
            this.view.acknowledgment.lblRefrenceNumber.setVisibility(true);
            this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(true);
            this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.nonKonyBankAcknowledge')}]); 
            this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
            this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.nonKonyBankAcknowledge");
            this.view.forceLayout();
          }
          else{
            this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
            this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
            this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");
            this.normalSkin();
            if(kony.application.getCurrentForm().id === "frmConfirmAccount")
              this.showVerificationOptions();

            this.view.forceLayout();
          }


        }
        if(viewModel[i].internationalAccount){    //To populate the infromation into the verifyAccount Form
          var Verifydata= viewModel[i].internationalAccount;

          this.view.lblBillerValue.text = Verifydata.bankName;
          this.view.lblAccountTypeValue.text = Verifydata.accountType;
          this.view.lblAccountNumberValue.text = Verifydata.accountNumber;
          this.view.lblBeneficiaryNameValue.text =Verifydata.beneficiaryName;
          this.view.lblAccountNickNameValue.text = Verifydata.nickName ;
          this.view.verifyByCredential.flxCheckbox.onClick=function(){
            CommonUtilities.toggleCheckBox(self.view.verifyByCredential.imgChecbox);
            self.checkSkin();
          };
          this.view.rtxBillerAddress.isVisible=false;
          this.view.lblBankAddress.isVisible=false;
          this.view.flxsemicolon.isVisible=false;
          this.view.btnConfirm.toolTip= kony.i18n.getLocalizedString("i18n.transfers.verifyAndAdd");
          this.normalSkin();
          this.view.btnConfirm.onClick = function () {
            self.checkVerificationOptions();
          };
          this.view.btnCancel.onClick=function(){
            self.presenter.cancelTransaction(this);
          }
          this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
          if(Verifydata.ownerImage === "unchecked_box.png"){
            this.internalAccountAcknowledgement(Verifydata.beneficiaryName);//Call to set an Acknowledgement 
            this.view.acknowledgment.lblRefrenceNumberValue.text =Verifydata.referenceNo; //set the reference no which was retrieved from service
            this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.nonKonyBankAcknowledge')}]); 
            this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
            this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.nonKonyBankAcknowledge");

            this.view.forceLayout();          
          }
          else{
            this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
            this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
            this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");
            this.normalSkin();
            if(kony.application.getCurrentForm().id === "frmConfirmAccount")
              this.showVerificationOptions();
            this.view.forceLayout();            
          }

        }
      }
      CommonUtilities.hideProgressBar(this.view);
      this.AdjustScreen();
      this.view.forceLayout();
    },

    postShowVerifyAccount: function(){   
      this.AdjustScreen();
    },
    //UI Code
    AdjustScreen: function() {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
          this.view.flxFooter.top = mainheight + diff + "dp";
        else
          this.view.flxFooter.top = mainheight + "dp";
        this.view.forceLayout();
      } else {
        this.view.flxFooter.top = mainheight + "dp";
        this.view.forceLayout();
      }
    },  

    showNextSteps: function(){
      this.view.acknowledgment.setVisibility(false);
      this.view.verifyByCredential.setVisibility(false);
      this.view.verifyNextSteps.setVisibility(true);
      this.view.verifyBankAccount.setVisibility(false);
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.Transfers.BacktoMakeTransfer");
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.Transfers.BacktoMakeTransfer");
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString('i18n.transfers.addAnotherAccount');
      this.view.flxVerifyActions.setVisibility(false);
      this.view.flxAcknowledgeActions.setVisibility(true);    
      this.view.forceLayout();
    },
    /**
     * Method to show error if validation of username and password fails
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None.
     * @throws Exception - None.
     */
    errorWhileValidating: function(errmsg){
      this.view.verifyByCredential.tbxUsername.text="";
      this.view.verifyByCredential.tbxPassword.text="";
      if(errmsg){
        this.view.verifyByCredential.rtxWarning.text=errmsg;
      }else{
        this.view.verifyByCredential.rtxWarning.text=kony.i18n.getLocalizedString("i18n.transfers.incorrectCredentials");
      }
      CommonUtilities.setCheckboxState(false,this.view.verifyByCredential.imgChecbox);
      this.view.verifyByCredential.flxWarning.setVisibility(true);
      this.blockedSkin();
      this.view.forceLayout();
    },

    /**
     * Method to show acknowledgment
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None.
     * @throws Exception - None.
     */
    showAcknowledgment: function(){
      this.internalAccountAcknowledgement(this.view.lblAccountNickNameValue.text);//Call to set an Acknowledgement 
      this.view.verifyByCredential.flxWarning.setVisibility(false);
      this.view.acknowledgment.setVisibility(true);
      this.view.verifyByCredential.setVisibility(false);
      this.view.verifyNextSteps.setVisibility(false);
      this.view.verifyBankAccount.setVisibility(false);
      this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.transfers.make_transfer");
      this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString('i18n.common.transferToThatPayee');
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString('i18n.transfers.addAnotherAccount');
      this.view.flxVerifyActions.setVisibility(false);
      this.view.flxAcknowledgeActions.setVisibility(true);
      this.view.acknowledgment.lblRefrenceNumber.setVisibility(false);
      this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(false);
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountHeading');
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountAcknowledge')}]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountAcknowledge");	
      this.view.forceLayout();
    },

    /**
     * Method to show Verify by Credentials or verify by Trial Deposit
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None.
     * @throws Exception - None.
     */
    showVerificationOptions: function(){
      this.view.acknowledgment.setVisibility(false);
      this.view.verifyByCredential.setVisibility(false);
      this.normalSkin();
      if(kony.onlineBanking.configurations.getConfiguration('verifyByCredentials')==="true"){
        this.view.verifyNextSteps.setVisibility(false);
        this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
        this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
        this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");	

        this.view.verifyBankAccount.setVisibility(true);
        this.view.flxVerifyActions.setVisibility(true);
        this.view.flxAcknowledgeActions.setVisibility(false);

      } 
      else{
        CommonUtilities.showProgressBar(this.view);
        var data;
        data = {
          ExternalAccountNumber: this.view.lblAccountNumberValue.text,
        };
        this.presenter.addToVerifyAccount(data);
      }
    },

    /**
     * Method to handle verify by credentials using username and password
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None.
     * @throws Exception - None.
     */
    checkVerificationOptions: function(){
      this.view.verifyByCredential.lblHeading.text=kony.i18n.getLocalizedString("i18n.transfers.nextStepsToVerifyAccount");
      this.view.verifyByCredential.Label0f96e5b8496f040.text=kony.i18n.getLocalizedString("i18n.transfers.verifyCredentials");
      this.view.verifyByCredential.tbxUsername.onKeyUp=function(){};
      this.view.verifyByCredential.tbxPassword.onKeyUp=function(){};
      this.view.verifyByCredential.tbxUsername.onEndEditing=function(){};
      this.view.verifyByCredential.tbxPassword.onEndEditing=function(){};
      if (this.view.verifyBankAccount.isVisible) {
        if (this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey !== null) {
          this.view.acknowledgment.setVisibility(false);
          this.view.flxVerifyActions.setVisibility(true);
          this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
          this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
          this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");	

          this.blockedSkin();
          this.view.verifyByCredential.setVisibility(true);
          this.view.verifyByCredential.tbxPassword.secureTextEntry = true;
          this.view.verifyByCredential.lblUsername.text=kony.i18n.getLocalizedString("i18n.common.EnterUsername")+":";
          this.view.verifyByCredential.lblPassword.text=kony.i18n.getLocalizedString("i18n.common.EnterPassword")+":";

          this.view.verifyByCredential.tbxUsername.placeholder=kony.i18n.getLocalizedString("i18n.common.EnterUsername");
          this.view.verifyByCredential.tbxPassword.placeholder=kony.i18n.getLocalizedString("i18n.common.EnterPassword");



          this.view.flxAcknowledgeActions.setVisibility(false);
          this.view.verifyNextSteps.setVisibility(false);
          this.view.verifyBankAccount.setVisibility(false);
        } else if (this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey !== null) {
          var data;
          CommonUtilities.showProgressBar(this.view);
          data = {
            ExternalAccountNumber: this.view.lblAccountNumberValue.text,
          };
          this.presenter.addToVerifyAccount(data);
          this.view.acknowledgment.setVisibility(false);
          this.view.verifyByCredential.setVisibility(false);
          this.view.flxVerifyActions.setVisibility(true);
          this.view.flxAcknowledgeActions.setVisibility(false);
          this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.nonKonyBankAcknowledge')}]);   
          this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
          this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.nonKonyBankAcknowledge");
          this.view.verifyNextSteps.setVisibility(true);
          this.view.verifyBankAccount.setVisibility(false);
        } 

      }
      else if (this.view.verifyByCredential.isVisible) {
        var data;
        if (this.view.verifyByCredential.tbxUsername.text !== null) {
          if (this.view.verifyByCredential.tbxPassword.text !== null) {
            if(CommonUtilities.isChecked(this.view.verifyByCredential.imgChecbox))
              data = {
                userName: this.view.verifyByCredential.tbxUsername.text,
                password: this.view.verifyByCredential.tbxPassword.text
              };
              CommonUtilities.showProgressBar(this.view);
            this.presenter.verifyCredentials(data);
          }
        }
      }
    },

    /**
     * Method to make New Transfer
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None.
     * @throws Exception - None.
     */
    makeNewTransfer: function(){
      if(this.view.btnMakeTransfer.text === kony.i18n.getLocalizedString("i18n.transfers.make_transfer"))
        this.presenter.showTransferScreen({accountTo:this.view.lblAccountNumberValue.text});
      else
        this.presenter.showTransferScreen();
    },

    /**
     * Method to show verify account screen using amounts sent by bank
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None. 
     * @throws Exception - None.
     */
    isBlank:function(input){
      var pattern=/\s/;   //to check blank inputs
      return input.match(pattern);
    },
    /**
     * Method to Validate the inputs by user
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None. 
     * @throws Exception - None.
     */
    validateDepositInput: function() {
      var deposit1 = this.view.verifyByCredential.tbxUsername.text;
      var deposit2 = this.view.verifyByCredential.tbxPassword.text;
      var chkBoxState=CommonUtilities.isChecked(this.view.verifyByCredential.imgChecbox);
      if (this.isBlank(deposit1) || this.isBlank(deposit2)|| !deposit1 || !deposit2 || !chkBoxState ) {
        this.blockedSkin();
      } else {
        this.normalSkin();
      }
    },

    /**
     * Method to validate amount fields
     * @memberof frmVerifyAccountController
     * @param {JSON} widget - widget reference
     * @returns {void} - None. 
     * @throws Exception - None.
     */
    setAmountValidation:function(widget){
      var self=this;
      widget.onKeyUp=function(){
          CommonUtilities.validateAmountFieldKeyPress(widget);
          self.validateDepositInput();
      }
      widget.onEndEditing=function(){
          CommonUtilities.validateAndFormatAmount(widget);   
          self.validateDepositInput();
      }
    },

    /**
     * Method to show verify account screen using amounts sent by bank
     * @memberof frmVerifyAccountController
     * @param {JSON} selectedRow - Details of selected account
     * @returns {void} - None. 
     * @throws Exception - None.
     */
    showVerifyAccountScreen: function(selectedRow) {
      var self=this;
      this.hideAll();
      this.view.flxAcknowledgeActions.setVisibility(false);
      this.view.flxVerifyActions.setVisibility(true);
      //Setting blocked skin for confirm button for first visit
      this.blockedSkin();
      this.setAmountValidation(this.view.verifyByCredential.tbxUsername);
      this.setAmountValidation(this.view.verifyByCredential.tbxPassword);
      
      CommonUtilities.setCheckboxState(false,this.view.verifyByCredential.imgChecbox);
      this.view.verifyByCredential.flxCheckbox.onClick=function(){
        CommonUtilities.toggleCheckBox(self.view.verifyByCredential.imgChecbox);
        self.validateDepositInput();
      };

      this.view.verifyByCredential.setVisibility(true);
      this.view.lblBillerValue.text = selectedRow.lblBankName;
      this.view.lblAccountTypeValue.text = selectedRow.lblAccountTypeValue;
      this.view.lblAccountNumberValue.text = selectedRow.txtAccountNumber.text;
      this.view.lblBeneficiaryNameValue.text = selectedRow.lblSeparator;
      this.view.lblAccountNickNameValue.text = selectedRow.lblAccountName;
      this.view.verifyByCredential.tbxPassword.secureTextEntry = false;
      this.view.verifyByCredential.lblUsername.text = kony.i18n.getLocalizedString("i18n.transfers.depositAmount1");
      this.view.verifyByCredential.lblPassword.text = kony.i18n.getLocalizedString("i18n.transfers.depositAmount2");
      this.view.verifyByCredential.tbxUsername.placeholder = kony.i18n.getLocalizedString("i18n.transfers.EnterAmountHere");
      this.view.verifyByCredential.tbxPassword.placeholder = kony.i18n.getLocalizedString("i18n.transfers.EnterAmountHere");
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.transfers.verifyAccountLC");
      this.view.verifyByCredential.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.verifyByProvidingDeposits");
      this.view.verifyByCredential.Label0f96e5b8496f040.text = selectedRow.lblBankName;
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts"),
        callback: function() {
          self.presenter.getExternalAccounts();
        }
      }, {
        text: kony.i18n.getLocalizedString('i18n.transfers.verifyAccount')
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.btnBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.external_accounts");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.verifyAccount");
      this.view.verifyByCredential.tbxUsername.left = 0;
      this.view.verifyByCredential.tbxUsername.left = "40%";
      this.view.verifyByCredential.tbxPassword.left = 0;
      this.view.verifyByCredential.tbxPassword.left = "40%";
      this.view.lblHeadingVerify.text = kony.i18n.getLocalizedString("i18n.transfers.YourTransactionDetails");
      this.view.verifyByCredential.lblUsername.skin = "sknlbl727272LatoReg15px";
      this.view.verifyByCredential.lblPassword.skin = "sknlbl727272LatoReg15px";
      this.view.breadcrumb.btnBreadcrumb2.skin = "sknBtnLato3343A813PxBg0";
      this.view.btnConfirm.toolTip= kony.i18n.getLocalizedString("i18n.transfers.verifyAndAdd");
      this.view.btnConfirm.onClick = function() {
        self.onBtnConfirmVerifyAndAdd({
          "accountNumber": selectedRow.txtAccountNumber.text,
          "firstDeposit": self.view.verifyByCredential.tbxUsername.text,
          "secondDeposit": self.view.verifyByCredential.tbxPassword.text,
          "nickName": selectedRow.lblAccountName,
          "accountType": selectedRow.lblAccountTypeValue
        });
      };
      this.view.btnCancel.toolTip= kony.i18n.getLocalizedString("i18n.transfers.Cancel");
      this.view.btnCancel.onClick=function(){
        self.presenter.getExternalAccounts();
      };
    },

    onBtnConfirmVerifyAndAdd: function(selectedRow){
      this.presenter.confirmVerifyAndAdd(selectedRow);
      this.view.breadcrumb.btnBreadcrumb2.skin="sknBtnLato3343A813PxBg0";
    },

    normalSkin: function(){
      this.view.btnConfirm.skin="sknBtnNormalLatoFFFFFF15Px";
      this.view.btnConfirm.hoverSkin="sknBtnNormalLatoFFFFFF15Px";
      this.view.btnConfirm.focusSkin="sknBtnNormalLatoFFFFFF15Px";
      this.view.btnConfirm.setEnabled(true);
    },

    blockedSkin: function(){
      this.view.btnConfirm.skin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.btnConfirm.hoverSkin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.btnConfirm.focusSkin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.btnConfirm.setEnabled(false);
    },

    /**
     * Method to check username and password validation for enable/disable of button
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None.
     * @throws Exception - None.
     */
    checkSkin: function(){
      if (this.view.verifyByCredential.tbxUsername.text !== "") {
        if (this.view.verifyByCredential.tbxPassword.text !== "") {
          if(CommonUtilities.isChecked(this.view.verifyByCredential.imgChecbox))
            this.normalSkin();
          else 
            this.blockedSkin();
        }
        else 
          this.blockedSkin();
      }
      else 
        this.blockedSkin();

    },

    /**
     * Method gets triggered at preshow of form to set data
     * @memberof frmVerifyAccountController
     * @param {void} - None
     * @returns {void} - None.
     * @throws Exception - None.
     */
    preshowfrmVerifyAccount: function () {
      var scopeObj = this;
      if(CommonUtilities.isCSRMode()){
         CSRAssistUI.setCSRAssistConfigurations(scopeObj,'frmVerifyAccount');
      }
      this.view.verifyByCredential.flxWarning.setVisibility(false);
      this.view.verifyByCredential.tbxUsername.text="";
      this.view.verifyByCredential.tbxPassword.text="";
      
      this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = null;
      this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = "rbg1";
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      CommonUtilities.setCheckboxState(false,this.view.verifyByCredential.imgChecbox);
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        kony.print("btn logout pressed");
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainContainer.frame.height;
        scopeObj.view.flxLogout.height = height + "dp";
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
    },

    internalAccountAcknowledgement: function (AccountName) {
      this.view.acknowledgment.lblTransactionMessage.text = "'"+AccountName+"'"+kony.i18n.getLocalizedString("i18n.common.HasBeenAddedSuccessfully");
      this.acknowledge();
    },

    externalAccount: function () {
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.transfers.transfer")
      },
                                              {
                                                text: kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify")
                                              }
                                             ]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");

      this.switchActions('verify');
    },

    switchActions: function (type) {
      switch (type) {
        case 'acknowledge':
          this.view.flxAcknowledgeActions.setVisibility(true);
          this.view.btnMakeTransfer.text = kony.i18n.getLocalizedString("i18n.Transfers.BacktoMakeTransfer");
          this.view.flxVerifyActions.setVisibility(false);
          break;
        case 'acknowledgement':
          this.view.flxAcknowledgeActions.setVisibility(true);
          this.view.flxVerifyActions.setVisibility(false);
          break;
        case 'verify':
          this.view.flxAcknowledgeActions.setVisibility(false);
          this.view.flxVerifyActions.setVisibility(true);
          break;
      }
    },


    verifyByDeposits: function () {
      this.hideAll();
      this.view.verifyNextSteps.setVisibility(true);
      this.switchActions('acknowledge');
    },

    verifyByCredentials: function () {
      this.hideAll();
      this.blockedSkin();
      this.view.verifyByCredential.setVisibility(true);
      this.switchActions('verify');
    },

    acknowledge: function () {
      this.hideAll();
      this.view.acknowledgment.setVisibility(true);
      this.switchActions('acknowledgement');
    },

    nextStep: function () {
      if (this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey !== null) {
        this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = null;
        this.verifyByDeposits();
        return;
      }
      if (this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey !== null) {
        this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = null;
        this.verifyByCredentials();
        return;
      }
      if (this.view.verifyByCredential.isVisible) {
        this.acknowledge();
        return;
      }
    },  

    setActionsOnCredential: function (){
      this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = "rbg1";
      this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = null;
    },

    setActionsOnTrialDeposit: function(){
      this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = null;
      this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = "rbg1";
    },

    setActions: function () {
      this.view.verifyBankAccount.rbtVerifyBankCredential.onSelection = function () {
        this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = null;
      }.bind(this);
      this.view.verifyBankAccount.rbtVerifyByDeposits.onSelection = function () {
        this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = null;
      }.bind(this);
    },

    hideAll: function () {
      this.view.verifyBankAccount.setVisibility(false);
      this.view.verifyByCredential.setVisibility(false);
      this.view.verifyNextSteps.setVisibility(false);
      this.view.acknowledgment.setVisibility(false);
    },

    updateHamburgerMenu: function (sideMenuModel,viewModel) {
      this.view.customheader.initHamburger(sideMenuModel);
      if(viewModel){
        for(var i=0;i<viewModel.length;i++){
          if(viewModel[i].internalAccount){
            this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Add Kony Accounts");        
          }else{
            this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Add Non Kony Accounts");
          }
        }
      }
    },
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);    
    } 

  };
});