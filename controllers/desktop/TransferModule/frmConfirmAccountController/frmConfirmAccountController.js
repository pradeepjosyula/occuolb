define(['commonUtilities','OLBConstants','CSRAssistUI'],function(commonUtilities,OLBConstants,CSRAssistUI){
  return {

  populateInVerify: function(){
    var newPayeeData= { 
      "bankName": this.view.confirmDialogAccounts.keyValueBankName.lblValue.text,
      "accountType": this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text,
      "accountNumber": this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text,
      "beneficiaryName": this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text,
      "nickName":this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text
    }; 
    this.presenter.createBankPayee(this,newPayeeData);
  },
  addAccount: function () {
    kony.olb.utils.showProgressBar(this.view);
    this.presenter.navigateToVerifyAccount(this);
  },
 
  willUpdateUI: function(viewModel){
     kony.olb.utils.hideProgressBar(this.view);
    for(var i=0;i<viewModel.length;i++){
     if (viewModel[i].sideMenu) this.updateHamburgerMenu(viewModel[i].sideMenu,viewModel);
     if (viewModel[i].topBar) this.updateTopBar(viewModel[i].topBar);
     if(viewModel[i].internalAccount){
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);      
      var data= viewModel[i].internalAccount;
	  this.view.confirmDialogAccounts.flxDileveryBy.isVisible=false;
        this.view.flxEditHolisticAccount.isVisible=false;
        this.view.flxConfirmContainer.isVisible=true;
	  this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountHeading');
	  this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfer')}, {text:kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountConfirm")}]);
	  this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountConfirm");
      this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.bankName;
      this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = data.accountType;
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.accountNumber;
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =data.beneficiaryName;
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.nickName ;
      this.view.confirmDialogAccounts.keyValueCountryName.isVisible=false;
    }
    if(viewModel[i].domesticAccount){
      var data= viewModel[i].domesticAccount;
	  this.view.confirmDialogAccounts.flxDileveryBy.isVisible=false;
        this.view.flxEditHolisticAccount.isVisible=false;
        this.view.flxConfirmContainer.isVisible=true;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true); 
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.routingNumber');      
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = data.routingNumber;
	  this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfer')}, {text:kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm")}]);
	this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm");
      this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.bankName;
      this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = data.accountType;
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.accountNumber;
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =data.beneficiaryName;
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.nickName ;
      this.view.confirmDialogAccounts.keyValueCountryName.isVisible=false;
    }
  if(viewModel[i].internationalAccount){
       var data= viewModel[i].internationalAccount;
	   this.view.confirmDialogAccounts.flxDileveryBy.isVisible=false;
        this.view.flxEditHolisticAccount.isVisible=false;
        this.view.flxConfirmContainer.isVisible=true;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);                  
       this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.swiftCode');
       this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = data.swiftCode;
       this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
       this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfer')}, {text:kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm")}]);
	    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
        this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm");
       this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.bankName;
       this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = data.accountType;
       this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.accountNumber;
       this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =data.beneficiaryName;
       this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.nickName ;
       this.view.confirmDialogAccounts.keyValueCountryName.isVisible=false;
  }}
    this.presenter.dataForNextForm(viewModel);
    this.AdjustScreen();
    this.view.forceLayout();
  },

preShowfrmConfirmAccount: function () {
    var scopeObj = this;
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
    this.view.confirmDialogAccounts.confirmButtons.btnConfirm.setEnabled(true);
    this.view.confirmDialogAccounts.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmAccountDetails');
    this.view.confirmDialogAccounts.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyAccountDetails');
    this.view.confirmDialogAccounts.confirmButtons.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.common.CancelAccount');  
    this.view.flxCancelPopup.setVisibility(false);
    this.view.customheader.forceCloseHamburger();
   
      //Below method enables and disables CSR functionality
      if(commonUtilities.isCSRMode()){
         CSRAssistUI.setCSRAssistConfigurations(scopeObj,"frmConfirmAccount");
      }
    //SET ACTIONS:
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainContainer.frame.height + scopeObj.view.flxFooter.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";      
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CustomPopup.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };

  },
  
  postShowfrmConfirmAccount: function(){
   this.AdjustScreen();
  },
     //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  },         
  
  showCancelPopup:function()
  {
        var height_to_set = 140 + this.view.flxMainContainer.frame.height + this.view.flxFooter.frame.height;
        this.view.flxCancelPopup.height = height_to_set + "dp";
        this.view.flxCancelPopup.isVisible = true;
        this.view.flxCancelPopup.setFocus(true);
        this.view.forceLayout();
  },
  hideCancelPopup:function()
  {
    this.view.flxCancelPopup.isVisible = false;
        this.view.forceLayout();
  },
  
  updateHamburgerMenu: function (sideMenuModel,viewModel) {
    this.view.customheader.initHamburger(sideMenuModel,"Transfers","Add Kony Accounts");
    for(var i=0;i<viewModel.length;i++){
      if(viewModel[i].internalAccount){
        this.view.customheader.customhamburger.activateMenu("Transfers", "Add Kony Accounts");        
      }else{
        this.view.customheader.customhamburger.activateMenu("Transfers", "Add Non Kony Accounts");
      }
    }
  },
  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  } 

}
});