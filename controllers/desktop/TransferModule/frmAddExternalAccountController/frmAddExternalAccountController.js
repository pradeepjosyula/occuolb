define(['commonUtilities'],function(commonUtilities){
  return { willUpdateUI: function(viewModel) {
      if (viewModel == undefined) {
        this.resetExternalAccount();
      } else {
        for (var i = 0; i < viewModel.length; i++) {
          if (viewModel[i].sideMenu) this.updateHamburgerMenu(viewModel[i].sideMenu);
          if (viewModel[i].topBar) this.updateTopBar(viewModel[i].topBar);
          if (viewModel[i].sameAccounts) {
            this.setSameBankAccounts(viewModel[i].sameAccounts);
          }
          if (viewModel[i].serverInternationalError) {
            this.resetExternalAccount();
            this.view.externalAccount.rtxDowntimeWarningInternational.text = viewModel[i].serverInternationalError;
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(true);
          }

          if (viewModel[i].domesticAccounts) {
            this.setDomesticAccounts(viewModel[i].domesticAccounts);
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
          }
          if (viewModel[i].internationalAccounts) {
            this.setInternationalAccounts(viewModel[i].internationalAccounts);
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
          }

          if (viewModel[i].serverDomesticError) {
            this.resetExternalAccount();
            this.view.externalAccount.rtxDowntimeWarningDomestic.text = viewModel[i].serverDomesticError;
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(true);
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
          }
        }
      }
      commonUtilities.hideProgressBar(this.view);
      this.AdjustScreen();
    },
    setDomesticAccounts: function(data) {
      var self = this;
      this.resetExternalAccount();
      this.view.breadcrumb.setBreadcrumbData([
        { text: kony.i18n.getLocalizedString("i18n.transfers.transfer") },
        {
          text: kony.i18n.getLocalizedString(
            "i18n.transfers.addNonKonyBankAccountDomestic"
          )
        }
      ]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountDomestic");
      this.view.externalAccount.flxDomesticDetailsKA.setVisibility(true);
      this.view.externalAccount.flxInternationalDetailsKA.setVisibility(false);
      this.view.externalAccount.btnInternationalAccountKA.skin = "sknBtnAccountSummaryUnselected";
      this.view.externalAccount.btnDomesticAccountKA.skin = "sknBtnAccountSummarySelected";
      this.view.previouslyAddedList.segkonyaccounts.widgetDataMap = { CopyLabel0a25bb187ff174b: "CopyLabel0a25bb187ff174b", CopyLabel0f686ea9ea96c4e: "CopyLabel0f686ea9ea96c4e", Label0j5951129f89142: "Label0j5951129f89142", btnviewdetails: "btnviewdetails", flxsegment: "flxsegment", flxsegmentseperator: "flxsegmentseperator", lblAccount: "lblAccount", lblbank: "lblbank", btnMakeTransfer: "btnMakeTransfer" };
      var segData = [];
      function getMapping(context) {
        if (context.btnMakeTransfer.isVerified == "true") {
          return { text: "MAKE TRANSFER", onClick: function() {
              self.onBtnMakeTransfer();
            } };
        } else {
          return { text: "PENDING", onClick: function() {}, skin: "sknlblLato5daf0b15px" };
        }
      }
      for (var i = 0; i < data.length; i++) {
        if (data[i] !== undefined && data[i].isInternationalAccount === "false" && data[i].isSameBankAccount === "false") {
          segData.push({
            btnviewdetails: {
              text: "View Details",
              onClick: function() {
                self.onBtnViewDetails();
              }
            },
            btnMakeTransfer: getMapping({ btnMakeTransfer: data[i] }),
            lblAccount: data[i].nickName,
            lblbank: data[i].bankName,
            CopyLabel0a25bb187ff174b: {
              text: data[i].accountNumber,
              isVisible: true
            },
            CopyLabel0f686ea9ea96c4e: {
              text: data[i].accountType,
              isVisible: true
            },
            Label0j5951129f89142: {
              text: "wwq",
              isVisible: true
            }
          });
        }
      }
      var len = segData.length;

      this.view.externalAccount.btnAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnAddAccountKA.setEnabled(false);
      if (len !== 0) {
        this.view.previouslyAddedList.flxheader.setVisibility(true);
        this.view.previouslyAddedList.flxseperator.setVisibility(true);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
         //hiding visiblity of separator for last account
        segData[len - 1].Label0j5951129f89142.isVisible = false;
        segData[len - 1].CopyLabel0f686ea9ea96c4e.isVisible = false;
        segData[len - 1].CopyLabel0a25bb187ff174b.isVisible = false;
        
        this.view.previouslyAddedList.segkonyaccounts.setData(segData);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "20px";
      } else {
        this.view.previouslyAddedList.flxheader.setVisibility(false);
        this.view.previouslyAddedList.flxseperator.setVisibility(false);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "0px";
      }
      this.view.forceLayout();
    },
    postshowAddExternalAccount: function() {
      this.AdjustScreen();
      this.setFlowActions();
    }, 
   //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  },        
          setFlowActions: function() {
      var scopeObj = this;
      this.view.externalAccount.flxInfoKA.onClick = function() {
        if (scopeObj.view.externalAccount.AllForms.isVisible === false) scopeObj.view.externalAccount.AllForms.setVisibility(true);
        else scopeObj.view.externalAccount.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.externalAccount.AllForms.flxCross.onClick = function() {
        scopeObj.view.externalAccount.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.externalAccount.flxIntInfoKA.onClick = function() {
        if (scopeObj.view.externalAccount.AllForms.isVisible === false) scopeObj.view.externalAccount.AllForms.setVisibility(true);
        else scopeObj.view.externalAccount.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.previouslyAddedList.flxinfo.onClick = function() {
        scopeObj.view.AllForms.top = scopeObj.view.previouslyAddedList.frame.height - 10 + "dp";
        if (scopeObj.view.AllForms.isVisible === false) scopeObj.view.AllForms.setVisibility(true);
        else scopeObj.view.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.AllForms.flxCross.onClick = function() {
        scopeObj.view.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.previouslyAddedList.flxaddkonyaccnt.onClick = function() {
        commonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.showSameBankAccounts();
      };
    }, setInternationalAccounts: function(data) {
      var self = this;
      this.resetExternalAccount();
      this.view.breadcrumb.setBreadcrumbData([
        { text: kony.i18n.getLocalizedString("i18n.transfers.transfer") },
        {
          text: kony.i18n.getLocalizedString(
            "i18n.transfers.addNonKonyBankAccountInternational"
          )
        }
      ]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountInternational");
      this.view.externalAccount.flxDomesticDetailsKA.setVisibility(false);
      this.view.externalAccount.flxInternationalDetailsKA.setVisibility(true);
      this.view.externalAccount.btnInternationalAccountKA.skin = "sknBtnAccountSummarySelected";
      this.view.externalAccount.btnDomesticAccountKA.skin = "sknBtnAccountSummaryUnselected";

      this.view.previouslyAddedList.segkonyaccounts.widgetDataMap = { CopyLabel0a25bb187ff174b: "CopyLabel0a25bb187ff174b", CopyLabel0f686ea9ea96c4e: "CopyLabel0f686ea9ea96c4e", Label0j5951129f89142: "Label0j5951129f89142", btnviewdetails: "btnviewdetails", flxsegment: "flxsegment", flxsegmentseperator: "flxsegmentseperator", lblAccount: "lblAccount", lblbank: "lblbank", btnMakeTransfer: "btnMakeTransfer" };
      var segData = [];
      function getMapping(context) {
        if (context.btnMakeTransfer.isVerified == "true") {
          return { text: "MAKE TRANSFER", onClick: function() {
              self.onBtnMakeTransfer();
            } };
        } else {
          return { text: "PENDING", onClick: function() {}, skin: "sknlblLato5daf0b15px" };
        }
      }
      for (var i = 0; i < data.length; i++) {
        if (data[i] !== undefined && data[i].isInternationalAccount === "true" && data[i].isSameBankAccount !== "true") {
          segData.push({
            btnviewdetails: {
              text: "View Details",
              onClick: function() {
                self.onBtnViewDetails();
              }
            },
            btnMakeTransfer: getMapping({ btnMakeTransfer: data[i] }),
            lblAccount: data[i].nickName,
            lblbank: data[i].bankName,
            CopyLabel0a25bb187ff174b: {
              text: data[i].accountNumber,
              isVisible: true
            },
            CopyLabel0f686ea9ea96c4e: {
              text: data[i].accountType,
              isVisible: true
            },
            Label0j5951129f89142: {
              text: "wwq",
              isVisible: true
            }
          });
        }
      }
      var len = segData.length;
      this.view.externalAccount.btnIntAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnIntAddAccountKA.setEnabled(false);
      if (len !== 0) {
        this.view.previouslyAddedList.flxheader.setVisibility(true);
        this.view.previouslyAddedList.flxseperator.setVisibility(true);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
        //hiding visiblity of separator for last account
        segData[len - 1].Label0j5951129f89142.isVisible = false;
        segData[len - 1].CopyLabel0f686ea9ea96c4e.isVisible = false;
        segData[len - 1].CopyLabel0a25bb187ff174b.isVisible = false;
        
        this.view.previouslyAddedList.segkonyaccounts.setData(segData);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "20px";
      } else {
        this.view.previouslyAddedList.flxheader.setVisibility(false);
        this.view.previouslyAddedList.flxseperator.setVisibility(false);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "0px";
      }
      this.view.forceLayout();
    },
    addInternationalAccount: function() {
      var scopeObj = this;
      var errMsg;
      var data = { ownerImage: this.view.externalAccount.imgcheckboxchecked.src, swiftCode: this.view.externalAccount.tbxIntSwiftCodeKA.text, bankName: this.view.externalAccount.tbxIntBankNameKA.text, accountType: this.view.externalAccount.lbxIntAccountTypeKA.selectedKeyValue[1], accountNumber: this.view.externalAccount.tbxIntAccountNumberKA.text, reAccountNumber: this.view.externalAccount.tbxIntAccountNumberAgainKA.text, beneficiaryName: commonUtilities.changedataCase(this.view.externalAccount.tbxIntBeneficiaryNameKA.text), nickName: commonUtilities.changedataCase(this.view.externalAccount.tbxIntAccountNickNameKA.text) };
      if ((data.nickName === null || data.nickName === "") && (data.beneficiaryName !== null || data.beneficiaryName !== "")) data.nickName = data.beneficiaryName;

      if (!(data.accountNumber === data.reAccountNumber)) {
        this.view.externalAccount.tbxIntAccountNumberKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.externalAccount.tbxIntAccountNumberAgainKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.externalAccount.btnIntAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.setEnabled(false);
        errMsg = kony.i18n.getLocalizedString("i18n.transfers.accNoDoNotMatch");
        this.errorInternational({ errorInternational: errMsg });
      } else {
        this.view.externalAccount.tbxIntAccountNumberKA.skin = "sknTextBox72";
        this.view.externalAccount.tbxIntAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.externalAccount.btnIntAddAccountKA.skin = "sknBtnNormalLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.Hoverkin = "sknBtnHoverLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.setEnabled(true);
        this.view.externalAccount.lblWarningInt.setVisibility(false);
        this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
        this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
        this.presenter.addInternationalAccount(this, data);
      }
    },
    addDomesticAccount: function() {
      var scopeObj = this;
      var data = { routingNumber: this.view.externalAccount.tbxRoutingNumberKA.text, bankName: this.view.externalAccount.tbxBankNameKA.text, accountType: this.view.externalAccount.lbxAccountTypeKA.selectedKeyValue[1], accountNumber: this.view.externalAccount.tbxAccountNumberKA.text, reAccountNumber: this.view.externalAccount.tbxAccountNumberAgainKA.text, beneficiaryName: commonUtilities.changedataCase(this.view.externalAccount.tbxBeneficiaryNameKA.text), nickName: commonUtilities.changedataCase(this.view.externalAccount.tbxAccountNickNameKA.text), ownerImage: this.view.externalAccount.imgchecked.src };

      if ((data.nickName === null || data.nickName === "") && (data.beneficiaryName !== null || data.beneficiaryName !== "")) data.nickName = data.beneficiaryName;
      if (!(data.accountNumber === data.reAccountNumber)) {
        this.view.externalAccount.tbxAccountNumberKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.externalAccount.tbxAccountNumberAgainKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.externalAccount.btnAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.setEnabled(false);
        errMsg = kony.i18n.getLocalizedString("i18n.transfers.accNoDoNotMatch");
        this.errorDomestic({ errorDomestic: errMsg });
      } else {
        this.view.externalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
        this.view.externalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.externalAccount.btnAddAccountKA.skin = "sknBtnNormalLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.setEnabled(true);
        this.view.externalAccount.lblWarning.setVisibility(false);
        this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
        this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
        this.presenter.addDomesticAccount(this, data);
      }
    },
    errorInternational: function(viewModel) {
      this.view.externalAccount.btnIntAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
      this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
      this.view.externalAccount.lblWarningInt.text = viewModel.errorInternational;
      this.view.externalAccount.lblWarningInt.setVisibility(true);
    },
    errorDomestic: function(viewModel) {
      this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
      this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
      this.view.externalAccount.lblWarning.text = viewModel.errorDomestic;
      this.view.externalAccount.lblWarning.setVisibility(true);
    },
    validateDomesticFields: function() {
      var errMsg;
      var data = { checkBox: this.view.externalAccount.flxcheckboximg.src, routingNumber: this.view.externalAccount.tbxRoutingNumberKA.text, bankName: this.view.externalAccount.tbxBankNameKA.text, accountNumber: this.view.externalAccount.tbxAccountNumberKA.text, reAccountNumber: this.view.externalAccount.tbxAccountNumberAgainKA.text, beneficiaryName: this.view.externalAccount.tbxBeneficiaryNameKA.text, nickName: this.view.externalAccount.tbxAccountNickNameKA.text, ownerImage: this.view.externalAccount.imgchecked.src };

      if (data.accountNumber === null || data.beneficiaryName === null || data.bankName === null || data.routingNumber === null || data.accountNumber === "" || data.beneficiaryName === "" || data.bankName === "" || data.routingNumber === "") {
        this.view.externalAccount.btnAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.setEnabled(false);
      } else {
        this.view.externalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
        this.view.externalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.externalAccount.btnAddAccountKA.skin = "sknBtnNormalLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.setEnabled(true);
      }
    },
    validateInternationalFields: function() {
      var errMsg;
      var data = { swiftCode: this.view.externalAccount.tbxIntSwiftCodeKA.text, bankName: this.view.externalAccount.tbxIntBankNameKA.text, accountNumber: this.view.externalAccount.tbxIntAccountNumberKA.text, reAccountNumber: this.view.externalAccount.tbxIntAccountNumberAgainKA.text, beneficiaryName: this.view.externalAccount.tbxIntBeneficiaryNameKA.text, nickName: this.view.externalAccount.tbxIntAccountNickNameKA.text };

      if (data.accountNumber === null || data.beneficiaryName === null || data.bankName === null || data.routingNumber === null || data.accountNumber === "" || data.beneficiaryName === "" || data.bankName === "" || data.routingNumber === "") {
        this.view.externalAccount.btnIntAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.setEnabled(false);
      } else {
        this.view.externalAccount.tbxIntAccountNumberKA.skin = "sknTextBox72";
        this.view.externalAccount.tbxIntAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.externalAccount.btnIntAddAccountKA.skin = "sknBtnNormalLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
        this.view.externalAccount.btnIntAddAccountKA.setEnabled(true);
      }
    },
    resetExternalAccount: function() {
      //for Domestic Accounts
      this.view.externalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
      this.view.externalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
      this.view.externalAccount.btnAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnAddAccountKA.setEnabled(false);
      this.view.externalAccount.tbxRoutingNumberKA.text = "";
      this.view.externalAccount.tbxBankNameKA.text = "";
      this.view.externalAccount.tbxAccountNumberKA.text = "";
      this.view.externalAccount.tbxAccountNumberAgainKA.text = "";
      this.view.externalAccount.tbxBeneficiaryNameKA.text = "";
      this.view.externalAccount.tbxAccountNickNameKA.text = "";
      this.view.externalAccount.btnAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
      this.view.externalAccount.lblWarning.setVisibility(false);
      //for International Accounts
      this.view.externalAccount.tbxIntAccountNumberKA.skin = "sknTextBox72";
      this.view.externalAccount.tbxIntAccountNumberAgainKA.skin = "sknTextBox72";
      this.view.externalAccount.btnIntAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnIntAddAccountKA.setEnabled(false);
      this.view.externalAccount.tbxIntSwiftCodeKA.text = "";
      this.view.externalAccount.tbxIntBankNameKA.text = "";
      this.view.externalAccount.tbxIntAccountNumberKA.text = "";
      this.view.externalAccount.tbxIntAccountNumberAgainKA.text = "";
      this.view.externalAccount.tbxIntBeneficiaryNameKA.text = "";
      this.view.externalAccount.tbxIntAccountNickNameKA.text = "";
      this.view.externalAccount.btnIntAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnIntAddAccountKA.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnIntAddAccountKA.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
      this.view.externalAccount.lblWarningInt.setVisibility(false);
    },
    preshowFrmAddAccount: function() {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      if (kony.onlineBanking.configurations.getConfiguration("addExternalAccount") === "false") {
        this.view.externalAccount.imgchecked.src = "unchecked_box.png";
        this.view.externalAccount.imgcheckboxchecked.src = "unchecked_box.png";
        this.view.externalAccount.flxCheckboxKA.setVisibility(false);
        this.view.externalAccount.flxcheckboxinternational.setVisibility(false);
        this.view.externalAccount.flxIntSwiftCodeKA.top = "-40dp";
      }
      this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Add Kony Accounts");
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.forceLayout();
      this.view.customheader.headermenu.btnLogout.onClick = function() {
        kony.print("btn logout pressed");
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopup.btnYes.onClick = function() {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance()
          .getModuleManager()
          .getModule("AuthModule");
        context = { action: "Logout" };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function() {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function() {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
    },
    onBtnMakeTransfer: function() {
      commonUtilities.showProgressBar(this.view);
      var index = this.view.previouslyAddedList.segkonyaccounts.selectedIndex[1];
      var data = this.view.previouslyAddedList.segkonyaccounts.data[index];
      kony.print(JSON.stringify(data));
      this.presenter.showTransferScreen({
        accountTo: data.CopyLabel0a25bb187ff174b.text
      });
    },
    onBtnViewDetails: function() {
      commonUtilities.showProgressBar(this.view);
      var index = this.view.previouslyAddedList.segkonyaccounts.selectedIndex[1];
      var data = this.view.previouslyAddedList.segkonyaccounts.data[index];
      kony.print(JSON.stringify(data));
      this.presenter.showExternalAccounts({
        getSelectedExternalAccount: data.CopyLabel0a25bb187ff174b.text
      });
    },
    toggleCheckBox: function() {
      if (this.view.externalAccount.imgchecked.src === "checked_box.png") {
        this.view.externalAccount.imgchecked.src = "unchecked_box.png";
      } else {
        this.view.externalAccount.imgchecked.src = "checked_box.png";
      }
    },
    toggleChekBoxNonDomestic: function() {
      if (this.view.externalAccount.imgcheckboxchecked.src === "checked_box.png") {
        this.view.externalAccount.imgcheckboxchecked.src = "unchecked_box.png";
      } else {
        this.view.externalAccount.imgcheckboxchecked.src = "checked_box.png";
      }
    },
    updateHamburgerMenu: function(sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel, "Transfers", "Add Non Kony Accounts");
    }, updateTopBar: function(topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    } };
});