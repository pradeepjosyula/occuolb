define(['commonUtilities'],function(commonUtilities){
  return {

    willUpdateUI:function(viewModel){
      if(viewModel==undefined){
        this.resetInternalAccount();
      }
      else{
        for(var i=0;i<viewModel.length;i++){
          if (viewModel[i].sideMenu) this.updateHamburgerMenu(viewModel[i].sideMenu);
          if (viewModel[i].topBar) this.updateTopBar(viewModel[i].topBar);
          if (viewModel[i].internalAccount){
            this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
          }
          if(viewModel[i].sameAccounts){
            this.resetInternalAccount();
            this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
            this.setSameBankAccounts(viewModel[i].sameAccounts);
          }
          if(viewModel[i].serverError){
            this.resetInternalAccount();
            this.view.internalAccount.rtxDowntimeWarning.text = viewModel[i].serverError;
            this.view.internalAccount.flxDowntimeWarning.setVisibility(true);
          }
        }
        commonUtilities.hideProgressBar(this.view);
      }
       this.AdjustScreen();
    },

    setDetailsToModify:function(data){

    },

    postShowAddInternalAccount: function(){
      this.AdjustScreen();
      this.setFlowActions(); 
    },
     //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  },     
    setFlowActions: function() {
      var scopeObj = this;
      this.view.internalAccount.flxInfoKA.onClick = function() {
        if(scopeObj.view.internalAccount.AllForms.isVisible === false) {
        scopeObj.view.internalAccount.AllForms.setVisibility(true);
        if(scopeObj.view.internalAccount.flxDowntimeWarning.isVisible === true)
           scopeObj.view.internalAccount.AllForms.top = "257dp";
        else
           scopeObj.view.internalAccount.AllForms.top = "180dp";   
        }
        else
        scopeObj.view.internalAccount.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.internalAccount.AllForms.flxCross.onClick = function() {
        scopeObj.view.internalAccount.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.previouslyAddedList.flxinfo.onClick = function() 
      {
        scopeObj.view.AllForms.top = scopeObj.view.flxPreviouslyAddedAcc.frame.height-10+"dp";
        if(scopeObj.view.AllForms.isVisible === false)
        scopeObj.view.AllForms.setVisibility(true);
        else
        scopeObj.view.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.AllForms.flxCross.onClick = function()
      {
        scopeObj.view.AllForms.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.previouslyAddedList.flxaddkonyaccnt.onClick=function(){
        commonUtilities.showProgressBar(scopeObj.view);
        scopeObj.presenter.showDomesticAccounts();
      }
    },

    setSameBankAccounts:function(data){
      var self=this;
      this.view.previouslyAddedList.segkonyaccounts.widgetDataMap={
        "CopyLabel0a25bb187ff174b": "CopyLabel0a25bb187ff174b",
        "CopyLabel0f686ea9ea96c4e": "CopyLabel0f686ea9ea96c4e",
        "Label0j5951129f89142": "Label0j5951129f89142",
        "btnviewdetails": "btnviewdetails",
        "flxsegment": "flxsegment",
        "flxsegmentseperator": "flxsegmentseperator",
        "lblAccount": "lblAccount",
        "lblbank": "lblbank",
        "btnMakeTransfer": "btnMakeTransfer"
      };
      var segData=[];
      function getMapping(context){
        if(context.btnMakeTransfer.isVerified==="true"){
          return {
            "text":kony.i18n.getLocalizedString("i18n.transfers.make_transfer"),
            "toolTip":kony.i18n.getLocalizedString("i18n.transfers.make_transfer"),
            "onClick":function () {
              self.onBtnMakeTransfer(); 
            }
          };
        }else{
          return {
            "text":kony.i18n.getLocalizedString("i18n.transfers.pending"),
            "toolTip":kony.i18n.getLocalizedString("i18n.transfers.pending"),
            "onClick":function () {},
            "skin":"sknlblLato5daf0b15px",
          };
        }
      }
      for(var i=0;i<(data.length);i++){
        if((data[i]!==undefined)&&(data[i].isSameBankAccount==="true")){
          segData.push({
            "btnviewdetails": {
              "text":kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
              "onClick":function () {
                self.onBtnViewDetails();
              }  
            },
            "btnMakeTransfer": getMapping({"btnMakeTransfer":data[i]}),
            "lblAccount":data[i].nickName,
            "lblbank":data[i].bankName,
            "CopyLabel0a25bb187ff174b": {
              text: data[i].accountNumber,
              isVisible: true
            },
            "CopyLabel0f686ea9ea96c4e": {
              text: data[i].accountType,
              isVisible: true
            },
            "Label0j5951129f89142": {
              text: "wwq",
              isVisible: true
            }
          });
        }
      }
      var len = segData.length;
           
      this.view.internalAccount.btnAddAccountKA.skin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.internalAccount.btnAddAccountKA.setEnabled(false);
      if (len !== 0) {
        this.view.previouslyAddedList.flxheader.setVisibility(true);
        this.view.previouslyAddedList.flxseperator.setVisibility(true);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(true);
        //hiding visiblity of separator for last account
        segData[len - 1].Label0j5951129f89142.isVisible = false;
        segData[len - 1].CopyLabel0f686ea9ea96c4e.isVisible = false;
        segData[len - 1].CopyLabel0a25bb187ff174b.isVisible = false;
        
        this.view.previouslyAddedList.segkonyaccounts.setData(segData);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "20px";
        
      } else {
        this.view.previouslyAddedList.flxheader.setVisibility(false);
        this.view.previouslyAddedList.flxseperator.setVisibility(false);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "0px";
      }
      this.view.forceLayout();
    },

    addInternalAccount:function(){
      var scopeObj=this;  
      var data= { 
        "bankName": this.view.internalAccount.lblBankNameValue.text,
        "accountType": this.view.internalAccount.lbxAccountTypeKA.selectedKeyValue[1],
        "accountNumber": this.view.internalAccount.tbxAccountNumberKA.text,
        "reAccountNumber":this.view.internalAccount.tbxAccountNumberAgainKA.text,
        "beneficiaryName":commonUtilities.changedataCase(this.view.internalAccount.tbxBeneficiaryNameKA.text),
        "nickName":commonUtilities.changedataCase(this.view.internalAccount.tbxAccountNickNameKA.text)
      }; 
      var errMsg; 
      if((data.nickName === null || data.nickName === "") && (data.beneficiaryName !== null || data.beneficiaryName !== ""))
        data.nickName = data.beneficiaryName;
      if(!(data.accountNumber === data.reAccountNumber)){
        this.view.internalAccount.tbxAccountNumberKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.internalAccount.tbxAccountNumberAgainKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.internalAccount.btnAddAccountKA.skin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.setEnabled(false);
        errMsg = kony.i18n.getLocalizedString("i18n.transfers.accNoDoNotMatch");
        this.errorInternal({"errorInternal":errMsg});
      }
      else{
        this.view.internalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
        this.view.internalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.internalAccount.btnAddAccountKA.skin="sknBtnNormalLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnHoverLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnFocusLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.setEnabled(true);
        this.view.internalAccount.lblWarning.setVisibility(false);
        this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
        this.presenter.addInternalAccount(this,data);
      }
    },

    validateInternalError:function(){
      var errMsg;
      var data= { 
        "bankName": this.view.internalAccount.lblBankNameValue.text,
        "accountNumber": this.view.internalAccount.tbxAccountNumberKA.text,
        "reAccountNumber":this.view.internalAccount.tbxAccountNumberAgainKA.text,
        "beneficiaryName": this.view.internalAccount.tbxBeneficiaryNameKA.text,
        "nickName":this.view.internalAccount.tbxAccountNickNameKA.text
      }; 

      if(data.accountNumber === null || data.beneficiaryName === null || data.bankName === null || data.accountNumber === "" || data.beneficiaryName === "" || data.bankName === ""){
        this.view.internalAccount.btnAddAccountKA.skin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.setEnabled(false);
      }
      else{
        this.view.internalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
        this.view.internalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.internalAccount.btnAddAccountKA.skin="sknBtnNormalLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnHoverLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnFocusLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.setEnabled(true);
      }
    },  


    errorInternal:function(viewModel){
      this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
      this.view.internalAccount.lblWarning.text = viewModel.errorInternal;
      this.view.internalAccount.lblWarning.setVisibility(true);
    },

    resetInternalAccount:function(){
      this.view.internalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
      this.view.internalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
      this.view.internalAccount.tbxAccountNumberKA.text="";
      this.view.internalAccount.tbxBeneficiaryNameKA.text="";
      this.view.internalAccount.tbxAccountNickNameKA.text="";
      this.view.internalAccount.tbxAccountNumberAgainKA.text="";
      this.view.internalAccount.lblWarning.setVisibility(false);
      this.view.internalAccount.btnAddAccountKA.skin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
    },

    preshowFrmAddAccount: function () {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.internalAccount.lblBankNameValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.bankName;
      this.view.internalAccount.flxInternationalDetailsKA.setVisibility(false);
      this.view.customheader.topmenu.flxMenu.skin="slFbox";
      this.view.customheader.topmenu.flxaccounts.skin="slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin="sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      this.view.customheader.topmenu.flxaccounts.skin="sknHoverTopmenu7f7f7pointer"; 
      this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
      this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
      this.view.customheader.customhamburger.activateMenu("Transfers", "Add Kony Accounts");
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")}, 
                                              {text:kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccount")}]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccount");											
      this.view.forceLayout();
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        scopeObj.view.flxLogout.height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainContainer.frame.height + scopeObj.view.flxFooter.frame.height;
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };

    },

    addInternationalAccount: function () {
      this.view.externalAccount.flxDomesticDetailsKA.setVisibility(false);
      this.view.externalAccount.flxInternationalDetailsKA.setVisibility(true);
      this.view.externalAccount.btnInternationalAccountKA.skin = "sknBtnAccountSummarySelected";
      this.view.externalAccount.btnDomesticAccountKA.skin = "sknBtnAccountTypeUnselected";
      this.view.forceLayout();
    },

    addDomesticAccount: function () {
      this.view.externalAccount.flxDomesticDetailsKA.setVisibility(true);
      this.view.externalAccount.flxInternationalDetailsKA.setVisibility(false);
      this.view.externalAccount.btnInternationalAccountKA.skin = "sknBtnAccountSummarySelected";
      this.view.forceLayout();
    },
    
    onBtnMakeTransfer:function(){
      commonUtilities.showProgressBar(this.view);
      var index = this.view.previouslyAddedList.segkonyaccounts.selectedIndex[1];
      var data = this.view.previouslyAddedList.segkonyaccounts.data[index];
      this.presenter.showTransferScreen({"accountTo":data.CopyLabel0a25bb187ff174b.text});
    },

    onBtnViewDetails:function(){
      commonUtilities.showProgressBar(this.view);
      var index = this.view.previouslyAddedList.segkonyaccounts.selectedIndex[1];
      var data = this.view.previouslyAddedList.segkonyaccounts.data[index];
      this.presenter.showExternalAccounts({'getSelectedExternalAccount':data.CopyLabel0a25bb187ff174b.text});
    },

    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel,"Transfers","Add Kony Accounts");
    },

    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    } 
  };
});