define(['commonUtilities','OLBConstants'],function(commonUtilities,OLBConstants){
  return {
  initialLoadingDone: false,
  transfersViewModel: {
    transactionsData: [],
    first:0,
    last:10
  },
  
  /**Create View Model For Transactions for a segment
   * @member  frmTransfersController
   * @param  {Array} data Array of transactions model 
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   * @returns {void} None
   * @throws {void} None
   */

  getNewTransfersData: function (data, onCancelCreateTransfer) {
    this.transfersViewModel.transactionsData = data.map(this.createNewTransfersData.bind(this, onCancelCreateTransfer))
  },

   /**Calls Repeat Transaction Method forming a transaction object
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   * @returns None
   * @throws None
   */

  repeatTransaction : function(onCancelCreateTransfer){
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    var selectedData = data[index];
    var TransactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('Transactions');
    var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var transactionObject = new transactionsModel({
      "transactionId": selectedData.lblReferenceNumberValue
    });
    transactionObject.amount = (selectedData.lblAmount).slice(1).replace(/,/g,"");
    transactionObject.scheduledDate = selectedData.lblDate;
    transactionObject.fromAccountNumber = selectedData.fromAccountNumber;
    transactionObject.notes = selectedData.lblNoteValue;
    transactionObject.numberOfRecurrences = selectedData.lblRecurrenceValue;
    transactionObject.toAccountNumber = selectedData.toAccountNumber;
    transactionObject.toAccountName = selectedData.lblSendTo;
    transactionObject.fromAccountName = selectedData.lblFromAccountValue;
    transactionObject.transactionType = selectedData.transactionType;
    transactionObject.frequencyType = selectedData.frequencyType;
    transactionObject.ExternalAccountNumber = selectedData.externalAccountNumber;
    this.presenter.repeatTransfer(transactionObject, onCancelCreateTransfer);
    this.AdjustScreen();
  },

   /**Dummy function for View Report
   * @returns None
   * @throws None
   */

  viewTransactionReport : function(){
    this.view.flxTransferViewReport.setVisibility(true);
    this.view.viewReport.btnClose.onClick = function () {
      this.view.flxTransferViewReport.setVisibility(false);
    }.bind(this);
    this.view.flxTransferViewReport.height = this.getPageHeight();
    this.AdjustScreen();
  },

    /**Shows Make Transfer View For Editing a  Trancaction
   * @param  {JSON} transaction Transaction Model
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   * @returns {void} None
   * @throws {void} None
   */

  editScheduledTransaction: function(transaction, onCancelCreateTransfer){
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    var selectedData = data[index];
    var record = {
      "amount": transaction.amount,
      "frequencyEndDate": transaction.frequencyEndDate,
      "frequencyStartDate": selectedData.frequencyStartDate,
      "frequencyType": selectedData.frequencyType,
      "fromAccountNumber": selectedData.fromAccountNumber,
      "isScheduled": "1",
      "numberOfRecurrences": selectedData.lblRecurrenceValue,
      "scheduledDate": transaction.scheduledDate,
      "toAccountNumber": selectedData.toAccountNumber,
      "transactionDate": selectedData.lblDate,
      "ExternalAccountNumber": selectedData.externalAccountNumber,
      "transactionId": selectedData.lblReferenceNumberValue,
      "transactionsNotes": selectedData.lblNoteValue ,
      "transactionType": selectedData.transactionType,
      "category": selectedData.category
    };
    this.presenter.showMakeTransferForEditTransaction(record, onCancelCreateTransfer);
    this.AdjustScreen();
  },


    /**Executes on delete transaction button
   * @returns {void} None
   * @throws {void} None
   */

  deleteTransaction: function() {
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    var selectedData = data[index];
    var TransactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('Transactions');
    var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var transactionObject = new transactionsModel({
      "transactionId": selectedData.lblReferenceNumberValue
    });
    var scopeObj = this;
    scopeObj.view.deletePopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
    scopeObj.view.deletePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("I18n.billPay.QuitTransactionMsg");
    scopeObj.view.flxDelete.height = this.getPageHeight();
    scopeObj.view.flxDelete.left = "0%";
    this.view.deletePopup.btnYes.onClick = function() {
      scopeObj.view.flxDelete.left = "-100%";
      scopeObj.presenter.deleteTransfer(this, transactionObject);
    }
    this.view.deletePopup.btnNo.onClick = function() {
      scopeObj.view.flxDelete.left = "-100%";
    }
    this.view.deletePopup.flxCross.onClick = function() {
      scopeObj.view.flxDelete.left = "-100%";
    }
    this.AdjustScreen();
  },

   /**Create View Model For Transactions for a segment
   * @member  frmTransfersController
   * @param  {Array} data Array of transactions model 
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   * @returns {void} None
   * @throws {void} None
   */


  createNewTransfersData: function (onCancelCreateTransfer,transaction) {
    var scopeObj = this;
    if(transaction.transactionsNotes===undefined || transaction.transactionsNotes===null)
      transaction.transactionsNotes=kony.i18n.getLocalizedString("i18n.common.none");
    var dataObject = {
      btnAction: {
        text: kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
        toolTip:kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
        onClick: this.viewTransactionReport.bind(this)
      },
      btnRecentRepeat: {
        text: kony.i18n.getLocalizedString("i18n.transfers.repeat"),
        toolTip:kony.i18n.getLocalizedString("i18n.transfers.repeat"),
        onClick: this.repeatTransaction.bind(this, onCancelCreateTransfer)
      },
      btnScheduledRepeat: {
        text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
        toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
        onClick: this.editScheduledTransaction.bind(this, transaction, onCancelCreateTransfer)
      },
      btnScheduledDelete: {
        text: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.Cancel"): kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
        toolTip: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.Cancel"): kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
		onClick:commonUtilities.isCSRMode()?commonUtilities.disableButtonActionForCSRMode():this.deleteTransaction
      },
      btnCancelScheduledOccurrence: {
        isVisible: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false: true,
        text:kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
        toolTip:kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
        onClick: commonUtilities.isCSRMode()?commonUtilities.disableButtonActionForCSRMode(): scopeObj.onCancelOccurrence.bind(this, transaction)
      },
      "flxDropdown": "flxDropdown",
      "imgDropdown": "arrow_down.png",
      "recentTemplate": "flxRecentTransfers",
      "scheduledTemplate": "flxScheduledTransfers",
      "lblFromAccountTitle": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
      "lblRecurrenceTitle":kony.i18n.getLocalizedString("i18n.transfers.lblRecurrences"),
      "lblReferenceNumberTitle":kony.i18n.getLocalizedString("i18n.billPay.referenceNumber"),
      "lblSeparator": "lblSeparator",
      "lblStatusTitle":kony.i18n.getLocalizedString("i18n.Transfers.Statustitle"),
      "lblNoteTitle": kony.i18n.getLocalizedString("i18n.transfers.note"),
      "category":transaction.category,
      "frequencyType": transaction.frequencyType,
      "transactionType": transaction.transactionType,	
      "fromAccountNumber": transaction.fromAccountNumber,
      "toAccountNumber": transaction.toAccountNumber,
      "lblAmount": scopeObj.presenter.formatCurrency(transaction.amount),
      "externalAccountNumber":transaction.ExternalAccountNumber, 
      "lblSendTo": transaction.toAccountName,
      "lblLatestScheduledTransaction": this.getDateFromDateStr(transaction.transactionDate),
      "newRecurrenceValue":(transaction.recurrenceDesc||kony.i18n.getLocalizedString("i18n.common.none")),
      "lblDate": this.getDateFromDateStr(transaction.transactionDate),
      "scheduledDate":this.getDateFromDateStr(transaction.transactionDate),
      "lblStatusValue": transaction.statusDescription,
      "lblReferenceNumberValue": transaction.transactionId,
      "lblRecurrenceValue": transaction.numberOfRecurrences,
      "recurrenceDescription": transaction.recurrenceDesc ? transaction.recurrenceDesc: "-",
      "lblFromAccountValue": transaction.fromAccountName,
      "lblNoteValue": transaction.transactionsNotes,
    };
	if(commonUtilities.isCSRMode()){
        dataObject.btnScheduledDelete.skin = commonUtilities.disableSegmentButtonSkinForCSRMode(13);
    }
    return dataObject;
  },

 /** Handler for cancel occurrence button
   * @member frmTransfersController
   * @returns {void} None
   * @throws None
   */
   onCancelOccurrence: function(transaction){
     var scopeObj = this;
     scopeObj.view.deletePopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.cancelUpper");
     scopeObj.view.deletePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.cancelOccurrenceMessage");
     scopeObj.view.flxDelete.height = this.getPageHeight();
     scopeObj.view.flxDelete.left = "0%";
     this.view.deletePopup.btnYes.onClick = function() {
      scopeObj.view.flxDelete.left = "-100%";
      scopeObj.presenter.cancelTransactionOccurrence(transaction);
     };
     this.view.deletePopup.btnNo.onClick = function() {
       scopeObj.view.flxDelete.left = "-100%";
     };
     this.view.deletePopup.flxCross.onClick = function() {
       scopeObj.view.flxDelete.left = "-100%";
     };
    this.AdjustScreen();
   },
    
 /**Configure pagination for previous button for recent transfers
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */


  setPaginationPrevious_Recent: function() {
    var self=this;
    if (this.first <= 0) {
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_inactive.png";
    } else {
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function(){
        commonUtilities.showProgressBar(self.view);
        self.getPreviousRecentTransactions();
      }
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_active.png";
    }
  },

   /**Configure Pagination for Next Button Of Recent Transfers
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */


  setPaginationNext_Recent: function() {
    var self=this;
    this.view.transfermain.tablePagination.flxPaginationNext.onClick = function(){
      commonUtilities.showProgressBar(self.view);
      self.getNextRecentTransactions();
    }
    this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_active.png";
  },


   /**Configure Paginations for Recent Transfers
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */


  setRecentTransactionsPagination: function() {
    this.setPaginationPrevious_Recent();
    this.setPaginationNext_Recent();
    this.view.transfermain.tablePagination.lblPagination.text = (this.first + 1) + '-' + (this.first + this.transfersViewModel.transactionsData.length)+' Transactions';
    if(this.transfersViewModel.transactionsData.length < 10){
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function(){};
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_inactive.png";
    } 
  },

   /**Configure Pagination for Previous Button for Scheduled Transactions
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */


  setPaginationPrevious_Scheduled: function() {
    var self=this;
    if (this.first <= 0) {
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_inactive.png";
    } else {
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function(){
        commonUtilities.showProgressBar(self.view);
        self.getPreviousScheduledTransactions();
      }
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_active.png";
    }
  },


   /**Configure Pagination for Next Button of Scheduled Transactions
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */


  setPaginationNext_Scheduled: function() {
    var self=this;
    this.view.transfermain.tablePagination.flxPaginationNext.onClick = function(){
      commonUtilities.showProgressBar(self.view);
      self.getNextScheduledTransactions();
    }
    this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_active.png";
  },

  /**Configure Pagination for Scheduled Transactions
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  setScheduledTransactionsPagination: function() {
    this.setPaginationPrevious_Scheduled();
    this.setPaginationNext_Scheduled();
    this.view.transfermain.tablePagination.lblPagination.text = (this.first + 1) + '-' + (this.first + this.transfersViewModel.transactionsData.length)+' Transactions';
    if(this.transfersViewModel.transactionsData.length < 10){
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function(){};
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_inactive.png";
    } 
  },

   /**Get Front End Date String
   * @member  frmTransfersController
   * @param  {string} dateStr Date String from backend
   * @returns {void} None
   * @throws {void} None
   */

  getDateFromDateStr:function(dateStr)
  {
    if(dateStr) {
      return commonUtilities.getFrontendDateString(dateStr,commonUtilities.getConfiguration('frontendDateFormat'));
     }
    else {
      return "";
    }
  },

  /**showPendingAccounts  - Shows wether is there any pending account for verification
   * @member  frmTransfersController
   * @param  {JSON} data Array of transactions model 
   * @param  {config} Sorting Config
   * @returns {void} None
   * @throws {void} None
   */
  showPendingAccounts: function(pendingAccounts){
    var self=this;
    if(pendingAccounts.error===true){
      this.view.flxVerifyAccount.setVisibility(false);
    }else{
      var count=0;
      for(var i in pendingAccounts){
        if(pendingAccounts[i].isVerified!=="true"){
          count+=1;
          break;
        }
      }
      if(count===0){
        this.view.flxVerifyAccount.setVisibility(false);
      }else{
        this.view.lblVerifyWarning.text=  kony.i18n.getLocalizedString("i18n.transfers.verifyAccountCount");
        this.view.lblVerifyWarning.toolTip = kony.i18n.getLocalizedString("i18n.transfers.verifyAccountCount");
        this.view.lblVerifyButton.text= this.view.lblVerifyButton.toolTip = kony.i18n.getLocalizedString("i18n.Transfers.Verify");
        this.view.lblVerifyButton.onClick=function(){
          commonUtilities.showProgressBar(self.view);
          self.presenter.showExternalAccounts();
        }
        this.view.flxVerifyAccount.setVisibility(true);
      }
    }
    commonUtilities.hideProgressBar(self.view);
  },

   /**Show Recent Tab Data  - Entry Point Method After Will Update UI 
   * @member  frmTransfersController
   * @param  {Array} data Array of transactions model 
   * @param  {config} Sorting Config
   * @returns {void} None
   * @throws {void} None
   */

  showRecentsData: function(data, config) {
    // Releasing Initial Lock
    this.view.breadcrumb.setBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.transfers.transfer")
    }, {
      text: kony.i18n.getLocalizedString("i18n.transfers.recent")
    }]);
	  this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.recent");
    this.setChangeTransferTypeView(this.getValidTransferTypes(), {headerText : kony.i18n.getLocalizedString("i18n.Transfers.ChooseTransferType")});
    var scopeObj = this;
    this.sortFlex(kony.i18n.getLocalizedString("i18n.transfers.recent"));
    this.view.transfermain.flxRowSeperator.setVisibility(true);
    if ((data == undefined) || ((data instanceof Array) && data.length === 0)) this.showNoTransactions();
    else this.showSegment();
    this.view.transfermain.btnRecent.skin = "sknBtnAccountSummarySelected";
    this.getNewTransfersData(data, this.getUserRecentTransactions);
    this.setRecentTransactionsPagination();
    this.view.transfermain.flxSortExternal.setVisibility(false);
    this.view.transfermain.flxSort.setVisibility(true);
    this.setSearchFlexVisibility(false);
    var recentsDataMap = {
      "btnRepeat": "btnRecentRepeat",
      "btnAction": "btnAction",
      "flxDropdown": "flxDropdown",
      "imgDropdown": "imgDropdown",
      "lblAmount": "lblAmount",
      "template": "recentTemplate",
      "lblDate": "lblDate",
      "lblFromAccountTitle": "lblFromAccountTitle",
      "lblFromAccountValue": "lblFromAccountValue",
      "lblIdentifier": "lblIdentifier",
      "lblNoteTitle": "lblNoteTitle",
      "lblNoteValue": "lblNoteValue",
      "lblRecurrenceTitle": "lblRecurrenceTitle",
      "lblRecurrenceValue": "newRecurrenceValue",
      "lblReferenceNumberTitle": "lblReferenceNumberTitle",
      "lblReferenceNumberValue": "lblReferenceNumberValue",
      "lblSendTo": "lblSendTo",
      "lblSeparator": "lblSeparator",
      "lblStatusTitle": "lblStatusTitle",
      "lblStatusValue": "lblStatusValue"
    };
    this.view.transfermain.segmentTransfers.widgetDataMap = recentsDataMap;
    this.view.transfermain.segmentTransfers.setData(this.transfersViewModel.transactionsData);
    commonUtilities.Sorting.setSortingHandlers(this.recentAndScheduledSortMap, this.onRecentSortClickHandler, this);
    commonUtilities.Sorting.updateSortFlex(this.recentAndScheduledSortMap, config);
    commonUtilities.hideProgressBar(this.view);
    this.view.forceLayout();
  },

   /**call when frequency is changed in make transfer form - Resets the UI
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  onFrequencyChanged: function () {
    this.view.transfermain.maketransfer.getFrequencyAndFormLayout(this.view.transfermain.maketransfer.lbxFrequency.selectedKey,
      this.view.transfermain.maketransfer.lbxForHowLong.selectedKey);
    this.checkValidityMakeTransferForm();
  },

   /**Call Back when for how long listbox value is changed - Resets UI based on selection
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  onHowLongChange: function () {
    this.view.transfermain.maketransfer.getForHowLongandFormLayout(this.view.transfermain.maketransfer.lbxForHowLong.selectedKey);
    this.checkValidityMakeTransferForm();
  },

   /**Checks Validity For Make Transfer - Call on every field changes - Changes button state
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  checkValidityMakeTransferForm: function () {
    this.view.transfermain.maketransfer.lblWarning.setVisibility(false);
    var formData = this.getFormData({});
    if (formData.amount === null || formData.amount === "" || formData.amount === "NaN") {
      commonUtilities.disableButton(this.view.transfermain.maketransfer.btnConfirm)
      return;
    }
    if(formData.frequencyKey !== "Once" && formData.howLongKey === "NO_OF_RECURRENCES" && formData.noOfRecurrences === "") {
      commonUtilities.disableButton(this.view.transfermain.maketransfer.btnConfirm);
      return;
    }
    commonUtilities.enableButton(this.view.transfermain.maketransfer.btnConfirm);


  },

   /**Expands/Collapse the recent Segment
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  recentsSegmentRowClick: function () {
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    if (data[index].template === "flxRecentTransfers") {
      data[index].imgDropdown = "chevron_up.png";
      data[index].template = "flxRecentTransfersSelected";
    } else {
      data[index].imgDropdown = "arrow_down.png";
      data[index].template = "flxRecentTransfers";
    }
    this.view.transfermain.segmentTransfers.setData(data);
  },

    /**Entry Point Method for Recent Tab
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  getUserRecentTransactions: function () {
    this.first = 0;
    this.last = 10;
    commonUtilities.showProgressBar(this.view);
    this.presenter.fetchRecentUserTransactions(this, {
      "offset": this.first,
      "limit": this.last,
      'resetSorting': true
    });
  },

     /**Entry Point Method of Scheduled Tab
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  getUserScheduledTransactions: function () {
    this.first = 0;
    this.last = 10;
    commonUtilities.showProgressBar(this.view);
    this.presenter.fetchScheduledUserTransactions(this, {
      "offset": this.first,
      "limit": this.last,
      'resetSorting': true
    });
  },

     /**Called when Pagination is triggered for Recent Tranctions
   * @member  frmTransfersController
   * @param  {Array} data Array of transactions model 
   * @param  {config} Sorting Config
   * @returns {void} None
   * @throws {void} None
   */

  getNextRecentTransactions: function () {
    this.first += 10;
    this.last = 10;
    this.view.transfermain.tablePagination.imgPaginationPrevious.src = "pagination_back_active.png";
    this.presenter.fetchRecentUserTransactions(this, {
      "offset": this.first,
      "limit": this.last
    });
  },

     /**Called when previos button is triggered from pagination
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  getPreviousRecentTransactions: function () {
    this.first -= 10;
    this.last = 10;
    if (this.first >= 0) this.presenter.fetchRecentUserTransactions(this, {
      "offset": this.first,
      "limit": this.last
    });
    else {
      this.first = 0;
      this.last = 10;
      this.view.transfermain.tablePagination.imgPaginationPrevious.src = "pagination_back_inactive.png";
    }
  },

  /**Called when Next Button is triggered from pagination from scheduled tab
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  getNextScheduledTransactions: function () {
    this.first += 10;
    this.last = 10;
    this.view.transfermain.tablePagination.imgPaginationPrevious.src = "pagination_back_active.png";
    this.presenter.fetchScheduledUserTransactions(this, {
      "offset": this.first,
      "limit": this.last
    });
  },

     /**Called when Previous Button is called from Scheduled Transaction Pagination
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  getPreviousScheduledTransactions: function () {
    this.first -= 10;
    this.last = 10;
    if (this.first >= 0) this.presenter.fetchScheduledUserTransactions(this, {
      "offset": this.first,
      "limit": this.last
    });
    else {
      this.first = 0;
      this.last = 10;
      this.view.transfermain.tablePagination.imgPaginationPrevious.src = "pagination_back_inactive.png";
    }
  },

     /**Show Scheduled Tab Data  - Entry Point Method After WillUpdateUI
   * @member  frmTransfersController
   * @param  {Array} data Array of transactions model 
   * @param  {config} Sorting Config
   * @returns {void} None
   * @throws {void} None
   */

  showScheduledData: function(data, config) {
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString('i18n.transfers.transfers')
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.scheduled")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.scheduled");
      this.setChangeTransferTypeView(this.getValidTransferTypes(), {
        headerText: kony.i18n.getLocalizedString("i18n.Transfers.ChooseTransferType")
      });
      if ((data == undefined) || ((data instanceof Array) && data.length === 0)) this.showNoTransactions();
      else this.showSegment();
      this.sortFlex(kony.i18n.getLocalizedString("i18n.transfers.recent"));
      this.view.transfermain.btnScheduled.skin = "sknBtnAccountSummarySelected";
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function() {};
      this.view.transfermain.flxRowSeperator.setVisibility(true);
      this.getNewTransfersData(data, this.getUserScheduledTransactions);
      this.setScheduledTransactionsPagination();
      this.view.transfermain.flxSortExternal.setVisibility(false);
      this.view.transfermain.flxSort.setVisibility(true);
      this.setSearchFlexVisibility(false);
      var scheduledDataMap = {
        "btnRepeat": "btnScheduledDelete",
        "btnAction": "btnScheduledRepeat",
        "btnCancelThisOccurence": "btnCancelScheduledOccurrence",
        "flxDropdown": "flxDropdown",
        "imgDropdown": "imgDropdown",
        "lblAmount": "lblAmount",
        "template": "scheduledTemplate",
        "lblDate": "lblDate",
        "lblFromAccountTitle": "lblFromAccountTitle",
        "lblFromAccountValue": "lblFromAccountValue",
        "lblIdentifier": "lblIdentifier",
        "lblNoteTitle": "lblNoteTitle",
        "lblNoteValue": "lblNoteValue",
        "lblFrequencyTitle": "lblFrequencyTitle",
        "lblFrequencyValue": "lblFrequencyValue",
        "lblRecurrenceTitle": "lblRecurrenceTitle",
        "lblRecurrenceValue": "recurrenceDescription",
        "lblReferenceNumberTitle": "lblReferenceNumberTitle",
        "lblReferenceNumberValue": "lblReferenceNumberValue",
        "lblSendTo": "lblSendTo",
        "lblSeparator": "lblSeparator",
        "lblStatusTitle": "lblStatusTitle",
        "lblStatusValue": "lblStatusValue",
      };
      this.view.transfermain.segmentTransfers.widgetDataMap = scheduledDataMap;
      this.view.transfermain.segmentTransfers.setData(this.transfersViewModel.transactionsData);
      commonUtilities.Sorting.setSortingHandlers(this.recentAndScheduledSortMap, this.onScheduledSortClickHandler, this);
      commonUtilities.Sorting.updateSortFlex(this.recentAndScheduledSortMap, config);
      commonUtilities.hideProgressBar(this.view);
      this.view.forceLayout();
    },
    
  willUpdateUI: function (viewModel) {
    if (viewModel === undefined) { }
    else if (viewModel.serverError) {
      this.showServerError(viewModel.serverError);
    } else {
      if(viewModel.transferError)this.showTransferError(viewModel.transferError);
      if(viewModel.isLoading!==undefined)this.changeProgressBarState(viewModel.isLoading);
      if (viewModel.gateway) this.updateGateWay(viewModel.gateway);
      if (viewModel.showRecentTransfers) this.resetFormForRecentTransfers();
      if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
      if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
      if (viewModel.makeTransfer) {
        this.updateMakeTransferForm(viewModel.makeTransfer);
      }
      if(viewModel.pendingAccounts) this.showPendingAccounts(viewModel.pendingAccounts);
      if (viewModel.recentTransfers) this.showRecentsData(viewModel.recentTransfers, viewModel.config);
      if (viewModel.scheduledTransfers) this.showScheduledData(viewModel.scheduledTransfers, viewModel.config);
      if(viewModel.externalAccounts) this.showExternalAccounts(viewModel.externalAccounts);
      if(viewModel.viewSelectedExternalAccount) this.showSelectedExternalAccount(viewModel.viewSelectedExternalAccount);
      if(viewModel.viewExternalAccountTransactionActivity) this.showExternalAccountTransactionActivity(viewModel.viewExternalAccountTransactionActivity);
      if(viewModel.searchTransferPayees) {
        this.showSearchTransferPayees(viewModel.searchTransferPayees);
      }
    }
    this.AdjustScreen();
    this.view.forceLayout();
    
  },

  /**Show Transfer error in the UI 
   * @member  frmTransfersController
   * @param  {String} errorMsg  Transfer error message
   * @returns {void} None
   * @throws {void} None
   */
  showTransferError:function(errorMsg){
    this.view.flxMakeTransferError.setVisibility(true);
    this.view.rtxMakeTransferError.text = errorMsg;
  },
    
   /**
  * Method to change the the progress bar state
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- None
  * @returns {void} - None
  * @throws {void} -None
  */  
  changeProgressBarState: function (isLoading) {
    if (isLoading) {
      commonUtilities.showProgressBar(this.view);
    } else {
      commonUtilities.hideProgressBar(this.view);
    }
  },

  /**Show Server error in the UI 
   * @member  frmTransfersController
   * @param  {JSON} viewModel ViewModel containing server error message
   * @returns {void} None
   * @throws {void} None
   */
    
  showServerError: function(viewModel){
    var scopeObj = this;
    commonUtilities.hideProgressBar(scopeObj.view);
    scopeObj.view.flxMakeTransferError.setVisibility(true);
    scopeObj.view.rtxMakeTransferError.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
  },

     /**To be called when Recent Transfers need to be shown initially
   * @member  frmTransfersController
   * @returns {void} None
   * @throws {void} None
   */

  resetFormForRecentTransfers: function () {
    this.showNoTransactions();
    this.getUserRecentTransactions();
  },

     /**Needs to be called when gateway need to be shown initially
   * @member  frmTransfersController
   * @param  {object} gateway View Model containing from account number for preselection
   * @returns {void} None
   * @throws {void} None
   */

  updateGateWay: function (gateway) {
    this.showTransfersGateway();
    this.configureTransferGateway(gateway.overrideFromAccount);
  },

   /**Converts account object to listbox item
   * @member  frmTransfersController
   * @param  {object} fromAccount Account Model Object
   * @returns {array}  key value pair
   * @throws {void} None
   */

  fromAccountsListBoxMapper: function(fromAccount) {
    return [fromAccount.accountID, commonUtilities.getAccountDisplayNameWithBalance(fromAccount)];
  },

   /**Converts account or external Account object to a list box item
   * @member  frmTransfersController
   * @param  {object} fromAccount Account Model or ExternalAccount Model
   * @returns {array} Key Value Pair
   * @throws {void} None
   */
  toAccountListBoxMapper: function (toAccount) {
    var cardInfo = {};
    if(toAccount.accountType === "CreditCard"){
      cardInfo.CreditCard = {
        currentBalance : toAccount.currentBalance,
        lastStatementBalance : toAccount.lastStatementBalance,
        minimumDue :toAccount.minimumDue,
        dueDate : toAccount.dueDate
      }
    }
    return toAccount.accountID ? [toAccount.accountID, toAccount.nickName, cardInfo] : [toAccount.accountNumber, toAccount.nickName, cardInfo];
  },

   /**onToAccountChange : methord to change cridit card actions.
   * @member  frmTransfersController
   * @param  {}
   * @returns {}
   * @throws {}
   */
  onToAccountChange:function(editTransaction) {
    var selectedCard = this.view.transfermain.maketransfer.lbxToAccount.selectedkeyvalue[2];
    if (selectedCard.CreditCard) {
      this.isCreditCardSelected = true;
      this.view.transfermain.maketransfer.flxContainer4.setVisibility(false);
      this.view.transfermain.maketransfer.flxAmount.setVisibility(false);
      this.view.transfermain.maketransfer.lblAmount.setVisibility(false);
      this.view.transfermain.maketransfer.flxOption1.setVisibility(true);
      this.view.transfermain.maketransfer.flxOption2.setVisibility(true);
      this.view.transfermain.maketransfer.flxOption3.setVisibility(true);
      this.view.transfermain.maketransfer.flxOption4.setVisibility(true);
      this.view.transfermain.maketransfer.lblDueDate.setVisibility(true);
      this.view.transfermain.maketransfer.lblDueDate.text = "(" + kony.i18n.getLocalizedString("i18n.billPay.DueDate") + " : "+ this.getDateFromDateStr(selectedCard.CreditCard.dueDate)+ ")";
      this.view.transfermain.maketransfer.lblCurrentBalanceValue.text = commonUtilities.formatCurrencyWithCommas(selectedCard.CreditCard.currentBalance);
      this.view.transfermain.maketransfer.lblStatementBalanceValue.text =  commonUtilities.formatCurrencyWithCommas(selectedCard.CreditCard.lastStatementBalance);
      this.view.transfermain.maketransfer.lblMinimumDueBalanceValue.text =  commonUtilities.formatCurrencyWithCommas(selectedCard.CreditCard.minimumDue);
      this.view.transfermain.maketransfer.lbxFrequency.selectedKey = "Once";
      if(editTransaction && editTransaction.amount){
          this.view.transfermain.maketransfer.tbxAmountValue.text = editTransaction.amount +"";
          this.onClickRadioButton(this.view.transfermain.maketransfer.flxNUORadioBtn4);
      }else{
        this.view.transfermain.maketransfer.tbxAmountValue.text = "";
        this.onClickRadioButton(this.view.transfermain.maketransfer.flxNUORadioBtn1);
      }
      this.onAmountChanged(this.view.transfermain.maketransfer.tbxAmountValue);
      this.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
      this.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
    } else {
      this.isCreditCardSelected = false;
        this.view.transfermain.maketransfer.flxOption1.setVisibility(false);
        this.view.transfermain.maketransfer.flxOption2.setVisibility(false);
        this.view.transfermain.maketransfer.flxOption3.setVisibility(false);
        this.view.transfermain.maketransfer.flxOption4.setVisibility(false);
        this.view.transfermain.maketransfer.flxAmount.setVisibility(true);
        this.view.transfermain.maketransfer.lblAmount.setVisibility(true);
      this.view.transfermain.maketransfer.flxContainer4.setVisibility(true);
      this.view.transfermain.maketransfer.lblDueDate.setVisibility(false);
      this.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(true);
      this.onFrequencyChanged();
      this.onAmountChanged(this.view.transfermain.maketransfer.tbxAmount);
    }
    this.view.transfermain.maketransfer.forceLayout();
  },
   /**Make Transfer Form Configuration based context - handles make transfer, repeats and edit transfer  
   * @member  frmTransfersController
   * @param  {JSON} makeTransferViewModel View Model containing context and data
   * @returns {void} None
   * @throws {void} None
   */

  updateMakeTransferForm: function (makeTransferViewModel) {
    this.showMainTransferWindow();
    this.setChangeTransferTypeView(this.getValidTransferTypes(), {fromAccount : makeTransferViewModel.defaultFromAccountNumber, headerText : kony.i18n.getLocalizedString("i18n.Transfers.ChangeTransferType")});
    commonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calSendOn);
    commonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calEndingOn);
    this.resetTransfersForm();
    if (makeTransferViewModel.toAccounts.length === 0) {
      this.showNoAccountsAvailableUI(makeTransferViewModel.type)
    }
    else {
      this.showMainTransferWindow();
      this.showMakeTransfer();
      this.view.transfermain.maketransfer.lbxFromAccount.masterData = makeTransferViewModel.fromAccounts.map(this.fromAccountsListBoxMapper);
    this.view.transfermain.maketransfer.lbxFromAccount.selectedKey =  makeTransferViewModel.defaultFromAccountNumber;
    this.view.transfermain.maketransfer.lbxToAccount.masterData = makeTransferViewModel.toAccounts.map(this.toAccountListBoxMapper);
    this.filterAccountsTo(makeTransferViewModel);
    this.filterFromAccount(makeTransferViewModel);
    if (makeTransferViewModel.editTransactionObject) {
      this.preFillTransfersForm(this.generateTransferData(makeTransferViewModel, makeTransferViewModel.editTransactionObject));
    }
    if (makeTransferViewModel.repeatTransactionObject) {
      this.preFillTransfersForm(this.generateTransferData(makeTransferViewModel, makeTransferViewModel.repeatTransactionObject));
    }
    if (makeTransferViewModel.transferData) {
      this.preFillTransfersForm(makeTransferViewModel.transferData)
    }
    this.view.transfermain.maketransfer.lbxToAccount.onSelection = this.onToAccountChange;
    if (makeTransferViewModel.editTransactionObject || makeTransferViewModel.repeatTransactionObject) {
        if(makeTransferViewModel.editTransactionObject !== undefined){
            var transactionObj = makeTransferViewModel.editTransactionObject
        }else{
            transactionObj = makeTransferViewModel.repeatTransactionObject
        }
        this.view.transfermain.maketransfer.lbxToAccount.onSelection(transactionObj);
    }else{
        this.view.transfermain.maketransfer.lbxToAccount.onSelection();
    }
//     this.view.transfermain.maketransfer.lbxToAccount.setEnabled(false);
    //   } else { 
    //     this.view.transfermain.maketransfer.lbxToAccount.setEnabled(true);
    //   }
      this.view.transfermain.maketransfer.btnConfirm.onClick = this.confirmTransfer.bind(this, makeTransferViewModel);
      this.view.transfermain.maketransfer.btnModify.onClick = makeTransferViewModel.onCancelCreateTransfer ||  function () {
        this.showTransfersGateway();
      }.bind(this);
      var scopeObj = this;
      this.view.transfermain.maketransfer.lbxFromAccount.onSelection = this.filterAccountsTo.bind(this, makeTransferViewModel);
      this.view.flxMakeTransferError.setVisibility(false);
    }
    },

     /** Returns Current Date in string format 
     * @member  frmTransfersController
     * @returns {string} Date String
     * @throws {void} None
     */


    getCurrDateString: function () {
      var currDate = new Date();
      var month = currDate.getMonth() + 1;
      if (commonUtilities.getConfiguration("frontendDateFormat") === "mm/dd/yyyy") {
          return  (month < 10 ? '0' + month : month) + "/" +  (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/"  + currDate.getFullYear()
      }
      else {
      return (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/" + (month < 10 ? '0' + month : month) + "/" + currDate.getFullYear();        
      }
  },

   /** Converts Transaction Model Object to a view model
   * @member  frmTransfersController
   * @param  {JSON} makeTransferViewModel View Model containing context and data
   * @param  {object} transaction Transaction Model Object
   * @returns {void} None
   * @throws {void} None
   */

    generateTransferData: function (makeTransferViewModel, transaction) {
      var viewModel = {};
        viewModel.accountFromKey = transaction.fromAccountNumber;
        viewModel.accountToKey = transaction.transactionType === 'InternalTransfer' ? transaction.toAccountNumber : transaction.ExternalAccountNumber
        viewModel.amount = transaction.amount || "";
        viewModel.frequencyKey = transaction.frequencyType || 'Once';
        viewModel.noOfRecurrences = transaction.numberOfRecurrences || "";
        viewModel.notes = transaction.transactionsNotes || "";
        viewModel.howLongKey = transaction.numberOfRecurrences ? "NO_OF_RECURRENCES" : "ON_SPECIFIC_DATE";
        
        if (this.getDateObject(transaction.scheduledDate).getTime() < new Date().getTime()) {
            viewModel.sendOnDate = this.getCurrDateString();
        }
        else {
            viewModel.sendOnDate = commonUtilities.getFrontendDateString(transaction.scheduledDate, commonUtilities.getConfiguration('frontendDateFormat'));
        }
        if (this.getDateObject(transaction.frequencyEndDate).getTime() < new Date().getTime()) {
            viewModel.endOnDate = this.getCurrDateString();
        }
        else {
            viewModel.endOnDate =  commonUtilities.getFrontendDateString(transaction.frequencyEndDate, commonUtilities.getConfiguration('frontendDateFormat'));
        }
        return viewModel;
    },

     /**Returns date object from a dateString
     * @member  frmTransfersController
     * @param  {JSON} dateString 
     * @returns {object} Date Object
     * @throws {void} None
     */


    getDateObject: function (dateString) {
      if (commonUtilities.getConfiguration('frontendDateFormat') === "mm/dd/yyyy") {
          return new Date(dateString);
      }
      else if (commonUtilities.getConfiguration('frontendDateFormat') === "dd/mm/yyyy")
      {
          var dateParts = dateString.split("/");
          var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
          return dateObject;
      }
      else {
          return new Date();
      }
  },
   /**Show field error for transfer form 
   * @member  frmTransfersController
   * @param  {string} errorKey i18n key for Make Transfer form
   * @returns {void} None
   * @throws {void} None
   */

    showFieldError: function (errorKey) {
      this.view.transfermain.maketransfer.lblWarning.setVisibility(true);
      this.view.transfermain.maketransfer.lblWarning.text = kony.i18n.getLocalizedString(errorKey);
      this.AdjustScreen();
    },

     /**Hides The Field Error from make Transfer Form
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */


    hideFieldError: function () {
      this.view.transfermain.maketransfer.lblWarning.setVisibility(false);      
      this.AdjustScreen();
    },

    /**Prefills a transfer form from view model
     * @member  frmTransfersController
     * @param  {JSON} transferData View Model containing context and data
     * @returns {void} None
     * @throws {void} None
     */


    preFillTransfersForm: function (transferData) {
      this.view.transfermain.maketransfer.lbxFrequency.selectedKey = transferData.frequencyKey;
      this.view.transfermain.maketransfer.calSendOn.date = transferData.sendOnDate;
      this.view.transfermain.maketransfer.calEndingOn.date = transferData.endOnDate;
      this.view.transfermain.maketransfer.lbxForHowLong.selectedKey = transferData.howLongKey;
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.text = transferData.noOfRecurrences;
      this.view.transfermain.maketransfer.txtNotes.text = transferData.notes; //UI textbox widget name is change to txtNotes
      this.view.transfermain.maketransfer.lbxFromAccount.selectedKey =  transferData.accountFromKey; 
      this.view.transfermain.maketransfer.lbxToAccount.selectedKey = transferData.accountToKey;     
      this.view.transfermain.maketransfer.tbxAmount.text = transferData.amount;
      this.onAmountChanged(this.view.transfermain.maketransfer.tbxAmount);
      this.checkValidityMakeTransferForm();
      this.onFrequencyChanged();
    },

    /**Resets the Transfer Form
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */


    resetTransfersForm: function () {
      this.view.transfermain.maketransfer.lblWarning.setVisibility(false);
      this.view.transfermain.maketransfer.lbxFromAccount.masterData = [];
      this.view.transfermain.maketransfer.lbxToAccount.masterData = [];
      this.view.transfermain.maketransfer.lbxFrequency.masterData = this.presenter.getFrequencies();
      this.view.transfermain.maketransfer.calSendOn.date = commonUtilities.DateUtils.toDateFormat(new Date(), kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'));
      this.view.transfermain.maketransfer.calEndingOn.date = commonUtilities.DateUtils.toDateFormat(new Date(), kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'));
      this.view.transfermain.maketransfer.lbxForHowLong.masterData = this.presenter.getForHowLong();
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.text = "";
      this.view.transfermain.maketransfer.txtNotes.text = "";
      this.view.transfermain.maketransfer.tbxAmount.text = "";
      this.onAmountChanged(this.view.transfermain.maketransfer.tbxAmount);
      this.checkValidityMakeTransferForm();
      this.onFrequencyChanged();
    },

    /**Returns Date Object from Date Components
     * @member  frmTransfersController
     * @param  {array} dateComponents Date Components returned from Calendar Widget
     * @returns {object} Date Object
     * @throws {void} None
     */



   getDateObj : function (dateComponents) {
      var date =  new Date();
      date.setDate(dateComponents[0]);
      date.setMonth(parseInt(dateComponents[1])-1);
      date.setFullYear(dateComponents[2]);
      date.setHours(0,0,0,0)
      return date;
    },

    /**Send Form Model to P.C for showing confirmation to user
     * @member  frmTransfersController
     * @param  {json} makeTransferViewModel OBject containing form data
     * @returns {void} None
     * @throws {void} None
     */

    confirmTransfer: function (makeTransferViewModel) {
      var formData = this.getFormData(makeTransferViewModel);      
      formData.accountFrom = this.getFromAccount(makeTransferViewModel, formData.accountFromKey);
      formData.accountTo = this.getToAccount(makeTransferViewModel, formData.accountToKey);
      if (this.validateTransferData (formData,makeTransferViewModel)) {
          this.presenter.confirmTransfer(makeTransferViewModel,formData);        
      }
      
    },

    /**Validated some of the business rules on transfer form data and shows field errors
     * @member  frmTransfersController
     * @param  {errorMsg} 
     * @returns {} 
     * @throws {void} None
     */
  showFieldErrorMinMaxError : function(errorMsg){
          this.view.transfermain.maketransfer.lblWarning.setVisibility(true);
          this.view.transfermain.maketransfer.lblWarning.text = errorMsg;
          this.AdjustScreen();
    },
    /**Validated some of the business rules on transfer form data and shows field errors
     * @member  frmTransfersController
     * @param  {JSON} formData Form Data
     * @returns {boolean} True if form data is correct and false if its not
     * @throws {void} None
     */

   validateTransferData : function (formData,makeTransferViewModel) {
      var currTime = new Date();
      var minMaxLimit={};
      minMaxLimit=makeTransferViewModel.limit;
      var amountFloats=parseFloat(formData.amount);
      currTime.setHours(0,0,0,0); // Sets to midnight.
      var sendOnDate = this.getDateObj(formData.sendOnDateComponents);
      var endOnDate = this.getDateObj(formData.endOnDateComponents);
      if (formData.accountFrom === formData.accountTo) {
          this.showFieldError("i18n.transfers.error.cannotTransferToSame");
          return false;
      }
      else if (!commonUtilities.isValidAmount(formData.amount.replace(/,/g, ""))){
          this.showFieldError("i18n.transfers.error.enterAmount");
          return false;            
      }
      else if(amountFloats < minMaxLimit.minimum){
          this.showFieldErrorMinMaxError(kony.i18n.getLocalizedString("i18n.common.minTransactionError") + " " + commonUtilities.formatCurrencyWithCommas(minMaxLimit.minimum));
          return false;
      }
      else if(amountFloats > minMaxLimit.maximum){
          this.showFieldErrorMinMaxError(kony.i18n.getLocalizedString("i18n.common.maxTransactionError") + " " + commonUtilities.formatCurrencyWithCommas(minMaxLimit.maximum));
          return false;
      }
      else if (sendOnDate.getTime() < currTime.getTime()) {
          this.showFieldError("i18n.transfers.error.invalidSendOnDate");
          return false;            
      }
      else if(formData.frequencyKey !== 'Once') {
          if (formData.howLongKey === 'ON_SPECIFIC_DATE') {
              if(endOnDate.getTime() < currTime.getTime()) {
                  this.showFieldError("i18n.transfers.errors.invalidEndOnDate");
                  return false;            
              }
              if(endOnDate.getTime() ===  sendOnDate.getTime()) {
                  this.showFieldError("i18n.transfers.errors.sameEndDate");
                  return false;            
              }
              if(endOnDate.getTime() < sendOnDate.getTime()) {
                  this.showFieldError("i18n.transfers.errors.beforeEndDate");
                  return false;            
              }
          }
          if (formData.howLongKey === 'NO_OF_RECURRENCES') {
              if(formData.noOfRecurrences === null || formData.noOfRecurrences === "" ||formData.noOfRecurrences === '0' || (isNaN(formData.noOfRecurrences) || !Number.isInteger(Number.parseFloat(formData.noOfRecurrences) )) ) {
                  this.showFieldError("i18n.transfers.error.invalidNoOfRecurrences");
                  return false;  
              }
          }

      }
      this.hideFieldError();

      return true;
  },

  
    /**Filters from account if to field is same. Needs to be called if there is only one item in to field
     * @member  frmTransfersController
     * @param  {JSON} makeTransferViewModel Make Transfer Context
     * @returns {void} None
     * @throws {void} None
     */

  filterFromAccount: function (makeTransferViewModel) {
    var scopeObj = this;
    var toAccount = scopeObj.getToAccount(makeTransferViewModel, scopeObj.view.transfermain.maketransfer.lbxToAccount.selectedKey);
    
    if (makeTransferViewModel.toAccounts.length === 1 && makeTransferViewModel.fromAccounts.length > 1) {
      var fromAccountMasterData = makeTransferViewModel.fromAccounts.filter(function (fromAccountPair) {
        return  toAccount !== fromAccountPair
      }).map(scopeObj.fromAccountsListBoxMapper)
      if (fromAccountMasterData.length < makeTransferViewModel.fromAccounts.length) {
        scopeObj.view.transfermain.maketransfer.lbxFromAccount.masterData = fromAccountMasterData;      
      }
    }
  },

  
    /**Filter Accounts To if from account is same
     * @member  frmTransfersController
     * @param  {array} makeTransferViewModel Make Transfer Context
     * @returns {void} None
     * @throws {void} None
     */

  filterAccountsTo: function (makeTransferViewModel) {
    var scopeObj = this;
    var fromAccount = scopeObj.getFromAccount(makeTransferViewModel, scopeObj.view.transfermain.maketransfer.lbxFromAccount.selectedKey);
    if (makeTransferViewModel.toAccounts.length > 1) {
      var toAccountMasterData = makeTransferViewModel.toAccounts.filter(function (account) {
        return fromAccount !== account
       }).map(scopeObj.toAccountListBoxMapper);
       if (toAccountMasterData.length < makeTransferViewModel.toAccounts.length) {
        scopeObj.view.transfermain.maketransfer.lbxToAccount.masterData = toAccountMasterData; 
        this.onToAccountChange();       
       }
    }
  },

  
    /**Returns Account Model Object for given account number
     * @member  frmTransfersController
     * @param  {array} makeTransferViewModel Make Transfer Context
     * @param  {array} selectedKey AccountID 
     * @returns {object} Account Model Object
     * @throws {void} None
     */

  getFromAccount: function (makeTransferViewModel, selectedKey) {
    return  makeTransferViewModel.fromAccounts.filter(function (fromAccount) {
      return fromAccount.accountID === selectedKey
    })[0]
    
  },

    
    /**Returns Account Model Object or ExternalAccount Object for given account number
     * @member  frmTransfersController
     * @param  {array} makeTransferViewModel Make Transfer Context
     * @param  {array} selectedKey AccountID 
     * @returns {object} Account Model Object or ExternalAccount Object
     * @throws {void} None
     */

  getToAccount: function (makeTransferViewModel, selectedKey) {
    return  makeTransferViewModel.toAccounts.filter(function (toAccount) {
      if (toAccount.accountID) {
        return toAccount.accountID === selectedKey;
      }
      else {
        return toAccount.accountNumber === selectedKey;
      }
    })[0]
  },

  /**Configure Transfer Gateway using a config
     * @member  frmTransfersController
     * @param  {array} overrideFromAccount AccountID for preselection
     * @returns {void} None
     * @throws {void} None
     */

	

  configureTransferGateway: function (overrideFromAccount) {
    var transferGatewayConfig = [{
        key: OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS,
        info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
        title: "i18n.transfers.toMyKonyBankAccounts",
        displayName:"KonyBankAccountsTransfer",
        image: "to_konybank_acc.png"
      },
      {
        key: OLBConstants.TRANSFER_TYPES.OTHER_INTERNAL_MEMBER,
        info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
        title: "i18n.transfers.toOtherKonyBankAccounts",
        displayName:"OtherKonyAccountsTransfer",
        image: "to_otherkonybank_acc.png"
      },
      {
        key: OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT,
        info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
        title: "i18n.transfers.toOtherBankAccounts",
        displayName:"OtherBankAccountsTransfer",
        image: "to_otherbank_acc.png"
      },
      {
        key: OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT,
        info: ["i18n.transfers.365days", "i18n.transfers.transferLimit"],
        title: "i18n.transfers.toInternationalAccounts",
        displayName:"InternationalAccountsTransfer",
        image: "to_internationalbank_acc.png",
        external: true
      },
      {
                key: OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER,
                info: ["i18n.transfers.365days", "i18n.transfers.transferLimitWireTransfer"],
                title: "i18n.transfers.wireTransfer",
                displayName: ["DomesticWireTransfer", "InternationalWireTransfer"],
                image: "wire_transfer.png"
      }
    ]
    var self = this;
    var transferToViews = this.view.transfermain.flxTransfersGateway.widgets();

    function createCallback(config, visible) {
      if(config.key === 'WIRE_TRANSFER') {
        return function () {
          var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
          WireTransferModule.presentationController.showWireTransfer();
        }
      }
      return function () {
        self.presenter.loadAccountsByTransferType(config.key, null, overrideFromAccount);
      }

    }
    var visibleCounter = 0;
    for (var index = 0; index < transferGatewayConfig.length; index++) {
      var transferToView = transferToViews[visibleCounter+1];
      var gatewayConfig = transferGatewayConfig[index];
      if (this.getTransferTypeVisibility(gatewayConfig.key)) {
        transferToView.setVisibility(true);
        var infoWidgets = transferToView.widgets()[0].widgets();
        infoWidgets[0].src = gatewayConfig.image;
        infoWidgets[1].text = kony.i18n.getLocalizedString(gatewayConfig.title);
        infoWidgets[2].text = this.getDisplayDescription(gatewayConfig.displayName,gatewayConfig.key);
        var buttonProceed = transferToView.widgets()[1].widgets()[0];
        buttonProceed.onClick = createCallback(gatewayConfig);
        visibleCounter++;
      }
       this.setVisibilityOfAddRecipients(gatewayConfig.displayName);
    }

    for (var i = visibleCounter + 1; i < transferToViews.length ; i++) {
      var view = transferToViews[i];
      view.setVisibility(false);
    }
  },

    /** Decides whether to show a Add Accounts window
         * @member  frmTransfersController
         * @returns {} 
         * @throws {} 
         */
    setVisibilityOfAddRecipients:function(type){
        if(type === "OtherKonyAccountsTransfer"){
            if(commonUtilities.getConfiguration("isOtherKonyAccountsTransfer") === "false"){
                this.view.lblAddKonyAccount.setVisibility(false);
       }
     }else if(type=== "OtherBankAccountsTransfer"){
         if(commonUtilities.getConfiguration("isOtherBankAccountsTransfer") === "false"){
             this.view.lblAddNonKonyAccount.setVisibility(false);
         }
       }else if(type === "InternationalAccountsTransfer"){
          if(commonUtilities.getConfiguration("isInternationalAccountsTransfer") === "false"){
            this.view.lblAddInternationalAccount.setVisibility(false);
          }
       }
        
    },
  /** Decides whether to show a Transfer Type
         * @member  frmTransfersController
         * @returns {boolean} true if type needs to be shown , false otherwise
         * @throws {void} None
         */
         getTransferTypeVisibility: function(transferType) {
            switch (transferType) {
                case OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS:
                    return commonUtilities.getConfiguration("isKonyBankAccountsTransfer") === "true";
                    break;
                case OLBConstants.TRANSFER_TYPES.OTHER_INTERNAL_MEMBER:
                    return commonUtilities.getConfiguration("isOtherKonyAccountsTransfer") === "true";
                    break;
                case OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT:
                    return commonUtilities.getConfiguration("isOtherBankAccountsTransfer") === "true";
                    break;
                case OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT:
                    return commonUtilities.getConfiguration("isInternationalAccountsTransfer") === "true";
                    break;
                case OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER:
                     var enabledFromEntitlements = commonUtilities.getConfiguration("isInternationalWireTransferEnabled") === "true" ||  commonUtilities.getConfiguration("isDomesticWireTransferEnabled") === "true";
                     var wireTransferEligible = kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData.isWireTransferEligible;
                     wireTransferEligible = wireTransferEligible === "true" || wireTransferEligible === true;
                     return enabledFromEntitlements && wireTransferEligible;
                     break;
            }
        },

  
   /**getDisplayDescription : This function gets the display description to be displayed 
         * @member  frmTransfersController
         * @param  {String,String} DisplayName for which description is to be displayed
         * @returns {void} None
         * @throws {void} None
         */
         getDisplayDescription: function(displayName, gatewayConfigkey) {
            var description = "";
            var servicesForUser = kony.mvc.MDAApplication.getSharedInstance().appContext.servicesList;
            if (servicesForUser) {
                servicesForUser.forEach(function(dataItem) {
                    if (gatewayConfigkey === OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER) {
                        if (dataItem.displayName === displayName[0] || dataItem.displayName === displayName[1]) {
                            description = !description ? dataItem.serviceDesc : description + " & " + dataItem.serviceDesc;
                        }
                    } else if (dataItem.displayName === displayName) {
                        description = dataItem.serviceDesc;
                    }
                });
            }
            return description;
        },

  /**  Returns form Data of Make Transfer Component
     * @member  frmTransfersController
     * @returns {JSON} JSON containing all the form data
     * @throws {void} None
     */


  getFormData: function () {
    var viewModel =  {};
    viewModel.accountFromKey = this.view.transfermain.maketransfer.lbxFromAccount.selectedKey;
    viewModel.accountToKey = this.view.transfermain.maketransfer.lbxToAccount.selectedKey;

    if (this.isCreditCardSelected !== true) {
      viewModel.amount = this.view.transfermain.maketransfer.tbxAmount.text.replace(/,/g, "");
    }else{
      if(this.view.transfermain.maketransfer.lblRadioBtn1.text === "M"){
        viewModel.amount = this.removeCurrencyWithCommas(this.view.transfermain.maketransfer.lblCurrentBalanceValue.text);
      }else if(this.view.transfermain.maketransfer.lblRadioBtn2.text === "M"){
        viewModel.amount = this.removeCurrencyWithCommas(this.view.transfermain.maketransfer.lblStatementBalanceValue.text);
      }else if(this.view.transfermain.maketransfer.lblRadioBtn3.text === "M"){
        viewModel.amount = this.removeCurrencyWithCommas(this.view.transfermain.maketransfer.lblMinimumDueBalanceValue.text);
      }else{
        viewModel.amount = this.view.transfermain.maketransfer.tbxAmountValue.text.replace(/,/g, "");
      }
    }
    
    viewModel.frequencyKey = this.view.transfermain.maketransfer.lbxFrequency.selectedKey;
    viewModel.howLongKey = this.view.transfermain.maketransfer.lbxForHowLong.selectedKey;
    viewModel.sendOnDate = this.view.transfermain.maketransfer.calSendOn.date;
    viewModel.sendOnDateComponents = this.view.transfermain.maketransfer.calSendOn.dateComponents;
    viewModel.endOnDate = this.view.transfermain.maketransfer.calEndingOn.date;
    viewModel.endOnDateComponents = this.view.transfermain.maketransfer.calEndingOn.dateComponents;
    viewModel.noOfRecurrences = this.view.transfermain.maketransfer.tbxNoOfRecurrences.text.trim();
    viewModel.notes = this.view.transfermain.maketransfer.txtNotes.text.trim();
    return viewModel;
  },

  //move to common
    removeCurrencyWithCommas: function (amount) {
      //var currencySymbol = kony.i18n.getLocalizedString("i18n.common.currencySymbol");
      if (amount === undefined || amount === null) {
        return;
      }
      amount = amount + "";
      amount = amount.replace(/,/g, "");
      amount = amount.replace(/\$/g, "");
      //amount = amount.replace(/"+currencySymbol+"/g,"");
      return amount;
    },

    onClickRadioButton: function (radioButton) {
      var self = this;
      var allRadioButtions = ["lblRadioBtn1", "lblRadioBtn2", "lblRadioBtn3", "lblRadioBtn4"];
      if (radioButton && radioButton.children) {
        var selectedButton = radioButton.children[0];
      }else{
        return;
      }
      var selectRadioButton = function (button) {
        var RadioBtn = self.view.transfermain.maketransfer[button];
        RadioBtn.text = "M";
        RadioBtn.skin = "sknLblFontTypeIcon3343e820pxMOD";
      }
      var unSelectRadioButton = function (button) {
        var RadioBtn = self.view.transfermain.maketransfer[button];
        RadioBtn.text = "L";
        RadioBtn.skin = "sknC0C0C020pxNotFontIconsMOD";
      }
      allRadioButtions.forEach(function (button) {
        if (button === selectedButton) {
          selectRadioButton(button);
        } else {
          unSelectRadioButton(button);
        }
      });
      this.checkValidityMakeTransferForm();
    },

  /**Initial Actions mapping - called on preshow
     * @member  frmTransfersController
     * @returns {void}None
     * @throws {void} None
     */



  initTabsActions: function () {
    var scopeObj = this;
    this.view.transfermain.Search.txtSearch.width = "100%";
    this.view.transfermain.imgSearch.src = "search_blue.png";
    this.setSearchFlexVisibility(false);
    this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    this.view.flxTransferViewReport.isVisible = false;
    this.fixBreadcrumb();
    this.view.transfermain.maketransfer.flxNUORadioBtn1.onClick = this.onClickRadioButton;
    this.view.transfermain.maketransfer.flxNUORadioBtn2.onClick = this.onClickRadioButton;
    this.view.transfermain.maketransfer.flxNUORadioBtn3.onClick = this.onClickRadioButton;
    this.view.transfermain.maketransfer.flxNUORadioBtn4.onClick = this.onClickRadioButton;

    this.view.transfermain.maketransfer.tbxAmountValue.onTouchStart = function(){
      scopeObj.onClickRadioButton(scopeObj.view.transfermain.maketransfer.flxNUORadioBtn4);
    }

    this.view.transfermain.maketransfer.tbxAmountValue.onKeyUp = commonUtilities.validateAmountFieldKeyPress.bind(this)
    this.view.transfermain.maketransfer.tbxAmountValue.onTextChange = this.view.transfermain.maketransfer.tbxAmountValue.onEndEditing = this.onAmountChanged;
    this.view.transfermain.maketransfer.tbxAmountValue.onBeginEditing = commonUtilities.removeDelimitersForAmount.bind(this);
    this.view.transfermain.btnMakeTransfer.onClick = function () {this.presenter.showTransferScreen()}.bind(this);
    this.view.transfermain.btnExternalAccounts.onClick=function(){
      commonUtilities.showProgressBar(this.view);
      this.presenter.showExternalAccounts();
    }.bind(this);
    this.view.transfermain.btnExternalAccounts.toolTip = kony.i18n.getLocalizedString('i18n.Transfers.Recipients');
    this.view.transfermain.btnRecent.onClick=function(){this.getUserRecentTransactions()}.bind(this);
    this.view.transfermain.btnScheduled.onClick=function(){this.getUserScheduledTransactions()}.bind(this);
    this.setHeaderActions();

    //Setting Actions for Add Kony Accounts
    this.view.flxAddKonyAccount.onClick=function(){
      commonUtilities.showProgressBar(this.view);
      this.presenter.showSameBankAccounts();
    }.bind(this);
    this.view.flxAddNonKonyAccount.onClick=function(){
      commonUtilities.showProgressBar(this.view);
      this.presenter.showDomesticAccounts();
    }.bind(this);
    this.view.flxAddInternationalAccount.onClick=function(){
      commonUtilities.showProgressBar(this.view);
      this.presenter.showInternationalAccounts();
    }.bind(this);
    this.view.transfermain.maketransfer.lbxForHowLong.onSelection = this.onHowLongChange.bind(this);
    this.view.transfermain.maketransfer.lbxFrequency.onSelection = this.onFrequencyChanged.bind(this);
    this.view.transfermain.maketransfer.tbxAmount.onKeyUp = commonUtilities.validateAmountFieldKeyPress.bind(this)
    this.view.transfermain.maketransfer.tbxAmount.onEndEditing = this.onAmountChanged
    this.view.transfermain.maketransfer.tbxAmount.onBeginEditing = commonUtilities.removeDelimitersForAmount.bind(this);
    this.view.transfermain.maketransfer.tbxNoOfRecurrences.onKeyUp = this.checkValidityMakeTransferForm.bind(this);
    this.view.transfermain.maketransfer.btnModify.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
    this.view.transfermain.maketransfer.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
    this.view.transfermain.maketransfer.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.common.continue")
  },

  /**Callback for AMoutn Field Key up
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  /**Callback for Amount Field onEndEditing
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  onAmountChanged: function (obj) {
    commonUtilities.validateAndFormatAmount(obj);
    this.checkValidityMakeTransferForm();
  },

  /**Binds Action for Logout and other header functionalities
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
  
  setHeaderActions: function () {
    var scopeObj = this;
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      scopeObj.view.flxLogout.height = scopeObj.getPageHeight();
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CustomPopup.btnYes.onClick = function () {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      scopeObj.view.flxLogout.left = "-100%";
    }
    this.view.CustomPopup.flxCross.onClick = function () {
      scopeObj.view.flxLogout.left = "-100%";
    }
  },

  /**Resets Bread crumb state
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  fixBreadcrumb: function () {
    this.view.breadcrumb.setBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.transfers.transfers")
    }, {
      text: kony.i18n.getLocalizedString("i18n.transfers.make_transfer")
    }]);
   this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
   this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.make_transfer");	
  },

  /**Hides all the main flexes and  Resets UI
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  hideAll: function () {
    this.view.transfermain.btnMakeTransfer.skin = "sknBtnAccountSummaryUnselectedmod";
    this.view.transfermain.btnRecent.skin = "sknBtnAccountSummaryUnselected";
    this.view.transfermain.btnScheduled.skin = "sknBtnAccountSummaryUnselected";
    this.view.transfermain.btnExternalAccounts.skin = "sknBtnAccountSummaryUnselected";
    this.view.flxMakeTransferError.setVisibility(false);
    this.view.transfermain.flxSort.setVisibility(false);
    this.view.transfermain.flxMakeTransferForm.setVisibility(false);
    this.view.transfermain.segmentTransfers.setVisibility(false);
    this.view.transfermain.flxNoTransactions.setVisibility(false);
    this.view.transfermain.tablePagination.setVisibility(false);
    this.view.transfermain.flxTransfersGateway.setVisibility(false);
    this.view.transfermain.flxSortExternal.setVisibility(false);
    this.view.transferactivity.isVisible=false;
  },

  /**Shoe main transfer window - contains tabs etc
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  showMainTransferWindow: function () {
    this.view.flxTrasfersWindow.setVisibility(true);
    this.view.flxNoAccounts.setVisibility(false);
  },

  /**Shows Transfer Gateway View
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  showTransfersGateway: function () {
    this.hideAll();
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfers')}, {text:kony.i18n.getLocalizedString("i18n.transfers.make_transfer")}]);
	 this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.make_transfer");
    this.view.transfermain.btnMakeTransfer.skin = "sknBtnAccountSummarySelectedmod";
    this.view.flxTrasfersWindow.setVisibility(true);
    this.view.transfermain.flxTransfersGateway.setVisibility(true);
    this.setSearchFlexVisibility(false);
    this.view.transfermain.flxRowSeperator.setVisibility(false);
    this.view.flxChangeTransferType.setVisibility(false); //Hide change tranfer.
    this.view.forceLayout();
  },

  /**Show Make Transfer
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  showMakeTransfer: function () {
    this.hideAll();
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfers')}, {text:kony.i18n.getLocalizedString("i18n.transfers.make_transfer")}]);
	 this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.make_transfer");
    this.view.transfermain.btnMakeTransfer.skin = "sknBtnAccountSummarySelectedmod";
    this.view.transfermain.flxMakeTransferForm.setVisibility(true);
    this.view.transfermain.flxRowSeperator.setVisibility(false);
    this.setSearchFlexVisibility(false);

    this.view.forceLayout();
  },

  /**Show Empty View
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  showNoTransactions: function (context) {
    this.hideAll();
    var self=this;
    this.view.transfermain.flxNoTransactions.setVisibility(true);
    if(context===kony.i18n.getLocalizedString("i18n.transfers.externalAccount")){
       this.view.transfermain.lblScheduleAPayment.setVisibility(false);
       this.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.Transfer.noExternalAccount'); 
    }else{
    this.view.transfermain.lblScheduleAPayment.setVisibility(true);
    this.view.transfermain.lblScheduleAPayment.onTouchEnd=function(){
         self.presenter.showTransferScreen();
    };  
    this.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.transfers.noTransactions');     
  }
    this.view.forceLayout();
  },

  /**Shows Segment for Tabs
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
  showSegment: function () {
    this.hideAll();
    this.view.transfermain.flxSort.setVisibility(true);
    this.view.transfermain.segmentTransfers.setVisibility(true);
    this.view.transfermain.tablePagination.setVisibility(true);
    this.view.forceLayout();
  },

  /**Configure No Accounts Available UI
     * @member  frmTransfersController
     * @param  {string} transferType Type constant of Transfer 
     * @returns {void} None
     * @throws {void} None
     */

  showNoAccountsAvailableUI: function (transferType) {
    function isExternal(type) {
      var externalAccountTypes = ['OTHER_EXTERNAL_ACCOUNT', 'WIRE_TRANSFER'];
      return externalAccountTypes.indexOf(type) > 0;
    }

    function getButtonConfig() {
      if (transferType === "OWN_INTERNAL_ACCOUNTS") {
        return {
          text:'i18n.transfers.add_kony_account',
          description: 'i18n.transfers.noMyInternalAccount',
          navigator: function () {

          } 
        }
      }
      if (transferType === "OTHER_INTERNAL_MEMBER") {
        return {
          text:'i18n.transfers.add_kony_account',
          description: 'i18n.transfers.noOtherInternalAccounts',                    
          navigator: function () {
            var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            trasferModule.presentationController.showSameBankAccounts(); 
          } 
        }
      }
      if(transferType === "INTERNATIONAL_ACCOUNT") {
        return  {
          text:'i18n.transfers.add_international_account',
          description: 'i18n.transfers.noInternationalAccount',                                                            
          navigator: function () {
            var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            trasferModule.presentationController.showInternationalAccounts(); 
          } 
        }
      }
      if(transferType === "OTHER_EXTERNAL_ACCOUNT") {
        return  {
          text:'i18n.transfers.add_non_kony_account',
          description: 'i18n.transfers.noExternalAccount',                                                            
          navigator: function () {
            var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            trasferModule.presentationController.showDomesticAccounts(); 
          } 
        }
      }
      else {
        return  {
          text:'i18n.transfers.add_kony_account',
          description: 'i18n.transfers.noOtherInternalAccounts',                                        
          navigator: function () {
            var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            trasferModule.presentationController.showSameBankAccounts(); 
          } 
        }
      }
    }


    this.view.flxTrasfersWindow.setVisibility(false);
    this.view.flxNoAccounts.setVisibility(true);
    this.view.NoAccounts.btnBack.onClick = function () {
      this.view.flxTrasfersWindow.setVisibility(true);
      this.view.flxNoAccounts.setVisibility(false);
    }.bind(this);
    var buttonConfig = getButtonConfig(transferType);
    this.view.NoAccounts.btnAddAccount.text = kony.i18n.getLocalizedString(buttonConfig.text);
    
    this.view.NoAccounts.txtMessage.text = kony.i18n.getLocalizedString(buttonConfig.description);
    this.view.NoAccounts.btnAddAccount.onClick = buttonConfig.navigator;

  },

  /**Configure Sort Flex
     * @member  frmTransfersController
     * @param  {string} tab Type of tab and shows sort flex 
     * @returns {void} None
     * @throws {void} None
     */

  sortFlex: function (tab) {
    if (tab === kony.i18n.getLocalizedString("i18n.transfers.externalAccount")) {
      this.view.transfermain.flxSortExternal.setVisibility(true);
      this.view.transfermain.flxSort.setVisibility(false);
      this.view.transfermain.lblSortDateExternal.text = kony.i18n.getLocalizedString("i18n.transfers.accountName");
      this.view.transfermain.lblSortDescriptionExternal.text = kony.i18n.getLocalizedString("i18n.transfers.bankName");
      this.view.transfermain.lblSortTypeExternal.text = kony.i18n.getLocalizedString("i18n.Transfers.Statustitle");
    } else if (tab === kony.i18n.getLocalizedString("i18n.transfers.recent")) {
      this.view.transfermain.flxSortExternal.setVisibility(false);
      this.view.transfermain.flxSort.setVisibility(true);
      this.view.transfermain.lblSortDate.text = kony.i18n.getLocalizedString("i18n.transfers.transactionDate");
      this.view.transfermain.lblSortDescription.text = kony.i18n.getLocalizedString("i18n.transfers.sentTo");
      this.view.transfermain.lblSortType.text = kony.i18n.getLocalizedString("i18n.transfers.amountlabel");
    }
  },

  /**Shows Add Internal Bank Account
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  addInternalBankAccount: function () {
    var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
    transferModule.presentationController.showSameBankAccounts();
  },

  /**Shows Next External Accounts - From pagination
     * @member  frmTransfersController
     * @param  {object} paginationValue current pagination values 
     * @returns {void} None
     * @throws {void} None
     */
 
  setNextExternalAccounts:function(paginationValue){
    var self=this;
    this.view.transfermain.tablePagination.flxPaginationNext.onClick = function(){
      commonUtilities.showProgressBar(self.view);
      self.presenter.getExternalAccounts({
        "offset":paginationValue.offset+paginationValue.limit,
        "limit":paginationValue.limit,
        'preservePrevSorting': true
      });
    };
    this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_active.png";
  },

  /**Shows Previous External Accounts
     * @member  frmTransfersController
     * @param  {object} paginationValue Current Pagination Value 
     * @returns {void} None
     * @throws {void} None
     */

  setPreviousExternalAccounts:function(paginationValue){
    var self=this;
    if(paginationValue.offset<=0){
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_inactive.png";
    }else{
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function(){
      	commonUtilities.showProgressBar(self.view);
        self.presenter.getExternalAccounts({
          "offset":paginationValue.offset-paginationValue.limit,
          "limit":paginationValue.limit,
          'preservePrevSorting': true
        });
      };
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_active.png";
    }
  },

  /**Configure Pagination for External Accounts
     * @member  frmTransfersController
     * @param  {object} viewModel of external accounts 
     * @param  {number} len Length of current records
     * @returns {void} None
     * @throws {void} None
     */

  setExternalAccountsPagination: function(viewModel,len) {
    this.view.transfermain.tablePagination.flxPagination.setVisibility(true);    
    this.setNextExternalAccounts(viewModel[len]);
    this.setPreviousExternalAccounts(viewModel[len]);
    this.view.transfermain.tablePagination.lblPagination.text = (viewModel[len].offset + 1) + '-' + (viewModel[len].offset + len) + ' Accounts';
    if (viewModel.length < 11) {
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_inactive.png";
    }
  },

  /**UI entrypoint for external accounts
     * @member  frmTransfersController
     * @param  {JSON} viewModel  
     * @param  {boolean} hidePagination  If pagination needs to be hidden or not
     * @returns {void} None
     * @throws {void} None
     */

 showExternalAccounts:function(viewModel, hidePagination){
    this.setChangeTransferTypeView(this.getValidTransferTypes(), {headerText : kony.i18n.getLocalizedString("i18n.Transfers.ChooseTransferType")});
    if(viewModel==="errorExternalAccounts"){
        this.hideAll();
        this.showServerError();
        this.view.transfermain.flxNoTransactions.setVisibility(true);
        this.view.transfermain.rtxNoPaymentMessage.i18n_text= kony.i18n.getLocalizedString("i18n.transfers.errorExternalAccounts");
        this.view.transfermain.rtxNoPaymentMessage.text= kony.i18n.getLocalizedString("i18n.transfers.errorExternalAccounts");
        this.view.transfermain.lblScheduleAPayment.isVisible=false;
        this.view.forceLayout();
    }else if ((viewModel === undefined) || ((viewModel instanceof Array) && viewModel.length < 2)) {
      this.showNoTransactions(kony.i18n.getLocalizedString("i18n.transfers.externalAccount"));
    }
	else{
      var scopeObj = this;
      this.view.flxMakeTransferError.setVisibility(false);
      this.view.flxTrasfersWindow.isVisible=true;
      this.view.transferactivity.isVisible=false;

      this.sortFlex(kony.i18n.getLocalizedString("i18n.transfers.externalAccount"));
      this.showSegment();
      this.view.transfermain.flxRowSeperator.setVisibility(true);
      this.view.transfermain.btnExternalAccounts.skin = "sknBtnAccountSummarySelected";
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.transfers.transfer")
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts")
      }]);
	   this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.external_accounts");
 
      this.view.transfermain.lblSortDate.text = kony.i18n.getLocalizedString("i18n.transfers.accountName");
      this.view.transfermain.lblSortDescription.text = kony.i18n.getLocalizedString("i18n.transfers.bankNameKey");
      this.view.transfermain.lblSortType.text = kony.i18n.getLocalizedString("i18n.transfers.amount");
      this.view.transfermain.flxSortExternal.setVisibility(true);
      this.view.transfermain.flxSort.setVisibility(false);
      this.view.forceLayout();
      var externalAccountDataMap = {
        "btnBankDetails": "btnBankDetails",
        "btnDelete": "btnDelete",
        "btnEdit": "btnEdit",
        "btnViewActivity": "btnViewActivity",
        "btnAction": "btnAction",
        "btnCancel": "btnCancel",
        "btnMakeTransfer": "btnMakeTransfer",
        "btnSave": "btnSave",
        "flxDropdown": "flxDropdown",
        "imgDropdown": "imgDropdown",
        "lblAccountHolderTitle": "lblAccountHolderTitle",
        "lblAccountHolderValue": "lblAccountHolderValue",
        "lblAccountName": "lblAccountName",
        "lblAccountNumberTitle": "lblAccountNumberTitle",
        "lblAccountNumberValue": "lblAccountNumberValue",
        "lblAccountTypeTitle": "lblAccountTypeTitle",
        "lblAccountTypeValue": "lblAccountTypeValue",
        "lblAddedOnTitle": "lblAddedOnTitle",
        "lblAddedOnValue": "lblAddedOnValue",
        "lblBankDetailsTitle": "lblBankDetailsTitle",
        "lblBankName": "lblBankName",
        "lblIdentifier": "lblIdentifier",
        "lblRoutingNumberTitle": "lblRoutingNumberTitle",
        "lblRoutingNumberValue": "lblRoutingNumberValue",
        "lblSeparator": "lblSeparator",
        "lblSeparatorActions": "lblSeparatorActions",
        "lblStatus": "lblStatus",
        "txtAccountName": "txtAccountName",
        "txtAccountNumber": "txtAccountNumber",
        "txtAccountType": "txtAccountType"
      };
      var len=viewModel.length;
      function getMappings(context){
        if(context.routingNumberDetails){
          context=context.routingNumberDetails;
          if(context.lblRoutingNumberTitle){
            if(context.lblRoutingNumberTitle.isInternationalAccount==="true"){
              return kony.i18n.getLocalizedString("i18n.accounts.swiftCode");
            }else{
              return kony.i18n.getLocalizedString("i18n.accounts.routingNumber");
            }
          }if(context.lblRoutingNumberValue){
            if(context.lblRoutingNumberValue.isInternationalAccount==="true"){
              return context.lblRoutingNumberValue.swiftCode;
            }else if(context.lblRoutingNumberValue.isSameBankAccount==="false"){
              return context.lblRoutingNumberValue.routingNumber;
            }else{
              return kony.i18n.getLocalizedString("i18n.common.NA");
            }
          }
        }
        if(context.accountStatus){
          if(context.accountStatus.isVerified==="true"){
            return kony.i18n.getLocalizedString("i18n.transfers.verified");
          }
          else{
            return kony.i18n.getLocalizedString("i18n.transfers.pending");
          }
        }
        if(context.viewActivity){
          if(commonUtilities.getConfiguration("fundTransferHistory")==='true'&&context.viewActivity.isVerified==="true"){
            return  {
              "text":kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
              "toolTip":kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
              "onClick":function(){
                scopeObj.onBtnViewActivity();
              }
            }  
          }else{
            return {
              "isVisible": false
            }
          }  
        }
        if(context.btnActivityAccountStatus){
          if(context.btnActivityAccountStatus.isVerified==="true"){
            return {
              "text":kony.i18n.getLocalizedString("i18n.transfers.make_transfer"),
              "toolTip":kony.i18n.getLocalizedString("i18n.common.transferToThatPayee"),

              "onClick":function(){
                commonUtilities.showProgressBar(scopeObj.view);
                scopeObj.onBtnMakeTransfer();
              }
            };
          }
          else{
            return {
              "text":kony.i18n.getLocalizedString("i18n.transfers.verifyAccount"),
              "toolTip":kony.i18n.getLocalizedString("i18n.transfers.verifyAccount"),

              "onClick":function(){
                commonUtilities.showProgressBar(scopeObj.view);
                scopeObj.onBtnVerifyAccount();
              }
            };
          }
        }
        if (context.congigkeyPlug) {
          if (commonUtilities.getConfiguration('addExternalAccount') === 'true') {
            return true;
          } else if (commonUtilities.getConfiguration('addExternalAccount') === 'false' && context.congigkeyPlug.isVerified === "true") {
            return true;
          } else {
            return false;
          }
        }
      }
      if(!hidePagination) {
        this.setExternalAccountsPagination(viewModel,len-1);          //new lOC                
      }

      var data = [];
      for (var i = 0; i < len-1; i++) {
        if (viewModel[i] !== undefined&&getMappings({"congigkeyPlug":viewModel[i]})) {
          var dataObject = {
            "btnDelete": {
              "text": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
              "toolTip": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
              "onClick":commonUtilities.isCSRMode()?commonUtilities.disableButtonActionForCSRMode():function() {
                scopeObj.onExternalAccountDelete();
              }
            },
            "btnEdit": {
              "toolTip":kony.i18n.getLocalizedString("i18n.common.edit"),
              "text": kony.i18n.getLocalizedString("i18n.common.edit"),
              "onClick": function() {
                scopeObj.externalAccountsSegmentRowClickEdit();
              }
            },
            "btnViewActivity": getMappings({"viewActivity":viewModel[i]}),
            "btnAction": getMappings({"btnActivityAccountStatus":viewModel[i]}),
            "btnCancel": {
              "text": kony.i18n.getLocalizedString('i18n.transfers.Cancel'),
              "toolTip": kony.i18n.getLocalizedString('i18n.transfers.Cancel'),
              "onClick": function() {
                scopeObj.showUnselectedRow();
              }
            },
            "btnSave": {
              "text": kony.i18n.getLocalizedString('i18n.ProfileManagement.Save'),
              "toolTip": kony.i18n.getLocalizedString('i18n.common.saveChanges'),
              "onClick":commonUtilities.isCSRMode()?commonUtilities.disableButtonActionForCSRMode():function() {
                scopeObj.saveChangedExternalAccount();
              }
            },
            "imgDropdown": "arrow_down.png",
            "lblAccountHolderTitle": kony.i18n.getLocalizedString('i18n.common.accountHolder'),
            "lblAccountHolderValue": kony.i18n.getLocalizedString('i18n.common.individualAccount'),
            "lblAccountName": viewModel[i].nickName,
            "lblAccountNumberTitle": kony.i18n.getLocalizedString('i18n.common.accountNumber'),
            "lblAccountNumberValue": viewModel[i].accountNumber,
            "lblAccountTypeTitle": kony.i18n.getLocalizedString('i18n.transfers.accountType'),
            "lblAccountTypeValue": viewModel[i].accountType,
            "lblAddedOnTitle": kony.i18n.getLocalizedString('i18n.common.addedOn'),
            "lblAddedOnValue": commonUtilities.getFrontendDateString(viewModel[i].createdOn),
            "lblBankDetailsTitle": kony.i18n.getLocalizedString('i18n.transfers.bankDetails'),
            "lblBankName": viewModel[i].bankName,
            "lblIdentifier": "lblIdentifier",
            "lblRoutingNumberTitle": getMappings({routingNumberDetails:{lblRoutingNumberTitle:viewModel[i]}}),    //new lOC
            "lblRoutingNumberValue": getMappings({routingNumberDetails:{lblRoutingNumberValue:viewModel[i]}})||"NA",    //new lOC
            "lblSeparator": viewModel[i].beneficiaryName,
            "lblSeparatorActions": "lblSeparatorActions",
            "lblStatus": getMappings({accountStatus:viewModel[i]}),
            "template": "flxExternalAccountsTransfersUnselected",
            "txtAccountName": {
              "text": viewModel[i].nickName,
              "placeholder": ""
            },
            "txtAccountNumber": {
              "text": viewModel[i].accountNumber,
              "placeholder": ""
            },
            "txtAccountType": {
              "text": viewModel[i].accountType,
              "placeholder": ""
            }
          };
		  if(commonUtilities.isCSRMode()){
             dataObject.btnDelete.skin = commonUtilities.disableSegmentButtonSkinForCSRMode(13);
             dataObject.btnSave.skin = commonUtilities.disableButtonSkinForCSRMode();
          }
          data.push(dataObject);
        }
      }
      this.view.transfermain.segmentTransfers.widgetDataMap = externalAccountDataMap;
      this.view.transfermain.segmentTransfers.setData(data);
      commonUtilities.Sorting.updateSortFlex(this.externalAccountsSortMap, viewModel[len-1]);
    }
    commonUtilities.hideProgressBar(this.view);
  },

  /**Shows the unselected row of segment
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
  
  showUnselectedRow: function(){
     var index = this.view.transfermain.segmentTransfers.selectedIndex[1]; 
     this.showSelectedRow(index);
  },

  /**Saves the changes of inline edit of external account
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
  
  saveChangedExternalAccount:function(){
    commonUtilities.showProgressBar(this.view);
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data[index];
    var params;
    if(data.txtAccountType.text===data.lblAccountTypeValue&&data.lblStatus===kony.i18n.getLocalizedString("i18n.transfers.verified")){
      params={
        "accountNumber":data.txtAccountNumber.text,
        "accountType":data.txtAccountType.text,
        "nickName":data.txtAccountName.text,
        "isVerified":1
      };
    }else{
      params={
        "accountNumber":data.txtAccountNumber.text,
        "accountType":data.txtAccountType.text,
        "nickName":data.txtAccountName.text,
        "isVerified":0
      };
    } 
    this.presenter.saveChangedExternalAccount(params,data);   
    this.AdjustScreen();
  },

  /**Shows Activity of a external account
     * @member  frmTransfersController
     * @param  {array} viewModel Array of transaction objects  
     * @returns {void} None
     * @throws {void} None
     */
  
   showExternalAccountTransactionActivity: function(viewModel) {
        var self=this;
        this.view.flxMakeTransferError.setVisibility(false);
        this.view.transferActivitymod.flximgdatemod.setVisibility(false);
        this.view.transferActivitymod.flxfromaccountimgmod.setVisibility(false);
        this.view.flxTrasfersWindow.isVisible = false;
        this.view.transferactivity.isVisible = true;
        this.view.transferActivitymod.tablePagination.isVisible = false;
        
        this.view.transferActivitymod.flxSort.isVisible = false;
        this.view.transferActivitymod.flxsorttransfers.isVisible = true;
        this.view.transferActivitymod.flxSegmentBillPay.isVisible = false;
        this.view.transferActivitymod.flxSegment.isVisible = true;
        this.view.breadcrumb.setBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.transfers.transfer"),
            callback: function(){
            	self.presenter.showTransferScreen();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts"),
            callback: function() {
            	commonUtilities.showProgressBar(self.view);
            	self.presenter.getExternalAccounts()
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.transfers.viewTransferActivity")
        }]);
        var len = viewModel.length;
        this.view.transferActivitymod.lblAccountName.text = viewModel[len - 1].accountNumber;
        this.view.transferActivitymod.lblAccountHolder.text = viewModel[len - 1].nickName;
        var widgetDataMap = {
            "flxAmount": "flxAmount",
            "flxDate": "flxDate",
            "flxFrom": "flxFrom",
            "flxRunningBalance": "flxRunningBalance",
            "flxSort": "flxSort",
            "flxStatus": "flxStatus",
            "lblAmount": "lblAmount",
            "lblDate": "lblDate",
            "lblFrom": "lblFrom",
            "lblpaiddate": "lblpaiddate",
            "lblRunningBalance": "lblRunningBalance",
            "lblStatus": "lblStatus"
        };
        
        var data = [];
        if (len < 2) {
            this.view.transferActivitymod.flxSegment.isVisible = false;
            this.view.transferActivitymod.flxNoRecords.isVisible = true;
            this.view.transferActivitymod.rtxNoRecords.text= kony.i18n.getLocalizedString("i18n.transfers.noTransactions");
            this.view.transferActivitymod.lblAmountDeducted.text = kony.i18n.getLocalizedString("i18n.common.NA");
        } else {
            for (var i = 0; i < len - 1; i++) {
                data.push({
                    "lblAmount": commonUtilities.formatCurrencyWithCommas(Math.abs(viewModel[i].amount)),
                    "lblDate": viewModel[i].transactionDate.slice(),
                    "lblFrom": viewModel[i].fromNickName,
                    "lblStatus": viewModel[i].statusDescription,
                    "lblpaiddate": commonUtilities.getFrontendDateString(viewModel[i].transactionDate, commonUtilities.getConfiguration("frontendDateFormat"))
                });
            }
            this.view.transferActivitymod.lblAmountDeducted.text = commonUtilities.formatCurrencyWithCommas(viewModel[0].totalAmount);
            this.view.transferActivitymod.segTransferActivity.widgetDataMap = widgetDataMap;
            this.view.transferActivitymod.segTransferActivity.setData(data);
            this.view.transferActivitymod.flxSegment.isVisible = true;
            this.view.transferActivitymod.flxNoRecords.isVisible = false;
        }
        commonUtilities.hideProgressBar(this.view);
    },
  

    /**Call back for Verify and Add Accounts
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  verifyAndAddAccount: function(viewModel){
      this.showExternalAccounts(viewModel);      
  }, 
  
/**Callbacks for External Account Delete Button
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

 onExternalAccountDelete:function(){
    var scopeObj=this;
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccountMsg");
      scopeObj.view.flxLogout.height = this.getPageHeight();
      scopeObj.view.flxLogout.left = "0%";
    
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    var accountNumber=data[index].txtAccountNumber.text;
    this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString('i18n.common.deleteTheAccount');
    this.view.CustomPopup.btnYes.onClick = function () {
      //function
    scopeObj.presenter.deleteExternalAccount(accountNumber);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString('i18n.common.noDontDelete');
    this.view.CustomPopup.btnNo.onClick = function () {
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      scopeObj.view.flxLogout.left = "-100%";
    };  
   this.AdjustScreen();
  },
  
  /**Shows Selected External Account
     * @member  frmTransfersController
     * @param  {JSON} viewModel of externalAccount 
     * @returns {void} None
     * @throws {void} None
     */

  showSelectedExternalAccount:function(viewModel){
    this.showExternalAccounts(viewModel);   //showExternalAccount in case of bhawna
    this.showSelectedRow(viewModel[viewModel.length-1].index);
  },

  /**Show Selected Row
     * @member  frmTransfersController
     * @param  {number} index Index of Row Needs to be expanded 
     * @returns {void} None
     * @throws {void} None
     */

  showSelectedRow: function(index)
  {
    var data =  this.view.transfermain.segmentTransfers.data;
    for(var i=0;i<data.length;i++)
    {
      if(i===index)
      {
       data[i].imgDropdown = "chevron_up.png";
       data[i].template = "flxExternalAccountsTransfersSelected";
      }
      else
      {
        data[i].imgDropdown = "arrow_down.png";
        data[i].template = "flxExternalAccountsTransfersUnselected";
      }
    }  
    this.view.transfermain.segmentTransfers.setData(data);
  }, 

  /**Call back function for View Activity Button
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  onBtnViewActivity:function(){
    commonUtilities.showProgressBar(this.view);
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data[index];
    this.presenter.showSelectedAccountTransactions(data);
    this.AdjustScreen();
  },

  /**Callback for Make Transfer Button
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
  
 onBtnMakeTransfer:function(){
     var self=this;
    commonUtilities.showProgressBar(this.view);
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data[index];
    var onCancelCreateTransfer=function(){
      commonUtilities.showProgressBar(this.view);
      self.setSearchFlexVisibility(false);
      self.presenter.showExternalAccounts();
    };
    this.presenter.showTransferScreen({"accountTo":data.txtAccountNumber.text,onCancelCreateTransfer :onCancelCreateTransfer});
    this.AdjustScreen(); 
 },

 
  /**Call back for Verify Account Button
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
  
  onBtnVerifyAccount: function(){
    commonUtilities.showProgressBar(this.view);
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    this.presenter.showVerifyAccounts(data[index]);
    this.AdjustScreen();
  },

  
  /**Callback for External Accounts Expand
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  externalAccountsSegmentRowClick: function () {
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    if (data[index].template === "flxRecentTransfers") {
      data[index].imgDropdown = "chevron_up.png";
      data[index].template = "flxExternalAccountsTransfersSelected";
    } else {
      data[index].imgDropdown = "arrow_down.png";
      data[index].template = "flxRecentTransfers";
    }

    this.view.transfermain.segmentTransfers.setData(data);
  },

  
  /**Callback for Edit Button on external accounts
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  externalAccountsSegmentRowClickEdit: function () {
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    if (data[index].template === "flxExternalAccountsTransfersSelected") {
      data[index].imgDropdown = "chevron_up.png";
      data[index].template = "flxExternalAccountsTransfersEdit";
    } else {
      data[index].imgDropdown = "arrow_down.png";
      data[index].template = "flxExternalAccountsTransfersSelected";
    }

    this.view.transfermain.segmentTransfers.setData(data);
    this.AdjustScreen(150);
  },

  
  /**Called when post show is called
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  postShowtransfers: function(){
    this.presenter.showPendingAccountsCount();
    this.AdjustScreen();
  },
    //UI Code

  
  /**Adjusts screen - Places footer on correct place
     * @member  frmTransfersController
     * @param  {number} data Offset needs to be added 
     * @returns {void} None
     * @throws {void} None
     */

  AdjustScreen: function(data) {
    if(data !== undefined) {     
    }
    else{
      data = 0;
    }
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + 40 + data + "dp";
        else
            this.view.flxFooter.top = mainheight + 40 + data + "dp";
        this.view.forceLayout();
     } else {
        this.view.flxFooter.top = mainheight + 40 + data + "dp";
        this.view.forceLayout();
     }
  },  
  
  
  /**Toggles Search Box
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
  toggleSearchBox: function () {
    this.setSearchFlexVisibility(!this.view.transfermain.flxSearch.isVisible);
    if(this.view.transfermain.btnExternalAccounts.skin === "sknBtnAccountSummaryUnselected" || this.searchView === true) {
        commonUtilities.showProgressBar(this.view);
        if(this.searchView === true) {
          this.presenter.getExternalAccounts(); //show external tab.
        } else {
          this.presenter.showExternalAccounts(); //show external tab.          
        }
    }
    if (!this.view.transfermain.flxSearch.isVisible) {
        this.searchView = false;       
        this.prevSearchText = '';     
    }
    this.view.forceLayout();
    this.AdjustScreen();     
    
    
  },

  
  /**Shows Search Box
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  showSearchBox: function () {
    var self = this;
    if (self.view.transfermain.btnMakeTransfer.skin === "sknBtnAccountSummarySelectedmod") {
      self.showScheduledData();
      self.toggleSearchBox();
    } else {
      self.toggleSearchBox();
    }
  },

    /**
    * Set search flex visibility.
     * @member  frmTransfersController
     * @param  {boolean} flag visibility value 
     * @returns {void} None
     * @throws {void} None
     */
  setSearchFlexVisibility: function(flag){
    if(typeof flag === "boolean") {
      this.view.transfermain.imgSearch.src = flag ? "selecetd_search.png" : "search_blue.png";
      this.view.transfermain.flxSearch.setVisibility(flag);
      this.view.transfermain.flxSearchSortSeparator.setVisibility(flag);
      if(flag === true) {
        this.view.transfermain.Search.txtSearch.text = '';
        this.view.transfermain.Search.txtSearch.setFocus();
        this.disableSearch();
      }
    }
    else {
    }
  },
  
  /**Turns off the Activity Flex
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */

  closeViewTransfers: function () {
    this.view.transferActivity.isVisible = false;
  },

  
  /**Hamburger Menu COnfiguration
     * @member  frmTransfersController
     * @param  {JSON} sideMenuModel Side Menu viewModel 
     * @returns {void} None
     * @throws {void} None
     */

  updateHamburgerMenu: function (sideMenuModel) {
    this.view.customheader.initHamburger(sideMenuModel);    
    this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
    if(this.view.transfermain.btnMakeTransfer.skin === 'sknBtnAccountSummarySelected'){
      this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
    }
    else if(this.view.transfermain.btnRecent.skin === 'sknBtnAccountSummarySelected'){
      this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer History");
    }
    else if(this.view.transfermain.btnExternalAccounts.skin === 'sknBtnAccountSummarySelected'){
      this.view.customheader.customhamburger.activateMenu("Transfers", "External Accounts");
    }
  },

  
  /**Configures Top Bar
     * @member  frmTransfersController
     * @param  {JSON} topBarModel Top Bar View Model 
     * @returns {void} None
     * @throws {void} None
     */

  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  },
 
  /**Form Preshow
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
 initActions: function(){
  var scopeObj = this;
  	this.view.customheader.forceCloseHamburger();
	this.view.transfermain.maketransfer.calSendOn.dateFormat = commonUtilities.getConfiguration("frontendDateFormat");
   this.view.transfermain.maketransfer.calEndingOn.dateFormat = commonUtilities.getConfiguration("frontendDateFormat");
  if (commonUtilities.getConfiguration("canSearchTransfers")==="true") {
    scopeObj.view.transfermain.flxSearchImage.setVisibility(true);
    scopeObj.view.transfermain.flxSearchImage.onClick = scopeObj.toggleSearchBox.bind(scopeObj);
    scopeObj.view.transfermain.Search.btnConfirm.onClick = scopeObj.onSearchBtnClick.bind(scopeObj);
    scopeObj.view.transfermain.Search.flxClearBtn.onClick = scopeObj.onSearchClearBtnClick.bind(scopeObj);
    scopeObj.view.transfermain.Search.txtSearch.onKeyUp = scopeObj.onTxtSearchKeyUp.bind(scopeObj);
    
  } else {
    scopeObj.view.transfermain.flxSearchImage.setVisibility(false);
  }
  //External Accounts
  scopeObj.externalAccountsSortMap = [
    { name : 'nickName' ,  imageFlx : scopeObj.view.transfermain.imgSortDateExternal , clickContainer : scopeObj.view.transfermain.flxSortDateExternal},
    { name : 'bankName' ,  imageFlx : scopeObj.view.transfermain.imgSortDescriptionExternal, clickContainer : scopeObj.view.transfermain.flxSortDescriptionExternal},
    { name : 'isVerified' ,  imageFlx : scopeObj.view.transfermain.imgSortTypeExternal, clickContainer : scopeObj.view.transfermain.flxSortAmountExternal }
  ];
  commonUtilities.Sorting.setSortingHandlers(scopeObj.externalAccountsSortMap, scopeObj.onExternalAccountsSortClickHandler, scopeObj);

  //Recent AND Scheduled Transactions.
  scopeObj.recentAndScheduledSortMap = [
    { name : 'transactionDate' ,  imageFlx : scopeObj.view.transfermain.imgSortDate , clickContainer :scopeObj.view.transfermain.flxSortDate},
    { name : 'a.nickName' ,  imageFlx : scopeObj.view.transfermain.imgSortDescription, clickContainer : scopeObj.view.transfermain.flxSortDescription},
    { name : 'amount' ,  imageFlx : scopeObj.view.transfermain.imgSortType, clickContainer : scopeObj.view.transfermain.flxSortAmount }
  ];
   
  //Hiding Error flex
   this.view.flxMakeTransferError.setVisibility(false);
   
   
 },
 

  /** On Recent Transactions Sort click handler.
     * @member  frmTransfersController
     * @param  {object} event object
     * @param  {JSON} data New Sorting Data
     * @returns {void} None
     * @throws {void} None
     */
onRecentSortClickHandler: function(event, data){
  var scopeObj = this;
  commonUtilities.showProgressBar(this.view);
  scopeObj.first = 0;
  scopeObj.presenter.fetchRecentUserTransactions(null,data);
},

/** On Scheduled Transactions Sort click handler.
     * @member  frmTransfersController
     * @param  {object} event object
     * @param  {JSON} data New Sorting Data
     * @returns {void} None
     * @throws {void} None
     */
onScheduledSortClickHandler: function(event, data){
  var scopeObj = this;
  commonUtilities.showProgressBar(this.view);
  scopeObj.first = 0;
  scopeObj.presenter.fetchScheduledUserTransactions(null,data);
},

/** On External Account  Sort click handler.
     * @member  frmTransfersController
     * @param  {object} event object
     * @param  {JSON} data New Sorting Data
     * @returns {void} None
     * @throws {void} None
     */
 onExternalAccountsSortClickHandler: function(event, data){
  var scopeObj = this;
  commonUtilities.showProgressBar(this.view);
  scopeObj.first = 0;
  scopeObj.presenter.getExternalAccounts(data);
  scopeObj.setSearchFlexVisibility(false);
  },


 /** On Search Text Key Up
     * @member  frmTransfersController
     * @param  {object} event object
     * @returns {void} None
     * @throws {void} None
     */
  onTxtSearchKeyUp: function(event){
    var scopeObj = this;
    var searchKeyword = scopeObj.view.transfermain.Search.txtSearch.text.trim();
    if(searchKeyword.length > 0){
      scopeObj.view.transfermain.Search.txtSearch.onDone = scopeObj.onSearchBtnClick.bind(scopeObj);
      scopeObj.enableSearch();
    } else{
      scopeObj.view.transfermain.Search.txtSearch.onDone = function(){};
      scopeObj.disableSearch();
    }
  },
 /** On Payee Search
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
 onSearchBtnClick: function(){
  var scopeObj = this;
  var searchKeyword = scopeObj.view.transfermain.Search.txtSearch.text.trim();
  if (searchKeyword.length >= 0 && scopeObj.prevSearchText !== searchKeyword) {
      commonUtilities.showProgressBar(this.view);
      scopeObj.presenter.searchTransferPayees({
          'searchKeyword': searchKeyword
      });
      scopeObj.searchView = true;
      scopeObj.prevSearchText = searchKeyword;
  }

 },

  /** On search Clear button
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
 onSearchClearBtnClick: function(){
  //this.toggleSearchBox();
  this.setSearchFlexVisibility(true);
  if (this.searchView === true) {
      commonUtilities.showProgressBar(this.view);
      this.presenter.getExternalAccounts(); //show external tab.
  }
  this.searchView = false;
  this.prevSearchText = '';
  this.view.forceLayout();
 },
/** On Search is complete show external accounts
     * @member  frmTransfersController
     * @param  {array} viewModel Array of recipients
     * @returns {void} None
     * @throws {void} None
     */
 showSearchTransferPayees: function(viewModel){
   var scopeObj = this;
   commonUtilities.hideProgressBar(this.view);
   scopeObj.view.transfermain.tablePagination.flxPagination.setVisibility(false);
   if (viewModel.error) {
     scopeObj.showExternalAccounts("errorExternalAccounts");
     return;
   }
   if(viewModel.externalAccounts.length === 0){
    scopeObj.view.transfermain.flxSortExternal.setVisibility(false);
    scopeObj.view.transfermain.segmentTransfers.setVisibility(false);
    scopeObj.view.transfermain.flxNoTransactions.setVisibility(true);
    scopeObj.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.transfers.searchNoPayees'); 
    scopeObj.view.transfermain.lblScheduleAPayment.setVisibility(false);    
    scopeObj.view.forceLayout();
    return;
   }
   viewModel.externalAccounts.push(viewModel.searchInputs); //Append search inputs for showExternalAccounts viewModel.   
   scopeObj.showExternalAccounts(viewModel.externalAccounts, true);
 },
 /** Disables Search Button
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
 disableSearch: function () {
   this.disableButton(this.view.transfermain.Search.btnConfirm)
   this.view.transfermain.Search.flxClearBtn.setVisibility(false);
 },

 loadMyPaymentAccountsData: function () {
  var dataMap = {
    "lblLeft": "lblLeft",
    "lblAccountName": "lblAccountName",
    "flxLeft": "flxLeft",
    "lblBalance": "lblBalance",
    "lblAvailableBalance": "lblAvailableBalance"
  };
  var data = [{
    "flxLeft": {
      "skin": "sknFlx9060b7"
    },
    "lblLeft": " ",
    "lblAccountName": "Personal Chekcing...1234",
    "lblBalance": "$6453.90",
    "lblAvailableBalance": "Available Balance"
  },
              {
                "flxLeft": {
                  "skin": "sknFlx26D0CE"
                },
                "lblLeft": " ",
                "lblAccountName": "Joint Savings...1234",
                "lblBalance": "$6444.00",
                "lblAvailableBalance": "Available Balance"
              },
              {
                "flxLeft": {
                  "skin": "sknFlxF4BA22"
                },
                "lblLeft": " ",
                "lblAccountName": "Credit Card...5432",
                "lblBalance": "$3840.50",
                "lblAvailableBalance": "Available Balance"
              },
              {
                "flxLeft": {
                  "skin": "sknFlx26D0CE"
                },
                "lblLeft": " ",
                "lblAccountName": "Joint Savings...4423",
                "lblBalance": "$3244.00",
                "lblAvailableBalance": "Available Balance"
              }
             ];
            },
 /** Enable Search Button
     * @member  frmTransfersController
     * @returns {void} None
     * @throws {void} None
     */
 enableSearch: function () {
   this.enableButton(this.view.transfermain.Search.btnConfirm)
   this.view.transfermain.Search.flxClearBtn.setVisibility(true);
   this.view.forceLayout();
 },
 /** Disable Button
     * @member  frmTransfersController
     * @param  {object} button object
     * @returns {void} None
     * @throws {void} None
     */
  disableButton : function (button) {
    button.setEnabled(false);
    button.skin = "sknBtnBlockedLatoFFFFFF15Px";
    button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
    button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";     
  },
  /** Enable Button
     * @member  frmTransfersController
     * @param  {object} button object
     * @returns {void} None
     * @throws {void} None
     */
  enableButton : function(button){
    button.setEnabled(true);   
    button.skin = "sknbtnLatoffffff15px";
    button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
    button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";    
  },

  //OCC Changes:
  /**
   * getValidTransferTypes : Method to filter Transfer type actions w.r.t configurtions
   * @member of {frmTransfersController}
   * @param {Array} actionKeys, action keys
   * @return {Array} transferTypes - eligible Transfer type actions
   * @throws {}
   */
  getValidTransferTypes: function( actionKeys ) {
    var scopeObj = this;
    var actionObj;
    var transferTypes = [];
    actionKeys = actionKeys || [
      OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS,
      OLBConstants.TRANSFER_TYPES.OTHER_INTERNAL_MEMBER,
      OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT,
      OLBConstants.TRANSFER_TYPES.INTERNATIONAL_ACCOUNT,
      OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER
    ];

    actionKeys.forEach(function (actionKey) {
      actionObj = {
        key : actionKey
      };
      if (scopeObj.getTransferTypeVisibility(actionKey)) {
        if (actionKey === OLBConstants.TRANSFER_TYPES.WIRE_TRANSFER) {
          actionObj.action = function () {
            var WireTransferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("WireTransferModule");
            WireTransferModule.presentationController.showWireTransfer();
          };
        } else {
          actionObj.action = function (fromAccount) {
            scopeObj.presenter.loadAccountsByTransferType(actionKey, null, fromAccount);
          };
        }
        transferTypes.push(actionObj);
      }
    });

    return transferTypes;
  },

  /**
   * setChangeTransferTypeView : Method to bind Right Transfer Type Actions 'Change Transfer Type' View/Flex
   * @member of {frmTransfersController}
   * @param {Array} actions, action object {key, action} array
   * @param {Number} fromAccount, from Account for all transfer actions
   * @throws {}
   */
  setChangeTransferTypeView: function(actions, dataInputs ) {
    var scopeObj = this;
    var transferTypeData = [];
    var actionObj;
    var widgetDataMap = null;

    dataInputs = dataInputs || {
      headerText : kony.i18n.getLocalizedString("i18n.Transfers.ChangeTransferType")
    };

    var UIMap = {
      OWN_INTERNAL_ACCOUNTS: {
        displayName: kony.i18n.getLocalizedString("i18n.transfers.toMyKonyBankAccounts").toUpperCase(),
        toolTip: kony.i18n.getLocalizedString("i18n.transfers.toMyKonyBankAccounts")
      },
      OTHER_INTERNAL_MEMBER: {
        displayName: kony.i18n.getLocalizedString("i18n.transfers.toOtherKonyBankAccounts").toUpperCase(),
        toolTip: kony.i18n.getLocalizedString("i18n.transfers.toOtherKonyBankAccounts")
      },
      OTHER_EXTERNAL_ACCOUNT: {
        displayName: kony.i18n.getLocalizedString("i18n.transfers.toOtherBankAccounts").toUpperCase(),
        toolTip: kony.i18n.getLocalizedString("i18n.transfers.toOtherBankAccounts")
      },
      INTERNATIONAL_ACCOUNT: {
        displayName: kony.i18n.getLocalizedString("i18n.transfers.toInternationalAccounts").toUpperCase(),
        toolTip: kony.i18n.getLocalizedString("i18n.transfers.toInternationalAccounts")
      },
      WIRE_TRANSFER: {
        displayName: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer").toUpperCase(),
        toolTip: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
      }
    };

    if (actions.length > 0) {
      widgetDataMap = {
        "flxTransferType": "flxTransferType",
        "lblTransferType": "lblTransferType",
        "lblSeperator": "lblSeperator"
      };

      transferTypeData = actions.map(function (actionObj, i) {
        return {
          "template": "flxChangeTransferType",
          "lblTransferType": {
            "text" :commonUtilities.changedataCase(UIMap[actionObj.key].displayName),
            "toolTip": commonUtilities.changedataCase(UIMap[actionObj.key].toolTip),
          },
          "flxTransferType": {
            "toolTip": commonUtilities.changedataCase(UIMap[actionObj.key].toolTip),
            "onClick": actionObj.action.bind(null, dataInputs.fromAccount)
          },
          "lblSeperator": "lblSeperator"
        };
      });

      scopeObj.view.lblChangeTransferTypeHeader.text = dataInputs.headerText;
      scopeObj.view.lblChangeTransferTypeHeader.toolTip = dataInputs.headerText;
      scopeObj.view.segChangeTransferType.widgetDataMap = widgetDataMap;
      scopeObj.view.segChangeTransferType.setData(transferTypeData);
      scopeObj.view.segChangeTransferType.setVisibility(true);
      scopeObj.view.flxChangeTransferType.setVisibility(true); //show Change Tranfers form.

    } else {
      //Hide Complete Flex/UI
      scopeObj.view.flxChangeTransferType.setVisibility(false);
    }
  },
  getPageHeight: function () {
    var height =  this.view.flxHeader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height + 40;
    return height + "dp";
  }
}
});