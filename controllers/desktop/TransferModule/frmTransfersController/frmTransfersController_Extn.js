define(['commonUtilities','OLBConstants'],function(commonUtilities,OLBConstants){
  return {
  initialLoadingDone: false,
  transfersViewModel: {
    transactionsData: [],
    first:0,
    last:10
  },
  getNewTransfersData: function (data, onCancelCreateTransfer) {
    this.transfersViewModel.transactionsData = data.map(this.createNewTransfersData.bind(this, onCancelCreateTransfer))
  },
  repeatTransaction : function(onCancelCreateTransfer){
    kony.print("repeat transaction");
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    var selectedData = data[index];
    var TransactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('Transactions');
    var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var transactionObject = new transactionsModel({
      "transactionId": selectedData.lblReferenceNumberValue,
    });
    transactionObject.amount = (selectedData.lblAmount).slice(1).replace(/,/g,"");
    transactionObject.scheduledDate = selectedData.lblDate;
    transactionObject.fromAccountNumber = selectedData.fromAccountNumber;
    transactionObject.notes = selectedData.lblNoteValue;
    transactionObject.numberOfRecurrences = selectedData.lblRecurrenceValue;
    transactionObject.toAccountNumber = selectedData.toAccountNumber;
    transactionObject.toAccountName = selectedData.lblSendTo;
    transactionObject.fromAccountName = selectedData.lblFromAccountValue;
    transactionObject.transactionType = selectedData.transactionType;
    transactionObject.frequencyType = selectedData.frequencyType;
    transactionObject.ExternalAccountNumber = selectedData.externalAccountNumber;
    this.presenter.repeatTransfer(transactionObject, onCancelCreateTransfer);
    this.AdjustScreen();
  },
  viewTransactionReport : function(){
    kony.print("view report clicked (in viewTransactionReport)");
    this.AdjustScreen();
  },
  editScheduledTransaction: function(transaction, onCancelCreateTransfer){
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    var selectedData = data[index];
    var record = {
      "amount": transaction.amount,
      "frequencyEndDate": transaction.frequencyEndDate,
      "frequencyStartDate": selectedData.frequencyStartDate,
      "frequencyType": selectedData.frequencyType,
      "fromAccountNumber": selectedData.fromAccountNumber,
      "isScheduled": "1",
      "numberOfRecurrences": selectedData.lblRecurrenceValue,
      "scheduledDate": transaction.scheduledDate,
      "toAccountNumber": selectedData.toAccountNumber,
      "transactionDate": selectedData.lblDate,
      "ExternalAccountNumber": selectedData.externalAccountNumber,
      "transactionId": selectedData.lblReferenceNumberValue,
      "transactionsNotes": selectedData.lblNoteValue ,
      "transactionType": selectedData.transactionType,
      "category": selectedData.category
    };
    this.presenter.showMakeTransferForEditTransaction(record, onCancelCreateTransfer);
    this.AdjustScreen();
  },
  deleteTransaction: function() {
    kony.print("delete transaction");
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    var selectedData = data[index];
    var TransactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('Transactions');
    var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
    var transactionObject = new transactionsModel({
      "transactionId": selectedData.lblReferenceNumberValue,
    });
    var scopeObj = this;
    scopeObj.view.deletePopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
    scopeObj.view.deletePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("I18n.billPay.QuitTransactionMsg");
    var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMain.frame.height;
    scopeObj.view.flxDelete.height = height + "dp";
    scopeObj.view.flxDelete.left = "0%";
    this.view.deletePopup.btnYes.onClick = function() {
      scopeObj.view.flxDelete.left = "-100%";
      scopeObj.presenter.deleteTransfer(this, transactionObject);
    }
    this.view.deletePopup.btnNo.onClick = function() {
      kony.print("btn no pressed");
      scopeObj.view.flxDelete.left = "-100%";
    }
    this.view.deletePopup.flxCross.onClick = function() {
      kony.print("btn no pressed");
      scopeObj.view.flxDelete.left = "-100%";
    }
    this.AdjustScreen();
  },
  createNewTransfersData: function (onCancelCreateTransfer,transaction) {
    var scopeObj = this;
    if(transaction.transactionsNotes===undefined || transaction.transactionsNotes===null)
      transaction.transactionsNotes="None";
    return {
      btnAction: {
        text: kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
        toolTip:kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
        onClick: this.viewTransactionReport,
      },
      btnRecentRepeat: {
        text: kony.i18n.getLocalizedString("i18n.transfers.repeat"),
        toolTip:kony.i18n.getLocalizedString("i18n.transfers.repeat"),
        onClick: this.repeatTransaction.bind(this, onCancelCreateTransfer),
      },
      btnScheduledRepeat: {
        text: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
        toolTip: kony.i18n.getLocalizedString("i18n.billPay.Edit"),
        onClick: this.editScheduledTransaction.bind(this, transaction, onCancelCreateTransfer),
      },
      btnScheduledDelete: {
        text:kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
        toolTip:kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
        onClick: this.deleteTransaction,
      },
      "flxDropdown": "flxDropdown",
      "imgDropdown": "arrow_down.png",
      "recentTemplate": "flxRecentTransfers",
      "scheduledTemplate": "flxScheduledTransfers",
      "lblFromAccountTitle": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
      "lblRecurrenceTitle":kony.i18n.getLocalizedString("i18n.transfers.lblRecurrences"),
      "lblReferenceNumberTitle":kony.i18n.getLocalizedString("i18n.billPay.referenceNumber"),
      "lblSeparator": "lblSeparator",
      "lblStatusTitle":kony.i18n.getLocalizedString("i18n.Transfers.Statustitle"),
      "lblNoteTitle": kony.i18n.getLocalizedString("i18n.transfers.note"),
      "category":transaction.category,
      "frequencyType": transaction.frequencyType,
      "transactionType": transaction.transactionType,	
      "fromAccountNumber": transaction.fromAccountNumber,
      "toAccountNumber": transaction.toAccountNumber,
      "lblAmount": scopeObj.presenter.formatCurrency(transaction.amount),
      "externalAccountNumber":transaction.ExternalAccountNumber, 
      "lblSendTo": transaction.toAccountName,
      "lblLatestScheduledTransaction": this.getDateFromDateStr(transaction.transactionDate),
      "newRecurrenceValue":(transaction.recurrenceDesc||' '),
      "lblDate": this.getDateFromDateStr(transaction.transactionDate),
      "scheduledDate":this.getDateFromDateStr(transaction.transactionDate),
      "lblStatusValue": transaction.statusDescription,
      "lblReferenceNumberValue": transaction.transactionId,
      "lblRecurrenceValue": transaction.numberOfRecurrences,
      "lblFromAccountValue": transaction.fromAccountName,
      "lblNoteValue": transaction.transactionsNotes,
    };
  },
  setPaginationPrevious_Recent: function() {
    if (this.first <= 0) {
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_inactive.png";
    } else {
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = this.getPreviousRecentTransactions;
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_active.png";
    }
  },
  setPaginationNext_Recent: function() {
    this.view.transfermain.tablePagination.flxPaginationNext.onClick = this.getNextRecentTransactions;
    this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_active.png";
  },
  setRecentTransactionsPagination: function() {
    this.setPaginationPrevious_Recent();
    this.setPaginationNext_Recent();
    this.view.transfermain.tablePagination.lblPagination.text = (this.first + 1) + '-' + (this.first + this.transfersViewModel.transactionsData.length)+' Transactions';
    if(this.transfersViewModel.transactionsData.length < 10){
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function(){};
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_inactive.png";
    } 
  },
  setPaginationPrevious_Scheduled: function() {
    if (this.first <= 0) {
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_inactive.png";
    } else {
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = this.getPreviousScheduledTransactions;
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_active.png";
    }
  },
  setPaginationNext_Scheduled: function() {
    this.view.transfermain.tablePagination.flxPaginationNext.onClick = this.getNextScheduledTransactions;
    this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_active.png";
  },
  setScheduledTransactionsPagination: function() {
    this.setPaginationPrevious_Scheduled();
    this.setPaginationNext_Scheduled();
    this.view.transfermain.tablePagination.lblPagination.text = (this.first + 1) + '-' + (this.first + this.transfersViewModel.transactionsData.length)+' Transactions';
    if(this.transfersViewModel.transactionsData.length < 10){
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function(){};
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_inactive.png";
    } 
  },
  getDateFromDateStr:function(dateStr)
  {
    if(dateStr) {
      return commonUtilities.getFrontendDateString(dateStr,commonUtilities.getConfiguration('frontendDateFormat'));
     }
    else {
      return "";
    }
  },
  showRecentsData: function(data, config) {
    // Releasing Initial Lock
    this.initialLoadingDone = true;
    this.view.breadcrumb.setBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.transfers.transfer")
    }, {
      text: kony.i18n.getLocalizedString("i18n.transfers.recent")
    }]);
	this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.recent");
    var scopeObj = this;
    this.sortFlex("Recent");
    this.view.transfermain.flxRowSeperator.setVisibility(true);
    if ((data == undefined) || ((data instanceof Array) && data.length === 0)) this.showNoTransactions();
    else this.showSegment();
    this.view.transfermain.btnRecent.skin = "sknBtnAccountSummarySelected";
    this.getNewTransfersData(data, this.getUserRecentTransactions);
    this.setRecentTransactionsPagination();
    this.view.transfermain.flxSortExternal.setVisibility(false);
    this.view.transfermain.flxSort.setVisibility(true);
    this.setSearchFlexVisibility(false);
    var recentsDataMap = {
      "btnRepeat": "btnRecentRepeat",
      "btnAction": "btnAction",
      "flxDropdown": "flxDropdown",
      "imgDropdown": "imgDropdown",
      "lblAmount": "lblAmount",
      "template": "recentTemplate",
      "lblDate": "lblDate",
      "lblFromAccountTitle": "lblFromAccountTitle",
      "lblFromAccountValue": "lblFromAccountValue",
      "lblIdentifier": "lblIdentifier",
      "lblNoteTitle": "lblNoteTitle",
      "lblNoteValue": "lblNoteValue",
      "lblRecurrenceTitle": "lblRecurrenceTitle",
      "lblRecurrenceValue": "newRecurrenceValue",
      "lblReferenceNumberTitle": "lblReferenceNumberTitle",
      "lblReferenceNumberValue": "lblReferenceNumberValue",
      "lblSendTo": "lblSendTo",
      "lblSeparator": "lblSeparator",
      "lblStatusTitle": "lblStatusTitle",
      "lblStatusValue": "lblStatusValue",
    };
    this.view.transfermain.segmentTransfers.widgetDataMap = recentsDataMap;
    this.view.transfermain.segmentTransfers.setData(this.transfersViewModel.transactionsData);
    commonUtilities.Sorting.setSortingHandlers(this.recentAndScheduledSortMap, this.onRecentSortClickHandler, this);
    commonUtilities.Sorting.updateSortFlex(this.recentAndScheduledSortMap, config);
    commonUtilities.hideProgressBar(this.view);
    this.view.forceLayout();
  },
  onFrequencyChanged: function () {
    this.view.transfermain.maketransfer.getFrequencyAndFormLayout(this.view.transfermain.maketransfer.lbxFrequency.selectedKey,
      this.view.transfermain.maketransfer.lbxForHowLong.selectedKey);
    this.checkValidityMakeTransferForm();
  },
  onHowLongChange: function () {
    this.view.transfermain.maketransfer.getForHowLongandFormLayout(this.view.transfermain.maketransfer.lbxForHowLong.selectedKey);
    this.checkValidityMakeTransferForm();
  },
  checkValidityMakeTransferForm: function () {

    var disableCreateTransferButton = function () {
      this.view.transfermain.maketransfer.btnConfirm.setEnabled(false);
      this.view.transfermain.maketransfer.btnConfirm.skin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.transfermain.maketransfer.btnConfirm.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";  
      this.view.transfermain.maketransfer.btnConfirm.focusSkin="sknBtnBlockedLatoFFFFFF15Px";        
    }.bind(this)

    var formData = this.getFormData({});
    if (formData.amount === null || formData.amount === "") {
      disableCreateTransferButton();
      return;
    }
    if(formData.frequencyKey !== "Once" && formData.howLongKey === "NO_OF_RECURRENCES" && formData.noOfRecurrences === "") {
      disableCreateTransferButton();
      return;
    }
    this.view.transfermain.maketransfer.btnConfirm.skin = "sknbtnLatoffffff15px";
    this.view.transfermain.maketransfer.btnConfirm.setEnabled(true);
    this.view.transfermain.maketransfer.btnConfirm.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
    this.view.transfermain.maketransfer.btnConfirm.focusSkin="sknBtnFocusLatoFFFFFF15Px";


  },
  recentsSegmentRowClick: function () {
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    kony.print("index:" + index);
    if (data[index].template == "flxRecentTransfers") {
      data[index].imgDropdown = "chevron_up.png";
      data[index].template = "flxRecentTransfersSelected";
    } else {
      data[index].imgDropdown = "arrow_down.png";
      data[index].template = "flxRecentTransfers";
    }
    this.view.transfermain.segmentTransfers.setData(data);
  },
  getUserRecentTransactions: function () {
    this.first = 0;
    this.last = 10;
    commonUtilities.showProgressBar(this.view);
    this.presenter.fetchRecentUserTransactions(this, {
      "offset": this.first,
      "limit": this.last,
      'resetSorting': true
    });
  },
  getUserScheduledTransactions: function () {
    this.first = 0;
    this.last = 10;
    commonUtilities.showProgressBar(this.view);
    this.presenter.fetchScheduledUserTransactions(this, {
      "offset": this.first,
      "limit": this.last,
      'resetSorting': true
    });
  },
  getNextRecentTransactions: function () {
    this.first += 10;
    this.last = 10;
    this.view.transfermain.tablePagination.imgPaginationPrevious.src = "pagination_back_active.png";
    this.presenter.fetchRecentUserTransactions(this, {
      "offset": this.first,
      "limit": this.last
    });
  },
  getPreviousRecentTransactions: function () {
    this.first -= 10;
    this.last = 10;
    if (this.first >= 0) this.presenter.fetchRecentUserTransactions(this, {
      "offset": this.first,
      "limit": this.last
    });
    else {
      this.first = 0;
      this.last = 10;
      this.view.transfermain.tablePagination.imgPaginationPrevious.src = "pagination_back_inactive.png";
    }
  },
  getNextScheduledTransactions: function () {
    this.first += 10;
    this.last = 10;
    this.view.transfermain.tablePagination.imgPaginationPrevious.src = "pagination_back_active.png";
    this.presenter.fetchScheduledUserTransactions(this, {
      "offset": this.first,
      "limit": this.last
    });
  },
  getPreviousScheduledTransactions: function () {
    this.first -= 10;
    this.last = 10;
    if (this.first >= 0) this.presenter.fetchScheduledUserTransactions(this, {
      "offset": this.first,
      "limit": this.last
    });
    else {
      this.first = 0;
      this.last = 10;
      this.view.transfermain.tablePagination.imgPaginationPrevious.src = "pagination_back_inactive.png";
    }
  },
  showScheduledData: function (data, config) {
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfers')}, {text:kony.i18n.getLocalizedString("i18n.transfers.scheduled")}]);
	this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.scheduled");
    if ((data == undefined) || ((data instanceof Array) && data.length === 0)) this.showNoTransactions();
    else this.showSegment();
    this.sortFlex("Recent");        
    this.view.transfermain.btnScheduled.skin = "sknBtnAccountSummarySelected";
    this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () {};
    this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () {};
    this.view.transfermain.flxRowSeperator.setVisibility(true);
    this.getNewTransfersData(data, this.getUserScheduledTransactions);
    this.setScheduledTransactionsPagination();
    this.view.transfermain.flxSortExternal.setVisibility(false);
    this.view.transfermain.flxSort.setVisibility(true);
    this.setSearchFlexVisibility(false);
    var scheduledDataMap = {
      "btnRepeat": "btnScheduledDelete",
      "btnAction": "btnScheduledRepeat",
      "flxDropdown": "flxDropdown",
      "imgDropdown": "imgDropdown",
      "lblAmount": "lblAmount",
      "template": "scheduledTemplate",
      "lblDate": "lblDate",
      "lblFromAccountTitle": "lblFromAccountTitle",
      "lblFromAccountValue": "lblFromAccountValue",
      "lblIdentifier": "lblIdentifier",
      "lblNoteTitle": "lblNoteTitle",
      "lblNoteValue": "lblNoteValue",
      "lblRecurrenceTitle": "lblRecurrenceTitle",
      "lblRecurrenceValue": "lblRecurrenceValue",
      "lblReferenceNumberTitle": "lblReferenceNumberTitle",
      "lblReferenceNumberValue": "lblReferenceNumberValue",
      "lblSendTo": "lblSendTo",
      "lblSeparator": "lblSeparator",
      "lblStatusTitle": "lblStatusTitle",
      "lblStatusValue": "lblStatusValue",
    };
    this.view.transfermain.segmentTransfers.widgetDataMap = scheduledDataMap;
    this.view.transfermain.segmentTransfers.setData(this.transfersViewModel.transactionsData);
    commonUtilities.Sorting.setSortingHandlers(this.recentAndScheduledSortMap, this.onScheduledSortClickHandler, this);
    commonUtilities.Sorting.updateSortFlex(this.recentAndScheduledSortMap, config);
    commonUtilities.hideProgressBar(this.view);
    this.view.forceLayout();
  },
  createAccountSegmentsModel: function (accounts) {
    var accountTypeConfig = {
      'Savings': {
        skin: "sknFlx26D0CE",
        balanceKey: 'availableBalance',
        balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
      },
      'Checking': {
        skin: "sknFlx9060b7",
        balanceKey: 'availableBalance',
        balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
      },
      'CreditCard': {
        skin: "sknFlxF4BA22",
        balanceKey: 'currentBalance',
        balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
      },
      'Deposit': {
        skin: "sknFlx4a90e2Border1pxRound",
        balanceKey: 'currentBalance',
        balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
      },
      'Mortgage': {
        skin: 'sknFlx4a90e2Border1pxRound',
        balanceKey: 'currentBalance',
        balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
      },
      'Default': {
        skin: 'sknFlx26D0CE',
        balanceKey: 'availableBalance',
        balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
      }
    };
    var balanceInDollars = function (amount) {
      return '$' + amount;
    };
    return Object.keys(accountTypeConfig).map(function (accountType) {
      return accounts.filter(function (account) {
        return account.type === accountType;
      });
    }).map(function (accounts) {
      return accounts.map(function (account) {
        return {

          "flxLeft": {
            "skin": accountTypeConfig[account.type].skin
          },
          "lblLeft": " ",
          "lblAccountName": account.nickName || account.accountName,
          "lblBalance": account[accountTypeConfig[account.type].balanceKey],
          "lblAvailableBalance": accountTypeConfig[account.type].balanceTitle
        };
      });
    }).reduce(function (p, e) {
      return p.concat(e);
    }, []);
  },

  willUpdateUI: function (viewModel) {
    if (viewModel == undefined) { }
    else if (viewModel.serverError) {
      this.showServerError(viewModel.serverError);
    } else {
      if(viewModel.isLoading!==undefined)this.changeProgressBarState(viewModel.isLoading);
      if (viewModel.showRecentTransfers) this.resetFormForRecentTransfers();
      if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
      if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
      if (viewModel.accounts) this.updateTransferAccountList(viewModel);
      if (viewModel.makeTransfer) this.updateMakeTransferForm(viewModel.makeTransfer);
      if (viewModel.recentTransfers) this.showRecentsData(viewModel.recentTransfers, viewModel.config);
      if (viewModel.scheduledTransfers) this.showScheduledData(viewModel.scheduledTransfers, viewModel.config);
      if(viewModel.externalAccounts) this.showExternalAccounts(viewModel.externalAccounts);
      if(viewModel.viewSelectedExternalAccount) this.showSelectedExternalAccount(viewModel.viewSelectedExternalAccount);
      if(viewModel.viewExternalAccountTransactionActivity) this.showExternalAccountTransactionActivity(viewModel.viewExternalAccountTransactionActivity);
      if(viewModel.searchTransferPayees) {
        this.showSearchTransferPayees(viewModel.searchTransferPayees);
      }
      if (this.initialLoadingDone) {
      commonUtilities.hideProgressBar(this.view);    
    }
    }
    this.AdjustScreen();
    this.view.forceLayout();
    
  },
    
   /**
  * Method to change the the progress bar state
  * @member of frmProfileManagementController
  * @param {JSON} viewModel- None
  * @returns {void} - None
  * @throws {void} -None
  */  
  changeProgressBarState: function (isLoading) {
    if (isLoading) {
      commonUtilities.showProgressBar(this.view);
    } else {
      commonUtilities.hideProgressBar(this.view);
    }
  },
    
  showServerError: function(viewModel){
    var scopeObj = this;
    commonUtilities.hideProgressBar(scopeObj.view);
    scopeObj.view.flxMakeTransferError.setVisibility(true);
    scopeObj.view.rtxMakeTransferError.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
  },
  resetFormForRecentTransfers: function () {
    // Will prevent Progress Bar to hide 
    this.initialLoadingDone = false;
    this.showNoTransactions();
    this.getUserRecentTransactions();
  },

  updateMakeTransferForm: function (makeTransferViewModel) {
    this.showMainTransferWindow();
	var scopeObj=this;
    commonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calSendOn);
    commonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calEndingOn);
    if (makeTransferViewModel.showProgressBar) {
      this.initialLoadingDone = false;
      commonUtilities.showProgressBar(this.view);
    }
    else {
      this.initialLoadingDone = true;
      commonUtilities.hideProgressBar(this.view);
    }
    if(makeTransferViewModel.selectedTransferToType == null) {
      this.configureTransferGateway(makeTransferViewModel);
      this.showTransfersGateway();
    }
    else {
      this.showMakeTransfer();
      if(makeTransferViewModel.toFieldDisabled) {
        this.view.transfermain.maketransfer.lbxToAccount.setEnabled(false);
      }
      else {
        this.view.transfermain.maketransfer.lbxToAccount.setEnabled(true);
      }
      this.view.transfermain.maketransfer.lbxFromAccount.masterData = makeTransferViewModel.accountsFrom;
      this.view.transfermain.maketransfer.lbxFromAccount.selectedKey = makeTransferViewModel.accountFromKey;
      this.view.transfermain.maketransfer.lbxToAccount.masterData = makeTransferViewModel.accountsTo;
      this.view.transfermain.maketransfer.lbxToAccount.selectedKey = makeTransferViewModel.accountToKey;
      this.filterAccountsTo(makeTransferViewModel);
      this.filterFromAccount(makeTransferViewModel);
      this.view.transfermain.maketransfer.lbxFrequency.masterData = makeTransferViewModel.frequencies;
      this.view.transfermain.maketransfer.lbxFrequency.selectedKey = makeTransferViewModel.frequencyKey;
      this.view.transfermain.maketransfer.calSendOn.date = makeTransferViewModel.sendOnDate;
      // Work around for calendar issue
      this.view.transfermain.maketransfer.calSendOn.dateComponents = this.view.transfermain.maketransfer.calSendOn.dateComponents;
      this.view.transfermain.maketransfer.calEndingOn.date = makeTransferViewModel.endOnDate;
      // Work around for calendar issue
      this.view.transfermain.maketransfer.calEndingOn.dateComponents = this.view.transfermain.maketransfer.calEndingOn.dateComponents;      
      this.view.transfermain.maketransfer.lbxForHowLong.masterData = makeTransferViewModel.forHowLong;
      this.view.transfermain.maketransfer.lbxForHowLong.selectedKey = makeTransferViewModel.howLongKey;
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.text = makeTransferViewModel.noOfRecurrences;
      this.view.transfermain.maketransfer.tbxNotes.text = makeTransferViewModel.notes;
      this.view.transfermain.maketransfer.setAmount(makeTransferViewModel.amount);
      this.view.transfermain.maketransfer.formatAndShow(); 
      this.checkValidityMakeTransferForm();
      this.onFrequencyChanged();
      this.view.transfermain.maketransfer.btnConfirm.onClick = function () {
        makeTransferViewModel.transferSubmitButtonListener(this.getFormData(makeTransferViewModel));
      }.bind(this);

      this.view.transfermain.maketransfer.btnModify.onClick = function () {
        makeTransferViewModel.cancelTransaction(makeTransferViewModel);
      }
      var scopeObj = this;
      this.view.transfermain.maketransfer.lbxFromAccount.onSelection = this.filterAccountsTo.bind(this, makeTransferViewModel);


      if (makeTransferViewModel.error) {
        this.view.transfermain.maketransfer.lblWarning.setVisibility(true);
        this.view.transfermain.maketransfer.lblWarning.text = kony.i18n.getLocalizedString(makeTransferViewModel.error);
      } else {
        this.view.transfermain.maketransfer.lblWarning.setVisibility(false);
      }

      if (makeTransferViewModel.serverError) {
        this.view.flxMakeTransferError.setVisibility(true);
        this.view.rtxMakeTransferError.text = makeTransferViewModel.serverError;
      } else {
        this.view.flxMakeTransferError.setVisibility(false);
      }
    }
  },
  filterFromAccount: function (makeTransferViewModel) {
    var scopeObj = this;
    if (makeTransferViewModel.accountsTo.length === 1 && makeTransferViewModel.accountsFrom.length > 1) {
      var fromAccountMasterData = makeTransferViewModel.accountsFrom.slice().filter(function (fromAccountPair) {
        return  !scopeObj.presenter.MakeTransfer.isSameAccount(fromAccountPair[0], makeTransferViewModel.accountsTo[0][0])
      })
      if (fromAccountMasterData.length < makeTransferViewModel.accountsFrom.length) {
        scopeObj.view.transfermain.maketransfer.lbxFromAccount.masterData = fromAccountMasterData;      
      }
    }
  },
  filterAccountsTo: function (makeTransferViewModel) {
    var scopeObj = this;
    var fromSelectedIndex = scopeObj.view.transfermain.maketransfer.lbxFromAccount.selectedKey;
    if (makeTransferViewModel.accountsTo.length > 1) {
      var toAccountMasterData = makeTransferViewModel.accountsTo.slice().filter(function (toAccountPair) {
        return  !scopeObj.presenter.MakeTransfer.isSameAccount(fromSelectedIndex, toAccountPair[0])
       })
       if (toAccountMasterData.length < makeTransferViewModel.accountsTo.length)
         scopeObj.view.transfermain.maketransfer.lbxToAccount.masterData = toAccountMasterData;
    }
  },
	
  configureTransferGateway: function (makeTransferViewModel) {
    var self = this;
    var transferToViews = this.view.transfermain.flxTransfersGateway.widgets();
    var transferGatewayConfig = makeTransferViewModel.transferGatewayConfig;

    function createCallback (transferType, visible) {
      // Blocking Wire Transfer not part of the scope
      if(transferType === 'WIRE_TRANSFER') {
        return function () {

        }
      }
      if (visible) {
        return function () {
          makeTransferViewModel.transferToTypeListener(transferType);                
        };
      }
      else {
        return function () {
          self.showNoAccountsAvailableUI(transferType);                
        };
      }
    }

    for (var index = 0; index < transferGatewayConfig.length; index++) {
      var transferToView = transferToViews[index+1]
      var gatewayConfig = transferGatewayConfig[index]
      transferToView.setVisibility(true);
      var infoWidgets = transferToView.widgets()[0].widgets();
      infoWidgets[0].src = gatewayConfig.image;
      infoWidgets[1].text = kony.i18n.getLocalizedString(gatewayConfig.title);
      infoWidgets[2].text = kony.i18n.getLocalizedString(gatewayConfig.info[0]);
      infoWidgets[3].text = kony.i18n.getLocalizedString(gatewayConfig.info[1]);
      var buttonProceed = transferToView.widgets()[1].widgets()[0];
      buttonProceed.onClick = createCallback(gatewayConfig.transferType, gatewayConfig.visible);
    }
  },

  getFormData: function (viewModel) {
    viewModel.accountFromKey = this.view.transfermain.maketransfer.lbxFromAccount.selectedKey;
    viewModel.accountToKey =  this.view.transfermain.maketransfer.lbxToAccount.selectedKey;
    viewModel.amount= this.view.transfermain.maketransfer.getAmount();
    viewModel.frequencyKey= this.view.transfermain.maketransfer.lbxFrequency.selectedKey;
    viewModel.howLongKey= this.view.transfermain.maketransfer.lbxForHowLong.selectedKey;
    viewModel.sendOnDate= this.view.transfermain.maketransfer.calSendOn.date;
    viewModel.sendOnDateComponents= this.view.transfermain.maketransfer.calSendOn.dateComponents;
    viewModel.endOnDate= this.view.transfermain.maketransfer.calEndingOn.date;
    viewModel.endOnDateComponents= this.view.transfermain.maketransfer.calEndingOn.dateComponents;
    viewModel.noOfRecurrences= this.view.transfermain.maketransfer.tbxNoOfRecurrences.text.trim();
    viewModel.notes= this.view.transfermain.maketransfer.tbxNotes.text.trim();
    return viewModel;
  },

  updateTransferAccountList: function (accountModel) {
    this.view.mypaymentAccounts.segMypaymentAccounts.widgetDataMap = {
      "lblLeft": "lblLeft",
      "lblAccountName": "lblAccountName",
      "flxLeft": "flxLeft",
      "lblBalance": "lblBalance",
      "lblAvailableBalance": "lblAvailableBalance"
    };
    var accounts=[]; 
    var scopeObj=this;
    accounts=scopeObj.filterAccountsBasedOnType(accountModel.accounts);
    this.view.mypaymentAccounts.segMypaymentAccounts.setData(this.createAccountSegmentsModel(accounts));
    this.view.forceLayout();
    kony.print('updated AccountsList From ViewModel.', accountModel);
  },
  filterAccountsBasedOnType:function(accounts){
    filteredAccounts=[];
    accounts.find(function(e){
      if(e.type==="Savings"||e.type==="Checking"||e.type==="Current"){
        filteredAccounts.push(e);
      }
    });
    return filteredAccounts;
  },
  initTabsActions: function () {
    var scopeObj = this;
    this.view.transfermain.Search.txtSearch.width = "100%";
    this.view.transfermain.imgSearch.src = "search_blue.png";
    this.setSearchFlexVisibility(false);
    this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    /// start
    this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
    this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
    this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
    //// End
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    this.view.flxTransferViewReport.isVisible = false;
    this.fixBreadcrumb();
    this.view.transfermain.btnMakeTransfer.onClick = function () {this.presenter.showTransferScreen()}.bind(this);
    this.view.transfermain.btnExternalAccounts.onClick=function(){this.presenter.showExternalAccounts()}.bind(this);
    this.view.transfermain.btnExternalAccounts.toolTip = kony.i18n.getLocalizedString('i18n.Transfers.Recipients');
    this.view.transfermain.btnRecent.onClick=function(){this.getUserRecentTransactions()}.bind(this);
    this.view.transfermain.btnScheduled.onClick=function(){this.getUserScheduledTransactions()}.bind(this);
    this.setHeaderActions();

    this.view.transfermain.maketransfer.lbxForHowLong.onSelection = this.onHowLongChange.bind(this);
    this.view.transfermain.maketransfer.lbxFrequency.onSelection = this.onFrequencyChanged.bind(this);
    this.view.transfermain.maketransfer.tbxAmount.onKeyUp = this.onAmountKeyUp.bind(this);
    this.view.transfermain.maketransfer.tbxAmount.onTextChange = this.onAmountChanged.bind(this);    
    this.view.transfermain.maketransfer.tbxNoOfRecurrences.onKeyUp = this.checkValidityMakeTransferForm.bind(this);
    this.view.transfermain.maketransfer.btnModify.text = "Cancel";

  },
  onAmountKeyUp: function () {
    this.view.transfermain.maketransfer.setAmount(this.view.transfermain.maketransfer.tbxAmount.text); 
    this.checkValidityMakeTransferForm();    
  },

  onAmountChanged: function () {
    this.view.transfermain.maketransfer.formatAndShow();
    this.checkValidityMakeTransferForm();
  },
  
  setHeaderActions: function () {
    var scopeObj = this;
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMain.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CustomPopup.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    }
    this.view.CustomPopup.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    }
  },
  fixBreadcrumb: function () {
    this.view.breadcrumb.setBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.transfers.transfers")
    }, {
      text: kony.i18n.getLocalizedString("i18n.transfers.make_transfer")
    }]);
   this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
   this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.make_transfer");	
  },
  hideAll: function () {
    this.view.transfermain.btnMakeTransfer.skin = "sknBtnAccountSummaryUnselectedmod";
    this.view.transfermain.btnRecent.skin = "sknBtnAccountSummaryUnselected";
    this.view.transfermain.btnScheduled.skin = "sknBtnAccountSummaryUnselected";
    this.view.transfermain.btnExternalAccounts.skin = "sknBtnAccountSummaryUnselected";
    this.view.flxMakeTransferError.setVisibility(false);
    this.view.transfermain.flxSort.setVisibility(false);
    this.view.transfermain.flxMakeTransferForm.setVisibility(false);
    this.view.transfermain.segmentTransfers.setVisibility(false);
    this.view.transfermain.flxNoTransactions.setVisibility(false);
    this.view.transfermain.tablePagination.setVisibility(false);
    this.view.transfermain.flxTransfersGateway.setVisibility(false);
    this.view.transfermain.flxSortExternal.setVisibility(false);
    this.view.transferactivity.isVisible=false;
  },
  showMainTransferWindow: function () {
    this.view.flxTrasfersWindow.setVisibility(true);
    this.view.flxNoAccounts.setVisibility(false);
  },
  showTransfersGateway: function () {
    this.hideAll();
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfers')}, {text:kony.i18n.getLocalizedString("i18n.transfers.make_transfer")}]);
	 this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.make_transfer");
    this.view.transfermain.btnMakeTransfer.skin = "sknBtnAccountSummarySelectedmod";
    this.view.transfermain.flxTransfersGateway.setVisibility(true);
    this.setSearchFlexVisibility(false);
    this.view.transfermain.flxRowSeperator.setVisibility(false);
    this.view.forceLayout();
  },
  showMakeTransfer: function () {
    this.hideAll();
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfers')}, {text:kony.i18n.getLocalizedString("i18n.transfers.make_transfer")}]);
	 this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.make_transfer");
    this.view.transfermain.btnMakeTransfer.skin = "sknBtnAccountSummarySelectedmod";
    this.view.transfermain.flxMakeTransferForm.setVisibility(true);
    this.view.transfermain.flxRowSeperator.setVisibility(false);
    this.setSearchFlexVisibility(false);

    this.view.forceLayout();
  },
  showNoTransactions: function (context) {
    this.hideAll();
    var self=this;
    this.view.transfermain.flxNoTransactions.setVisibility(true);
    if(context==="EXTERNAL ACCOUNT"){
       this.view.transfermain.lblScheduleAPayment.setVisibility(false);
       this.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.Transfer.noExternalAccount'); 
    }else{
    this.view.transfermain.lblScheduleAPayment.setVisibility(true);
    this.view.transfermain.lblScheduleAPayment.onTouchEnd=function(){
         self.presenter.showTransferScreen();
    };  
    this.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.transfers.noTransactions');     
  }
    this.view.forceLayout();
  },
  showSegment: function () {
    this.hideAll();
    this.view.transfermain.flxSort.setVisibility(true);
    this.view.transfermain.segmentTransfers.setVisibility(true);
    this.view.transfermain.tablePagination.setVisibility(true);
    this.view.forceLayout();
  },
  showNoAccountsAvailableUI: function (transferType) {
    function isExternal(type) {
      var externalAccountTypes = ['OTHER_EXTERNAL_ACCOUNT', 'WIRE_TRANSFER'];
      return externalAccountTypes.indexOf(type) > 0;
    }

    function getButtonConfig() {
      if (transferType === "OWN_INTERNAL_ACCOUNTS") {
        return {
          text:'i18n.transfers.add_kony_account',
          description: 'i18n.transfers.noMyInternalAccount',
          navigator: function () {

          } 
        }
      }
      if (transferType === "OTHER_INTERNAL_MEMBER") {
        return {
          text:'i18n.transfers.add_kony_account',
          description: 'i18n.transfers.noOtherInternalAccounts',                    
          navigator: function () {
            var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            trasferModule.presentationController.showSameBankAccounts(); 
          } 
        }
      }
      if(transferType === "INTERNATIONAL_ACCOUNT") {
        return  {
          text:'i18n.transfers.add_international_account',
          description: 'i18n.transfers.noInternationalAccount',                                                            
          navigator: function () {
            var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            trasferModule.presentationController.showInternationalAccounts(); 
          } 
        }
      }
      if(transferType === "OTHER_EXTERNAL_ACCOUNT") {
        return  {
          text:'i18n.transfers.add_non_kony_account',
          description: 'i18n.transfers.noExternalAccount',                                                            
          navigator: function () {
            var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            trasferModule.presentationController.showDomesticAccounts(); 
          } 
        }
      }
      else {
        return  {
          text:'i18n.transfers.add_kony_account',
          description: 'i18n.transfers.noOtherInternalAccounts',                                        
          navigator: function () {
            var trasferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            trasferModule.presentationController.showSameBankAccounts(); 
          } 
        }
      }
    }


    this.view.flxTrasfersWindow.setVisibility(false);
    this.view.flxNoAccounts.setVisibility(true);
    this.view.NoAccounts.btnBack.onClick = function () {
      this.view.flxTrasfersWindow.setVisibility(true);
      this.view.flxNoAccounts.setVisibility(false);
    }.bind(this);
    var buttonConfig = getButtonConfig(transferType);
    this.view.NoAccounts.btnAddAccount.text = kony.i18n.getLocalizedString(buttonConfig.text);
    this.view.NoAccounts.txtMessage.text = kony.i18n.getLocalizedString(buttonConfig.description);
    this.view.NoAccounts.btnAddAccount.onClick = buttonConfig.navigator;

  },
  sortFlex: function (tab) {
    if (tab == "ExternalAcc") {
      this.view.transfermain.flxSortExternal.setVisibility(true);
      this.view.transfermain.flxSort.setVisibility(false);
      this.view.transfermain.lblSortDateExternal.text = kony.i18n.getLocalizedString("i18n.transfers.accountName");
      this.view.transfermain.lblSortDescriptionExternal.text = kony.i18n.getLocalizedString("i18n.WireTransfer.AccountNumber");
      this.view.transfermain.lblSortTypeExternal.text = kony.i18n.getLocalizedString("i18n.Transfers.Statustitle");
    } else if (tab == "Recent") {
      this.view.transfermain.flxSortExternal.setVisibility(false);
      this.view.transfermain.flxSort.setVisibility(true);
      this.view.transfermain.lblSortDate.text = kony.i18n.getLocalizedString("i18n.transfers.transactionDate");
      this.view.transfermain.lblSortDescription.text = kony.i18n.getLocalizedString("i18n.transfers.sentTo");
      this.view.transfermain.lblSortType.text = kony.i18n.getLocalizedString("i18n.transfers.amountlabel");
    }
  },
  addExternalAccount: function () {
    var nav = new kony.mvc.Navigation("frmAddExternalAccount");
    nav.navigate({
      initialTab: "domestic"
    });
  },
  addInternationalAccount: function () {
    var nav = new kony.mvc.Navigation("frmAddExternalAccount");
    nav.navigate({
      initialTab: "international"
    });
  },
  addInternalBankAccount: function () {
    var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
    transferModule.presentationController.showSameBankAccounts();
  },
 
  setNextExternalAccounts:function(paginationValue){
    var self=this;
    this.view.transfermain.tablePagination.flxPaginationNext.onClick = function(){
      commonUtilities.showProgressBar(self.view);
      self.presenter.getExternalAccounts({
        "offset":paginationValue.offset+paginationValue.limit,
        "limit":paginationValue.limit,
        'preservePrevSorting': true
      });
    };
    this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_active.png";
  },

  setPreviousExternalAccounts:function(paginationValue){
    var self=this;
    if(paginationValue.offset<=0){
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_inactive.png";
    }else{
      this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function(){
      	commonUtilities.showProgressBar(self.view);
        self.presenter.getExternalAccounts({
          "offset":paginationValue.offset-paginationValue.limit,
          "limit":paginationValue.limit,
          'preservePrevSorting': true
        });
      };
      this.view.transfermain.tablePagination.flxPaginationPrevious.imgPaginationPrevious.src = "pagination_back_active.png";
    }
  },

  setExternalAccountsPagination: function(viewModel,len) {
    this.view.transfermain.tablePagination.flxPagination.setVisibility(true);    
    this.setNextExternalAccounts(viewModel[len]);
    this.setPreviousExternalAccounts(viewModel[len]);
    this.view.transfermain.tablePagination.lblPagination.text = (viewModel[len].offset + 1) + '-' + (viewModel[len].offset + len) + ' Accounts';
    if (viewModel.length < 11) {
      this.view.transfermain.tablePagination.flxPaginationNext.onClick = function() {};
      this.view.transfermain.tablePagination.flxPaginationNext.imgPaginationNext.src = "pagination_next_inactive.png";
    }
  },

 showExternalAccounts:function(viewModel, hidePagination){
    if(viewModel=="errorExternalAccounts"){
        this.hideAll();
        this.showServerError();
        this.view.transfermain.flxNoTransactions.setVisibility(true);
        this.view.transfermain.rtxNoPaymentMessage.i18n_text= kony.i18n.getLocalizedString("i18n.transfers.errorExternalAccounts");
        this.view.transfermain.lblScheduleAPayment.isVisible=false;
        this.view.forceLayout();
    }else if ((viewModel === undefined) || ((viewModel instanceof Array) && viewModel.length < 2)) {
      this.showNoTransactions("EXTERNAL ACCOUNT");
    }
	else{
      var scopeObj = this;
      this.view.flxMakeTransferError.setVisibility(false);
      this.view.flxTrasfersWindow.isVisible=true;
      this.view.transferactivity.isVisible=false;
      this.sortFlex("ExternalAcc");
      this.showSegment();
      this.view.transfermain.flxRowSeperator.setVisibility(true);
      this.view.transfermain.btnExternalAccounts.skin = "sknBtnAccountSummarySelected";
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.transfers.transfer")
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts")
      }]);
	   this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.external_accounts");
 
      this.view.transfermain.lblSortDate.text = "Account Name";
      //this.view.transfermain.lblSortDescription.text = "Bank Name";
      this.view.transfermain.lblSortType.text = "Amount";
      this.view.transfermain.flxSortExternal.setVisibility(true);
      this.view.transfermain.flxSort.setVisibility(false);
      this.view.forceLayout();
      var externalAccountDataMap = {
        "btnBankDetails": "btnBankDetails",
        "btnDelete": "btnDelete",
        "btnEdit": "btnEdit",
        "btnViewActivity": "btnViewActivity",
        "btnAction": "btnAction",
        "btnCancel": "btnCancel",
        "btnMakeTransfer": "btnMakeTransfer",
        "btnSave": "btnSave",
        "flxDropdown": "flxDropdown",
        "imgDropdown": "imgDropdown",
        "lblAccountHolderTitle": "lblAccountHolderTitle",
        "lblAccountHolderValue": "lblAccountHolderValue",
        "lblAccountName": "lblAccountName",
        "lblAccountNumberTitle": "lblAccountNumberTitle",
        "lblAccountNumberValue": "lblAccountNumberValue",
        "lblAccountTypeTitle": "lblAccountTypeTitle",
        "lblAccountTypeValue": "lblAccountTypeValue",
        "lblAddedOnTitle": "lblAddedOnTitle",
        "lblAddedOnValue": "lblAddedOnValue",
        "lblBankDetailsTitle": "lblBankDetailsTitle",
        "lblBankName": "lblBankName",
        "lblIdentifier": "lblIdentifier",
        "lblRoutingNumberTitle": "lblRoutingNumberTitle",
        "lblRoutingNumberValue": "lblRoutingNumberValue",
        "lblSeparator": "lblSeparator",
        "lblSeparatorActions": "lblSeparatorActions",
        "lblStatus": "lblStatus",
        "txtAccountName": "txtAccountName",
        "txtAccountNumber": "txtAccountNumber",
        "txtAccountType": "txtAccountType"
      };
      var len=viewModel.length;
      function getMappings(context){
        if(context.routingNumberDetails){
          context=context.routingNumberDetails;
          if(context.lblRoutingNumberTitle){
            if(context.lblRoutingNumberTitle.isInternationalAccount=="true"){
              return kony.i18n.getLocalizedString("i18n.accounts.swiftCode");
            }else{
              return kony.i18n.getLocalizedString("i18n.accounts.routingNumber");
            }
          }if(context.lblRoutingNumberValue){
            if(context.lblRoutingNumberValue.isInternationalAccount=="true"){
              return context.lblRoutingNumberValue.swiftCode;
            }else if(context.lblRoutingNumberValue.isSameBankAccount=="false"){
              return context.lblRoutingNumberValue.routingNumber;
            }else{
              return "NA";
            }
          }
        }
        if(context.accountStatus){
          if(context.accountStatus.isVerified=="true"){
            return kony.i18n.getLocalizedString("i18n.transfers.verified");
          }
          else{
            return kony.i18n.getLocalizedString("i18n.transfers.pending");
          }
        }
        if(context.viewActivity){
          if(commonUtilities.getConfiguration("externalTransactionHistory")=='true'&&context.viewActivity.isVerified=="true"){
            return  {
              "text":kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
              "onClick":function(){
                kony.print("viewActivity clicked");
                scopeObj.onBtnViewActivity();
              }
            }  
          }else{
            return {
              "isVisible": false
            }
          }  
        }
        if(context.btnActivityAccountStatus){
          if(context.btnActivityAccountStatus.isVerified=="true"){
            return {
              "text":kony.i18n.getLocalizedString("i18n.transfers.make_transfer"),
              "toolTip":kony.i18n.getLocalizedString("i18n.common.transferToThatPayee"),

              "onClick":function(){
                scopeObj.onBtnMakeTransfer();
              }
            };
          }
          else{
            return {
              "text":kony.i18n.getLocalizedString("i18n.transfers.verifyAccount"),
              "toolTip":kony.i18n.getLocalizedString("i18n.transfers.verifyAccount"),

              "onClick":function(){
                scopeObj.onBtnVerifyAccount();
              }
            };
          }
        }
        if (context.congigkeyPlug) {
          if (commonUtilities.getConfiguration('addExternalAccount') == 'true') {
            return true;
          } else if (commonUtilities.getConfiguration('addExternalAccount') == 'false' && context.congigkeyPlug.isVerified == "true") {
            return true;
          } else {
            return false;
          }
        }
        if(context.addedOn){
          var date=context.addedOn.slice(0,10);
          return date.slice(8,10)+"/"+date.slice(5,7)+"/"+date.slice(0,4);
        }
      }
      if(!hidePagination) {
        this.setExternalAccountsPagination(viewModel,len-1);          //new lOC                
      }

      var data = [];
      for (var i = 0; i < len-1; i++) {
        if (viewModel[i] !== undefined&&getMappings({"congigkeyPlug":viewModel[i]})) {
          data.push({
            "btnDelete": {
              "text": "DELETE",
              "onClick": function() {
                kony.print("external accounts DELETE pressed");
                scopeObj.onExternalAccountDelete();
              }
            },
            "btnEdit": {
              "text": "EDIT",
              "onClick": function() {
                kony.print("external accounts EDIT pressed");
                scopeObj.externalAccountsSegmentRowClickEdit();
              }
            },
            "btnViewActivity": getMappings({"viewActivity":viewModel[i]}),
            "btnAction": getMappings({"btnActivityAccountStatus":viewModel[i]}),
            "btnCancel": {
              "text": kony.i18n.getLocalizedString('i18n.transfers.Cancel'),
              "toolTip": kony.i18n.getLocalizedString('i18n.transfers.Cancel'),
              "onClick": function() {
                kony.print("external accounts CANCEL pressed");
                scopeObj.showUnselectedRow();
              }
            },
            "btnSave": {
              "text": kony.i18n.getLocalizedString('i18n.ProfileManagement.Save'),
              "toolTip": kony.i18n.getLocalizedString('i18n.common.saveChanges'),
              "onClick": function() {
                kony.print("external accounts SAVE pressed");
                scopeObj.saveChangedExternalAccount();
              }
            },
            "imgDropdown": "arrow_down.png",
            "lblAccountHolderTitle": "Account Holder",
            "lblAccountHolderValue": "Individual Account",
            "lblAccountName": viewModel[i].nickName,
            "lblAccountNumberTitle": viewModel[i].accountNumber,
            "lblAccountNumberValue": viewModel[i].accountNumber,
            "lblAccountTypeTitle": "Account Type",
            "lblAccountTypeValue": viewModel[i].accountType,
            "lblAddedOnTitle": "Added On",
            "lblAddedOnValue": getMappings({"addedOn":viewModel[i].createdOn}),
            "lblBankDetailsTitle": "Bank Details",
            "lblBankName": viewModel[i].bankName,
            "lblIdentifier": "lblIdentifier",
            "lblRoutingNumberTitle": getMappings({routingNumberDetails:{lblRoutingNumberTitle:viewModel[i]}}),    //new lOC
            "lblRoutingNumberValue": getMappings({routingNumberDetails:{lblRoutingNumberValue:viewModel[i]}})||"NA",    //new lOC
            "lblSeparator": viewModel[i].beneficiaryName,
            "lblSeparatorActions": "lblSeparatorActions",
            "lblStatus": getMappings({accountStatus:viewModel[i]}),
            
            "template": "flxExternalAccountsTransfersUnselected",
            "txtAccountName": {
              "text": viewModel[i].nickName,
              "placeholder": ""
            },
            "txtAccountNumber": {
              "text": viewModel[i].accountNumber,
              "placeholder": ""
            },
            "txtAccountType": {
              "text": viewModel[i].accountType,
              "placeholder": ""
            }
            
          });
        }
      }
      this.view.transfermain.segmentTransfers.widgetDataMap = externalAccountDataMap;
      this.view.transfermain.segmentTransfers.setData(data);
      commonUtilities.Sorting.updateSortFlex(this.externalAccountsSortMap, viewModel[len-1]);
    }
  },
  
  showUnselectedRow: function(){
     var index = this.view.transfermain.segmentTransfers.selectedIndex[1]; 
     this.showSelectedRow(index);
  },
  
  saveChangedExternalAccount:function(){
    commonUtilities.showProgressBar(this.view);
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data[index];
    var params;
    if(data.txtAccountType.text===data.lblAccountTypeValue&&data.lblStatus==kony.i18n.getLocalizedString("i18n.transfers.verified")){
      params={
        "accountNumber":data.txtAccountNumber.text,
        "accountType":data.txtAccountType.text,
        "nickName":data.txtAccountName.text,
        "isVerified":1
      };
    }else{
      params={
        "accountNumber":data.txtAccountNumber.text,
        "accountType":data.txtAccountType.text,
        "nickName":data.txtAccountName.text,
        "isVerified":0
      };
    } 
    this.presenter.saveChangedExternalAccount(params,data);   
    this.AdjustScreen();
  },

  
   showExternalAccountTransactionActivity: function(viewModel) {
        var self=this;
        this.view.flxTrasfersWindow.isVisible = false;
        this.view.transferactivity.isVisible = true;
        this.view.transferActivitymod.tablePagination.isVisible = false;
        
        this.view.transferActivitymod.flxSort.isVisible = false;
        this.view.transferActivitymod.flxsorttransfers.isVisible = true;
        this.view.transferActivitymod.flxSegmentBillPay.isVisible = false;
        this.view.transferActivitymod.flxSegment.isVisible = true;
        this.view.transferActivitymod.flxStatus.isVisible=false;
        this.view.breadcrumb.setBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.transfers.transfer")
        }, {
            text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts"),
            callback: function() {
            	commonUtilities.showProgressBar(self.view);
            	self.presenter.getExternalAccounts()
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.transfers.viewTransferActivity")
        }]);
        var len = viewModel.length;
        this.view.transferActivitymod.lblAccountName.text = viewModel[len - 1].accountNumber;
        this.view.transferActivitymod.lblAccountHolder.text = viewModel[len - 1].nickName;
        var widgetDataMap = {
            "flxAmount": "flxAmount",
            "flxDate": "flxDate",
            "flxFrom": "flxFrom",
            "flxRunningBalance": "flxRunningBalance",
            "flxSort": "flxSort",
            "flxStatus": "flxStatus",
            "lblAmount": "lblAmount",
            "lblDate": "lblDate",
            "lblFrom": "lblFrom",
            "lblpaiddate": "lblpaiddate",
            "lblRunningBalance": "lblRunningBalance",
            "lblStatus": "lblStatus"
             
        };
        
        var data = [];
        if (len < 2) {
            this.view.transferActivitymod.flxSegment.isVisible = false;
            this.view.transferActivitymod.flxNoRecords.isVisible = true;
            this.view.transferActivitymod.lblAmountDeducted.text = "NA";
        } else {
            for (var i = 0; i < len - 1; i++) {
                data.push({
                    "lblAmount": commonUtilities.formatCurrencyWithCommas(Math.abs(viewModel[i].amount)),
                    "lblDate": viewModel[i].transactionDate.slice(),
                    "lblFrom": viewModel[i].fromNickName,
                    "lblStatus": viewModel[i].statusDescription,
                    "lblpaiddate": commonUtilities.getFrontendDateString(viewModel[i].transactionDate, commonUtilities.getConfiguration("frontendDateFormat")),
                });
            }
            this.view.transferActivitymod.lblAmountDeducted.text = commonUtilities.formatCurrencyWithCommas(viewModel[0].totalAmount);
            this.view.transferActivitymod.segTransferActivity.widgetDataMap = widgetDataMap;
            this.view.transferActivitymod.segTransferActivity.setData(data);
            this.view.transferActivitymod.flxSegment.isVisible = true;
            this.view.transferActivitymod.flxNoRecords.isVisible = false;
        }
    },
  
  verifyAndAddAccount: function(viewModel){
      this.showExternalAccounts(viewModel);      
  }, 
  
 onExternalAccountDelete:function(){
    var scopeObj=this;
      kony.print("btn Delete pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccountMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMain.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    var accountNumber=data[index].txtAccountNumber.text;
    this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString('i18n.common.deleteTheAccount');
    this.view.CustomPopup.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      //function
    scopeObj.presenter.deleteExternalAccount(accountNumber);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString('i18n.common.noDontDelete');
    this.view.CustomPopup.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };  
   this.AdjustScreen();
  },
  
  showSelectedExternalAccount:function(viewModel){
    kony.print(JSON.stringify(viewModel));
    this.showExternalAccounts(viewModel);   //showExternalAccount in case of bhawna
    this.showSelectedRow(viewModel[viewModel.length-1].index);
  },

  showSelectedRow: function(index)
  {
    var data =  this.view.transfermain.segmentTransfers.data;
    for(var i=0;i<data.length;i++)
    {
      if(i==index)
      {kony.print("index:" + index);
       data[i].imgDropdown = "chevron_up.png";
       data[i].template = "flxExternalAccountsTransfersSelected";
      }
      else
      {
        data[i].imgDropdown = "arrow_down.png";
        data[i].template = "flxExternalAccountsTransfersUnselected";
      }
    }  
    this.view.transfermain.segmentTransfers.setData(data);
  }, 

  onBtnViewActivity:function(){
    commonUtilities.showProgressBar(this.view);
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data[index];
    this.presenter.showSelectedAccountTransactions(data);
    this.AdjustScreen();
  },
  
 onBtnMakeTransfer:function(){
     var self=this;
    commonUtilities.showProgressBar(this.view);
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data[index];
    var onCancelCreateTransfer=function(){
      commonUtilities.showProgressBar(this.view);
      self.setSearchFlexVisibility(false);
      self.presenter.showExternalAccounts();
    };
    this.presenter.showTransferScreen({"accountTo":data.txtAccountNumber.text,onCancelCreateTransfer :onCancelCreateTransfer});
    this.AdjustScreen(); 
 },
  
  onBtnVerifyAccount: function(){
    commonUtilities.showProgressBar(this.view);
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    this.presenter.showVerifyAccounts(data[index]);
    this.AdjustScreen();
  },

  externalAccountsSegmentRowClick: function () {
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    kony.print("index:" + index);
    if (data[index].template == "flxRecentTransfers") {
      data[index].imgDropdown = "chevron_up.png";
      data[index].template = "flxExternalAccountsTransfersSelected";
    } else {
      data[index].imgDropdown = "arrow_down.png";
      data[index].template = "flxRecentTransfers";
    }

    this.view.transfermain.segmentTransfers.setData(data);
  },
  externalAccountsSegmentRowClickEdit: function () {
    var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
    var data = this.view.transfermain.segmentTransfers.data;
    kony.print("index:" + index);
    if (data[index].template == "flxExternalAccountsTransfersSelected") {
      data[index].imgDropdown = "chevron_up.png";
      data[index].template = "flxExternalAccountsTransfersEdit";
    } else {
      data[index].imgDropdown = "arrow_down.png";
      data[index].template = "flxExternalAccountsTransfersSelected";
    }

    this.view.transfermain.segmentTransfers.setData(data);
    this.AdjustScreen(150);
  },

  loadMyPaymentAccountsData: function () {
    var dataMap = {
      "lblLeft": "lblLeft",
      "lblAccountName": "lblAccountName",
      "flxLeft": "flxLeft",
      "lblBalance": "lblBalance",
      "lblAvailableBalance": "lblAvailableBalance"
    };
    var data = [{
      "flxLeft": {
        "skin": "sknFlx9060b7"
      },
      "lblLeft": " ",
      "lblAccountName": "Personal Chekcing...1234",
      "lblBalance": "$6453.90",
      "lblAvailableBalance": "Available Balance"
    },
                {
                  "flxLeft": {
                    "skin": "sknFlx26D0CE"
                  },
                  "lblLeft": " ",
                  "lblAccountName": "Joint Savings...1234",
                  "lblBalance": "$6444.00",
                  "lblAvailableBalance": "Available Balance"
                },
                {
                  "flxLeft": {
                    "skin": "sknFlxF4BA22"
                  },
                  "lblLeft": " ",
                  "lblAccountName": "Credit Card...5432",
                  "lblBalance": "$3840.50",
                  "lblAvailableBalance": "Available Balance"
                },
                {
                  "flxLeft": {
                    "skin": "sknFlx26D0CE"
                  },
                  "lblLeft": " ",
                  "lblAccountName": "Joint Savings...4423",
                  "lblBalance": "$3244.00",
                  "lblAvailableBalance": "Available Balance"
                }
               ];

    
  },
  postShowtransfers: function(){
    this.AdjustScreen();
    this.view.transfermain.btnWireTransferProceed.onClick = function() {
       var nav = new kony.mvc.Navigation("frmWireTransfer");
       nav.navigate();
    };
  },
    //UI Code
  AdjustScreen: function(data) {
    if(data !== undefined) {     
    }
    else{
      data = 0;
    }
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + 40 + data + "dp";
        else
            this.view.flxFooter.top = mainheight + 40 + data + "dp";
        this.view.forceLayout();
     } else {
        this.view.flxFooter.top = mainheight + 40 + data + "dp";
        this.view.forceLayout();
     }
  },  
  
  /**
   * Toggle seach box.
   */
  toggleSearchBox: function () {
    kony.print("inside toggleSearchBox");
    this.setSearchFlexVisibility(!this.view.transfermain.flxSearch.isVisible);
    if(this.view.transfermain.btnExternalAccounts.skin === "sknBtnAccountSummaryUnselected" || this.searchView === true) {
        commonUtilities.showProgressBar(this.view);
        if(this.searchView === true) {
          this.presenter.getExternalAccounts(); //show external tab.
        } else {
          this.presenter.showExternalAccounts(); //show external tab.          
        }
    }
    if (!this.view.transfermain.flxSearch.isVisible) {
        this.searchView = false;       
        this.prevSearchText = '';     
    }
    this.view.forceLayout();
    this.AdjustScreen();     
    
    
  },
  showSearchBox: function () {
    var self = this;
    if (self.view.transfermain.btnMakeTransfer.skin == "sknBtnAccountSummarySelectedmod") {
      self.showScheduledData();
      self.toggleSearchBox();
    } else {
      self.toggleSearchBox();
    }
  },

  /**
   * Set search flex visibility.
   * @Input flag {boolean} true/flase.
   * 
   */
  setSearchFlexVisibility: function(flag){
    if(typeof flag === "boolean") {
      this.view.transfermain.imgSearch.src = flag ? "selecetd_search.png" : "search_blue.png";
      this.view.transfermain.flxSearch.setVisibility(flag);
      this.view.transfermain.flxSearchSortSeparator.setVisibility(flag);
      if(flag === true) {
        this.view.transfermain.Search.txtSearch.text = '';
        this.view.transfermain.Search.txtSearch.setFocus();
        this.disableSearch();
      }
    }
    else {
        kony.print("Invalid input.");
    }
  },

  closeViewTransfers: function () {
    this.view.transferActivity.isVisible = false;
  },

  updateHamburgerMenu: function (sideMenuModel) {
    this.view.customheader.initHamburger(sideMenuModel);    
    this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
    if(this.view.transfermain.btnMakeTransfer.skin == 'sknBtnAccountSummarySelected'){
      this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
    }
    else if(this.view.transfermain.btnRecent.skin == 'sknBtnAccountSummarySelected'){
      this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer History");
    }
    else if(this.view.transfermain.btnExternalAccounts.skin == 'sknBtnAccountSummarySelected'){
      this.view.customheader.customhamburger.activateMenu("Transfers", "External Accounts");
    }
  },
  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  },
  /**
   * Form Init actions
   */

 initActions: function(){
  var scopeObj = this;
  	this.view.customheader.forceCloseHamburger();
	this.view.transfermain.maketransfer.calSendOn.dateFormat = commonUtilities.getConfiguration("frontendDateFormat");
   this.view.transfermain.maketransfer.calEndingOn.dateFormat = commonUtilities.getConfiguration("frontendDateFormat");
  if (commonUtilities.getConfiguration("canSearchTransfers")==="true") {
    scopeObj.view.transfermain.flxSearchImage.setVisibility(true);
    scopeObj.view.transfermain.flxSearchImage.onClick = scopeObj.toggleSearchBox.bind(scopeObj);
    scopeObj.view.transfermain.Search.btnConfirm.onClick = scopeObj.onSearchBtnClick.bind(scopeObj);
    scopeObj.view.transfermain.Search.flxClearBtn.onClick = scopeObj.onSearchClearBtnClick.bind(scopeObj);
    scopeObj.view.transfermain.Search.txtSearch.onKeyUp = scopeObj.onTxtSearchKeyUp.bind(scopeObj);
  } else {
    scopeObj.view.transfermain.flxSearchImage.setVisibility(false);
  }
  //External Accounts
  scopeObj.externalAccountsSortMap = [
    { name : 'nickName' ,  imageFlx : scopeObj.view.transfermain.imgSortDateExternal , clickContainer : scopeObj.view.transfermain.flxSortDateExternal},
    { name : 'bankName' ,  imageFlx : scopeObj.view.transfermain.imgSortDescriptionExternal, clickContainer : scopeObj.view.transfermain.flxSortDescriptionExternal},
    { name : 'isVerified' ,  imageFlx : scopeObj.view.transfermain.imgSortTypeExternal, clickContainer : scopeObj.view.transfermain.flxSortAmountExternal }
  ];
  commonUtilities.Sorting.setSortingHandlers(scopeObj.externalAccountsSortMap, scopeObj.onExternalAccountsSortClickHandler, scopeObj);

  //Recent AND Scheduled Transactions.
  scopeObj.recentAndScheduledSortMap = [
    { name : 'transactionDate' ,  imageFlx : scopeObj.view.transfermain.imgSortDate , clickContainer :scopeObj.view.transfermain.flxSortDate},
    { name : 'a.nickName' ,  imageFlx : scopeObj.view.transfermain.imgSortDescription, clickContainer : scopeObj.view.transfermain.flxSortDescription},
    { name : 'amount' ,  imageFlx : scopeObj.view.transfermain.imgSortType, clickContainer : scopeObj.view.transfermain.flxSortAmount }
  ];
   
  //Hiding Error flex
   this.view.flxMakeTransferError.setVisibility(false);
   
   
 },
 
/**
 * On Recent Transactions Sort click handler.
 * @Input : event {object} event object.
 * @Input : data {object} column details.
 *  
 */
onRecentSortClickHandler: function(event, data){
  var scopeObj = this;
  commonUtilities.showProgressBar(this.view);
  scopeObj.first = 0;
  scopeObj.presenter.fetchRecentUserTransactions(null,data);
},

/**
 * On Scheduled Transactions Sort click handler.
 * @Input : event {object} event object.
 * @Input : data {object} column details.
 *  
 */
onScheduledSortClickHandler: function(event, data){
  var scopeObj = this;
  commonUtilities.showProgressBar(this.view);
  scopeObj.first = 0;
  scopeObj.presenter.fetchScheduledUserTransactions(null,data);
},

/**
 * On External Accounts Sort click handler.
 * @Input : event {object} event object.
 * @Input : data {object} column details.
 *  
 */
 onExternalAccountsSortClickHandler: function(event, data){
  var scopeObj = this;
  commonUtilities.showProgressBar(this.view);
  scopeObj.first = 0;
  scopeObj.presenter.getExternalAccounts(data);
  scopeObj.setSearchFlexVisibility(false);
  },


 /**
  * On Search text change
  */
  onTxtSearchKeyUp: function(event){
    var scopeObj = this;
    var searchKeyword = scopeObj.view.transfermain.Search.txtSearch.text.trim();
    if(searchKeyword.length > 0){
      scopeObj.enableSearch();
    } else{
      scopeObj.disableSearch();
    }
  },
 /**
  * Transfer Payees Search button click handler.
  *
  */
 onSearchBtnClick: function(){
  var scopeObj = this;
  var searchKeyword = scopeObj.view.transfermain.Search.txtSearch.text.trim();
  if (searchKeyword.length >= 0 && scopeObj.prevSearchText !== searchKeyword) {
      commonUtilities.showProgressBar(this.view);
      scopeObj.presenter.searchTransferPayees({
          'searchKeyword': searchKeyword
      });
      scopeObj.searchView = true;
      scopeObj.prevSearchText = searchKeyword;
  }

 },
  /**
  * Transfer Payees Search clear button click handler.
  *
  */
 onSearchClearBtnClick: function(){
  //this.toggleSearchBox();
  this.setSearchFlexVisibility(true);
  if (this.searchView === true) {
      commonUtilities.showProgressBar(this.view);
      this.presenter.getExternalAccounts(); //show external tab.
  }
  this.searchView = false;
  this.prevSearchText = '';
  this.view.forceLayout();
 },
 /**
  * Show search transfer Payees list.
  * @Input: viewModel {object} search result from Presentation Controller. 
  */
 showSearchTransferPayees: function(viewModel){
   var scopeObj = this;
   commonUtilities.hideProgressBar(this.view);
   scopeObj.view.transfermain.tablePagination.flxPagination.setVisibility(false);
   if (viewModel.error) {
     scopeObj.showExternalAccounts("errorExternalAccounts");
     return;
   }
   if(viewModel.externalAccounts.length === 0){
    scopeObj.view.transfermain.flxSortExternal.setVisibility(false);
    scopeObj.view.transfermain.segmentTransfers.setVisibility(false);
    scopeObj.view.transfermain.flxNoTransactions.setVisibility(true);
    scopeObj.view.transfermain.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.transfers.searchNoPayees'); 
    scopeObj.view.transfermain.lblScheduleAPayment.setVisibility(false);    
    scopeObj.view.forceLayout();
    return;
   }
   viewModel.externalAccounts.push(viewModel.searchInputs); //Append search inputs for showExternalAccounts viewModel.   
   scopeObj.showExternalAccounts(viewModel.externalAccounts, true);
 },
 /**
  * Disable search 
  */
 disableSearch: function () {
   this.disableButton(this.view.transfermain.Search.btnConfirm)
   this.view.transfermain.Search.flxClearBtn.setVisibility(false);
 },
 /**
 * Disable search 
 */
 enableSearch: function () {
   this.enableButton(this.view.transfermain.Search.btnConfirm)
   this.view.transfermain.Search.flxClearBtn.setVisibility(true);
   this.view.forceLayout();
 },
 /**
  *  Disable button.
  */
  disableButton : function (button) {
    button.setEnabled(false);
    button.skin = "sknBtnBlockedLatoFFFFFF15Px";
    button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
    button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";     
  },
  /**
   * Enable button.
   */
  enableButton : function(button){
    button.setEnabled(true);   
    button.skin = "sknbtnLatoffffff15px";
    button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
    button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";    
  }

}
});