define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnEdit **/
    AS_Button_jd6ad5b2c8274bd6a0d1f81a44e81027: function AS_Button_jd6ad5b2c8274bd6a0d1f81a44e81027(eventobject, context) {
        var self = this;
        this.showEditAccountsPreferences();
    },
    /** onClick defined for flxAccountName **/
    AS_FlexContainer_f03a7fe36c594723812ff20dc88b60bd: function AS_FlexContainer_f03a7fe36c594723812ff20dc88b60bd(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxFavoriteCheckBox **/
    AS_FlexContainer_eb098f2af52643b79d6495f7c0a8f3f4: function AS_FlexContainer_eb098f2af52643b79d6495f7c0a8f3f4(eventobject, context) {
        var self = this;
        this.toggleFavouriteCheckBox();
    },
    /** onClick defined for flxEStatementCheckBox **/
    AS_FlexContainer_i0e2d2c700ae4750a6aa0376dd344a70: function AS_FlexContainer_i0e2d2c700ae4750a6aa0376dd344a70(eventobject, context) {
        var self = this;
        this.toggleEstatementCheckBox();
    },
    /** onClick defined for flxContent **/
    AS_FlexContainer_df073d195a67484fa42b1db1ab59f3fa: function AS_FlexContainer_df073d195a67484fa42b1db1ab59f3fa(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxMoveUp **/
    AS_FlexContainer_id4cba8fd7784efbacc919db4ef18068: function AS_FlexContainer_id4cba8fd7784efbacc919db4ef18068(eventobject, context) {
        var self = this;
    },
    /** onClick defined for flxMoveDown **/
    AS_FlexContainer_i9ea79e99ea542dd93359a98d3507447: function AS_FlexContainer_i9ea79e99ea542dd93359a98d3507447(eventobject, context) {
        var self = this;
    },
    /** onClick defined for flxMenu **/
    AS_FlexContainer_d5240ffe33a8453283208df900c38461: function AS_FlexContainer_d5240ffe33a8453283208df900c38461(eventobject, context) {
        var self = this;
        this.menuPressed();
    }
});