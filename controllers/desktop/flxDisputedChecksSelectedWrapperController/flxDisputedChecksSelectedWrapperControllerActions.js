define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_be5c1e4670354b6db0ce974fd81e2912: function AS_FlexContainer_be5c1e4670354b6db0ce974fd81e2912(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for btnCancelRequests **/
    AS_Button_hfc9d6b653334b6faeb2c9f641ed064e: function AS_Button_hfc9d6b653334b6faeb2c9f641ed064e(eventobject, context) {
        var self = this;
        var currForm = kony.application.getCurrentForm();
        currForm.flxLogoutStopCheckPayment.setVisibility(true);
        currForm.forceLayout();
    },
    /** onClick defined for btnSendAMessage **/
    AS_Button_cbb71b974bdb468488781c0413272aa0: function AS_Button_cbb71b974bdb468488781c0413272aa0(eventobject, context) {
        var self = this;
        var nav = new kony.mvc.Navigation("frmNotificationsAndMessages");
        nav.navigate();
    }
});