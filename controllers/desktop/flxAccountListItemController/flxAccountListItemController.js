define({

    menuPressed: function (data) {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.accountList.segAccounts.selectedIndex[1];  
        //modified for OCCU project
        var secindex = currForm.accountList.segAccounts.selectedIndex[0];
		var segmentData = currForm.accountList.segAccounts.data[secindex][1][index];
        segmentData.onQuickActions();
        currForm.accountListMenu.flxIdentifier.skin = segmentData.flxMenu.skin;
      
        if (currForm.accountListMenu.isVisible === true) {
            currForm.accountListMenu.top = 280;
            currForm.accountListMenu.isVisible = false;
            currForm.forceLayout();
        } else {
            var currTop = currForm.accountListMenu.top;
            currTop = parseInt(currTop);
            currTop += 100 * index;
            currForm.accountListMenu.top = currTop;
            currForm.accountListMenu.isVisible = true;
            currForm.forceLayout();
        }
    },

    imgPressed: function () {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.accountList.segAccounts.selectedIndex[1];
        //modified for OCCU project
        var secindex = currForm.accountList.segAccounts.selectedIndex[0];
        var segmentData = currForm.accountList.segAccounts.data[secindex][1][index];
        segmentData.toggleFavourite();
        //_kony.mvc.GetController(currForm.id, true).presenter.AccountsSummary.toggleFavourite(account);
        // var img = data.imgFavourite;
        // if (img === "filled_star.png") {
        //     data.imgFavourite = "unfilled_star.png";
        // } else {
        //     data.imgFavourite = "filled_star.png";
        // }
        // currForm.accountList.segAccounts.setDataAt(data, index);
    },

    accountPressed: function () {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.accountList.segAccounts.selectedIndex[1];
        //modified for OCCU project
        var secindex = currForm.accountList.segAccounts.selectedIndex[0];
        var segmentData = currForm.accountList.segAccounts.data[secindex][1][index];
        segmentData.onAccountClick();
    },

});