define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_c006be99489f49f8babfe9bc5da1d11e: function AS_FlexContainer_c006be99489f49f8babfe9bc5da1d11e(eventobject, context) {
        var self = this;
        this.segmentManagePayeesRowClick();
    },
    /** onClick defined for btnEbill **/
    AS_Button_a8f6a78c3e0c4ca1bc0fbd60946f8ee2: function AS_Button_a8f6a78c3e0c4ca1bc0fbd60946f8ee2(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnPayBill **/
    AS_Button_dce2271c9adb4ff288c6aa86d9dc85ff: function AS_Button_dce2271c9adb4ff288c6aa86d9dc85ff(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnViewActivity **/
    AS_Button_jf9351ce577f490784da5e5b59e550b4: function AS_Button_jf9351ce577f490784da5e5b59e550b4(eventobject, context) {
        var self = this;
        //comment
    },
    /** onClick defined for btnViewEbill **/
    AS_Button_c0b27a4e2d3c45c1b512c819b09cddf8: function AS_Button_c0b27a4e2d3c45c1b512c819b09cddf8(eventobject, context) {
        var self = this;
        //comment
    },
    /** onClick defined for btnEditBiller **/
    AS_Button_a09f87c1d0784a8f8df8dd35d563e674: function AS_Button_a09f87c1d0784a8f8df8dd35d563e674(eventobject, context) {
        var self = this;
        //comment
    },
    /** onClick defined for btnDeleteBiller **/
    AS_Button_j20e91923a28407fab5bcb22545b0f06: function AS_Button_j20e91923a28407fab5bcb22545b0f06(eventobject, context) {
        var self = this;
        //comment
    }
});