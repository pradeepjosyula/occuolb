define({ 
  preshowFrmAddPayee:function(){
    var scopeObj = this;
    this.view.customheader.forceCloseHamburger();
    this.hideCancelPopup();
    this.view.breadcrumb.setBreadcrumbData([{text:"BILL PAY"}, {text:"ADD PAYEE"}]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.addPayee");
    // this.view.flxMainWrapper.isVisible=true;
    // this.view.flxVerifyPayeeInformation.isVisible=false;
    // this.view.flxAddBillerDetails.isVisible=false;
    // this.view.flxAddPayeeAck.isVisible=false;
    this.view.customheader.topmenu.flxMenu.skin="slFbox";
    this.view.customheader.topmenu.flxaccounts.skin="slFbox";
    this.view.customheader.topmenu.flxTransfersAndPay.skin="sknFlxFFFFFbrdr3343a8";
    // this.view.customheader.topmenu.flxaccounts.skin="sknHoverTopmenu7f7f7pointer"; 
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
    this.view.CustomPopupCancel.btnYes.onClick = this.viewallpayeesbtnaction;
  },

  CommonUtilities: require('CommonUtilities'),

  showFlxEnterPayeeInfo:function (){
    this.view.btnSearchPayee.skin = 'sknBtnAccountSummaryUnSelectedlb0pxrb0pxfbfbfb';
    this.view.btnSearchPayee.hoverSkin= 'sknhoverf8f8f8';
    this.view.btnEnterPayeeInfo.skin = 'sknBtnAccountSummarySelectedlb1pxrb1px';
    this.view.flxSearchPayee.isVisible = false;
    this.view.flxRestNext2.isVisible = false;
    this.view.flxEnterPayeeInfo.isVisible = true;
    this.view.flxResetNext.isVisible = true;
    this.selectedMode = 'Manual';
    this.AdjustScreen(100);
    this.view.forceLayout();
    this.AdjustScreen(100);
  },
  showFlxSearchPayee:function (){
    this.view.btnSearchPayee.skin = 'sknBtnAccountSummarySelectedlb1pxrb0px';
    this.view.btnEnterPayeeInfo.skin = 'sknBtnAccountSummaryUnSelectedlb1pxrb0pxfbfbfb';
    this.view.btnEnterPayeeInfo.hoverSkin = 'sknhoverf8f8f8';
    this.view.flxEnterPayeeInfo.isVisible = false;
    this.view.flxResetNext.isVisible = false;
    this.view.flxSearchPayee.isVisible = true;
    this.view.flxRestNext2.isVisible = true;
    this.AdjustScreen(100);
    this.selectedMode = 'Search';
    this.view.forceLayout();
    this.AdjustScreen(100);
  },
  showFlxVerifyPayeeInformation:function (){ 
    this.view.flxMainWrapper.isVisible = false;
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.billPay.BillPay")},
                                            {text:kony.i18n.getLocalizedString("i18n.billPay.addPayee")+'-'+kony.i18n.getLocalizedString("i18n.transfers.Confirm")}]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");	
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.addPayee")+'-'+kony.i18n.getLocalizedString("i18n.transfers.Confirm");	
    this.view.flxAddBillerDetails.isVisible=false;
    this.view.flxVerifyPayeeInformation.isVisible = true;
    this.view.flxAddPayeeAck.isVisible=false;
    this.view.forceLayout();	
  },
  showflxAddBillerDetailsInformation:function (){ 
    this.view.flxMainWrapper.isVisible = false;
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.billPay.BillPay")},
                                            {text:kony.i18n.getLocalizedString("i18n.billPay.addPayee")+'-'+kony.i18n.getLocalizedString("i18n.transfers.Confirm")}]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");	
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.addPayee")+'-'+kony.i18n.getLocalizedString("i18n.transfers.Confirm");												
    this.view.flxAddBillerDetails.top = '0dp';
    this.view.flxAddBillerDetails.isVisible=true;
    this.view.flxVerifyPayeeInformation.isVisible = false;
    this.view.flxAddPayeeAck.isVisible = false;
    this.view.forceLayout();	    	 
  },
  selectedMode:'Search',
  initializePayeeInformation:function(){
    this.view.lblNotMatching.isVisible = false;
    this.view.flxMisMatch.isVisible = false;
    this.view.tbxCustomerName.text = "";
    this.view.tbxZipcode.text = "";
    this.view.tbxOne.text = "";
    this.view.tbxTwo.text = "";
    this.view.tbxThree.text = "";
    this.normalizeBoxes(this.view.tbxOne,this.view.tbxTwo);
    this.disableButton(this.view.btnNext2);  

    this.view.flxNoExactMatch.isVisible = false; 
    this.view.lblErrorInfo.isVisible = false;
    this.view.tbxEnterName.text = "";
    this.view.tbxEnterAddress.text = "";
    this.view.tbxEnterAddressLine2.text = "";
    this.view.tbxCity.text = "";
    this.view.tbxEnterZipCode.text = "";

    this.view.imgRememberMe.src = 'checked_box.png';
    this.view.tbxAdditionalNote.text = "";
    this.rememberThis();


    this.view.tbxEnterAccountNmber.text = "";
    this.view.tbxConfirmAccNumber.text = "";    
    this.normalizeBoxes(this.view.tbxEnterAccountNmber,this.view.tbxConfirmAccNumber);      
    this.disableButton(this.view.btnNext);  
    this.view.CustomPopupCancel.btnYes.onClick = this.viewallpayeesbtnaction;
  },
  resetSearchedInformation:function(){
    this.view.lblNotMatching.isVisible = false;
    this.view.flxMisMatch.isVisible = false;
    this.view.tbxCustomerName.text = "";
    this.view.tbxZipcode.text = "";
    this.view.tbxOne.text = "";
    this.view.tbxTwo.text = "";
    this.view.tbxThree.text = "";
    this.normalizeBoxes(this.view.tbxOne,this.view.tbxTwo);
    this.disableButton(this.view.btnNext2);  
  },
  resetManualInformation:function(){
    this.view.flxNoExactMatch.isVisible = false; 
    this.view.lblErrorInfo.isVisible = false;
    this.view.tbxEnterName.text = "";
    this.view.tbxEnterAddress.text = "";
    this.view.tbxEnterAddressLine2.text = "";
    this.view.tbxCity.text = "";
    this.view.tbxEnterZipCode.text = "";

    this.view.tbxAdditionalNote.text = "";
    if(this.accountNumberAvailable){
      this.view.tbxEnterAccountNmber.text = "";
      this.view.tbxConfirmAccNumber.text = "";    
      this.normalizeBoxes(this.view.tbxEnterAccountNmber,this.view.tbxConfirmAccNumber);      
    }

    this.disableButton(this.view.btnNext);  
  },
  resetPayeeInformation:function(){
    if(this.selectedMode === 'Search'){
      this.resetSearchedInformation();
    }
    if(this.selectedMode === 'Manual'){
      this.resetManualInformation();
    }	
    this.view.forceLayout();
  },
  showFlxAddPayeeAck:function (){     
    this.view.flxVerifyPayeeInformation.isVisible = false;
    this.view.flxMainWrapper.isVisible=false;
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.billPay.BillPay")}, 
                                            {text:kony.i18n.getLocalizedString("i18n.billPay.addPayee")+'-'+kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement")}]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");	
    this.view.breadcrumb.lblBreadcrumb2.toolTip= kony.i18n.getLocalizedString("i18n.billPay.addPayee")+'-'+kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement")	
    this.view.imgDownloadIcon.isVisible = true;
    this.view.imgPrintIcon.isVisible = true;
    this.view.flxAddPayeeAck.isVisible = true;
    this.view.flxAddBillerDetails.isVisible=false;
    this.view.forceLayout();
  },
  showQuitPopup:function (){     
    this.view.flxPopup.height = kony.os.deviceInfo().screenHeight+"dp"; 
    this.view.flxPopup.isVisible = true;
    this.view.forceLayout();
  },
  alterBreadcrumb:function (){     
    this.view.breadcrumb.imgBreadcrumb2.isVisible = false; 
    this.view.breadcrumb.lblBreadcrumb3.isVisible = false;
    this.view.forceLayout();
  },
  postShowAddPayee: function(){
    this.AdjustScreen(100);
  },
  //UI Code
  AdjustScreen: function(data) {
    if(data !== undefined){
    }
    else {
      data = 0;
    }
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
      diff = diff - this.view.flxFooter.frame.height;
      if (diff > 0) 
        this.view.flxFooter.top = mainheight + diff + data + "dp";
      else
        this.view.flxFooter.top = mainheight + data + "dp";        
    } else {
      this.view.flxFooter.top = mainheight + data + "dp";
    }
    this.view.forceLayout();
  },       

  rememberThis:function(){
    if(this.view.imgRememberMe.src == 'unchecked_box.png'){
      this.view.imgRememberMe.src = 'checked_box.png';
      this.view.tbxAdditionalNote.isVisible = true;
      this.view.tbxEnterAccountNmber.skin='sknLato42424215BgD3D3D3Op10BorderE3E3E3';
      this.view.tbxEnterAccountNmber.focusSkin='sknLato42424215BgD3D3D3Op10BorderE3E3E3';
      this.view.tbxEnterAccountNmber.hoverSkin='sknLato42424215BgD3D3D3Op10BorderE3E3E3';
      this.view.tbxEnterAccountNmber.setEnabled(false);
      this.view.tbxConfirmAccNumber.skin='sknLato42424215BgD3D3D3Op10BorderE3E3E3';
      this.view.tbxConfirmAccNumber.focusSkin='sknLato42424215BgD3D3D3Op10BorderE3E3E3';
      this.view.tbxConfirmAccNumber.hoverSkin='sknLato42424215BgD3D3D3Op10BorderE3E3E3';
      this.view.tbxConfirmAccNumber.setEnabled(false);
      this.accountNumberAvailable = false;
      this.checkIfAllManualFieldsAreFilled();
    }
    else{
      this.view.imgRememberMe.src = 'unchecked_box.png';
      this.view.tbxAdditionalNote.isVisible = false;
      this.view.tbxAdditionalNote.text = "";
      this.view.tbxEnterAccountNmber.skin='sknTbxLatoffffff15PxBorder727272opa20';
      this.view.tbxEnterAccountNmber.focusSkin='sknLato42424215PxBorder4A90E2';
      this.view.tbxEnterAccountNmber.hoverSkin='sknLato42424215PxBorder4A90E2';
      this.view.tbxEnterAccountNmber.setEnabled(true);
      this.view.tbxConfirmAccNumber.skin='sknTbxLatoffffff15PxBorder727272opa20';
      this.view.tbxConfirmAccNumber.focusSkin='sknLato42424215PxBorder4A90E2';
      this.view.tbxConfirmAccNumber.hoverSkin='sknLato42424215PxBorder4A90E2';
      this.view.tbxConfirmAccNumber.setEnabled(true);
      this.accountNumberAvailable = true;
      this.checkIfAllManualFieldsAreFilled();
    }
    this.view.forceLayout();
  },
  navigateToBillPayHistoryTab:function()
  {
    //           var obj1 = {
    //         "tabname": "history"
    //       };
    var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
    var data={
      "data":null,
      "show":'History',
    };
    BillPayModule.presentationController.showBillPayData(null,data); 
  },
  viewallpayeesbtnaction: function()
  {
    var obj1 = {
      "tabname": "managepayees"
    };

    var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
    BillPayModule.presentationController.showBillPayData();

    //        var navObj = new kony.mvc.Navigation("frmBillPay");
    //        navObj.navigate(obj1);

  },

  makeabillpaymentbtnaction: function()
  {
    var obj1 = {
      "tabname": "payabill"
    };
    var navObj = new kony.mvc.Navigation("frmBillPay");
    navObj.navigate(obj1);
  },
  showMainWrapper:function()
  {
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.billPay.BillPay")}, {text:kony.i18n.getLocalizedString("i18n.billPay.addPayee")}]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");	
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.addPayee");
    this.view.flxMainWrapper.isVisible=true;
    this.view.flxVerifyPayeeInformation.isVisible=false;
    this.view.flxAddBillerDetails.isVisible=false;
    this.view.flxAddPayeeAck.isVisible=false;
    this.view.flxAdditionError.isVisible = false;
    this.view.forceLayout();
  },
  showCancelPopup:function()
  {
    var height_to_set = 170 + this.view.flxMain.frame.height;
    this.view.flxCancelPopup.height = height_to_set + "dp";
    this.view.flxCancelPopup.isVisible = true;
    this.view.flxCancelPopup.setFocus(true);
    this.view.forceLayout();
  },
  hideCancelPopup:function()
  {
    this.view.flxCancelPopup.isVisible = false;
    this.view.forceLayout();
  },
  /*showFlxAddBillerDetails:function (){     
     this.view.flxVerifyPayeeInformation.isVisible = false;
     this.view.flxAddBillerDetails.isVisible = true;
     this.view.forceLayout();
}*/
  //****************UI CODE ABOVE****************************
  payeeList:null,
  accountNumberAvailable: true,
  shouldUpdateUI: function(viewModel) {
    return typeof viewModel !== 'undefined' && viewModel !== null;
  },

  willUpdateUI: function(viewModel) {
    kony.olb.utils.hideProgressBar(this.view); 
    if(viewModel.ShowProgressBar)this.showProgressBar();
    if(viewModel.HideProgressBar)this.hideProgressBar();
    if(viewModel.firstLoad)this.initiateAddPayee(viewModel.firstLoad);
    if(viewModel.billersList)this.showPayeeList(viewModel.billersList);
    //if(viewModel.payeeUpdateDetails)this.presentPayeeDetails(viewModel.payeeUpdateDetails);
    if (viewModel.payeeDetails) this.presentPayeeDetails(viewModel.payeeDetails,viewModel.payeeUpdateDetails);
    if(viewModel.payeeConfirmDetails)this.presentConfirmPayeeDetails(viewModel.payeeConfirmDetails);
    if(viewModel.payeeSuccessDetails)this.presentAddPayeeAcknowledgement(viewModel.payeeSuccessDetails);
    if(viewModel.billerDetails)this.selectPayeeName(viewModel.billerDetails);
    if(viewModel.registeredPayeeList)this.updateRegisteredPayees(viewModel.registeredPayeeList);
    if(viewModel.isInvalidPayee)this.enterCorrectBillerName();
    if(viewModel.errorInAddingPayee)this.showAddPayeeErrorMessage(viewModel.errorInAddingPayee);
    if(viewModel.sideMenu){
      this.updateHamburgerMenu(viewModel.sideMenu);         
    }
    if (viewModel.topBar) this.updateTopBar(viewModel.topBar);

    //one time payment
    if(viewModel.initOneTimePayment)this.initOneTimePayment(viewModel.initOneTimePayment);
    if(viewModel.validationError)this.showValidationError(viewModel.validationError);
    if(viewModel.showBillpayTransaction)this.showBillpayTransaction();
    this.AdjustScreen(100);
  }, 

  showBillpayTransaction : function(){
    this.view.oneTimePay.flxSearchPayee.setVisibility(false);
    this.view.oneTimePay.flxDetails.setVisibility(true);
  },

  initOneTimePayment : function(){
    this.view.breadcrumb.setBreadcrumbData([{text:"BILL PAY"}, {text:"ONE TIME PAYMENT"}]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
    this.view.breadcrumb.lblBreadcrumb2.toolTip="ONE TIME PAYMENT";
    this.view.lblAddPayee.text = "One Time Payment";
    this.view.flxAddPayee.setVisibility(false);
    //this.view.oneTimePayment.flxSearchPayee.setVisibility(true);
    //this.view.oneTimePayment.lblHeader.text = "SEARCH FOR PAYEE BY NAME";
    this.view.flxonetimepayment.setVisibility(true);
    this.setLogoutEvents();
    this.resetTextBoxes();
    this.hideCancelPopup();
    this.view.oneTimePay.flxmobilenumber.setVisibility(false);
    this.view.oneTimePay.flxDetails.setVisibility(false);
    this.view.oneTimePay.lblWarning.isVisible = false;
    this.view.oneTimePay.flxPayeeList.setVisibility(false);
    this.view.flxVerifyPayeeInformation.isVisible=false;
    this.view.flxAddBillerDetails.isVisible=false;
    this.view.flxAddPayeeAck.isVisible=false;  
    this.view.flxMainWrapper.isVisible=true;    
    this.disableButton(this.view.oneTimePay.btnNext);
    this.normalizeBoxes(this.view.oneTimePay.tbxName,this.view.oneTimePay.txtAccountNumber, this.view.oneTimePay.txtAccountNumberAgain);

    this.view.oneTimePay.btnNext.onClick = this.updateNewPayee;
    this.view.btnDetailsBack.onClick = this.navigateToAcknowledgementForm;
    this.view.oneTimePay.btnCancel.text = kony.i18n.getLocalizedString("i18n.billpay.reset");
    this.view.oneTimePay.btnCancel.onClick = this.resetTextBoxes;
    this.view.btnModify.onClick = this.showUpdateBillerPage;
    this.view.CustomPopupCancel.btnYes.onClick = this.viewallpayeesbtnaction;
    this.view.flxAdditionError.isVisible = false;
    this.view.oneTimePay.flxSearchPayee.setVisibility(true);
  },








  showUpdateBillerPage : function(){
    this.presenter.showUpdateBillerPage();
  },
  resetTextBoxes : function(){
    [this.view.oneTimePay.tbxName,this.view.oneTimePay.txtZipCode,this.view.oneTimePay.txtAccountNumber,
     this.view.oneTimePay.txtAccountNumberAgain,this.view.oneTimePay.txtmobilenumber].map(function(element){
      element.onKeyUp = this.checkIfAllSearchFieldsAreFilled;
      element.text="";
    },this);
    this.view.oneTimePay.tbxName.onKeyUp = this.callOnKeyUp;
  },
  navigateToAcknowledgementForm: function(){
    var BillPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
    BillPayModule.presentationController.navigateToAcknowledgementForm({});
  },
  updateNewPayee : function(){
    var inputObject  = {
      billerName : this.view.oneTimePay.tbxName.text,
      zipCode : this.view.oneTimePay.txtZipCode.text,
      accountNumber : this.view.oneTimePay.txtAccountNumber.text,
      accountNumberAgain : this.view.oneTimePay.txtAccountNumberAgain.text,
      mobileNumber : this.view.oneTimePay.txtmobilenumber.text
    };
    this.view.oneTimePay.lblWarning.isVisible = false;
    this.normalizeBoxes(this.view.oneTimePay.tbxName,this.view.oneTimePay.txtAccountNumber, this.view.oneTimePay.txtAccountNumberAgain);
    this.presenter.updateOneTimePayeeInfo(inputObject);
  },
  setLogoutEvents : function(){
    scopeObj= this;
    this.view.customheader.headermenu.btnLogout.onClick = function(){
      // kony.print("btn logout pressed");  
      scopeObj.view.CustomPopup.lblHeading.text=kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text=kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height=scopeObj.view.flxHeader.frame.height+scopeObj.view.flxMain.frame.height;
      scopeObj.view.flxLogout.height=height+"dp";
      scopeObj.view.flxLogout.left="0%";     
    } ;
    this.view.CustomPopupLogout.btnYes.onClick = function(){
      // kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";      

    };
    this.view.CustomPopupLogout.btnNo.onClick = function(){
      // kony.print("btn no pressed");  
      scopeObj.view.flxLogout.left="-100%";
    };
    this.view.CustomPopupLogout.flxCross.onClick = function(){
      // kony.print("btn no pressed");  
      scopeObj.view.flxLogout.left="-100%";
    };
  },
  showPayeeList:function(payeeList){
    // kony.print(payeeList);
    //this.payeeList = payeeList;

    if (this.view.flxonetimepayment.isVisible) {
      this.view.oneTimePay.segPayeesName.widgetDataMap = {
        "flxAccountTypes": "flxNewPayees",
        "lblUsers": "lblNewPayees"
      };
      if (payeeList.length > 0) {
        this.view.oneTimePay.segPayeesName.setData(this.createNewPayeeModel(payeeList));
        this.view.oneTimePay.flxPayeeList.isVisible = true;
        this.view.forceLayout();
      }
      else {
        this.view.oneTimePay.flxPayeeList.isVisible = false;
        this.view.forceLayout();
      }
    } else {
      this.view.segPayeesName.widgetDataMap = {
        "flxNewPayees": "flxNewPayees",
        "lblNewPayees": "lblNewPayees"
      };
      if (payeeList.length > 0) {
        this.view.segPayeesName.setData(this.createNewPayeeModel(payeeList));
        this.view.flxPayeeList.isVisible = true;
        this.view.forceLayout();
      }
      else {
        this.view.flxPayeeList.isVisible = false;
        this.view.forceLayout();
      }
    }

  },
  createNewPayeeModel:function(billerList){
    return billerList.map(function(biller) {
      return {
        "lblNewPayees": biller.billerName,
        "flxNewPayees":{
          "onClick":biller.onBillerSelection

        }           
      };
    }).reduce(function(p, e) {return p.concat(e);}, []);        
  },
  selectPayeeName:function(viewModel){

    if(this.view.flxonetimepayment.isVisible){
      this.view.oneTimePay.tbxName.text= viewModel.billerName;
      this.view.oneTimePay.flxPayeeList.isVisible = false;
    }else{
      this.view.tbxCustomerName.text = viewModel.billerName;
      this.view.flxPayeeList.isVisible = false;
    }


    this.setDynamicFields(viewModel.billerCategoryName);
    this.checkIfAllSearchFieldsAreFilled();
    this.view.forceLayout();
  },
  setDynamicFields:function(billerCategory){
    if(billerCategory == 'Credit Card'){

      this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.accounts.accountNumber"),
                                               kony.i18n.getLocalizedString("i18n.common.AccountNumberPlaceholder"),
                                               {'fieldHolds':'AccountNumber'},
                                               kony.i18n.getLocalizedString("i18n.addPayee.ConfirmAccountNumber"),
                                               kony.i18n.getLocalizedString("i18n.addPayee.ConfirmAccountNumber")
                                              );
    }
    else if(billerCategory == 'Phone'){
      this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumber"),
                                               kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberPlaceholder"),
                                               {'fieldHolds':'RelationshipNumber'},
                                               kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
                                               kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),                                               
                                               kony.i18n.getLocalizedString("i18n.common.MobilePhone"),
                                               kony.i18n.getLocalizedString("i18n.common.MobilePhonePlaceholder"),
                                               {'fieldHolds':'MobileNumber'});   
    }
    else if(billerCategory == 'Utilities'){

      this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.accounts.accountNumber"),
                                               kony.i18n.getLocalizedString("i18n.common.AccountNumberPlaceholder"),
                                               {'fieldHolds':'AccountNumber'},
                                               kony.i18n.getLocalizedString("i18n.addPayee.ConfirmAccountNumber"),
                                               kony.i18n.getLocalizedString("i18n.addPayee.ConfirmAccountNumber")
                                              );
    }
    else if(billerCategory == 'Insurance'){
      this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.accounts.accountNumber"),
                                               kony.i18n.getLocalizedString("i18n.common.AccountNumberPlaceholder"),
                                               {'fieldHolds':'AccountNumber'},
                                               kony.i18n.getLocalizedString("i18n.addPayee.ConfirmAccountNumber"),
                                               kony.i18n.getLocalizedString("i18n.addPayee.ConfirmAccountNumber"),                                               
                                               kony.i18n.getLocalizedString("i18n.addPayee.PolicyNumber"),
                                               kony.i18n.getLocalizedString("i18n.addPayee.PolicyNumberPlaceholder"),
                                               {'fieldHolds':'PolicyNumber'});   
    }
  },
  doDynamicChangesAccordingToCategory:function(displaylabel1,placeholder1,info1,displaylabel2,placeholder2,displaylabel3,placeholder3,info3){

    if (this.view.flxonetimepayment.isVisible) {
      this.view.oneTimePay.lblAccountNumber.text = displaylabel1;
      this.view.oneTimePay.txtAccountNumber.placeholder = placeholder1;
      this.view.oneTimePay.txtAccountNumber.info = info1;

      this.view.oneTimePay.lblAccountNumberAgain.text = displaylabel2;
      this.view.oneTimePay.txtAccountNumberAgain.placeholder = placeholder2;

      if (displaylabel3 !== undefined && placeholder3 !== undefined && info3 !== undefined) {
        this.view.oneTimePay.lblmobilenumeber.text = displaylabel3;
        this.view.oneTimePay.txtmobilenumber.placeholder = placeholder3;
        this.view.oneTimePay.txtmobilenumber.info = info3;
        this.view.oneTimePay.lblmobilenumeber.isVisible = true;
        this.view.oneTimePay.txtmobilenumber.isVisible = true;
        this.view.oneTimePay.flxmobilenumber.setVisibility(true);
      } else {
        this.view.oneTimePay.lblmobilenumeber.isVisible = false;
        this.view.oneTimePay.txtmobilenumber.isVisible = false;
        this.view.oneTimePay.flxmobilenumber.setVisibility(false);
      }
    } else {

      this.view.lblOne.text = displaylabel1;
      this.view.tbxOne.placeholder = placeholder1;
      this.view.tbxOne.info = info1;

      this.view.lblTwo.text = displaylabel2;
      this.view.tbxTwo.placeholder = placeholder2;

      if (displaylabel3 !== undefined && placeholder3 !== undefined && info3 !== undefined) {
        this.view.lblThree.text = displaylabel3;
        this.view.tbxThree.placeholder = placeholder3;
        this.view.tbxThree.info = info3;
        this.view.lblThree.isVisible = true;
        this.view.tbxThree.isVisible = true;
      }
      else {
        this.view.lblThree.isVisible = false;
        this.view.tbxThree.isVisible = false;
      }
    }

  },

  //  manuallyAddedPayee:{
  //       "billerName":null,
  //       "addressLine1":null,
  //       "addressLine2":null,
  //       "cityName":null,
  //       "state":null,
  //       "note":null,
  //       "accountNumber":null,
  //       "relationShipNumber":null,
  //        "mobileNumber":null,
  //       "policyNumber":null,
  //       "zipCode":null,
  //       "payeeNickName":null,
  //       "nameOnBill":null,
  //       "isManualUpdate":false,
  //       "noAccountNumber":false,
  //     },
  editManuallyAddedPayeeDetails:function(){
    var manuallyAddedPayee ={};
    manuallyAddedPayee.isManualUpdate = true;
    manuallyAddedPayee.billerName = this.view.tbxEnterName.text;
    if(this.accountNumberAvailable){
      manuallyAddedPayee.noAccountNumber = false;
      manuallyAddedPayee.accountNumber = this.view.tbxConfirmAccNumber.text;      
    }
    else{
      manuallyAddedPayee.noAccountNumber = true;
      manuallyAddedPayee.note = this.view.tbxAdditionalNote.text;
    }

    manuallyAddedPayee.addressLine1= this.view.tbxEnterAddress.text;
    manuallyAddedPayee.addressLine2 = this.view.tbxEnterAddressLine2.text;
    manuallyAddedPayee.cityName = this.view.tbxCity.text;
    manuallyAddedPayee.state = this.view.lbxState.selectedKeyValue[1];
    manuallyAddedPayee.zipCode = this.view.tbxEnterZipCode.text;
    return manuallyAddedPayee;
  },

  goToSerchedBillerDetails:function(){
    var manuallyAddedPayee = {};
    this.view.flxMisMatch.isVisible = false;
    if(this.handleErrorInBillerSearch()){
      manuallyAddedPayee.isManualUpdate = false;
      this.view.flxPayeeList.isVisible = false;
      if(this.view.tbxOne.info.fieldHolds === 'AccountNumber'){
        manuallyAddedPayee.accountNumber = this.view.tbxOne.text; 
        if(this.view.tbxThree.isVisible){
          if(this.view.tbxThree.info.fieldHolds === 'PolicyNumber'){
            manuallyAddedPayee.policyNumber = this.view.tbxThree.text;
          }          
        }
      }
      else if(this.view.tbxOne.info.fieldHolds === 'RelationshipNumber' ){
        manuallyAddedPayee.relationShipNumber = this.view.tbxOne.text;
        manuallyAddedPayee.mobileNumber = this.view.tbxThree.text;
      }
      manuallyAddedPayee.billerName = this.view.tbxCustomerName.text;
      manuallyAddedPayee.zipCode = this.view.tbxZipcode.text;    
      this.presenter.showUpdateBillerPage(manuallyAddedPayee);
      this.resetManualInformation();
    }

  },
  goToAddBillerDetails:function(){
    if(this.handleErrorInManualAddition()){
      var contextData = this.editManuallyAddedPayeeDetails();

      this.presenter.showUpdateBillerPage(contextData); 
      this.resetSearchedInformation();
    }

  },
  
  
  //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.flxHeader.frame.height + this.view.flxMain.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";
        this.view.forceLayout();
     } else {
        this.view.flxFooter.top = mainheight + "dp";
        this.view.forceLayout();
     }
  },
  presentPayeeDetails:function(payeeDetails,addressDetails){

    this.prePopulatePayeeDetails(payeeDetails,addressDetails);
    this.showflxAddBillerDetailsInformation();
    this.view.forceLayout();
  },
  prePopulatePayeeDetails:function(payeeDetails,addressDetails){

    if(payeeDetails.accountNumber === ""){
      this.view.lblAddAccNumValue.text =  kony.i18n.getLocalizedString("i18n.common.NotAvailable");
    }else{
      this.view.lblAddAccNumValue.text = payeeDetails.accountNumber; 
    }
    this.view.lblAddBillerValue.text = payeeDetails.billerName;
    //this.view.lstbxBillerAddress.mastarData = data;
     if(payeeDetails.Match === "0"){
      this.view.flxAddAddress.setVisibility(false);
    } 
    else if(addressDetails !== null && addressDetails !== "" && addressDetails.length>0 ){
    this.view.lstbxBillerAddress.masterData = addressDetails;
    this.view.lstbxBillerAddress.setVisibility(true);
	this.view.flxManullayAddress.setVisibility(false);
    this.view.flxAddAddress.setVisibility(true); 
    }
    else{
      this.view.flxAddAddress.setVisibility(true); 
      this.view.lstbxBillerAddress.setVisibility(false);
      this.view.flxManullayAddress.setVisibility(true);
      this.onSelectionPayeeAddress12();
    }
    this.view.tbxAddNickValue.text = payeeDetails.payeeNickName;
    this.view.tbxAddNameOnBillValue.text = payeeDetails.nameOnBill; 
  },
onSelectionPayeeAddress12: function() {
                    this.view.txtBxAddress.text = "";
            this.view.txtBxCity.text = "";
            this.view.txtBxZipCode.text = "";
            var jsonObj = {
                "AL": "Alabama",
                "AK": "Alaska",
                "AS": "American Samoa",
                "AZ": "Arizona",
                "AR": "Arkansas",
                "CA": "California",
                "CO": "Colorado",
                "CT": "Connecticut",
                "DE": "Delaware",
                "DC": "District Of Columbia",
                "FM": "Federated States Of Micronesia",
                "FL": "Florida",
                "GA": "Georgia",
                "GU": "Guam",
                "HI": "Hawaii",
                "ID": "Idaho",
                "IL": "Illinois",
                "IN": "Indiana",
                "IA": "Iowa",
                "KS": "Kansas",
                "KY": "Kentucky",
                "LA": "Louisiana",
                "ME": "Maine",
                "MH": "Marshall Islands",
                "MD": "Maryland",
                "MA": "Massachusetts",
                "MI": "Michigan",
                "MN": "Minnesota",
                "MS": "Mississippi",
                "MO": "Missouri",
                "MT": "Montana",
                "NE": "Nebraska",
                "NV": "Nevada",
                "NH": "New Hampshire",
                "NJ": "New Jersey",
                "NM": "New Mexico",
                "NY": "New York",
                "NC": "North Carolina",
                "ND": "North Dakota",
                "MP": "Northern Mariana Islands",
                "OH": "Ohio",
                "OK": "Oklahoma",
                "OR": "Oregon",
                "PW": "Palau",
                "PA": "Pennsylvania",
                "PR": "Puerto Rico",
                "RI": "Rhode Island",
                "SC": "South Carolina",
                "SD": "South Dakota",
                "TN": "Tennessee",
                "TX": "Texas",
                "UT": "Utah",
                "VT": "Vermont",
                "VI": "Virgin Islands",
                "VA": "Virginia",
                "WA": "Washington",
                "WV": "West Virginia",
                "WI": "Wisconsin",
                "WY": "Wyoming"
            };
            var data = [];
            for (var key in jsonObj) {
                if (jsonObj.hasOwnProperty(key)) {
                    var list = [];
                    list.push(key, jsonObj[key]);
                    data.push(list);
                }
            }
            this.view.lstbxState.masterData = data;
            this.view.lstbxState.selectedKeyValue = "Select";
            this.view.flxManullayAddress.setVisibility(true);
        
        this.view.forceLayout();
        this.AdjustScreen()
    },
  onSelectionPayeeAddress: function(){
    var selAddress = this.view.lstbxBillerAddress.selectedKeyValue[0];
    if(selAddress === "Other"){
      this.view.txtBxAddress.text="";
      this.view.txtBxCity.text="";
      this.view.txtBxZipCode.text="";
    var jsonObj={
    "AL": "Alabama",
    "AK": "Alaska",
    "AS": "American Samoa",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "DC": "District Of Columbia",
    "FM": "Federated States Of Micronesia",
    "FL": "Florida",
    "GA": "Georgia",
    "GU": "Guam",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MH": "Marshall Islands",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "MP": "Northern Mariana Islands",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PW": "Palau",
    "PA": "Pennsylvania",
    "PR": "Puerto Rico",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VI": "Virgin Islands",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming"
};
      var data = [];
      for(var key in jsonObj){
			if(jsonObj.hasOwnProperty(key)){
            var list = [];
            list.push(key,jsonObj[key]);
            data.push(list);
            }
       } 
      this.view.lstbxState.masterData=data;
      this.view.lstbxState.selectedKeyValue="Select";
      this.view.flxManullayAddress.setVisibility(true);
    } else {
      this.view.flxManullayAddress.setVisibility(false);
    }
    this.view.forceLayout();
    this.AdjustScreen();
  },  


  goToConfirmPayeeDetails:function(){
    var data = this.storeNickNameAndNameOnBill();
    this.view.flxAdditionError.isVisible = false;
    this.presenter.showAddPayeeConfirmPage(data);
  },
  storeNickNameAndNameOnBill:function(){
    var model = {
      payeeNickName:null,
      nameOnBill:null
    };
    model.payeeNickName = this.view.tbxAddNickValue.text;
    model.nameOnBill = this.view.tbxAddNameOnBillValue.text;
    return model;
  },
  presentConfirmPayeeDetails:function(completePayeeDetails){
    this.prePopulateConfirmPayeeDetails(completePayeeDetails);
    this.showFlxVerifyPayeeInformation();
  },
  prePopulateConfirmPayeeDetails:function(completePayeeDetails){
    if(this.view.flxAddAddress.isVisible === false){
      this.view.flxBillerAddress.setVisibility(false);
    }
    else if(this.view.lstbxBillerAddress.selectedKeyValue[0] === "Other"){
      var address= this.view.txtBxAddress.text;
      var city= this.view.txtBxCity.text;
      var zipCode= this.view.txtBxZipCode.text;
      var state= this.view.lstbxState.selectedKey;
      var totalAddress= address + " , " + city +" "+state + " "+zipCode;
      this.view.flxBillerAddress.setVisibility(true);
      completePayeeDetails.billerAddress=totalAddress;
    } 
    else if(this.view.lstbxBillerAddress.isVisible === false){
      var address= this.view.txtBxAddress.text;
      var city= this.view.txtBxCity.text;
      var zipCode= this.view.txtBxZipCode.text;
      var state= this.view.lstbxState.selectedKey;
      var totalAddress= address + " , " + city +" "+state + " "+zipCode;
      this.view.flxBillerAddress.setVisibility(true);
      completePayeeDetails.billerAddress=totalAddress;
    }
      else{
        this.view.flxBillerAddress.setVisibility(true);
      completePayeeDetails.billerAddress = this.view.lstbxBillerAddress.selectedKeyValue[1];
    }
    this.view.lblBillerValue.text = completePayeeDetails.billerName;
    this.view.rtxBillerAddress.text = completePayeeDetails.billerAddress;
    if(completePayeeDetails.accountNumber === ""){
      this.view.lblBillerAccNumValue.text =  kony.i18n.getLocalizedString("i18n.common.NotAvailable");
    }
    else{
      this.view.lblBillerAccNumValue.text = completePayeeDetails.accountNumber;
    }

    this.view.lblNicknameValue.text = completePayeeDetails.payeeNickName;
    this.view.lblNameOnBillValue.text = completePayeeDetails.nameOnBill;
  },
  goToAddPayeeAcknowledgement:function(){
    kony.olb.utils.showProgressBar(this.view);
    var address = this.view.lstbxBillerAddress.selectedKeyValue[0]; 
    if(this.view.lstbxBillerAddress.selectedKeyValue[0] === "Other"){
      var tmpaddress= this.view.txtBxAddress.text;
      var city= this.view.txtBxCity.text;
      var zipCode= this.view.txtBxZipCode.text;
      var state= this.view.lstbxState.selectedKey;
      address= tmpaddress + "@" + city +"@"+state + "@"+zipCode;
    }    
    else if(this.view.lstbxBillerAddress.isVisible === false){
			var tmpaddress = this.view.txtBxAddress.text;
            var city = this.view.txtBxCity.text;
            var zipCode = this.view.txtBxZipCode.text;
            var state = this.view.lstbxState.selectedKey;
            address = tmpaddress + "@" + city + "@" + state + "@" + zipCode;
		}
    this.presenter.showAddPayeeSucess(null,address);
  },
  presentAddPayeeAcknowledgement:function(successDetails){
    this.prePopulateAcknowledgementDetails(successDetails);
    this.showFlxAddPayeeAck();
  },
  prePopulateAcknowledgementDetails:function(model){
    this.view.lblAcknowledgementMessage.text = "'"+model.billerName+"' " + kony.i18n.getLocalizedString("i18n.common.HasBeenAddedSuccessfully");
    if(this.view.flxBillerAddress.isVisible ===false){
    this.view.flxDetailsAddess.setVisibility(false);
    }
    else if(this.view.lstbxBillerAddress.selectedKeyValue[0] === "Other"){
      var address= this.view.txtBxAddress.text;
      var city= this.view.txtBxCity.text;
      var zipCode= this.view.txtBxZipCode.text;
      var state= this.view.lstbxState.selectedKey;
      var totalAddress= address + " , " + city +" "+state + " "+zipCode;
      this.view.flxDetailsAddess.setVisibility(true);
      model.billerAddress=totalAddress;
    }
    else if(this.view.lstbxBillerAddress.isVisible === false){
      var address= this.view.txtBxAddress.text;
      var city= this.view.txtBxCity.text;
      var zipCode= this.view.txtBxZipCode.text;
      var state= this.view.lstbxState.selectedKey;
      var totalAddress= address + " , " + city +" "+state + " "+zipCode;
      this.view.flxDetailsAddess.setVisibility(true);
      model.billerAddress=totalAddress;
    }
    else{
    this.view.flxDetailsAddess.setVisibility(true);
    model.billerAddress = this.view.lstbxBillerAddress.selectedKeyValue[1];
    }
    this.view.lblBillerVal.text = model.billerName;
    this.view.rtxDetailsAddressVal.text = model.billerAddress;
    if(model.accountNumber === ""){
      this.view.lblAccNumberVal.text = kony.i18n.getLocalizedString("i18n.common.NotAvailable");
    }
    else{
      this.view.lblAccNumberVal.text = model.accountNumber;
    }

    this.view.lblDetailsNickVal.text = model.payeeNickName;
    // this.view.lblDetailsNOBVal.text = model.nameOnBill;
  },
  disableButton : function (button) {
    button.setEnabled(false);
    button.skin = "sknBtnBlockedLatoFFFFFF15Px";
    button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
    button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";     
  },
  enableButton : function(button){
    button.setEnabled(true);   
    button.skin = "sknbtnLatoffffff15px";
    button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
    button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";    
  },
  initiateAddPayee:function(){
    this.hideCancelPopup();
    this.view.flxAddPayee.setVisibility(true);
    this.view.flxonetimepayment.setVisibility(false);
    this.view.breadcrumb.setBreadcrumbData([{text:"BILL PAY"}, {text:"ADD PAYEE"}]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.addPayee");

    this.view.lblAddPayee.text = "ADD PAYEE";    
    this.view.flxMainWrapper.isVisible=true;
    this.view.flxVerifyPayeeInformation.isVisible=false;
    this.view.flxAddBillerDetails.isVisible=false;
    this.view.flxAddPayeeAck.isVisible=false;  
    this.initializePayeeInformation();
    this.registerWidgetActions();
    this.showFlxSearchPayee();
    this.accountNumberAvailable = true;

    this.view.customheader.topmenu.flxMenu.skin="slFbox";
    this.view.customheader.topmenu.flxaccounts.skin="slFbox";
    this.view.customheader.topmenu.flxTransfersAndPay.skin="sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.flxaccounts.skin="sknHoverTopmenu7f7f7pointer"; 
    //this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
    this.view.flxAdditionError.isVisible = false;
    this.setLogoutEvents();
    this.view.btnDetailsBack.onClick = this.showMainWrapper;
    this.view.btnModify.onClick = this.showMainWrapper;
  },

  validateManuallyAddedPayeeDetails:function(){
    //var name = this.view.tbxEnterName.text;
    //var address = this.view.tbxEnterAddress.text;
    //var city = this.view.tbxCity.text;
    //var zipcode = this.view.tbxEnterZipCode.text;
    var accountNumber = this.view.tbxEnterAccountNmber.text;
    var duplicateAccountNumber = this.view.tbxConfirmAccNumber.text ;
    // var note = this.view.tbxAdditionalNote.text;
    if(this.accountNumberAvailable){

      if(accountNumber === duplicateAccountNumber){
        return 'VALIDATION_SUCCESS';
      }
      else{
        return 'ACCOUNT_NUMBER_MISMATCH';
      }
    }

    else{
      return 'VALIDATION_SUCCESS';
    } 

  },

  handleErrorInManualAddition:function(){
    var response = this.validateManuallyAddedPayeeDetails();
    if( response === 'VALIDATION_SUCCESS'){
      //this.enableButton(this.view.btnNext);
      this.view.lblErrorInfo.isVisible = false;
      this.normalizeBoxes(this.view.tbxEnterAccountNmber,this.view.tbxConfirmAccNumber);
      return true;
    }
    else if(response === 'ACCOUNT_NUMBER_MISMATCH'){
      this.view.lblErrorInfo.text = kony.i18n.getLocalizedString("i18n.addPayee.AccountNumberMismatch");
      this.view.lblErrorInfo.isVisible = true;
      this.disableButton(this.view.btnNext);
      this.higlightBoxes(this.view.tbxEnterAccountNmber,this.view.tbxConfirmAccNumber);
      this.view.forceLayout();
      return false;
    }

  },
  validateSearchedPayeeDetails:function(){
    var name = this.view.tbxCustomerName.text;
    var zipcode = this.view.tbxZipcode.text;
    var identityNumber = this.view.tbxOne.text;
    var duplicateIdentityNumber = this.view.tbxTwo.text ;
    if(this.view.tbxOne.info === undefined){
      return 'NO_BILLER_SELECTED_FROM_LIST';
    }
    else{
      if(identityNumber === duplicateIdentityNumber){
        return 'VALIDATION_SUCCESS';
      }
      else{
        return 'IDENTITY_NUMBER_MISMATCH';
      }            
    }


  }, 
  handleErrorInBillerSearch:function(){
    var response = this.validateSearchedPayeeDetails();   
    if( response === 'VALIDATION_SUCCESS'){
      //this.enableButton(this.view.btnNext);
      this.normalizeBoxes(this.view.tbxOne,this.view.tbxTwo);
      this.view.lblNotMatching.isVisible = false;
      return true;
    }
    else if(response === 'IDENTITY_NUMBER_MISMATCH'){
      if(this.view.tbxOne.info.fieldHolds === 'AccountNumber'){
        this.view.lblNotMatching.text = kony.i18n.getLocalizedString("i18n.addPayee.AccountNumberMismatch");
      }
      else if(this.view.tbxOne.info.fieldHolds === 'RelationshipNumber'){
        this.view.lblNotMatching.text = kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberMismatch");
      }

      this.view.flxMisMatch.isVisible = false;      
      this.view.lblNotMatching.isVisible = true;
      this.disableButton(this.view.btnNext2);
      this.higlightBoxes(this.view.tbxOne,this.view.tbxTwo);
      this.view.forceLayout();
      return false;
    }
    else if(response === 'NO_BILLER_SELECTED_FROM_LIST'){
      this.view.lblNotMatching.text = kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller");
      this.view.flxMisMatch.isVisible = false;      
      this.view.lblNotMatching.isVisible = true;
      this.disableButton(this.view.btnNext2);

      this.view.forceLayout();
      return false;
    }
  },    
  registerWidgetActions:function(){
    this.disableButton(this.view.btnNext);
    this.disableButton(this.view.btnNext2);
    [this.view.tbxEnterName,this.view.tbxEnterAddress,this.view.tbxEnterAddressLine2,this.view.tbxCity,
     this.view.tbxEnterZipCode,this.view.tbxEnterAccountNmber,this.view.tbxConfirmAccNumber,this.view.tbxAdditionalNote].map(function(element){
      element.onKeyUp = this.checkIfAllManualFieldsAreFilled;
    },this);
    [this.view.tbxZipcode,this.view.tbxOne,this.view.tbxTwo,this.view.tbxThree].map(function(element){
      element.onKeyUp = this.checkIfAllSearchFieldsAreFilled;
    },this);  
    this.view.tbxCustomerName.onKeyUp = this.callOnKeyUp;
  },
  callOnKeyUp:function(){
    //this.presenter.fetchBillerList(this.view.tbxCustomerName.text);
    var AddPayeeModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AddPayeeModule");
    AddPayeeModule.presentationController.fetchBillerList(this.view.flxonetimepayment.isVisible ? this.view.oneTimePay.tbxName.text:this.view.tbxCustomerName.text);   
    this.checkIfAllSearchFieldsAreFilled();
  },
  higlightBoxes:function(){
    for(var i = 0; i < arguments.length; i++){
      arguments[i].skin = 'sknTbxLatoffffff15PxBorderFF0000opa50';
    }
  },
  normalizeBoxes:function(){
    for(var i = 0; i < arguments.length; i++){
      arguments[i].skin = 'sknTbxLatoffffff15PxBorder727272opa20';
    }    
  },
  checkIfAllSearchFieldsAreFilled:function(){
    if (this.view.flxonetimepayment.isVisible) {
      if (this.view.oneTimePay.tbxName.text && this.view.oneTimePay.txtZipCode.text && this.view.oneTimePay.txtAccountNumber.text && this.view.oneTimePay.txtAccountNumberAgain.text &&
          ((this.view.oneTimePay.txtmobilenumber.isVisible && this.view.oneTimePay.txtmobilenumber.text) || !this.view.oneTimePay.txtmobilenumber.isVisible)) {
        this.enableButton(this.view.oneTimePay.btnNext);
      } else {
        this.disableButton(this.view.oneTimePay.btnNext);
      }
    } else { // need to change the conditions
      if (this.view.tbxThree.isVisible === true) {
        if (this.view.tbxCustomerName.text && this.view.tbxZipcode.text && this.view.tbxOne.text && this.view.tbxTwo.text && this.view.tbxThree.text) {
          this.enableButton(this.view.btnNext2);
        }
        else {
          this.disableButton(this.view.btnNext2);
        }
      }
      else {
        if (this.view.tbxCustomerName.text && this.view.tbxZipcode.text && this.view.tbxOne.text && this.view.tbxTwo.text) {
          this.enableButton(this.view.btnNext2);
        }
        else {
          this.disableButton(this.view.btnNext2);
        }
      }
    }

  },
  checkIfAllManualFieldsAreFilled:function(){
    if(this.view.tbxAdditionalNote.isVisible === true){
      if(this.view.tbxEnterName.text && this.view.tbxEnterAddress.text && this.view.tbxCity.text && this.view.tbxEnterZipCode.text ){
        this.enableButton(this.view.btnNext);
      }
      else{
        this.disableButton(this.view.btnNext);
      }      
    }
    else{
      if(this.view.tbxEnterName.text && this.view.tbxEnterAddress.text && this.view.tbxCity.text && this.view.tbxEnterZipCode.text &&
         this.view.tbxEnterAccountNmber.text && this.view.tbxConfirmAccNumber.text){
        this.enableButton(this.view.btnNext);
      }
      else{
        this.disableButton(this.view.btnNext);
      }      
    }

  },

  updateRegisteredPayees:function(registeredPayees){
    this.view.segRegisteredPayees.widgetDataMap = {
      "flxRegistered": "flxRegistered",
      "lblCustomer": "lblCustomer",
      "lblAmount":"lblAmount",
      "lblDate":"lblDate",
      "btnViewDetails":"btnViewDetails",
      "btnPayBills":"btnPayBills",
      "lblHorizontalLine":"lblHorizontalLine"
    };
    this.view.segRegisteredPayees.setData(this.createRegisteredPayeesSegmentModel(registeredPayees));
    this.view.forceLayout();

  },
  createRegisteredPayeesSegmentModel:function(payees){
    var scopeObj=this;
    var balanceInDollars = function(amount) {
      return scopeObj.CommonUtilities.formatCurrencyWithCommas(amount);
    };     
    var getDateFromDateString = function(dateStr)
    {
      if(dateStr) {
        return scopeObj.CommonUtilities.getFrontendDateString(dateStr, kony.onlineBanking.configurations.getConfiguration("frontendDateFormat"));
      }
      else {
        return "";
      }
    };    
    return payees.map(function(payee) {
      return {
        "lblCustomer": payee.payeeName,
        "lblHorizontalLine": "A",
        "lblDate": getDateFromDateString(payee.lastPaidDate),
        "lblAmount": balanceInDollars(payee.lastPaidAmount),
        "btnViewDetails":{
          text:kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
          onClick:payee.onViewDetailsClick
        },
        "btnPayBills":{
          text:kony.i18n.getLocalizedString("i18n.addPayee.PayBills"),
          onClick:payee.onPayBillsClick					
        },

      };
    }).reduce(function(p, e) {return p.concat(e);}, []);    
  },
  enterCorrectBillerName: function(){
    this.view.rtxMisMatch.text = kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller");;
    this.view.lblNotMatching.isVisible = false;
    this.view.flxMisMatch.isVisible = true;
    this.disableButton(this.view.btnNext2);
    this.view.forceLayout();
  },
  showValidationError:function(response){
    var errorMessage = response;
    if(response === "INVALID_BILLER"){
      errorMessage = kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller");
    }else if(response === "MISMATCH_RELATIONSHIPNUMBER"){
      errorMessage = kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberMismatch");
    }else if (response === "MISMATCH_ACCOUNTNUMBER"){
      errorMessage = kony.i18n.getLocalizedString("i18n.addPayee.AccountNumberMismatch");
    }
    if(this.view.flxonetimepayment.isVisible){
      this.view.oneTimePay.lblWarning.text = errorMessage;
      this.view.oneTimePay.lblWarning.isVisible= true;
      this.disableButton(this.view.oneTimePay.btnNext);
      if(response === "INVALID_BILLER"){
        this.higlightBoxes(this.view.oneTimePay.tbxName);
      }else{
        this.higlightBoxes(this.view.oneTimePay.txtAccountNumber, this.view.oneTimePay.txtAccountNumberAgain);
      }
    } else {
      this.view.rtxMisMatch.text = errorMessage;
      this.view.lblNotMatching.isVisible = false;
      this.view.flxMisMatch.isVisible = true;
      this.disableButton(this.view.btnNext2);
    }
    this.view.forceLayout();
  },
  showAddPayeeErrorMessage:function(msg){
    this.view.rtxAdditionError.text = msg;
    this.view.flxAdditionError.isVisible = true;

    this.showflxAddBillerDetailsInformation(); 
    this.view.flxAddBillerDetails.top = '10dp';
    this.view.forceLayout();
  },

  updateHamburgerMenu: function (sideMenuModel) {
    this.view.customheader.initHamburger(sideMenuModel,"BILL PAY","Add Payee");
  },
  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  },
  showProgressBar:function(){
    kony.olb.utils.showProgressBar(this.view);    
  },
  hideProgressBar:function(){
    kony.olb.utils.hideProgressBar(this.view);    
  },
});
