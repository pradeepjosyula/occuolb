define(['CommonUtilities'], function(CommonUtilities) {
 return{
  shouldUpdateUI: function (viewModel) {
    return viewModel !== undefined && viewModel !== null;
  },

  willUpdateUI : function(locateUsViewModel) {
    if(locateUsViewModel.ProgressBar){
      if(locateUsViewModel.ProgressBar.show){
        
        this.showProgressBar();
      }
      else {
        this.hideProgressBar();
      }
    }
    if (locateUsViewModel.sideMenu) {
      this.updateHamburgerMenu(locateUsViewModel.sideMenu);
    }
    if (locateUsViewModel.topBar) {
      this.updateTopBar(locateUsViewModel.topBar);
    }
    if(locateUsViewModel.preLoginView) {
      this.showPreLoginView();
    }
    if(locateUsViewModel.postLoginView) {
      this.showPostLoginView();
    }
    if(locateUsViewModel.locationListViewModel) {
      this.searchResultData=locateUsViewModel.locationListViewModel;
      this.updateSearchSegmentResult(locateUsViewModel.locationListViewModel);
      this.globalMapLat=this.presenter.globalLat;
      this.globalMapLong=this.presenter.globalLon;
      this.filterSearchData();
    }
	if(locateUsViewModel.getAtmorBranchDetailsSuccess){
      this.getAtmOrBranchDetailsSuccessCallback(locateUsViewModel.getAtmorBranchDetailsSuccess);
    }
    if(locateUsViewModel.getAtmorBranchDetailsFailure){
      this.getAtmOrBranchDetailsErrorCallback();
    }
    if(locateUsViewModel.getSearchBranchOrATMListSuccess){
      this.getSearchBranchOrATMListSuccessCallback(locateUsViewModel.getSearchBranchOrATMListSuccess);
    }
    if(locateUsViewModel.getSearchBranchOrATMListFailure){
      this.getSearchBranchOrATMListErrorCallback();
    }
    if(locateUsViewModel.getBranchOrATMListSuccess){
      this.getBranchOrATMListSuccessCallback(locateUsViewModel.getBranchOrATMListSuccess);
    }
    if(locateUsViewModel.getBranchOrATMListFailure){
       this.getBranchOrATMListErrorCallback();
    }
    if(locateUsViewModel.geoLocationError){
       this.noSearchResultUI();
       this.view.LocateUs.rtxNoSearchResults.text="GeoLocation Not supported by the browser";
       kony.olb.utils.hideProgressBar(this.view);
    }
    this.AdjustScreen();   
    
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox"; 
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
  },

  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  },

  updateHamburgerMenu: function (sideMenuModel) {
    this.view.customheader.initHamburger(sideMenuModel,"About Us","Locate Us");    
  },

  /**
   * Hide Progress bar
   */
  hideProgressBar : function(data){
    CommonUtilities.hideProgressBar(this.view);
  },
  /**
   * Hide Progress bar
   */
  showProgressBar : function(data){
    CommonUtilities.showProgressBar(this.view);
  },

  getCurrentLocationData : function() {
    return {
        "lat": this.globalMapLat,
        "lon": this.globalMapLong,
        "locationId": "",
        "showcallout": false,
        "calloutData": {
            "lblBranchName": "",
            "lblBranchAddressOneLine": "",
            "lblBranchAddress2": "",
            "lblOpen": "",
            "lblClosed": "",
            "imgGo": "arrow_left_grey.png",
            "flxLocationDetails": {
                "onClick": function() {
                    
                },
            },
        },
        "imgBuildingType":  "bank_icon.png",
        "imgGo": "arrow_left_grey.png",
        "imgIndicator": {
            "src": "accounts_sidebar_blue.png",
            "isVisible": false,
        },
        "lblAddress": "",
        "lblClosed": "",
        "lblName": "",
        "lblOpen": "",
        "lblSeperator": " ",
        "flxSearchResults": {
            "skin": "sknFlexF9F9F9",
            "hoverSkin": "sknsegf9f9f9hover"
        },
        "template": "flxSearchResults",
    };
  },

  bindMapData : function(data) {
    var scopeObj = this;
    var mapData = data;
    if(mapData.length>1) {
        mapData = JSON.parse(JSON.stringify(data));
        mapData.map(function(dataItem){
          dataItem.calloutData.flxLocationDetails.onClick = function() {
              scopeObj.view.LocateUs.mapLocateUs.dismissCallout();
              scopeObj.onRowClickSegment(dataItem);
              scopeObj.pinToSegment(dataItem);
              scopeObj.showBranchDetails();
          }
          return dataItem;
        });
        mapData.push(this.getCurrentLocationData());
    }
    this.view.LocateUs.flxMap.setVisibility(true);
    this.view.LocateUs.flxBranchDetails.setVisibility(false);
    this.view.LocateUs.flxHideMap.setVisibility(false);
    this.view.LocateUs.mapLocateUs.calloutTemplate = "flxDistanceDetails";
	  this.view.LocateUs.mapLocateUs.zoomLevel = 15;
    this.view.LocateUs.mapLocateUs.widgetDataMapForCallout = {
        "lblBranchName": "lblBranchName",
        "lblBranchAddressOneLine": "lblBranchAddressOneLine",
        "lblBranchAddress2": "lblBranchAddress2",
        "lblOpen": "lblOpen",
        "lblClosed": "lblClosed",
        "imgGo": "imgGo",
      	"flxLocationDetails":"flxLocationDetails",
    };
    this.view.LocateUs.mapLocateUs.locationData = mapData;
  },

  bindSearchSegmentData : function(data) {
    var scopeObj=this;
    scopeObj.searchResultUI();
    var segmentDataMap = {
      "flxDetails": "flxDetails",
      "flxDistanceAndGo": "flxDistanceAndGo",
      "flxIndicator": "flxIndicator",
      "flxSearchResults": "flxSearchResults",
      "imgBuildingType": "imgBuildingType",
      "imgGo": "imgGo",
      "imgIndicator": "imgIndicator",
      "lblAddress": "lblAddress",
      "lblClosed": "lblClosed",
      "lblName": "lblName",
      "lblOpen": "lblOpen",
      "lblSeperator": "lblSeperator"
    };
    this.view.LocateUs.segResults.widgetDataMap = segmentDataMap;
    this.view.LocateUs.segResults.setData(data);
  },

  updateSearchSegmentResult : function(locationListViewModel){
    var scopeObj=this;
    if(locationListViewModel.length === 0 || locationListViewModel[0].latitude==null ) {
      scopeObj.noSearchResultUI();
    } else {
        var data = locationListViewModel.map(function(dataItem) {
          return {
              "lat": dataItem.latitude,
              "lon": dataItem.longitude,
              "locationId" : dataItem.locationId,
              "showcallout" : true,
              "image" : dataItem.type === "BRANCH" ? "bank_icon_blue.png" : "atm_icon_blue.png",
              "calloutData": {
                  "lblBranchName" : dataItem.informationTitle,
                  "lblBranchAddressOneLine" : dataItem.addressLine1,
                  "lblBranchAddress2" : dataItem.addressLine2,
                  "lblOpen" : {
                      "text": "OPEN",
                      "isVisible": dataItem.status === "OPEN" ? true : false
                   },
                  "lblClosed" :  {
                      "text": "CLOSED",
                      "isVisible": dataItem.status === "OPEN" ? false : true
                  },
                  "imgGo" : "arrow_left_grey.png",
                  "flxLocationDetails":{
                    "onClick" : function () {
                      scopeObj.view.LocateUs.mapLocateUs.dismissCallout();
                      scopeObj.onRowClickSegment(dataItem);
                      scopeObj.pinToSegment(dataItem);
                      scopeObj.showBranchDetails();
                    }
                  }
              },
              "imgBuildingType": dataItem.type === "BRANCH" ? "bank_icon.png" : "transaction_type_withdrawl.png",
              "imgGo": "arrow_left_grey.png",
              "imgIndicator":{
                "src":"accounts_sidebar_blue.png",
                "isVisible":false,
              },
              "lblAddress": dataItem.addressLine2,
              "lblClosed": {
                "text":"CLOSED",
                "isVisible" : dataItem.status === "OPEN" ? false : true
               },
              "lblName": dataItem.informationTitle,
              "lblOpen": {
                "text":"OPEN",
                "isVisible":dataItem.status === "OPEN" ? true : false
              },
              "lblSeperator": " ",
              "flxSearchResults":{
                "skin":"sknFlexF9F9F9",
                "hoverSkin": "sknsegf9f9f9hover"
              },
              "template":"flxSearchResults",
          };
        });
        this.bindSearchSegmentData(data);        
        this.bindMapData(data);
    }
  },
  postShowLocateUs: function(){
    this.AdjustScreen();
  },
  //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  },   
  showNoMapPins:function(){
    var scopeObj=this;
    var locationData = {
        lat: scopeObj.globalMapLat, 
        lon: scopeObj.globalMapLong,
    }
    //this.view.LocateUs.mapLocateUs.navigateToLocation(locationData, false, false);
    var data = [];
    data.push(locationData);
    this.bindMapData(data);
  },  
  gotoCurrentLocation : function(){  
	this.view.LocateUs.mapLocateUs.navigateToLocation({ lat: this.presenter.globalLat, 
                                                lon: this.presenter.globalLon
                                               }, false, false);
  this.view.LocateUs.mapLocateUs.zoomLevel = 15;
    this.view.forceLayout();

  },
  processServices: function(data) {
    data = data.split("||");
    try {
        this.view.LocateUs.lblService1.text = data[0].trim();
        this.view.LocateUs.lblService2.text = data[1].trim();
        this.view.LocateUs.lblService3.text = data[2].trim();
        this.view.LocateUs.lblService4.text = data[3].trim();
    } catch (ex) {
        kony.print("Services UnAvailable");
    }
  },

  showViews : function(views){
    this.view.LocateUs.flxRight.height="650px";
    this.view.LocateUs.height="700px";
    if (views=="flxSearch" || views == "flxViewsAndFilters" || views=="flxDirections") {
      this.view.LocateUs.flxSearch.setVisibility(false);
      this.view.LocateUs.flxViewsAndFilters.setVisibility(false);
      this.view.LocateUs.flxDirections.setVisibility(false);
    }
    else{
      this.view.LocateUs.flxMap.setVisibility(false);
      this.view.LocateUs.flxBranchDetails.setVisibility(false);
      this.view.LocateUs.flxHideMap.setVisibility(false);
    }
    for (var i = 0; i < views.length; i++) {
      this.view.LocateUs[views[i]].isVisible = true;
    }
    this.view.LocateUs.forceLayout();
  },

  //all flex show methods
  showPreLoginView: function(){
    this.view.flxContainer.top = "70dp";
    this.view.customheader.FlexContainer0e2898aa93bca45.height = "70dp";
    this.view.customheader.flxBottomContainer.height = "70dp";
    this.view.customheader.flxTopmenu.setVisibility(false);
    this.view.customheader.headermenu.flxMessages.isVisible = false;
    this.view.customheader.headermenu.flxVerticalSeperator1.isVisible = false;
    this.view.customheader.headermenu.flxResetUserImg.isVisible = false;
    this.view.customheader.headermenu.flxUserId.isVisible = false;
    this.view.customheader.flxSeperatorHor2.setVisibility(false);
    this.view.customheader.headermenu.flxWarning.isVisible = true;
    this.view.customheader.headermenu.btnLogout.text = kony.i18n.getLocalizedString("i18n.common.login");
    this.view.customheader.headermenu.btnLogout.onClick = function () {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        authModule.presentationController.showLoginScreen();
    };
    this.view.LocateUs.segResults.setData([]);
    this.view.LocateUs.mapLocateUs.locationData = [];
    this.presenter.getBranchOrATMList();
    this.view.LocateUs.forceLayout();
  },
  showPostLoginView: function(){
    var self = this;
    this.view.flxContainer.top = "120dp";
    this.view.customheader.FlexContainer0e2898aa93bca45.height = "120dp";
    this.view.customheader.flxBottomContainer.height = "120dp";
    this.view.customheader.flxTopmenu.setVisibility(true);
    this.view.customheader.headermenu.flxMessages.isVisible = true;
    this.view.customheader.headermenu.flxVerticalSeperator1.isVisible = true;
    this.view.customheader.headermenu.flxResetUserImg.isVisible = true;
    this.view.customheader.headermenu.flxUserId.isVisible = true;
    this.view.customheader.flxSeperatorHor2.setVisibility(true);
    this.view.customheader.headermenu.flxWarning.isVisible = false;
    this.view.customheader.headermenu.btnLogout.text = kony.i18n.getLocalizedString("i18n.common.logout");
    this.view.LocateUs.segResults.setData([]);
    this.view.LocateUs.mapLocateUs.locationData = [];
    this.view.customheader.headermenu.btnLogout.onClick = function () {
        kony.print("btn logout pressed");
        self.view.flxLogout.setVisibility(true);
        kony.print("btn logout pressed");
        self.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        self.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height = self.view.flxHeader.frame.height + self.view.flxContainer.frame.height;
        self.view.flxLogout.height = height + "dp";
        self.view.flxLogout.left = "0%";
    };
    this.view.CustomPopup.btnYes.onClick = function () {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
            "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        self.view.flxLogout.setVisibility(false);
        self.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        self.view.flxLogout.setVisibility(false);
        self.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        self.view.flxLogout.setVisibility(false);
        self.view.flxLogout.left = "-100%";
    };
    this.presenter.getBranchOrATMList();
    this.view.LocateUs.forceLayout();
  },

  getBranchOrATMListSuccessCallback : function(data) {
  	  this.searchResultData = data;
      this.updateSearchSegmentResult(data);
      this.globalMapLat=this.presenter.globalLat;
      this.globalMapLong=this.presenter.globalLon;
      this.filterSearchData();
      kony.olb.utils.hideProgressBar(this.view);
      this.view.LocateUs.forceLayout();
  },

  getBranchOrATMListErrorCallback : function() {
  	   this.noSearchResultUI();
       kony.olb.utils.hideProgressBar(this.view);
  	   this.view.LocateUs.forceLayout();
  },

  showSearch : function(){
   // this.setFilters();
    this.segSetFilters();
    this.showViews(["flxSearch"]);
  },
  showViewsAndFilters : function(){
    this.showViews(["flxViewsAndFilters"]);
    this.showHideMap();
    
  },
  showDirections : function(){
    this.showViews(["flxDirections"]);
  },
  showMap : function(){
    this.showViews(["flxMap"]);
  },
  showBranchDetails : function(){
    var scopeObj=this;
    scopeObj.showViews(["flxBranchDetails"]);
    
  },
  showHideMap : function(){
    this.showViews(["flxHideMap"]);
  },
  showRedoSearch : function(boolValue){
    if(this.view.LocateUs.flxRedoSearch.isVisible!=boolValue){
      this.view.LocateUs.flxRedoSearch.isVisible=boolValue;
    }
  },
  removeFilter2 :function(filterNum){
    var lblName="lblFilterName"+filterNum;
    var cancelBtnName="flxCancelFilter"+filterNum;
    this.view.LocateUs[lblName].setVisibility(false);
    this.view.LocateUs[cancelBtnName].setVisibility(false);
  },
  selectRadioButton :function(imgRadio){
    this.view.LocateUs.imgRadioButtonAll.src="icon_radiobtn.png";
    this.view.LocateUs.imgRadioButtonBranch.src="icon_radiobtn.png";
    this.view.LocateUs.imgRadioButtonAtm.src="icon_radiobtn.png";
    imgRadio.src="radiobtn_active.png";
  },
  toggleCheckBox: function(imgCheckBox){
    var data = this.view.LocateUs.segViewsAndFilters.data;
    if(imgCheckBox.src=="unchecked_box.png"){
      imgCheckBox.src="checked_box.png";
    }
    else{
      imgCheckBox.src="unchecked_box.png";
    }
    this.view.LocateUs.segViewsAndFilters.setData(data);
  },
	SegRemoveFilter: function(a){
    var data = this.view.LocateUs.segViewsAndFilters.data;
   	data[a].imgCheckBox.src="unchecked_box.png";   
    this.view.LocateUs.segViewsAndFilters.setData(data);
  },
  clearAll : function(){
    var data = this.view.LocateUs.segViewsAndFilters.data;
    for(var i=0; i<data.length; i++)
      {
        data[i].imgCheckBox.src = "unchecked_box.png";
      }
    this.view.LocateUs.segViewsAndFilters.setData(data);
  },
  showShareDirection: function () {
    this.view.flxShareDirection.setVisibility(true);
  },
  showTextbox : function(){
    if(this.view.lbxSendMapTo.selectedKey!="key1"){
      this.view.tbxSendMapTo.setVisibility(true);
      this.view.btnShareSend.skin="sknBtn3343a8Border3343a82pxOp100Radius2px";
      this.view.btnShareCancel.skin="sknBtnffffffBorder3343a81pxRadius2px";
    }
    else{
      this.view.tbxSendMapTo.setVisibility(false);
      this.view.btnShareSend.skin="sknBtnBlockedLatoFFFFFF15Px";
      //this.view.btnShareCancel.skin="sknBtnffffffBorder3343a81pxRadius2px"; 
    }
  },
  hideShareDirection: function(){
    this.view.flxShareDirection.setVisibility(false);
  },
  changeResultsRowSkin: function(){
    var index = this.view.LocateUs.segResults.selectedIndex;
    var rowIndex = index[1];
    var data = this.view.LocateUs.segResults.data;
    for (var i = 0; i < data.length; i++) {
      data[i].flxSearchResults.skin="sknFlxffffffBorder0";
      data[i].imgIndicator.isVisible=false;
    }
    data[rowIndex].flxSearchResults.skin="sknFlexF9F9F9";
    data[rowIndex].imgIndicator.isVisible=true;
    this.view.LocateUs.segResults.setData(data);
    this.view.forceLayout();
  },
  //preshow
  locateUsPreshow: function(){
    var scopeObj=this;
    this.setFlowActions();
    this.setDirectionsSegmentData();
    this.setViewsAndFilterSegmentData();
    this.segSetFilters();
    this.selectedViewOnApply=this.view.LocateUs.imgRadioButtonAll
    this.selectedServicesList=[];
    this.view.LocateUs.lblViewType.text=kony.i18n.getLocalizedString("i18n.locateus.view")+": "+kony.i18n.getLocalizedString("i18n.locateus.Branches&Atms");
    this.initializationForFirstCancel();   
    this.view.LocateUs.tbxSearchBox.text="";
    this.view.customheader.flxTopmenu.topmenu.flxaccounts.skin = "copyslfbox1";
  },
  initializationForFirstCancel:function(){
    this.viewText=this.view.LocateUs.lblViewType.text;
    this.prevData=["unchecked_box.png", "unchecked_box.png", "unchecked_box.png", "unchecked_box.png", "unchecked_box.png"];
    this.selectedView="ALL"; 
  },
  segSetFilters: function(){
    var scopeObj=this;
    var filterWidget;
    var filterCloseBtn;
    var filterNum=1;
    this.selectedServicesList=[];
    scopeObj.prevData=[];
    var data = this.view.LocateUs.segViewsAndFilters.data;
    var l= data.length;
    for (var i = 0; i < l; i++) {
      scopeObj.prevData.push(data[i].imgCheckBox.src);
      if (data[i].imgCheckBox.src=="checked_box.png") 
      {
        filterlblWidget="lblFilterName"+filterNum;
        filterCloseBtn="flxCancelFilter"+filterNum;
        var lbl = data[i].lblOption;
        this.selectedServicesList.push(lbl);
        this.view.LocateUs[filterlblWidget].text=lbl;
        this.view.LocateUs[filterlblWidget].setVisibility(true);
        this.view.LocateUs[filterCloseBtn].setVisibility(true);
        filterNum++;
      }
    }
    if (filterNum>1) {
      this.view.LocateUs.btnClearAll.setVisibility(true);
    }
    if(filterNum === 1 )
    {
      this.view.LocateUs.flxFilters.height= "0dp";
      this.view.LocateUs.btnClearAll.setVisibility(false);
    }
    if(filterNum === 2 || filterNum===3)
    {
      this.view.LocateUs.flxFilters.height= "42dp";
      this.view.LocateUs.btnClearAll.top = "80dp";
    }
    if(filterNum === 4 || filterNum===5)
    {
      this.view.LocateUs.flxFilters.height= "74dp";
      this.view.LocateUs.btnClearAll.top = "110dp";     
    }
    if(filterNum === 6)
    {
      this.view.LocateUs.flxFilters.height= "104dp"; 
      this.view.LocateUs.btnClearAll.top = "150dp";    
    }
    for (var i = filterNum; i < 6; i++) {
      filterlblWidget="lblFilterName"+i;
      filterCloseBtn="flxCancelFilter"+i;
      this.view.LocateUs[filterlblWidget].setVisibility(false);
      this.view.LocateUs[filterCloseBtn].setVisibility(false);
    }
  },
  removeFilter :function(filterName){
    var scopeObj = this;
    var data = this.view.LocateUs.segViewsAndFilters.data;
    for (var i = 0; i < data.length; i++) {
      if(data[i].lblOption===filterName){
        scopeObj.SegRemoveFilter(i); 
        break;
      }
    }
    scopeObj.segSetFilters();
    scopeObj.filterSearchData();
  },

  //flow actions
  setFlowActions: function () {
    var scopeObj=this;
	this.view.LocateUs.flxSearchImage.onClick=function(){
      scopeObj.performSearch();      
    };
    this.view.LocateUs.flxCloseIcon.onTouchEnd=function(){
      scopeObj.view.LocateUs.tbxSearchBox.text="";
    };
    this.view.LocateUs.tbxSearchBox.onDone=function(){
      scopeObj.performSearch();      
    };
    this.view.LocateUs.imgMyLocation.onTouchEnd = function() {
    	scopeObj.gotoCurrentLocation();
    };
    this.view.LocateUs.segResults.onRowClick= function(obj){
      scopeObj.onRowClickSegment(obj.selecteditems[0]);
      scopeObj.showBranchDetails();
      scopeObj.changeResultsRowSkin();
    };

    this.view.LocateUs.flxViewAndFilters.onClick= function(){
      scopeObj.showViewsAndFilters();
      scopeObj.showMap();
      scopeObj.view.LocateUs.flxHideMap.setVisibility(true);
     // scopeObj.setViewsAndFilterSegmentData();
    };
    this.view.LocateUs.flxRedoSearch.onClick= function(){
      scopeObj.showRedoSearch(false);
    };
    this.view.LocateUs.flxViewsAndFiltersClose.onClick= function(){
      scopeObj.cancelFilters();
      scopeObj.showSearch();
      scopeObj.showMap();
      scopeObj.view.LocateUs.flxHideMap.setVisibility(false);
    };
    this.view.LocateUs.btnCancelFilters.onClick= function(){
      scopeObj.cancelFilters();
      scopeObj.view.LocateUs.flxHideMap.setVisibility(false);
    };
    this.view.LocateUs.btnApplyFilters.onClick= function(){
      scopeObj.applyFilters();
      scopeObj.view.LocateUs.flxHideMap.setVisibility(false);
    };
    this.view.LocateUs.imgCheckBox1.onClick= function(){
      scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox1);
    };
    this.view.LocateUs.imgCheckBox2.onClick= function(){
      scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox2);
    };
    this.view.LocateUs.imgCheckBox3.onClick= function(){
      scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox3);
    };
    this.view.LocateUs.imgCheckBox4.onClick= function(){
      scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox4);
    };
    this.view.LocateUs.imgCheckBox5.onClick= function(){
      scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox5);
    };
    this.view.LocateUs.flxRadioAtm.onTouchEnd= function(){
      scopeObj.selectedView="ATM";
      scopeObj.view.LocateUs.lblViewType.text=kony.i18n.getLocalizedString("i18n.locateus.view")+": "+kony.i18n.getLocalizedString("i18n.locateus.OnlyAtm");
      scopeObj.selectRadioButton(scopeObj.view.LocateUs.imgRadioButtonAtm);
    };
    this.view.LocateUs.flxRadioBranch.onTouchEnd= function(){
      scopeObj.selectedView="BRANCH";
      scopeObj.view.LocateUs.lblViewType.text=kony.i18n.getLocalizedString("i18n.locateus.view")+": "+kony.i18n.getLocalizedString("i18n.locateus.OnlyBranches");
      scopeObj.selectRadioButton(scopeObj.view.LocateUs.imgRadioButtonBranch);
    };
    this.view.LocateUs.flxRadioAll.onTouchEnd= function(){
      scopeObj.selectedView="ALL";
      scopeObj.view.LocateUs.lblViewType.text=kony.i18n.getLocalizedString("i18n.locateus.view")+": "+kony.i18n.getLocalizedString("i18n.locateus.Branches&Atms");
      scopeObj.selectRadioButton(scopeObj.view.LocateUs.imgRadioButtonAll);
    };
    this.view.LocateUs.btnBackToMap.onClick= function(){
      scopeObj.clearSelectedIndicator(); 
   	  scopeObj.showSearch();
      scopeObj.showMap();
      var locationData = {
        lat: scopeObj.globalMapLat, 
        lon: scopeObj.globalMapLong,
    	};
      scopeObj.view.LocateUs.mapLocateUs.navigateToLocation(locationData, false, false); 
      scopeObj.view.LocateUs.mapLocateUs.zoomLevel = 15;
      scopeObj.view.forceLayout();
    };
    this.view.LocateUs.btnProceed.onClick= function(){
      scopeObj.showDirections();
      scopeObj.showMap();
    };
    this.view.LocateUs.flxCancelFilter1.onClick = function() {
            scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName1.text);
        };
        this.view.LocateUs.flxCancelFilter2.onClick = function() {
            scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName2.text);
        };
        this.view.LocateUs.flxCancelFilter3.onClick = function() {
            scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName3.text);
        };
       this.view.LocateUs.flxCancelFilter4.onClick = function() {
            scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName4.text);
        };
       this.view.LocateUs.flxCancelFilter5.onClick = function() {
            scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName5.text);
        };
    this.view.LocateUs.flxBack.onClick= function(){
      scopeObj.showBranchDetails();
      scopeObj.showSearch();
    };
    this.view.LocateUs.btnBackToSearch.onClick= function(){
      scopeObj.showViewsAndFilters();
    };
    this.view.LocateUs.flxZoomIn.onClick= function(){
    //  scopeObj.showRedoSearch(true);
      var zoom=scopeObj.view.LocateUs.mapLocateUs.zoomLevel;
	  zoom++;
	  scopeObj.view.LocateUs.mapLocateUs.zoomLevel=zoom;
    };
    this.view.LocateUs.flxZoomOut.onClick= function(){
     // scopeObj.showRedoSearch(true);
      var zoom=scopeObj.view.LocateUs.mapLocateUs.zoomLevel;
      zoom--;
      scopeObj.view.LocateUs.mapLocateUs.zoomLevel=zoom;
    };
    this.view.LocateUs.flxShare.onClick= function(){
      scopeObj.showShareDirection();
    };
    this.view.LocateUs.btnClearAll.onClick= function(){
      scopeObj.searchTextClearAndRefresh();
      scopeObj.clearAll();
      scopeObj.segSetFilters();
      scopeObj.view.LocateUs.btnClearAll.setVisibility(false);

    };
    this.view.LocateUs.btnShare.onClick= function(){
      scopeObj.showShareDirection();
    };
    this.view.LocateUs.btnBackToDetails.onClick= function(){
      scopeObj.showBranchDetails();
      scopeObj.showSearch();
    };
    this.view.imgShareClose.onTouchEnd = function(){
      scopeObj.hideShareDirection();
    };
    this.view.btnShareCancel.onClick= function(){
      scopeObj.hideShareDirection();
    };
    this.view.btnShareSend.onClick= function(){
      var selKey=scopeObj.view.lbxSendMapTo.selectedKey;
      if(selKey!="key1"){
        kony.print("sharing directions to selected options");
      }
      else{
        kony.print("share button disabled");
      }
      scopeObj.hideShareDirection();
    };
    this.view.lbxSendMapTo.onSelection= function(){
      scopeObj.showTextbox();
    };
    this.view.LocateUs.mapLocateUs.onPinClick=scopeObj.pinCallback;

  },
  pinCallback:function(mapid,locationdata){
    var scopeObj=this;
    scopeObj.pinToSegment(locationdata);
  },
  segToggleCheckBox: function() {
    var index = this.view.LocateUs.segViewsAndFilters.selectedIndex;
    kony.print("index:" + index);
    var rowIndex = index[1];
    var data = this.view.LocateUs.segViewsAndFilters.data;    
    if (data[rowIndex].imgCheckBox.src === "unchecked_box.png") {
      data[rowIndex].imgCheckBox.src = "checked_box.png";
    } else {
      data[rowIndex].imgCheckBox.src = "unchecked_box.png";
    }
    this.view.LocateUs.segViewsAndFilters.setDataAt(data[rowIndex],rowIndex);
  },
  onRowClickSegment: function(obj) {
    var self = this;
    this.view.LocateUs.flxMap.isVisible = false;
    this.view.LocateUs.flxBranchDetails.isVisible = true;
    var params = {
        "type": "details",
        "placeID": obj.locationId
    };
    kony.olb.utils.showProgressBar(this.view);
    this.presenter.getAtmorBranchDetails(params);
  },
  getAtmOrBranchDetailsSuccessCallback : function(locationDetailsViewModel) {
  		this.view.LocateUs.imgBankImage.src = "bank_img_rounded.png";
	    this.view.LocateUs.lblDistanceAndTimeFromUser.text = "";
	    if(locationDetailsViewModel.addressLine2) {
	        this.view.LocateUs.lblAddressLine2.text = locationDetailsViewModel.addressLine2.trim();    
	    }
	    if(locationDetailsViewModel.addressLine1) {
	        this.view.LocateUs.lblBranchName2.text = locationDetailsViewModel.addressLine1.trim();
	        this.view.LocateUs.lblBranchName3.text = locationDetailsViewModel.addressLine1.trim();
	        this.view.LocateUs.lblAddressLine1.text = locationDetailsViewModel.addressLine1.trim();
	    }
	    this.processServices(locationDetailsViewModel.services);
	    if(locationDetailsViewModel.phoneNumber) {
	        this.view.LocateUs.lblPhoneNumber1.text = locationDetailsViewModel.phoneNumber.trim();    
	    } else {
	        this.view.LocateUs.lblPhoneNumber1.text = "N/A";
	    }
	    
	    this.view.LocateUs.lblPhoneNumber2.text = "";

	    if (locationDetailsViewModel.workingHours) {
          var workingHours = locationDetailsViewModel.workingHours.split("||");
	        var data = workingHours.map(function(dataItem) {
	            return {
	                "lblTimings": dataItem.replace(/\?/g, " to ")
	            };
	        })
	        this.view.LocateUs.segDayAndTime.widgetDataMap = {
	            "lblTimings": "lblTimings"
	        };
	        this.view.LocateUs.segDayAndTime.setData(data);
	    } else {
	        this.view.LocateUs.segDayAndTime.setData([]);
	    }
      kony.olb.utils.hideProgressBar(this.view);
	    this.view.forceLayout();
  },
  getAtmOrBranchDetailsErrorCallback : function() {
	this.view.LocateUs.imgBankImage.src = "bank_img_rounded.png";
    this.view.LocateUs.lblBranchName2.text = kony.i18n.getLocalizedString("i18n.locateus.detailsNA");
    this.view.LocateUs.lblDistanceAndTimeFromUser.text = "";
    this.view.LocateUs.lblAddressLine2.text="";
    this.view.LocateUs.lblAddressLine1.text="";
    this.view.LocateUs.lblPhoneNumber1.text="";
    this.view.LocateUs.lblPhoneNumber2.text ="";
    this.view.LocateUs.segDayAndTime.setData([]);
    this.view.LocateUs.lblService1.text ="";
    this.view.LocateUs.lblService2.text ="";
    this.view.LocateUs.lblService3.text = "";
    this.view.LocateUs.lblService4.text = "";
    this.view.LocateUs.lblBranchName3.text="";
    kony.olb.utils.hideProgressBar(this.view);
    this.view.forceLayout();
  },
  setViewsAndFilterSegmentData : function(){
    var scopeObj=this;
    var services=scopeObj.getServices();
    var dataMap = {
      "flximg":"flximg",
      "flxOption": "flxOption",
      "imgCheckBox": "imgCheckBox",
      "lblOption": "lblOption",
      "lblSepartor": "lblSepartor"
    };
    var data = services.map(function(item){
			return {
			"flximg": {
                "onClick": scopeObj.segToggleCheckBox,
            },
            "imgCheckBox": {
                "src": "unchecked_box.png",
            },
            "lblOption": item,
            "lblSepartor": "."
        }
	});
    this.view.LocateUs.segViewsAndFilters.widgetDataMap=dataMap;
    this.view.LocateUs.segViewsAndFilters.setData(data);
 	this.view.forceLayout();
    },
  setDirectionsSegmentData: function(){
    var dataMap={    
      "flxDirectionDetails": "flxDirectionDetails",
      "flxDirections": "flxDirections",
      "imgDirection": "imgDirection",
      "lblDirection": "lblDirection",
      "lblDistanceAndTime": "lblDistanceAndTime",
      "lblSeperator": "lblSeperator"
    };
    var data=[
      {    
        "imgDirection": "arrow_up_blue.png",
        "lblDirection": "Head southwest on S El Camino Real toward W 4th Ave Lorem Ipsum",
        "lblDistanceAndTime": "80 Feet | 20 Seconds",
        "lblSeperator": " ",
        "template":"flxDirections"
      },
      {    
        "imgDirection": "arrow_right_blue.png",
        "lblDirection": "Head southwest on S El Camino Real toward W 4th Ave Lorem Ipsum",
        "lblDistanceAndTime": "80 Feet | 20 Seconds",
        "lblSeperator": " ",
        "template":"flxDirections"
      },
      {    
        "imgDirection": "arrow_turn_blue.png",
        "lblDirection": "Head southwest on S El Camino Real toward W 4th Ave Lorem Ipsum",
        "lblDistanceAndTime": "80 Feet | 20 Seconds",
        "lblSeperator": " ",
        "template":"flxDirections"
      },
      {    
        "imgDirection": "arrow_right_blue.png",
        "lblDirection": "Head southwest on S El Camino Real toward W 4th Ave Lorem Ipsum",
        "lblDistanceAndTime": "80 Feet | 20 Seconds",
        "lblSeperator": " ",
        "template":"flxDirections"
      },
      {    
        "imgDirection": "arrow_up_blue.png",
        "lblDirection": "Head southwest on S El Camino Real toward W 4th Ave Lorem Ipsum",
        "lblDistanceAndTime": "80 Feet | 20 Seconds",
        "lblSeperator": " ",
        "template":"flxDirections"
      }
    ];
    this.view.LocateUs.segDirections.widgetDataMap=dataMap;
    this.view.LocateUs.segDirections.setData(data);
  },
  filterSearchData:function(){
    var scopeObj=this;
    var data=scopeObj.searchResultData;
    var filterData=scopeObj.filterDataByView(data);
    data=scopeObj.filterDataByServices(filterData);
    if(data){
      scopeObj.updateSearchSegmentResult(data);
    }
    else{
      scopeObj.noSearchResultUI();
    }
  },
  filterDataByView:function(data){
    var scopeObj=this;
    var view=scopeObj.selectedView;
    if(view=="ALL")
      return data;
    else if(view=="ATM"){
        return data.filter(function(row){
          if(row.type=="ATM")
            return true;
          else
            return false;
        });
    }
    else if(view=="BRANCH"){
      return data.filter(function(row){
        if(row.type=="BRANCH")
          return true;
        else
          return false;
      });
    }
  },
  filterDataByServices:function(data){
    var scopeObj=this;
    var filter=scopeObj.selectedServicesList;
    if(filter.length>0){
      //TO DO-filter data 
      //As of now no criteria for how to filter
      //if any of the filter is selected-Showing no data is found
      return null;
    }
    else
      return data;
  },
  noSearchResultUI:function(){
    this.showNoMapPins();
    this.view.LocateUs.flxSearch.setVisibility(true);
    this.view.LocateUs.flxDirections.setVisibility(false);
    this.view.LocateUs.flxViewsAndFilters.setVisibility(false);
    this.view.LocateUs.flxSearchBox.setVisibility(true);
    this.view.LocateUs.segResults.setVisibility(false);
    this.view.LocateUs.flxNoSearchResult.setVisibility(true);
    this.view.LocateUs.rtxNoSearchResults.text="<b>No search result found.</b><br>Please change the search criteria";
    this.view.forceLayout();
  },
  searchResultUI:function(){
    this.view.LocateUs.flxSearch.setVisibility(true);
    this.view.LocateUs.flxDirections.setVisibility(false);
    this.view.LocateUs.flxViewsAndFilters.setVisibility(false);
    this.view.LocateUs.flxSearchBox.setVisibility(true);
    this.view.LocateUs.segResults.setVisibility(true);
    this.view.LocateUs.flxNoSearchResult.setVisibility(false);
    this.view.forceLayout();
  },
  pinToSegment:function(dataItem){
    var scopeObj=this;
    var locationId=dataItem.locationId;
    var informationTitle=dataItem.informationTitle?dataItem.informationTitle:dataItem.lblName;
    var data = scopeObj.view.LocateUs.segResults.data;
    var index = data.findIndex(function(item, i){
      return (item.locationId === locationId && item.lblName ===informationTitle);
    }); 
    scopeObj.view.LocateUs.segResults.selectedIndex = [0,index];
    scopeObj.changeResultsRowSkin();
    scopeObj.view.forceLayout();   
  },
  searchData:function(searchText){
    var scopeObj=this;
	  var queryParams = {
      "query" : "atms or banks in " + searchText.trim()
    };
    scopeObj.presenter.getSearchBranchOrATMList(queryParams);
  },

  getSearchBranchOrATMListSuccessCallback : function(data) {
    if(data.length===0)
    {
      this.getSearchBranchOrATMListErrorCallback();
    }
    else{
      this.searchResultData = data;
      if(this.searchResultData[0].latitude && this.searchResultData[0].longitude) {
        this.globalMapLat=data[0].latitude;
        this.globalMapLong=data[0].longitude;
      }
      this.filterSearchData();
      kony.olb.utils.hideProgressBar(this.view);
    }
  },

  getSearchBranchOrATMListErrorCallback : function() {
  	this.noSearchResultUI();
    kony.olb.utils.hideProgressBar(this.view);
  },

  performSearch:function(){
    var scopeObj=this;
    var searchText=scopeObj.view.LocateUs.tbxSearchBox.text.trim();
      if(searchText !== null && searchText !== '' && searchText.length > 0){
        scopeObj.searchData(searchText);    			
      }
      else{
        scopeObj.searchTextClearAndRefresh();
        return;
      }
  },
  applyFilters:function(){
    var scopeObj=this;
    scopeObj.performSearch();
    scopeObj.segSetFilters();
    scopeObj.viewText=scopeObj.view.LocateUs.lblViewType.text;
    scopeObj.selectedViewOnApply=scopeObj.selectedRadioBtn();
    scopeObj.showViews(["flxSearch"]);
  },
 cancelFilters:function(){
    var scopeObj=this;
    scopeObj.setPreviousViewSelection();
   	scopeObj.setPreviousFilterSelection();     
 	scopeObj.searchResultUI();
    scopeObj.performSearch();
   	scopeObj.view.forceLayout();
 },
  searchTextClearAndRefresh:function(){
    var scopeObj=this;
    scopeObj.presenter.getBranchOrATMList();
    scopeObj.view.LocateUs.tbxSearchBox.text="";
 },
  setPreviousFilterSelection:function(){
    var scopeObj=this;
    scopeObj.setViewsAndFilterSegmentData();
    var data = scopeObj.view.LocateUs.segViewsAndFilters.data;
    for (var i = 0; i < data.length; i++) {
      data[i].imgCheckBox.src=scopeObj.prevData[i];
      data[i].flximg.onClick= scopeObj.segToggleCheckBox;
  	}
    scopeObj.view.LocateUs.segViewsAndFilters.setData(data);
    scopeObj.view.forceLayout();
  },
  setPreviousViewSelection:function(){
    var scopeObj=this;
    scopeObj.view.LocateUs.lblViewType.text=scopeObj.viewText;
    scopeObj.selectRadioButton(scopeObj.selectedViewOnApply);
  },
  selectedRadioBtn:function(){
    var scopeObj=this;
    if(scopeObj.view.LocateUs.imgRadioButtonAll.src=="radiobtn_active.png")
      return scopeObj.view.LocateUs.imgRadioButtonAll;
    if(scopeObj.view.LocateUs.imgRadioButtonBranch.src=="radiobtn_active.png")
      return scopeObj.view.LocateUs.imgRadioButtonBranch;
    if(scopeObj.view.LocateUs.imgRadioButtonAtm.src=="radiobtn_active.png")
      return scopeObj.view.LocateUs.imgRadioButtonAtm;
  },
  getServices:function(){
    return ["Drive Up ATM", "Surcharge Free ATM", "Deposit Taking ATM", "Co-Op Shared Branch", "Safe Deposit Box"];
  },
  clearSelectedIndicator:function(){
    var data = this.view.LocateUs.segResults.data;
    for (var i = 0; i < data.length; i++) {
      data[i].flxSearchResults.skin = "sknFlxffffffBorder0";
      data[i].imgIndicator.isVisible = false;
    }
    this.view.LocateUs.segResults.setData(data);
    this.view.forceLayout();
  },
}});