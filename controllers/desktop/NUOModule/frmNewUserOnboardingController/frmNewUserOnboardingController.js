define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities, OLBConstants) {
    return {
        //eslint-disable-next-line 
        willUpdateUI: function (nuoViewModel) {
            if (nuoViewModel.showProgressBar) {
                CommonUtilities.showProgressBar(this.view);
            } else if (nuoViewModel.hideProgressBar) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (nuoViewModel.serverError) {
                this.showServerError(nuoViewModel.serverError);
            }
            if (nuoViewModel.productSelection) {
                this.showProductSelection(nuoViewModel.productSelection);
            }
            if (nuoViewModel.createNewCustomer) {
                this.showEnterOTP();
            }
            if (nuoViewModel.customerExists) {
                this.showPhoneAlreadyOptedScreen(true);
            }
            if (nuoViewModel.continueApplication) {
                this.showPhoneAlreadyOptedScreen(false);
            }
            if (nuoViewModel.OTPValidated) {
                this.showEligibilityScreen();
            }
            if (nuoViewModel.NUOLanding) {
                this.showNUOEnterPhoneScreen();
            }
            if (nuoViewModel.showUserInformationStep) {
                this.showUserInformationStep();
            }
            if (nuoViewModel.showUserInformationViewModel) {
                this.showUserInformationForm(nuoViewModel.showUserInformationViewModel);
            }
            if (nuoViewModel.showEmploymentInformationViewModel) {
                this.showEmploymentInformationForm(nuoViewModel.showEmploymentInformationViewModel);
            }
            if(nuoViewModel.showFinancialInformationViewModel) {
                this.showFinancialInformationForm(nuoViewModel.showFinancialInformationViewModel);
            }
            if (nuoViewModel.continueOrResetApplication) {
                this.showOpenNewAccount(nuoViewModel.continueOrResetApplication);
            }
            if(nuoViewModel.showUploadDocuments){
                this.showUploadDocumentsScreen1(nuoViewModel.showUploadDocuments.employementStatus);
            }
            if(nuoViewModel.documentsSaved){
                this.getIdentityVerification();
            }
            if(nuoViewModel.identityVerificationQuestions){
                this.showIdentityVerification(nuoViewModel.identityVerificationQuestions);
            }
            if(nuoViewModel.identityVerificationSuccess){
                this.performCreditCheck();
            }
            if(nuoViewModel.identityVerificationError){
                this.showIdentityVerificationError();
            }
            if(nuoViewModel.userCreditCheckSuccess){
                this.showDigitalSignatureFlow();
            }
            if(nuoViewModel.userCreditCheckError){
                this.showCreditCheckErrorFlow();
            }
            if(nuoViewModel.uploadSignatureSuccess){
                this.showAcknowledgementScreen();
            }
            if(nuoViewModel.enrollError){
                this.showEnrollError(nuoViewModel.enrollError);
            }
            if(nuoViewModel.loginError){
                this.showLoginError(nuoViewModel.loginError);
            }
            if(nuoViewModel.uploadSignature){
                this.showUploadSignatureScreen(nuoViewModel.uploadSignature);
            }
            if(nuoViewModel.customerLoginError){
                this.showCustomerLoginError(nuoViewModel.customerLoginError);
            }
            if(nuoViewModel.usernamepasswordrules){
                this.showEnrollForm(nuoViewModel.usernamepasswordrules);
          }
        },

        /**
         * Handle server error, shows serverFlex
         * @member frmNewUserOnBoardingController
         * @param {object} serverError error
         * @returns {void} - None
         * @throws {void} - None
         */
        showServerError: function (serverError) {
            this.view.flxDowntimeWarning.setVisibility(true);
            this.view.rtxDowntimeWarning.setVisibility(true);
        },

        /**
         * Entry Point Method for Product Selection 
         * @member frmNewUserOnBoardingController
         * @param {object} viewModel 
         * @returns {void} - None
         * @throws {void} - None
         */
        showProductSelection: function (productSelectionViewModel) {
            this.hideAll();
            this.showProductSelectionUI();
            this.showTopMenu();
            if (productSelectionViewModel.isRevisiting) {
                this.view.flxWelcomeMessage.setVisibility(false);
            }
            else {
                this.view.flxWelcomeMessage.setVisibility(true);
                this.view.lblWelcome.text = kony.i18n.getLocalizedString("i18n.accounts.welcome") + " " + productSelectionViewModel.username + "!";                
            }
            this.setProductSelectionData(productSelectionViewModel.products, productSelectionViewModel.selectedProducts);
            this.validateProductSelection();
            this.view.btnContinueProceed.onClick = this.saveProducts.bind(this, productSelectionViewModel.products);
        },

        saveProducts: function (productList) {
            var selectedProductObjects = [];
            this.view.segProductsNUO.data.forEach(function (segementDataItem, index) {
                if (segementDataItem.lblCheckBox === "C") {
                    selectedProductObjects.push(productList[index])                    
                }
            })
           this.presenter.saveUserProducts(selectedProductObjects);
        },

        /**
         * Binds Products to Product Segment
         * @member frmNewUserOnBoardingController
         * @param {object} products model objects array 
         * @returns {void} - None
         * @throws {void} - None
         */
        setProductSelectionData: function (products, selectedProducts) {
            var selectedProductIds = selectedProducts.map(function (selectedProduct) {
                return selectedProduct.productId
            })
            var self = this;
            var widgetDataMap = {
                "imgflxAccounts": "imgflxAccounts",
                "lblAccountsName": "lblAccountsName",
                "lblFeature": "lblFeature",
                "flxRule1": "flxRule1",
                "lblRule1": "lblRule1",
                "flxRule2": "flxRule2",
                "imgRule1": "imgRule1",
                "imgRule2": "imgRule2",
                "imgRule3": "imgRule3",
                "lblRule2": "lblRule2",
                "flxRule3": "flxRule3",
                "lblRule3": "lblRule3",
                "lblMonthlyServiceFee": "lblMonthlyServiceFee",
                "lblCheckBox": "lblCheckBox",
                "lblAccounts": "lblAccounts",
                "btnKnowMore": "btnKnowMore",
                "flxCheckBox": "flxCheckBox"

            }
            this.view.segProductsNUO.widgetDataMap = widgetDataMap;
            var data = products.map(function (product, index) {
                return {
                    "imgflxAccounts": "nuo_savings.png",
                    "lblAccountsName": product.productName,
                    "lblFeature": kony.i18n.getLocalizedString("i18n.NUO.Features"),
                    "lblRule1": product.productDescription,
                    "lblRule2": product.features,
                    "lblRule3": product.info,
                    "imgRule1": "pageoffdot.png",
                    "imgRule2": "pageoffdot.png",
                    "imgRule3": "pageoffdot.png",
                    "lblMonthlyServiceFee": product.rates,
                    "btnKnowMore": {
                        "text": kony.i18n.getLocalizedString("i18n.NUO.KnowMore"),
                        "toolTip":kony.i18n.getLocalizedString("i18n.NUO.KnowMore"),
                        "onClick": self.showProductDetail.bind(this, product, index)
                    },
                    "lblAccounts": kony.i18n.getLocalizedString("i18n.NUO.SlectProduct"),
                    "lblCheckBox": selectedProductIds.indexOf(product.productId) > -1 ?  "C": "D",
                    "flxCheckBox": {
                        "onClick": self.toggleProductSelectionCheckbox.bind(this, index)
                    }
                }
            })
            this.view.segProductsNUO.setData(data);
            this.AdjustScreen();
        },
        /**
  *AdjustScreen- function to adjust the footer
  * @member of frmNewUserOnboardingController
  * @param {void} - None
  * @returns {void} - None
  * @throws {void} -None
  */
  AdjustScreen: function() {
    this.view.forceLayout();
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  },  

        /**
         * Product Selection UI 
         * @member frmNewUserOnBoardingController
         * @returns {void} - None
         * @throws {void} - None
         */
        showProductSelectionUI: function () {
            this.hideAll();
            this.view.flxMainContainer.setVisibility(true);
            this.view.flxAccounts.setVisibility(false);
            this.view.flxAccountsView.setVisibility(true);
            this.view.flxColorLine.width="14.28%";
            this.AdjustScreen();
        },

        /**
         * Toggle Select Product Checkbox of Select Products Segment
         * @member frmNewUserOnBoardingController
         * @param index index of row
         * @returns {void} - None
         * @throws {void} - None
         */
        toggleProductSelectionCheckbox: function (index) {
            var data = this.view.segProductsNUO.data[index];
            data.lblCheckBox = data.lblCheckBox === "C" ? "D" : "C";
            this.view.segProductsNUO.setDataAt(data, index);
            this.validateProductSelection();
        },
        /**
         *  Select Product Checkbox of Select Products Segment
         * @member frmNewUserOnBoardingController
         * @param index index of row
         * @returns {void} - None
         * @throws {void} - None
         */
        selectProductSelectionCheckbox: function (index) {
            var data = this.view.segProductsNUO.data[index];
            data.lblCheckBox = "C";
            this.view.segProductsNUO.setDataAt(data, index);
            this.validateProductSelection();
        },

        validateProductSelection: function () {
            var count = this.view.segProductsNUO.data.filter(function (item) {
                return item.lblCheckBox === "C";
            }).length;
            if (count === 0) {
                CommonUtilities.disableButton(this.view.btnContinueProceed);                
            }
            else {
                CommonUtilities.enableButton(this.view.btnContinueProceed);                
            }
        },


        /**
         * Show Product Detail
         * @member frmNewUserOnBoardingController
         * @param product NewUserProduct Model Object 
         * @param index of which row is shown in segment 
         * @returns {void} - None
         * @throws {void} - None
         */
        showProductDetail: function (product, index) {
            this.showProductDetailUI();
            var data = [product.productDescription,
            product.features,
            product.info,
            product.rates,
            product.features
            ];

            var widgetDataMap = {
                "lblRule1": "lblRule1",
                "imgRule1": "imgRule1"
            }

            var segData = data.map(function (text) {
                return {
                    "lblRule1": text,
                    "imgRule1": "pageoffdot.png"
                }
            });
            this.view.AccountsDescription.segFeatures.setData(segData);
            this.view.AccountsDescription.imgflxAccountsDescription.src = "nuo_savings.png";
            this.view.AccountsDescription.lblAccountName.text = product.productName;
            this.view.AccountsDescription.btnAccountsDescriptionCancel.onClick = function () {
                this.showProductSelectionUI();
            }.bind(this);
            this.view.AccountsDescription.btnAccountsDescriptionProceed.onClick = function () {
                this.selectProductSelectionCheckbox(index)
                this.showProductSelectionUI();
            }.bind(this);
            this.AdjustScreen();            
        },

        /**
         * Show Product Detail UI
         * @member frmNewUserOnBoardingController
         * @param product Product Model Object
         * @returns {void} - None
         * @throws {void} - None
         */
        showProductDetailUI: function (product) {
            this.view.flxMainContainer.setVisibility(false);
            this.view.flxAccounts.setVisibility(true);
        },
        /**
         * shows Eligibility Screen for the User and handles the action
         * @member frmNewUserOnboardingController
         * @param {} 
         * @returns {void} - None
         * @throws {void} - None
         */
        showEligibilityScreen: function () {
            var scopeObj = this;
            this.showEligibilityScreenUI();
            this.radioBtnAction(this.getEgligibilityRadioButtonFlexes(),  this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage1);
            this.view.ChooseEligibilityCriteria.tbxEnterTheCompany.text = "";
            this.view.ChooseEligibilityCriteria.btnSendOTPCancel.onClick = function () {
                scopeObj.navigateToLoginScreen();
            };
            this.view.ChooseEligibilityCriteria.tbxEnterTheCompany.onKeyUp = this.validateEgligibilityForm.bind(this);
            this.view.ChooseEligibilityCriteria.btnProceed.onClick = function () {
                scopeObj.presenter.showEnrollForm();
            }
            this.bindEgligibilityRadioButtonActions();
        },

        /**
         * Sets Eglibility Radio Button Actions
         * @returns {void} - None
         * @throws {void} - None
         */

        bindEgligibilityRadioButtonActions: function () {
            var scopeObj = this;
            this.getEgligibilityRadioButtonFlexes().forEach(function (radioFlex) {
                radioFlex.onClick = function () {
                    scopeObj.radioBtnAction(scopeObj.getEgligibilityRadioButtonFlexes(), radioFlex);
                    scopeObj.validateEgligibilityForm();
                }
            })
        },

        /**
         * Validates Egligibility Form 
         * @returns {void} - None
         * @throws {void} - None
         */

        validateEgligibilityForm: function () {
            var valid = false;
            this.getEgligibilityRadioButtonFlexes().forEach(function (radioFlex) {
                if (radioFlex.widgets()[0].text === "R") {
                    valid = true;
                }
            })
            var companyName = this.view.ChooseEligibilityCriteria.tbxEnterTheCompany.text;
            if (!valid) {
                CommonUtilities.disableButton(this.view.ChooseEligibilityCriteria.btnProceed);
            }
            else if (this.view.ChooseEligibilityCriteria.lblRadioBtn2.text === "R" && companyName === "") {
                CommonUtilities.disableButton(this.view.ChooseEligibilityCriteria.btnProceed);
            }
            else {
                CommonUtilities.enableButton(this.view.ChooseEligibilityCriteria.btnProceed);
            }
        },

        /**
         * Returns array of Radio Flexes of egligibility in order.
         * @returns {void} - None
         * @throws {void} - None
         */

        getEgligibilityRadioButtonFlexes: function () {
            return [
                this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage1,
                this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage2,
                this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage3,
                this.view.ChooseEligibilityCriteria.flxChooseEligibilityCriteriaImage4
            ];
        },

        /**
         * Shows the egligibility UI
         * @returns {void} - None
         * @throws {void} - None
         */

        showEligibilityScreenUI: function () {
            this.hideAll();
            this.view.flxChooseEligibilityCriteria.setVisibility(true);
            this.AdjustScreen();
        },

        /**
         * Entry point method of enroll form 
         * @returns {void} - None
         * @throws {void} - None
         */

        showEnrollForm: function (usernameAndPasswordRules) {
            var scopeObj = this;
            this.bindEnrollFieldActions();
            this.showUserNameRules(usernameAndPasswordRules);
            this.showPasswordRules(usernameAndPasswordRules);
            this.view.CreateUsernamePassword.btnCreateUsernamePasswordCancel.onClick = function () {
                scopeObj.navigateToLoginScreen();
            };

            this.view.CreateUsernamePassword.btnCreateUsernamePasswordProceed.onClick = this.enrollUser.bind(this);
            this.view.CreateUsernamePassword.tbxEnterUsername.onBeginEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxRulesUsername.setVisibility(true);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxEnterUsername.onEndEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxRulesUsername.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxEnterPassword.onBeginEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxPasswordUsername.setVisibility(true);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxEnterPassword.onEndEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxPasswordUsername.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxReEnterPassowrd.onBeginEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxPasswordUsername.setVisibility(true);
                scopeObj.view.forceLayout();
            };
            this.view.CreateUsernamePassword.tbxReEnterPassowrd.onEndEditing = function () {
                scopeObj.view.CreateUsernamePassword.flxPasswordUsername.setVisibility(false);
                scopeObj.view.forceLayout();
            };
            this.showEnrollFormUI();
            this.resetEnrollForm();
        },

        /**
         * Enroll the user - Checks validity and sends data to presentation controller
         * @returns {void} - None
         * @throws {void} - None
         */

        enrollUser: function () {
            if (this.isEnrollDataValid()) {
                this.presenter.enrollUser(this.getEnrollData());
            }
        },

        /**
         * Checks if enroll data is valid or not
         * @param {object} products model objects array 
         * @returns {boolean} - True if valid and false if not. 
         * @throws {void} - None
         */

        isEnrollDataValid: function() {
            var enrollData = this.getEnrollData();
            var scopeObj = this;
            function showError(text) {
                scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.text = text;
                scopeObj.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(true);
                scopeObj.view.forceLayout();
            }
            if (!this.isValidEmail(enrollData.email)) {
                showError("Not a valid email");
                return false;
            } else if (!this.isUsernameValid(enrollData.username)) {
                showError("Not a valid username");
                return false;
            } else if (enrollData.username === enrollData.email) {
                showError("Username cannot be same as Email");
                return false;
            } else if (enrollData.username === enrollData.password) {
                showError("Password cannot be same as username");
                return false;
            } else if (!this.isPasswordValid(enrollData.password)) {
                showError("Not a valid password");
                return false;
            } else if (enrollData.password !== this.view.CreateUsernamePassword.tbxReEnterPassowrd.text) {
                showError("Password doesn't Match. Please enter it again");
                return false;
            }
            this.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(false);
            return true;
        },

        showUserNameRules:function(data)
        {
            var rules = [];
            for (var i = 0; i < data.length; i++) {
                var policyName = data[i].policyName;
                if (CommonUtilities.substituteforIncludeMethod(policyName, "Username Policy for End Customers")) {
                    rules.push(data[i].policyDescription);
                }
            }
            if (rules.length >= 0) {
               this.view.CreateUsernamePassword.rtxRulesUsername.text = rules;
            }
        } ,  

         showPasswordRules:function(data){
            var rules = [];
            for (var i = 0; i < data.length; i++) {
                var policyName = data[i].policyName;
                if (CommonUtilities.substituteforIncludeMethod(policyName, "Password Policy for End Customers")) {
                    rules.push(data[i].policyDescription);
                }
            }
            if (rules.length >= 0) {
              this.view.CreateUsernamePassword.rtxRulesPassword.text = rules;
            }
      },     
        /**
         * Returns enroll data from fields
         * @returns {object} - Object contains data from fields
         * @throws {void} - None
         */

        getEnrollData: function () {
            return {
                username: this.view.CreateUsernamePassword.tbxEnterUsername.text,
                password: this.view.CreateUsernamePassword.tbxEnterPassword.text,
                phone: this.view.SendOTP.tbxOTP.text,
                email: this.view.CreateUsernamePassword.tbxNewEmailId.text
            }
        },

        /**
         * Binds Enroll field key up acion to validate function
         * @returns {void} - None
         * @throws {void} - None
         */


        bindEnrollFieldActions: function () {
            this.view.CreateUsernamePassword.tbxEnterUsername.onKeyUp = this.validateEnrollFields.bind(this);
            this.view.CreateUsernamePassword.tbxNewEmailId.onKeyUp = this.validateEnrollFields.bind(this);
            this.view.CreateUsernamePassword.tbxEnterPassword.onKeyUp = this.validateEnrollFields.bind(this);
            this.view.CreateUsernamePassword.tbxReEnterPassowrd.onKeyUp = this.validateEnrollFields.bind(this);
        },

        /**
         * Show enroll form UI
         * @returns {void} - None
         * @throws {void} - None
         */

        showEnrollFormUI: function () {
            this.hideAll();
            this.view.flxCreateUsernamePassowrd.setVisibility(true);
            this.view.CreateUsernamePassword.imgValidEmail.setVisibility(false);
            this.view.CreateUsernamePassword.imgUsername.setVisibility(false);
            this.view.CreateUsernamePassword.imgPassword.setVisibility(false);
            this.view.CreateUsernamePassword.imgReEnterPassword.setVisibility(false);
            this.view.forceLayout();
        },

        /**
         * Resets enroll form UI 
         * @returns {void} - None
         * @throws {void} - None
         */

        resetEnrollForm: function () {
            this.view.CreateUsernamePassword.lblUsernameDoesnotMatch.text = "";
            this.view.CreateUsernamePassword.tbxNewEmailId.text = "";
            this.view.CreateUsernamePassword.tbxEnterUsername.text = "";
            this.view.CreateUsernamePassword.tbxEnterPassword.text = "";
            this.view.CreateUsernamePassword.tbxReEnterPassowrd.text = "";
            this.disableButton(this.view.CreateUsernamePassword.btnCreateUsernamePasswordProceed);
        },

        /**
         * shows phone number required Screen
         * @param {} 
         * @returns {void} - None
         * @throws {void} - None
         */
        showNUOEnterPhoneScreen: function () {
            var scopeObj = this;
            this.view.SendOTP.tbxOTP.text = "";            
            this.showNUOEnterPhoneScreenUI();
            this.hideTopMenu();
            this.view.SendOTP.tbxOTP.onKeyUp = this.checkPhoneNumber.bind(this);
            this.view.SendOTP.btnSendOTPCancel.onClick = this.navigateToLoginScreen.bind(this);
            this.view.SendOTP.btnSendOTPProceed.onClick = function () {
                scopeObj.presenter.checkPhoneNumber(scopeObj.view.SendOTP.tbxOTP.text);
            };
        },

        /**
         * Show NUO Phone Screen UI 
         * @returns {void} - None
         * @throws {void} - None
         */

        showNUOEnterPhoneScreenUI: function () {
            this.hideAll();
            this.disableButton(this.view.EnterSecureAccessCode.btnSendOTPProceed);
            this.showViews(["flxSendOTP"]);
        },

        /**
         * Navigate to login screen
         * @returns {void} - None
         * @throws {void} - None
         */

        navigateToLoginScreen: function () {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.showLoginScreen();
        },

        isValidOTP: function (otpString) {
            return /^\d{6}$/.test(otpString);
        },

        /**
         * Callback for onKeyUp of any OTP
         * @returns {void} - None
         * @throws {void} - None
         */


        onKeyUpOtp: function (widget) {
            var scopeObj = this;
            if (this.isValidOTP(this.getOTP())) {
                this.enableButton(scopeObj.view.EnterSecureAccessCode.btnSendOTPProceed);
            }
            else {
                this.changeFocus(widget);
                this.disableButton(scopeObj.view.EnterSecureAccessCode.btnSendOTPProceed);
            }
        },

        /**
         * Change focus of OTP Field to next/previous OTP Field
         * @returns {void} - None
         * @throws {void} - None
         */

        changeFocus: function (currentField) {
            var fields = this.getOTPTextFields();
            if (currentField.text.length === 0) {
                var previousField = fields[fields.indexOf(currentField) - 1];
                if (previousField) {
                    previousField.setFocus();
                }
            } else {
                var nextField = fields[fields.indexOf(currentField) + 1];
                if (nextField) {
                    nextField.setFocus();
                }
            }
        },

        /**
         * Show Enter OTP Screen
         * @returns {void} - None
         * @throws {void} - None
         */

        showEnterOTP: function () {
            var scopeObj = this;
            scopeObj.showEnterOTPUI();
            scopeObj.resetOTP();
            this.view.EnterSecureAccessCode.btnResend.onClick = function () {
                scopeObj.presenter.requestOTP();
            }
            this.view.EnterSecureAccessCode.btnSendOTPProceed.onClick = function () {
                scopeObj.presenter.validateOTP(scopeObj.getOTP());
            };
            this.view.EnterSecureAccessCode.btnSendOTPCancel.onClick = scopeObj.navigateToLoginScreen.bind(scopeObj);

            this.getOTPTextFields().forEach(function (otpField) {
                otpField.onKeyUp = scopeObj.onKeyUpOtp.bind(this);
            })

        },

        /**
         * Returns the arary of text fields of enter OTP
         * @returns {array} - Array of text fields of OTP Screen
         * @throws {void} - None
         */

        getOTPTextFields: function () {
            return [this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit1,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit2,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit3,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit4,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit5,
            this.view.EnterSecureAccessCode.tbxEnterSecureAccessCodeDigit6]
        },

        /**
         * Return OTP value from all text fields
         * @param {} 
         * @returns {void} - None
         * @throws {void} - None
         */

        getOTP: function () {
            var fields = this.getOTPTextFields();
            return fields.reduce(function (prevValue, currentValue, index) {
                return prevValue + fields[index].text;
            }, "");
        },

        /**
         * Resets OTP
         * @returns {void} - None
         * @throws {void} - None
         */

        resetOTP: function () {
            this.getOTPTextFields().forEach(function (otpField) {
                otpField.text = "";
            })
            this.disableButton(this.view.EnterSecureAccessCode.btnSendOTPProceed);
        },

        /**
         * shows Enter OTP UI
         * @member frmNewUserOnboardingController
         * @returns {void} - None
         * @throws {void} - None
         */

        showEnterOTPUI: function () {
            this.hideAll();
            this.view.flxEnterSecureAccessCode.setVisibility(true);
            this.AdjustScreen();
        },

        /**
         * Check Phone Number on send otp screen
         * @returns {void} - None
         * @throws {void} - None
         */

        isPhoneNumberValid: function (phoneNumber) {
            if (phoneNumber === "" || phoneNumber === null || phoneNumber === undefined) {
              return false;                
            }
            else {
              return /^\d{10}$/.test(phoneNumber) 
            }
          },

        checkPhoneNumber: function () {
            var enteredPhoneNumber = this.view.SendOTP.tbxOTP.text;
            if (this.isPhoneNumberValid(enteredPhoneNumber)) {
                this.enableButton(this.view.SendOTP.btnSendOTPProceed);
            }
            else {
                this.disableButton(this.view.SendOTP.btnSendOTPProceed);
            }
        },

        /**
         * shows requests OTP from back end
         * @member frmNewUserOnboardingController
         * @param {} 
         * @returns {void} - None
         * @throws {void} - None
         */
        requestOTP: function () {
            this.presenter.requestOTP();
        },

        /**
         * shows Enter OTP Screen
         * @member frmNewUserOnboardingController
         * @param {} 
         * @returns {void} - None
         * @throws {void} - None
         */
        showPhoneAlreadyOptedScreen: function (isEnrolled) {
            var scopeObj = this;
            this.hideAll();
            this.view.flxCreateAccountUponAccountFound.setVisibility(true);
            this.AdjustScreen();
            this.view.CreateAccountUponAccountFound.btnCreateAccountEditPhoneNumber.onClick = function () {
                scopeObj.showNUOEnterPhoneScreenUI();
            };
            if (isEnrolled) {
                this.view.CreateAccountUponAccountFound.btnCreateAccountLogin.onClick = this.navigateToLoginScreen.bind(this);
            }
            else {
                this.view.CreateAccountUponAccountFound.btnCreateAccountLogin.onClick = this.showNUOLogin.bind(this);
            }
        },


        /**
         * shows NUO login
         * @member frmNewUserOnboardingController
         * @returns {void} - None
         * @throws {void} - None
         */

        showNUOLogin: function () {
           var self = this;
           this.showNUOLoginUI();
           this.resetNUOLoginForm();
           this.view.EnterUsernamePassword.tbxNewUsername.onKeyUp = this.checkNUOLoginForm.bind(this);
           this.view.EnterUsernamePassword.tbxEnterpassword.onKeyUp = this.checkNUOLoginForm.bind(this);
           this.view.EnterUsernamePassword.btnOpenNewAccountCancel.onClick = this.navigateToLoginScreen.bind(this);           
           this.view.EnterUsernamePassword.btnOpenNewAccountProceed.onClick = function () {
			   self.presenter.loginAndContinueApplication(self.getNUOLoginFormData());
           } 
        },


        getNUOLoginFormData: function () {
            return {
                username: this.view.EnterUsernamePassword.tbxNewUsername.text.trim(),
                password: this.view.EnterUsernamePassword.tbxEnterpassword.text
            }
        },

        resetNUOLoginForm: function () {
           this.view.EnterUsernamePassword.tbxNewUsername.text = "";
           this.view.EnterUsernamePassword.tbxEnterpassword.text = "";
           this.view.EnterUsernamePassword.lblError.setVisibility(false);
        },

        checkNUOLoginForm: function () {
            var loginFormData = this.getNUOLoginFormData();
            if (loginFormData.username.length > 0 && loginFormData.password.length > 0) {
                CommonUtilities.enableButton(this.view.EnterUsernamePassword.btnOpenNewAccountProceed);
            }
            else {
                CommonUtilities.disableButton(this.view.EnterUsernamePassword.btnOpenNewAccountProceed);
            }
        },

        showNUOLoginUI: function () {
            this.hideAll();
            this.view.flxEnterUsernamePassword.setVisibility(true);
            this.AdjustScreen();
        },
        /**
         * shows Enter username/password Screen
         * @member frmNewUserOnboardingController
         * @param {} 
         * @returns {void} - None
         * @throws {void} - None
         */
        showEnterUsernamePassword: function () {
            var scopeObj = this;
            this.view.EnterUsernamePassword.tbxNewUsername.text = "";
            this.view.EnterUsernamePassword.tbxEnterpassword.text = "";
            this.disableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
            this.showViews(["flxEnterUsernamePassword"]);
            this.view.EnterUsernamePassword.tbxNewUsername.onKeyUp = function () {
                if (scopeObj.view.EnterUsernamePassword.tbxNewUsername.text.length > 0 && scopeObj.view.EnterUsernamePassword.tbxEnterpassword.text.length > 0) {
                    scopeObj.enableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
                }
                else {
                    scopeObj.disableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
                }
            }
            this.view.EnterUsernamePassword.tbxEnterpassword.onKeyUp = function () {
                if (scopeObj.view.EnterUsernamePassword.tbxNewUsername.text.length > 0 && scopeObj.view.EnterUsernamePassword.tbxEnterpassword.text.length > 0) {
                    scopeObj.enableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
                }
                else {
                    scopeObj.disableButton(scopeObj.view.EnterUsernamePassword.btnOpenNewAccountProceed);
                }
            }
            this.view.EnterUsernamePassword.btnOpenNewAccountCancel.onClick = function () {
                scopeObj.showViews(["flxCreateAccountUponAccountFound"]);
            }
            this.view.EnterUsernamePassword.btnOpenNewAccountProceed.onClick = function () {
                username = scopeObj.view.EnterUsernamePassword.tbxNewUsername.text;
                password = scopeObj.view.EnterUsernamePassword.tbxEnterpassword.text;
                usernamePasswordJSON = {
                    username: username,
                    password: password
                };
                scopeObj.presenter.doLogin(usernamePasswordJSON);
            }
        },
        /**
   * Method to check whether the username is valid or not
   * @member of frmNewUserOnboardingController
   * @param {String} enteredUserName - username entered by the User
   * @returns {void} - None
   * @throws {void} -None
   */
        isUsernameValid: function (enteredUserName) {
            var userName = /^[a-zA-Z0-9.]+$/;
            if ((enteredUserName.match(userName)) && (enteredUserName.length > 7) && (enteredUserName.length < 25)) {
                if (this.checkDot(enteredUserName))
                    return true;
            }
            return false;
        },
        /**
    * Method to Check Dots in Username Entered by the User
    * @member of frmNewUserOnboardingController
    * @param {String} enteredUserName - username entered by the User
    * @returns {void} - None
    * @throws {void} -None
    */
        checkDot: function (enteredUserName) {
            var count = 0;
            if (enteredUserName.charAt(0) === "." || enteredUserName.charAt(enteredUserName.length - 1) === ".") return false;
            var index = enteredUserName.indexOf(".");
            for (var i = index + 1; i < enteredUserName.length - 1; i++) {
                if (enteredUserName[i] === ".") {
                    return false;
                }
            }
            return true;
        },
        /**
    * Method to Check Dots in Username Entered by the User
    * @member of frmNewUserOnboardingController
    * @param {String} enteredUserName - username entered by the User
    * @returns {void} - None
    * @throws {void} -None
    */
        isPasswordValid: function (enteredPassword) {
            var password = /^(?=.*\d)(?=.*[a-zA-Z])(?=.+[\!\#\$\%\(\*\)\+\,\-\;\=\?\@\[\\\]\^\_\'\{\}]).{8,20}$/;
            if (enteredPassword.match(password) && !this.hasConsecutiveDigits(enteredPassword)) {
                return true;
            }
            return false;
        },
        /**
        * Method to Check whether the entered text has consecutive digits or not
        * @member of frmNewUserOnboardingController
        * @param {Int} input - field entered by the User
        * @returns {void} - None
        * @throws {void} -None
        */
        hasConsecutiveDigits: function (input) {
            var i;
            var count = 0;
            for (i = 0; i < input.length; i++) {
                // alert(abc[i]);
                if (input[i] >= 0 && input[i] <= 9)
                    count++;
                else
                    count = 0;
                if (count === 9)
                    return true;
            }
            return false;
        },
        /**
      * Method to validate email by the User
      * @member of frmNewUserOnboardingController
      * @param {String} email - email entered by the User
      * @returns {void} - None
      * @throws {void} -None
      */
        isValidEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        /**
      * Method to validate fields entered by the User
      * @member of frmNewUserOnboardingController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} -None
      */
        validateEnrollFields: function () {

            function isUsernameAndEmailNotSame (enrollData) {
                return (enrollData.username.length > 0 && enrollData.email.length > 0 && (enrollData.username !== enrollData.email));
            }

            var scopeObj = this;
            var enrollData = this.getEnrollData();
            if (enrollData.email.length < 1 || enrollData.username.length < 1 || enrollData.password.length < 1 || scopeObj.view.CreateUsernamePassword.tbxReEnterPassowrd.text.length < 1) {
                scopeObj.disableButton(scopeObj.view.CreateUsernamePassword.btnCreateUsernamePasswordProceed);
            }
            else {
                scopeObj.enableButton(scopeObj.view.CreateUsernamePassword.btnCreateUsernamePasswordProceed);
            }
            if (this.isValidEmail(enrollData.email) && isUsernameAndEmailNotSame(enrollData)) {
                this.view.CreateUsernamePassword.imgValidEmail.setVisibility(true);
            } else {
                this.view.CreateUsernamePassword.imgValidEmail.setVisibility(false); 
            }
            if (this.isUsernameValid(enrollData.username) &&  isUsernameAndEmailNotSame(enrollData)) {
                this.view.CreateUsernamePassword.imgUsername.setVisibility(true);
            } else {
                this.view.CreateUsernamePassword.imgUsername.setVisibility(false);
            }
            if (this.isPasswordValid(enrollData.password) && enrollData.username !== enrollData.password) {
                this.view.CreateUsernamePassword.imgPassword.setVisibility(true);  
            } else {
                this.view.CreateUsernamePassword.imgPassword.setVisibility(false);   
            }
            if (this.isPasswordValid(enrollData.password) && enrollData.password === this.view.CreateUsernamePassword.tbxReEnterPassowrd.text) {
                this.view.CreateUsernamePassword.imgReEnterPassword.setVisibility(true);
            } else {
                this.view.CreateUsernamePassword.imgReEnterPassword.setVisibility(false);
            }
            this.view.CreateUsernamePassword.forceLayout();
           
        },
        /**
         * Show First page of the User information Step[Step 3]
         * @member frmNewUserOnBoardingController
         * @param {void} - None
         * @returns {void} - None
         * @throws {void} - None
         */
        showUserInformationStep: function () {
            var self = this;
            self.hideAll();
            self.view.UploadDocPhoneOrComputer.flxColorLine.width="28.57%";
            self.view.flxUploadDocFromPhoneOrComputer.setVisibility(true);
            self.view.UploadDocPhoneOrComputer.flxProofs.setVisibility(false);
            self.view.UploadDocPhoneOrComputer.lblUserInformation.text=kony.i18n.getLocalizedString("i18n.NUO.UserInformation");
            self.view.UploadDocPhoneOrComputer.lblUserInformationMessage1.text=kony.i18n.getLocalizedString("i18n.NUO.UserInformationMessage1");
            self.view.UploadDocPhoneOrComputer.lblUserInformationMessage2.text=kony.i18n.getLocalizedString("i18n.NUO.UserInformationMessage2");
            self.view.UploadDocPhoneOrComputer.btnBack.onClick = function () {
                self.presenter.navigateToProductSelection({isRevisiting: true});
            };
            self.view.UploadDocPhoneOrComputer.btnUserInformationSaveClose.onClick = self.ShowSaveClosePopup.bind(self);
            self.view.SaveAndClose.btnYes.onClick = function () {
                self.presenter.doLogout();
            }
            self.view.UploadDocPhoneOrComputer.btnUserInformationProceed.onClick = function () {
                if (self.view.UploadDocPhoneOrComputer.lblRadioBtn2.text === "R") {
                    self.showLinkSentToPhoneForm();
                } else if (self.view.UploadDocPhoneOrComputer.lblRadioBtn1.text === "R") {
                    self.showUserInformationForm();
                }
            };
            self.view.UploadDocPhoneOrComputer.flxUserInformationRadiobtn1.onClick = self.radioBtnAction.bind(self, self.getUploadDocRadioWidgetsArray());
            self.view.UploadDocPhoneOrComputer.flxUserInformationRadioBtn2.onClick = self.radioBtnAction.bind(self, self.getUploadDocRadioWidgetsArray());
            self.AdjustScreen();
        },
        /**
         * Get the Radio Button Widges array for Upload Doc From Computer or Phone flex
         * @param {void} - None
         * @returns {void} - None
         * @throws {void} - None
         */
        getUploadDocRadioWidgetsArray: function () {
            return [this.view.UploadDocPhoneOrComputer.flxUserInformationRadiobtn1, this.view.UploadDocPhoneOrComputer.flxUserInformationRadioBtn2];
        },
        
        /**
         * Get the Radio Button Widges array for Gender in UserInformation flex
         * @param {void} - None
         * @returns {void} - None
         * @throws {void} - None
         */
        getGenderRadioWidgetsArray: function () {
            return [this.view.UserInformationForm.flxRadioBtnMale, this.view.UserInformationForm.flxRadioBtnFemale];
        },

        /**
         * Show the form when user selects to upload data from mobile [Step 3]
         * @member frmNewUserOnBoardingController
         * @param {void} - None
         * @returns {void} - None
         * @throws {void} - None
         */
        showLinkSentToPhoneForm: function () {
            var self = this;
            this.hideAll();
            this.view.flxIdentityVerification.setVisibility(true);
            this.view.IdentityVerification.flxIdentityVerificationButtons.setVisibility(true);
            this.view.IdentityVerification.lblIdentityVerification.text = kony.i18n.getLocalizedString("i18n.NUO.UploadFromPhone");
            this.view.IdentityVerification.lblMessageHeading.text = kony.i18n.getLocalizedString("i18n.NUO.LinkSentToPhone");
            this.view.IdentityVerification.lblMessage.text = kony.i18n.getLocalizedString("i18n.NUO.ClickTheLinkToContinue");
            this.view.IdentityVerification.btnAction.text = kony.i18n.getLocalizedString("i18n.NUO.RESEND");
            this.view.IdentityVerification.btnAction.toolTip = kony.i18n.getLocalizedString("i18n.NUO.RESEND");
            this.view.IdentityVerification.btnAction.onClick = function(){
                self.showLoadingIndicator();
            };
            this.view.IdentityVerification.imgflxIdentityVerification.src = "nuo_signature_mobile.png";
            this.view.IdentityVerification.btnDONE.onClick = function () {
                self.presenter.doLogout();
            }
        },
    showLoadingIndicator:function(){
            var self=this;
            CommonUtilities.showProgressBar(self.view);
                function hideProgressBar() 
                    {
                        CommonUtilities.hideProgressBar(self.view);
                    }
            kony.timer.schedule("timer5",hideProgressBar, 3, false);
        },
        getUserPersonalInformationFromUI: function () {
            var userInfo = {
                "userfirstname": this.view.UserInformationForm.tbxFirstName.text,
                "userlastname": this.view.UserInformationForm.tbxLastName.text,
                "dateOfBirth": CommonUtilities.sendDateToBackend(this.view.UserInformationForm.calDOB.date, CommonUtilities.getConfiguration('frontendDateFormat')),
                "gender": this.view.UserInformationForm.lblRadioBtnMale.text === 'R' ? 'Male' : 'Female',
                "addressLine1": this.view.UserInformationForm.tbxAddressLine1.text,
                "addressLine2": this.view.UserInformationForm.tbxAddressLine2.text,
                "country": this.view.UserInformationForm.tbxCountry.text,
                "state": this.view.UserInformationForm.tbxState.text,
                "city": this.view.UserInformationForm.tbxCity.text,
                "zipcode": this.view.UserInformationForm.tbxZipcode.text,
                "maritalStatus": this.view.UserInformationForm.lbxMaritalStatus.selectedKeyValue[0],
              	"noOfDependents": this.view.UserInformationForm.lbxDependents.selectedKeyValue[0],
                "ssn": this.view.UserInformationForm.tbxEnterSSN.text,
                "informationType": "PersonalInfo"
            };
            if(userInfo.maritalStatus === "Married") {
                userInfo.spouseFirstName = this.view.UserInformationForm.tbxSpouseName.text;
                userInfo.spouseLastName = this.view.UserInformationForm.tbxSpouseLastName.text;
            }
            return userInfo;
        },
        showOrHideSpouseDetails : function () {
            if(this.view.UserInformationForm.lbxMaritalStatus.selectedKey === "Married") {
                this.view.UserInformationForm.flxUserInfoRow2.setVisibility(true);
            } else {
                this.view.UserInformationForm.flxUserInfoRow2.setVisibility(false);
            }
            this.AdjustScreen();
        },
        /**
         * Show Second page of the User information Step[Step 3 - Screen 2]
         * @member frmNewUserOnBoardingController
         * @param {viewModel} - it is an optional parameter, if provided it contains the data to be binded in the form
         * @returns {void} - None
         * @throws {void} - None
         */
        showUserInformationForm: function (viewModel) {
            var self = this;
            self.hideAll();
            self.view.UserInformationForm.flxColorLine.width="28.57%";
            self.view.flxUserInformationForm.setVisibility(true);
            self.view.UserInformationForm.btnBack.onClick = function () {
                self.showUserInformationStep();
            };
            self.view.UserInformationForm.calDOB.dateComponents = self.view.UserInformationForm.calDOB.dateComponents;
            self.view.UserInformationForm.tbxFirstName.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxLastName.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxAddressLine1.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxZipcode.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxCountry.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxState.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxCity.onKeyUp = self.validateUserForm.bind(self);
            self.view.UserInformationForm.tbxEnterSSN.onKeyUp = self.validateUserForm.bind(self);
          	self.view.UserInformationForm.calDOB.skin = "sknCalTransactions";
            self.view.UserInformationForm.calDOB.onSelection = function() {
              self.view.UserInformationForm.calDOB.skin = "sknCalTransactions";
              self.validateUserForm();
            } ;
            self.view.UserInformationForm.lbxMaritalStatus.onSelection = self.showOrHideSpouseDetails.bind(self);
            self.view.UserInformationForm.flxRadioBtnMale.onClick = self.radioBtnAction.bind(self, self.getGenderRadioWidgetsArray());
            self.view.UserInformationForm.flxRadioBtnFemale.onClick = self.radioBtnAction.bind(self, self.getGenderRadioWidgetsArray());
            self.view.UserInformationForm.tbxEnterSSN.skin = "sknTbxLatoffffff15PxBorder727272opa20";
            self.view.UserInformationForm.tbxEnterSSN.onBeginEditing = function() {
                self.view.UserInformationForm.tbxEnterSSN.skin = "sknTbxLatoffffff15PxBorder727272opa20";
            };
            //Terms and Conditions in user personal information and popup
            self.view.UserInformationForm.flxTCContentsCheckbox.onClick = function () {
                CommonUtilities.toggleCheckBox(self.view.UserInformationForm.imgTCContentsCheckbox);
                CommonUtilities.toggleCheckBox(self.view.imgTCContentsCheckbox);
                self.validateUserForm();
            };
            self.view.UserInformationForm.btnTermsAndConditions.onClick = function () {
                if (self.view.UserInformationForm.imgTCContentsCheckbox.src === "unchecked_box.png") {
                    self.view.imgTCContentsCheckbox.src = "unchecked_box.png";
                } else {
                    self.view.imgTCContentsCheckbox.src = "checked_box.png";
                }
                self.view.flxTermsAndConditions.setVisibility(true);
                var height = self.SetPopUpHeight();
                self.view.flxTermsAndConditions.height = height + "dp";
                self.view.flxTermsAndConditions.setFocus(true);
            };
            self.view.flxTCContentsCheckbox.onClick = function () {
                CommonUtilities.toggleCheckBox(self.view.imgTCContentsCheckbox);
            };
            self.view.btnSave.onClick = function () {
                if (self.view.imgTCContentsCheckbox.src === "unchecked_box.png") {
                    self.view.UserInformationForm.imgTCContentsCheckbox.src = "unchecked_box.png";
                } else {
                    self.view.UserInformationForm.imgTCContentsCheckbox.src = "checked_box.png";
                }
                self.validateUserForm();
                self.hideTermsAndConditionsFlex();
            };
            self.view.btnCancel.onClick = self.hideTermsAndConditionsFlex.bind(self);
            self.view.flxClose.onClick = self.hideTermsAndConditionsFlex.bind(self);
            self.view.UserInformationForm.btnUserInformationFormProceed.onClick = function () {
                self.validateDOBAndSSNAndProceed({ "showEmploymentInformationViewModel": "showEmploymentInformationViewModel" });
            };
            self.view.UserInformationForm.btnUserInformationFormSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.SaveAndClose.btnYes.onClick = function () {
                self.view.flxSaveClose.setVisibility(false);
                self.validateDOBAndSSNAndProceed({ "action": "saveAndClose" });
            };
            if (viewModel && viewModel.data !== undefined) {
                var data = viewModel.data;
                self.view.UserInformationForm.tbxFirstName.text = data.userfirstname;
                self.view.UserInformationForm.tbxLastName.text = data.userlastname;
                self.view.UserInformationForm.calDOB.dateFormat = CommonUtilities.getConfiguration('frontendDateFormat');
                self.view.UserInformationForm.calDOB.date = CommonUtilities.getFrontendDateString(data.dateOfBirth, CommonUtilities.getConfiguration('frontendDateFormat'));
                self.view.UserInformationForm.lblRadioBtnMale.text = data.gender === 'Male' ? 'R' : "";
                self.view.UserInformationForm.lblFemale.text = data.gender === 'Female' ? 'R' : "";
                self.view.UserInformationForm.tbxAddressLine1.text = data.addressLine1;
                self.view.UserInformationForm.tbxAddressLine2.text = data.addressLine2;
                self.view.UserInformationForm.tbxCountry.text = data.country;
                self.view.UserInformationForm.tbxState.text = data.state;
                self.view.UserInformationForm.tbxCity.text = data.city;
                self.view.UserInformationForm.tbxZipcode.text = data.zipcode;
                self.view.UserInformationForm.lbxMaritalStatus.selectedKey = data.maritalStatus;
                if(data.maritalStatus === "Married") {
                    self.view.UserInformationForm.tbxSpouseName.text = data.spouseFirstName;
                    self.view.UserInformationForm.tbxSpouseLastName.text = data.spouseLastName;
                } else {
                    self.view.UserInformationForm.tbxSpouseName.text = "";
                    self.view.UserInformationForm.tbxSpouseLastName.text = "";
                }
                self.view.UserInformationForm.lbxDependents.selectedKey = data.noOfDependents;
                self.view.UserInformationForm.tbxEnterSSN.text = data.ssn;
                self.view.UserInformationForm.imgTCContentsCheckbox.src = "checked_box.png";
            }
            self.validateUserForm();
            self.showOrHideSpouseDetails();
            self.AdjustScreen();
        },

        /**
         * Hide Terms and conditions Flex
         * @member frmNewUserOnBoardingController
         * @param {void} - None
         * @returns {void} - None
         * @throws {void} - None
         */
        hideTermsAndConditionsFlex: function () {
            this.view.flxTermsAndConditions.setVisibility(false);
        },

        /**
         * Show Employment Information Step[Step 4 -- Screen 1]
         * @member frmNewUserOnBoardingController
         * @param {viewModel} - it is an optional parameter, if provided it contains the data to be binded in the form
         * @returns {void} - None
         * @throws {void} - None
         */
        showEmploymentInformationForm: function (viewModel) {
            var self = this;
            self.hideAll();
            this.view.EmploymentInformation.flxColorLine.width="42.85%";
            this.view.flxEmploymentInformation.setVisibility(true);
            this.view.EmploymentInformation.lbxStatus.onSelection = function () {
                self.validateEmploymentForm();
                self.showOrHideCompanyNameAndJobTitle();
            };
            this.view.EmploymentInformation.tbxCompanyName.onKeyUp = self.validateEmploymentForm.bind(self);
            this.view.EmploymentInformation.tbxJobTitle.onKeyUp = self.validateEmploymentForm.bind(self);
            this.view.EmploymentInformation.btnBack.onClick = function () {
                self.presenter.getUserInformation("showUserInformationViewModel");
            };
            this.view.EmploymentInformation.btnEmploymentInfoProceed.onClick = function () {
                var employementInfo = self.getEmploymentInformationfromUI();
                self.presenter.createUserInformation(employementInfo);
            };
            this.view.EmploymentInformation.btnEmploymentInfoSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.SaveAndClose.btnYes.onClick = function () {
                var employementInfo = self.getEmploymentInformationfromUI();
                self.presenter.createUserInformation(employementInfo, { "action": "saveAndClose" });
            };
            if (viewModel && viewModel.data !== undefined) {
                var data = viewModel.data;
                this.view.EmploymentInformation.lbxStatus.selectedKey = data.employmentInfo;
                this.view.EmploymentInformation.tbxCompanyName.text = data.company;
                this.view.EmploymentInformation.tbxJobTitle.text = data.jobProfile;
                this.view.EmploymentInformation.lbxYearsOfExperience.selectedKey = data.experience;
                this.showOrHideCompanyNameAndJobTitle();
            }
            self.validateEmploymentForm();
            self.AdjustScreen();
        },
        showOrHideCompanyNameAndJobTitle : function() {
            if(this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Employed" ||
                this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Retired") {
                this.view.EmploymentInformation.flxRow2.setVisibility(true);
            } else {
                this.view.EmploymentInformation.flxRow2.setVisibility(false);
            }            
        },
        getEmploymentInformationfromUI: function () {
            var employmentInfo = {
                "employmentInfo": this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0],
              	"experience": this.view.EmploymentInformation.lbxYearsOfExperience.selectedKeyValue[0],
                "informationType": "EmploymentInfo"
            };
            if(this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Employed" ||
            this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Retired") {
                employmentInfo.company = this.view.EmploymentInformation.tbxCompanyName.text;
                employmentInfo.jobProfile = this.view.EmploymentInformation.tbxJobTitle.text;
            } else {
                employmentInfo.company = "";
                employmentInfo.jobProfile = "";
            }
            return employmentInfo;
        },

        getFinancialInformationFromUI: function () {
            var financialInfo = {
                "annualIncome": this.view.FinancialInformation.lbxAnnualGrossIncome.selectedKeyValue[0],
                "assets": this.view.FinancialInformation.lbxValuOfAssetsYouOwn.selectedKeyValue[0],
                "montlyExpenditure": this.view.FinancialInformation.lbxMonthlyExpenditure.selectedKeyValue[0],
                "informationType": "FinancialInfo"
            };
            return financialInfo;
        },

        /**
         * Show Financial Information Step[Step 5 -- Screen 1]
         * @member frmNewUserOnBoardingController
         * @param {viewModel} - it is an optional parameter, if provided it contains the data to be binded in the form
         * @returns {void} - None
         * @throws {void} - None
         */
        showFinancialInformationForm: function (viewModel) {
            var self = this;
            self.hideAll();
            this.view.FinancialInformation.flxColorLine.width="57.14%";
            this.view.flxFinancialInformation.setVisibility(true);
            this.view.FinancialInformation.btnBack.onClick = function () {
                self.presenter.getUserInformation("showEmploymentInformationViewModel");
            };
            this.view.FinancialInformation.btnFinancialInfoSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.SaveAndClose.btnYes.onClick = function () {
                var financialInfo = self.getFinancialInformationFromUI();
                self.presenter.createUserInformation(financialInfo, { "action" : "saveAndClose"});
            };
            this.view.FinancialInformation.btnFinancialInfoProceed.onClick = function () {
                var financialInfo = self.getFinancialInformationFromUI();
                self.presenter.createUserInformation(financialInfo);
            };
            if (viewModel && viewModel.data !== undefined) {
                var data = viewModel.data;
                this.view.FinancialInformation.lbxAnnualGrossIncome.selectedKey = data.annualIncome;
                this.view.FinancialInformation.lbxValuOfAssetsYouOwn.selectedKey = data.assets;
                this.view.FinancialInformation.lbxMonthlyExpenditure.selectedKey = data.montlyExpenditure;
            }
            self.AdjustScreen();
        },

        validateUserForm: function () {
            var self = this;
            var isValid = true;
            if (!self.view.UserInformationForm.tbxFirstName.text ||
                !self.view.UserInformationForm.tbxLastName.text ||
                !self.view.UserInformationForm.tbxAddressLine1.text ||
                !self.view.UserInformationForm.tbxCountry.text ||
                !self.view.UserInformationForm.tbxState.text ||
                !self.view.UserInformationForm.tbxCity.text ||
                !self.view.UserInformationForm.tbxZipcode.text ||
                !self.view.UserInformationForm.tbxEnterSSN.text ||
                self.view.UserInformationForm.imgTCContentsCheckbox.src !== "checked_box.png") {
                isValid = false;
            }
            if (!isValid) {
                CommonUtilities.disableButton(self.view.UserInformationForm.btnUserInformationFormProceed);
                self.view.UserInformationForm.btnUserInformationFormSaveClose.setEnabled(false);
                self.view.UserInformationForm.btnUserInformationFormSaveClose.skin="sknBtnffffffBorder3343a81pxRadius2pxDisabled";
            } else {
                CommonUtilities.enableButton(self.view.UserInformationForm.btnUserInformationFormProceed);
                self.view.UserInformationForm.btnUserInformationFormSaveClose.setEnabled(true);
                self.view.UserInformationForm.btnUserInformationFormSaveClose.skin="sknBtnffffffBorder3343a81pxRadius2px";
            }
            //self.view.UserInformationForm.lblWarningUserInfo.setVisibility(false);
            //self.AdjustScreen();
        },

        validateDOBAndSSNAndProceed : function (context) {
            var isValid = true;
            var errMsg = "";
          	this.view.UserInformationForm.calDOB.skin = "sknCalTransactions";
            if(!this.isValidSSN(this.view.UserInformationForm.tbxEnterSSN.text)) {
                isValid = false;
                errMsg = kony.i18n.getLocalizedString("i18n.NUO.invalidSSN");
            }
            if (!this.isValidDateOfBirth(this.view.UserInformationForm.calDOB.date)) {
                isValid = false;
                errMsg = kony.i18n.getLocalizedString("i18n.NUO.invalidDOB");
            }
            if(isValid) {
                this.view.UserInformationForm.lblWarningUserInfo.setVisibility(false);
                var userInfo = this.getUserPersonalInformationFromUI();
                this.presenter.createUserInformation(userInfo, context);
            } else {
                if(errMsg === kony.i18n.getLocalizedString("i18n.NUO.invalidSSN")) {
                    this.view.UserInformationForm.tbxEnterSSN.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
                } else {
                    this.view.UserInformationForm.calDOB.skin = "sknff0000Cal";
                }
                this.view.UserInformationForm.lblWarningUserInfo.text =  errMsg;
                this.view.UserInformationForm.lblWarningUserInfo.setVisibility(true);
                this.view.UserInformationForm.lblWarningUserInfo.setFocus(true);
                this.AdjustScreen();
            }
        },

        isValidSSN : function (ssn) {
            var isValid = false;
            var ssnRE = /^\d{9}$/;
            if(ssn && ssnRE.test(ssn)) {
                isValid = true;
            }
            return isValid;
        },
        isValidDateOfBirth: function (dateOfBirth) {
            var isValid = false;
            if(dateOfBirth) {
                var currDateValue = new Date();
                var dobArray = dateOfBirth.split('/');
                var userDOB = new Date(dobArray[2], dobArray[0] - 1, dobArray[1]);
                var age = currDateValue.getFullYear() - userDOB.getFullYear();
                var m = currDateValue.getMonth() - userDOB.getMonth();
                if (m < 0 || (m === 0 && currDateValue.getDate() < userDOB.getDate())) {
                    age--;
                }
                if (age > 18) {
                    isValid = true;
                }
            }
            return isValid;
        },
        validateEmploymentForm: function () {
            if (this.view.EmploymentInformation.lbxStatus.selectedKeyValue[0] === "Employed" && 
                (!this.view.EmploymentInformation.tbxCompanyName.text ||
                !this.view.EmploymentInformation.tbxJobTitle.text)) {
                CommonUtilities.disableButton(this.view.EmploymentInformation.btnEmploymentInfoProceed);
                this.view.EmploymentInformation.btnEmploymentInfoSaveClose.setEnabled(false);
                this.view.EmploymentInformation.btnEmploymentInfoSaveClose.skin="sknBtnffffffBorder3343a81pxRadius2pxDisabled";
            } else {
                CommonUtilities.enableButton(this.view.EmploymentInformation.btnEmploymentInfoProceed);
                this.view.EmploymentInformation.btnEmploymentInfoSaveClose.setEnabled(true);
                this.view.EmploymentInformation.btnEmploymentInfoSaveClose.skin="sknBtnffffffBorder3343a81pxRadius2px";
            }
        },
        initialize : function() {
            this.view.UserInformationForm.calDOB.dateFormat = CommonUtilities.getConfiguration('frontendDateFormat');
            var mydate = new Date();
            var currDate = [mydate.getDate(), mydate.getMonth() + 1, mydate.getFullYear()];
            this.view.UserInformationForm.calDOB.validEndDate = currDate;
            this.view.UserInformationForm.calDOB.date = CommonUtilities.getFrontendDateString(new Date(), CommonUtilities.getConfiguration("frontendDateFormat"));
            this.view.UserInformationForm.calDOB.dateComponents = this.view.UserInformationForm.calDOB.dateComponents;
        },
        
        /**
         * populate listboxes
         */
        resetPersonalInfoData: function () {
            this.view.UserInformationForm.lbxMaritalStatus.masterData = [['Single', 'Single'], ['Married', 'Married'], ['Widowed', 'Widowed'], ['Divorced', 'Divorced']];
            this.view.UserInformationForm.lbxDependents.masterData = [['0','00'],['1','01'],['2','02'],['3','03'],['4','04'],['5','05'],['6','06'],['7','07'],['8','08']];
            this.view.EmploymentInformation.lbxStatus.masterData = [['Employed', 'Employed'], ['Unemployed', 'Unemployed'], ['Retired', 'Retired'], ['Student', 'Student']];
            this.view.EmploymentInformation.lbxYearsOfExperience.masterData = [[0, '0 year to 1 year'],[1, '1 year to 2 years'],[2, '2 years to 3 years'],[3, '3 years to 4 years'],[4,'4 years to 5 years'],[5, '5 years to 6 years'],[6, '6 years to 7 years'],[7, '7 years to 8 years'],[8, '8 years to 9 years'],[9, '9 years to 10 years'],[10, '10+ years']];
            this.view.FinancialInformation.lbxAnnualGrossIncome.masterData = [[0, '0 - 25000'], [1, '25001 - 50000'], [2, '50001 - 100000'], [3, '100001 - 200000'], [4, '200001 - 300000'], [5, '300001 - 500000'], [6, '500001 - 1200000'], [7, '1200001 - 9999999']];
            this.view.FinancialInformation.lbxValuOfAssetsYouOwn.masterData = [[0, '0 - 25000'], [1, '25001 - 50000'], [2, '50001 - 100000'], [3, '100001 - 200000'], [4, '200001 - 300000'], [5, '300001 - 500000'], [6, '500001 - 1200000'], [7, '1200001 - 9999999']];
            this.view.FinancialInformation.lbxMonthlyExpenditure.masterData = [[0, '0 - 25000'], [1, '25001 - 50000'], [2, '50001 - 100000'], [3, '100001 - 200000'], [4, '200001 - 300000'], [5, '300001 - 500000'], [6, '500001 - 1200000'], [7, '1200001 - 9999999']];
            this.view.UserInformationForm.lblWarningUserInfo.setVisibility(false);
            this.view.UserInformationForm.tbxFirstName.text = "";
            this.view.UserInformationForm.tbxLastName.text = "";
            this.view.UserInformationForm.tbxAddressLine1.text = "";
            this.view.UserInformationForm.tbxAddressLine2.text = "";
            this.view.UserInformationForm.tbxZipcode.text = "";
            this.view.UserInformationForm.tbxSpouseName.text = "";
            this.view.UserInformationForm.tbxSpouseLastName.text = "";
            this.view.UserInformationForm.tbxEnterSSN.text = "";
            this.view.UserInformationForm.lblRadioBtnMale.text = 'R';
            this.view.UserInformationForm.lblRadioBtnMale.skin = "sknLblFontTypeIcon3343e820px";
            this.view.UserInformationForm.lblFemale.text = "";
            this.view.UserInformationForm.lblFemale.skin = "sknC0C0C020pxNotFontIcons";
            this.view.UserInformationForm.tbxCountry.text = "";
            this.view.UserInformationForm.tbxState.text = "";
            this.view.UserInformationForm.tbxCity.text = "";
            //this.view.UserInformationForm.lbxMaritalStatus.selectedKey = "Single";
            //this.view.UserInformationForm.lbxDependents.selectedKey = data.noOfDependents;
            //this.view.EmploymentInformation.lbxStatus.selectedKey = "Employed";
            //this.view.EmploymentInformation.lbxYearsOfExperience.selectedKey = 0;
            this.view.EmploymentInformation.tbxCompanyName.text = "";
            this.view.EmploymentInformation.tbxJobTitle.text = "";
            //this.view.FinancialInformation.lbxAnnualGrossIncome.selectedKey = 0;
            //this.view.FinancialInformation.lbxValuOfAssetsYouOwn.selectedKey = 0;
            //this.view.FinancialInformation.lbxMonthlyExpenditure.selectedKey = 0;
        },

        /**
         * Change focus of OTP Field to next/previous OTP Field
         * @param radioWidgetFlexArray Radio Widget FLex Array 
         * @param toBeSelected User Clicks on to be selected
         * @example 
         *  this.view.flxRadioBtn1.onClick = this.radioBtnAction.bind(this, [this.view.flxRadioBtn1, this.view.flxRadioBtn1])
         * @returns {void} - None
         * @throws {void} - None
         */

        radioBtnAction: function (radioWidgetFlexArray, toBeSelected) {
            radioWidgetFlexArray.forEach(function (flex) {
                flex.widgets()[0].text = " ";
                flex.widgets()[0].skin = "sknC0C0C020pxNotFontIcons";
            })
            toBeSelected.widgets()[0].text = "R";
            toBeSelected.widgets()[0].skin = "sknLblFontTypeIcon3343e820px";
        },

        RadioBtnAction: function (RadioBtn1, RadioBtn2, RadioBtn3, RadioBtn4) {
            if (RadioBtn1.text === "R") {
                RadioBtn1.text = " ";
                RadioBtn1.skin = "sknC0C0C020pxNotFontIcons";
                if (RadioBtn2 != " " || RadioBtn2 !== null) {
                    RadioBtn2.text = "R";
                    RadioBtn2.skin = "sknLblFontTypeIcon3343e820px";
                }
                if (RadioBtn3 != " " || RadioBtn3 !== null) {
                    RadioBtn3.text = "R";
                    RadioBtn3.skin = "sknLblFontTypeIcon3343e820px";
                }
                if (RadioBtn4 != " " || RadioBtn2 !== null) {
                    RadioBtn4.text = "R";
                    RadioBtn4.skin = "sknLblFontTypeIcon3343e820px";
                }
            } else {
                if (RadioBtn2 != " " || RadioBtn2 !== null) {
                    RadioBtn2.text = " ";
                    RadioBtn2.skin = "sknC0C0C020pxNotFontIcons";
                }
                if (RadioBtn3 != " " || RadioBtn3 !== null) {
                    RadioBtn3.text = " ";
                    RadioBtn3.skin = "sknC0C0C020pxNotFontIcons";
                }
                if (RadioBtn4 != " " || RadioBtn4 !== null) {
                    RadioBtn4.text = " ";
                    RadioBtn4.skin = "sknC0C0C020pxNotFontIcons";
                }
                RadioBtn1.text = "R";
                RadioBtn1.skin = "sknLblFontTypeIcon3343e820px";
            }
		},
		
		showOpenNewAccount: function (continueOrResetApplicationViewModel) {
            var scopeObj = this;
            this.showOpenNewAccountUI();
            this.showTopMenu();
            this.view.OpenNewAccount.lblUploadSignatureError.text = kony.i18n.getLocalizedString("i18n.NUO.youhavealreadystartedwarning");
            this.view.OpenNewAccount.lblOpenNewAccountMessageBox2.text = kony.i18n.getLocalizedString("i18n.NUO.youwillstartapplication");
            var radioFlexes = [this.view.OpenNewAccount.flxOpenNewAccountRadiobtn1, this.view.OpenNewAccount.flxOpenNewAccountRadioBtn2];
            this.view.OpenNewAccount.flxOpenNewAccountRadiobtn1.onClick = function () {
                scopeObj.validateOpenNewAccount();
                scopeObj.radioBtnAction(radioFlexes, scopeObj.view.OpenNewAccount.flxOpenNewAccountRadiobtn1);
            }
            this.view.OpenNewAccount.flxOpenNewAccountRadioBtn2.onClick = function () {
                scopeObj.validateOpenNewAccount();                
                scopeObj.radioBtnAction(radioFlexes, scopeObj.view.OpenNewAccount.flxOpenNewAccountRadioBtn2);              
            };
            this.view.OpenNewAccount.lblUploadSignatureColorSeperator.width = continueOrResetApplicationViewModel.progress + "%";
            this.view.OpenNewAccount.btnOpenNewAccountCancel.onClick = function () {
                scopeObj.presenter.doLogout();
            }
            this.view.OpenNewAccount.btnOpenNewAccountProceed.onClick = this.openNewAccountProceed.bind(this, continueOrResetApplicationViewModel);
        },

        openNewAccountProceed: function (continueOrResetApplicationViewModel) {
            if (this.view.OpenNewAccount.lblRadioBtn1.text === 'R' ) {
                this.presenter.findStageAndContinueApplication(continueOrResetApplicationViewModel.userDetails, continueOrResetApplicationViewModel.userState);
            }
            else {
                this.presenter.resetApplication(continueOrResetApplicationViewModel.userDetails);
            }
        },

        validateOpenNewAccount: function () {
            var valid = false;
            var radioFlexes = [this.view.OpenNewAccount.flxOpenNewAccountRadiobtn1, this.view.OpenNewAccount.flxOpenNewAccountRadioBtn2];
            radioFlexes.forEach(function (radioFlex) {
                if (radioFlex.widgets()[0].text === "R") {
                    valid = true;
                }
            })
            if (valid) {
                CommonUtilities.enableButton(this.view.OpenNewAccount.btnOpenNewAccountProceed);
            }
            else{
                CommonUtilities.disableButton(this.view.OpenNewAccount.btnOpenNewAccountProceed);
            }            
        },
        /**
         * Method to show upload document screen with channels option
         * @member frmNewUserOnboardingController
         * @param employmentStatus 
         * @returns None
         * @throws None
         */
        showUploadDocumentsScreen1:function(employmentStatus){
            var self = this;
            self.hideAll();
            self.view.UploadDocPhoneOrComputer.flxColorLine.width="71.42%";
            self.view.UploadDocPhoneOrComputer.flxProofs.setVisibility(true);
            self.view.UploadDocPhoneOrComputer.flxEmployementProof.isVisible=(employmentStatus==="Employed")?true:false;
            self.view.UploadDocPhoneOrComputer.flxIncomeProof.isVisible=(employmentStatus==="Employed")?true:false;
            self.view.flxUploadDocFromPhoneOrComputer.setVisibility(true);
            self.view.UploadDocPhoneOrComputer.lblUserInformation.text=kony.i18n.getLocalizedString("i18n.NUO.UploadDocuments");
            self.view.UploadDocPhoneOrComputer.lblUserInformationMessage1.text=(employmentStatus==="Employed")?kony.i18n.getLocalizedString("i18n.NUO.UploadDocumentMessage1"):kony.i18n.getLocalizedString("i18n.NUO.UploadDocumentMessage1ForAddress");
            self.view.UploadDocPhoneOrComputer.lblUserInformationMessage2.text=kony.i18n.getLocalizedString("i18n.NUO.UploadDocumentMessage2");
            self.view.UploadDocPhoneOrComputer.btnBack.onClick = function () {
                self.presenter.getUserInformation("showFinancialInformationViewModel");
            };
            self.view.UploadDocPhoneOrComputer.btnUserInformationProceed.onClick = function () {
                if (self.view.UploadDocPhoneOrComputer.lblRadioBtn2.text === "R") {
                    self.showLinkSentToPhoneForm();
                } else if (self.view.UploadDocPhoneOrComputer.lblRadioBtn1.text === "R") {
                    self.showUploadDocumentsScreen2(employmentStatus);
                }
            };
            self.view.UploadDocPhoneOrComputer.flxUserInformationRadiobtn1.onClick = self.radioBtnAction.bind(self, self.getUploadDocRadioWidgetsArray());
            self.view.UploadDocPhoneOrComputer.flxUserInformationRadioBtn2.onClick = self.radioBtnAction.bind(self, self.getUploadDocRadioWidgetsArray());
        
            self.view.UploadDocPhoneOrComputer.btnUserInformationSaveClose.onClick = function () {
                self.ShowSaveClosePopup();
            };
            self.view.SaveAndClose.btnYes.onClick = function () {
                self.presenter.doLogout();
            }
            self.AdjustScreen();
        },

        /**
         * Method to show upload document screen 2
         * @member frmNewUserOnboardingController
         * @param employmentStatus 
         * @returns None
         * @throws None
         */
        showUploadDocumentsScreen2:function(employmentStatus){
            var self = this;
            self.hideAll();
            self.view.UploadDocuments.flxHeaderUploadDocuments.setFocus(true);            
            self.view.flxUploadDocuments.setVisibility(true);
            self.view.UploadDocuments.flxColorLine.width="71.42%";
            self.view.UploadDocuments.flxUploadDocumentsEmploymentProof.isVisible=(employmentStatus==="Employed")?true:false;
            self.view.UploadDocuments.lblUploadDocumentsSeperator.isVisible=(employmentStatus==="Employed")?true:false;
            self.view.UploadDocuments.flxUploadDocumentsIncomeProof.isVisible=(employmentStatus==="Employed")?true:false;
            if(employmentStatus !== "Employed"){                
                self.resetUIAfterRemovingAttach("employmentProof");
                self.resetUIAfterRemovingAttach("incomeProof");
            }

            var btnVisibilityEmployed = (self.view.UploadDocuments.flxDocAttachmentIP.isVisible===true && self.view.UploadDocuments.flxDocAttachmentEP.isVisible===true && self.view.UploadDocuments.flxDocAttachmentAP.isVisible===true )
            var btnVisibilityUnemployed=(employmentStatus !== "Employed" && self.view.UploadDocuments.flxDocAttachmentAP.isVisible===true);
            //To enable Proceed button based on some condition - (to solve a issue if user is traversing back and proceed flows)
            if(btnVisibilityEmployed || btnVisibilityUnemployed){
                self.view.UploadDocuments.btnUploadDocumentsProceed.setEnabled(true);
                self.view.UploadDocuments.btnUploadDocumentsProceed.skin="sknBtn3343a8Border3343a82pxRadius2px";
            }
            if (employmentStatus !== "Employed" && self.filesToBeUploaded) {
                //Clearing the "extra" earlier selected files if any - (to solve a issue if user is traversing back and proceed flows)
                if(self.filesToBeUploaded.employmentProof){
                    self.filesToBeUploaded.employmentProof=null;
                }
                if(self.filesToBeUploaded.incomeProof = null){
                    self.filesToBeUploaded.incomeProof = null;
                }
            }
            self.view.UploadDocuments.btnAttachAP.onClick = function () {
                self.browseFiles("addressProof");
            };
            self.view.UploadDocuments.btnAttachEP.onClick = function () {
                self.browseFiles("employmentProof");
            };
            self.view.UploadDocuments.btnAttachIP.onClick = function () {
                self.browseFiles("incomeProof");
            };
            self.view.UploadDocuments.flxRemoveAttachmentAP.onClick = function () {
                self.actionForRemoveAttachment("addressProof");
            };            
            self.view.UploadDocuments.flxRemoveAttachmentEP.onClick = function () {
                self.actionForRemoveAttachment("employmentProof");
            };            
            self.view.UploadDocuments.flxRemoveAttachmentIP.onClick = function () {                
                self.actionForRemoveAttachment("incomeProof");
            };
            self.view.UploadDocuments.btnBack.onClick = function () {
                self.hideAll();
                self.view.flxUploadDocFromPhoneOrComputer.setVisibility(true);
                self.AdjustScreen();
            };
            self.view.UploadDocuments.btnUploadDocumentsProceed.onClick = function () {
                self.saveUploadedDocuments();                
            };
            self.view.UploadDocuments.btnUploadDocumentsSaveClose.onClick =  function(){
                self.ShowSaveClosePopup();
            };
            self.AdjustScreen();
        },
        resetUIAfterRemovingAttach:function(context){
            var self=this;
            self.getConfigForUploadUI(context).btnAttach.skin="sknBtnLato3343A813PxBg0";
            self.getConfigForUploadUI(context).btnAttach.setEnabled(true);
            self.getConfigForUploadUI(context).errorMsg.setVisibility(false);
            self.getConfigForUploadUI(context).attachmentFlex.setVisibility(false);
        },
        actionForRemoveAttachment:function(context){
            var self=this;
            self.resetUIAfterRemovingAttach(context);
            //disable Proceed & saveNclose button
            self.view.UploadDocuments.btnUploadDocumentsProceed.setEnabled(false);
            self.view.UploadDocuments.btnUploadDocumentsProceed.skin="sknBtn3343a8Border3343a82pxRadius2pxDisabled"; 
            self.AdjustScreen();
        },
        /**
         * Opens the browser for uploading the files on click of the Attachment icon
         * @member frmNewUserOnboardingController
         * @param context defines attachment type  
         * @return {}
         * @throws {}
         */
        browseFiles: function (context) {
            var self=this;
            var config = {
                selectMultipleFiles: false,
                filter: ["application/pdf"]
            };
            self.getConfigForUploadUI(context).errorMsg.setVisibility(false);
            kony.io.FileSystem.browse(config, this.browseFilesCallback.bind(self,context));
        },
        /**
         * Returns UI - widget paths based on context
         * @member frmNewUserOnboardingController
         * @param context defines attachment type  
         * @return {}
         * @throws {}
         */
        getConfigForUploadUI: function(context){
            var self=this;
            if(context==="addressProof"){
                return {
                    errorMsg:self.view.UploadDocuments.lblWarningAddressProof,
                    btnAttach:self.view.UploadDocuments.btnAttachAP,
                    fileName:self.view.UploadDocuments.lblDocNameAP,
                    attachmentFlex:self.view.UploadDocuments.flxDocAttachmentAP
                };
            }
            else if(context==="employmentProof"){
                return {
                    errorMsg:self.view.UploadDocuments.lblWarningEmployementProof,
                    btnAttach:self.view.UploadDocuments.btnAttachEP,
                    fileName:self.view.UploadDocuments.lblDocNameEP,
                    attachmentFlex:self.view.UploadDocuments.flxDocAttachmentEP
                };
            }
            else if(context==="incomeProof"){
                return {
                    errorMsg:self.view.UploadDocuments.lblWarningIncomeProof,
                    btnAttach:self.view.UploadDocuments.btnAttachIP,
                    fileName:self.view.UploadDocuments.lblDocNameIP,
                    attachmentFlex:self.view.UploadDocuments.flxDocAttachmentIP
                };
            }
        },
        /**
         * Callback for browse files
         * @member frmNewUserOnboardingController
         * @param context defines attachment type  
         * @return {}
         * @throws {}
         */
        browseFilesCallback: function (context, event, file) {
            var self=this;
            self.getBase64(file[0].file, context);
            self.bindFileToUI(file[0], context);
        },
        /**
        * Binds the attachment data to the segment 
        * @member frmNewUserOnboardingController
        * @param file is a  JSON which consists of the data of the files attached ,
        * @param context defines attachment type
        * @return {} 
        * @throws {}
        */
        bindFileToUI:function(file, context){
            var self = this;
            if(self.isFileTypeNotSupported(file.file)){
                self.getConfigForUploadUI(context).errorMsg.text = kony.i18n.getLocalizedString("i18n.NUO.FileTypeNotSupported");
                self.getConfigForUploadUI(context).errorMsg.setVisibility(true);
            } else if (self.isFileSizeExceeds(file)) {
                self.getConfigForUploadUI(context).errorMsg.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.SizeExceeds1MB");
                self.getConfigForUploadUI(context).errorMsg.setVisibility(true);
            } else {
                if (file) {
                    self.getConfigForUploadUI(context).btnAttach.skin="sknBtnLato3343A813PxBg0CSR";
                    self.getConfigForUploadUI(context).btnAttach.setEnabled(false);
                    self.getConfigForUploadUI(context).fileName.text= file.name;
                    self.getConfigForUploadUI(context).attachmentFlex.setVisibility(true);
                    if(self.view.UploadDocuments.flxUploadDocumentsIncomeProof.isVisible===false || (self.view.UploadDocuments.flxDocAttachmentIP.isVisible===true && self.view.UploadDocuments.flxDocAttachmentEP.isVisible===true && self.view.UploadDocuments.flxDocAttachmentAP.isVisible===true )){
                        //enable Proceed & saveNclose button
                        self.view.UploadDocuments.btnUploadDocumentsProceed.setEnabled(true);
                        self.view.UploadDocuments.btnUploadDocumentsProceed.skin="sknBtn3343a8Border3343a82pxRadius2px";                        
                    }                    
                }
            }
            self.AdjustScreen();
        },
        isFileTypeNotSupported:function(file){
            var type=file.type;
            if(type!=="application/pdf"){
                return true;
            }
            else{
                return false;
            }
        },
        /**
         * Checks if the size of the file exceeds more than 1MB
         * @member frmNewUserOnboardingController
         * @param {JSON} file JSON which consists of the data of the files
         * @return boolean true if the size of the file exceeds false if the file size is less than 1MB
         * @throws {}
         */
        isFileSizeExceeds: function (file) {
            return file.size > 1048576;
        },
        getBase64: function (file, context) {
            var self=this;
            var reader = new FileReader();
            self.filesToBeUploaded = this.filesToBeUploaded || {};
            reader.onloadend = function() {
               self.filesToBeUploaded[context]=reader.result;
            }
            reader.readAsDataURL(file);
        },
        /**
         * Method to save uploaded documents
         * @member frmNewUserOnboardingController
         * @param None 
         * @returns None
         * @throws None
         */
        saveUploadedDocuments:function(){
            var self = this;
            self.presenter.uploadDocuments(self.filesToBeUploaded);            
        },
        /**
         * Method to show next flow after saving documents
         * @member frmNewUserOnboardingController
         * @param None 
         * @returns None
         * @throws None
         */
        performIdentityVerification:function(){
            var self=this;
            var response=[];
            self.presenter.doIdentityVerification(response);
        },
        /**
         * Get identity verification questions
         * @member frmNewUserOnboardingController
         * @param None 
         * @returns None
         * @throws None
         */
        getIdentityVerification:function(){
            var self=this;
            self.presenter.getIdentityVerificationQuestion();
        },
        /**
         * Show identity verification flow
         * @member frmNewUserOnboardingController
         * @param None 
         * @returns None
         * @throws None
         */
        showIdentityVerification:function(questions){
            var self=this;
            self.hideAll();
            self.view.flxIdentityVerificationQuestions.setVisibility(true);
            self.view.IdentityVerificationQuestions.setFocus(true);
            self.view.IdentityVerificationQuestions.flxColorLine.width="71.42%";
            self.bindIdentityQuestionSegment(questions);
            self.view.IdentityVerificationQuestions.btnBack.onClick = function () {
                self.hideAll();
                self.view.flxUploadDocuments.setVisibility(true);
                self.AdjustScreen();
            };
            self.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.onClick = function(){
                self.ShowSaveClosePopup();                
            };
            self.view.IdentityVerificationQuestions.btndentityVerificationProceed.onClick = function () {
                self.showSubmitApplicationPopup();                
            };
            self.validateIdentityQuestions();
            self.AdjustScreen();
        },
        validateIdentityQuestions: function () {
            function isRowAnwsered (rowData) {
                var isSelected = false;
                [1,2,3,4].forEach(function (index) {
                    var isRadioBtnSelected = rowData["lblRadioBtn" + index].text === "M";
                    if (isRadioBtnSelected) {
                        isSelected = true;
                    }
                })
                return isSelected;
            }

            var answeredRows = 0;
            var questionsData = this.view.IdentityVerificationQuestions.segIdentityVerQuestions.data;
            questionsData.forEach(function (questionRow) {
                if (isRowAnwsered(questionRow)) {
                    answeredRows ++;
                }
            })

            if (answeredRows === questionsData.length) {
                this.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.skin = "sknBtnffffffBorder3343a81pxRadius2px";
                this.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.setEnabled(true);
                CommonUtilities.enableButton(this.view.IdentityVerificationQuestions.btndentityVerificationProceed)
                
            }
            else  {
                this.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.skin = "sknBtnffffffBorder3343a81pxRadius2pxDisabled";
                this.view.IdentityVerificationQuestions.btnIdentityVerificationSaveClose.setEnabled(false);
                CommonUtilities.disableButton(this.view.IdentityVerificationQuestions.btndentityVerificationProceed)
            }
        },

        bindIdentityQuestionSegment:function(data){
            var self=this;
            var widgetDataMap = {
                "lblQuestion":"lblQuestion",
                "lblRadioBtn1":"lblRadioBtn1",
                "lblRadioBtn2":"lblRadioBtn2",
                "lblRadioBtn3":"lblRadioBtn3",
                "lblRadioBtn4":"lblRadioBtn4",
                "lblOption1":"lblOption1",
                "lblOption2":"lblOption2",
                "lblOption3":"lblOption3",                
                "lblOption4":"lblOption4",
                "flxNUORadioBtn1": "flxNUORadioBtn1",
                "flxNUORadioBtn2": "flxNUORadioBtn2",
                "flxNUORadioBtn3": "flxNUORadioBtn3",
                "flxNUORadioBtn4": "flxNUORadioBtn4",
                "lblQuestionSeperator":"lblQuestionSeperator"
            };

            var segData = [
                 {
                    "lblQuestion":kony.i18n.getLocalizedString("i18n.NUO.Question1"),
                    "lblRadioBtn1":{
                        text : "L",
                        skin : "sknC0C0C020pxNotFontIconsMOD",
                    },
                    "lblRadioBtn2":{
                        text:"L",
                        skin : "sknC0C0C020pxNotFontIconsMOD",
                    },
                    "lblRadioBtn3":{
                        text:"L",
                        skin : "sknC0C0C020pxNotFontIconsMOD",
                    },
                    "lblRadioBtn4":{
                        text:"L",
                        skin : "sknC0C0C020pxNotFontIconsMOD",
                    },
                    "flxNUORadioBtn1": {
                        onClick: this.toggleRadioBtn.bind(this, 1)
                    },
                    "flxNUORadioBtn2": {
                        onClick: this.toggleRadioBtn.bind(this, 2)
                    },
                    "flxNUORadioBtn3": {
                        onClick: this.toggleRadioBtn.bind(this, 3)
                    },
                    "flxNUORadioBtn4": {
                        onClick: this.toggleRadioBtn.bind(this, 4)
                    },
                    "lblOption1":"January 2016",
                    "lblOption2":"March 2013",
                    "lblOption3":"December 2015",                
                    "lblOption4":"None of the Above",
                    "lblQuestionSeperator":"lblQuestionSeperator"
                },
                {
                    "lblQuestion":kony.i18n.getLocalizedString("i18n.NUO.Question2"),
                    "lblRadioBtn1":{
                        text:"L",
                        skin : "sknC0C0C020pxNotFontIconsMOD",
                    },
                    "lblRadioBtn2":{
                        text:"L",
                        skin : "sknC0C0C020pxNotFontIconsMOD",
                    },
                    "lblRadioBtn3":{
                        text:"L",
                        skin : "sknC0C0C020pxNotFontIconsMOD",
                    },
                    "lblRadioBtn4":{
                        text:"L",
                        skin : "sknC0C0C020pxNotFontIconsMOD",
                    },
                    "flxNUORadioBtn1": {
                        onClick: this.toggleRadioBtn.bind(this,1)
                    },
                    "flxNUORadioBtn2": {
                        onClick: this.toggleRadioBtn.bind(this,2)
                    },
                    "flxNUORadioBtn3": {
                        onClick: this.toggleRadioBtn.bind(this,3)
                    },
                    "flxNUORadioBtn4": {
                        onClick: this.toggleRadioBtn.bind(this,4)
                    },
                    "lblOption1":"Bank Of America",
                    "lblOption2":"Wells Fargo",
                    "lblOption3":"Chase",                
                    "lblOption4":"None of the Above",
                    "lblQuestionSeperator":"lblQuestionSeperator"
                }
            ];
            this.view.IdentityVerificationQuestions.segIdentityVerQuestions.widgetDataMap=widgetDataMap;
            this.view.IdentityVerificationQuestions.segIdentityVerQuestions.setData(segData);
        },
        toggleRadioBtn: function(index) {
            var self=this;
                var labelWidgetIndex = this.view.IdentityVerificationQuestions.segIdentityVerQuestions.selectedIndex[1];
                var segData = this.view.IdentityVerificationQuestions.segIdentityVerQuestions.data;
                var rowData = this.view.IdentityVerificationQuestions.segIdentityVerQuestions.selectedItems[0];
                rowData.lblRadioBtn1 = {
                    "text": "L",
                    "skin": "sknC0C0C020pxNotFontIconsMOD"
                };
                rowData.lblRadioBtn2 = {
                    "text": "L",
                    "skin": "sknC0C0C020pxNotFontIconsMOD"
                };
                rowData.lblRadioBtn3 = {
                    "text": "L",
                    "skin": "sknC0C0C020pxNotFontIconsMOD"
                };
                rowData.lblRadioBtn4 = {
                    "text": "L",
                    "skin": "sknC0C0C020pxNotFontIconsMOD"
                };
                rowData['lblRadioBtn' + index] = {
                    "text": "M",
                    "skin": "sknLblFontTypeIcon3343e820pxMOD"
                };
                this.view.IdentityVerificationQuestions.segIdentityVerQuestions.setDataAt(rowData, labelWidgetIndex);
                this.validateIdentityQuestions();
            },
        showIdentityVerificationError:function(){
            var self=this;
            self.hideAll();
            self.view.flxIdentityVerification.setVisibility(true);
            self.view.IdentityVerification.flxIdentityVerificationButtons.setVisibility(false);
            self.view.IdentityVerification.lblIdentityVerification.text = kony.i18n.getLocalizedString("i18n.NUO.IdentityVerification");
            self.view.IdentityVerification.lblMessageHeading.text = kony.i18n.getLocalizedString("i18n.NUO.IdentityVerificationError");
            self.view.IdentityVerification.lblMessage.text = kony.i18n.getLocalizedString("i18n.NUO.TheIdentityVerificationhasFailed");
            self.view.IdentityVerification.btnAction.text = kony.i18n.getLocalizedString("i18n.NUO.retry");
            self.view.IdentityVerification.btnAction.toolTip = kony.i18n.getLocalizedString("i18n.NUO.retry");
            self.view.IdentityVerification.btnAction.onClick=function(){
                self.showIdentityVerification();
            }
            self.view.IdentityVerification.imgflxIdentityVerification.src = "nuo_credit_check_failure.png";
            self.AdjustScreen();
        },
        showSubmitApplicationPopup:function(){   
            var self=this;         
            self.view.flxSubmitApplication.setVisibility(true);
            self.view.SubmitApplication.btnYes.onClick = function () {
                self.performIdentityVerification();
            };
            var height = self.SetPopUpHeight();
            self.view.flxSubmitApplication.height = height + "dp";
            self.view.flxSubmitApplication.setFocus(true);
            self.view.forceLayout();
        },
        performCreditCheck:function(){
            var self=this;
            self.presenter.userCreditCheck();
        },
		showOpenNewAccountUI: function () {
			this.hideAll();
			this.view.flxOpenNewAccount.setVisibility(true);
			this.AdjustScreen();
        },
        showDigitalSignatureFlow:function(){
            var self=this;
            self.hideAll();
            self.view.flxDigitalSignatureSelectionMode.setVisibility(true);
            self.view.DigitalSignatureSelectionMode.flxColorLine.width="85.71%";
            self.view.DigitalSignatureSelectionMode.btnDigitalSignatureProceed.onClick = function () {
                if (self.view.DigitalSignatureSelectionMode.lblRadioBtn2.text === "R") {
                    self.showUploadSignatureFromPhone();
                }
                else if (self.view.UploadDocPhoneOrComputer.lblRadioBtn1.text === "R") {
                    self.showUploadSignatureFromSystem();
                }
            };
            self.AdjustScreen();
        },
        showUploadSignatureFromPhone:function(){
            var self=this;
            self.hideAll();
            self.view.flxIdentityVerification.setVisibility(true);
            self.view.IdentityVerification.flxColorLine.width="85.71%";
            self.view.IdentityVerification.flxIdentityVerificationButtons.setVisibility(true);
            self.view.IdentityVerification.lblIdentityVerification.text = kony.i18n.getLocalizedString("i18n.NUO.DigitalSignature");
            self.view.IdentityVerification.lblMessageHeading.text = kony.i18n.getLocalizedString("i18n.NUO.LinkSentToPhone");
            self.view.IdentityVerification.lblMessage.text = kony.i18n.getLocalizedString("i18n.NUO.ClickTheLinkToContinue");
            self.view.IdentityVerification.btnAction.text = kony.i18n.getLocalizedString("i18n.NUO.RESEND");
            self.view.IdentityVerification.btnAction.toolTip = kony.i18n.getLocalizedString("i18n.NUO.RESEND");
            self.view.IdentityVerification.btnAction.onClick = function(){
                self.showLoadingIndicator();
            };
            self.view.IdentityVerification.imgflxIdentityVerification.src = "nuo_signature_mobile.png";
            self.view.IdentityVerification.btnDONE.onClick = function () {
                self.presenter.doLogout();
            }
        },
        showUploadSignatureFromSystem:function(){
            var self=this;
            self.hideAll();
            var data={
                url:"http://pmqa.konylabs.net/KonyWebBanking/digital_signature_form.pdf",
                filename:kony.i18n.getLocalizedString('i18n.NUO.SignatureForm')
            }
            self.downloadFile(data);
            self.view.flxDigitalSignature.setVisibility(true);
            if(self.view.DigitalSignatureUploadForm.flxDocAttachment.isVisible === false){
                self.view.DigitalSignatureUploadForm.btnUploadForm.skin = "sknBtnLato3343A813PxBg0";
                self.view.DigitalSignatureUploadForm.btnUploadForm.setEnabled(true);
            }
            else {
                self.view.DigitalSignatureUploadForm.btnUploadForm.skin = "sknBtnLato3343A813PxBg0CSR";
                self.view.DigitalSignatureUploadForm.btnUploadForm.setEnabled(false);
            }
            self.view.DigitalSignatureUploadForm.flxColorLine.width="85.71%";
            self.AdjustScreen();
            self.view.DigitalSignatureUploadForm.btnDigitalSignatureBack.onClick = function () {
                self.hideAll();
                self.view.flxDigitalSignatureSelectionMode.setVisibility(true);
                self.AdjustScreen();
            };
            this.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.onClick = function () {
                self.presenter.uploadSignature(self.filesToBeUploaded.signature);
            };
            this.view.DigitalSignatureUploadForm.btnUploadForm.onClick = function () {
                self.browseFilesForSignature();
            };
            this.view.DigitalSignatureUploadForm.flxRemoveAttachment.onClick = function () {
                self.view.DigitalSignatureUploadForm.btnUploadForm.skin="sknBtnLato3343A813PxBg0";
                self.view.DigitalSignatureUploadForm.flxDocAttachment.setVisibility(false);                
                self.view.DigitalSignatureUploadForm.btnUploadForm.setEnabled(true);
                self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.setEnabled(false);
                self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.skin="sknBtn3343a8Border3343a82pxRadius2pxDisabled";
                self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.hoverSkin="sknBtn3343a8Border3343a82pxRadius2pxDisabled"; 
            };
        },
        showCreditCheckErrorFlow:function(){
            var self=this;
            self.hideAll();
            self.view.flxCreditCheckError.setVisibility(true);
            self.view.CreditCheckNAO.btnCreditCheckClose.onClick=function(){
                self.presenter.doLogout();
            }
            self.AdjustScreen();
        },
        /**
         * Download Upload Signature File
         * @member frmNewUserOnboardingController
         * @param None
         * @return {}
         * @throws {} 
         */
        downloadFile: function(data) {
            CommonUtilities.downloadFile({
                'url': data.url,
                'filename': data.filename
            })            
        },
        showAcknowledgementScreen: function() {
            var self=this;
            self.hideAll();
            self.view.flxReEnterPassword.setVisibility(true); 
            self.view.ReEnterPassword.txtEnterPassword.text = "";
            self.view.ReEnterPassword.txtEnterPassword.onKeyUp = function () {
                if (self.view.ReEnterPassword.txtEnterPassword.text.length < 1) {
                    CommonUtilities.disableButton(self.view.ReEnterPassword.btnProceed)
                }
                else {
                    CommonUtilities.enableButton(self.view.ReEnterPassword.btnProceed)
                }
            }
            self.view.ReEnterPassword.btnProceed.onClick = function () {
                self.presenter.doCustomerLogin(self.view.ReEnterPassword.txtEnterPassword.text);
            }
            self.AdjustScreen();            

        },
        browseFilesForSignature: function () {
            var self=this;
            var config = {
                selectMultipleFiles: false,
                filter: ["application/pdf"]
            };
            self.view.DigitalSignatureUploadForm.lblWarningAddressProof.setVisibility(false);
            kony.io.FileSystem.browse(config, self.signatureBrowseCallback);
        },
        signatureBrowseCallback:function (event, file){
            var self=this;
            self.getBase64(file[0].file, "signature");
            self.bindSignatureFileToUI(file[0]);
        },
        bindSignatureFileToUI:function(file){
            var self = this;
            if(self.isFileTypeNotSupported(file.file)){
                self.view.DigitalSignatureUploadForm.lblWarningAddressProof.text = kony.i18n.getLocalizedString("i18n.NUO.FileTypeNotSupported");
                self.view.DigitalSignatureUploadForm.lblWarningAddressProof.setVisibility(true);
            } else if (self.isFileSizeExceeds(file)) {
                self.view.DigitalSignatureUploadForm.lblWarningAddressProof.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.SizeExceeds1MB");
                self.view.DigitalSignatureUploadForm.lblWarningAddressProof.setVisibility(true);
            }else {
                if (file) {
                    self.view.DigitalSignatureUploadForm.btnUploadForm.skin="sknBtnLato3343A813PxBg0CSR";
                    self.view.DigitalSignatureUploadForm.btnUploadForm.setEnabled(false);
                    self.view.DigitalSignatureUploadForm.lblDocName.text= file.name;
                    self.view.DigitalSignatureUploadForm.flxDocAttachment.setVisibility(true);
                    //enable Done button
                    self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.setEnabled(true);
                    self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.skin="sknBtn3343a8Border3343a82pxRadius2px";
                    self.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.hoverSkin="sknBtn3343a8Border3343a82pxRadius2px";
                }
            }
            self.AdjustScreen();
        },

        hideTopMenu: function () {
            this.view.customheader.headermenu.setVisibility(false);
        },

        showTopMenu: function () {
            this.view.customheader.headermenu.setVisibility(true);   
            this.view.customheader.headermenu.btnLogout.onClick = function () {
                this.presenter.doLogout();
            }.bind(this);   
            this.view.forceLayout();    
        },

        showEnrollError: function (error) {
            if (error === 'userNameError') {
                this.view.CreateUsernamePassword.lblUsernameDoesnotMatch.setVisibility(true);
                this.view.CreateUsernamePassword.imgUsername.setVisibility(false);                                
                this.view.CreateUsernamePassword.lblUsernameDoesnotMatch.text = "This username is not available.";                
                this.view.forceLayout();
            }
        },
        showLoginError: function (errorMessage) {
            this.view.EnterUsernamePassword.lblError.setVisibility(true);                                
            this.view.EnterUsernamePassword.lblError.text = errorMessage;                
            this.view.forceLayout();
        },

        showUploadSignatureScreen: function () {
            var self = this;
            self.hideAll();
            self.view.flxUploadSignature.setVisibility(true);
            self.view.UploadSignature.flxUploadSignatureMailAddress2.text="Theodore Lowe";            
            self.view.UploadSignature.flxUploadSignatureMailAddress3.text="Ap #867-859 Sit Rd.";
            self.view.UploadSignature.flxUploadSignatureMailAddress4.text="Azusa New York - 39531";
            self.AdjustScreen();
            self.view.UploadSignature.btnProceed.text = kony.i18n.getLocalizedString("i18n.common.proceed");
            self.view.UploadSignature.btnProceed.onClick = function () {
                self.presenter.navigateToCreditCheckSuccess();
            }
            self.showTopMenu();
        },

        showCustomerLoginError: function (errorMessage) {
            this.view.ReEnterPassword.lblWarningReenterPassword.text = errorMessage;
            this.view.ReEnterPassword.lblWarningReenterPassword.setVisibility(true);            
            this.view.forceLayout();
        },



















        // UI Code
        postShowfrmNewUserOnboarding: function () {
            var self=this;
            this.disableButton(this.view.SendOTP.btnSendOTPProceed);
            this.view.BannerNUO.imgBanner.src = OLBConstants.IMAGES.BANNER_IMAGE;            
            this.disableButton(this.view.EnterUsernamePassword.btnOpenNewAccountProceed);
            this.disableButton(this.view.DigitalSignatureUploadForm.btnDigitalSignatureDone);
            self.view.UploadDocuments.btnUploadDocumentsProceed.setEnabled(false);
            self.view.UploadDocuments.btnUploadDocumentsProceed.skin="sknBtn3343a8Border3343a82pxRadius2pxDisabled"; 
            self.view.DigitalSignatureUploadForm.flxDocAttachment.setVisibility(false);       
            self.view.UploadDocuments.flxDocAttachmentAP.setVisibility(false);
            self.view.UploadDocuments.flxDocAttachmentEP.setVisibility(false);
            self.view.UploadDocuments.flxDocAttachmentIP.setVisibility(false);
            self.view.UploadDocuments.btnAttachAP.skin="sknBtnLato3343A813PxBg0";
            self.view.UploadDocuments.btnAttachAP.setEnabled(true);
            self.view.UploadDocuments.btnAttachEP.skin="sknBtnLato3343A813PxBg0";
            self.view.UploadDocuments.btnAttachEP.setEnabled(true);
            self.view.UploadDocuments.btnAttachIP.skin="sknBtnLato3343A813PxBg0";
            self.view.UploadDocuments.btnAttachIP.setEnabled(true);            
            this.resetPersonalInfoData();
            this.setFlowActions();
            this.AdjustScreen();            
        },
        setFlowActions: function () {
            var scopeObj = this;
            
            this.view.CreateAccountUponAccountFound.btnCreateAccountLogin.onClick = function () {
                scopeObj.showEnterUsernamePassword();
            };

            this.view.EnterUsernamePassword.btnOpenNewAccountProceed.onClick = function () {
                scopeObj.showViews(["flxChooseEligibilityCriteria"]);
            };
            this.view.AccountsDescription.btnAccountsDescriptionProceed.onClick = function () {
                scopeObj.view.flxMainContainer.setVisibility(false);
                scopeObj.view.flxAccounts.setVisibility(true);
            };
            this.view.AccountsDescription.btnAccountsDescriptionCancel.onClick = function () {
                scopeObj.view.flxMainContainer.setVisibility(false);
                scopeObj.view.flxAccounts.setVisibility(true);
            };
            this.view.btnContinueProceed.onClick = function () {
                scopeObj.showViews(["flxUploadDocFromPhoneOrComputer"]);
            };
            /*
            this.view.UploadDocPhoneOrComputer.btnUserInformationProceed.onClick = function() {
              if(scopeObj.view.UploadDocPhoneOrComputer.flxProofs.isVisible === true) {
                 scopeObj.showViews(["flxUploadDocuments"]);
              }
              else{
                if(scopeObj.view.UploadDocPhoneOrComputer.lblRadioBtn2.text === "R") {
                    scopeObj.showViews(["flxIdentityVerification"]);
                    scopeObj.view.IdentityVerification.lblIdentityVerification.text = "UPLOAD FROM PHONE";
                    scopeObj.view.IdentityVerification.lblMessageHeading.text = "LINK SENT TO PHONE";
                    scopeObj.view.IdentityVerification.lblMessage.text = "Click the link sent on phone to continue. Incase you have not recieved the link click resend.";
                    scopeObj.view.IdentityVerification.btnAction.text = "RESEND";
                    scopeObj.view.IdentityVerification.flxIdentityVerificationButtons.setVisibility(true);
                }
                else if(scopeObj.view.UploadDocPhoneOrComputer.lblRadioBtn1.text === "R") {
                    scopeObj.showViews(["flxUserInformationForm"]);
                }
              }
            };
            */
            
            // this.view.IdentityVerificationQuestions.btnBack.onClick = function () {
            //     scopeObj.showViews(["flxUserInformationForm"]);
            // };
            // this.view.UploadDocuments.btnBack.onClick = function () {
            //     if (scopeObj.view.UploadDocuments.flxDocAttachmentAP.isVisible === true) {
            //         scopeObj.view.UploadDocuments.flxDocAttachmentAP.setVisibility(false);
            //         scopeObj.view.UploadDocuments.flxDocAttachmentEP.setVisibility(false);
            //         scopeObj.view.UploadDocuments.flxDocAttachmentIP.setVisibility(false);
            //     }
            //     else {
            //         scopeObj.showViews(["flxUploadDocFromPhoneOrComputer"]);
            //         scopeObj.view.UploadDocPhoneOrComputer.flxProofs.isVisible = true;
            //     }
            // };
            // this.view.UploadDocuments.btnUploadDocumentsProceed.onClick = function () {
            //     if (scopeObj.view.UploadDocuments.flxDocAttachmentAP.isVisible === true) {
            //         scopeObj.showViews(["flxIdentityVerificationQuestions"]);
            //     }
            //     else {
            //         scopeObj.view.UploadDocuments.flxDocAttachmentAP.setVisibility(true);
            //         scopeObj.view.UploadDocuments.flxDocAttachmentEP.setVisibility(true);
            //         scopeObj.view.UploadDocuments.flxDocAttachmentIP.setVisibility(true);
            //         scopeObj.view.forceLayout();
            //     }
            // };
            // this.view.IdentityVerificationQuestions.btndentityVerificationProceed.onClick = function () {
            //     scopeObj.view.flxSubmitApplication.setVisibility(true);
            //     var height = scopeObj.SetPopUpHeight();
            //     scopeObj.view.flxSubmitApplication.height = height + "dp";
            //     scopeObj.view.flxSubmitApplication.setFocus(true);
            // };
            this.view.SubmitApplication.flxCross.onClick = function () {
                scopeObj.view.flxSubmitApplication.setVisibility(false);
            };
            this.view.SubmitApplication.btnNo.onClick = function () {
                scopeObj.view.flxSubmitApplication.setVisibility(false);
            };
            // this.view.SubmitApplication.btnYes.onClick = function () {
            //     scopeObj.showViews(["flxDigitalSignatureSelectionMode"]);
            //     scopeObj.view.flxSubmitApplication.setVisibility(false);
            // };
            this.view.DigitalSignatureSelectionMode.flxDigitalSignatureRadiobtn1.onClick = function () {
                scopeObj.RadioBtnAction(scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn1, scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn2, " ", " ");
            };
            this.view.DigitalSignatureSelectionMode.flxUserInformationRadioBtn2.onClick = function () {
                scopeObj.RadioBtnAction(scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn2, scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn1, " ", " ");
            };
            // this.view.DigitalSignatureSelectionMode.btnDigitalSignatureProceed.onClick = function () {
            //     if (scopeObj.view.DigitalSignatureSelectionMode.lblRadioBtn2.text === "R") {
            //         scopeObj.showViews(["flxIdentityVerification"]);
            //         scopeObj.view.IdentityVerification.lblIdentityVerification.text = "DIGITAL SIGNATURE";
            //         scopeObj.view.IdentityVerification.lblMessageHeading.text = "LINK SENT TO PHONE";
            //         scopeObj.view.IdentityVerification.lblMessage.text = "Click the link sent on phone to Sign. Incase you have not recieved the link click resend.";
            //         scopeObj.view.IdentityVerification.btnAction.text = "RESEND";
            //         scopeObj.view.IdentityVerification.imgflxIdentityVerification.src = "nuo_signature_mobile.png";
            //         scopeObj.view.IdentityVerification.flxIdentityVerificationButtons.setVisibility(true);
            //     }
            //     else if (scopeObj.view.UploadDocPhoneOrComputer.lblRadioBtn1.text === "R") {
            //         scopeObj.view.flxViewReport.setVisibility(true);
            //         var height = scopeObj.SetPopUpHeight();
            //         scopeObj.view.flxViewReport.height = height + "dp";
            //     }
            // };
            this.view.ViewReport.btnClose.onClick = function () {
                scopeObj.showViews(["flxDigitalSignature"]);
            };
            // this.view.DigitalSignatureUploadForm.btnDigitalSignatureBack.onClick = function () {
            //     scopeObj.showViews(["flxDigitalSignatureSelectionMode"]);
            // };
            // this.view.DigitalSignatureUploadForm.btnUploadForm.onClick = function () {
            //     scopeObj.view.DigitalSignatureUploadForm.flxDocAttachment.setVisibility(true);
            //     scopeObj.enableButton(scopeObj.view.DigitalSignatureUploadForm.btnDigitalSignatureDone);
            // };
            // this.view.DigitalSignatureUploadForm.flxRemoveAttachment.onClick = function () {
            //     scopeObj.view.DigitalSignatureUploadForm.flxDocAttachment.setVisibility(false);
            //     scopeObj.disableButton(scopeObj.view.DigitalSignatureUploadForm.btnDigitalSignatureDone);
            // };
            // this.view.DigitalSignatureUploadForm.btnDigitalSignatureDone.onClick = function () {
            //     scopeObj.showViews(["flxAcknowledgement"]);
            // };
            this.view.AcknowledgementNAO.btnAckGoToAccounts.onClick = function () {
                var nav = new kony.mvc.Navigation("frmAccountsLanding");
                nav.navigate();
            };
            this.view.DigitalSignatureSelectionMode.btnDigitalSignatureSaveClose.onClick = this.ShowSaveClosePopup;
            this.view.ReviewApplication.btnReviewApplicationSaveClose.onClick = this.ShowSaveClosePopup;
            this.view.OpenNewAccount.flxOpenNewAccountRadiobtn1.onClick = function () {
                scopeObj.RadioBtnAction(scopeObj.view.OpenNewAccount.lblRadioBtn1, scopeObj.view.OpenNewAccount.lblRadioBtn2, " ", " ");
            };
            this.view.OpenNewAccount.flxOpenNewAccountRadioBtn2.onClick = function () {
                scopeObj.RadioBtnAction(scopeObj.view.OpenNewAccount.lblRadioBtn2, scopeObj.view.OpenNewAccount.lblRadioBtn1, " ", " ");
            };

        },
        ShowSaveClosePopup: function () {
            var scopeObj = this;
            this.view.flxSaveClose.setVisibility(true);
            var height = this.SetPopUpHeight();
            this.view.flxSaveClose.height = height + "dp";
            this.view.flxSaveClose.setFocus(true);
            this.view.flxSaveClose.setFocus(false);
            this.view.SaveAndClose.btnNo.onClick = function () {
                scopeObj.view.flxSaveClose.setVisibility(false);
            };
            this.view.SaveAndClose.flxCross.onClick = function () {
                scopeObj.view.flxSaveClose.setVisibility(false);
            };
        },
        showViews: function (views) {
            this.hideAll();
            for (var i = 0; i < views.length; i++) {
                this.view[views[i]].isVisible = true;
            }
            this.view.forceLayout();
        },
        toggleCheckBox: function (imgCheckBox) {
            if (imgCheckBox.src == "unchecked_box.png") {
                imgCheckBox.src = "checked_box.png";
            } else {
                imgCheckBox.src = "unchecked_box.png";
            }
        },
        SetPopUpHeight: function () {
            var mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height +this.view.flxFooter.frame.height;
            return mainheight;
        },
        hideAll: function () {
            this.view.flxEnterSecureAccessCode.setVisibility(false);
            this.view.flxChooseEligibilityCriteria.setVisibility(false);
            this.view.flxUploadSignature.setVisibility(false);
            this.view.flxCreateUsernamePassowrd.setVisibility(false);
            this.view.flxEnterUsernamePassword.setVisibility(false);
            this.view.flxCreateAccountUponAccountFound.setVisibility(false);
            this.view.flxOpenNewAccount.setVisibility(false);
            this.view.flxSendOTP.setVisibility(false);
            this.view.flxIdentityVerification.setVisibility(false);
            this.view.flxUserInformationForm.setVisibility(false);
            this.view.flxIdentityVerificationQuestions.setVisibility(false);
            this.view.flxAccountsView.setVisibility(false);
            this.view.flxCreditCheckError.setVisibility(false);
            this.view.flxAcknowledgement.setVisibility(false);
            this.view.flxDigitalSignatureSelectionMode.setVisibility(false);
            this.view.flxReviewApplication.setVisibility(false);
            this.view.flxEmploymentInformation.setVisibility(false);
            this.view.flxFinancialInformation.setVisibility(false);
            this.view.flxDigitalSignature.setVisibility(false);
            this.view.flxReEnterPassword.setVisibility(false);
            this.view.flxUploadDocuments.setVisibility(false);
            this.view.flxSubmitApplication.setVisibility(false);
            this.view.flxUploadDocFromPhoneOrComputer.setVisibility(false);
        },
        /**
         *  Disable button.
         */
        disableButton: function (button) {
            button.setEnabled(false);
            button.skin = "sknBtnBlockedLatoFFFFFF15Px";
            button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
            button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
        },
        /**
         * Enable button.
         */
        enableButton: function (button) {
            button.setEnabled(true);
            button.skin = "sknbtnLatoffffff15px";
            button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
            button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
        }


    };

});
