define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnMakeTransfer **/
    AS_Button_b86f0b9fb79344678e17324a5ce90e5a: function AS_Button_b86f0b9fb79344678e17324a5ce90e5a(eventobject) {
        var self = this;
        //test
    },
    /** onClick defined for btnPayABill **/
    AS_Button_g16d4b800b324495836d3cae5adf9e1d: function AS_Button_g16d4b800b324495836d3cae5adf9e1d(eventobject) {
        var self = this;
        //test
    },
    /** onClick defined for btnTxnLink **/
    AS_Button_c6be953f59a143368dfde744f4eea992: function AS_Button_c6be953f59a143368dfde744f4eea992(eventobject) {
        var self = this;
        window.open("https://www.orangecountyscu.org/");
    },
    /** init defined for frmAccountsDetails **/
    AS_Form_f8aea1b7f61843bd906101be866f633f: function AS_Form_f8aea1b7f61843bd906101be866f633f(eventobject) {
        var self = this;
        this.initFrmAccountDetails();
        this.TransactionTabActions();
        //this.checkNumberAction();
    },
    /** preShow defined for frmAccountsDetails **/
    AS_Form_g2660024916f4f939511fc6bf325d5bc: function AS_Form_g2660024916f4f939511fc6bf325d5bc(eventobject) {
        var self = this;
        this.preshowFrmAccountDetails();
    },
    /** postShow defined for frmAccountsDetails **/
    AS_Form_i89b033fd6b04805a7caa26fe32cb50b: function AS_Form_i89b033fd6b04805a7caa26fe32cb50b(eventobject) {
        var self = this;
        this.postShowFrmAccountDetails();
    },
    /** onDeviceBack defined for frmAccountsDetails **/
    AS_Form_d2c95aad04d24a50bbcce19bc10796a6: function AS_Form_d2c95aad04d24a50bbcce19bc10796a6(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAccountsDetails **/
    AS_Form_ef02bd910fb04f22ad9daa9bc454523d: function AS_Form_ef02bd910fb04f22ad9daa9bc454523d(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});