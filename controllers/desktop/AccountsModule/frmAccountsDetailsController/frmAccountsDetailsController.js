define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities, OLBConstants) {

  return {
  noOfClonedChecks:0,

  willUpdateUI: function (viewModel) {
    if (viewModel) {
      if (viewModel.progressBar !== undefined){
        this.updateProgressBarState(viewModel.progressBar);
      } else if (viewModel.serverError)  {
        this.setServerError(viewModel.serverError);
      } else {
        if (viewModel.accountList) {
          this.updateAccountList(viewModel.accountList);
        }
        if (viewModel.accountDetails) {
          setTimeout(this.alignAccountTypesToAccountSelectionImg, 500);
          this.showAccountSummary();
          this.showAccountDetailsScreen();
          this.updateAccountTransactionsTypeSelector(viewModel.accountDetails.accountInfo.accountType);
          this.updateAccountDetails(viewModel.accountDetails);
        }
        if(viewModel.showDownloadStatement){
          this.downloadUrl(viewModel.showDownloadStatement);
        }
         if (viewModel.action==="Account Details Success") {
          this.setDataToLoanOnModify(viewModel.data[0],viewModel.data[1]);
        }
        if (viewModel.action==="Navigation To AccountDetails"){
          this.backToAccountDetailsCallback(viewModel.data);
        }
        if (viewModel.action==="Navigation To eStatements"){
          this.showFormatScreen(viewModel.data);
        }
        if (viewModel.transactionDetails) {
          this.highlightTransactionType(viewModel.transactionDetails.showingType);
          if (viewModel.transactionDetails.renderTransactions) {
            this.updateTransactions(viewModel.transactionDetails);
            this.updatePaginationBar(viewModel.transactionDetails);
          }
          this.updateSearchTransactionView(viewModel.transactionDetails.searchViewModel)
        }
        if (viewModel.sideMenu) {
          this.updateHamburgerMenu(viewModel.sideMenu);
        }
        if (viewModel.topBar) {
          this.updateTopBar(viewModel.topBar);
        }  
      }      
    }
     this.AdjustScreen();
  },
     /**
     * Method to handle Scheduled loan transactions
     * @member of {frmAccountDetailsController}
     * @param {Json object} account- to account json 
     * @param {string} fromAccount- from account id
     * @return {}
     * @throws {}
     */
    setDataToLoanOnModify: function(account, fromAccount) {
            CommonUtilities.showProgressBar(this.view);
            account.currentAmountDue = CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue);
            account.currentAmountDue = account.currentAmountDue.slice(1);
            var data={"accounts":account,
                     "fromAccount":fromAccount};
            var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LoanPayModule");
            loanModule.presentationController.navigateToLoanDue(data);
        },
  /**
     * Method to handle Server error 
     * @member of {frmAccountDetailsController}
     * @param {boolena} isError , error flag to show/hide error flex.
     * @return {}
     * @throws {}
     */
  setServerError: function (isError) {
    var scopeObj = this;
    scopeObj.view.flxDowntimeWarning.setVisibility(isError);
    if (isError) { 
      scopeObj.view.lblDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
    }
    scopeObj.updateProgressBarState(false);
    scopeObj.view.forceLayout();
  },
  /**
   * updateProgressBarState : Method to show or hide Loading progress bar
   * @param {boolena} isError , error flag to show/hide error flex.
   * @member of {frmAccountDetailsController}
   * @param {boolean} isLoading - loading flag
   * @return {}
   * @throws {} 
   */
  updateProgressBarState: function (isLoading) {
    if (isLoading) {
      this.setServerError(false);
      CommonUtilities.showProgressBar(this.view);
    }
    else {
      CommonUtilities.hideProgressBar(this.view);
    }
  },

  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  },

  /**
  * showViewStatements - On click of btnDownload after selecting a format
  * @member of {frmAccountDetailsController}
  * @params {}
  * @return {}
  * @throws {}
  */
  showViewStatements: function(){
    var scopeObj = this;
    var text1=kony.i18n.getLocalizedString("i18n.topmenu.accounts");
    var text2=kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
    var text3=kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS");
    this.view.breadcrumb.setBreadcrumbData([{text:text1}, {text:text2,  callback:scopeObj.backToAccountDetails },{text:text3}]);	
    this.format=this.view.downloadTransction.lbxSelectFormat.selectedKeyValue[1];
    this.view.ViewStatements.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.ViewStatements.GoToAccountSummary"); 
    this.view.ViewStatements.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.accounts.accountDetails");
    this.view.flxHeader.isVisible=true;
    this.view.flxMainWrapper.isVisible=true;
    this.view.flxMain.isVisible=true;
    this.view.flxEditRule.isVisible=false;
    this.view.flxCheckImage.isVisible=false;
    //this.view.flxLoading.isVisible=false;
    this.view.flxLogout.isVisible=true;
    this.view.flxAccountTypesAndInfo.isVisible=false;
    this.view.flxAccountSummaryAndActions.isVisible=false;
    this.view.flxDisputedTransactionDetail.isVisible=false;
    this.view.flxTransactions.isVisible=false;
    this.view.flxFooter.isVisible=true;
    this.view.accountInfo.isVisible=false;
    this.view.accountTypes.isVisible=false;
    this.view.moreActions.isVisible=false;
    this.view.moreActionsDup.isVisible=false;
    this.view.breadcrumb.isVisible=true;
    this.view.flxskncontainer.isVisible=false;
    this.view.flxViewStatements.isVisible=true;
    this.view.flxDownloadTransaction.isVisible=false;
    this.view.downloadTransction.lblHeader.text=kony.i18n.getLocalizedString("i18n.accounts.viewStatments_Small");
    this.view.downloadTransction.lblPickDateRange.isVisible=true;
    this.view.downloadTransction.lblTo.isVisible=true;
    this.view.downloadTransction.flxFromDate.isVisible=true;
    this.view.downloadTransction.flxToDate.isVisible=true;
    this.view.ViewStatements.Segment0d986ba7141b544.setData([]);
    this.view.forceLayout();
    this.updateProgressBarState(true);
    this.presenter.fetchAccounts(this.initViewStatements);
  },
   /**
  * setMonthsData -  Months population in viw Statements flex basing on year, accountID
  * @member of {frmAccountDetailsController}
  * @params {}
  * @return {}
  * @throws {}
  * */
  setMonthsData: function(){
    var noOfMonths=0;
    var self=this;  
    var year=this.view.ViewStatements.lblSelectYear.selectedKey;
    var date=new Date();
    var month=date.getMonth();
    if(year==date.getFullYear()){
      noOfMonths=month;
    }
    else{
      noOfMonths=12;
    }
    var accountID=this.view.ViewStatements.lstSelectAccount.selectedKey; 
    var statementsWidgetDataMap={ "btnStatement1": "btnStatement1",
                                 "btnStatement2": "btnStatement2",
                                 "flxMonthStatements": "flxMonthStatements",
                                 "lblMonth1": "lblMonth1",
                                 "lblMonth2": "lblMonth2",
                                 "lblSeparator": "lblSeparator"
                                };
    var data=[];
    this.view.ViewStatements.Segment0d986ba7141b544.widgetDataMap = statementsWidgetDataMap;
    for(var i=noOfMonths; i>0;i--){
      var rightMonth=i-6;
      var month1=(this.fetchMonth(i)).toUpperCase();
      var month2=(this.fetchMonth(rightMonth)).toUpperCase();
      var list={
        "btnStatement1": {
          "text" : month1 + " "+year, 
          "onClick" : this.btnMonthAction.bind(this,accountID,year,i),
        },
        "btnStatement2":{
          "text" : month2+ " "+year,
          "isVisible" : rightMonth <= 0 ? false : true,
          "onClick" :  this.btnMonthAction.bind(this,accountID,year,rightMonth),
        },
        "flxMonthStatements": "flxMonthStatements",
        "lblMonth1": this.fetchMonth(i),
        "lblMonth2": {
          "text": this.fetchMonth(rightMonth),
          "isVisible" :  rightMonth<=0 ? false : true,
        },
        "lblSeparator": "lblSeparator"
       };
       data.push(list);
      if(rightMonth===1){
        i=0;
      }
    }
    this.view.ViewStatements.Segment0d986ba7141b544.setData(data);
    this.view.forceLayout();
  },
   /**
  * btnMonthAction - button action of monthly statement which triggers service to download the statement
  * @member of {frmAccountDetailsController}
  * @param {String} accountID - accountID of account clicked
  * @param {String} year - year 
  * @param {String} month - month 
  * @param {String} format - format of statement Ex: pdf or csv
  * @return {}
  * @throws {}
  */
  btnMonthAction: function(accountID,year,month){
    this.updateProgressBarState(true);
    this.presenter.showDownloadStatementScreen(accountID, year, month, this.format);
  },
   /**
  * fetchMonth - map function for months
  * @member of {frmAccountDetailsController}
  * @param {String} month - number that represent month
  * @returns corresponding month {String}
  * @throws {}
  */
  fetchMonth: function(month){
    switch(month){
      case 1:
        return OLBConstants.MONTHS_FULL.January;
      case 2:
        return OLBConstants.MONTHS_FULL.February;
      case 3:
        return OLBConstants.MONTHS_FULL.March;
      case 4:
        return OLBConstants.MONTHS_FULL.April;
      case 5:
        return OLBConstants.MONTHS_FULL.May; 
      case 6:
        return OLBConstants.MONTHS_FULL.June;  
      case 7:
        return OLBConstants.MONTHS_FULL.July;
      case 8:
        return OLBConstants.MONTHS_FULL.August; 
      case 9:
        return OLBConstants.MONTHS_FULL.September; 
      case 10:
        return OLBConstants.MONTHS_FULL.October; 
      case 11:
        return OLBConstants.MONTHS_FULL.November; 
      case 12:
        return OLBConstants.MONTHS_FULL.December;
      default:
        return "None";
    }
  },
  /**
  * showFormatScreen - On click of View Statements
  * @member of {frmAccountDetailsController}
  * @param {type} accountID - accountID of account clicked
  * @returns {}
  * @throws {}
  */
  showFormatScreen: function(accountID){
    var self=this;
    this.updateProgressBarState(true);
    this.enableButton(this.view.downloadTransction.btnDownload);
    this.view.flxHeader.isVisible=true;
    this.view.flxMainWrapper.isVisible=true;
    this.view.flxMain.isVisible=true;
    this.view.flxEditRule.isVisible=false;
    this.view.flxCheckImage.isVisible=false;
    //this.view.flxLoading.isVisible=false;
    this.view.flxLogout.isVisible=true;
    this.view.flxAccountTypesAndInfo.isVisible=true;
    this.view.flxAccountSummaryAndActions.isVisible=true;
    this.view.flxDisputedTransactionDetail.isVisible=false;
    this.view.flxTransactions.isVisible=true;
    this.view.flxFooter.isVisible=true;
    this.view.accountInfo.isVisible=false;
    this.view.accountTypes.isVisible=false;
    this.view.moreActions.isVisible=false;
    this.view.moreActionsDup.isVisible=false;
    this.view.breadcrumb.isVisible=true;
    this.view.flxskncontainer.isVisible=true;
    this.view.flxViewStatements.isVisible=false;
    this.view.flxDownloadTransaction.isVisible=true;
    this.view.downloadTransction.lblPickDateRange.isVisible = false;
    this.view.downloadTransction.lblTo.isVisible = false;
    this.view.downloadTransction.flxFromDate.isVisible = false;
    this.view.downloadTransction.flxToDate.isVisible = false;
    this.view.downloadTransction.lblSelectFormat.top =  "38dp";
    this.view.downloadTransction.lbxSelectFormat.top = "30dp"; 
    this.view.downloadTransction.setFocus(true);
    this.view.downloadTransction.lblHeader.text=kony.i18n.getLocalizedString("i18n.accounts.viewStatments_Small");
    this.view.downloadTransction.lblPickDateRange.isVisible=false;
    this.view.downloadTransction.lblTo.isVisible=false;
    this.view.downloadTransction.flxFromDate.isVisible=false;
    this.view.downloadTransction.flxToDate.isVisible=false;
    this.view.downloadTransction.lblPickDateRange.isVisible = false;
    this.view.downloadTransction.lblTo.isVisible = false;
    this.view.downloadTransction.flxFromDate.isVisible = false;
    this.view.downloadTransction.flxToDate.isVisible = false;
    this.view.downloadTransction.lblSelectFormat.top =  "38dp";
    this.view.downloadTransction.lbxSelectFormat.top = "30dp"; 
    this.view.forceLayout();
    this.accID=accountID;
    if(CommonUtilities.getConfiguration("eStatementsFormat") === "pdf,csv"){
      this.view.downloadTransction.lbxSelectFormat.masterData=[["lbl0","pdf"],["lbl1","csv"]];
    }
    else{
      this.view.downloadTransction.lbxSelectFormat.masterData=[["lbl0","pdf"]];
    }
    this.view.downloadTransction.btnDownload.onClick = self.showViewStatements;
    this.updateProgressBarState(false);
  },
   /**
  * downloadUrl - method that triggers download for the statement
  * @member of {frmAccountDetailsController}
  * @param {String} url - contains url of the statement
  * @params {}
  * @return {}
  * @throws {}
  */
  downloadUrl:function(url){
    var data={"url":url};
    CommonUtilities.downloadFile(data);
    this.updateProgressBarState(false);
  },
    /**
  * initViewStatements - initializing ui for view statements after fetching accounts list
  * @member of {frmAccountDetailsController}
  * @param {JSON Object} response - list of alla ccounts sligible for eStatements
  * @params {}
  * @return {}
  * @throws {}
  */
  initViewStatements: function(response){
    this.setYearsToListBox();
    this.updateListBox(response);
    this.view.ViewStatements.confirmButtons.btnModify.onClick=this.backToAccounts;
    this.view.ViewStatements.confirmButtons.btnConfirm.onClick=this.backToAccountDetails;
    this.setMonthsData();
    this.updateProgressBarState(false);
    this.view.forceLayout();
  },
    backToAccounts: function(){
      this.presenter.showAccountsDashboard();
    },
    /**
  * setYearsToListBox - populating years to listbox 
  * @member of {frmAccountDetailsController}
  * @params {}
  * @return {}
  * @throws {}
  */
  setYearsToListBox:function(){
    var date=new Date();
    var yy=date.getFullYear();
    var list = [];
    for ( var i = 0; i < 2; i++,yy--) {
      var tempList = [];
      tempList.push(yy);
      tempList.push(yy);
      list.push(tempList);
    }
    this.view.ViewStatements.lblSelectYear.masterData=list;
    this.view.ViewStatements.lblSelectYear.onSelection=this.setMonthsData;
  },
 /**
  * backToAccountDetails - onClick of button to navigate back to account details
  * @member of {frmAccountDetailsController}
  * @params {}
  * @return {}
  * @throws {}
  */
  backToAccountDetails:function(){
    this.updateProgressBarState(true);
    var selectedKey=this.view.ViewStatements.lstSelectAccount.selectedKey;
    this.showAccountDetailsScreen();
    this.presenter.fetchUpdatedAccountDetails(selectedKey);
  },
  /*
  * backToAccountDetailsCallback - Call back for backToAccountDetails
  * @member of {frmAccountDetailsController}
  * @param {JSON Object} - details of account 
  * @params {}
  * @return {}
  * @throws {}
  */
  backToAccountDetailsCallback:function(account){
    this.presenter.showAccountDetails(account);
    this.updateProgressBarState(false);
  },
   /*
  * showAccountDetailsScreen - function to set ui for account details screen
  * @member of {frmAccountDetailsController}
  * @params {}
  * @return {}
  * @throws {}
  */
  showAccountDetailsScreen: function() {
    var scopeObj = this;
    this.view.flxHeader.isVisible = true;
    this.view.flxMainWrapper.isVisible = true;
    this.view.flxMain.isVisible = true;
    this.view.flxEditRule.isVisible = false;
    this.view.flxCheckImage.isVisible = false;
    this.view.flxDownloadTransaction.isVisible = false;
    //this.view.flxLoading.isVisible = false;
    var text1=kony.i18n.getLocalizedString("i18n.topmenu.accounts");
    var text2=kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
    this.view.breadcrumb.setBreadcrumbData([{text:text1}, {text:text2,  callback:scopeObj.backToAccountDetails }])
    this.view.flxLogout.isVisible = true;
    this.view.flxAccountTypesAndInfo.isVisible = true;
    this.view.flxAccountSummaryAndActions.isVisible = true;
    this.view.flxDisputedTransactionDetail.isVisible = false;
    this.view.flxViewStatements.isVisible = false;
    this.view.flxTransactions.isVisible = true;
    this.view.flxFooter.isVisible = true;
    this.view.accountInfo.isVisible = false;
    this.view.accountTypes.isVisible = false;
    this.view.moreActions.isVisible = false;
    this.view.moreActionsDup.isVisible = false;
    this.view.breadcrumb.isVisible = true;
    this.view.flxskncontainer.isVisible = true;
    this.view.forceLayout();
  },
 /*
  * updateListBox - Updating accounts listbox with all the accounts
  * @member of {frmAccountDetailsController}
  * @param {JSON Object} - list of accounts  
  * @params {}
  * @return {}
  * @throws {}
  */
  updateListBox:function(fromAccounts){
    var accounts=[{}];
    for(var i=0,j=0;i<fromAccounts.length;i++)
      if(fromAccounts[i].eStatementEnable === "true"  && CommonUtilities.getConfiguration("enableEstatements") === "true"){
      	  accounts[j]=fromAccounts[i];
        j++;
      }
    this.view.ViewStatements.lstSelectAccount.masterData=this.showAccountsForSelection(accounts);
    this.view.ViewStatements.lstSelectAccount.selectedKey=this.accID;
    this.view.ViewStatements.lstSelectAccount.onSelection=this.onSelectionOfNewAccount;
    this.view.forceLayout();
  },
     /*
  * onSelectionOfNewAccount -On selection of new account from list box
  * @member of {frmAccountDetailsController}
  * @param {JSON Object} - details of account 
  * @params {}
  * @return {}
  * @throws {}
  */
    onSelectionOfNewAccount:function(){
      var date=new Date();
      var year=date.getFullYear();
      this.view.ViewStatements.lblSelectYear.selectedKey=year;
      this.setMonthsData();
    },
 /*
  * showAccountsForSelection - Call back for backToAccountDetails
  * @member of {frmAccountDetailsController}
  * @param {JSON Object} - details of account 
  * @params {}
  * @return {}
  * @throws {}
  */
  showAccountsForSelection: function(presentAccounts) {
    var list = [];
    for (i = 0; i < presentAccounts.length; i++) {
      var tempList = [];
      tempList.push(presentAccounts[i].accountID);
      var tempAccountNumber=presentAccounts[i].accountID;
      var name=CommonUtilities.getAccountDisplayName(presentAccounts[i]);
      tempList.push(name);
      list.push(tempList);
    }
    return list;
  }, 
  updatePaginationBar: function (transactionDetails) {
    if (transactionDetails.paginationString) {
      this.view.transactions.lblPagination.text = transactionDetails.paginationString + ' Transactions'; //TODO: replace with i18n lookup
	  this.view.transactions.flxPagination.isVisible=true; 
    } else {
 //     this.view.transactions.lblPagination.text = 'No Transactions'; //TODO: replace with i18n lookup
	  this.view.transactions.flxPagination.isVisible=false;
    }
    var frmObj = this;
    this.view.transactions.flxPaginationPrevious.onClick = function(){
      if(transactionDetails.canGoPrev()){
        frmObj.updateProgressBarState(true);
        transactionDetails.prevPage();
      }
    }
    this.view.transactions.flxPaginationNext.onClick = function(){
      if(transactionDetails.canGoNext()){
        frmObj.updateProgressBarState(true);
        transactionDetails.nextPage();
      }
    }
    this.view.transactions.imgPaginationFirst.isVisible = false;
    this.view.transactions.imgPaginationLast.isVisible = false;
    this.view.transactions.imgPaginationPrevious.src = transactionDetails.canGoPrev() ? 'pagination_back_active.png' : 'pagination_back_inactive.png';
    this.view.transactions.imgPaginationNext.src = transactionDetails.canGoNext() ? 'pagination_next_active.png' : 'pagination_next_inactive.png';
  },
  updateAccountList: function (accountModel) {
    var data = this.createAccountListSegmentsModel(accountModel);
    this.view.accountTypes.segAccountTypes.setData(data);
    this.view.forceLayout();
  },
  /**
   * Renders Transaction Search View Model
   * @param searchViewModel - Child Search View Model from Transactions
   */

  updateSearchTransactionView: function (searchViewModel) {
      this.view.transactions.flxSearch.onClick = searchViewModel.toggleSearchView;
      if (searchViewModel.visible && !searchViewModel.searchPerformed) {
        this.renderSearchForm(searchViewModel);
      }
      else if (searchViewModel.visible && searchViewModel.searchPerformed) {
        this.showSearchResults(searchViewModel);
      }
      else {
        this.view.transactions.setSearchVisible(false);
        this.view.transactions.setSearchResultsVisible(false);
      }
      this.view.forceLayout();
  },
  /**
   * Renders Search Form from Search View Model
   * 
   */
  renderSearchForm: function (searchViewModel) {
    this.view.transactions.setSearchVisible(true);
    this.view.transactions.setSearchResultsVisible(false);
    this.view.transactions.txtKeyword.text = searchViewModel.keyword;
    this.view.transactions.lstbxTransactionType.masterData = searchViewModel.transactionTypes;
    this.view.transactions.lstbxTransactionType.selectedKey = searchViewModel.transactionTypeSelected;
    this.view.transactions.lstbxTimePeriod.masterData = searchViewModel.timePeriods;
    this.view.transactions.lstbxTimePeriod.selectedKey = searchViewModel.timePeriodSelected;
    this.onTimePeriodChange();
    this.view.transactions.txtCheckNumberFrom.text = searchViewModel.fromCheckNumber;
    this.view.transactions.txtCheckNumberTo.text = searchViewModel.toCheckNumber;
    this.view.transactions.txtAmountRangeFrom.text = searchViewModel.fromAmount;
    this.view.transactions.txtAmountRangeTo.text = searchViewModel.toAmount;
    this.view.transactions.calDateFrom.date = searchViewModel.fromDate;
    this.view.transactions.calDateTo.date = searchViewModel.toDate;
    this.bindSearchFormActions(searchViewModel);
    this.checkSearchButtonState();
  },
  bindSearchFormActions: function (searchViewModel) {
    var scopeObj = this;
    this.view.transactions.btnSearch.onClick = function () {
      scopeObj.updateProgressBarState(true);
      searchViewModel.performSearch(this.updateSearchViewModel(searchViewModel));
    }.bind(this);
    this.view.transactions.btnCancel.onClick = searchViewModel.clearSearch;

    this.view.transactions.txtKeyword.onKeyUp = this.checkSearchButtonState.bind(this);
    this.view.transactions.lstbxTransactionType.onSelection = this.checkSearchButtonState.bind(this);
    this.view.transactions.txtCheckNumberFrom.onKeyUp = this.checkSearchButtonState.bind(this);
    this.view.transactions.lstbxTimePeriod.onSelection = this.onTimePeriodChange.bind(this);    
    this.view.transactions.txtCheckNumberTo.onKeyUp = this.checkSearchButtonState.bind(this);
    this.view.transactions.txtAmountRangeFrom.onKeyUp = this.checkSearchButtonState.bind(this);
    this.view.transactions.txtAmountRangeTo.onKeyUp = this.checkSearchButtonState.bind(this);
  },

  userCanSearch: function (formData) {
    function checkIfEmpty (value) {
      if (value === null || value === "") {
        return true;
      }
      return false;
    }
    if (!checkIfEmpty(formData.keyword)) {
      return true;
    }
    //for occu
   // else if (formData.transactionTypeSelected !== OLBConstants.BOTH) {
    else if (formData.transactionTypeSelected !== null) {
      return true;
    }
    //else if (formData.timePeriodSelected !== OLBConstants.ANY_DATE) {
    else if (formData.timePeriodSelected !== null) {
      return true;
    }
    else if (!checkIfEmpty(formData.fromCheckNumber) && !checkIfEmpty(formData.toCheckNumber)) {
      return true;
    }
    else if (!checkIfEmpty(formData.fromAmount) && !checkIfEmpty(formData.toAmount)) {
      return true;
    }
    else {
      return false;
    }
  },

  checkSearchButtonState: function () {
    var formData = this.updateSearchViewModel({});
    if (this.userCanSearch(formData)) {
      this.view.transactions.enableSearchButton();
    }
    else {
      this.view.transactions.disableSearchButton();
    }
  },

  showSearchResults: function (searchViewModel) {
    this.view.transactions.flxPagination.setVisibility(false);
    this.view.transactions.btnModifySearch.onClick =function () {
      searchViewModel.modifySearch(searchViewModel);
    };
    this.view.transactions.btnClearSearch.onClick = function () {
      searchViewModel.clearSearch(searchViewModel);
    };
    this.view.transactions.setSearchVisible(false);
    this.view.transactions.setSearchResultsVisible(true);
    this.configureActionsForTags(searchViewModel);    

  },

  showSearchTagsData: function (searchViewModel) {

  },
  tagState: {
    visible: 0,
    NUMBER_OF_TAGS: 5,
    decrement: function () {
      if (this.visible > 0)      
      this.visible--;
    },
    increment: function () {
      if (this.visible < this.NUMBER_OF_TAGS)
      this.visible++;
    }
  },
  configureActionsForTags : function (searchViewModel) {
    var scopeObj = this;
    var tagConfig = [
      {
        actionOn: 'flxCancelKeyword',
        hide: ['lblKeywordTitle', 'lblKeywordValue'],
        clearPropertiesFromViewModel: [
          {propertyName: 'keyword', resetValue: ''}
        ],
        value: {label: 'lblKeywordValue', computedValue: function () {
          if (searchViewModel.keyword === "") {
            return null;
          }
          return searchViewModel.keyword;
        }}
      },
      {
        actionOn: 'flxCancelType',
        hide: ['lblTypeValue', 'lblTypeTitle'],
        clearPropertiesFromViewModel: [
          {propertyName: 'transactionTypeSelected', resetValue: OLBConstants.BOTH}
        ],
        value: {label: 'lblTypeValue', computedValue: function () {
          if (searchViewModel.transactionTypeSelected === OLBConstants.BOTH) {
            return null;
          }
          return searchViewModel.getTransactionTypeFromKey(searchViewModel.transactionTypeSelected);
        }}
      },
      {
        actionOn: 'flxCancelAmountRange',
        hide: ['lblAmountRangeTitle', 'lblAmountRangeValue'],
        clearPropertiesFromViewModel: [
          {propertyName: 'fromAmount', resetValue: ''},
          {propertyName: 'toAmount', resetValue: ''}
        ],
        value: {label: 'lblAmountRangeValue', computedValue: function () {
          if (searchViewModel.fromAmount === "" || searchViewModel.toAmount === "") {
            return null;
          }
          return searchViewModel.fromAmount + " to " + searchViewModel.toAmount;
        }}
      },
      {
        actionOn: 'flxCancelDateRange',
        hide: ['lblDateRangeTitle','lblDateRangeValue'],
        clearPropertiesFromViewModel: [
          {propertyName: 'timePeriodSelected', resetValue: OLBConstants.ANY_DATE}
        ],
        value: {label: 'lblDateRangeValue', computedValue: function () {
          if (searchViewModel.timePeriodSelected === OLBConstants.ANY_DATE) {
            return null;
          }
          else if(searchViewModel.timePeriodSelected === OLBConstants.CUSTOM_DATE_RANGE) {
            return searchViewModel.fromDate + " " +  kony.i18n.getLocalizedString('i18n.common.to') + " " +  searchViewModel.toDate;
          }
          return searchViewModel.getTimePeriodFromKey(searchViewModel.timePeriodSelected);
        }}
      },
      {
        actionOn: 'flxCancelCheckNumber',
        hide: ['lblCheckNumberTitle','lblCheckNumberValue'],
        clearPropertiesFromViewModel: [
          {propertyName: 'fromCheckNumber', resetValue: ''},
          {propertyName: 'toCheckNumber', resetValue: ''}
        ],
        value: {label: 'lblCheckNumberValue', computedValue: function () {
          if (searchViewModel.fromCheckNumber === "" || searchViewModel.toCheckNumber === "") {
            return null;
          }
          return searchViewModel.fromCheckNumber + " " +  kony.i18n.getLocalizedString('i18n.common.to') + " " + searchViewModel.toCheckNumber;
        }}
      }
    ];

    function generateClickListenerForTag (config) {
      return function () {
        hideTag(config);
         scopeObj.tagState.decrement();            
        config.clearPropertiesFromViewModel.forEach(function (property) {
          searchViewModel[property.propertyName] = property.resetValue;
        });
        scopeObj.updateProgressBarState(true);
        if (scopeObj.tagState.visible === 0) {
          searchViewModel.clearSearch(searchViewModel);
        }
        else {
          searchViewModel.performSearch(searchViewModel);        
        }
        scopeObj.view.transactions.forceLayout();
      };
    }

    function hideTag (config) {
      scopeObj.view.transactions[config.actionOn].setVisibility(false);
      config.hide.forEach(function (widgetToHide) {
        scopeObj.view.transactions[widgetToHide].setVisibility(false);
      });
      scopeObj.view.transactions.forceLayout();
    }

    function showTag (config) {
      scopeObj.view.transactions[config.actionOn].setVisibility(true);
      config.hide.forEach(function (widgetToHide) {
        scopeObj.view.transactions[widgetToHide].setVisibility(true);
      });
      scopeObj.view.transactions.forceLayout();  
      scopeObj.tagState.increment();    
    }

    this.tagState.visible = 0;    
    tagConfig.forEach(function (config) {
      if (config.value.computedValue() === null){
        hideTag(config);
      }
      else {
        showTag(config);
        scopeObj.view.transactions[config.actionOn].onClick = generateClickListenerForTag(config);
        scopeObj.view.transactions[config.value.label].text = config.value.computedValue();
      }
    });



  },
  showTransactions: function (transactionsResults) {
    var segmentData = transactionsResults.map(this.createTransactionSegmentModel);
    this.view.transactions.segTransactions.setData(segmentData);
  }, 

  updateSearchViewModel: function (searchViewModel) {
    searchViewModel.keyword = this.view.transactions.txtKeyword.text.trim();
    searchViewModel.transactionTypeSelected  = this.view.transactions.lstbxTransactionType.selectedKey;
    searchViewModel.timePeriodSelected = this.view.transactions.lstbxTimePeriod.selectedKey;
    searchViewModel.fromCheckNumber = this.view.transactions.txtCheckNumberFrom.text.trim();
    searchViewModel.toCheckNumber = this.view.transactions.txtCheckNumberTo.text.trim();
    searchViewModel.fromAmount = this.view.transactions.txtAmountRangeFrom.text.trim();
    searchViewModel.toAmount = this.view.transactions.txtAmountRangeTo.text.trim();
    searchViewModel.fromDate = this.view.transactions.calDateFrom.date;
    searchViewModel.toDate = this.view.transactions.calDateTo.date;
    return searchViewModel;
  },

  onTimePeriodChange: function () {
    if (this.view.transactions.lstbxTimePeriod.selectedKey === OLBConstants.CUSTOM_DATE_RANGE) {
      this.view.transactions.showByDateWidgets();
    }
    else {
      this.view.transactions.hideByDateWidgets();      
    }
    this.checkSearchButtonState();
  },

  ACCOUNT_LIST_NAME_MAX_LENGTH: OLBConstants.ACCOUNT_LIST_NAME_MAX_LENGTH,
  formatAccountName: function (accountName) {
    if (accountName.length <= this.ACCOUNT_LIST_NAME_MAX_LENGTH) {
      return accountName;
    } else {
      return accountName.substring(0, this.ACCOUNT_LIST_NAME_MAX_LENGTH) + '...';
    }
  },
  createAccountListSegmentsModel: function (accounts) {
    var myAccountsMainSegData = [];
    var myAccountsrowData = [];
    var accountNumhdval = "";
    if(accounts !== null && accounts.length > 0)
    {
		for(var i=0; i<accounts.length; i++){
					var account = accounts[i];
                    var accountobj = { "lblUsers": {
                          "text" : account.accountshareID,
                          "toolTip" : account.accountshareID
                        },
                        "lblSeparator": "Separator",
                        "flxAccountTypes": {
                        "onClick": account.onAccountSelection
                        }
                    }
                   var tempaccountheading = account.accountOwner;
                    if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
                        var accountHeader = {
                            "lblHeader": accountNumhdval
                        };
                        myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
                        myAccountsrowData = [];
                        myAccountsrowData.push(accountobj);
                    } else {
                        myAccountsrowData.push(accountobj);
                    }
                    accountNumhdval =account.accountOwner;
                    if (i === accounts.length - 1) {
                        var accountfinHeader = {
                            "lblHeader": accountNumhdval
                        };
                        myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
                        kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
                    }
         }
    }
    
    return myAccountsMainSegData;
  },
  /**
   * Init form actions 
   */
  initFrmAccountDetails: function(){
    var scopeObj = this;

    //External Accounts
    scopeObj.transactionsSortMap = [
      { name : 'transactionDate' ,  imageFlx : scopeObj.view.transactions.imgSortDate , clickContainer : scopeObj.view.transactions.flxSortDate},
      //{ name : 'description' ,  imageFlx : scopeObj.view.transactions.imgSortDescription, clickContainer : scopeObj.view.transactions.flxSortDescription},
      { name : 'amount' ,  imageFlx : scopeObj.view.transactions.imgSortAmount, clickContainer : scopeObj.view.transactions.flxSortAmount }
      //{ name : 'fromAccountBalance' ,  imageFlx : scopeObj.view.transactions.imgSortBalance, clickContainer : scopeObj.view.transactions.flxSortBalance}      
    ];
    scopeObj.view.transactions.imgSortType.setVisibility(false);
    scopeObj.view.transactions.imgSortDescription.setVisibility(false);
    scopeObj.view.transactions.imgSortBalance.setVisibility(false);
    CommonUtilities.Sorting.setSortingHandlers(scopeObj.transactionsSortMap, scopeObj.onTransactionsSortClickHandler, scopeObj);
    scopeObj.view.imgCloseDowntimeWarning.onTouchEnd = function(){
      scopeObj.setServerError(false);
    };
  },
  /**
   * On Transactions Sort click handler.
   * @Input : event {object} event object.
   * @Input : data {object} column details.
   *  
   */
  onTransactionsSortClickHandler: function (event, data) {
    var scopeObj = this;
    scopeObj.presenter.Transactions.showTransactionsByType(data);
  },

  preshowFrmAccountDetails: function () {
    var scopeObj = this;
    this.view.customheader.forceCloseHamburger();
    this.view.accountInfo.isVisible = false;
    this.view.accountTypes.isVisible = false;
    this.view.moreActions.isVisible = false;
    this.view.flxEditRule.isVisible = false;
    this.view.forceLayout();
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    /// Start
    this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
    this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtmffffffOccu";
    this.view.customheader.topmenu.imgTransfers.src = "sendmoney.png";
    this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.lblFeedback.skin = "sknLblGtmffffffOccu";
    this.view.customheader.topmenu.imgFeedback.src = "feedback.png";
    //this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    /// End
    if(CommonUtilities.getConfiguration("printingTransactionDetails")==="true"){
      this.view.transactions.imgPrintIcon.setVisibility(true);
      this.view.transactions.imgPrintIcon.onTouchStart = this.onClickPrint;
    }
    else this.view.transactions.imgPrintIcon.setVisibility(false);
    this.view.forceLayout();

    this.view.breadcrumb.setBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
    }, {
      text: kony.i18n.getLocalizedString("i18n.accounts.accountDetails")
    }]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.accounts.accountsTitle");
	this.view.breadcrumb.lblBreadcrumb2.toolTip= kony.i18n.getLocalizedString("i18n.accounts.accountDetails");
    
    //SET ACTIONS:
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainWrapper.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CheckImage.flxImgCancel.onClick = function() {
      scopeObj.view.flxCheckImage.setVisibility(false);
    };
    this.view.CheckImage.flxFlip.onClick = function() {
      if (scopeObj.view.CheckImage.imgCheckImage.src === "check_img.png") scopeObj.view.CheckImage.imgCheckImage.src = "check_back.png";
      else scopeObj.view.CheckImage.imgCheckImage.src = "check_img.png";
    };
    this.view.flxCheckImage.setVisibility(false);
    this.view.CustomPopup.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";

    };
    this.view.editRule.btnCancel.onClick = function () {
      scopeObj.view.flxEditRule.isVisible = false;
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.flxAccountTypes.onClick = function () {
      kony.print("flxAccountTypes clicked");
      scopeObj.showAccountTypes();
    };
    this.view.imgAccountTypes.onTouchStart = function () {
      kony.print("imgAccountTypes onTouchStart");
      scopeObj.showAccountTypes();
    };    
    this.view.btnViewAccountInfo.onClick = function () {
      scopeObj.showAccountInfo();
    };
    this.view.flxSecondaryActions.onClick = function () {
      scopeObj.showMoreActions();
    };
    this.view.accountSummary.btnAccountSummary.onClick = function () {
      scopeObj.showAccountSummary();
    };
    this.view.accountSummary.btnBalanceDetails.onClick = function () {
      scopeObj.showBalanceDetails();
    };
    this.view.transactions.flxDownload.onClick = function () {
      scopeObj.view.flxDownloadTransaction.isVisible = true;
    };

    this.view.downloadTransction.imgClose.onTouchEnd = function () {
      scopeObj.view.flxDownloadTransaction.isVisible = false;
    };
    this.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.onClick = function(){
      scopeObj.noOfClonedChecks++;
      var newCheck=scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.clone(scopeObj.noOfClonedChecks);
      scopeObj.view.StopCheckPaymentSeriesMultiple.flxStopCheckPayments.addAt(newCheck,3);
    };
    this.view.downloadTransction.btnCancel.onClick = function () {
      scopeObj.view.flxDownloadTransaction.isVisible = false;
      scopeObj.presenter.fetchUpdatedAccountDetails(scopeObj.accID);
      scopeObj.showAccountDetailsScreen();
    };
  },
  postShowFrmAccountDetails: function () {
    this.setfootertop();
    
    var accountListX = this.view.accountSummary.frame.x;
    this.view.accountTypes.left = accountListX + "dp";
    var secondaryActionsX = this.view.flxActions.frame.x;
    var secondaryActionsPadding = this.view.flxSecondaryActions.frame.x;
    this.view.moreActions.left = secondaryActionsX + secondaryActionsPadding + "dp";
	this.view.moreActions.right = 89 + this.view.flxMain.frame.x + "dp";
    this.view.moreActions.left = "";
    this.view.accountInfo.left = 350 + this.view.flxMain.frame.x + "dp";
    this.view.accountTypes.left = 89 + this.view.flxMain.frame.x + "dp";

    var scopeObj = this;
    var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainWrapper.frame.height;
    scopeObj.view.editRule.height = height + "dp";
    scopeObj.view.flxEditRule.height = height + "dp";
    this.view.flxDownloadTransaction.height = height + "dp";
    this.AdjustScreen();
    //this.setFlowActions();
  },
     //UI Code
  AdjustScreen: function() {
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxMainWrapper.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff + "dp";
        else
            this.view.flxFooter.top = mainheight + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + "dp";
     }
    this.view.forceLayout();
  }, 
  setFlowActions: function() {
     var scopeObj = this;
     this.view.btnViewDisputedTransactions.onClick = this.DisputedTransactionTabAction;
     this.view.btnViewDisputedChecks.onClick = this.DisputedChecksTabAction;
     this.view.MyRequestsTabs.btnAddNewStopCheckPayments.onClick = this.showStopCheckPaymentsSeriesMultiple;
     this.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.onClick = function() {
        if (scopeObj.view.oneTimeTransfer.imgRadioBtn1.src === "radiobtn_active_small.png") {
			scopeObj.view.oneTimeTransfer.imgRadioBtn2.src = "radiobtn_active_small.png";
			scopeObj.view.oneTimeTransfer.imgRadioBtn1.src = "icon_radiobtn.png";
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(false);
            scopeObj.view.forceLayout();
		} else {
			scopeObj.view.oneTimeTransfer.imgRadioBtn2.src = "icon_radiobtn.png";
			scopeObj.view.oneTimeTransfer.imgRadioBtn1.src = "radiobtn_active_small.png";
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(true);
            scopeObj.view.forceLayout();
		}
     };
     this.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.onClick = function() {
        if (scopeObj.view.oneTimeTransfer.imgRadioBtn2.src === "radiobtn_active_small.png") {
			scopeObj.view.oneTimeTransfer.imgRadioBtn1.src = "radiobtn_active_small.png";
			scopeObj.view.oneTimeTransfer.imgRadioBtn2.src = "icon_radiobtn.png";
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(true);
            scopeObj.view.forceLayout();
		} else {
			scopeObj.view.oneTimeTransfer.imgRadioBtn1.src = "icon_radiobtn.png";
			scopeObj.view.oneTimeTransfer.imgRadioBtn2.src = "radiobtn_active_small.png";
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(false);
            scopeObj.view.forceLayout();
		}
     };
    this.view.StopCheckPaymentSeriesMultiple.flxTCContentsCheckbox.onClick = function()
    {
       var flag = scopeObj.toggleCheckBox(scopeObj.view.StopCheckPaymentSeriesMultiple.imgTCContentsCheckbox);
       if(flag === 0){
         scopeObj.disableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
       }
      else{
         scopeObj.enableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
      }
    };
    this.view.StopCheckPaymentSeriesMultiple.btnProceed.onClick = function() {
        var nav = new kony.mvc.Navigation("frmConfirm");
	    nav.navigate();
    };
    this.view.StopCheckPaymentSeriesMultiple.btnCancel.onClick = function() {
        // Goes To previous page
    };
    this.view.CancelStopCheckPayments.flxCross.onClick = function(){
       scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
    };
    this.view.CancelStopCheckPayments.btnNo.onClick = function() {
       scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
    };
    
    //UI Related Code
    this.view.flxChart.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flexCategorizedMonthlySpending.setVisibility(true);
       scopeObj.setBreadCrumbData();
       scopeObj.view.CategorizedMonthlySpending.CommonHeader.btnRequest.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.flxDonutChart.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flexCategorizedMonthlySpending.setVisibility(true);
       scopeObj.setBreadCrumbData();
       scopeObj.view.CategorizedMonthlySpending.CommonHeader.btnRequest.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.CommonHeader.btnRequest.onClick = function() {   
       scopeObj.hideAll();
       scopeObj.view.flxUncategorizedTransactions.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.text = "BULK UPDATE";
       scopeObj.view.TransactionsUnCategorized.flxSort.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxPagination.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSeparatorSort.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxButtons.setVisibility(false);
       scopeObj.setBreadCrumbData();
       scopeObj.setTransactionData();
       scopeObj.view.forceLayout();
    };
    this.view.TransactionsUnCategorized.CommonHeader.btnRequest.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flxUncategorizedTransactions.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSort.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxPagination.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSeparatorSort.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.flxButtons.setVisibility(true);
       scopeObj.setBulkUpdateTransactionData();
       scopeObj.view.forceLayout();
    };
    this.view.TransactionsUnCategorized.btnCancel.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flxUncategorizedTransactions.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.text = "BULK UPDATE";
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.toolTip = "BULK UPDATE";
       scopeObj.view.TransactionsUnCategorized.flxSort.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxPagination.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSeparatorSort.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxButtons.setVisibility(false);
       scopeObj.setTransactionData();
       scopeObj.view.forceLayout();
    };
    this.view.TransactionsUnCategorized.btnDownload.onClick = function() {
       scopeObj.view.flxPFMAssignCategory.setVisibility(true);
       scopeObj.view.forceLayout();
    };
    this.view.AssignCategory.flxCross.onClick = function() {
       scopeObj.view.flxPFMAssignCategory.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.AssignCategory.btnCancel.onClick = function() {
       scopeObj.view.flxPFMAssignCategory.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.AssignCategory.btnDownload.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flxPFMContainers.setVisibility(true);
       scopeObj.view.flxPFMAssignCategory.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.TransactionsUnCategorized.flxCheckbox.onClick = function() {
       if(scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src === "checked_box.png") {
        scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "unchecked_box.png"; 
        var data = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.data;
        for(i=0;i<data.length;i++)
         {
           data[i].imgCheckBox = "unchecked_box.png";
         }  
      	kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.setData(data);   
       }
       else {
        var data1 = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.data;
        scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "checked_box.png"; 
         for(i=0;i<data1.length;i++)
         {
           data1[i].imgCheckBox = "checked_box.png";
         }  
      	kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.setData(data1);   
       }
    };
    this.view.AllForms.flxCross.onClick = function() {
       scopeObj.view.AllForms.setVisibility(false);
    };
    this.view.accountSummary.flxInfo.onClick = function() {
      if(scopeObj.view.AllForms.isVisible === false)
         scopeObj.view.AllForms.setVisibility(true);
      else
         scopeObj.view.AllForms.setVisibility(false);
    };
  },
  setBreadCrumbData: function() {
       this.view.breadcrumb.btnBreadcrumb1.text = kony.i18n.getLocalizedString("i18n.accounts.accountsTitle");
       this.view.breadcrumb.lblBreadcrumb2.setVisibility(false);
       this.view.breadcrumb.btnBreadcrumb2.setVisibility(true);
       this.view.breadcrumb.btnBreadcrumb2.text = kony.i18n.getLocalizedString("i18n.accounts.PersonalFinanceManagement");
       this.view.breadcrumb.btnBreadcrumb2.skin = "sknBtnLato3343A813PxBg0";
       this.view.breadcrumb.lblBreadcrumb3.text = kony.i18n.getLocalizedString("i18n.accounts.categorizedMonthlySpending");
       this.view.breadcrumb.lblBreadcrumb3.setVisibility(true);  
       this.view.breadcrumb.imgBreadcrumb2.setVisibility(true);
  },  
  //UI Segment Data 
  setTransactionData: function() {
     var TransactionData = {
    "flxAmount": "flxAmount",
    "flxCategory": "flxCategory",
    "flxDate": "flxDate",
    "flxDescription": "flxDescription",
    "flxDetail": "flxDetail",
    "flxDetailData": "flxDetailData",
    "flxDetailHeader": "flxDetailHeader",
    "flxDropdown": "flxDropdown",
    "flxFromAccount": "flxFromAccount",
    "flxFromAccountData": "flxFromAccountData",
    "flxIdentifier": "flxIdentifier",
    "flxInformation": "flxInformation",
    "flxLeft": "flxLeft",
    "flxPFMUnCategorizedTransactionSelected": "flxPFMUnCategorizedTransactionSelected",
    "flxPFMUnCategorizedTransactionsSelected": "flxPFMUnCategorizedTransactionsSelected",
    "flxRight": "flxRight",
    "flxSegDisputedTransactionRowWrapper": "flxSegDisputedTransactionRowWrapper",
    "flxTo": "flxTo",
    "flxToAccount": "flxToAccount",
    "flxToAccountData": "flxToAccountData",
    "flxWrapper": "flxWrapper",
    "imgCategoryDropdown": "imgCategoryDropdown",
    "imgDropdown": "imgDropdown",
    "lblAmount": "lblAmount",
    "lblCategory": "lblCategory",
    "lblDate": "lblDate",
    "lblDescription": "lblDescription",
    "lblFromAccount": "lblFromAccount",
    "lblFromAccountData": "lblFromAccountData",
    "lblIdentifier": "lblIdentifier",
    "lblSeparator": "lblSeparator",
    "lblSeparator2": "lblSeparator2",
    "lblTo": "lblTo",
    "lblToAccount": "lblToAccount",
    "lblToAccountData": "lblToAccountData"
	};
	var data = [{
    "imgCategoryDropdown": "arrow_down.png",
    "imgDropdown": "arrow_down.png",
    "lblAmount": "$100000.00",
    "lblCategory": "Select Category",
    "lblDate": "06/05/2017",
    "lblDescription": "Time Warner Cable On-Line",
    "lblFromAccount": "From Account",
    "lblFromAccountData": "Personal Checking ...1234",
    "lblIdentifier": 
      {
        "skin" : "sknflx4a902"
      },
    "lblSeparator": "lblSeparator",
    "lblSeparator2": "lblSeparator2",
    "lblTo": "ABC. Inc Direct Deposit",
    "lblToAccount": "To Account",
    "lblToAccountData": "Warner Data",
    "template" : "flxPFMUnCategorizedTransactions"
	}, {
		"imgCategoryDropdown": "arrow_down.png",
    "imgDropdown": "arrow_down.png",
    "lblAmount": "$100000.00",
    "lblCategory": "Select Category",
    "lblDate": "06/05/2017",
    "lblDescription": "Time Warner Cable On-Line",
    "lblFromAccount": "From Account",
    "lblFromAccountData": "Personal Checking ...1234",
    "lblIdentifier": 
      {
        "skin" : "sknflx4a902"
      },
    "lblSeparator": "lblSeparator",
    "lblSeparator2": "lblSeparator2",
    "lblTo": "ABC. Inc Direct Deposit",
    "lblToAccount": "To Account",
    "lblToAccountData": "Warner Data",
    "template" : "flxPFMUnCategorizedTransactionsSelected"
	},
     {
		"imgCategoryDropdown": "arrow_down.png",
    "imgDropdown": "arrow_down.png",
    "lblAmount": "$100000.00",
    "lblCategory": "Select Category",
    "lblDate": "06/05/2017",
    "lblDescription": "Time Warner Cable On-Line",
    "lblFromAccount": "From Account",
    "lblFromAccountData": "Personal Checking ...1234",
    "lblIdentifier": 
      {
        "skin" : "sknflx4a902"
      },
    "lblSeparator": "lblSeparator",
    "lblSeparator2": "lblSeparator2",
    "lblTo": "ABC. Inc Direct Deposit",
    "lblToAccount": "To Account",
    "lblToAccountData": "Warner Data",
    "template" : "flxPFMUnCategorizedTransactions"
	},            
   ];
	this.view.TransactionsUnCategorized.segTransactions.widgetDataMap = TransactionData;
	this.view.TransactionsUnCategorized.segTransactions.setData(data);
	this.view.forceLayout();
  },  
  setBulkUpdateTransactionData: function() {
      var BulkTransactionData = {
    "flxAmount": "flxAmount",
    "flxCheckbox": "flxCheckbox",
    "flxDate": "flxDate",
    "flxDescription": "flxDescription",
    "flxDetail": "flxDetail",
    "flxDetailData": "flxDetailData",
    "flxDetailHeader": "flxDetailHeader",
    "flxDropdown": "flxDropdown",
    "flxFromAccount": "flxFromAccount",
    "flxFromAccountData": "flxFromAccountData",
    "flxIdentifier": "flxIdentifier",
    "flxInformation": "flxInformation",
    "flxLeft": "flxLeft",
    "flxPFMBulkUpdateTransactions": "flxPFMBulkUpdateTransactions",
    "flxPFMBulkUpdateTransactionsSelected": "flxPFMBulkUpdateTransactionsSelected",
    "flxRight": "flxRight",
    "flxSegDisputedTransactionRowWrapper": "flxSegDisputedTransactionRowWrapper",
    "flxTo": "flxTo",
    "flxToAccount": "flxToAccount",
    "flxToAccountData": "flxToAccountData",
    "flxWrapper": "flxWrapper",
    "imgCheckBox": "imgCheckBox",
    "imgDropdown": "imgDropdown",
    "lblAmount": "lblAmount",
    "lblDate": "lblDate",
    "lblDescription": "lblDescription",
    "lblFromAccount": "lblFromAccount",
    "lblFromAccountData": "lblFromAccountData",
    "lblIdentifier": "lblIdentifier",
    "lblSeparator": "lblSeparator",
    "lblSeparator2": "lblSeparator2",
    "lblTo": "lblTo",
    "lblToAccount": "lblToAccount",
    "lblToAccountData": "lblToAccountData"
	};
	var data = [{
    "imgCheckBox": "unchecked_box.png",
    "imgDropdown": "arrow_down.png",
    "lblAmount": "$10000.00",
    "lblDate": "06/05/2017",
    "lblDescription": "Time Warner Cable On-Line",
    "lblFromAccount": "From Account",
    "lblFromAccountData": "Personal Checking ...1234",
    "lblIdentifier": 
      {
        "skin" : "sknflx4a902"
      },
    "lblSeparator": "lblSeparator",
    "lblSeparator2": "lblSeparator2",
    "lblTo": "ABC. Inc Direct Deposit",
    "lblToAccount": "To Account",
    "lblToAccountData": "Warner Data",
    "template" : "flxPFMBulkUpdateTransaction"
	}, {
		"imgCheckBox": "unchecked_box.png",
    "imgDropdown": "arrow_down.png",
    "lblAmount": "$10000.00",
    "lblDate": "06/05/2017",
    "lblDescription": "Time Warner Cable On-Line",
    "lblFromAccount": "From Account",
    "lblFromAccountData": "Personal Checking ...1234",
    "lblIdentifier": 
      {
        "skin" : "sknflx4a902"
      },
    "lblSeparator": "lblSeparator",
    "lblSeparator2": "lblSeparator2",
    "lblTo": "ABC. Inc Direct Deposit",
    "lblToAccount": "To Account",
    "lblToAccountData": "Warner Data",
    "template" : "flxPFMBulkUpdateTransactionsSelected"
	},
     {
		"imgCheckBox": "unchecked_box.png",
    "imgDropdown": "arrow_down.png",
    "lblAmount": "$10000.00",
    "lblDate": "06/05/2017",
    "lblDescription": "Time Warner Cable On-Line",
    "lblFromAccount": "From Account",
    "lblFromAccountData": "Personal Checking ...1234",
    "lblIdentifier": 
      {
        "skin" : "sknflx4a902"
      },
    "lblSeparator": "lblSeparator",
    "lblSeparator2": "lblSeparator2",
    "lblTo": "ABC. Inc Direct Deposit",
    "lblToAccount": "To Account",
    "lblToAccountData": "Warner Data",
    "template" : "flxPFMBulkUpdateTransaction"
	},            
   ];
	this.view.TransactionsUnCategorized.segTransactions.widgetDataMap = BulkTransactionData;
	this.view.TransactionsUnCategorized.segTransactions.setData(data);
	this.view.forceLayout();
  }, 
  toggleCheckBox: function(imgCheckBox) {
            if (imgCheckBox.src == "unchecked_box.png") {
                imgCheckBox.src = "checked_box.png";
                return 1;
            } else {
                imgCheckBox.src = "unchecked_box.png";
                return 0;
            }
        },
  /**
   *  Disable button.
   */
  disableButton: function(button) {
      button.setEnabled(false);
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
  },
  /**
   * Enable button.
   */
  enableButton: function(button) {
      button.setEnabled(true);
      button.skin = "sknbtnLatoffffff15px";
      button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
  },
  DisputedTransactionTabAction: function() {
     this.view.MyRequestsTabs.btnDisputedTrnsactions.skin = "sknbtnffffffNoBorder";
	 this.view.MyRequestsTabs.btnDisputedChecks.skin = "sknbtnfbfbfbBottomBordere3e3e3";
     this.ShowAllSeperators();
     this.view.MyRequestsTabs.flxTabsSeperator3.setVisibility(false);
     this.view.MyRequestsTabs.flxSort.setVisibility(true);
     this.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(false);
     this.view.forceLayout();
  },
  DisputedChecksTabAction: function() {
     this.view.MyRequestsTabs.btnDisputedTrnsactions.skin = "sknbtnfbfbfbBottomBordere3e3e3";
	 this.view.MyRequestsTabs.btnDisputedChecks.skin = "sknbtnffffffNoBorder";
     this.ShowAllSeperators();
     this.view.MyRequestsTabs.flxTabsSeperator1.setVisibility(false);
     this.view.MyRequestsTabs.flxSortDisputedChecks.setVisibility(true);
     this.view.MyRequestsTabs.flxSort.setVisibility(false);
     this.view.forceLayout();
  },
  ShowAllSeperators: function() {
     this.view.MyRequestsTabs.flxTabsSeperator1.setVisibility(true);
     this.view.MyRequestsTabs.flxTabsSeperator2.setVisibility(true);
     this.view.MyRequestsTabs.flxTabsSeperator3.setVisibility(true);
     this.view.forceLayout();
  },
  showDisputeTransactionDetail: function() {
     this.hideAll();
     this.view.flxDisputedTransactionDetail.setVisibility(true);
     this.view.flxActionsDisputeTransactionDetails.setVisibility(true);
     this.view.flxDisputedTransactionDetails.setVisibility(true);
     this.view.DisputeTransactionDetail.setVisibility(true);
     this.view.StopCheckPaymentSeriesMultiple.setVisibility(false);
     this.view.forceLayout();
  },
  showStopCheckPaymentsSeriesMultiple: function() {
     this.hideAll();
     this.view.flxDisputedTransactionDetail.setVisibility(true);
     this.view.flxActionsDisputeTransactionDetails.setVisibility(true);
     this.view.flxDisputedTransactionDetails.setVisibility(true);
     this.view.DisputeTransactionDetail.setVisibility(false);
     this.view.StopCheckPaymentSeriesMultiple.setVisibility(true);
     this.view.forceLayout();
  },
  hideAll: function() {
     this.view.flxAccountTypesAndInfo.setVisibility(false);
     this.view.flxAccountSummaryAndActions.setVisibility(false);
     this.view.flxDisputedTransactionDetail.setVisibility(false);
     this.view.flxMyRequestsTabs.setVisibility(false);
     this.view.flxViewStatements.setVisibility(false);
     this.view.flxTransactions.setVisibility(false);
     this.view.flexCategorizedMonthlySpending.setVisibility(false);
     this.view.flxPFMContainers.setVisibility(false);
     this.view.flxUncategorizedTransactions.setVisibility(false);
     this.view.forceLayout();
  },
 
  onClickPrint : function(){
    this.accountDetailsModel.accountDisplayName = this.view.lblAccountTypes.text;
    this.presenter.showPrintPage({
      transactions : this.view.transactions.segTransactions.data,
      accountDetailsModel : this.accountDetailsModel
    });
  },
  onClickPrintRow : function(){
    var index = this.view.transactions.segTransactions.selectedIndex[1];
    var data = this.view.transactions.segTransactions.data[0][1][index];
    this.presenter.showTransferPrintPage({
      transactionRowData : data,
      accountDisplayName : this.view.lblAccountTypes.text
    });
  },
  showAccountInfo: function () {
    if (this.view.accountInfo.isVisible == false) {
      this.view.accountInfo.isVisible = true;
    } else {
      this.view.accountInfo.isVisible = false;
    }
  },
  showAccountSummary: function () {
    this.view.accountSummary.flxSummaryDesktop.isVisible = true;
    this.view.accountSummary.flxBalanceDetailDesktop.isVisible = false;
    this.view.accountSummary.btnAccountSummary.skin = "sknBtnAccountSummarySelected";
    this.view.accountSummary.btnBalanceDetails.skin = "sknBtnAccountSummaryUnselected";
    this.view.accountSummary.btnBalanceDetails.hoverSkin = "sknhoverf0f0f0";
  },
  setfootertop: function()
  {
     var ftop = this.view.flxMainWrapper.height - this.view.flxskncontainer.height;
     this.view.flxskncontainer.top = ftop + "dp";
  },
  showBalanceDetails: function () {
    this.view.accountSummary.flxSummaryDesktop.isVisible = false;
    this.view.accountSummary.flxBalanceDetailDesktop.isVisible = true;
    this.view.accountSummary.btnAccountSummary.skin = "sknBtnAccountSummaryUnselected";
    this.view.accountSummary.btnAccountSummary.hoverSkin = "sknhoverf0f0f0";
    this.view.accountSummary.btnBalanceDetails.skin = "sknBtnAccountSummarySelected";
  },
  showAccountTypes: function () {

    if (this.view.accountTypes.isVisible == false) {
      this.view.imgAccountTypes.src = "arrow_up.png";
      this.view.accountTypes.isVisible = true;
    } else {
      this.view.imgAccountTypes.src = "arrow_down.png";
      this.view.accountTypes.isVisible = false;
    }
  },
  showMoreActions: function () {
    if (this.view.moreActions.isVisible == false) {
      this.view.imgSecondaryActions.src = "arrow_up.png";
      this.view.moreActions.isVisible = true;
      if(this.view.flxPrimaryActions.frame.height < 152)
        {
          this.view.moreActions.top = "300dp";
        }
    } else {
      this.view.imgSecondaryActions.src = "arrow_down.png";
      this.view.moreActions.isVisible = false;
    }
  },
  transactionsSegmentRowClick: function () {
    var index = this.view.transactions.segTransactions.selectedIndex;
    var sectionIndex = index[0];
    var rowIndex = index[1];
    var data = this.view.transactions.segTransactions.data;
    kony.print("index:" + index);
    if (data[sectionIndex][1][rowIndex].template == "flxSegTransactionRowSavings") {
      data[sectionIndex][1][rowIndex].imgDropdown = "chevron_up.png";
      data[sectionIndex][1][rowIndex].template = "flxSegTransactionRowSelected";
    } else {
      data[sectionIndex][1][rowIndex].imgDropdown = "arrow_down.png";
      data[sectionIndex][1][rowIndex].template = "flxSegTransactionRowSavings";
    }

    this.view.transactions.segTransactions.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);
  },
  toggleEditRuleVisibility: function () {
    if (this.view.editRule.isVisible == true) {
      this.view.editRule.isVisible = false;
    } else {
      this.view.editRule.isVisible = true;
      this.view.editRule.setFocus(true);
    }
  },
  getTransactionIconFor: function (transactionType) {
    var img;
    switch (transactionType) {
      case "InternalTransfer":
        img = "payment_method.png";
        break;

      case "BillPay":
        img = "transaction_type_others.png";
        break;

      case "ExternalTransfer":
        img = "payment_method.png";
        break;

      case "Deposit":
        img = "transaction_type_checkdeposit.png";
        break;

      case "P2P":
        img = "payment_method.png";
        break;

      case "Cardless":
        img = "transaction_type_withdrawl.png";
        break;

      case "CheckWithdrawal":
        img = "transaction_type_checkdeposit.png";
        break;

      case "Withdrawal":
        img = "transaction_type_withdrawl.png";
        break;

      case "Interest":
        img = "transaction_type_interestrate.png";
        break;
      case "Loan":
        img= "home_loan_icon.png";
        break;
      default :
        img = "transaction_type_others.png";  
    }
    return img;
  },

  onBtnRepeat:function(transaction){
    this.presenter.onBtnRepeatAccountDetailsTransaction(transaction);
  },
    //for occu
    accountSummaryDetailsNew: function(){
      
      if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1== null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1 == undefined){
				this.view.accountSummary.flxInfoNew.isVisible = false;
				this.view.accountSummary.lblCurrentBalanceTitleNew.text = "";
				}else{
                this.view.accountSummary.lblCurrentBalanceTitleNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1;
				}
        if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1!=null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1 == undefined){
        this.view.accountSummary.lblCurrentBalanceValueNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1;
        }else{
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }
		
       if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2==null ||this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2 == undefined){
				this.view.accountSummary.flxInfoNew.isVisible = false;
				this.view.accountSummary.lblAvailableBalanceTitle.text = "";
				}else{
                this.view.accountSummary.lblAvailableBalanceTitle.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2;
				}
        if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2!=null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2 == undefined){
        this.view.accountSummary.lblAvailableBalanceValue.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2;
        }else{
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }
      
      
    //this.view.accountSummary.lblCurrentBalanceTitleNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1;
    //this.view.accountSummary.lblCurrentBalanceValueNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1;
     // this.view.accountSummary.lblAvailableBalanceTitle.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2;
     // this.view.accountSummary.lblAvailableBalanceValue.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2;
    },
  updateTransactions: function (transactionDetails) {
    var controller = this;
    //for occu
    //this.accountSummaryDetailsNew();
    if(this.presenter.viewModel.transactionsViewModel.recentTransactions.length > 0){
   
      if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1== null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1 == undefined){
				this.view.accountSummary.flxInfoNew.isVisible = false;
				this.view.accountSummary.lblCurrentBalanceTitleNew.text = "";
				}else{
                this.view.accountSummary.lblCurrentBalanceTitleNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1;
				}
        if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1!=null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1 == undefined){
        this.view.accountSummary.lblCurrentBalanceValueNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1;
        }else{
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }
		
       if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2==null ||this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2 == undefined){
				this.view.accountSummary.flxInfoNew.isVisible = false;
				this.view.accountSummary.lblAvailableBalanceTitle.text = "";
				}else{
                this.view.accountSummary.lblAvailableBalanceTitle.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2;
				}
        if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2!=null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2 == undefined){
        this.view.accountSummary.lblAvailableBalanceValue.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2;
        }else{
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }   
    }
    
    this.view.transactions.flxPagination.setVisibility(true);
    this.adjustUIForTransactions(transactionDetails.pendingTransactions.length > 0 || transactionDetails.recentTransactions.length > 0);
    var createSegmentSection = function (sectionHeaderText, transactions) {
      return [{
          "lblTransactionHeader": sectionHeaderText,
          "lblSeparator": "."
        },
        transactions.map(controller.createTransactionSegmentModel),
      ];
    };
    var pendingTransactionsSection = createSegmentSection(kony.i18n.getLocalizedString('i18n.accounts.pending'), transactionDetails.pendingTransactions);
    var postedTransactionsSection = createSegmentSection(kony.i18n.getLocalizedString('i18n.accounts.posted'), transactionDetails.recentTransactions);
    var transactionsExistInSection = function (section) {
      return section[1] && section[1].length && section[1].length > 0;
    };
    this.view.transactions.segTransactions.widgetDataMap={
      			"btnCheckNumber":"btnCheckNumber",
                "btnRepeat":"btnRepeat",
                "lblNoteTitle":"lblNoteTitle",
                "lblNoteValue":"lblNoteValue",
                "lblFrequencyTitle":"lblFrequencyTitle",
                "lblFrequencyValue":"lblFrequencyValue",
                "lblRecurrenceTitle":"lblRecurrenceTitle",
                "lblRecurrenceValue":"lblRecurrenceValue",
                "btnDisputeTransaction": "btnDisputeTransaction",
                "btnEditRule": "btnEditRule",
                "btnPrint": "btnPrint",
                "cbxRememberCategory": "cbxRememberCategory",
      			"flxCheckContainer": "flxCheckContainer",
                "flxActions": "flxActions",
                "flxActionsWrapper": "flxActionsWrapper",
                "flxAmount": "flxAmount",
                "flxBalance": "flxBalance",
                "flxCategory": "flxCategory",
                "flxDate": "flxDate",
                "flxDescription": "flxDescription",
                "flxDetail": "flxDetail",
                "flxDetailData": "flxDetailData",
                "flxDetailHeader": "flxDetailHeader",
                "flxDropdown": "flxDropdown",
                "flxIdentifier": "flxIdentifier",
                "flxInformation": "flxInformation",
                "flxLeft": "flxLeft",
                "flxMemo": "flxMemo",
                "flxRight": "flxRight",
                "flxSegTransactionHeader": "flxSegTransactionHeader",
                "flxSegTransactionRowSavings": "flxSegTransactionRowSavings",
                "flxSegTransactionRowSelected": "flxSegTransactionRowSelected",
                "flxSegTransactionRowWrapper": "flxSegTransactionRowWrapper",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxToData": "flxToData",
                "flxToHeader": "flxToHeader",
                "flxType": "flxType",
                "flxTypeData": "flxTypeData",
                "flxTypeHeader": "flxTypeHeader",
                "flxWithdrawalAmountData": "flxWithdrawalAmountData",
                "flxWithdrawalAmountHeader": "flxWithdrawalAmountHeader",
                "flxWrapper": "flxWrapper",
                "imgCategoryDropdown": "imgCategoryDropdown",
                "imgDropdown": "imgDropdown",
                "imgType": "imgType",
                "lblType": "lblType",
                "lblAmount": "lblAmount",
                "lblBalance": "lblBalance",
                "lblCategory": "lblCategory",
                "lblDate": "lblDate",
                "lblDescription": "lblDescription",
                "lblIdentifier": "lblIdentifier",
                "lblSeparator": "lblSeparator",
                "lblSeparatorActions": "lblSeparatorActions",
                "lblSeparatorDetailData": "lblSeparatorDetailData",
                "lblSeparatorDetailHeader": "lblSeparatorDetailHeader",
                "lblToTitle": "lblToTitle",
                "lblToValue": "lblToValue",
                "lblTransactionHeader": "lblTransactionHeader",
                "lblTypeTitle": "lblTypeTitle",
                "lblTypeValue": "lblTypeValue",
                "lblWithdrawalAmountTitle": "lblWithdrawalAmountTitle",
                "lblWithdrawalAmountValue": "lblWithdrawalAmountValue",
                "txtMemo": "txtMemo",

                // checks datamap
                "btnDisputeTransaction": "btnDisputeTransaction",
                "btnPrint": "btnPrint",
                "btnRepeat": "btnRepeat",
                "CopyflxToHeader0g61ceef5594d41": "CopyflxToHeader0g61ceef5594d41",
                "CopylblToTitle0a2c47b22996e4f": "CopylblToTitle0a2c47b22996e4f",
                "flxActions": "flxActions",
                "flxActionsWrapper": "flxActionsWrapper",
                "flxAmount": "flxAmount",
                "flxBalance": "flxBalance",
                "flxBankName1": "flxBankName1",
                "flxBankName2": "flxBankName2",
                "flxCash": "flxCash",
                "flxCheck1": "flxCheck1",
                "flxCheck1Ttitle": "flxCheck1Ttitle",
                "flxCheck2": "flxCheck2",
                "flxCheck2Ttitle": "flxCheck2Ttitle",
                "flxCheckImage": "flxCheckImage",
                "flxCheckImage2Icon": "flxCheckImage2Icon",
                "flxCheckImageIcon": "flxCheckImageIcon",
                "flxDate": "flxDate",
                "flxDescription": "flxDescription",
                "flxDetail": "flxDetail",
                "flxDetailHeader": "flxDetailHeader",
                "flxDropdown": "flxDropdown",
                "flxIdentifier": "flxIdentifier",
                "flxLeft": "flxLeft",
                "flxRememberCategory": "flxRememberCategory",
                "flxRight": "flxRight",
                "flxSegCheckImages": "flxSegCheckImages",
                "flxSegTransactionRowSavings": "flxSegTransactionRowSavings",
                "flxSegTransactionRowWrapper": "flxSegTransactionRowWrapper",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxToHeader": "flxToHeader",
                "flxTotal": "flxTotal",
                "flxTotalValue": "flxTotalValue",
                "flxTypeHeader": "flxTypeHeader",
                "flxWithdrawalAmount": "flxWithdrawalAmount",
                "flxWithdrawalAmountCash": "flxWithdrawalAmountCash",
                "flxWithdrawalAmountCheck1": "flxWithdrawalAmountCheck1",
                "flxWithdrawalAmountCheck2": "flxWithdrawalAmountCheck2",
                "flxWithdrawalAmountHeader": "flxWithdrawalAmountHeader",
                "flxWrapper": "flxWrapper",
                "imgCheckimage": "imgCheckimage",
                "imgCheckImage1Icon": "imgCheckImage1Icon",
                "imgCheckImage2Icon": "imgCheckImage2Icon",
                "imgDropdown": "imgDropdown",
                "imgRememberCategory": "imgRememberCategory",
                "lblAmount": "lblAmount",
                "lblBalance": "lblBalance",
                "lblBankName1": "lblBankName1",
                "lblBankName2": "lblBankName2",
                "lblCheck1Ttitle": "lblCheck1Ttitle",
                "lblCheck2Ttitle": "lblCheck2Ttitle",
                "lblDate": "lblDate",
                "lblDescription": "lblDescription",
                "lblIdentifier": "lblIdentifier",
                "lblRememberCategory": "lblRememberCategory",
                "lblSeparator": "lblSeparator",
                "lblSeparator2": "lblSeparator2",
                "lblSeparatorActions": "lblSeparatorActions",
                "lblSeperatorhor1": "lblSeperatorhor1",
                "lblSeperatorhor2": "lblSeperatorhor2",
                "lblSeperatorhor3": "lblSeperatorhor3",
                "lblSeperatorhor4": "lblSeperatorhor4",
                "lblSeperatorhor5": "lblSeperatorhor5",
                "lblTotalValue": "lblTotalValue",
                "lblToTitle": "lblToTitle",
                "lblTypeTitle": "lblTypeTitle",
                "lblWithdrawalAmount": "lblWithdrawalAmount",
                "lblWithdrawalAmountCash": "lblWithdrawalAmountCash",
                "lblWithdrawalAmountCheck1": "lblWithdrawalAmountCheck1",
                "lblWithdrawalAmountCheck2": "lblWithdrawalAmountCheck2",
                "lblWithdrawalAmountTitle": "lblWithdrawalAmountTitle",
                "segCheckImages" : "segCheckImages",
                "txtFieldMemo": "txtFieldMemo"
            };
    this.view.transactions.segTransactions.setData([pendingTransactionsSection, postedTransactionsSection].filter(transactionsExistInSection));
    CommonUtilities.Sorting.updateSortFlex(this.transactionsSortMap, transactionDetails.config);
    this.view.forceLayout();
  },

  adjustUIForTransactions: function (isPresent) {
    if (isPresent) {
      this.view.transactions.flxNoTransactions.isVisible = false;
      this.view.transactions.flxSort.isVisible = true;
    } else {
      this.view.transactions.flxNoTransactions.isVisible = true;
      this.view.transactions.flxSort.isVisible = false;
    }
  },

  getDataByType: function (accountType,transaction) {
    var accountTypeToSegmentConfigMap = {
      'Savings': {
        editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
        transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
        ToText: kony.i18n.getLocalizedString('i18n.common.To'),
        ToValue:transaction!==undefined?transaction.nickName:null,
        withdrawFlag: true,
        rememberFlag: false, // Made false as it's Not in scope R4
        typeText: kony.i18n.getLocalizedString('i18n.common.Type')
      },
      'Checking': {
        editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
        transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
        ToText: kony.i18n.getLocalizedString('i18n.common.To'),
        ToValue:transaction!==undefined?transaction.nickName:null,
        withdrawFlag: true,
        rememberFlag: false, // Made false as it's Not in scope R4
        typeText: kony.i18n.getLocalizedString('i18n.common.Type')
      },
      'CreditCard': {
        editText: kony.i18n.getLocalizedString('i18n.common.Download'),
        transactionText: kony.i18n.getLocalizedString('i18n.accounts.repeatTransaction'),
        ToText: kony.i18n.getLocalizedString('i18n.accounts.referenceNumber'),
        ToValue:transaction!==undefined?transaction.reference:null,
        withdrawFlag: false,
        rememberFlag: false,
        typeText: kony.i18n.getLocalizedString('i18n.accounts.TransactionType')
      },
      'Current': {
        editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
        transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
        ToText: kony.i18n.getLocalizedString('i18n.common.To'),
        ToValue:transaction!==undefined?transaction.nickName:null,
        withdrawFlag: true,
        rememberFlag: false, // Made false as it's Not in scope R4
        typeText: kony.i18n.getLocalizedString('i18n.common.Type')
      },
      'Loan': {
        editText: kony.i18n.getLocalizedString('i18n.common.Download'),
        transactionText: kony.i18n.getLocalizedString('i18n.accounts.repeatTransaction'),
        ToText: kony.i18n.getLocalizedString('i18n.accounts.referenceNumber'),
        ToValue:transaction!==undefined?transaction.reference:null,
        withdrawFlag: false,
        rememberFlag: false,
        typeText: kony.i18n.getLocalizedString('i18n.accounts.TransactionType')
      },
      'Deposit': {
        editText: kony.i18n.getLocalizedString('i18n.common.Download'),
        transactionText: kony.i18n.getLocalizedString('i18n.accounts.repeatTransaction'),
        ToText: kony.i18n.getLocalizedString('i18n.accounts.referenceNumber'),
        ToValue:transaction!==undefined?transaction.reference:null,
        withdrawFlag: false,
        rememberFlag: false,
        typeText: kony.i18n.getLocalizedString('i18n.accounts.TransactionType')
      },
      'AccountTypeNotFound': {
        editText: kony.i18n.getLocalizedString('i18n.accounts.editRule'),
        transactionText: kony.i18n.getLocalizedString('i18n.accounts.disputeTransaction'),
        ToText:  kony.i18n.getLocalizedString('i18n.accounts.referenceNumber'),
        ToValue:transaction!==undefined?transaction.reference:null,
        withdrawFlag: true,
        rememberFlag: false, // Made false as it's Not in scope R4
        typeText: kony.i18n.getLocalizedString('i18n.common.Type')
      }
    };
    return accountTypeToSegmentConfigMap[accountType] ?
      accountTypeToSegmentConfigMap[accountType] : accountTypeToSegmentConfigMap.AccountTypeNotFound;
  },

  createTransactionSegmentModel: function(transaction) {
      var self = this;
      var transactionType = transaction.type;
      var isRepeatableTransaction = (transaction.accountType === OLBConstants.ACCOUNT_TYPE.SAVING || transaction.accountType === OLBConstants.ACCOUNT_TYPE.CHECKING); 
      var isFeesOrInterestTransaction = (transactionType === OLBConstants.TRANSACTION_TYPE.FEES || transactionType === OLBConstants.TRANSACTION_TYPE.INTERESTCREDIT || transactionType === OLBConstants.TRANSACTION_TYPE.INTERESTDEBIT); 
      function getMappings(context) {
          if (context == "btnDisputeTransaction") {
              if (isRepeatableTransaction && CommonUtilities.getConfiguration("editDisputeATransaction") === "true") {
                  return {
                      "text": self.getDataByType(transaction.accountType).transactionText,
                      "isVisible": false // Made false as it's Not in scope R4
                  };
              } else {
                  return {
                      "isVisible": false,
                  };
              }
          }
          if (context == "btnPrint") {
              if (CommonUtilities.getConfiguration("printingTransactionDetails") === "true") {
                  return {
                      "text": kony.i18n.getLocalizedString('i18n.accounts.print'),
                      "onClick": function() {
                          self.onClickPrintRow();
                      }
                  };
              } else {
                  return {
                      "isVisible": false,
                  };
              }
          }
          if (context == "nickName") {
              return self.getDataByType(transaction.accountType, transaction).ToValue;
          }
      }
      if (transaction.showTransactionIcons) {
          this.view.transactions.lblSortType.isVisible = true;
      } else {
          this.view.transactions.lblSortType.isVisible = false;
      }
      var accountType = transaction.accountType;

      function btnRepeatActions() {
          if (isRepeatableTransaction && !isFeesOrInterestTransaction && transaction.statusDescription == 'successful') {
              return {
                  "text": kony.i18n.getLocalizedString('i18n.accounts.repeat'),
                  "toolTip": kony.i18n.getLocalizedString('i18n.accounts.repeat'),
                  "isVisible": true,
                  "onClick": function() {
                      kony.print("Repeat Pressed");
                      self.onBtnRepeat(transaction);
                  }
              };
          } else {
              return {
                  "text": kony.i18n.getLocalizedString('i18n.accounts.repeat'),
                  "isVisible": false,
                  "onClick": function() {
                      kony.print("Repeat Pressed");
                  }
              };
          }
      }
      if (transaction.type == "Checks" || transaction.type == "CheckWithdrawal" || transaction.type == "CheckDeposit") {
        var dataMap = {
            //"lblRecurrenceTitle": "Recurrence",
            "lblIdentifier": "",
            /*"imgRememberCategory": {
                "src": "unchecked_box.png",
                "isVisible": "false"
            },
            "flxRememberCategory" : {"isVisible" : "false"},
            "lblRememberCategory": {
                "text": "Remember Category",
                "isVisible": "false"
            },*/
            "flxCheck2" : {
              "isVisible" : transaction.frontImage2 ? true : false
            },
            "flxCash" : {
              isVisible : transaction.cashAmount ? true : false
            },            
            "btnPrint": getMappings("btnPrint"),
            "btnEditRule": {
                "isVisible": "false"
            },
            //"btnDisputeTransaction": {"isVisible": "false"},
            "btnRepeat": {
                "isVisible": "false"
            },
            "lblTypeTitle": kony.i18n.getLocalizedString('i18n.CheckImages.Bank'),
            "lblWithdrawalAmountTitle": kony.i18n.getLocalizedString('i18n.accounts.withdrawalAmount'),
            "CopylblToTitle0a2c47b22996e4f": kony.i18n.getLocalizedString('i18n.CheckImages.Cash'),
            //"imgCheckimage": {"text": "C"},
            "imgCheckImage1Icon": {
                "text": "V"
            },
            "imgCheckImage2Icon": {
                "text": "V"
            },
            "lblType": {
              "text": transaction.type,
              "isVisible": transaction.showTransactionIcons
          },
            "lblSeparator": "lblSeparator",
            "lblSeparator2": "lblSeparator2",
            "lblSeparatorActions": "lblSeparatorActions",
            "lblSeperatorhor1": "lblSeperatorhor1",
            "lblSeperatorhor2": "lblSeperatorhor2",
            "lblSeperatorhor3": "lblSeperatorhor3",
            "lblSeperatorhor4": "lblSeperatorhor4",
            "lblSeperatorhor5": "lblSeperatorhor5",
            "lblTotalValue": kony.i18n.getLocalizedString('i18n.CheckImages.Total'),
            "lblToTitle": kony.i18n.getLocalizedString('i18n.CheckImages.Checks/cash'),
            "lblCheck1Ttitle": transaction.checkNumber1,
            "lblCheck2Ttitle": transaction.checkNumber2,
            "flxCheckImageIcon": {
                "onClick": function() {
                    self.showCheckImage(transaction.frontImage1, transaction.backImage1, transaction.date, transaction.withdrawlAmount1, transaction.memo)
                }
            },
            "flxCheckImage2Icon": {
                "onClick": function() {
                    self.showCheckImage(transaction.frontImage2, transaction.backImage2, transaction.date, transaction.withdrawlAmount2, transaction.memo)
                }
            },
            "lblBankName1": transaction.bankName1,
            "lblBankName2": transaction.bankName2,
            "lblWithdrawalAmountCheck1": transaction.withdrawlAmount1,
            "lblWithdrawalAmountCheck2": transaction.withdrawlAmount2,
            "lblWithdrawalAmountCash": transaction.cashAmount,
            "lblWithdrawalAmount": transaction.totalAmount,
            "txtFieldMemo": {
              "placeholder": kony.i18n.getLocalizedString('i18n.CheckImages.MemoOptional'),              
              "isVisible": true,
              "text":transaction.memo
          },
            "imgDropdown": {
                "src": "arrow_down.png",
            },
            "flxDropdown": "flxDropdown",
            "lblDate": transaction.date,
            "lblTypeValue": transaction.type,
            "lblDescription": transaction.description,
            "lblAmount": transaction.amount,
            "lblBalance": transaction.balance,
            "template": "flxSegTransactionRowSavings"
        };
    } else {      
      dataMap = {
          "lblNoteTitle": { "text": kony.i18n.getLocalizedString('i18n.accounts.Note'),
              "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblNoteValue": { 
              "text": transaction.notes,
              "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblFrequencyTitle": { "text": kony.i18n.getLocalizedString('i18n.accounts.frequency'),
              "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblFrequencyValue": { "text": transaction.frequencyType,
              "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblRecurrenceTitle": { "text": kony.i18n.getLocalizedString('i18n.accounts.recurrence'),
              "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblDescription": transaction.description,
          "lblRecurrenceValue": { 
            "text": transaction.numberOfRecurrences,
              "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblIdentifier": "A",
          "imgDropdown": {
              "src": "arrow_down.png",
          },
          "flxDropdown": "flxDropdown",
          "lblDate": transaction.date,
          /* "imgType": {
             "isVisible": transaction.showTransactionIcons, //hidden as per requirement.
             "isVisible": false,
             "src": this.getTransactionIconFor(transaction.type),
             "toolTip":transaction.type
           }, */
          "lblType": {
              "text": transaction.type,
              "isVisible": transaction.showTransactionIcons
          },
          "flxRememberCategory": {
            "isVisible": this.getDataByType(transaction.accountType).rememberFlag
          },
          "imgRememberCategory": {
              "src": "unchecked_box.png",
              "isVisible": this.getDataByType(transaction.accountType).rememberFlag
          },
          "lblRememberCategory": {
              "text": kony.i18n.getLocalizedString('i18n.accounts.rememberCategory'),
              "isVisible": this.getDataByType(transaction.accountType).rememberFlag
          },
          "lblCategory": " ",
          "imgCategoryDropdown": " ",
          "lblAmount": transaction.amount,
          "lblBalance": transaction.balance,
          "lblSeparator": "lblSeparator",
          "lblSeparator2": "lblSeparator2",
          "btnPrint": getMappings("btnPrint"),
          "btnEditRule": {
              "text": this.getDataByType(transaction.accountType).editText
          },
          "btnDisputeTransaction": getMappings("btnDisputeTransaction"),
          "btnRepeat": btnRepeatActions(),
          "lblfrmAccountNumber": transaction.fromAcc,
          "lblSeparatorActions": "lblSeparatorActions",
          "lblTypeTitle": this.getDataByType(transaction.accountType).typeText,
          "lblToTitle": { "text": this.getDataByType(transaction.accountType).ToText,
              "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "fromAccountName": transaction.fromAccountName,
          "lblWithdrawalAmountTitle": {
              "isVisible": this.getDataByType(transaction.accountType).withdrawFlag,
              "text": kony.i18n.getLocalizedString('i18n.accounts.withdrawalAmount'),
          },
          "lblSeparatorDetailHeader": "lblSeparatorDetailHeader",
          "lblTypeValue": transaction.type,
          "lblToValue": { "text": getMappings("nickName"),
              "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblWithdrawalAmountValue": {
              "isVisible": this.getDataByType(transaction.accountType).withdrawFlag,
              "text": transaction.amount
          },
          "lblExternalAccountNumber": transaction.externalAccountNumber,
          "lblSeparatorDetailData": "lblSeparatorDetailData",
          "txtMemo": transaction.notes,
          "toAcc": transaction.toAcc,
          "externalAcc": transaction.externalAccountNumber,
          "template": "flxSegTransactionRowSavings"
      };
    }
    //dataMap.flxDropdown = {
    //    "onClick": this.onClickToggle
    //} // toggle rows is handling in segment controller
    return dataMap;
  },
  showCheckImage: function(url1, url2, postDate, amount, memo) {
    var self= this;
    this.view.CheckImage.lblPostDateValue.text = postDate;
    this.view.CheckImage.lblAmountValue.text = amount;
    this.view.CheckImage.lblMemoValue.text = memo;
    this.view.CheckImage.imgCheckImage.src = url1;
    this.view.flxCheckImage.setVisibility(true);
    this.view.CheckImage.flxFlip.onClick = function () {
      if (self.view.CheckImage.imgCheckImage.src === url1)
        self.view.CheckImage.imgCheckImage.src = url2;
      else
        self.view.CheckImage.imgCheckImage.src = url1;
    };
    this.view.forceLayout();
},
 getSkinForAccount: function (type) {
    var skinConfig = {
      'Savings': 'sknFlx26d0ceBorder1pxRound',
      'Checking': 'sknFlx9060b7Border1pxRound',
      'CreditCard': 'sknFlxf4ba22Border1pxRound',
      'Deposit': 'sknFlx4a90e2Border1pxRound',
      'Mortgage': 'sknFlx8d6429Border1pxRound',
      'Loan': 'sknFlx8d6429Border1pxRound',    
      'UnConfiguredAccount': 'sknFlx26D0CEOpacity20'
    };
    if (skinConfig[type]) {
      return skinConfig[type];
    } else {
      kony.print(accountType + ' is not configured for skin. Using Default configuration.');
      return skinConfig.UnConfiguredAccount;
    }
  },

  updateAccountInfo: function (accountDetailsModel) {
    var controller = this;
    var LabelToDisplayMap = {
      accountNumber: "Account Number",
      routingNumber: "Routing Number",
      swiftCode: "Swift Code",
      accountName: "Primary Account Holder",
      jointHolder: "Joint Account Holder",
      creditcardNumber: "Credit Card Number",
      creditIssueDate: "Credit Issue Date",
    };
    var AccountTypeConfig = {
      'Savings': ['accountNumber', 'routingNumber', 'swiftCode', 'accountName', 'jointHolder'],
      'CreditCard': ['creditcardNumber', 'accountName', 'creditIssueDate'],
      'Mortgage': ['accountNumber', 'accountName', 'jointHolder'],
      'Loan': ['accountNumber', 'accountName', 'jointHolder'],
      'Deposit': ['accountNumber', 'accountName', 'jointHolder'],
      'Checking': ['accountNumber', 'routingNumber', 'swiftCode', 'accountName', 'jointHolder'],
    };
    var AccountInfoUIConfigs = [{
        flx: 'flx' + 'AccountNumber',
        label: 'lbl' + 'AccountNumber',
        value: 'lbl' + 'AccountNumber' + 'Value'
      },
      {
        flx: 'flx' + 'RoutingNumber',
        label: 'lbl' + 'RoutingNumber',
        value: 'lbl' + 'RoutingNumber' + 'Value'
      },
      {
        flx: 'flx' + 'SwiftCode',
        label: 'lbl' + 'SwiftCode',
        value: 'lbl' + 'SwiftCode' + 'Value'
      },
      {
        flx: 'flx' + 'PrimaryHolder',
        label: 'lbl' + 'PrimaryHolder',
        value: 'lbl' + 'PrimaryHolder' + 'Value'
      },
      {
        flx: 'flx' + 'JointHolder',
        label: 'lbl' + 'JointHolder' + 'Title',
        value: 'lbl' + 'JointHolder' + 'Value'
      },
    ];
    AccountInfoUIConfigs.map(function (uiConfig, i) {
      var toShow = null;
      var key = AccountTypeConfig[accountDetailsModel.accountInfo.accountType][i];
      if (key) {
        toShow = {
          label: key,
          value: accountDetailsModel.accountInfo[key]
        };
      }
      return {
        uiConfig: uiConfig,
        toShow: toShow
      };
    }).forEach(function (config) {
      if (config.toShow === null) {
        controller.view.accountInfo[config.uiConfig.flx].isVisible = false;
        controller.view.accountInfo[config.uiConfig.label].text = '';
        controller.view.accountInfo[config.uiConfig.value].text = '';
      } else {
        controller.view.accountInfo[config.uiConfig.flx].isVisible = true;
        controller.view.accountInfo[config.uiConfig.label].text = LabelToDisplayMap[config.toShow.label];
        controller.view.accountInfo[config.uiConfig.value].text = config.toShow.value;
      }
    });
    this.view.forceLayout();
  },

  updateAccountDetails: function (accountDetailsModel) {
    var scopeObj = this;
	this.view.accountSummary.flxIdentifier.skin = this.getSkinForAccount(accountDetailsModel.accountType);
	    var getLastFourDigit = function (numberStr) {
      if (numberStr.length < 4) return numberStr;
      return numberStr.slice(-4);
    };
    this.accountDetailsModel = accountDetailsModel;
    this.view.lblAccountTypes.text = this.view.lblAccountTypes.toolTip = accountDetailsModel.accountInfo.accountName +
      ' xxxxxxxxxxxx' + getLastFourDigit(accountDetailsModel.accountInfo.accountNumber);
      this.view.AllForms.lblInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.Information");
    if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.SAVING) {
      this.view.accountSummary.flxExtraField.isVisible = false;
      this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
      this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
      this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.currentBalance;
      this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingDeposit");
      this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.pendingDeposit;
      this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingWithdrawals");
      this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.accountSummary.pendingWithdrawals;


      this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
      this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.dividendRate;
      this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
      this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.dividendPaidYTD;
      this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastDividendPaid");
      this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.balanceAndOtherDetails.lastDividendPaid;
      this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paidOn");
      this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.paidOn;
      this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalCredits");
      this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.totalCredit;
      this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalDebts");
      this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.totalDebits;
      this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
      this.view.accountSummary.lblCurrentBalanceValueNew.text = accountDetailsModel.accountSummary.currentBalance;
      this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
      this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
      this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.SavingCurrentAccount");
      this.view.accountSummary.flxDummyOne.isVisible = false;
      this.view.accountSummary.flxDummyTwo.isVisible = false;
    }
    if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.CREDITCARD) {
      this.view.accountSummary.flxExtraField.isVisible = false;
      this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndBillDetail");
	  
      this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
      this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.currentBalance;
      this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
      this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.currentDueAmount;
      this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit");
      this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.accountSummary.creditLimit;
      this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
      this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.availableCredit"); 
      this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableCredit;
     
            
	  this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
      this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.accountSummary.currentDueAmount;
      this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.minimumDueAmount");
      this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.minimumDueAmount;
      this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paymentDueDate");
      this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.paymentDueDate;
      this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastStatementBalance");
      this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.lastStatementBalance;
      this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentAmount");
      this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.balanceAndOtherDetails.lastPaymentAmount;
      this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentDate");
      this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.lastPaymentDate;
      this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.rewardBalance");
      this.view.accountSummary.lblDummyOneValue.text = accountDetailsModel.balanceAndOtherDetails.rewardsBalance;
      this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
      this.view.accountSummary.lblDummyTwoValue.text = accountDetailsModel.balanceAndOtherDetails.interestRate;
      this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.CreditCard");

      this.view.accountSummary.flxDummyOne.isVisible = true;
      this.view.accountSummary.flxDummyTwo.isVisible = true;
    }
    if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.LOAN || accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.MORTGAGE) {
      this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestDetail");

	  this.view.accountSummary.flxExtraField.isVisible = false;
      this.view.accountSummary.lblExtraFieldTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.principalBalance");
      this.view.accountSummary.lblExtraFieldValue.text = accountDetailsModel.accountSummary.principalBalance;
      this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.principalAmount");
      this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.principalAmount;
      this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
      this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.currentDueAmount;
      this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.nextPaymentDueDate");
      this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.balanceAndOtherDetails.paymentDueDate;

	  this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
      this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.interestRate;
      this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestPaidYTD");
      this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.interestPaidYTd;
      this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestPaidLastYear");
      this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.interestPaidLastYear;
      this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentAmount");
      this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.lastPaymentAmount;

      this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentDate");
      this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.balanceAndOtherDetails.lastPaymentDate;
      this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.originalAmount");
      this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.originalAmount;
      this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.originalDate");
      this.view.accountSummary.lblDummyOneValue.text = accountDetailsModel.balanceAndOtherDetails.originalDate;
      this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.payoffAmount");
      this.view.accountSummary.lblDummyTwoValue.text = accountDetailsModel.accountSummary.principalBalance;
      this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
      this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.outstandingBalance");
      this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableCredit;
      
      this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.Loan");
      this.view.accountSummary.flxDummyOne.isVisible = true;
      this.view.accountSummary.flxDummyTwo.isVisible = true;
    }
    if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.DEPOSIT) {
      this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestAndMaturityDetail");

	  this.view.accountSummary.flxExtraField.isVisible = true;
      this.view.accountSummary.lblExtraFieldTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
      this.view.accountSummary.lblExtraFieldValue.text = accountDetailsModel.accountSummary.currentBalance;
      this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestEarned");
      this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.interestEarned;
      this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityDate");
      this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.maturityDate;
      this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.term");
      this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.accountSummary.paymentTerm;

      this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
      this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.dividendRate;
      this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
      this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.dividendPaidYTD;
      this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendLastPaidAmount");
      this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.dividendLastPaidAmount;
      this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendLastPaidDate");
      this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.dividendLastPaidDate;
 
      this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityDate");
      this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.accountSummary.maturityDate;
      this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityAmount");
      this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.maturityAmount;
      this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityOption");
      this.view.accountSummary.lblDummyOneValue.text = accountDetailsModel.accountSummary.maturityOption;
      this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.Deposit");
      this.view.accountSummary.flxDummyOne.isVisible = true;
      this.view.accountSummary.flxDummyTwo.isVisible = false;

      this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
      this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.outstandingBalance");
	  this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.balanceAndOtherDetails.outstandingBalance;
            
    }
    if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.CHECKING || accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.CURRENT) {
      this.view.accountSummary.flxExtraField.isVisible = false;
       this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
      this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
      this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.currentBalance;
      this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingDeposit");
      this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.pendingDeposit;
      this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingWithdrawals");
      this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.accountSummary.pendingWithdrawals;

	  this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalCredits");
      this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.totalCredit;
      this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalDebts");
      this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.totalDebits;
      this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.bondInterestLastYear");
      this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.bondInterestLastYear;
      this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidLastYear");
      this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.dividendPaidYTD;

	  this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
      this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.balanceAndOtherDetails.dividendRate;
      this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
      this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.dividendPaidYTD;
      this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastDividendPaid");
      this.view.accountSummary.lblDummyOneValue.text = accountDetailsModel.balanceAndOtherDetails.lastDividendPaid;
      this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paidOn");
      this.view.accountSummary.lblDummyTwoValue.text = accountDetailsModel.balanceAndOtherDetails.paidOn;
          
      this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
      this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
      this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
      this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.SavingCurrentAccount");

      this.view.accountSummary.flxDummyTwo.isVisible = true;
      this.view.accountSummary.flxDummyTwo.isVisible = true;
    }
    if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.LINE_OF_CREDIT) {
      this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
      this.view.accountSummary.flxExtraField.isVisible = false;
      this.view.accountSummary.flxPendingWithdrawals.isVisible = false;
      this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
      this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
      this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
      this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.currentBalance;
      
	  this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
      this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.interestRate;
      this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.regularPaymentAmount");
      this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.regularPaymentAmount;
      this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
      this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.currentDueAmount;
      this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dueDate");
      this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.paymentDueDate;
      
	  this.view.accountSummary.flxBalanceDetailsRight.isVisible = false;
      this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
      this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
      this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
      
     
      this.view.accountSummary.flxDummyOne.isVisible = false;
      this.view.accountSummary.flxDummyTwo.isVisible = false;
      this.view.accountSummary.flxPaidOn.isVisible = false;
    }
    this.setRightSideActions(accountDetailsModel.rightSideActions); //Set Right side actions
    this.setSecondayActions(accountDetailsModel.secondaryActions); //Set Secondary actions

    this.updateAccountInfo(accountDetailsModel);
    this.view.forceLayout();
    
  },
  /**
   * setRightSideActions : Set Right side click actions
   * @member of {frmAccountDetailsController}
   * @param {Array} viewModel - Primary/Rightside actions array
   * @return {}
   * @throws {}
   */
  setRightSideActions : function(viewModel){
    var scopeObj = this;
    var dataItem , j;
    viewModel = viewModel || [];
    // Right action buttonas
    scopeObj.rightActionButtons = [
      scopeObj.view.btnScheduledTransfer,
      scopeObj.view.btnMakeTransfer,
      scopeObj.view.btnPayABill
    ];
    // Right action saparators
    scopeObj.rightActionSeparator = [
      scopeObj.view.flxSeparatorPrimaryActions,
      scopeObj.view.flxSeparatorPrimaryActionsTwo
    ];
    scopeObj.resetRightSideActions(); //Reset Actions
    if(viewModel.length === 0) {
      scopeObj.view.flxPrimaryActions.setVisibility(false);
    }
    else {
      scopeObj.view.flxPrimaryActions.setVisibility(true);
      for (var i = 0; i < 2 && viewModel.length >= 1; i++) {
        dataItem = viewModel[i];
        scopeObj.rightActionButtons[i].text = dataItem.displayName.toUpperCase();
        scopeObj.rightActionButtons[i].setVisibility(true);
        scopeObj.rightActionButtons[i].onClick = dataItem.action;
        scopeObj.rightActionButtons[i].toolTip = dataItem.displayName;
        j = i-1;
        if(j>=0 && j <scopeObj.rightActionSeparator.length) {
          scopeObj.rightActionSeparator[j].setVisibility(true);
        }
      }
    }
  },
  /**
   * resetRightSideActions: Reset Right side Actions UI
   * @member of {frmAccountDetailsController}
   * @param {}  
   * @return {}
   * @throws {}
   */
  resetRightSideActions: function(){
    var scopeObj = this;
    var j;
    for (var i = 0; i < scopeObj.rightActionButtons.length; i++) {
      scopeObj.rightActionButtons[i].setVisibility(false);
      scopeObj.rightActionButtons[i].onClick = null;
      if(i>=0 && i <scopeObj.rightActionSeparator.length) {
        scopeObj.rightActionSeparator[i].setVisibility(false);
      }
    }
  },

  updateHamburgerMenu: function (sideMenuModel) {
    this.view.customheader.initHamburger(sideMenuModel, "Accounts", "My Accounts");
  },

  updateAccountTransactionsTypeSelector: function (accountType) {
    var accountTypeConfig = {
      'Savings': function () {
        this.view.transactions.flxTabsChecking.isVisible = true;
        this.view.transactions.flxTabsCredit.isVisible = false;
        this.view.transactions.flxTabsDeposit.isVisible = false;
        this.view.transactions.flxTabsLoan.isVisible = false;
        this.view.transactions.flxSortType.isVisible = true;
        this.view.transactions.lblSortType.isVisible = true;
        //this.view.transactions.imgSortType.isVisible = true;
        this.view.accountSummary.flxExtraField.isVisible = false;
      },
      'Current': function () {
        this.view.transactions.flxTabsChecking.isVisible = true;
        this.view.transactions.flxTabsCredit.isVisible = false;
        this.view.transactions.flxTabsDeposit.isVisible = false;
        this.view.transactions.flxTabsLoan.isVisible = false;
        this.view.transactions.flxSortType.isVisible = false;
        this.view.accountSummary.flxExtraField.isVisible = false;
        this.view.transactions.lblSortType.isVisible = false;
        this.view.transactions.imgSortType.isVisible = false;
      },
      'CreditCard': function () {
        this.view.transactions.flxTabsChecking.isVisible = false;
        this.view.transactions.flxTabsCredit.isVisible = true;
        this.view.transactions.flxTabsDeposit.isVisible = false;
        this.view.transactions.flxTabsLoan.isVisible = false;
        this.view.transactions.flxSortType.isVisible = false;
        this.view.transactions.lblSortType.isVisible = false;
        this.view.transactions.imgSortType.isVisible = false;
      },
      'Loan': function () {
        this.view.transactions.flxTabsChecking.isVisible = false;
        this.view.transactions.flxTabsCredit.isVisible = false;
        this.view.transactions.flxTabsDeposit.isVisible = false;
        this.view.transactions.flxTabsLoan.isVisible = true;
        this.view.transactions.flxSortType.isVisible = false;
        this.view.transactions.lblSortType.isVisible = false;
        this.view.transactions.imgSortType.isVisible = false;

      },
      'Deposit': function () {
        this.view.transactions.flxTabsChecking.isVisible = false;
        this.view.transactions.flxTabsCredit.isVisible = false;
        this.view.transactions.flxTabsDeposit.isVisible = true;
        this.view.transactions.flxTabsLoan.isVisible = false;
        this.view.transactions.flxSortType.isVisible = false;
        this.view.transactions.lblSortType.isVisible = false;
        this.view.transactions.imgSortType.isVisible = false;
      },
      'Checking': function () {
        this.view.transactions.flxTabsChecking.isVisible = true;
        this.view.transactions.flxTabsCredit.isVisible = false;
        this.view.transactions.flxTabsDeposit.isVisible = false;
        this.view.transactions.flxTabsLoan.isVisible = false;
        this.view.transactions.flxSortType.isVisible = true;
        this.view.accountSummary.flxExtraField.isVisible = false;
        this.view.transactions.lblSortType.isVisible = true;
        //this.view.transactions.imgSortType.isVisible = true;
      },
      'AccountTypeNotFound': function () {
        this.view.transactions.flxTabsChecking.isVisible = false;
        this.view.transactions.flxTabsCredit.isVisible = false;
        this.view.transactions.flxTabsDeposit.isVisible = false;
        this.view.transactions.flxTabsLoan.isVisible = true;
        this.view.transactions.flxSortType.isVisible = false;
        this.view.accountSummary.flxExtraField.isVisible = false;
        this.view.transactions.lblSortType.isVisible = false;
        this.view.transactions.imgSortType.isVisible = false;
      }
    };
    if (accountTypeConfig[accountType]) {
      accountTypeConfig[accountType].bind(this)();
    } else {
      kony.print('In Config, Could Not Find Account Type : ', accountType);
      accountTypeConfig.AccountTypeNotFound.bind(this)();
    }
  },

  highlightTransactionType: function (transactionType) {
    var skins = {
      selected: 'sknBtnAccountSummarySelected',
      unselected: 'sknBtnAccountSummaryUnselected',
    hover: 'sknBtnAccountSummarySelected',
    hovered: 'sknhoverbtnacc'
    };
    var transactionTypeToButtonIdMap = {
      'All': ['btnAllChecking', 'btnAllCredit', 'btnAllDeposit', 'btnAllLoan'],
      'Checks': ['btnChecksChecking'],
      'Deposits': ['btnDepositsChecking', 'btnDepositDeposit'],
      'Transfers': ['btnTransfersChecking'],
      'Withdrawals': ['btnWithdrawsChecking', 'btnWithdrawDeposit'],
      'Payments': ['btnPaymentsCredit'],
      'Purchases': ['btnPurchasesCredit'],
      'Interest': ['btnInterestDeposit']
    };
    var tabs = [this.view.transactions.flxTabsChecking,
      this.view.transactions.flxTabsCredit,
      this.view.transactions.flxTabsDeposit,
      this.view.transactions.flxTabsLoan
    ];

    var currentTab = tabs.find(function (tab) {
      return tab.isVisible === true;
    });

    if (transactionTypeToButtonIdMap[transactionType]) {
      var isForTransactionType = function (button) {
        return transactionTypeToButtonIdMap[transactionType]
          .some(function (buttonId) {
            return buttonId === button.id;
          });
      };
      var updateButtonSkin = function (button) {
        if (isForTransactionType(button)) {
          button.skin = skins.selected;
      button.hoverSkin = skins.hover;
        } else {
          button.skin = skins.unselected;
          button.hoverSkin = skins.hovered;
        }
      };
      var getButtonsFrom = function (tab) {
        var hasText = function (text) {
          return function (fullText) {
            return fullText.indexOf(text) > -1;
          };
        };
        var getValueFrom = function (object) {
          return function (key) {
            return object[key];
          };
        };
        return Object.keys(tab)
          .filter(hasText('btn'))
          .map(getValueFrom(tab));
      };
      getButtonsFrom(currentTab).forEach(updateButtonSkin);
    } else {
      kony.print(transactionType + ' not found in config.');
    }
  },
  footeralignment: function(){
    if (kony.os.deviceInfo().screenHeight >= "900") {
      var mainheight = 0;
      var screenheight  = kony.os.deviceInfo().screenHeight;
      mainheight =this.view.customheader.frame.height;
      mainheight = mainheight + this.view.flxMain.frame.height;
      var diff = screenheight - mainheight;
      
      if(diff>0)
          this.view.flxFooter.top = diff + "dp";
  }
  },
  alignAccountTypesToAccountSelectionImg: function () {
    var getNumber = function (str) {
      if (str.length > 2) {
        return Number(str.substring(0, str.length - 2));
      }
      return 0;
    };
    var topImgCenter = this.view.imgAccountTypes.frame.x + (this.view.imgAccountTypes.frame.width / 2);
    var bottomImgLeftPos = (topImgCenter - (getNumber(this.view.accountTypes.imgToolTip.width) / 2));
    this.view.accountTypes.imgToolTip.left = (bottomImgLeftPos / 2 ) + 'dp';
  },
   /*
  * Set secondary actions
  */
  setSecondayActions: function(viewModel) {
        this.updateSecondaryActionsSegment(viewModel);
    }, 
  removeCurrency: function(amount) {
        if (amount === undefined || amount === null) return;
        if (amount[0] === '$') {
            amount = amount.slice(1);
        }
        var res = amount.split(",");
        var value = "";
        for (var i = 0; i < res.length; i++) {
            value = value + res[i];
        }
        return value;
    }, 

 actionForLoan: function(type, data) {
        var scopeObj=this;
        var info={
            accountID:data.accountInfo.accountID,
            principalBalance: data.accountSummary.payOffLoan, 
            payOffCharge: data.balanceAndOtherDetails.payOffCharge,
            closingDate: data.balanceAndOtherDetails.loanClosingDate,
            currentAmountDue: data.accountSummary.currentDueAmount,
            dueDate: data.balanceAndOtherDetails.paymentDueDate,
            fullData: data
          
        };
        var dataInfo={"accounts":info};
        var onNavigationLoanPay=function(){
        var loanModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('LoanPayModule');
        loanModule.presentationController.navigateToLoanPay(dataInfo);    
        };
        if (type === "Payoff Loan") {
                return onNavigationLoanPay;
        }
    },
      
    updateSecondaryActionsSegment: function (viewModel) {
      var scopeObj = this;
      var newModel = this;
      var dataMap = {
        "lblUsers": "lblUsers",
        "lblSeparator": "lblSeparator ",
        "flxAccountTypes": "flxAccountTypes",
      };
      var secondModel = [];
      if(viewModel.length > 2){
        var j=0;
        for(i=2;i<viewModel.length;i++){
          secondModel[j++] = viewModel[i];
        }
      }
      var secondaryActions = secondModel.map(function (dataItem) {
        return {
          "lblUsers": {
            "text": dataItem.displayName,
            "toolTip": dataItem.displayName,
          },
          "lblSeparator": " ",
          "flxAccountTypes": {
            "onClick": dataItem.action
          }
        };
      });
      this.view.moreActions.segAccountTypes.widgetDataMap = dataMap;
      this.view.moreActions.segAccountTypes.setData(secondaryActions);
    }
  };
});