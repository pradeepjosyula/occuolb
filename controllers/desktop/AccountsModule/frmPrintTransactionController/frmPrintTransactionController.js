define({
	/**
     * willUpdateUI : MDA Method to start binding
     * @member of {frmPrintTransactionController}
     * @param {JsonObject} viewModel, model to display data
     * @return {}
     * @throws {}
     */
    willUpdateUI: function (viewModel) {
        if (viewModel.transactions) this.setDataForTransaction(viewModel.transactions);
        if (viewModel.accountDetailsModel) this.setAccountDetailsInformation(viewModel.accountDetailsModel);
    },
  	/**
     * setAccountDetailsInformation : Method to set Account Details Information
     * @member of {frmPrintTransactionController}
     * @param {JsonObject} accountDetailsModel, account model
     * @return {}
     * @throws {}
     */
    setAccountDetailsInformation: function (accountDetailsModel) {
        this.view.lblMyCheckingAccount.text = accountDetailsModel.accountDisplayName;
        this.view.lblKonyBank.text = accountDetailsModel.accountDisplayName;

        this.view.lblTransactionsFrom.setVisibility(false);
        this.view.btnBack.setVisibility(false);
        this.view.btnBackBottom.setVisibility(false);

        this.view.btnBack.onClick = this.loadAccountsDetails;
        this.view.btnBackBottom.onClick = this.loadAccountsDetails;
        this.view.keyValueAccHolderName.lblValue.text = accountDetailsModel.accountInfo.primaryAccountHolder;
        this.view.keyValueAccNumber.lblValue.text = accountDetailsModel.accountInfo.accountNumber;
        this.view.keyValueBank.lblValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.bankName;
        //this.view.keyValueBranch.lblValue.text = account.;

        if (accountDetailsModel.accountInfo.accountType == "Savings") {

            this.view.keyValueAvailableBalance.lblValue.text = accountDetailsModel.accountSummary.availableBalance;
            this.view.keyValueAvailableBalance.lblKey.text = "Available Balance";

            this.view.keyValueCurrentBalance.lblValue.text = accountDetailsModel.accountSummary.currentBalance;
            this.view.keyValueCurrentBalance.lblKey.text = "Current Balance";

            this.view.keyValuePendingDeposits.lblValue.text = accountDetailsModel.accountSummary.pendingDeposit;
            this.view.keyValuePendingDeposits.lblKey.text = "Pending Deposit";

            this.view.keyValuePendingWithdrawal.lblValue.text = accountDetailsModel.accountSummary.pendingWithdrawals;
            this.view.keyValuePendingWithdrawal.lblKey.text = "Pending Withdrawals";
        }
        if (accountDetailsModel.accountInfo.accountType == "CreditCard") {

            this.view.keyValueAvailableBalance.lblValue.text = accountDetailsModel.accountSummary.availableCredit;
            this.view.keyValueAvailableBalance.lblKey.text = "Available Credit";

            this.view.keyValueCurrentBalance.lblValue.text = accountDetailsModel.accountSummary.currentBalance;
            this.view.keyValueCurrentBalance.lblKey.text = "Current Balance";

            this.view.keyValuePendingDeposits.lblValue.text = accountDetailsModel.accountSummary.currentDueAmount;
            this.view.keyValuePendingDeposits.lblKey.text = "Current Due Amount";

            this.view.keyValuePendingWithdrawal.lblValue.text = accountDetailsModel.accountSummary.creditLimit;
            this.view.keyValuePendingWithdrawal.lblKey.text = "Credit Limit";
        }
        if (accountDetailsModel.accountInfo.accountType == "Loan") {

            this.view.keyValueAvailableBalance.lblValue.text = accountDetailsModel.accountSummary.outsandingBalance;
            this.view.keyValueAvailableBalance.lblKey.text = "Outstanding Balance";

            this.view.keyValueCurrentBalance.lblValue.text = accountDetailsModel.accountSummary.principalBalance;
            this.view.keyValueCurrentBalance.lblKey.text = "Principal Balance";

            this.view.keyValuePendingDeposits.lblValue.text = accountDetailsModel.accountSummary.principalAmount;
            this.view.keyValuePendingDeposits.lblKey.text = "Principal Amount";

            this.view.keyValuePendingWithdrawal.lblValue.text = accountDetailsModel.accountSummary.currentDueAmount;
            this.view.keyValuePendingWithdrawal.lblKey.text = "Current Due Amount";

        }
        if (accountDetailsModel.accountInfo.accountType == "Deposit") {

            this.view.keyValueAvailableBalance.lblValue.text = accountDetailsModel.accountSummary.availableBalance;
            this.view.keyValueAvailableBalance.lblKey.text = "Available Balance";

            this.view.keyValueCurrentBalance.lblValue.text = accountDetailsModel.accountSummary.currentBalance;
            this.view.keyValueCurrentBalance.lblKey.text = "Current Balance";

            this.view.keyValuePendingDeposits.lblValue.text = accountDetailsModel.accountSummary.interestEarned;
            this.view.keyValuePendingDeposits.lblKey.text = "Interest Earned";

            this.view.keyValuePendingWithdrawal.lblValue.text = accountDetailsModel.accountSummary.maturityDate;
            this.view.keyValuePendingWithdrawal.lblKey.text = "Maturity Date";
        }
        if (accountDetailsModel.accountInfo.accountType == "Checking" || accountDetailsModel.accountInfo.accountType == "Current") {

            this.view.keyValueAvailableBalance.lblValue.text = accountDetailsModel.accountSummary.availableBalance;
            this.view.keyValueAvailableBalance.lblKey.text = "Available Balance";

            this.view.keyValueCurrentBalance.lblValue.text = accountDetailsModel.accountSummary.currentBalance;
            this.view.keyValueCurrentBalance.lblKey.text = "Current Balance";

            this.view.keyValuePendingDeposits.lblValue.text = accountDetailsModel.accountSummary.pendingDeposit;
            this.view.keyValuePendingDeposits.lblKey.text = "Pending Deposit";

            this.view.keyValuePendingWithdrawal.lblValue.text = accountDetailsModel.accountSummary.pendingWithdrawals;
            this.view.keyValuePendingWithdrawal.lblKey.text = "Pending Withdrawals";
        }
        if (accountDetailsModel.accountInfo.accountType == "Line of Credit") {

            this.view.accountSummary.lblAvailableBalanceTitle.text = "Available Balance";
            this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;

            this.view.keyValueAvailableBalance.lblValue.text = accountDetailsModel.accountSummary.availableBalance;
            this.view.keyValueAvailableBalance.lblKey.text = "Available Balance";

            this.view.keyValueCurrentBalance.lblValue.text = accountDetailsModel.accountSummary.currentBalance;
            this.view.keyValueCurrentBalance.lblKey.text = "Current Balance";

            this.view.keyValuePendingDeposits.lblValue.text = accountDetailsModel.accountSummary.interestRate;
            this.view.keyValuePendingDeposits.lblKey.text = "Intrest Rate";

            this.view.keyValuePendingWithdrawal.lblValue.text = accountDetailsModel.accountSummary.regularPaymentAmount;
            this.view.keyValuePendingWithdrawal.lblKey.text = "Regular Payment Amount";
        }
        this.view.forceLayout();
        var self = this;
        this.view.postShow= function() {
            window.print();
            self.loadAccountsDetails();
        };
    },
	/**
     * setDataForTransaction : Method set data in transaction segment
     * @member of {frmPrintTransactionController}
     * @param {JsonObject} transactions, transactions object
     * @return {}
     * @throws {}
     */  
    setDataForTransaction: function (transactions) {
        var firstTable = transactions[0];
        var secondTable = transactions[1];
        if (firstTable) {
            this.view.lblPendingTransactions.text = firstTable[0].lblTransactionHeader + " TRANSACTIONS";
            this.view.segPendingTransaction.setData(firstTable[1].map(this.createTransactionSegmentsModel));
            this.view.flxPendingTransactions.setVisibility(true);
        } else {
            this.view.flxPendingTransactions.setVisibility(false);
        }
        if (secondTable) {
            this.view.CopylblPendingTransactions0a979ad50321149.text = secondTable[0].lblTransactionHeader + " TRANSACTIONS";
            this.view.segPostedTransactions.setData(secondTable[1].map(this.createTransactionSegmentsModel));
            this.view.flxPostedTransactions.setVisibility(true);
        } else {
            this.view.flxPostedTransactions.setVisibility(false);
        }
        this.view.forceLayout();
    },
	/**
     * createTransactionSegmentsModel : Method to create Transaction SegmentsModel
     * @member of {frmPrintTransactionController}
     * @param {JsonObject} transaction object to map
     * @return {JsonObject} transaction object with segment mapping
     * @throws {}
     */  
    createTransactionSegmentsModel: function (transaction) {
        return {
            lblDate: transaction.lblDate,
            lblDescription: transaction.lblDescription,
            lblType: transaction.lblTypeValue,
            lblCategory: transaction.lblCategory,
            lblAmount: transaction.lblAmount,
            lblBalance: transaction.lblBalance,
            lblSeparator: transaction.lblSeparator
        };
    },
	/**
     * loadAccountsDetails : Method to accounts details
     * @member of {frmPrintTransactionController}
     * @param {}
     * @return {}
     * @throws {}
     */  
    loadAccountsDetails : function(){
        this.presenter.showAgainAccountsDetails();
    }
});