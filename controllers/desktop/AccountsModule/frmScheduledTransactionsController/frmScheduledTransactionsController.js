define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities, OLBConstants) {

  return {
    /**
     * preshowFunction :  Method to handle pre show form activieis
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    preshowFunction: function () {
      var scopeObj = this;
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
      this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      //this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.forceLayout();

      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
      }, {
        text: kony.i18n.getLocalizedString("i18n.accounts.accountDetails")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.accounts.accountsTitle");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.accounts.accountDetails");
      this.view.accountTypes.isVisible = false;
      this.view.accountInfo.isVisible = false;

      //SET ACTIONS:
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height = scopeObj.getPageHeight();
        scopeObj.view.flxLogout.height = height + "dp";
        scopeObj.view.flxLogout.left = "0%";
        scopeObj.view.flxLogout.setFocus(true);
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        var context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.btnViewAccountInfo.onClick = function () {
        scopeObj.showAccountInfo();
      };
      this.view.flxAccountTypes.onClick = function () {
        scopeObj.showAccountTypes();
      };
    },

    /**
     * postShowFrmAccountDetails : Method to handle post show form activieis
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    postShowFrmAccountDetails: function () {
      this.AdjustScreen();
      this.view.accountInfo.left = 420 + this.view.flxMain.frame.x + "dp";
      this.view.accountTypes.left = 89 + this.view.flxMain.frame.x + "dp";
    },
    
    /**
     * AdjustScreen : Ui team proposed method to handle screen aligment
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    AdjustScreen: function () {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMainWrapper.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0)
          this.view.flxFooter.top = mainheight + diff + "dp";
        else
          this.view.flxFooter.top = mainheight + "dp";
        this.view.forceLayout();
      } else {
        this.view.flxFooter.top = mainheight + "dp";
        this.view.forceLayout();
      }
    },

    /**
     * showAccountTypes : Method to display account types flex
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    showAccountTypes: function () {
//       if (this.view.accountTypes.isVisible === false) {
//         this.view.imgAccountTypes.src = OLBConstants.IMAGES.ARRAOW_UP;
//         var top;
//         if (this.view.flxDowntimeWarning.isVisible) {
//           top = "185dp";
//         } else {
//           top = "105dp";
//         }
//         this.view.accountTypes.top = top;
//         this.view.accountTypes.isVisible = true;
//       } else {
//         this.view.imgAccountTypes.src = OLBConstants.IMAGES.ARRAOW_DOWN;
//         this.view.accountTypes.isVisible = false;
//       }
      if ((accntflag % 2) === 0) {
            this.view.imgAccountTypes.src = OLBConstants.IMAGES.ARRAOW_UP;
        var top;
        if (this.view.flxDowntimeWarning.isVisible) {
          top = "185dp";
        } else {
          top = "105dp";
        }
        this.view.accountTypes.top = top;
        this.view.accountTypes.isVisible = true;
            accntflag = 1;
        } else if( (accntflag === 1) && (this.view.accountTypes.isVisible === false) ) {
            this.view.imgAccountTypes.src = OLBConstants.IMAGES.ARRAOW_DOWN;
            this.view.accountTypes.isVisible = false;
            accntflag = 0;
        }
    },

    /**
     * showAccountInfo : Method to display account info
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    showAccountInfo: function () {
      if (this.view.accountInfo.isVisible === false) {
        var top;
        if (this.view.flxDowntimeWarning.isVisible) {
          top = "185dp";
        } else {
          top = "105dp";
        }
        this.view.accountInfo.top = top;
        this.view.accountInfo.isVisible = true;
      } else {
        this.view.accountInfo.isVisible = false;
      }
    },

    /**
     * initFrmFunction : Method to handle Init Form actions.
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    initFrmFunction: function () {
      var scopeObj = this;
      scopeObj.view.scheduledTransactions.segTransactions.setData([]); //Reset Segment

      //Schedule Transactions sort Map
      scopeObj.schduledTransactionsSortMap = [
        { name: 'scheduledDate', imageFlx: scopeObj.view.scheduledTransactions.imgSortDate, clickContainer: scopeObj.view.scheduledTransactions.flxDate },
        //{ name : 't.description' ,  imageFlx : scopeObj.view.scheduledTransactions.imgSortDescription, clickContainer : scopeObj.view.scheduledTransactions.flxDescription},
        //{ name : 'tt.description' ,  imageFlx : scopeObj.view.scheduledTransactions.imgSortType, clickContainer : scopeObj.view.scheduledTransactions.flxType }
        { name: 'amount', imageFlx: scopeObj.view.scheduledTransactions.imgSortAmount, clickContainer: scopeObj.view.scheduledTransactions.flxAmount }
      ];
      scopeObj.view.scheduledTransactions.imgSortDescription.setVisibility(false);
      scopeObj.view.scheduledTransactions.imgSortType.setVisibility(false);
      CommonUtilities.Sorting.setSortingHandlers(scopeObj.schduledTransactionsSortMap, scopeObj.onSchduledTransactionsSortClickHandler, scopeObj);
      scopeObj.view.imgCloseDowntimeWarning.onTouchEnd = function () {
        scopeObj.setServerError(false);
      };
    },
   
    /**
     * onSchduledTransactionsSortClickHandler : On Transactions Sort click handler.
     * @member of {frmScheduledTransactionsController}
     * @param {object} event, click event object.
     * @param {object} data, column details.
     * @return {}
     * @throws {}
     */
    onSchduledTransactionsSortClickHandler: function (event, data) {
      var scopeObj = this;
      scopeObj.presenter.ScheduledTransactions.showTransactionsByType(data);
    },

    /**
     * setServerError : Method to handle Server error 
      * @member of {frmScheduledTransactionsController}
     * @param {boolena} isError , error flag to show/hide error flex.
     * @return {}
     * @throws {}
     */
    setServerError: function (isError) {
      var scopeObj = this;
      scopeObj.view.flxDowntimeWarning.setVisibility(isError);
      if (isError) {
        scopeObj.view.lblDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
      }
      CommonUtilities.hideProgressBar(scopeObj.view);
      scopeObj.view.forceLayout();
    },

    /**
     * willUpdateUI :  Method to update form UI w.r.t view Model data called by presentUserInterface
     * @member of {frmScheduledTransactionsController}
     * @param {object}  viewModel, view model object
     * @return {}
     * @throws {}
     */
    willUpdateUI: function (viewModel) {
      if (viewModel) {
        if (viewModel.progressBar !== undefined){
          if (viewModel.progressBar) {
            this.setServerError(false);
            CommonUtilities.showProgressBar(this.view);
          }
          else {
            CommonUtilities.hideProgressBar(this.view);
          }
        } else {
          if (viewModel.serverError) {
            this.setServerError(viewModel.serverError);
          }
          if (viewModel.events) {
            this.update_Events(viewModel.events);
          }
          if (viewModel.accountList) {
            this.updateAccountList(viewModel.accountList);
          }
          if (viewModel.accountInfo) {
            setTimeout(this.alignAccountTypesToAccountSelectionImg, 500);
            this.updateAccountDetails(viewModel.accountInfo);
          }
          if (viewModel.transactionDetails) {
            this.setServerError(false);
            this.updateTransactions(viewModel.transactionDetails);
          }
          if (viewModel.sideMenu) {
            this.updateHamburgerMenu(viewModel.sideMenu);
          }
          if (viewModel.topBar) {
            this.updateTopBar(viewModel.topBar);
          }
        }        
      }
      this.AdjustScreen();
    },

    totalLodedModules: null,

    /**
     * update_Events : Method to bind form events.
     * @member of {frmScheduledTransactionsController}
     * @param {object} events object
     * @return {}
     * @throws {}
     */
    update_Events: function (events) {
      var scopeObj = this;
      scopeObj.view.scheduledTransactions.btnViewAccountList.toolTip = CommonUtilities.changedataCase(kony.i18n.getLocalizedString("i18n.CardManagement.ViewAccountDetails"));
      scopeObj.view.scheduledTransactions.btnViewAccountList.onClick = events.showAccountDetails;
    },

    /**
     * updateAccountList : Method to bind accounts list
     * @member of {frmScheduledTransactionsController}
     * @param {Array}  accountModel, account list view model
     * @return {}
     * @throws {}
     */
    updateAccountList: function (accountModel) {
      var data = accountModel.map(this.createAccountListSegmentsModel);
      this.view.accountTypes.segAccountTypes.setData(data);
      this.view.forceLayout();
    },

    ACCOUNT_LIST_NAME_MAX_LENGTH: OLBConstants.ACCOUNT_LIST_NAME_MAX_LENGTH,

    /**
     * formatAccountName : Method to format account name
     * @member of {frmScheduledTransactionsController}
     * @param {string}  accountName, account name
     * @return {striung} formattd account name
     * @throws {}
     */
    formatAccountName: function (accountName) {
      if (accountName.length <= this.ACCOUNT_LIST_NAME_MAX_LENGTH) {
        return accountName;
      } else {
        return accountName.substring(0, this.ACCOUNT_LIST_NAME_MAX_LENGTH) + '...';
      }
    },

    /**
     * createAccountListSegmentsModel : Method to create accouts list UI view Modlel
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    createAccountListSegmentsModel: function (account) {
      return {
        "lblUsers": {
          "text": this.formatAccountName(account.accountName),
          "toolTip": this.formatAccountName(account.accountName)
        },
        "lblSeparator": "Separator",
        "flxAccountTypes": {
          "onClick": account.onAccountSelection
        }
      };
    },

    /**
     * alignAccountTypesToAccountSelectionImg : Method to handle account type to account image w.r.t account info
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    alignAccountTypesToAccountSelectionImg: function () {
      var getNumber = function (str) {
        if (str.length > 2) {
          return Number(str.substring(0, str.length - 2));
        }
        return 0;
      };
      var topImgCenter = this.view.imgAccountTypes.frame.x + (this.view.imgAccountTypes.frame.width / 2);
      var bottomImgLeftPos = (topImgCenter - (getNumber(this.view.accountTypes.imgToolTip.width) / 2));
      this.view.accountTypes.imgToolTip.left = bottomImgLeftPos + 'dp';
    },

    /**
     * updateAccountInfo : Method to display account info w.r.t account
     * @member of {frmScheduledTransactionsController}
     * @param {object}  accountInfo , account Info view model
     * @return {}
     * @throws {}
     */
    updateAccountInfo: function (accountInfo) {
      var controller = this;
      var LabelToDisplayMap = {
        accountNumber: kony.i18n.getLocalizedString("i18n.accounts.accountNumber"),
        routingNumber: kony.i18n.getLocalizedString("i18n.accounts.routingNumber"),
        swiftCode: kony.i18n.getLocalizedString("i18n.accounts.swiftCode"),
        primaryAccountHolder: kony.i18n.getLocalizedString("i18n.accounts.primaryholder"),
        jointHolder: kony.i18n.getLocalizedString("i18n.accounts.jointHolder"),
        creditcardNumber: kony.i18n.getLocalizedString("i18n.accountDetail.creditCardNumber"),
        creditIssueDate: kony.i18n.getLocalizedString("i18n.accountDetail.creditIssueDate"),
      };
      var AccountTypeConfig = {
        'Savings': ['accountNumber', 'routingNumber', 'swiftCode', 'primaryAccountHolder', 'jointHolder'],
        'CreditCard': ['creditcardNumber', 'primaryAccountHolder', 'creditIssueDate'],
        'Mortgage': ['accountNumber', 'primaryAccountHolder', 'jointHolder'],
        'Deposit': ['accountNumber', 'primaryAccountHolder', 'jointHolder'],
        'Checking': ['accountNumber', 'routingNumber', 'swiftCode', 'primaryAccountHolder', 'jointHolder'],
        'Loan': ['accountNumber', 'accountName', 'jointHolder']
      };
      var AccountInfoUIConfigs = [
        {
          flx: 'flx' + 'AccountNumber',
          label: 'lbl' + 'AccountNumber',
          value: 'lbl' + 'AccountNumber' + 'Value'
        },
        {
          flx: 'flx' + 'RoutingNumber',
          label: 'lbl' + 'RoutingNumber',
          value: 'lbl' + 'RoutingNumber' + 'Value'
        },
        {
          flx: 'flx' + 'SwiftCode',
          label: 'lbl' + 'SwiftCode',
          value: 'lbl' + 'SwiftCode' + 'Value'
        },
        {
          flx: 'flx' + 'PrimaryHolder',
          label: 'lbl' + 'PrimaryHolder',
          value: 'lbl' + 'PrimaryHolder' + 'Value'
        },
        {
          flx: 'flx' + 'JointHolder',
          label: 'lbl' + 'JointHolder' + 'Title',
          value: 'lbl' + 'JointHolder' + 'Value'
        },
      ];
      AccountInfoUIConfigs.map(function (uiConfig, i) {
        var toShow = null;
        var key = AccountTypeConfig[accountInfo.accountType][i];
        if (key) {
          toShow = {
            label: key,
            value: accountInfo[key]
          };
        }
        return {
          uiConfig: uiConfig,
          toShow: toShow
        };
      }).forEach(function (config) {
        if (config.toShow === null) {
          controller.view.accountInfo[config.uiConfig.flx].isVisible = false;
          controller.view.accountInfo[config.uiConfig.label].text = '';
          controller.view.accountInfo[config.uiConfig.value].text = '';
        } else {
          controller.view.accountInfo[config.uiConfig.flx].isVisible = true;
          controller.view.accountInfo[config.uiConfig.label].text = LabelToDisplayMap[config.toShow.label];
          controller.view.accountInfo[config.uiConfig.value].text = config.toShow.value;
        }
      });
      this.view.forceLayout();
    },

    /**
     * updateAccountDetails : Method to handle account details w.r.t account Info
     * @member of {frmScheduledTransactionsController}
     * @param {object}  accountInfo , account Info view model  
     * @return {}
     * @throws {}
     */
    updateAccountDetails: function (accountInfo) {
      var getLastFourDigit = function (numberStr) {
        if (numberStr.length < 4) return numberStr;
        return numberStr.slice(-4);
      };
      this.view.lblAccountTypes.text = this.view.lblAccountTypes.toolTip = accountInfo.accountName +
        ' xxxxxxxxxxxx' + getLastFourDigit(accountInfo.accountNumber);

      this.updateAccountInfo(accountInfo);
    },

    /**
     * updateTransactions : Method to update transaction 
     * @member of {frmScheduledTransactionsController}
     * @param {object} transactionDetails , transactions view model
     * @return {}
     * @throws {}
     */
    updateTransactions: function (transactionDetails) {
      this.view.flxCancelPopup.setVisibility(false);
      this.view.accountTypes.isVisible = false;
      this.view.scheduledTransactions.segTransactions.widgetDataMap = {
        btnAction: "btnAction",
        btnCancelThisOccurrence: "btnCancelThisOccurrence",
        btnCancelSeries: "btnCancelSeries",
        flxAction: "flxAction",
        flxAmount: "flxAmount",
        flxDate: "flxDate",
        flxDescription: "flxDescription",
        flxDropdown: "flxDropdown",
        flxScheduledTransactions: "flxScheduledTransactions",
        flxSegScheduledTransactions: "flxSegScheduledTransactions",
        flxSegTransactionRowWrapper: "flxSegTransactionRowWrapper",
        flxType: "flxType",
        flxWrapper: "flxWrapper",
        imgDropdown: "imgDropdown",
        lblAmount: "lblAmount",
        lblDate: "lblDate",
        lblDescription: "lblDescription",
        lblSeparator2: "lblSeparator2",
        lblType: "lblType",
        flxDetail: 'flxDetail',
        flxDetailWrapperOne: 'flxDetailWrapperOne',
        flxDetailWrapperTwo: 'flxDetailWrapperTwo',
        flxToTitle: 'flxToTitle',
        flxNotesTitle: 'flxNotesTitle',
        flxToValue: 'flxToValue',
        flxNotesValue: 'flxNotesValue',
        flxFrequencyTitle: 'flxFrequencyTitle',
        flxFrequencyValue: 'flxFrequencyValue',
        flxRecurrenceTitle: 'flxRecurrenceTitle',
        flxRecurrenceValue: 'flxRecurrenceTitle',
        lblFrequencyTitle: 'lblFrequencyTitle',
        lblFrequencyValue: 'lblFrequencyValue',
        lblRecurrenceTitle: 'lblRecurrenceTitle',
        lblRecurrenceValue: 'lblRecurrenceValue',
        lblToTitle: "lblToTitle",
        lblNotesTitle: "lblNotesTitle",
        lblToValue: "lblToValue",
        lblNotesValue: "lblNotesValue",
        lblSeperator: "lblSeperator",
      };
      this.adjustUIForTransactions(transactionDetails.scheduledTransactions.length > 0);
      this.view.scheduledTransactions.segTransactions.setData(transactionDetails.scheduledTransactions.map(this.createTransactionSegmentModel));
      CommonUtilities.Sorting.updateSortFlex(this.schduledTransactionsSortMap, transactionDetails.config);
      this.view.forceLayout();
    },

    /**
     * adjustUIForTransactions : Method update flexs w.r.t transactions
     * @member of {frmScheduledTransactionsController}
     * @param {boolean}  isPresent , whether transactions are available or not
     * @return {}
     * @throws {}
     */
    adjustUIForTransactions: function (isPresent) {
      if (isPresent) {
        this.view.scheduledTransactions.flxNoTransactions.isVisible = false;
        this.view.scheduledTransactions.flxSort.isVisible = true;
      } else {
        this.view.scheduledTransactions.flxNoTransactions.isVisible = true;
        this.view.scheduledTransactions.flxSort.isVisible = false;
      }
    },

    /**
     * createTransactionSegmentModel : Method to create transaction UI view model
     * @member of {frmScheduledTransactionsController}
     * @param {object}  transaction , transaction view model
     * @return {object} formatted UI transaction view model
     * @throws {}
     */
    createTransactionSegmentModel: function (transaction) {
      var scopeObj = this;
      return {
        btnAction: {
          text: kony.i18n.getLocalizedString("i18n.accounts.Modify"),
          toolTip: kony.i18n.getLocalizedString("i18n.common.modifyTransaction"),
          onClick: function () {
            scopeObj.onModifyTransaction(transaction.transaction);
          }
        },
        btnCancelThisOccurrence:{
          text: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.common.cancel"): kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
          toolTip: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.common.cancel"): kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
          isVisible: true,
          onClick: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? function(){
            scopeObj.onCancelTransaction(transaction.transaction);
          } : function(){
            scopeObj.onCancelOccurrence(transaction.transaction);
          }
        },
        btnCancelSeries:{
          text: kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
          toolTip: kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
          isVisible: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false: true,
          onClick: function(){
            scopeObj.onCancelSeries(transaction.transaction);
          }
        },
        imgDropdown: {
          "src": OLBConstants.IMAGES.ARRAOW_DOWN,
        },
        lblAmount: transaction.amount,
        lblDate: transaction.date,
        lblDescription: transaction.description,
        lblSeparator2: " ",
        lblType: transaction.type,
        lblToTitle: kony.i18n.getLocalizedString("i18n.common.To"),
        lblNotesTitle: kony.i18n.getLocalizedString("i18n.accounts.Note"),
        lblToValue: transaction.nickName,
        lblNotesValue: transaction.notes? transaction.notes: kony.i18n.getLocalizedString("i18n.common.none"),
        lblFrequencyTitle: kony.i18n.getLocalizedString("i18n.common.frequency"),
        lblFrequencyValue: transaction.frequencyType,
        flxRecurrenceTitle:{
          isVisible: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false: true,
        },
        flxRecurrenceValue: {
          isVisible: transaction.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false: true,
        },
        lblRecurrenceTitle: kony.i18n.getLocalizedString("i18n.common.recurrence"),
        lblRecurrenceValue: transaction.recurrenceDesc ? transaction.recurrenceDesc: "-",
        lblSeperator: " ",
        template: "flxScheduledTransactions"
      };
    },

    /**
     * onModifyTransaction : Method to handle transaction Modify action
     * @member of {frmScheduledTransactionsController}
     * @param {object}  data, transactio view model
     * @return {}
     * @throws {}
     */
    onModifyTransaction: function (data) {
      var scopeObj = this;
      scopeObj.presenter.ScheduledTransactions.onModifyTransaction(data);
    },
	
    /**
     * onCancelTransaction : Method to handle transaction cancel action
     * @member of {frmScheduledTransactionsController}
     * @param {object} transaction object
     * @return {}
     * @throws {}
     */
    onCancelTransaction: function(data) {
      kony.print("onCancelTransaction");
      var self = this;
      this.view.flxCancelPopup.setVisibility(true);
      this.view.CustomPopupCancel.lblPopupMessage.text = "Are you sure you want to cancel the transaction?";
      this.view.CustomPopupCancel.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.cancelUpper");
      this.view.CustomPopupCancel.btnYes.onClick = function(){
        self.view.flxCancelPopup.setVisibility(false);
        CommonUtilities.showProgressBar(self.view);
        self.presenter.cancelScheduledTransaction(data);
      };
      this.view.CustomPopupCancel.btnNo.onClick = function(){
        self.view.flxCancelPopup.setVisibility(false);
      };
      this.view.CustomPopupCancel.imgCross.onTouchEnd = function(){
        self.view.flxCancelPopup.setVisibility(false);
      };
    },
    /**
   	 * onCancelOccurrence : Method to handle transaction cancel occurrence action
   	 * @member of {frmScheduledTransactionsController}
   	 * @param {object} transaction object
   	 * @return {}
   	 * @throws {}
   	 */
    onCancelOccurrence: function(data) {
      kony.print("onCancelOccurrence");
      var self = this;
      this.view.flxCancelPopup.setVisibility(true);
      this.view.CustomPopupCancel.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.cancelOccurrenceMessage");
      this.view.CustomPopupCancel.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.cancelUpper");
      this.view.CustomPopupCancel.btnYes.onClick = function(){
        CommonUtilities.showProgressBar(self.view);
        self.presenter.cancelScheduledTransactionOccurrence(data);
      };
      this.view.CustomPopupCancel.btnNo.onClick = function(){
        self.view.flxCancelPopup.setVisibility(false);
      };
      this.view.CustomPopupCancel.imgCross.onTouchEnd = function(){
        self.view.flxCancelPopup.setVisibility(false);
      };
      
    },
    /**
     * onCancelSeries : Method to handle transaction cancel series action
     * @member of {frmScheduledTransactionsController}
     * @param {object} transaction object
     * @return {}
     * @throws {}
     */
    onCancelSeries: function(data) {
      kony.print("onCancelSeries");
      var self = this;
      this.view.flxCancelPopup.setVisibility(true);
      this.view.CustomPopupCancel.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.cancelSeriesMessage");
      this.view.CustomPopupCancel.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.cancelUpper");
      this.view.CustomPopupCancel.btnYes.onClick = function(){
        CommonUtilities.showProgressBar(self.view);
        self.presenter.cancelScheduledTransactionSeries(data);
      };
      this.view.CustomPopupCancel.btnNo.onClick = function(){
        self.view.flxCancelPopup.setVisibility(false);
      };
      this.view.CustomPopupCancel.imgCross.onTouchEnd = function(){
        self.view.flxCancelPopup.setVisibility(false);
      };
    },
    /**
     * updateHamburgerMenu : Methiod to handle Hamburger Menu
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel, "ACCOUNTS", "My Accounts");
      if (sideMenuModel.contents.selectedMenu === 'myAccounts') {
        this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "My Accounts");
      } else if (sideMenuModel.contents.selectedMenu === 'transferMoney') {
        this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Transfer Money");
      } else if (sideMenuModel.contents.selectedMenu === 'transferHistory') {
        this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Transfer History");
      } else if (sideMenuModel.contents.selectedMenu === 'externalAccounts') {
        this.view.customheader.customhamburger.activateMenu("TRANSFERS", "External Accounts");
      } else if (sideMenuModel.contents.selectedMenu === 'addKonyAccounts') {
        this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Add Kony Accounts");
      } else if (sideMenuModel.contents.selectedMenu === 'addNonKonyAccounts') {
        this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Add Non Kony Accounts");
      } else if (sideMenuModel.contents.selectedMenu === 'payABill') {
        this.view.customheader.customhamburger.activateMenu("BILL PAY", "Pay a Bill");
      } else if (sideMenuModel.contents.selectedMenu === 'addPayee') {
        this.view.customheader.customhamburger.activateMenu("BILL PAY", "Add Payee");
      }
    },
    
    /**
     * updateTopBar : Method to handle top bar menu
     * @member of {frmScheduledTransactionsController}
     * @param {}  
     * @return {}
     * @throws {}
     */
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },
    /**
        * getPageHeight : Return Page height.
        * @member of {frmScheduledTransactionsController}
        * @param {} 
        * @return {} 
        * @throws {}
        */
       getPageHeight: function () {
        return this.view.flxHeader.frame.height + this.view.flxMainWrapper.frame.height + this.view.flxFooter.frame.height;
    }
  };
});