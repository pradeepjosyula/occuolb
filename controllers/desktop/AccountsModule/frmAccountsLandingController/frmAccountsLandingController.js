define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities,OLBConstants) {

  return {
    enableButton: function(button) {
      button.setEnabled(true);
      button.skin = "sknbtnLatoffffff15px";
      button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
    },
    disableButton: function(button) {
      button.setEnabled(false);
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
    },
    enableOrDisableExternalLogin: function(username, password) {
      if (username && String(username).trim() !== "" && password && String(password).trim() !== "") {
        this.enableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
        return true;
      } else {
        this.disableButton(this.view.AddExternalAccounts.LoginUsingSelectedBank.btnLogin);
        return false;
      }

    },
    /**
         * initShowFrmAccountsLanding: Method to handle init form activieis
         * @member of {frmAccountsLandingController}
         * @param {}
         * @return {}
         * @throws {} 
         */
    initActions: function() {
      this.view.banner.imgBanner.src =  OLBConstants.IMAGES.BANNER_IMAGE;
      this.initlizeDonutChart();
    },
    /**
         * initlizeDonutChart: Method to used to initlize the DonutChart
         * @member of {frmAccountsLandingController}
         * @param {}
         * @return {}
         * @throws {} 
         */
    initlizeDonutChart: function() {
      var data = [];
      var options = {
        height: 310,
        width: 470,
        position: 'top',
        chartArea : {
          right :120,
          left: 30
        },
        title: '',
        pieHole: 0.6,
        pieSliceText: 'none',
        tooltip: {
          text: 'percentage'
        },
        colors: ["#FEDB64", "#E87C5E", "#6753EC", "#E8A75E", "#3645A7", "#04B6DF", "#8ED174", "#D6B9EA", "#B160DC", "#23A8B1"]
      };
      var donutChart = new kony.ui.CustomWidget({
        "id": "donutChart",
        "isVisible": true,
        "left": "1dp",
        "top": "0dp",
        "width": "800dp",
        "height": "390dp",
        "zIndex": 1000000
      }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
      }, {
        "widgetName": "DonutChart",
        "chartData": data,
        "chartProperties": options,
        "OnClickOfPie": function(data) {

        }
      });
      this.view.mySpending.flxMySpendingWrapper.onTouchEnd=function(){
        var pfmModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PersonalFinanceManagementModule");
        pfmModule.presentationController.initPFMForm();
      }
      this.view.flxSecondaryDetails.mySpending.flxMySpendingWrapper.flxMySpending.add(donutChart);
    },
    preShowFrmAccountsLanding: function () {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.accountListMenu.isVisible = false;
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney.png";
      //this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
      // var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      // authModule.presentationController.hideProgressBar(); 
      this.view.forceLayout();
      //SET ACTIONS:
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        kony.print("btn logout pressed");
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMain.frame.height;
        scopeObj.view.flxLogout.height = height + "dp";
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.mySpending.flxMySpendingWrapper.onClick = function() {
        var navObj = new kony.mvc.Navigation("frmPersonalFinanceManagement");
        navObj.navigate();
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.myMessages.btnWriteNewMessage.onClick = function() {
        scopeObj.presenter.newMessage()
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      scopeObj.view.imgCloseDowntimeWarning.onTouchEnd = function(){
        scopeObj.setServerError(false);
      };
    },
    hidePopups: function () {
      this.view.accountListMenu.top = 280;
      this.view.accountListMenu.isVisible = false;
    },
    setContextualMenuLeft: function () {
      var flxMainLeft = this.view.flxMainWrapper.frame.x;
      var frameLeft = this.view.accountList.frame.x;
      var segmentLeft = this.view.accountList.flxAccountsSegments.frame.x;
      var frameWidth = this.view.accountList.segAccounts.frame.width;
      var menuWidth = 260;
      var menuLeft = (frameLeft + segmentLeft + frameWidth + flxMainLeft) - menuWidth;
      this.view.accountListMenu.left = menuLeft + "dp";
    },
    setAccountListData: function () {
      return;
    },
    showAllAccounts: function () {
      return;
    },
    showAccountListMenu: function (topValue) {
      if (this.view.accountListMenu.isVisible == true) {
        this.view.accountListMenu.top = 280;
        this.view.accountListMenu.isVisible = false;
        this.view.forceLayout();
      } else {
        kony.print("topValue:" + topValue);
        var currTop = this.view.accountListMenu.top;
        currTop = parseInt(currTop);
        currTop += 100 * topValue;
        kony.print("currTop:" + currTop);
        this.view.accountListMenu.top = currTop;
        this.view.accountListMenu.isVisible = true;
        this.view.forceLayout();
      }
    },
    onLoadChangePointer: function () {
      this.AdjustScreen();
      var elems = document.querySelectorAll("input[kwidgettype='Button']");
      for (var i = 0, iMax = elems.length; i < iMax; i++) {
        elems[i].style.cursor = 'pointer';
      }
    },
    shouldUpdateUI: function (viewModel) {
      return typeof viewModel !== 'undefined' && viewModel !== null;
    },
    willUpdateUI: function (viewModel) {

      if (viewModel.progressBar !== undefined){
        this.updateProgressBarState(viewModel.progressBar);
      } else if (viewModel.serverError)  {
        this.setServerError(viewModel.serverError);
      } else {
        if (viewModel.accountsSummary) this.updateAccountList(viewModel.accountsSummary);
        if (viewModel.welcomeBanner) this.updateProfileBanner(viewModel.welcomeBanner);
        if(viewModel.sideMenu){
          this.updateHamburgerMenu(viewModel.sideMenu); 
        }

        if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
        if (viewModel.accountQuickActions) this.updateAccountQuickActions(viewModel.accountQuickActions);
        if( viewModel.UpcomingTransactionsWidget) this.showUpcomingTransactionsWidget(viewModel.UpcomingTransactionsWidget);
        if(viewModel.messagesWidget) this.showMessagesWidget(viewModel.messagesWidget);
        if(viewModel.PFMDisabled) this.disablePFMWidget();  
        if(viewModel.unreadCount) this.updateAlertIcon(viewModel.unreadCount);
      }
      this.AdjustScreen();
    },

    /**
   * Method to handle Server error 
   * @param {boolena} isError , error flag to show/hide error flex.
   */
    setServerError: function (isError) {
      var scopeObj = this;
      scopeObj.view.flxDowntimeWarning.setVisibility(isError);
      if (isError) { 
        scopeObj.view.lblDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
      }      
      scopeObj.view.forceLayout();
    },
    disablePFMWidget : function(){
      /* 
      Method to Hide the PFM Widget on DashBoard Based on the configuration flag. "isPFMWidgetEnabled"
    */
      this.view.mySpending.setVisibility(false);
      this.view.upcomingTransactions.left = "6%";
      this.view.upcomingTransactions.width = "100%";
      this.view.myMessages.width = "28.4%";
    },
    showUpcomingTransactionsWidget: function(transactions) {
      var self = this;
      this.view.upcomingTransactions.lblHeader.text = kony.i18n.getLocalizedString("i18n.Accounts.upComingTransactions");
      var formattedTransactions = [];
      if (transactions !== undefined && transactions.length > 0){
        //show only 3 transactions
        transactions = transactions.slice(0,3);
        transactions.map(function(transaction) {
          //for OCCU
          var transactionType ="";
          var scheduledDate = "01/08/2012";
          if(transaction.scheduledDate != "--/--/----"){
            scheduledDate = transaction.scheduledDate;
          }else{
            scheduledDate = "";
            //self.view.upcomingTransactions.lblDate.setVisibility(false);
          }
          if(transaction.transactionType!=null){
            transactionType = transaction.transactionType;
          }else{
            transactionType = " ";
          }
          formattedTransactions.push({
            "lblDate": CommonUtilities.getFrontendDateString(scheduledDate, CommonUtilities.getConfiguration("frontendDateFormat")),
            "lblTo": kony.i18n.getLocalizedString("i18n.transfers.lblTo"),
            "lblToValue": transaction.toAccountName,
            "lblFormToNavigate": transactionType,
            "lblAmount": self.presenter.formatCurrency(transaction.amount),
            "lblSeparator": "lblSeparator"
          });
        });
      }
      if (formattedTransactions.length == 0) {
        this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(false);
        this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(true);
        this.view.upcomingTransactions.flxNoTransactions.setVisibility(true);
        this.view.upcomingTransactions.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.Accounts.NoUpcomingTransactions");
      } else {
        this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(true);
        this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(false);
        this.view.upcomingTransactions.segMessages.setData(formattedTransactions);
      }
      this.view.forceLayout();
    },
    showSelectedMessage : function(message)
    {
      var self = this;
      self.presenter.showSelectedMessage(message);
    },
    showMessagesWidget: function(messages) {
      var self = this;
      this.view.myMessages.lblHeader.text = kony.i18n.getLocalizedString("i18n.Accounts.messageTitle");
      var formattedMessages = [];
      messages.map(function(message) {
        formattedMessages.push({
          "lblDate": CommonUtilities.getDateAndTime(message.requestCreatedDate, CommonUtilities.getConfiguration('frontendDateFormat')),
          "rtxDescription": message.requestsubject,
          "lblSeparator": "lblSeparator",
          "onClick": self.showSelectedMessage.bind(self, message)
        });
      });

      if (formattedMessages.length == 0) {
        this.view.myMessages.flxMyMessagesWrapper.setVisibility(false);
        this.view.myMessages.flxMyMeesagesWarningWrapper.setVisibility(true);
        this.view.myMessages.flxNoMessages.setVisibility(true);
        this.view.myMessages.rtxNoMessage.text = kony.i18n.getLocalizedString("i18n.Accounts.noMessage");
      } else {
        this.view.myMessages.flxMyMessagesWrapper.setVisibility(true);
        this.view.myMessages.flxMyMeesagesWarningWrapper.setVisibility(false);
        this.view.myMessages.segMessages.setData(formattedMessages);
      }

      this.view.forceLayout();
    },
    /**
      * updateAlertIcon : Method to update the Alerts Icon in the headermenu when there are unread messages/notifications
      * @param {Object} data - object containing the count which specifies the unread messages/notifications count
      * @return {}
      * @throws {}
      */
    updateAlertIcon : function(data) {
      //Show or hide the new message icon in the header
      if(data.count > 0) {
        this.view.customheader.headermenu.lblNewMessages.setVisibility(true);
      } else {
        this.view.customheader.headermenu.lblNewMessages.setVisibility(false);
      }
    },
    /**
   * updateProgressBarState : Method to show or hide Loading progress bar
   * @param {boolena} isError , error flag to show/hide error flex.
   * @member of {frmAccountsLandingController}
   * @param {boolean} isLoading - loading flag
   * @return {}
   * @throws {} 
   */
    updateProgressBarState: function (isLoading) {
      if (isLoading) {
        this.setServerError(false);
        CommonUtilities.showProgressBar(this.view);
      }
      else {
        CommonUtilities.hideProgressBar(this.view);
      }
    },
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);    
    },
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel, "ACCOUNTS" ,"My Accounts");    
    },

    updateProfileBanner: function (profileBannerModel) {
      this.view.welcome.lblWelcome.text = kony.i18n.getLocalizedString('i18n.accounts.welcome') + ' ' + profileBannerModel.firstName + '!';
      this.view.welcome.lblLastLoggedIn.text = kony.i18n.getLocalizedString('i18n.accounts.lastLoggedIn') + ' ' + this.getFormattedDate(profileBannerModel.lastLoggedInTime);
      this.view.welcome.imgProfile.src = profileBannerModel.userImage;
    },  
    getFormattedDate: function (dateObj) {
      var self = this;
      if (dateObj) return CommonUtilities.getFrontendDateString(dateObj,CommonUtilities.getConfiguration("frontendDateFormat")) + ' ' + (dateObj.getHours() % 12 || 12) + ':' + (dateObj.getMinutes() < 10 ? '0' + dateObj.getMinutes() : dateObj.getMinutes()) + ' ' + (dateObj.getHours() >= 12 ? 'PM' : 'AM');
    },
    createAccountSegmentsModel: function (accounts) {
      var accountTypeConfig = {
        'Savings': {
          sideImage: 'accounts_sidebar_turquoise.png',
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        },
        'Checking': {
          sideImage: 'accounts_sidebar_purple.png',
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        },
        'CreditCard': {
          sideImage: 'accounts_sidebar_yellow.png',
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
        },
        'Deposit': {
          sideImage: 'accounts_sidebar_blue.png',
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
        },
        'Mortgage': {
          sideImage: 'accounts_sidebar_brown.png',
          balanceKey: 'outstandingBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.outstandingBalance'),
        },
        'Loan':{
          sideImage: 'accounts_sidebar_brown.png',
          balanceKey: 'outstandingBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.outstandingBalance'),
        },
        'Default': {
          sideImage: 'accounts_sidebar_turquoise.png',
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        }
      };
      //Modified account number mask for OCCU project
      var accountNumberMask = function (accountNumber) {
        var stringAccNum = '' + accountNumber;
        /*var isLast4Digits = function (index) {
          return index > (stringAccNum.length - 5);
        };
        return stringAccNum.split('').map(function (c, i) {
          return isLast4Digits(i) ? c : 'X';
        }).join('');*/
        var index = stringAccNum.length - 4;
        var accountNum = stringAccNum.substring(index,stringAccNum.length);
        accountNum = 'X'+accountNum;
        return accountNum;
      };
      var getConfigFor = function(accountType){
        if(accountTypeConfig[accountType]){
          return accountTypeConfig[accountType];
        }else{
          kony.print(accountType+' is not configured for display. Using Default configuration.');
          return accountTypeConfig.Default;
        }
      };
      kony.print("accounts is "+JSON.stringify(accounts));
      var accountTypes = [];
      accounts.map(function(account){
        return account.type;
      }).forEach(function(type){
        if(accountTypes.indexOf(type) === -1){
          accountTypes.push(type);
        }
      });
      var self = this;
      //modified for OCCU project
      var myAccountsMainSegData = [];
      var myAccountsrowData = [];
      var accountNumhdval = "";
      var accountNumhdvalMask = "";
      var count = 0;
      //for occu color code

      if(accounts !== null && accounts.length > 0);{
        for(var i=0; i<accounts.length; i++){
          var account = accounts[i];
          var hexcolor = account.hexcolor;
          hexcolor =hexcolor.substring(1,hexcolor.length);
          var hexImage = "accounts_sidebar_blue.png";
          if(hexcolor == "004B98"){
            hexImage = "imghexone.png";
          }else if(hexcolor == "4F8B89"){
            hexImage = "imghextwo.png";
          }else if(hexcolor == "5C4F79"){
            hexImage = "imghexnine.png";
          }else if(hexcolor == "006AA7"){
            hexImage = "imghexthree.png";
          }else if(hexcolor == "7E57C6"){
            hexImage = "imghexfour.png";
          }else if(hexcolor == "8C829E"){
            hexImage = "imghexfive.png";
          }else if(hexcolor == "62A07B"){
            hexImage = "imghexsix.png";
          }else if(hexcolor == "87D400"){
            hexImage = "imghexseven.png";
          }else if(hexcolor == "53638D"){
            hexImage = "imghexeight.png";
          }else if(hexcolor == "CEDF00"){
            hexImage = "imghexten.png";
          }
          var accountobj = {
            // "imgIdentifier": getConfigFor(account.type).sideImage,
            "imgIdentifier":hexImage,
            "lblIdentifier": "A",
            "imgFavourite": account.isFavourite ? "filled_star.png" : "noimg.png",
            "lblAccountName": account.accountName,
            "lblAccountNumber": account.type +" "+account.shareId,
            "lblAccountType": account.type,
            "lblAvailableBalanceValue": account.availableBalance,
            "lblAvailableBalanceTitle": account.balanceType, //getConfigFor(account.type).balanceTitle,
            "imgMenu": "contextual_menu.png",
            "toggleFavourite": account.toggleFavourite,
            "onAccountClick": account.onAccountSelection,
            "onQuickActions": account.openQuickActions,
            "flxMenu": {
              //modified for occu color code
              // "skin": self.getSkinForAccount(account.type)
              "skin":"skin"+hexcolor
            }
          };
          var accountNumber = account.accountNumber;
          kony.print("accountNumber is " + accountNumber);
          var tempaccountheading = account.nickName + " | " + accountNumber;
          if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
            var accountHeader = {
              "lblHeader": accountNumhdvalMask
            };
            myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
            myAccountsrowData = [];
            myAccountsrowData.push(accountobj);
          } else {
            myAccountsrowData.push(accountobj);
          }
          accountNumhdval = account.nickName + " | " + accountNumber;
          accountNumhdvalMask = account.nickName + " | " + accountNumberMask(accountNumber);
          var fullAccountsLen = accounts.length;
          if (count === fullAccountsLen - 1) {
            var accountfinHeader = {
              "lblHeader": accountNumhdvalMask
            };
            myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
            kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
          }
          count++;
        }
      }
      return myAccountsMainSegData;
    },
    getSkinForAccount : function(type){
      var skinConfig = {
        'Savings' : 'sknFlx26D0CEOpacity20',
        'Checking' : 'sknFlx9060b7Opacity20',
        'CreditCard' : 'sknFlxF4BA22Opacity20',
        'Deposit' : 'sknFlx4A90E2Opacity20',
        'Mortgage' : 'sknFlx8D6429Opacity20',
        'Loan': 'sknFlx8D6429Opacity20',
        'UnConfiguredAccount' : 'sknFlx26D0CEOpacity20'
      };
      if(skinConfig[type]){
        return skinConfig[type];
      }else{
        kony.print(type+' is not configured for skin. Using Default configuration.');
        return skinConfig.UnConfiguredAccount;
      }
    },
    updateAccountList: function (accountModel) {
      this.view.accountList.btnShowAllAccounts.onClick = accountModel.toggleAccounts;
      if(accountModel.toggleAccountsText)this.view.accountList.btnShowAllAccounts.text = accountModel.toggleAccountsText;
      //modified for OCCU project - toggle text for all accounts and favorite accounts
      if(accountModel.toggleFavAccountsText)this.view.accountList.lblAccountsHeader.text = accountModel.toggleFavAccountsText;    
      if (accountModel.showFavouriteText) {
        this.view.accountList.btnShowAllAccounts.setVisibility(true);
      } else {
        this.view.accountList.btnShowAllAccounts.setVisibility(false);
      }
      this.view.accountList.segAccounts.setData(this.createAccountSegmentsModel(accountModel.accounts));
      kony.print("final is  "+JSON.stringify(this.createAccountSegmentsModel(accountModel.accounts)));
      this.view.forceLayout();
    },

    quickActionEvent: function () {
      var  currForm  =  kony.application.getCurrentForm();
      var  index  =  currForm.accountListMenu.segAccountListActions.selectedIndex[1];
      var  segmentData  =  currForm.accountListMenu.segAccountListActions.data[index];
      segmentData.onRowClick();
    },

    //UI Code
    AdjustScreen: function() {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
          this.view.flxFooter.top = mainheight + diff + "dp";
        else
          this.view.flxFooter.top = mainheight + "dp";        
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },    
    updateAccountQuickActions: function (quickActionsModel) {
      var toQuickActionSegmentModel = function (quickAction) {
        return {
          "lblUsers": {
            "text": quickAction.displayName,
            "toolTip": quickAction.displayName,
          },
          //modified for OCCU project - Removing line seperator in context menu of accounts landing screen
          //"lblSeparator": "lblSeparator",
          "onRowClick": quickAction.action
        };
      };
      this.view.accountListMenu.segAccountListActions.setData(quickActionsModel.map(toQuickActionSegmentModel));
      this.view.forceLayout();
    }
  }
});
