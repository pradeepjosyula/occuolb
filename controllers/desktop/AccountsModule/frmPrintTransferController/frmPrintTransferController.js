define({
    /**
     * isValidAction: Method to validate action w.r.t given data inputs.
     * @member of {frmPrintTransferController}
     * @param {string} actionName , action name
     * @return {}
     * @throws {}
     */
    willUpdateUI: function (viewModel) {
        if (viewModel.accountDisplayName) this.view.lblKonyBank.text = viewModel.accountDisplayName;
        if (viewModel.transactionRowData) this.setTransactionData(viewModel.transactionRowData);
        if (viewModel.transactionSuccessData) this.setTransactionSuccessData(viewModel.transactionSuccessData);
        if (viewModel.transactionList) this.setTransactions(viewModel.transactionList);
    },
    /**
     * setTransactions: Method to set segment Transactions
     * @member of {frmPrintTransferController}
     * @param {jsonobject} dataList, transaction object
     * @return {}
     * @throws {}
     */  
    setTransactions: function (dataList) {
        var self = this;
        var segHeader = {
            "lblHeader": kony.i18n.getLocalizedString("i18n.billPay.TransactionDetails")
        };
        this.view.lblTitle.text = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
        this.view.postShow = function () {
            window.print();
            self.loadBillpayModule();
        };
        var dataMap = {
            "flxPrintTransfer": "flxPrintTransfer",
            "flxPrintTransferHeader": "flxPrintTransferHeader",
            "lblHeader": "lblHeader",
            "lblKey": "lblKey",
            "lblValue": "lblValue",
            "lblDummy": "lblDummy"
        };
        var segDataModel = [];
        this.view.segTransfers.widgetDataMap = dataMap;

        for (var i = 0; i < dataList.details.length; i++) {
            var rowData = dataList.details[i];
            segDataModel.push([segHeader, this.createSegDataForBulkBillPay(rowData)]);
        }

        this.view.segTransfers.setData(segDataModel);
    },
    /**
     * createSegDataForBulkBillPay: Method to create Segment Data For Bulk BillPay
     * @member of {frmPrintTransferController}
     * @param {jsonobject} data , billpay object
     * @return {jsonobject} segment data mapping object
     * @throws {}
     */  
    createSegDataForBulkBillPay: function (data) {
        var keyMap = {
            lblAmount: kony.i18n.getLocalizedString("i18n.transfers.lblAmount"),
            lblDeliverBy: kony.i18n.getLocalizedString("i18n.billPay.DeliverBy"),
            lblEndingBalanceAccount: kony.i18n.getLocalizedString("i18n.transfer.EndingBalance"),
            lblPayee: kony.i18n.getLocalizedString("i18n.billPay.Payee"),
            lblPayeeAddress: kony.i18n.getLocalizedString("i18n.billpay.payeeAddress"),
            lblPaymentAccount: kony.i18n.getLocalizedString("i18n.billPay.PaymentAccount"),
            lblRefernceNumber: kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
            lblSendOn: kony.i18n.getLocalizedString("i18n.billPay.sendOn")
        };
        function getValue(val) {
            if (key === "lblEndingBalanceAccount" && val) {
                val = val.split(":")[1];
            }
            if (key === "lblPayeeAddress" && val) {
                val = val.split(":")[1];
            }
            return val;
        }
        var segRowData = [];
        for (var key in keyMap) {
            var value = keyMap[key];
            segRowData.push({
                lblKey: value,
                lblValue: getValue(data[key])
            });
        }
        segRowData.push({
            "lblDummy": " ",
            "template": "flxPrintTransferSpace"
        });
        return segRowData;
    },
    /**
     * setTransactionSuccessData: Method to update transaction segment data
     * @member of {frmPrintTransferController}
     * @param {jsonObject} data, transaction object
     * @return {}
     * @throws {}
     */  
    setTransactionSuccessData: function (data) {
        var self = this;
        var segHeader = {
            "lblHeader": kony.i18n.getLocalizedString("i18n.billPay.TransactionDetails")
        };
        this.view.lblTitle.text = data.module;
        this.view.postShow = function () {
            window.print();
            if (data.module.trim() == kony.i18n.getLocalizedString("i18n.accounts.billPay")) {
                self.loadBillpayModule();
            } else {
                self.loadTransferModule();
            }
        };
        var dataMap = {
            "flxPrintTransfer": "flxPrintTransfer",
            "flxPrintTransferHeader": "flxPrintTransferHeader",
            "lblHeader": "lblHeader",
            "lblKey": "key",
            "lblValue": "value",
            "lblDummy": "lblDummy"
        };
        data.details.push({
            "lblDummy": " ",
            "template": "flxPrintTransferSpace"
        });
        var segDataModel = [[segHeader, data.details]];
        this.view.segTransfers.widgetDataMap = dataMap;
        this.view.segTransfers.setData(segDataModel);

    },
    /**
     * setTransactionData: Method to set data in segment
     * @member of {frmPrintTransferController}
     * @param {JSonObject} data, transaction object
     * @return {}
     * @throws {}
     */  
    setTransactionData: function (data) {
        var self = this;
        var segHeader = {
            "lblHeader": kony.i18n.getLocalizedString("i18n.billPay.TransactionDetails")
        };
        this.view.lblTitle.text = kony.i18n.getLocalizedString("i18n.billPay.TransactionDetails");
        this.view.postShow = function () {
            window.print();
            self.loadAccountsDetails();
        };
        var dataMap = {
            "flxPrintTransfer": "flxPrintTransfer",
            "flxPrintTransferHeader": "flxPrintTransferHeader",
            "lblHeader": "lblHeader",
            "lblKey": "lblKey",
            "lblValue": "lblValue",
            "lblDummy": "lblDummy"
        };
        var segRowData = this.createSegData(data);
        var segDataModel = [[segHeader, segRowData]];
        this.view.segTransfers.widgetDataMap = dataMap;
        this.view.segTransfers.setData(segDataModel);

    },
    /**
     * createSegData: Method to create segment mapping
     * @member of {frmPrintTransferController}
     * @param {JsonObject} data, input values
     * @return {Array} segRowdata, data to set in segment
     * @throws {}
     */  
    createSegData: function (data) {
        var segRowData = [];

        if (data.lblTypeValue === "CheckWithdrawal" || data.lblTypeValue === "Checks" || data.lblTypeValue === "CheckDeposit") {
            var isSecondCheckAvailable = data.lblBankName2 != "0" ? true : false;
            segRowData.push({
                lblKey: data.lblTypeTitle,
                lblValue: data.lblBankName1 +
                (isSecondCheckAvailable ?  "," + data.lblBankName2 : "")
            });
            segRowData.push({
                lblKey: data.lblWithdrawalAmountTitle,
                lblValue: data.lblWithdrawalAmountCheck1 + 
                (isSecondCheckAvailable ?  "," + data.lblWithdrawalAmountCheck2 : "")
            });
            segRowData.push({
                lblKey: data.CopylblToTitle0a2c47b22996e4f,
                lblValue: data.lblWithdrawalAmountCash
            });
            segRowData.push({
                lblKey: data.lblTotalValue,
                lblValue: data.lblWithdrawalAmount
            });
            segRowData.push({
                lblKey: data.lblToTitle,
                lblValue: data.lblCheck1Ttitle +
                (isSecondCheckAvailable ?  "," + data.lblCheck2Ttitle : "")
            });
        } else {
            segRowData.push({
                lblKey: kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                lblValue: data.fromAccountName
            });

            /*segRowData.push({
                lblKey: "Category",
                lblValue: data.lblCategory
            });*/

            segRowData.push({
                lblKey: kony.i18n.getLocalizedString("i18n.accounts.ExternalAccountNumber"),
                lblValue: data.lblExternalAccountNumber
            });
            segRowData.push({
                lblKey: data.lblFrequencyTitle,
                lblValue: data.lblFrequencyValue
            });
            segRowData.push({
                lblKey: data.lblNoteTitle,
                lblValue: data.lblNoteValue
            });
            segRowData.push({
                lblKey: data.lblRecurrenceTitle,
                lblValue: data.lblRecurrenceValue
            });
            segRowData.push({
                lblKey: data.lblToTitle.text,
                lblValue: data.lblToValue
            });

            if (data.lblWithdrawalAmountTitle.isVisible && data.lblWithdrawalAmountValue.isVisible) {
                segRowData.push({
                    lblKey: data.lblWithdrawalAmountTitle.text,
                    lblValue: data.lblWithdrawalAmountValue
                });
            }
            /*segRowData.push({
                lblKey: "From AccountNumber",
                lblValue: data.lblfrmAccountNumber
            });
            segRowData.push({
                lblKey: "Note/Memo",
                lblValue: data.txtMemo
            });*/
        }
        segRowData.push({
            lblKey: kony.i18n.getLocalizedString("i18n.transfers.lblAmount"),
            lblValue: data.lblAmount
        });
        segRowData.push({
            lblKey: kony.i18n.getLocalizedString("i18n.accounts.availableBalance"),
            lblValue: data.lblBalance
        });
        segRowData.push({
            lblKey: data.lblTypeTitle,
            lblValue: data.lblTypeValue
        });
        segRowData.push({
            lblKey: kony.i18n.getLocalizedString("i18n.transfers.lblDate"),
            lblValue: data.lblDate
        });
        segRowData.push({
            lblKey: kony.i18n.getLocalizedString("i18n.transfers.Description"),
            lblValue: data.lblDescription
        });
        segRowData.push({
            "lblDummy": " ",
            "template": "flxPrintTransferSpace"
        });
        return segRowData;
    },
    /**
     * loadBillpayModule: Method to navigate billpay module
     * @member of {frmPrintTransferController}
     * @param {}
     * @return {}
     * @throws {}
     */  
    loadBillpayModule: function () {
        this.presenter.navigateToAcknowledgementForm({});
    },
    /**
     * loadTransferModule: Method to navigate transfer module
     * @member of {frmPrintTransferController}
     * @param {}
     * @return {}
     * @throws {}
     */  
    loadTransferModule: function () {
        this.presenter.presentTransferAcknowledge({});
    },
    /**
     * loadAccountsDetails: Method to navigate account details page
     * @member of {frmPrintTransferController}
     * @param {}
     * @return {}
     * @throws {}
     */  
    loadAccountsDetails: function () {
        this.presenter.showAgainAccountsDetails();
    },
    /**
     * preShowFunction: Method to handle form preshow
     * @member of {frmPrintTransferController}
     * @param {}
     * @return {}
     * @throws {}
     */  
    preShowFunction: function () {
        this.view.btnBack.setVisibility(false);
        this.view.btnBackBottom.setVisibility(false);
        this.view.lblKonyBank.setVisibility(false);
    }
});