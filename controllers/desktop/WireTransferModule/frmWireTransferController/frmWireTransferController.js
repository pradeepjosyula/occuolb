define(['CommonUtilities', 'IBANUtils', 'OLBConstants'], function (CommonUtilities, IBANUtils, OLBConstants) {

  return {
    shouldUpdateUI: function (viewModel) {
      return viewModel !== undefined && viewModel !== null;
    },
    /**
     * Function to make changes to UI
     * Parameters: wireTransferViewModel {Object}
     */
    willUpdateUI: function (wireTransferViewModel) {
      if (wireTransferViewModel.showProgressBar) {
        this.showProgressBar();
      } else if (wireTransferViewModel.hideProgressBar) {
        this.hideProgressBar();
      }
      if (wireTransferViewModel.onServerDownError) {
        this.showServerDownForm(wireTransferViewModel.onServerDownError);
      }
      if (wireTransferViewModel.sideMenu) {
        this.updateHamburgerMenu(wireTransferViewModel.sideMenu);
      }
      if (wireTransferViewModel.topBar) {
        this.updateTopBar(wireTransferViewModel.topBar);
      }
      if (wireTransferViewModel.wireTransferRecipients) {
        this.displayWireTransferRecipients(wireTransferViewModel.wireTransferRecipients);
      }
      if (wireTransferViewModel.wireTransferDeactivated) {
        this.loadAccounts();
      }
      if (wireTransferViewModel.states) {
        this.populateStates(wireTransferViewModel.states);
      }
      if (wireTransferViewModel.countries) {
        this.populateCountries(wireTransferViewModel.countries);
      }
      if(wireTransferViewModel.manageWireRecipients){
        this.displayWireTransferRecipients(wireTransferViewModel.manageWireRecipients,"manageRecipients");
      }
      if(wireTransferViewModel.wireTransferTransactions){
        this.displayWireTransferTransactions(wireTransferViewModel.wireTransferTransactions);
      }
      if (wireTransferViewModel.paymentAccounts) {
        this.updatePaymentAccounts(wireTransferViewModel.paymentAccounts)
      }
      if (wireTransferViewModel.addAccountAcknowledgement) {
        this.showAddAccountAcknowledgement(wireTransferViewModel.addAccountAcknowledgement);
      }
      if (wireTransferViewModel.oneTimeTransferAcknowledgement) {
        this.showOneTimeTransferAcknowledgement(wireTransferViewModel.oneTimeTransferAcknowledgement);
      }

      if (wireTransferViewModel.checkingAccounts) {
        this.populateAccountsinListBox(wireTransferViewModel.checkingAccounts);
      }
    },



    /**
    * Handle server error, navigate to serverdown page
    * @member of frmWireTransferController
    * @param {error} server error
    * @returns {void} - None
    * @throws {void} - None
    */
    showServerDownForm: function (onServerDownError) {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.navigateToServerDownScreen();
    },

    /**
     * Hide Progress bar
     */
    hideProgressBar: function (data) {
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
     * Show Progress bar
     */
    showProgressBar: function (data) {
      CommonUtilities.showProgressBar(this.view);
    },
    /**
     * Initialize HamburgerMenu
     * @param: sideMenuModel
     */
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel);
    },
    /**
     * Initialize TopBar
     * @param: topBarModel
     */
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },
    /*
     * Function to show Logout flex
     */
    showLogout: function () {
      var scopeObj = this;
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxContainer.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    },
    /**
     * Function to perform Logout, on confirmation
     */
    performLogout: function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      this.view.flxLogout.left = "-100%";
    },
    /**
    * Fetch all the accounts of user
    * @member of frmWireTransferController
    * @param {void} - None
    * @returns {void} - None
    * @throws {void} - None
    */
    loadAccounts: function () {
      var scopeObj=this;
      var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
      accountsModule.presentationController.fetchAccounts(scopeObj.accountsListCallback.bind(scopeObj),scopeObj.showServerDownForm.bind(scopeObj));
    },

    /**
    * Filters checking accounts
    * @member of frmWireTransferController
    * @param {JSON} userAccounts from backend
    * @returns {void} - None
    * @throws {void} - None
    */
    accountsListCallback:function(response){
      var checkingAccounts=response.filter(function(account) {
        return account.accountType === "Checking";
      });
      if(checkingAccounts.length>0){
        this.populateAccounts(checkingAccounts);
      }
      else{
        //Display No Checking Account UI
        this.hideAll();
        this.view.NotEligibleToTransfer.txtMessage.text="Hi "+kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userName+kony.i18n.getLocalizedString("i18n.WireTransfer.NotEligibleToTransfer");
        this.view.frmNotEligibleToTransfer.setVisibility(true);
      }
    },

    /**
    * Populate accounts on activate screen
    * @member of frmWireTransferController
    * @param {JSON} accounts list
    * @returns {void} - None
    * @throws {void} - None
    */
    populateAccounts: function(checkingAccounts) {
      var scopeObj=this;
      var list = [];
      for (i = 0; i < checkingAccounts.length; i++) {
        var tempList = [];
        tempList.push(checkingAccounts[i].accountID);
        tempList.push(CommonUtilities.getAccountDisplayName(checkingAccounts[i]));
        list.push(tempList);
      }
      this.view.ActivateWireTransferWindow.lbxDefaultAccountForSending.masterData= list;
      this.showActivateWireTransferScreen();
    }, 

    /**
    * Returns the form data for recipient details
    * @member of frmWireTransferController
    * @param {void} - None    
    * @returns {object} - Containing details of recipient details
    * @throws {void} - None
    */

    getRecipientFormData: function () {
      return {
        recipientName: this.view.AddRecipientAccount.tbxRecipientName.text.trim(),
        recipientType: this.view.AddRecipientAccount.imgTypeRadio1.src === 'radiobtn_active_small.png' ? 'Individual' : 'Business',
        recipientAddressLine1: this.view.AddRecipientAccount.tbxAddressLine1.text.trim(),
        recipientAddressLine2: this.view.AddRecipientAccount.tbxAddressLine2.text.trim(),
        recipientCity: this.view.AddRecipientAccount.tbxCity.text.trim(),
        recipientState: this.view.AddRecipientAccount.lbxState.selectedKey,
        recipientCountry: this.view.AddRecipientAccount.lbxCountry.selectedKey,
        recipientZipcode: this.view.AddRecipientAccount.tbxZipcode.text.trim()
      };
    },

    /**
    * Resets the fields of add recipient form.
    * @member of frmWireTransferController
    * @param {void} - None    
    * @returns {void} - None
    * @throws {void} - None
    */

    resetAddRecipientForms: function () {
      this.view.AddRecipientAccount.tbxRecipientName.text = "";
      this.view.AddRecipientAccount.imgTypeRadio1.src = 'radiobtn_active_small.png';
      this.view.AddRecipientAccount.imgTypeRadio2.src = 'icon_radiobtn.png';
      this.view.AddRecipientAccount.tbxAddressLine1.text = ""
      this.view.AddRecipientAccount.tbxAddressLine2.text = ""
      this.view.AddRecipientAccount.tbxCity.text = ""
      this.view.AddRecipientAccount.tbxZipcode.text = "";
      this.view.AddRecipientAccount.tbxSwiftCode.text = "";
      this.view.AddRecipientAccount.tbxIBANOrIRC.text = "";
      this.view.AddRecipientAccount.tbxAccountNumber.text = "";
      this.view.AddRecipientAccount.tbxReAccountNumber.text = "";
      this.view.AddRecipientAccount.tbxNickName.text = "";
      this.view.AddRecipientAccount.tbxBankName.text = "";
      this.view.AddRecipientAccount.tbxBankAddressLine1.text = "";
      this.view.AddRecipientAccount.tbxBankAddressLine2.text = "";
      this.view.AddRecipientAccount.tbxBankCity.text = "";
      this.view.AddRecipientAccount.tbxBankZipcode.text = "";
      this.disableButton(this.view.AddRecipientAccount.btnProceed);
      this.disableButton(this.view.AddRecipientAccount.btnAddRecipent);        
    },

    /**
    * Resets one time transfer form.
    * @member of frmWireTransferController
    * @param {void} - None    
    * @returns {void} - None
    * @throws {void} - None
    */

    resetOneTimeTransferForms: function () {
      this.view.oneTimeTransfer.tbxRecipientName.text = "";
      this.view.oneTimeTransfer.imgRadioBtnAccountType1.text = 'R';
      this.view.oneTimeTransfer.imgRadioBtnAccountType2.text = ' ';
      this.view.oneTimeTransfer.imgRadioBtnRecipient1.text = 'R';
      this.view.oneTimeTransfer.imgRadioBtnRecipient2.text = ' ';
      this.view.oneTimeTransfer.tbxAddressLine1.text = ""
      this.view.oneTimeTransfer.tbxAddressLine2.text = ""
      this.view.oneTimeTransfer.tbxCity.text = ""
      this.view.oneTimeTransfer.tbxZipcode.text = "";
      this.view.oneTimeTransfer.tbxSwiftCode.text = "";
      this.view.oneTimeTransfer.tbxIBANOrIRC.text = "";
      this.view.oneTimeTransfer.tbxAccountNumber.text = "";
      this.view.oneTimeTransfer.tbxReason.text = "";
      this.view.oneTimeTransfer.tbxReAccountNumber.text = "";
      this.view.oneTimeTransfer.tbxNickName.text = "";
      this.view.oneTimeTransfer.tbxBankName.text = "";
      this.view.oneTimeTransfer.tbxBankAddressLine1.text = "";
      this.view.oneTimeTransfer.tbxBankAddressLine2.text = "";
      this.view.oneTimeTransfer.tbxBankCity.text = "";
      this.view.oneTimeTransfer.tbxBankZipcode.text = "";
      this.view.oneTimeTransfer.tbxAmount.text = "";
      this.view.oneTimeTransfer.tbxNote.text = "";
      this.disableButton(this.view.oneTimeTransfer.btnProceed);
      this.disableButton(this.view.oneTimeTransfer.btnStep2Proceed);
      this.disableButton(this.view.oneTimeTransfer.btnStep3MakeTransfer);
    },


    /**
    * Returns the form data for recipient account details
    * @member of frmWireTransferController
    * @returns {object} - Containing details of recipient account details
    * @throws {void} - None
    */

    getRecipientAccountFormData: function () {
      return {
        recipientAccountNickName: this.view.AddRecipientAccount.tbxNickName.text.trim(),
        recipientAccountRoutingCode: this.view.AddRecipientAccount.tbxSwiftCode.text.trim(),
        recipientAccountSwiftCode: this.view.AddRecipientAccount.tbxSwiftCode.text.trim(),
        recipientAccountIBAN: this.view.AddRecipientAccount.tbxIBANOrIRC.text.trim(),
        recipientAccountIRC: this.view.AddRecipientAccount.tbxIBANOrIRC.text.trim(),
        recipientAccountNumber: this.view.AddRecipientAccount.tbxAccountNumber.text.trim(),
        recipientAccountNumberConfirm: this.view.AddRecipientAccount.tbxReAccountNumber.text.trim(),
        recipientAccountNickName: this.view.AddRecipientAccount.tbxNickName.text.trim(),
        recipientBankName: this.view.AddRecipientAccount.tbxBankName.text.trim(),
        recipientBankAddressLine1: this.view.AddRecipientAccount.tbxBankAddressLine1.text.trim(),
        recipientBankAddressLine2: this.view.AddRecipientAccount.tbxBankAddressLine2.text.trim(),
        recipientBankCity: this.view.AddRecipientAccount.tbxBankCity.text.trim(),
        recipientBankState: this.view.AddRecipientAccount.lbxBankState.selectedKey,
        recipientBankZipcode: this.view.AddRecipientAccount.tbxBankZipcode.text.trim()
      }
    },



    /**
    * Returns the complete data for recipient
    * @member of frmWireTransferController
    * @returns {object} - Containing details of recipient  details
    * @throws {void} - None
    */

    getFullRecipientData: function () {
      return {
        recipientDetails: this.getRecipientFormData(),
        recipientAccountDetails: this.getRecipientAccountFormData()
      }
    },

    /**
     * Validates domestic recipient account details form
     * @member of frmWireTransferController
     * @returns {void} - None
     * @throws {void} - None
     */

    checkDomesticRecipientAccountDetailForm: function() {
      var formData = this.getRecipientAccountFormData();        
      if (formData.recipientAccountSwiftCode === "" || formData.recipientAccountNumber === "" || formData.recipientAccountNumberConfirm === "" || formData.recipientAccountNickName === "" || formData.recipientBankName === "" || formData.recipientBankAddressLine1 === ""  || formData.recipientBankCity === "" || formData.recipientBankZipcode === "" || (this.view.AddRecipientAccount.tbxIBANOrIRC.text.isVisible && formData.recipientAccountIBAN === "" || (formData.recipientAccountNumber !== formData.recipientAccountNumberConfirm))) {
        this.disableButton(this.view.AddRecipientAccount.btnAddRecipent);
      } else {
        this.enableButton(this.view.AddRecipientAccount.btnAddRecipent);
      }
    },

    /**
     * Validates domestic recipient details form
     * @member of frmWireTransferController
     * @returns {object} - None
     * @throws {void} - None
     */

    checkDomesticRecipientDetailForm: function() {
      var formData = this.getRecipientFormData();        
      if (formData.recipientName === "" || formData.recipientAddressLine1 === "" || formData.recipientCity === "" || formData.recipientZipcode === "") {
        this.disableButton(this.view.AddRecipientAccount.btnProceed);
      } else {
        this.enableButton(this.view.AddRecipientAccount.btnProceed);
      }
    },

    /**
     * Validates domestic recipient details form in OTT
     * @member of frmWireTransferController
     * @returns {object} - None
     * @throws {void} - None
     */


    checkOTTRecipientDetailForm: function() {
      var formData = this.getOTTRecipientFormData();        
      if (formData.recipientName === "" || formData.recipientAddressLine1 === "" || formData.recipientCity === "" || formData.recipientZipcode === "") {
        this.disableButton(this.view.oneTimeTransfer.btnProceed);
      } else {
        this.enableButton(this.view.oneTimeTransfer.btnProceed);
      }
    },

    /**
     * Validates recipient account details form in OTT
     * @member of frmWireTransferController
     * @returns {object} - None
     * @throws {void} - None
     */


    checkOTTRecipientAccountDetailForm: function() {
      var formData = this.getOTTRecipientAccountFormData();        
      if (formData.recipientAccountSwiftCode === "" || formData.recipientAccountNumber === "" || formData.recipientAccountNumberConfirm === "" || formData.recipientAccountNickName === "" || formData.recipientBankName === "" || formData.recipientBankAddressLine1 === ""  || formData.recipientBankCity === "" || formData.recipientBankZipcode === "" || (this.view.oneTimeTransfer.tbxIBANOrIRC.text.isVisible && formData.recipientAccountIBAN === "" || (formData.recipientAccountNumber !== formData.recipientAccountNumberConfirm))) {
        this.disableButton(this.view.oneTimeTransfer.btnStep2Proceed);
      } else {
        this.enableButton(this.view.oneTimeTransfer.btnStep2Proceed);
      }
    },

    /**
     * Validates transaction  details form in OTT
     * @member of frmWireTransferController
     * @returns {object} - None
     * @throws {void} - None
     */


    checkOTTTransactionForm: function () {
      var formData = this.getOTTTransactionDetails();        
      if (formData.notes === "" || formData.amount === "") {
        this.disableButton(this.view.oneTimeTransfer.btnStep3MakeTransfer);
      }
      else {
        this.enableButton(this.view.oneTimeTransfer.btnStep3MakeTransfer);
      }
    },




    /**
    * Binds initial widget actions - Should be called from pre show
    * @member of frmWireTransferController
    * @returns {object} - None
    * @throws {void} - None
    */

    setInitialActions: function () {
      var scopeObj=this;
      this.view.flxAddKonyAccount.onClick = this.showDomesticAddForm.bind(this);
      this.view.flxAddNonKonyAccount.onClick = this.showInternationalAddForm.bind(this);
      this.view.AddRecipientAccount.btnDomesticAccount.onClick = this.showDomesticAddForm.bind(this);
      this.view.AddRecipientAccount.btnInternationalAccount.onClick = this.showInternationalAddForm.bind(this);
      this.view.NoRecipientsWindow.btnSendMoney.onClick = this.showDomesticAddForm.bind(this);
      //Activate Wire Transfer
      this.view.ActivateWireTransferWindow.flxCheckbox.onClick = function () {
        scopeObj.toggleCheckBox(scopeObj.view.ActivateWireTransferWindow.imgChecbox);
        if (CommonUtilities.isChecked(scopeObj.view.ActivateWireTransferWindow.imgChecbox.src)) scopeObj.enableButton(scopeObj.view.ActivateWireTransferWindow.btnProceed);
        else scopeObj.disableButton(scopeObj.view.ActivateWireTransferWindow.btnProceed);
      };
      this.view.ActivateWireTransferWindow.btnProceed.onClick = this.activateWireTransfer;
      this.view.ActivateWireTransferWindow.btnCancel.onClick = this.NavigateToTransfers;
      this.view.NoRecipientsWindow.btnRequestMoney.onClick = this.showOneTimeTransfer;	//No-Recipients
      this.view.WireTransferContainer.btnMakeTransfer.onClick = function () {
        scopeObj.presenter.fetchWireTransferAccounts("makeTransfer");
      }//MakeTransferAction;	//Make Transfer - Wire Transfer
      this.view.WireTransferContainer.btnRecent.onClick = function () {
        scopeObj.presenter.fetchRecentWireTransferTransactions();
      }	//Recent - Wire Transfer
      this.view.WireTransferContainer.btnManageRecipient.onClick = function () {
        scopeObj.presenter.fetchWireTransferAccounts("manageRecipients");
      }
      //ManageRecipientsAction;	//Manage Recipient 
      this.view.flxAddInternationalAccount.onClick = this.showOneTimeTransfer.bind(this);
      this.setDomesticAddFormValidationActions();
      this.setOneTimeTransferValidationActions();
    },



    /**
    * Binds initial validations actions on text fields - Should be called from pre show
    * @member of frmWireTransferController
    * @returns {object} - None
    * @throws {void} - None
    */

    setDomesticAddFormValidationActions: function () {
      this.view.AddRecipientAccount.tbxRecipientName.onKeyUp = this.checkDomesticRecipientDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxAddressLine1.onKeyUp = this.checkDomesticRecipientDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxAddressLine2.onKeyUp = this.checkDomesticRecipientDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxCity.onKeyUp = this.checkDomesticRecipientDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxZipcode.onKeyUp = this.checkDomesticRecipientDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxSwiftCode.onKeyUp = this.checkDomesticRecipientAccountDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxAccountNumber.onKeyUp = this.checkDomesticRecipientAccountDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxReAccountNumber.onKeyUp = this.checkDomesticRecipientAccountDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxNickName.onKeyUp = this.checkDomesticRecipientAccountDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxBankName.onKeyUp = this.checkDomesticRecipientAccountDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxBankAddressLine1.onKeyUp = this.checkDomesticRecipientAccountDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxBankAddressLine2.onKeyUp = this.checkDomesticRecipientAccountDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxBankCity.onKeyUp = this.checkDomesticRecipientAccountDetailForm.bind(this);
      this.view.AddRecipientAccount.tbxBankZipcode.onKeyUp  = this.checkDomesticRecipientAccountDetailForm.bind(this);
    },

    /**
    * Binds initial validations actions on text fields of OTT - Should be called from pre show
    * @member of frmWireTransferController
    * @returns {object} - None
    * @throws {void} - None
    */

    setOneTimeTransferValidationActions: function () {
      this.view.oneTimeTransfer.tbxRecipientName.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxAddressLine1.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxAddressLine2.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxCity.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxZipcode.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxSwiftCode.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxAccountNumber.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxReAccountNumber.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxNickName.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxBankName.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxBankAddressLine1.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxBankAddressLine2.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxBankCity.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxBankZipcode.onKeyUp  = this.checkOTTRecipientAccountDetailForm.bind(this);
      this.view.oneTimeTransfer.tbxAmount.onKeyUp = this.checkOTTTransactionForm.bind(this);
      this.view.oneTimeTransfer.tbxNote.onKeyUp = this.checkOTTTransactionForm.bind(this);
    },


    /**
    * Shows Domestic recipient add form
    * @member of frmWireTransferController
    * @param {function} onCancel for cancel button
    * @returns {object} - None
    * @throws {void} - None
    */


    showDomesticAddForm: function (onCancel) {
      var scopeObj = this;
      if (onCancel === undefined || typeof onCancel !== "function") {
        onCancel = function () {
          scopeObj.presenter.showWireTransfer();
        };
      }
      this.view.AddRecipientAccount.btnCancel.onClick = onCancel;
      this.ShowDomesticAccountUI();
      this.resetAddRecipientForms();
      this.view.AddRecipientAccount.lbxState.masterData = [];
      this.view.AddRecipientAccount.btnProceed.onClick = this.showDomesticAccountDetailsForm.bind(this);
      this.presenter.fetchStates("US");
    },

    /**
    * Shows Domestic recipient add form - step 2
    * @member of frmWireTransferController
    * @returns {object} - None
    * @throws {void} - None
    */


    showDomesticAccountDetailsForm: function () {
      this.setAddRecipientsStep(2);
      this.view.AddRecipientAccount.btnBack.onClick = this.setAddRecipientsStep.bind(this, 1);
      this.view.AddRecipientAccount.btnAddRecipent.onClick = function () {
        this.confirmAddRecipient(this.getFullRecipientData(), 'Domestic');
      }.bind(this);
    },

    /**
    * UI logic for  add domestic recipient form
    * @member of frmWireTransferController
    * @returns {object} - None
    * @throws {void} - None
    */
    ShowDomesticAccountUI: function () {
      this.showAddRecipientsScreen();
      this.showRoutingCodeField();
      this.view.AddRecipientAccount.btnDomesticAccount.skin = "sknbtnffffffNoBorder";
      this.view.AddRecipientAccount.btnInternationalAccount.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.ShowAllSeperators();
      this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - DOMESTIC";
      this.view.AddRecipientAccount.flxTabsSeperator3.setVisibility(false);
      this.setAddRecipientsStep(1);
      this.view.AddRecipientAccount.flxCountry.setVisibility(false);
      this.view.AddRecipientAccount.lblIBANOrIRC.setVisibility(false);
      this.view.AddRecipientAccount.tbxIBANOrIRC.setVisibility(false);
      this.view.AddRecipientAccount.flxReason.setVisibility(false);
      this.disableButton(this.view.AddRecipientAccount.btnProceed);
      this.detailsJson.isDomesticVar = "true";
      this.detailsJson.isInternationalVar = "false";
      this.view.forceLayout();
    },

    /**
    * Toggles between steps of add recipient form
    * @member of frmWireTransferController
    * @param {number} step - Step Number 1 or 2 
    * @returns {object} - None
    * @throws {void} - None
    */
    setAddRecipientsStep: function (step) {
      this.view.AddRecipientAccount.flxStep1.setVisibility(false);
      this.view.AddRecipientAccount.flxStep2.setVisibility(false);
      this.view.AddRecipientAccount["flxStep" + step].setVisibility(true);
    },

    /**
    * Calls business logic to save recipient.
    * @member of frmWireTransferController
    * @param {string} type - type of recipient to save domestic/international
    * @returns {object} - None
    * @throws {void} - None
    */

    saveRecipient: function (type) {
      this.presenter.saveRecipient(this.getFullRecipientData(), type);
    },

    /**
    * Activate wire transfer 
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */
    activateWireTransfer: function () {
      var defaultAccountID=this.view.ActivateWireTransferWindow.lbxDefaultAccountForSending.selectedKey;
      this.presenter.updateWireTransferForUser(defaultAccountID);
    },

    /**
    * Display wire transfer recipients
    * @member of frmWireTransferController
	* @param {JSON} wire transfer recipients from backened
    * @returns {void} - None
    * @throws {void} - None
    */
    displayWireTransferRecipients:function(wireTransferRecipients,context){
      this.showRightBar("WireTransfersWindow");
      if(wireTransferRecipients.recipients.length>0){
        if(context=="manageRecipients"){
          this.ManageRecipientsAction();
          this.setManageRecipientrData(wireTransferRecipients);
        }
        else{
          this.MakeTransferAction();
          this.setMakeTransferData(wireTransferRecipients);          
        }
      }
      else{
        this.showNoRecipientsScreen();
      }
    },

    /**
    * Display recent wire transfer transactions
    * @member of frmWireTransferController
	* @param {JSON} wire transfer transactions from backened
    * @returns {void} - None
    * @throws {void} - None
    */
    displayWireTransferTransactions:function(wireTransferTransactions,context){
      if(wireTransferTransactions.length>0){
        this.RecentAction();
        this.setRecentData(wireTransferTransactions);
      }
      else{
        //TO DO - No transaction UI 
      }
    },
    /**
    * Display No Recipient Screen - UI logic 
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */
    showNoRecipientsScreen: function () {
      this.hideAll();
      this.view.flxNoRecipients.setVisibility(true);
      this.showRightBar("noRecipient");
      this.view.forceLayout();
    },

    /**
    * UI logic to Change UI based on flow 
    * @member of frmWireTransferController
	* @param {string} screen name
    * @returns {void} - None
    * @throws {void} - None
    */
    showRightBar: function (screenName) {
      this.view.flxRightBar.setVisibility(true);
      if (screenName == "noRecipient") {
        this.view.flxRightBar.top = "55dp";
        this.configureRightActionButton([]);
      } else if (screenName == "oneTimeTransfer") {
        this.view.flxRightBar.top = "45dp";
        this.configureRightActionButton(['add'])
      } else if (screenName == "addRecipient") {
        this.view.flxRightBar.top = "45dp";
        this.configureRightActionButton(['oneTimeTransfer'])
      } else if (screenName == "WireTransfersWindow") {
        this.view.flxRightBar.top = "45dp";
        this.configureRightActionButton(['add', 'oneTimeTransfer'])
      }
    },
    /**
    * Populate List of states in state listbox
    * @member of frmWireTransferController
    * @returns {object} - None
    * @throws {void} - None
    */

    populateStates: function (states) {
      var data = states.map(function (stateModel) {
        return [stateModel.stateName, stateModel.stateName]
      })
      this.view.AddRecipientAccount.lbxState.masterData = data;
      this.view.oneTimeTransfer.lbxState.masterData = data;
      this.view.AddRecipientAccount.lbxBankState.masterData = data;        
    },

    /**
    * Populate List of countries in countries listbox
    * @member of frmWireTransferController
    * @returns {object} - None
    * @throws {void} - None
    */

    populateCountries: function (countries) {
      var data = countries.map(function (countryModel) {
        return [countryModel.Name, countryModel.Name]
      });
      this.view.oneTimeTransfer.lbxCountry.masterData = data;
      this.view.AddRecipientAccount.lbxCountry.masterData=data;
    },

    /**
    * Entry point for international add form
    * @member of frmWireTransferController
    * @param {function} onCancel - Call back for cancel button    
    * @returns {void} - None
    * @throws {void} - None
    */

    showInternationalAddForm: function (onCancel) {
      var scopeObj = this;
      if (onCancel === undefined || typeof onCancel !== "function") {
        onCancel = function () {
          scopeObj.presenter.showWireTransfer();
        };
      }
      this.view.AddRecipientAccount.btnCancel.onClick = onCancel;
      this.showInternationalAccountUI();
      this.resetAddRecipientForms();        
      this.view.AddRecipientAccount.lbxCountry.masterData = [];        
      this.view.AddRecipientAccount.lbxState.masterData = [];
      this.view.AddRecipientAccount.btnProceed.onClick = this.showInternationalAccountDetailsForm.bind(this);
      this.view.AddRecipientAccount.lbxCountry.onSelection = this.onCountryChanged.bind(this);        
      this.presenter.fetchCountries();
      this.presenter.fetchStates("US");        
    },

    /**
    * Shows the UI for international add 
    * @member of frmWireTransferController
    * @param {void} - None    
    * @returns {void} - None
    * @throws {void} - None
    */


    showInternationalAccountUI: function () {
      this.showAddRecipientsScreen();
      this.showSwiftCodeField();
      this.view.AddRecipientAccount.btnDomesticAccount.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.view.AddRecipientAccount.btnInternationalAccount.skin = "sknbtnffffffNoBorder";
      this.ShowAllSeperators();
      this.view.AddRecipientAccount.flxTabsSeperator1.setVisibility(false);
      this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - INTERNATIONAL";
      this.setAddRecipientsStep(1);
      this.view.AddRecipientAccount.flxCountry.setVisibility(true);
      this.view.AddRecipientAccount.lblIBANOrIRC.setVisibility(true);
      this.view.AddRecipientAccount.tbxIBANOrIRC.setVisibility(true);
      this.view.AddRecipientAccount.flxReason.setVisibility(true);
      this.disableButton(this.view.AddRecipientAccount.btnProceed);
      this.detailsJson.isDomesticVar = "false";
      this.detailsJson.isInternationalVar = "true";
      this.view.forceLayout();
    },

    /**
    * Shows step 2 for international add form.
    * @member of frmWireTransferController
    * @param {void} - None    
    * @returns {void} - None
    * @throws {void} - None
    */

    showInternationalAccountDetailsForm: function () {
      this.setAddRecipientsStep(2);
      if (IBANUtils.isCountrySupportsIBAN(this.getRecipientFormData().recipientCountry)) {
        this.showIBANField();
      }
      else {
        this.showIRCField();
      }
      this.view.AddRecipientAccount.btnBack.onClick = this.setAddRecipientsStep.bind(this, 1);
      this.view.AddRecipientAccount.btnAddRecipent.onClick = function () {
        this.confirmAddRecipient(this.getFullRecipientData(), 'International');
      }.bind(this);
    },

    /**
    * Navigates to transfers module.
    * @member of frmWireTransferController
    * @param {void} - None    
    * @returns {void} - None
    * @throws {void} - None
    */

    NavigateToTransfers: function () {
      //go back to transfers 
      var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      transfersModule.presentationController.showTransferScreen();
    },

    /**
    * Fetch states when country is changed on list box
    * @member of frmWireTransferController
    * @param {void} - None    
    * @returns {void} - None
    * @throws {void} - None
    */

    onCountryChanged: function () {
      var selectedCountry = this.view.AddRecipientAccount.lbxCountry.selectedKey;
      this.presenter.fetchStates(selectedCountry);
    },

    /**
        * Show Iban field
        * @member of frmWireTransferController
        * @returns {void} - None
        * @param {void} - None
        * @throws {void} - None
        */

    showIBANField: function () {
      this.view.AddRecipientAccount.lblIBANOrIRC.text = "International Account Number (IBAN)"
      this.view.AddRecipientAccount.tbxIBANOrIRC.placeholder = "Enter International Account Number";
    },

    /**
        * Show International routing code field. 
        * @member of frmWireTransferController
        * @returns {void} - None
        * @param {void} - None
        * @throws {void} - None
        */

    showIRCField: function () {
      this.view.AddRecipientAccount.lblIBANOrIRC.text = "International Routing Code";//TODO i18n
      this.view.AddRecipientAccount.tbxIBANOrIRC.placeholder = "Enter International Routing Code";//TODO i18n
    },

    /**
        * Show swift code field
        * @member of frmWireTransferController
        * @returns {void} - None
        * @param {void} - None
        * @throws {void} - None
        */

    showSwiftCodeField: function () {
      this.view.AddRecipientAccount.lblSwiftCode.text = kony.i18n.getLocalizedString('i18n.WireTransfer.SwiftCode'); //TODO i18n
      this.view.AddRecipientAccount.tbxSwiftCode.placeholder = "Enter Swift Code"; //TODO i18n
    },

    /**
        * Show routing Code field. 
        * @member of frmWireTransferController
        * @returns {void} - None
        * @param {void} - None
        * @throws {void} - None
        */

    showRoutingCodeField: function () {
      this.view.AddRecipientAccount.lblSwiftCode.text = "Routing Code";
      this.view.AddRecipientAccount.tbxSwiftCode.placeholder = "Enter Routing Code";
    },

    /**
    * UI logic hide all flex 
    * @member of frmWireTransferController
    * @returns {void} - None
    * @param {void} - None
    * @throws {void} - None
    */
    hideAll: function () {
      this.view.flxWireTransfersWindow.setVisibility(false);
      this.view.flxWireTransferActivityWindow.setVisibility(false);
      this.view.flxAddRecipientsWindow.setVisibility(false);
      this.view.flxNoRecipients.setVisibility(false);
      this.view.flxActivateWireTransfer.setVisibility(false);
      this.view.flxConfirmDetails.setVisibility(false);
      this.view.flxRightBar.setVisibility(false);
      this.view.flxOneTimeTransfer.setVisibility(false);
    },

    /**
    * UI logic to show make transfer tab
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */
    MakeTransferAction: function () {
      this.showWireTransfersScreen();
      this.view.WireTransferContainer.btnMakeTransfer.skin = "sknbtnffffffNoBorder";
      this.view.WireTransferContainer.btnRecent.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.view.WireTransferContainer.btnManageRecipient.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.showAllSeperatorsWireTransfer();
      this.view.WireTransferContainer.flxTabsSeperator4.setVisibility(false);
      this.view.breadcrumb.btnBreadcrumb2.text = "MAKE TRANSFER";
      this.view.WireTransferContainer.flxSortMakeTransfers.setVisibility(true);
      this.view.WireTransferContainer.flxSortRecent.setVisibility(false);
      this.showRightBar("WireTransfersWindow");
    },

    /**
    * UI logic to show recent tab
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */
    RecentAction: function () {
      this.showWireTransfersScreen();
      this.view.WireTransferContainer.btnRecent.skin = "sknbtnffffffNoBorder";
      this.view.WireTransferContainer.btnMakeTransfer.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.view.WireTransferContainer.btnManageRecipient.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.showAllSeperatorsWireTransfer();
      this.view.WireTransferContainer.flxTabsSeperator4.setVisibility(false);
      this.view.WireTransferContainer.flxTabsSeperator1.setVisibility(false);
      this.view.breadcrumb.btnBreadcrumb2.text = "RECENT";
      this.view.WireTransferContainer.flxSortMakeTransfers.setVisibility(false);
      this.view.WireTransferContainer.flxSortRecent.setVisibility(true);
      this.showRightBar("WireTransfersWindow");
    },

    /**
    * UI logic to manage recipient tab
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */
    ManageRecipientsAction: function () {
      this.showWireTransfersScreen();
      this.view.WireTransferContainer.btnManageRecipient.skin = "sknbtnffffffNoBorder";
      this.view.WireTransferContainer.btnRecent.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.view.WireTransferContainer.btnMakeTransfer.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.showAllSeperatorsWireTransfer();
      this.view.WireTransferContainer.flxTabsSeperator1.setVisibility(false);
      this.view.breadcrumb.btnBreadcrumb2.text = "MANAGE RECIPIENT";
      this.view.WireTransferContainer.flxSortMakeTransfers.setVisibility(true);
      this.view.WireTransferContainer.flxSortRecent.setVisibility(false);
      this.showRightBar("WireTransfersWindow");
    },

    /**
    * Bind segment data for make transfer tab
    * @member of frmWireTransferController
	* @param {JSON} recipient list
    * @returns {void} - None
    * @throws {void} - None
    */
    setMakeTransferData: function (viewModel) {
      var scopeObj=this;
      var sortMap  = [
        { name : 'nickName' ,  imageFlx : this.view.WireTransferContainer.imgSortAccountName , clickContainer : this.view.WireTransferContainer.flxAccountName},
        { name : 'bankName' ,  imageFlx : this.view.WireTransferContainer.imgSortBankName, clickContainer : this.view.WireTransferContainer.flxBankName},
      ];
      var MakeTransferData = {
        "btnAction": "btnAction",
        "flxAccountName": "flxAccountName",
        "flxAccountType": "flxAccountType",
        "flxBankName": "flxBankName",
        "flxDropdown": "flxDropdown",
        "flxMakeTransfersTransfersUnselected": "flxMakeTransfersTransfersUnselected",
        "flxRow": "flxRow",
        "imgDropdown": "imgDropdown",
        "lblAccountName": "lblAccountName",
        "lblAccountType": "lblAccountType",
        "lblBankName": "lblBankName",
        "lblSeparator": "lblSeparator",
        "flxWireTransferMakeTransfersSelected":"flxWireTransferMakeTransfersSelected",
        "lblIdentifier":"lblIdentifier",
        "flxIdentifier":"flxIdentifier",
        "lblAccountHolder":"lblAccountHolder",
        "lblAccountNumber":"lblAccountNumber",
        "lblRoutingNumber":"lblRoutingNumber",
        "lblAccountNumberValue":"lblAccountNumberValue",
        "lblAccountTypeValue":"lblAccountTypeValue",
        "lblAddedOnValue":"lblAddedOnValue",
        "lblSwiftCode":"lblSwiftCode",
        "lblIBANNo":"lblIBANNo",
        "lblBankDetailsTitle":"lblBankDetailsTitle",
        "lblRoutingNumberValue":"lblRoutingNumberValue",
        "lblIBANNumber":"lblIBANNumber",
        "lblBankAddressValue":"lblBankAddressValue",
        "lblRowSeperator":"lblRowSeperator"
      };
      var segData = viewModel.recipients.map(function(dataItem) {
        return {        
          "btnAction": kony.i18n.getLocalizedString("i18n.transfers.make_transfer"),
          "imgDropdown": "arrow_down.png",
          "lblAccountName": dataItem.payeeNickName,
          "lblAccountType": dataItem.wireAccountType,
          "lblBankName": dataItem.bankName,
          "lblAccountHolder":kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountHolder"),
          "lblAccountNumberValue":dataItem.payeeName,
          "lblAccountNumber":kony.i18n.getLocalizedString("i18n.WireTransfer.AccountNumber"),
          "lblAccountTypeValue":dataItem.payeeAccountNumber,
          "lblSeparator": " ",
          "lblIdentifier":" ",
          "lblRoutingNumber":kony.i18n.getLocalizedString("i18n.accountDetail.routingNumber"),
          "lblAddedOnValue":dataItem.routingCode,
          "lblSwiftCode":kony.i18n.getLocalizedString("i18n.WireTransfer.SwiftCode"),
          "lblRoutingNumberValue":dataItem.swiftCode,
          "lblIBANNo":kony.i18n.getLocalizedString("i18n.WireTransfer.IBAN"),
          "lblIBANNumber":dataItem.IBAN,
          "lblBankDetailsTitle":kony.i18n.getLocalizedString("i18n.WireTransfer.BankDetails"),
          "lblBankAddressValue":scopeObj.returnBankAddress(dataItem),
          "lblRowSeperator":" "
        }
      });
      this.view.WireTransferContainer.segWireTransfers.widgetDataMap = MakeTransferData;
      this.view.WireTransferContainer.segWireTransfers.setData(segData);
      CommonUtilities.Sorting.setSortingHandlers(sortMap, this.onMakeTransferSorting.bind(this), this);
      CommonUtilities.Sorting.updateSortFlex(sortMap, viewModel.config);
      this.configurePagination(this.presenter.fetchWireTransferAccounts.bind(this.presenter, 'wireTransferRecipients'), viewModel.config, segData.length, "i18n.Transfers.Recipients")
      this.view.forceLayout();
    },

    onMakeTransferSorting: function (event, data) {
      this.presenter.fetchWireTransferAccounts("wireTransferRecipients", data);
    },

    /**
    * To return Bank Address
    * @member of frmWireTransferController
	* @param {JSON} recipient list
    * @returns {void} - None
    * @throws {void} - None
    */
    returnBankAddress:function(dataItem){
      var scopeObj=this;
      var bankAdd1=scopeObj.notNull(dataItem.bankAddressLine1);
      var bankAdd2=scopeObj.notNull(dataItem.bankAddressLine2);
      var city=scopeObj.notNull(dataItem.bankCity);
      var state=scopeObj.notNull(dataItem.bankState);
      var zip=scopeObj.notNull(dataItem.bankZip);
      return bankAdd1+" "+bankAdd2+" "+city+" "+state+" "+zip;
    },

    /**
    * Checks if value is not null
    * @member of frmWireTransferController
    * @param {void} - None    
    * @returns {void} - None
    * @throws {void} - None
    */

    notNull: function(value) {
      return value; //TO DO 
    },
    /**
    * Bind segment data for Manage Recipient tab
    * @member of frmWireTransferController
	* @param {JSON} recipient list
    * @returns {void} - None
    * @throws {void} - None
    */
    setManageRecipientrData: function (viewModel) {
      var sortMap  = [
        { name : 'nickName' ,  imageFlx : this.view.WireTransferContainer.imgSortAccountName , clickContainer : this.view.WireTransferContainer.flxAccountName},
        { name : 'bankName' ,  imageFlx : this.view.WireTransferContainer.imgSortBankName, clickContainer : this.view.WireTransferContainer.flxBankName},
      ];
      var scopeObj=this;
      var ManageRecipientData = {
        "btnAction": "btnAction",
        "btnMakeTransfer":"btnMakeTransfer",
        "btnVewActivity":"btnVewActivity",
        "btnDelete":"btnDelete",
        "flxAccountName": "flxAccountName",
        "flxAccountType": "flxAccountType",
        "flxBankName": "flxBankName",
        "flxDropdown": "flxDropdown",
        "flxManageRecipientsSelected1": "flxManageRecipientsSelected1",
        "flxManageRecipientsUnselected1":"flxManageRecipientsUnselected1",
        "flxRow": "flxRow",
        "imgDropdown": "imgDropdown",
        "lblAccountName": "lblAccountName",
        "lblAccountType": "lblAccountType",
        "lblBankName": "lblBankName",
        "lblSeparator": "lblSeparator",
        "flxWireTransferMakeTransfersSelected":"flxWireTransferMakeTransfersSelected",
        "lblIdentifier":"lblIdentifier",
        "flxIdentifier":"flxIdentifier",
        "lblAccountHolder":"lblAccountHolder",
        "lblAccountNumber":"lblAccountNumber",
        "lblRoutingNumber":"lblRoutingNumber",
        "lblAccountNumberValue":"lblAccountNumberValue",
        "lblAccountTypeValue":"lblAccountTypeValue",
        "lblAddedOnValue":"lblAddedOnValue",
        "lblSwiftCode":"lblSwiftCode",
        "lblIBANNo":"lblIBANNo",
        "lblBankDetailsTitle":"lblBankDetailsTitle",
        "lblRoutingNumberValue":"lblRoutingNumberValue",
        "lblIBANNumber":"lblIBANNumber",
        "lblBankAddressValue":"lblBankAddressValue",
        "lblRowSeperator":"lblRowSeperator"
      };
      var segData = viewModel.recipients.map(function(dataItem) {
        return {        
          "btnAction": {
            "text":kony.i18n.getLocalizedString("i18n.ProfileManagement.Edit"),
            "onClick": scopeObj.showEditRecipientUI.bind(scopeObj,dataItem),
          },
          "btnMakeTransfer":kony.i18n.getLocalizedString("i18n.transfers.make_transfer"),
          "btnVewActivity":kony.i18n.getLocalizedString("i18n.PayAPerson.ViewActivity"),
          "btnDelete":kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
          "imgDropdown": "arrow_down.png",
          "lblAccountName": dataItem.payeeNickName,
          "lblAccountType": dataItem. wireAccountType,
          "lblBankName": dataItem.bankName,
          "lblAccountHolder":kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountHolder"),
          "lblAccountNumberValue":dataItem.payeeName,
          "lblAccountNumber":kony.i18n.getLocalizedString("i18n.WireTransfer.AccountNumber"),
          "lblAccountTypeValue":dataItem.payeeAccountNumber,
          "lblSeparator": " ",
          "lblIdentifier":" ",
          "lblRoutingNumber":kony.i18n.getLocalizedString("i18n.accountDetail.routingNumber"),
          "lblAddedOnValue":dataItem.routingCode,
          "lblSwiftCode":kony.i18n.getLocalizedString("i18n.WireTransfer.SwiftCode"),
          "lblRoutingNumberValue":dataItem.swiftCode,
          "lblIBANNo":kony.i18n.getLocalizedString("i18n.WireTransfer.IBAN"),
          "lblIBANNumber":dataItem.IBAN,
          "lblBankDetailsTitle":kony.i18n.getLocalizedString("i18n.WireTransfer.BankDetails"),
          "lblBankAddressValue":scopeObj.returnBankAddress(dataItem),
          "lblRowSeperator":" "
        }
      });
      this.view.WireTransferContainer.segWireTransfers.widgetDataMap = ManageRecipientData;
      this.view.WireTransferContainer.segWireTransfers.setData(segData);
      CommonUtilities.Sorting.setSortingHandlers(sortMap, this.onManageRecipientSorting.bind(this), this);
      CommonUtilities.Sorting.updateSortFlex(sortMap, viewModel.config);
      this.configurePagination(this.presenter.fetchWireTransferAccounts.bind(this.presenter, 'manageRecipients'), viewModel.config, segData.length, "i18n.Transfers.Recipients")
      this.view.forceLayout();
    },

    configurePagination (trigger, config, currentRecordLength, paginationKey) {

      if (currentRecordLength === OLBConstants.PAGING_ROWS_LIMIT) {
        this.view.WireTransferContainer.tablePagination.flxPaginationNext.onClick =  function () {
          trigger({offset: config.offset+ OLBConstants.PAGING_ROWS_LIMIT});
        }
        this.view.WireTransferContainer.tablePagination.imgPaginationNext.src = "pagination_next_active.png";
      }
      else {
        this.view.WireTransferContainer.tablePagination.flxPaginationNext.onClick = null;
        this.view.WireTransferContainer.tablePagination.imgPaginationNext.src = "pagination_next_inactive.png";
      }

      if (config.offset > OLBConstants.DEFAULT_OFFSET) {
        this.view.WireTransferContainer.tablePagination.flxPaginationPrevious.onClick =  function () {
          trigger({offset: config.offset- OLBConstants.PAGING_ROWS_LIMIT});
        }
        this.view.WireTransferContainer.tablePagination.imgPaginationPrevious.src = "pagination_back_active.png";
      }
      else {
        this.view.WireTransferContainer.tablePagination.flxPaginationPrevious.onClick =  null;
        this.view.WireTransferContainer.tablePagination.imgPaginationPrevious.src = "pagination_back_inactive.png";
      }
      this.view.WireTransferContainer.tablePagination.lblPagination.text = ( config.offset + 1 ) + "-" + (config.offset + currentRecordLength) + " " + CommonUtilities.changedataCase(kony.i18n.getLocalizedString(paginationKey));
      this.view.forceLayout();

    },

    onManageRecipientSorting: function (event, data) {
      this.presenter.fetchWireTransferAccounts("manageRecipients", data);
    },

    /**
    * Bind segment data for Recent tab
    * @member of frmWireTransferController
	* @param {JSON} Wire transfer transactions
    * @returns {void} - None
    * @throws {void} - None
    */
    setRecentData: function (transactions) {
      var widgetDataMap = {
        "btnAction": "btnAction",
        "flxAmount": "flxAmount",
        "flxDate": "flxDate",
        "flxDropdown": "flxDropdown",
        "flxRecentWireTransfers": "flxRecentWireTransfers",
        "flxRow": "flxRow",
        "flxSendTo": "flxSendTo",
        "flxIdentifier": "flxIdentifier",
        "imgDropdown": "imgDropdown",
        "lblAmount": "lblAmount",
        "lblDate": "lblDate",
        "lblSendTo": "lblSendTo",
        "lblAmount":"lblAmount",
        "lblFrom":"lblFrom",
        "lblReferenceNumberValue":"lblReferenceNumberValue",
        "lblReferenceNumber":"lblReferenceNumber",
        "lblFromAccountValue":"lblFromAccountValue",
        "lblRoutingNumber":"lblRoutingNumber",
        "lblStatusValue":"lblStatusValue",
        "lblAccountNumber":"lblRecurrenceValue",
        "lblRecurrenceValue":"lblRecurrenceValue",
        "lblAccountType":"lblAccountType",
        "lblNoteValue":"lblNoteValue",
        "lblBankDetailsTitle":"lblBankDetailsTitle",
        "lblBankAddressValue": "lblBankAddressValue",
        "lblSeparator": "lblSeparator"
      };
      var data = transactions.map(function(dataItem) {
        return {        
          "btnAction": kony.i18n.getLocalizedString('i18n.transfers.repeat'),
          "imgDropdown": "arrow_down.png",
          "lblDate": dataItem.transactionDate,
          "lblSendTo": dataItem.payeeNickName,
          "lblAmount":CommonUtilities.formatCurrencyWithCommas(dataItem.amount),
          "lblFrom":kony.i18n.getLocalizedString("i18n.PayAPerson.From"),
          "lblReferenceNumberValue": dataItem.fromAccount,
          "lblReferenceNumber":kony.i18n.getLocalizedString('i18n.transfers.RefrenceNumber'),
          "lblFromAccountValue":dataItem.referenceNumber,
          "lblRoutingNumber": kony.i18n.getLocalizedString('i18n.accounts.routingNumber'),
          "lblStatusValue":dataItem.routingNumber,
          "lblAccountNumber": kony.i18n.getLocalizedString('i18n.accounts.accountNumber'),
          "lblRecurrenceValue":dataItem.fromAccountNumber,
          "lblAccountType":kony.i18n.getLocalizedString('i18n.transfers.accountType'),
          "lblNoteValue":dataItem.nationality,
          "lblBankDetailsTitle":kony.i18n.getLocalizedString('i18n.WireTransfer.BankDetails'),
          "lblBankAddressValue": dataItem.bankName,
          "lblSeparator": ""
        }
      });
      this.view.WireTransferContainer.segWireTransfers.widgetDataMap = widgetDataMap;
      this.view.WireTransferContainer.segWireTransfers.setData(data);
      this.view.forceLayout();
    },
    /**
    * Creates view model for right bar payment accounts.
    * @member of frmWireTransferController
    * @param {array} - Array of accounts model objects     
    * @returns {void} - list of view models for every row of payment accounts.
    * @throws {void} - None
    */
    createAccountSegmentsModel: function (accounts) {
      var accountTypeConfig = {
        'Savings': {
          skin: "sknFlx26D0CE",
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
        },
        'Checking': {
          skin: "sknFlx9060b7",
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
        },
        'CreditCard': {
          skin: "sknFlxF4BA22",
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
        },
        'Deposit': {
          skin: "sknFlx4a90e2Border1pxRound",
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
        },
        'Mortgage': {
          skin: 'sknFlx4a90e2Border1pxRound',
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
        },
        'Default': {
          skin: 'sknFlx26D0CE',
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        }
      };
      var balanceInDollars = function (amount) {
        return '$' + amount;
      };
      return Object.keys(accountTypeConfig).map(function (accountType) {
        return accounts.filter(function (account) {
          return account.type === accountType;
        });
      }).map(function (accounts) {
        return accounts.map(function (account) {
          return {

            "flxLeft": {
              "skin": accountTypeConfig[account.type].skin
            },
            "lblLeft": " ",
            "lblAccountName": account.nickName || account.accountName,
            "lblBalance": account[accountTypeConfig[account.type].balanceKey],
            "lblAvailableBalance": accountTypeConfig[account.type].balanceTitle
          };
        });
      }).reduce(function (p, e) {
        return p.concat(e);
      }, []);
    },

    /**
    * Renders payment accounts on right bar
    * @member of frmWireTransferController
    * @param {array} - Array of accounts model objects     
    * @returns {void} - None
    * @throws {void} - None
    */

    updatePaymentAccounts: function (paymentAccounts) {
      this.view.mypaymentAccounts.segMypaymentAccounts.widgetDataMap = {
        "lblLeft": "lblLeft",
        "lblAccountName": "lblAccountName",
        "flxLeft": "flxLeft",
        "lblBalance": "lblBalance",
        "lblAvailableBalance": "lblAvailableBalance"
      };
      this.view.mypaymentAccounts.segMypaymentAccounts.setData(this.createAccountSegmentsModel(paymentAccounts));
      this.view.forceLayout();
    },   
    /**
    * Shows data forconfirmation UI
    * @member of frmWireTransferController
    * @param {JSON} - data of add recipient form
    * @param {string} - type of recipient 'domestic'/'international'   
    * @returns {void} - None
    * @throws {void} - None
    */

    confirmAddRecipient: function (data, type) {
      //recipient details
      this.showAddRecipientConfirmUI(type);
      this.view.confirmDetails.lblDetailsKey1.text = "Recipient Name :";
      this.view.confirmDetails.rtxDetailsValue1.text = data.recipientDetails.recipientName;
      this.view.confirmDetails.lblDetailsKey2.text = "Recipient Type :";
      this.view.confirmDetails.rtxDetailsValue2.text = data.recipientDetails.recipientType;
      this.view.confirmDetails.lblDetailsKey3.text = "Account Type :";
      this.view.confirmDetails.rtxDetailsValue3.text = type;
      this.view.confirmDetails.lblDetailsKey4.text = "Recipient Address :";
      this.view.confirmDetails.rtxDetailsValue4.text = data.recipientDetails.recipientAddressLine1;
      //bank details
      this.view.confirmDetails.lblBankDetailsKey3.text = "Recipient Account Number :";
      this.view.confirmDetails.rtxBankDetailsValue3.text = data.recipientAccountDetails.recipientAccountNumber;
      this.view.confirmDetails.lblBankDetailsKey5.text = "Account Nick Name :";
      this.view.confirmDetails.rtxBankDetailsValue5.text = data.recipientAccountDetails.recipientAccountNickName;
      this.view.confirmDetails.lblBankDetailsKey6.text = "Recipient Bank & Address :";
      this.view.confirmDetails.rtxBankDetailsValue6.text = data.recipientAccountDetails.recipientBankAddressLine1;

      //location specific bank details
      if (type === "Domestic") {
        this.view.confirmDetails.lblBankDetailsKey1.text = "Routing Number :";
        this.view.confirmDetails.rtxBankDetailsValue1.text = data.recipientAccountDetails.recipientAccountRoutingCode;
      } else if (type === "International" && IBANUtils.isCountrySupportsIBAN(data.recipientDetails.country)) {
        if (IBANUtils.isCountrySupportsIBAN(data.recipientDetails.country)) {
          this.view.confirmDetails.lblBankDetailsKey2.text = "International Bank Account Number (IBAN)";
          this.view.confirmDetails.rtxBankDetailsValue2.text = data.recipientAccountDetails.recipientAccountIBAN                                                            
        }
        else {
          this.view.confirmDetails.lblBankDetailsKey2.text = "International Routing No :";                            
          this.view.confirmDetails.rtxBankDetailsValue2.text = data.recipientAccountDetails.recipientAccountIRC
        }
        this.view.confirmDetails.lblBankDetailsKey1.text = "Swift code :";
        this.view.confirmDetails.rtxBankDetailsValue1.text =data.recipientAccountDetails.recipientAccountSwiftCode;
      }
      this.view.confirmDetails.btnCancel.onClick = this.showConfirmDialog.bind(this,"i18n.PayAPerson.Quit", "i18n.wireTransfers.cancelAddRecipient",function () {
        this.presenter.showWireTransfer();
      }.bind(this))
      this.view.confirmDetails.btnModify.onClick = this.setAddRecipientsStep.bind(this, 1);
      this.view.confirmDetails.btnConfirm.onClick = this.saveRecipient.bind(this, type);
    },



    /**
    * Shows UI for add recipient Confirmation
    * @member of frmWireTransferController
    * @param {JSON} - data of add recipient form
    * @returns {void} - None
    * @throws {void} - None
    */


    showAddRecipientConfirmUI: function (type) {
      this.hideAll();

      this.view.flxConfirmDetails.setVisibility(true);
      this.view.lblAddAccountHeading.text = "Add Recipient - Confirm";        
      this.view.confirmDetails.flxTransactionDetails.setVisibility(false);                        
      this.view.flxTermsAndConditions.setVisibility(false);        
      this.view.flxSuccessMessage.setVisibility(false);   
      this.view.flxbuttons.setVisibility(false);
      this.view.confirmDetails.flxStep2Buttons.setVisibility(true);
      this.view.lblAddAccountHeading.setVisibility(true);
      this.view.flxPrint.setVisibility(false);
      this.view.flxDownload.setVisibility(false);
      this.view.confirmDetails.flxSeparator.setVisibility(false);
      if (type === "Domestic") {
        this.view.confirmDetails.flxRow2.setVisibility(false);
        this.view.breadcrumb.setBreadcrumbData([{
          text: "WIRE TRANSFER"
        }, {
          text: "ADD RECIPIENT - DOMESTIC - CONFIRMATION"
        }]);
      }
      else {
        this.view.confirmDetails.flxRow2.setVisibility(true);
        this.view.breadcrumb.setBreadcrumbData([{
          text: "WIRE TRANSFER"
        }, {
          text: "ADD RECIPIENT - INTERNATIONAL - CONFIRMATION"
        }]);
      }
      this.view.confirmDetails.flxRow4.setVisibility(false);        
      this.view.confirmDetails.flxRecipientDetails.frame.height = this.view.confirmDetails.flxBankDetails.frame.height;
      this.view.forceLayout();        

    },

    /**
    * Shows recipient data on acknowledgement screen
    * @member of frmWireTransferController
    * @param {JSON} - data of add recipient form
    * @returns {void} - None
    * @throws {void} - None
    */


    showAddAccountAcknowledgement: function (data) {
      this.showAddAccountAcknowledgementUI(data);
      this.view.lblSuccessAcknowledgement.text = data.recipientAccountDetails.recipientAccountNickName + " has been added successfully";
      this.view.btnMakeAnotherWireTransfer.onClick = function () {
        this.presenter.fetchWireTransferAccounts();
      }.bind(this);
    },

    /**
    * Shows UI for add account acknowledgement
    * @member of frmWireTransferController
    * @param {JSON} - data of add recipient form
    * @returns {void} - None
    * @throws {void} - None
    */

    showAddAccountAcknowledgementUI: function (data) {
      this.view.btnSaveRecipient.setVisibility(false);
      this.view.lblAddAccountHeading.setVisibility(true);
      this.view.flxPrint.setVisibility(true);
      this.view.flxDownload.setVisibility(true);
      this.view.flxSuccessMessage.setVisibility(true);
      this.view.btnSaveRecipient.setVisibility(false);
      this.view.btnMakeAnotherWireTransfer.text = "MAKE WIRE TRANSFER";
      this.view.btnViewSentTransactions.text = "ADD ANOTHER RECIPIENT";
      this.view.lblAddAccountHeading.text = "Add Recipient - Acknowledgement";
      this.view.btnMakeAnotherWireTransfer.text = "MAKE WIRE TRANSFER";
      this.view.btnViewSentTransactions.text = "ADD ANOTHER RECIPIENT";
      if (data.wireAccountType === "Domestic") {
        this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT DOMESTIC - ACKNOWLEDGEMENT";
      }
      else {
        this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT INTERNATIONAL - ACKNOWLEDGEMENT";
      }
      this.view.lblRefrenceNumber.setVisibility(false);
      this.view.lblRefrenceNumberValue.setVisibility(false);
      this.view.flxbuttons.setVisibility(true);
      this.view.confirmDetails.flxStep2Buttons.setVisibility(false);
      this.view.confirmDetails.flxRecipientDetails.frame.height = this.view.confirmDetails.flxBankDetails.frame.height;
      this.view.forceLayout();

    },

    /**
    * Show Edit Recipient UI 
    * @member of frmWireTransferController
	* @param {void} - None
    * @returns {void} - None
    * @throws {void} - None
    */
    showEditRecipientUI:function(dataItem){
      var scopeObj=this;
      this.view.oneTimeTransfer.btnCancel.onClick = scopeObj.presenter.fetchWireTransferAccounts.bind(scopeObj.presenter,"manageRecipients");
      this.view.oneTimeTransfer.lbxCountry.masterData = [];        
      this.view.oneTimeTransfer.lbxState.masterData = [];
      this.presenter.fetchCountries();
      this.presenter.fetchStates("US");
      var countryShow=(dataItem.nationality=="Domestic")?false:true;
      scopeObj.hideAll();
      scopeObj.view.flxOneTimeTransfer.setVisibility(true);
      scopeObj.showRightBar("addRecipient");
      scopeObj.view.oneTimeTransfer.lblHeader.text=kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient");
      scopeObj.view.oneTimeTransfer.flxAccountType.setVisibility(false);
      scopeObj.view.oneTimeTransfer.flxCountry.setVisibility(countryShow);
      scopeObj.view.flxAddKonyAccount.setVisibility(false);
      scopeObj.view.flxAddNonKonyAccount.setVisibility(false);
      scopeObj.enableButton(this.view.oneTimeTransfer.btnProceed);
      this.view.oneTimeTransfer.btnProceed.onClick = this.editRecipient.bind(scopeObj,dataItem.payeeId);
      this.view.oneTimeTransfer.tbxRecipientName.text=dataItem.payeeName;
      if(dataItem.type=='Individual') {
        this.view.oneTimeTransfer.imgTypeRadio1.src="radiobtn_active_small.png";
        this.view.oneTimeTransfer.imgTypeRadio2.src="icon_radiobtn.png";
      }
      else{
        this.view.oneTimeTransfer.imgTypeRadio1.src="icon_radiobtn.png";
        this.view.oneTimeTransfer.imgTypeRadio2.src="radiobtn_active_small.png";
      }
      this.view.oneTimeTransfer.tbxAddressLine1.text=dataItem.addressLine1;
      this.view.oneTimeTransfer.tbxAddressLine2.text=dataItem.addressLine2;
      this.view.oneTimeTransfer.tbxCity.text=dataItem.cityName;
      this.view.oneTimeTransfer.tbxZipcode.text=dataItem.zipCode;
      scopeObj.view.forceLayout();
    },
    /**
    * Edit Recipient
    * @member of frmWireTransferController
	* @param {string} - payeeId
    * @returns {void} - None
    * @throws {void} - None
    */
    editRecipient:function(payeeId){
      var updateRecipientData={
        "payeeName": this.view.oneTimeTransfer.tbxRecipientName.text.trim(),
        "zipCode": this.view.oneTimeTransfer.tbxZipcode.text.trim(),
        "cityName": this.view.oneTimeTransfer.tbxCity.text.trim(),
        "state": this.view.oneTimeTransfer.lbxState.selectedKey,
        "addressLine1": this.view.oneTimeTransfer.tbxAddressLine1.text.trim(),
        "addressLine2": this.view.oneTimeTransfer.tbxAddressLine2.text.trim(),
        "type": this.view.oneTimeTransfer.imgTypeRadio1.src === 'radiobtn_active_small.png' ? 'Individual' : 'Business',
        "country": this.view.oneTimeTransfer.lbxCountry.selectedKey,
        "payeeId":payeeId
      };
      this.presenter.updateRecipient(updateRecipientData);
    },

    /**
    * Show Confirm Dialog
    * @member of frmWireTransferController
    * @param headineKey {string} - i18n of heading
    * @param messageKey {string} - i18n of message
    * @param onYesPressed {function} - callback for yes button 
    * @returns {void} - None
    * @throws {void} - None
    */

    showConfirmDialog: function (headingKey, messageKey, onYesPressed) {
      this.view.flxPopup.setVisibility(true);
      this.view.flxPopup.setFocus(true);
      this.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString(headingKey);
      this.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString(messageKey);;
      this.view.forceLayout();
      //Quit Pop-Up Message Box
      this.view.CustomPopup.flxCross.onClick = function () {
        this.view.flxPopup.setVisibility(false);
      }.bind(this);
      this.view.CustomPopup.btnNo.onClick = function () {
        this.view.flxPopup.setVisibility(false);
      }.bind(this);

      this.view.CustomPopup.btnYes.onClick = onYesPressed;
    },

    /**
    * Entry point method for one time transfer  
    * @member of frmWireTransferController
	* @param onCancel {function} - Optional callback for cancel button
    * @returns {void} - None
    * @throws {void} - None
    */


    showOneTimeTransfer: function (onCancel) {
      this.resetOneTimeTransferForms();
      if (onCancel === undefined || typeof onCancel !== "function") {
        onCancel = function () {
          this.presenter.showWireTransfer();
        }.bind(this);
      }
      this.showOneTimeTransferUI();
      this.view.oneTimeTransfer.flxAccountTypeRadio1.onClick = function() {
        this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnAccountType1,this.view.oneTimeTransfer.imgRadioBtnAccountType2);
        this.checkAndShowCountryFieldinOTT();
      }.bind(this);
      this.view.oneTimeTransfer.flxAccountTypeRadio2.onClick = function() {
        this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnAccountType2,this.view.oneTimeTransfer.imgRadioBtnAccountType1);
        this.checkAndShowCountryFieldinOTT();
      }.bind(this);

      this.view.oneTimeTransfer.flxTypeRadio1.onClick = function() {
        this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnRecipient1,this.view.oneTimeTransfer.imgRadioBtnRecipient2);
      }.bind(this);
      this.view.oneTimeTransfer.flxTypeRadio2.onClick = function() {
        this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnRecipient2,this.view.oneTimeTransfer.imgRadioBtnRecipient1);
      }.bind(this);
      this.view.AddRecipientAccount.lbxCountry.onSelection = this.onCountryChanged.bind(this);                
      this.view.oneTimeTransfer.btnCancel.onClick = onCancel;
      this.view.oneTimeTransfer.btnProceed.onClick = this.showOneTimeTransferAccountDetailsForm.bind(this, onCancel);      
      this.presenter.fetchCountries();
      this.presenter.fetchStates("US");
    },

    /**
    * Check Radio box value and show/ hide country field in OTT
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */


    checkAndShowCountryFieldinOTT: function () {
      var showCountryField = this.view.oneTimeTransfer.imgRadioBtnAccountType2.text === "R";
      this.view.oneTimeTransfer.flxCountry.setVisibility(showCountryField)
    },

    /**
    * Shows Account details form for one time transfer and binds actions
    * @member of frmWireTransferController
	* @param onCancel {function} - Optional callback for cancel button
    * @returns {void} - None
    * @throws {void} - None
    */




    showOneTimeTransferAccountDetailsForm: function (onCancel) {
      this.setOneTimeTransferStep(2);
      this.ShowAllStep2();
      var step1Data = this.getOTTRecipientFormData();
      if (step1Data.recipientAccountType === "Domestic") {
        this.view.oneTimeTransfer.lblSwiftCode.text = "Routing Code"
        this.view.oneTimeTransfer.tbxSwiftCode.placeholder = "Enter Routing Code";
        this.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(false);
        this.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(false);
      }
      else {
        this.view.oneTimeTransfer.lblSwiftCode.text = kony.i18n.getLocalizedString('i18n.WireTransfer.SwiftCode');;
        this.view.oneTimeTransfer.tbxSwiftCode.placeholder = "Enter Swift Code";
        this.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(true);
        this.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(true);
        if (IBANUtils.isCountrySupportsIBAN(step1Data.recipientCountry)) {
          this.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(true);
          this.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(true);
          this.view.oneTimeTransfer.lblIBANOrIRC.text = "International Account Number (IBAN)"
          this.view.oneTimeTransfer.tbxIBANOrIRC.placeholder = "Enter International Account Number";
        }
        else {
          this.view.AddRecipientAccount.lblIBANOrIRC.text = "International Routing Code";//TODO i18n
          this.view.AddRecipientAccount.tbxIBANOrIRC.placeholder = "Enter International Routing Code";//TODO i18n
        }
      }
      this.view.oneTimeTransfer.btnStep2Proceed.onClick = this.showOneTimeTransferTransactionDetails.bind(this, onCancel);
      this.view.oneTimeTransfer.btnBack.onClick = this.setOneTimeTransferStep.bind(this, 1);
      this.view.oneTimeTransfer.btnStep2Cancel.onClick = onCancel;
    },


    /**
    * Shows transactions details form for one time transfer and binds actions
    * @member of frmWireTransferController
	* @param onCancel {function} - Optional callback for cancel button
    * @returns {void} - None
    * @throws {void} - None
    */

    showOneTimeTransferTransactionDetails: function (onCancel) {
      this.setOneTimeTransferStep(3);
      var step2Data = this.getOTTRecipientAccountFormData();
      this.view.oneTimeTransfer.lblToValue.text = step2Data.recipientAccountNickName;
      this.view.oneTimeTransfer.lblBankValue.text = step2Data.recipientBankName;
      this.view.oneTimeTransfer.btnStep3Back.onClick = this.setOneTimeTransferStep.bind(this, 2);
      this.view.oneTimeTransfer.btnStep3Cancel.onClick = onCancel;
      this.presenter.getCheckingAccounts();
      this.view.oneTimeTransfer.btnStep3MakeTransfer.onClick = this.confirmOneTimeTransfer.bind(this);
    },

    /**
    * Toggles between steps of one time transfer
    * @member of frmWireTransferController
	* @param step {number} - Number of step from 1 to 3
    * @returns {void} - None
    * @throws {void} - None
    */


    setOneTimeTransferStep: function(step) {
      if (step > 0 && step < 4) {
        this.view.oneTimeTransfer.flxStep1.setVisibility(false);
        this.view.oneTimeTransfer.flxStep2.setVisibility(false);
        this.view.oneTimeTransfer.flxStep3.setVisibility(false);
        this.view.oneTimeTransfer["flxStep" + step].setVisibility(true);
      }
    },

    /**
    * Resets form Show one time transfer UI 
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */




    showOneTimeTransferUI: function() {
      this.hideAll();
      this.view.flxOneTimeTransfer.setVisibility(true);
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString('i18n.WireTransfer.WireTranferCapson')  
      },
                                              {
                                                text: kony.i18n.getLocalizedString('i18n.WireTransfer.MakeOneTimeTransfer')
                                              }
                                             ])
      this.disableButton(this.view.oneTimeTransfer.btnProceed);
      this.showRightBar("oneTimeTransfer");
      this.view.flxSeperator3.setVisibility(false);
      this.view.forceLayout();
    },


    /**
    * Resets all the fields of step 2 OTT
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */

    ShowAllStep2: function() {
      this.view.oneTimeTransfer.flxSwiftCode.setVisibility(true);
      this.view.oneTimeTransfer.flxAccountNumber.setVisibility(true);
      this.view.oneTimeTransfer.flxReAccountNumber.setVisibility(true);
      this.view.oneTimeTransfer.flxReason.setVisibility(true);
      this.view.oneTimeTransfer.flxNickName.setVisibility(true);
      this.view.oneTimeTransfer.flxBankName.setVisibility(true);
      this.view.oneTimeTransfer.flxBankAddress1.setVisibility(true);
      this.view.oneTimeTransfer.flxBankCityStateZipcode.setVisibility(true);
    },

    /**
    * Configure right action button based on a config
    * @member of frmWireTransferController
    * @param config  config for buttons 
    * @returns {void} - None
    * @throws {void} - None
    */

    configureRightActionButton: function (config) {
      if (config.constructor === Array) {
        var showAddButtons = config.indexOf('add') > -1;
        var showMakeOneTimeTransfer = config.indexOf('oneTimeTransfer') > -1;
        this.view.flxAddKonyAccount.setVisibility(showAddButtons);
        this.view.flxAddNonKonyAccount.setVisibility(showAddButtons);
        this.view.flxAddInternationalAccount.setVisibility(showMakeOneTimeTransfer);
      }
    },

    /**
    * Get Recipient Account Details for One time transfer (Step 2 data) 
    * @member of frmWireTransferController
    * @returns {JSON} - JSON for data
    * @throws {void} - None
    */



    getOTTRecipientAccountFormData: function () {
      return {
        recipientAccountNickName: this.view.oneTimeTransfer.tbxNickName.text.trim(),
        recipientAccountRoutingCode: this.view.oneTimeTransfer.tbxSwiftCode.text.trim(),
        recipientAccountSwiftCode: this.view.oneTimeTransfer.tbxSwiftCode.text.trim(),
        recipientAccountIBAN: this.view.oneTimeTransfer.tbxIBANOrIRC.text.trim(),
        recipientAccountIRC: this.view.oneTimeTransfer.tbxIBANOrIRC.text.trim(),
        recipientAccountNumber: this.view.oneTimeTransfer.tbxAccountNumber.text.trim(),
        recipientAccountNumberConfirm: this.view.oneTimeTransfer.tbxReAccountNumber.text.trim(),
        recipientReasonForTransfer:this.view.oneTimeTransfer.tbxReason.text.trim(),
        recipientAccountNickName: this.view.oneTimeTransfer.tbxNickName.text.trim(),
        recipientBankName: this.view.oneTimeTransfer.tbxBankName.text.trim(),
        recipientBankAddressLine1: this.view.oneTimeTransfer.tbxBankAddressLine1.text.trim(),
        recipientBankAddressLine2: this.view.oneTimeTransfer.tbxBankAddressLine2.text.trim(),
        recipientBankCity: this.view.oneTimeTransfer.tbxBankCity.text.trim(),
        recipientBankState: this.view.oneTimeTransfer.lbxBankState.selectedKey,
        recipientBankZipcode: this.view.oneTimeTransfer.tbxBankZipcode.text.trim()
      }
    },

    /**
    * Get Recipient  Details for One time transfer (Step 1 data) 
    * @member of frmWireTransferController
    * @returns {JSON} - JSON for data
    * @throws {void} - None
    */


    getOTTRecipientFormData: function () {
      return {
        recipientAccountType: this.view.oneTimeTransfer.imgRadioBtnAccountType1.text === "R" ? 'Domestic' : 'International',
        recipientName: this.view.oneTimeTransfer.tbxRecipientName.text.trim(),
        recipientType: this.view.oneTimeTransfer.imgRadioBtnRecipient1.src === 'R' ? 'Individual' : 'Business',
        recipientAddressLine1: this.view.oneTimeTransfer.tbxAddressLine1.text.trim(),
        recipientAddressLine2: this.view.oneTimeTransfer.tbxAddressLine2.text.trim(),
        recipientCity: this.view.oneTimeTransfer.tbxCity.text.trim(),
        recipientState: this.view.oneTimeTransfer.lbxState.selectedKey,
        recipientCountry: this.view.oneTimeTransfer.lbxCountry.selectedKey,
        recipientZipcode: this.view.oneTimeTransfer.tbxZipcode.text.trim()
      };
    },

    /**
    * Get Transaction  Details for One time transfer (Step 3 data) 
    * @member of frmWireTransferController
    * @returns {JSON} - JSON for data
    * @throws {void} - None
    */

    getOTTTransactionDetails: function () {
      return {
        fromAccountNumber: this.view.oneTimeTransfer.lbxStep3From.selectedKey,
        fromAccountName: this.view.oneTimeTransfer.lbxStep3From.selectedKeyValue[1],
        currency: this.view.oneTimeTransfer.lbxStep3Currency.selectedKey,
        amount: this.view.oneTimeTransfer.tbxAmount.text.trim(),
        notes: this.view.oneTimeTransfer.tbxNote.text.trim()
      }
    },

    /**
    * Get Full  Details for One time transfer (All Step  data) 
    * @member of frmWireTransferController
    * @returns {JSON} - JSON for data
    * @throws {void} - None
    */

    getFullOTTDetails: function () {
      return {
        recipientAccountDetails: this.getOTTRecipientAccountFormData(),
        recipientDetails: this.getOTTRecipientFormData(),
        transactionDetails: this.getOTTTransactionDetails()
      }
    },

    /**
    * Returns list box data for a account (OTT) 
    * @member of frmWireTransferController
    * @returns {JSON} - Key value pair in form of array
    * @throws {void} - None
    */

    listBoxMapperForAccounts: function (fromAccount) {
      return [fromAccount.accountID, fromAccount.nickName];
    },

    /**
    * Populates Accounts in list boxes
    * @member of frmWireTransferController
    * @returns {void} - None
    * @throws {void} - None
    */




    populateAccountsinListBox: function (accounts) {
      this.view.oneTimeTransfer.lbxStep3From.masterData = accounts.map(this.listBoxMapperForAccounts); 
    },

    confirmOneTimeTransfer: function () {
      var data = this.getFullOTTDetails();
      this.showOneTimeTransferConfirmUI(data.recipientDetails.recipientAccountType);
      this.view.confirmDetails.lblDetailsKey1.text = "Recipient Name :";
      this.view.confirmDetails.rtxDetailsValue1.text = data.recipientDetails.recipientName;
      this.view.confirmDetails.lblDetailsKey2.text = "Recipient Type :";
      this.view.confirmDetails.rtxDetailsValue2.text = data.recipientDetails.recipientType;
      this.view.confirmDetails.lblDetailsKey3.text = "Account Type :";
      this.view.confirmDetails.rtxDetailsValue3.text = data.recipientDetails.recipientAccountType;
      this.view.confirmDetails.lblDetailsKey4.text = "Recipient Address :";
      this.view.confirmDetails.rtxDetailsValue4.text = [data.recipientDetails.recipientAddressLine1,
                                                        data.recipientDetails.recipientAddressLine2, 
                                                        data.recipientDetails.recipientCity, 
                                                        data.recipientDetails.recipientState, 
                                                        data.recipientDetails.recipientCountry]
        .filter(function(line) {return line !== ""})
        .join(", ");
      //bank details
      this.view.confirmDetails.lblBankDetailsKey3.text = "Recipient Account Number :";
      this.view.confirmDetails.rtxBankDetailsValue3.text = data.recipientAccountDetails.recipientAccountNumber;
      this.view.confirmDetails.lblBankDetailsKey5.text = "Account Nick Name :";
      this.view.confirmDetails.rtxBankDetailsValue5.text = data.recipientAccountDetails.recipientAccountNickName;
      this.view.confirmDetails.lblBankDetailsKey6.text = "Recipient Bank & Address :";
      this.view.confirmDetails.rtxBankDetailsValue6.text =[data.recipientAccountDetails.recipientBankAddressLine1,
                                                           data.recipientAccountDetails.recipientBankAddressLine2,
                                                           data.recipientAccountDetails.recipientBankCity,
                                                           data.recipientAccountDetails.recipientBankState,
                                                          ]
        .filter(function (line) { return line !== "" })
        .join(", ");

      //location specific bank details
      if (data.recipientDetails.recipientAccountType === "Domestic") {
        this.view.confirmDetails.lblBankDetailsKey1.text = "Routing Number :";
        this.view.confirmDetails.rtxBankDetailsValue1.text = data.recipientAccountDetails.recipientAccountRoutingCode;
      } else if (data.recipientDetails.recipientAccountType === "International" && IBANUtils.isCountrySupportsIBAN(data.recipientDetails.country)) {
        if (IBANUtils.isCountrySupportsIBAN(data.recipientDetails.country)) {
          this.view.confirmDetails.lblBankDetailsKey2.text = "International Bank Account Number (IBAN)";
          this.view.confirmDetails.rtxBankDetailsValue2.text = data.recipientAccountDetails.recipientAccountIBAN                                                            
        }
        else {
          this.view.confirmDetails.lblBankDetailsKey2.text = "International Routing No :";                            
          this.view.confirmDetails.rtxBankDetailsValue2.text = data.recipientAccountDetails.recipientAccountIRC
        }
        this.view.confirmDetails.lblBankDetailsKey1.text = "Swift code :";
        this.view.confirmDetails.rtxBankDetailsValue1.text = data.recipientAccountDetails.recipientAccountSwiftCode;
      }
      //  Transaction Details
      this.view.confirmDetails.rtxFromAccountValue.text = data.transactionDetails.fromAccountName;
      this.view.confirmDetails.rtxToAccountValue.text = data.recipientAccountDetails.recipientAccountNickName;
      this.view.confirmDetails.rtxAmountValue.text = CommonUtilities.formatCurrencyWithCommas(data.transactionDetails.amount);
      this.view.confirmDetails.rtxTransactionFeeValue.text = CommonUtilities.formatCurrencyWithCommas(10);
      this.view.confirmDetails.rtxNoteValue.text = data.transactionDetails.notes;
      this.view.confirmDetails.rtxCurrencyValue.text = data.transactionDetails.currency;
      this.view.confirmDetails.btnCancel.onClick = this.showConfirmDialog.bind(this,"i18n.PayAPerson.Quit", "i18n.wireTransfers.cancelAddRecipient",function () {
        this.presenter.showWireTransfer();
      }.bind(this));

      this.view.confirmDetails.btnModify.onClick = this.setOneTimeTransferStep.bind(this, 3);
      this.view.confirmDetails.btnConfirm.onClick = this.createOneTimeTransfer.bind(this);

    },

    createOneTimeTransfer: function () {
      var data = this.getFullOTTDetails();
      this.presenter.createOneTimeTransfer(data);
    },

    showOneTimeTransferConfirmUI: function (type) {
      this.hideAll();
      this.view.flxConfirmDetails.setVisibility(true);
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.wireTransfers.makeTransferConfirm');        
      this.view.confirmDetails.flxTransactionDetails.setVisibility(true);                        
      this.view.flxTermsAndConditions.setVisibility(false);        
      this.view.flxSuccessMessage.setVisibility(false);   
      this.view.flxbuttons.setVisibility(false);
      this.view.confirmDetails.flxStep2Buttons.setVisibility(true);
      this.view.lblAddAccountHeading.setVisibility(true);
      this.view.flxPrint.setVisibility(false);
      this.view.flxDownload.setVisibility(false);
      this.view.confirmDetails.flxSeparator.setVisibility(false);
      if (type === "Domestic") {
        this.view.confirmDetails.flxRow2.setVisibility(false);
      }
      else {
        this.view.confirmDetails.flxRow2.setVisibility(true);
      }
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.WireTransfers.WireTransfer")
      }, {
        text: kony.i18n.getLocalizedString('i18n.wireTransfer.makeOneTimeTransferConfirmation')
      }]);
      this.view.confirmDetails.flxRow4.setVisibility(true);        
      this.view.confirmDetails.flxRecipientDetails.frame.height = this.view.confirmDetails.flxBankDetails.frame.height;
      this.view.forceLayout(); 
    },

    showOneTimeTransferAcknowledgement: function (data) {
      this.showOneTimeTransferAcknowledgementUI(data);
      this.view.lblRefrenceNumberValue.text = data.referenceNumber;
      this.view.btnMakeAnotherWireTransfer.onClick = this.showOneTimeTransfer.bind(this);
      this.view.btnSaveRecipient.onClick = this.confirmAddRecipient.bind(this, data);
      this.view.btnViewSentTransactions.onClick = this.presenter.showWireTransfer.bind(this.presenter);
    },

    showOneTimeTransferAcknowledgementUI: function (data) {
      this.view.btnSaveRecipient.setVisibility(true);
      this.view.lblAddAccountHeading.setVisibility(true);
      this.view.flxPrint.setVisibility(true);
      this.view.flxDownload.setVisibility(true);
      this.view.flxSuccessMessage.setVisibility(true);
      this.view.btnSaveRecipient.setVisibility(true);
      this.view.btnMakeAnotherWireTransfer.text = kony.i18n.getLocalizedString('i18n.WireTranfers.MakeAnotherWireTransfer');
      this.view.btnViewSentTransactions.text = kony.i18n.getLocalizedString('i18n.payaperson.viewSentTrasactions');
      this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.wiretransfers.makeTransferAck');
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.WireTransfers.WireTransfer")
      }, {
        text: kony.i18n.getLocalizedString('i18n.wireTransfers.makeOneTimeTransferAck')
      }]); 
      this.view.lblSuccessAcknowledgement.text = kony.i18n.getLocalizedString('i18n.transfers.acknowledgmentMsg');
      this.view.confirmDetails.lblConfirmHeader.text = kony.i18n.getLocalizedString('i18n.transfers.Acknowledgement');          
      this.view.lblRefrenceNumber.setVisibility(true);
      this.view.lblRefrenceNumberValue.setVisibility(true);
      this.view.flxbuttons.setVisibility(true);
      this.view.confirmDetails.flxStep2Buttons.setVisibility(false);
      this.view.confirmDetails.flxRecipientDetails.frame.height = this.view.confirmDetails.flxBankDetails.frame.height;
      this.view.forceLayout();  
    },















    // UI CODE
    detailsJson: [{
      "fromAccount": "Personal Savings Account",
      "amount": "$1000.00",
      "transactionFee": "$10",
      "to": "Jack’s savings account",
      "currency": "US.Dollar",
      "note": "International Transfer",
      "recipientName": "Jack Jones",
      "recipientType": "Individual",
      "accountType": "International",
      "recipientAddress": "601, castle ave, castle st<br>Canada, Nova Scotia, Oxford<br>BOL 1E0",
      "recipientAccountNumber": "345151524361",
      "accountNickName": "Lind’s Account",
      "recipientBankAddress": "ZYX Bank<br>142 LakeView, M.G Road, Hyderbad<br>500072",
      "routingOrIBAN": "74287787416878",
      "swiftCode": "FSSU2878901",
      "reasonForTransfer": "Sandra’s new account",
      "acknowledgementMessage": "‘Sandra’s’ Personal checkings has been added successfully",
      "referenceNo": "45423792753",
      "isDomestic": false,
      "isInternationalUS": false,
      "isInternationalEuro": false,
      "isOneTimeTransfer": true,
      "isAcknowledgement": false,
      "isMakeTransfer": false,
      "isTransfer": false,
      "isDomesticVar": false,
      "isWireTransfer": false,
      "isInternationalVar": false
    }],
    formPreshow: function() {
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.disableButton(this.view.ActivateWireTransferWindow.btnProceed);
      this.setFlowActions();
      this.setInitialActions();
      this.hideAll();
      this.view.flxRightBar.setVisibility(false);
      this.disableButton(this.view.oneTimeTransfer.btnProceed);
    },
    toggleCheckBox: function(widget) {
      CommonUtilities.toggleCheckBox();
    },
    setFlowActions: function() {
      var scopeObj = this;
      this.view.ActivateWireTransferWindow.flxDefaultAccountForSendingInfo.onClick = function() {
        if(scopeObj.view.AllFormsActivateWireTransfer.isVisible === false)
          scopeObj.view.AllFormsActivateWireTransfer.isVisible = true;
        else
          scopeObj.view.AllFormsActivateWireTransfer.isVisible = false;
        scopeObj.view.forceLayout();
      };
      this.view.AllFormsActivateWireTransfer.flxCross.onClick = function() {
        scopeObj.view.AllFormsActivateWireTransfer.isVisible = false;
        scopeObj.view.forceLayout();
      };
      this.view.confirmDetails.flxTransactionFeeInfo.onClick = function() {
        if(scopeObj.view.AllFormsConfirmDetails.isVisible === false){
          scopeObj.view.AllFormsConfirmDetails.isVisible = true;
          scopeObj.view.AllFormsConfirmDetails.top = "220dp";
          scopeObj.view.AllFormsConfirmDetails.left = "110dp";
          scopeObj.view.AllFormsConfirmDetails.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.WireTransfer.msgInfo2Confirm");
          scopeObj.view.forceLayout();
        }
        else
          scopeObj.view.AllFormsConfirmDetails.isVisible = false;
      };
      this.view.AllFormsConfirmDetails.flxCross.onClick = function() {
        scopeObj.view.AllFormsConfirmDetails.isVisible = false;
      };
      this.view.flxInfo.onClick = function() {
        if(scopeObj.view.AllFormsConfirmDetails.isVisible === false) {
          scopeObj.view.AllFormsConfirmDetails.isVisible = true;
          scopeObj.view.AllFormsConfirmDetails.top = "560dp";
          scopeObj.view.AllFormsConfirmDetails.left = "1000dp";
          scopeObj.view.AllFormsConfirmDetails.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.WireTransfer.msgInfo2OneTime");
          scopeObj.view.forceLayout();
        }
        else
          scopeObj.view.AllFormsConfirmDetails.isVisible = false;
      };
      this.view.customheader.headermenu.btnLogout.onClick = this.showLogout;
      this.view.CustomPopup.btnYes.onClick = this.performLogout;
      this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.WireTransferActivityWindow.btnbacktopayeelist.onClick = this.ManageRecipientsAction;
      //Activate Wire Transfer
      this.view.ActivateWireTransferWindow.flxCheckbox.onClick = function() {
        scopeObj.toggleCheckBox(scopeObj.view.ActivateWireTransferWindow.imgChecbox);
        if (CommonUtilities.isChecked(scopeObj.view.ActivateWireTransferWindow.imgChecbox.src)) scopeObj.enableButton(scopeObj.view.ActivateWireTransferWindow.btnProceed);
        else scopeObj.disableButton(scopeObj.view.ActivateWireTransferWindow.btnProceed);
      };
      this.view.ActivateWireTransferWindow.btnProceed.onClick = this.showNoRecipientsScreen;
      this.view.ActivateWireTransferWindow.btnCancel.onClick = this.NavigateToTransfers;
      //No-Recipients
      this.view.NoRecipientsWindow.btnSendMoney.onClick = this.showAddRecipientsScreen;
      this.view.NoRecipientsWindow.btnRequestMoney.onClick = this.showOneTimeTransfer;
      this.view.oneTimeTransfer.lbxCountry.onTouchEnd = function() {
        scopeObj.enableButton(scopeObj.view.oneTimeTransfer.btnProceed);
      };
      //Add-Recipients
      this.view.AddRecipientAccount.lbxCountry.onTouchEnd = function() {
        scopeObj.enableButton(scopeObj.view.AddRecipientAccount.btnProceed);
      };
      this.view.AddRecipientAccount.btnCancel.onClick = this.NavigateToTransfers;
      this.view.AddRecipientAccount.btnProceed.onClick = function() {
        if (scopeObj.view.AddRecipientAccount.lbxCountry.selectedKey === "Canada" && scopeObj.detailsJson.isInternationalVar === "true") {
          scopeObj.view.AddRecipientAccount.flxReason.setVisibility(false);
          scopeObj.view.AddRecipientAccount.lblIBANOrIRC.text = "International Routing Code";
          scopeObj.view.AddRecipientAccount.tbxIBANOrIRC.text = "FSSU2878901";
          scopeObj.setAddRecipientsStep(2);
        } else if (scopeObj.view.AddRecipientAccount.lbxCountry.selectedKey === "France" && scopeObj.detailsJson.isInternationalVar === "true") {
          scopeObj.view.AddRecipientAccount.flxReason.setVisibility(true);
          scopeObj.view.AddRecipientAccount.lblIBANOrIRC.text = "International Bank Account Number (IBAN)";
          scopeObj.view.AddRecipientAccount.tbxIBANOrIRC.text = "FR14 2004 1010 0505 0001 3M02 606";
          scopeObj.setAddRecipientsStep(2);
        } else if (scopeObj.detailsJson.isDomesticVar === "true") {
          scopeObj.setAddRecipientsStep(2);
          scopeObj.view.AddRecipientAccount.flxReason.setVisibility(false);
          scopeObj.view.AddRecipientAccount.lblIBANOrIRC.setVisibility(false);
          scopeObj.view.AddRecipientAccount.tbxIBANOrIRC.setVisibility(false);
        }
        scopeObj.view.forceLayout();
      };
      this.view.AddRecipientAccount.btnStep2Cancel.onClick = this.NavigateToTransfers;
      this.view.AddRecipientAccount.btnBack.onClick = function() {
        scopeObj.setAddRecipientsStep(1);
        if (scopeObj.detailsJson.isInternationalVar === "true" && scopeObj.detailsJson.isDomesticVar === "false") {
          scopeObj.view.AddRecipientAccount.flxCountry.setVisibility(true);
        } else {
          scopeObj.view.AddRecipientAccount.flxCountry.setVisibility(false);
        }
        scopeObj.view.forceLayout();
      };
      this.view.AddRecipientAccount.btnAddRecipent.onClick = function() {
        scopeObj.DetailsJsonStatus();
        if (scopeObj.detailsJson.isDomesticVar === "true" && scopeObj.detailsJson.isInternationalVar === "false") scopeObj.detailsJson.isDomestic = "true";
        else if (scopeObj.view.AddRecipientAccount.lblIBANOrIRC.text === "International Bank Account Number (IBAN)") scopeObj.detailsJson.isInternationalEuro = true;
        else if (scopeObj.view.AddRecipientAccount.lblIBANOrIRC.text === "International Routing Code") scopeObj.detailsJson.isInternationalUS = true;
        scopeObj.view.flxAddRecipientsWindow.setVisibility(false);
        scopeObj.view.flxRightBar.setVisibility(false);
        // scopeObj.view.isOneTimeTransfer = true;
        // scopeObj.view.isAcknowledgement = false;
        scopeObj.showConfirmDetails();
        scopeObj.view.forceLayout();
      };
      //One-Time Transfer
      //  this.view.flxMakeOneTimeTransfer.onClick = this.showOneTimeTransfer;
      this.view.oneTimeTransfer.btnProceed.onClick = function() {
        scopeObj.setOneTimeTransferStep(2);
        scopeObj.ShowAllStep2();
        scopeObj.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(false);
        scopeObj.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(false);
        scopeObj.view.oneTimeTransfer.flxReason.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.oneTimeTransfer.btnCancel.onClick = function() {
        // empty the listbox
      };
      /// Step 2
      this.view.oneTimeTransfer.btnStep2Cancel.onClick = this.NavigateToTransfers;
      this.view.oneTimeTransfer.btnBack.onClick = function() {
        scopeObj.setOneTimeTransferStep(1);
      };
      this.view.oneTimeTransfer.btnStep2Proceed.onClick = function() {
        scopeObj.setOneTimeTransferStep(3);
      };
      /// Step 3
      this.view.oneTimeTransfer.btnStep3Cancel.onClick = this.NavigateToTransfers;
      this.view.oneTimeTransfer.btnStep3Back.onClick = function() {
        scopeObj.setOneTimeTransferStep(2);
        scopeObj.ShowAllStep2();
        scopeObj.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(false);
        scopeObj.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(false);
        scopeObj.view.oneTimeTransfer.flxReason.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      this.view.oneTimeTransfer.btnStep3MakeTransfer.onClick = function() {
        scopeObj.hideAll();
        scopeObj.view.flxConfirmDetails.setVisibility(true);
        scopeObj.view.confirmDetails.lblConfirmHeader.text = "TRANSACTION DETAILS";
        scopeObj.view.breadcrumb.btnBreadcrumb2.text = "MAKE ONE TIME TRANSFER - CONFIRMATION";
        scopeObj.view.lblAddAccountHeading.setVisibility(true);
        scopeObj.view.flxSuccessMessage.setVisibility(false);
        scopeObj.view.flxbuttons.setVisibility(false);
        scopeObj.view.confirmDetails.flxBankDetails.height = "preferred";
        scopeObj.view.lblAddAccountHeading.text = "Make Transfer - Confirm";
        scopeObj.view.confirmDetails.btnModify.setVisibility(true);
        scopeObj.view.confirmDetails.btnCancel.right = "510dp";
        scopeObj.view.confirmDetails.flxStep2Buttons.setVisibility(true);
        scopeObj.view.flxTermsAndConditions.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      // this.view.WireTransferContainer.btnStepContinue.onClick = function() {
      //     scopeObj.hideAll();
      //     scopeObj.detailsJson.isTransfer = true;
      //     scopeObj.showConfirmDetails();
      //     scopeObj.view.confirmDetails.lblConfirmHeader.text = "TRANSACTION DETAILS";
      //     scopeObj.view.breadcrumb.btnBreadcrumb2.text = "MAKE TRANSFER - CONFIRMATION";
      //     scopeObj.view.lblAddAccountHeading.setVisibility(true);
      //     scopeObj.view.flxSuccessMessage.setVisibility(false);
      //     scopeObj.view.flxbuttons.setVisibility(false);
      //     scopeObj.view.confirmDetails.flxBankDetails.height = "preferred";
      //     scopeObj.view.lblAddAccountHeading.text = "Make Transfer - Confirm";
      //     scopeObj.view.confirmDetails.btnModify.setVisibility(true);
      //     scopeObj.view.confirmDetails.flxStep2Buttons.setVisibility(true);
      //     scopeObj.view.confirmDetails.btnCancel.right = "510dp";
      //     scopeObj.view.flxTermsAndConditions.setVisibility(false);
      //     scopeObj.view.forceLayout();
      // };
      // this.view.WireTransferContainer.btnStepCancel.onClick = function() {
      //     scopeObj.showWireTransfersScreen();
      // };
      //Confirm Button 
      this.view.confirmDetails.btnConfirm.onClick = function() {
        if (scopeObj.detailsJson["isOneTimeTransfer"]) {
          scopeObj.view.confirmDetails.lblConfirmHeader.text = "TRANSACTION DETAILS";
          scopeObj.view.lblAddAccountHeading.setVisibility(true);
          scopeObj.view.lblAddAccountHeading.text = "Make Transfer - Acknowledgement";
          scopeObj.view.btnMakeAnotherWireTransfer.text = "MAKE ANOTHER WIRE TRANSFER";
          scopeObj.view.btnViewSentTransactions.text = "VIEW SENT TRANSACTION";
          scopeObj.view.btnSaveRecipient.setVisibility(true);
          scopeObj.view.breadcrumb.btnBreadcrumb2.text = "MAKE ONE TIME TRANSFER - ACKNOWLEDGEMENT";
        } else if (scopeObj.detailsJson["isWireTransfer"]) {
          scopeObj.view.btnSaveRecipient.setVisibility(false);
          scopeObj.view.btnMakeAnotherWireTransfer.text = "MAKE WIRE TRANSFER";
          scopeObj.view.btnViewSentTransactions.text = "ADD ANOTHER RECIPIENT";
          scopeObj.view.lblAddAccountHeading.text = "Add Recipient - Acknowledgement";
          scopeObj.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - ACKNOWLEDGEMENT";
        }else if(scopeObj.view.confirmDetails.btnModify.isVisible === "false") {

        }
        else {
          scopeObj.view.confirmDetails.lblConfirmHeader.text = "CONFIRM DETAILS";
          scopeObj.view.lblAddAccountHeading.setVisibility(true);
          scopeObj.view.lblAddAccountHeading.text = "Add Recipient - Acknowledgement";
          scopeObj.view.btnSaveRecipient.setVisibility(false);
          scopeObj.view.btnMakeAnotherWireTransfer.text = "MAKE WIRE TRANSFER";
          scopeObj.view.btnViewSentTransactions.text = "ADD ANOTHER RECIPIENT";
          if (scopeObj.detailsJson.isDomestic) scopeObj.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT DOMESTIC - ACKNOWLEDGEMENT";
          else if (scopeObj.detailsJson.isInternationalUS) scopeObj.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT INTERNATIONAL - ACKNOWLEDGEMENT";
          else if (scopeObj.detailsJson.isInternationalEuro) scopeObj.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT INTERNATIONAL - ACKNOWLEDGEMENT";
        }
        scopeObj.view.flxbuttons.setVisibility(true);
        scopeObj.view.confirmDetails.btnModify.setVisibility(true);
        scopeObj.view.confirmDetails.btnCancel.right = "510dp";
        scopeObj.view.flxSuccessMessage.setVisibility(true);
        scopeObj.view.confirmDetails.flxStep2Buttons.setVisibility(false);
        scopeObj.view.flxPrint.setVisibility(true);
        scopeObj.view.flxDownload.setVisibility(true);
        scopeObj.view.confirmDetails.flxRecipientDetails.frame.height = scopeObj.view.confirmDetails.flxBankDetails.frame.height;
        scopeObj.view.forceLayout();
        scopeObj.view.flxTermsAndConditions.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      //Modify Button 
      this.view.confirmDetails.btnModify.onClick = function() {
        if (scopeObj.detailsJson["isOneTimeTransfer"]) {
          scopeObj.showOneTimeTransfer();
          scopeObj.setOneTimeTransferStep(3);
        } else {
          scopeObj.showAddRecipientsScreen();
          scopeObj.setAddRecipientsStep(2);
        }
        scopeObj.view.breadcrumb.btnBreadcrumb2.text = "MAKE ONE TIME TRANSFER";
        scopeObj.view.flxTermsAndConditions.setVisibility(true);
        scopeObj.view.forceLayout();
      };
      //Cancel Button
      this.view.confirmDetails.btnCancel.onClick = function() {
        scopeObj.view.flxPopup.setVisibility(true);
        scopeObj.view.flxPopup.setFocus(true);
        scopeObj.view.CustomPopup.lblHeading.text = "QUIT";
        scopeObj.view.CustomPopup.lblPopupMessage.text = "Are you sure you want to undo the process of ‘Adding an Recipient’?";
        scopeObj.view.forceLayout();
      };
      //Quit Pop-Up Message Box
      this.view.CustomPopup.flxCross.onClick = function() {
        scopeObj.view.flxPopup.setVisibility(false);
      };
      this.view.CustomPopup.btnNo.onClick = function() {
        scopeObj.view.flxPopup.setVisibility(false);
      };
      this.view.CustomPopup.btnYes.onClick = this.NavigateToTransfers;
      //View Sent Transaction
      this.view.btnViewSentTransactions.onClick = function() {
        if (scopeObj.detailsJson.isOneTimeTransfer) {
          scopeObj.hideAll();
          scopeObj.view.flxWireTransfersWindow.setVisibility(true);
          scopeObj.showRightBar("WireTransfersWindow");
        }
        scopeObj.view.forceLayout();
      };
      // Make Another Wire Transfer
      this.view.btnMakeAnotherWireTransfer.onClick = function() {
        if (scopeObj.detailsJson.isOneTimeTransfer) {
          scopeObj.showOneTimeTransfer();
          scopeObj.setOneTimeTransferStep(1);
          scopeObj.view.forceLayout();
        } else if(scopeObj.view.btnMakeAnotherWireTransfer.text === "MAKE WIRE TRANSFER")
        {
          scopeObj.showWireTransfersScreen();
          scopeObj.view.forceLayout();
          scopeObj.view.flxWireTransfersWindow.height = "preferred";
          scopeObj.view.breadcrumb.btnBreadcrumb2.text = "MAKE TRANSFER";
          scopeObj.view.flxAddKonyAccount.setVisibility(true);
          scopeObj.view.flxAddNonKonyAccount.setVisibility(true);
          scopeObj.view.forceLayout();
        }
        else if(scopeObj.view.btnMakeAnotherWireTransfer.text === "MAKE ANOTHER WIRE TRANSFER"){
          scopeObj.showOneTimeTransfer();
          scopeObj.setOneTimeTransferStep(1);
          scopeObj.view.forceLayout();
        }
        scopeObj.view.lblAddAccountHeading.setVisibility(false);
        scopeObj.view.flxPrint.setVisibility(false);
        scopeObj.view.flxDownload.setVisibility(false);
        scopeObj.view.forceLayout();
      };
      //ADD ANOTHER RECIPIENT 
      this.view.btnViewSentTransactions.onClick = function() {
        if(scopeObj.view.btnViewSentTransactions.text === "VIEW SENT TRANSACTION") {
          scopeObj.showWireTransfersScreen();
          scopeObj.RecentAction();
        }
        else {
          scopeObj.showAddRecipientsScreen();
          scopeObj.setAddRecipientsStep(1);
        }
        scopeObj.view.forceLayout();
      };
      //btnSaveRecipient
      this.view.btnSaveRecipient.onClick = function() {
        scopeObj.hideAll();
        scopeObj.DetailsJsonStatus();
        scopeObj.detailsJson.isWireTransfer = true;
        scopeObj.showConfirmDetails();
        scopeObj.view.confirmDetails.lblConfirmHeader.text = "CONFIRM DETAILS";
        scopeObj.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - CONFIRMATION";
        scopeObj.view.lblAddAccountHeading.setVisibility(true);
        scopeObj.view.flxSuccessMessage.setVisibility(false);
        scopeObj.view.flxbuttons.setVisibility(false);
        scopeObj.view.confirmDetails.flxBankDetails.height = "preferred";
        scopeObj.view.lblAddAccountHeading.text = "Add Recipient - Confirm";
        scopeObj.view.confirmDetails.btnModify.setVisibility(false);
        scopeObj.view.confirmDetails.btnCancel.right = "270dp";
        scopeObj.view.confirmDetails.flxStep2Buttons.setVisibility(true);
        scopeObj.view.forceLayout();
      };


      this.view.AddRecipientAccount.flxTypeRadio1.onClick = function() {
        scopeObj.RadioBtnAction(scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType1,scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType2);
      };
      this.view.AddRecipientAccount.flxTypeRadio2.onClick = function() {
        scopeObj.RadioBtnAction(scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType2,scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType1);
      };
      //search image 
      this.view.WireTransferContainer.flxSearchImage.onClick = function() {
        if (scopeObj.view.WireTransferContainer.flxSearch.isVisible === true) {
          scopeObj.view.WireTransferContainer.flxSearchImage.skin = "CopyslFbox";
          scopeObj.view.WireTransferContainer.flxSearch.setVisibility(false);
        } else {
          scopeObj.view.WireTransferContainer.flxSearchImage.skin = "sknFlxfbfbfbBorder1px";
          scopeObj.view.WireTransferContainer.flxSearch.setVisibility(true);
        }
        scopeObj.view.forceLayout();
      };
      this.view.WireTransferContainer.Search.txtSearch.onBeginEditing = function() {
        scopeObj.view.Search.flxClearBtn.setVisibility(true);
        if (scopeObj.view.Search.txtSearch.text === null || scopeObj.view.Search.txtSearch.text === " ") {
          scopeObj.view.Search.flxClearBtn.setVisibility(false);
        }
      };
      //Domestic Account 
      this.view.AddRecipientAccount.btnDomesticAccount.onClick = function() {
        scopeObj.ShowDomesticAccount();
        scopeObj.view.forceLayout();
      };
      //International Account
      this.view.AddRecipientAccount.btnInternationalAccount.onClick = function() {
        scopeObj.showInternationalAccount();
        scopeObj.view.forceLayout();
      };
      //Make Transfer - Wire Transfer
      this.view.WireTransferContainer.btnMakeTransfer.onClick = this.MakeTransferAction;
      //Recent - Wire Transfer
      this.view.WireTransferContainer.btnRecent.onClick = this.RecentAction;
      //Manage Recipient 
      this.view.WireTransferContainer.btnManageRecipient.onClick = this.ManageRecipientsAction;
    },
    RadioBtnAction : function(RadioBtn1,RadioBtn2) {
      if(RadioBtn1.text === "R") {
        RadioBtn1.text = " ";
        RadioBtn1.skin = "sknC0C0C020pxNotFontIcons";
        RadioBtn2.text = "R";
        RadioBtn2.skin = "sknLblFontTypeIcon3343e820px";
      }
      else {
        RadioBtn2.text = " ";
        RadioBtn2.skin = "sknC0C0C020pxNotFontIcons";
        RadioBtn1.text = "R";
        RadioBtn1.skin = "sknLblFontTypeIcon3343e820px";
      }
    },  
    ShowAllSeperators: function() {
      this.view.AddRecipientAccount.flxTabsSeperator1.setVisibility(true);
      this.view.AddRecipientAccount.flxTabsSeperator2.setVisibility(true);
      this.view.AddRecipientAccount.flxTabsSeperator3.setVisibility(true);
      this.view.forceLayout();
    },
    showAllSeperatorsWireTransfer: function() {
      this.view.WireTransferContainer.flxTabsSeperator1.setVisibility(true);
      this.view.WireTransferContainer.flxTabsSeperator2.setVisibility(true);
      this.view.WireTransferContainer.flxTabsSeperator3.setVisibility(true);
      this.view.WireTransferContainer.flxTabsSeperator4.setVisibility(true);
    },
    ShowDomesticAccount: function() {
      this.showAddRecipientsScreen();
      this.view.AddRecipientAccount.btnDomesticAccount.skin = "sknbtnffffffNoBorder";
      this.view.AddRecipientAccount.btnInternationalAccount.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.ShowAllSeperators();
      this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - DOMESTIC";
      this.view.AddRecipientAccount.flxTabsSeperator3.setVisibility(false);
      this.setAddRecipientsStep(1);
      this.view.AddRecipientAccount.flxCountry.setVisibility(false);
      this.view.AddRecipientAccount.lblIBANOrIRC.setVisibility(false);
      this.view.AddRecipientAccount.tbxIBANOrIRC.setVisibility(false);
      this.view.AddRecipientAccount.flxReason.setVisibility(false);
      this.disableButton(this.view.AddRecipientAccount.btnProceed);
      this.detailsJson.isDomesticVar = "true";
      this.detailsJson.isInternationalVar = "false";
      this.view.forceLayout();
    },
    showInternationalAccount: function() {
      this.showAddRecipientsScreen();
      this.view.AddRecipientAccount.btnDomesticAccount.skin = "sknbtnfbfbfbBottomBordere3e3e3";
      this.view.AddRecipientAccount.btnInternationalAccount.skin = "sknbtnffffffNoBorder";
      this.ShowAllSeperators();
      this.view.AddRecipientAccount.flxTabsSeperator1.setVisibility(false);
      this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - INTERNATIONAL";
      this.setAddRecipientsStep(1);
      this.view.AddRecipientAccount.flxCountry.setVisibility(true);
      this.view.AddRecipientAccount.lblIBANOrIRC.setVisibility(true);
      this.view.AddRecipientAccount.tbxIBANOrIRC.setVisibility(true);
      this.view.AddRecipientAccount.flxReason.setVisibility(true);
      this.disableButton(this.view.AddRecipientAccount.btnProceed);
      this.detailsJson.isDomesticVar = "false";
      this.detailsJson.isInternationalVar = "true";
      this.view.forceLayout();
    },
    showConfirmDetails: function() {
      //recipient details
      this.view.confirmDetails.lblDetailsKey1.text = "Recipient Name :";
      this.view.confirmDetails.rtxDetailsValue1.text = this.detailsJson.recipientName;
      this.view.confirmDetails.lblDetailsKey2.text = "Recipient Type :";
      this.view.confirmDetails.rtxDetailsValue2.text = this.detailsJson.recipientType;
      this.view.confirmDetails.lblDetailsKey3.text = "Account Type :";
      this.view.confirmDetails.rtxDetailsValue3.text = this.detailsJson.accountType;
      this.view.confirmDetails.lblDetailsKey4.text = "Recipient Address :";
      this.view.confirmDetails.rtxDetailsValue4.text = this.detailsJson.recipientAddress;
      //bank details
      this.view.confirmDetails.lblBankDetailsKey3.text = "Recipient Account Number :";
      this.view.confirmDetails.rtxBankDetailsValue3.text = this.detailsJson.recipientAccountNumber;
      this.view.confirmDetails.lblBankDetailsKey5.text = "Account Nick Name :";
      this.view.confirmDetails.rtxBankDetailsValue5.text = this.detailsJson.accountNickName;
      this.view.confirmDetails.lblBankDetailsKey6.text = "Recipient Bank & Address :";
      this.view.confirmDetails.rtxBankDetailsValue6.text = this.detailsJson.recipientBankAddress;
      this.view.lblAddAccountHeading.setVisibility(true);
      this.view.lblAddAccountHeading.text = "Add Recipient - Confirm";
      this.view.flxPrint.setVisibility(false);
      this.view.flxDownload.setVisibility(false);
      //location specific bank details
      if (this.detailsJson.isDomestic) {
        this.view.confirmDetails.flxTransactionDetails.setVisibility(false);
        this.view.confirmDetails.flxRow2.setVisibility(false);
        this.view.confirmDetails.flxRow4.setVisibility(false);
        this.view.confirmDetails.lblBankDetailsKey1.text = "Routing Number :";
        this.view.confirmDetails.rtxBankDetailsValue1.text = this.detailsJson.routingOrIBAN;
        this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - DOMESTIC - CONFIRMATION";
        this.view.confirmDetails.flxSeparator.setVisibility(false);
        this.view.forceLayout();
      } else if (this.detailsJson.isWireTransfer) {
        this.view.confirmDetails.flxTransactionDetails.setVisibility(false);
        this.view.confirmDetails.flxSeparator.setVisibility(false);
        this.view.confirmDetails.lblConfirmHeader.text = "CONFIRM DETAILS";
      } else if (this.detailsJson.isTransfer) {
        this.DetailsJsonStatus();
        this.detailsJson.isOneTimeTransfer = true;
        this.showConfirmDetails();
      } else if (this.detailsJson.isInternationalUS) {
        this.view.confirmDetails.flxTransactionDetails.setVisibility(false);
        this.view.confirmDetails.flxRow2.setVisibility(true);
        this.view.confirmDetails.flxRow4.setVisibility(false);
        this.view.confirmDetails.flxSeparator.setVisibility(false);
        this.view.confirmDetails.lblBankDetailsKey2.text = "International Routing No :";
        this.view.confirmDetails.rtxBankDetailsValue2.text = this.detailsJson.routingOrIBAN;
        this.view.confirmDetails.lblBankDetailsKey1.text = "Swift code :";
        this.view.confirmDetails.rtxBankDetailsValue1.text = this.detailsJson.swiftCode;
        this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - INTERNATIONAL - CONFIRMATION";
      } else if (this.detailsJson.isInternationalEuro) {
        this.view.confirmDetails.flxTransactionDetails.setVisibility(false);
        this.view.confirmDetails.flxRow2.setVisibility(true);
        this.view.confirmDetails.flxRow4.setVisibility(true);
        this.view.confirmDetails.lblBankDetailsKey2.text = "International Bank Account No :";
        this.view.confirmDetails.rtxBankDetailsValue2.text = this.detailsJson.routingOrIBAN;
        this.view.confirmDetails.lblBankDetailsKey1.text = "Swift code :";
        this.view.confirmDetails.rtxBankDetailsValue1.text = this.detailsJson.swiftCode;
        this.view.confirmDetails.lblBankDetailsKey4.text = "Reason for Transfer :";
        this.view.confirmDetails.rtxBankDetailsValue4.text = this.detailsJson.reasonForTransfer;
        this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - INTERNATIONAL - CONFIRMATION";
        this.view.confirmDetails.flxSeparator.setVisibility(false);
      }
      this.view.confirmDetails.flxStep2Buttons.setVisibility(true);
      this.view.flxbuttons.setVisibility(false);
      this.view.confirmDetails.flxRecipientDetails.frame.height = this.view.confirmDetails.flxBankDetails.frame.height;
      //if it is an acknowledgement screen
      if (this.detailsJson.isAcknowledgement) {
        this.view.confirmDetails.flxStep2Buttons.setVisibility(false);
        this.view.flxbuttons.setVisibility(true);
        this.view.btnSaveRecipient.setVisibility(false);
        this.view.lblAddAccountHeading.setVisibility(true);
        this.view.lblAddAccountHeading.text = "Add Recipient - Acknowledgement";
        this.view.flxPrint.setVisibility(true);
        this.view.flxDownload.setVisibility(true);
        this.view.btnMakeAnotherWireTransfer.text = "MAKE WIRE TRANSFER";
        this.view.btnViewSentTransactions.text = "ADD ANOTHER RECIPIENT";
        this.view.flxSuccessMessage.setVisibility(true);
        if (this.detailsJson.isOneTimeTransfer) {
          this.view.btnSaveRecipient.setVisibility(true);
          this.view.breadcrumb.btnBreadcrumb2.text = "ONE TIME TRANSFER - ACKNOWLEDGEMENT";
        } else if (this.detailsJson.isTransfer) {
          this.view.btnViewSentTransactions.setVisibility(false);
          this.view.btnMakeAnotherWireTransfer.text = "MAKE ANOTHER WIRE TRANSFER";
          this.view.btnViewSentTransactions.text = "VIEW SENT TRANSACTION";
        } else {
          this.view.btnSaveRecipient.setVisibility(false);
          // this.view.btnMakeAnotherWireTransfer.text = "MAKE WIRE TRANSFER";
          // this.view.btnViewSentTransactions.text = "ADD ANOTHER RECIPIENT";
          if (this.detailsJson.isDomestic) this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT DOMESTIC - ACKNOWLEDGEMENT";
          else if (this.detailsJson.isInternationalUS) this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT INTERNATIONAL - ACKNOWLEDGEMENT";
          else if (this.detailsJson.isInternationalEuro) this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT INTERNATIONAL - ACKNOWLEDGEMENT";
          else if (this.detailsJson.isMakeTransfer) {
            this.view.breadcrumb.btnBreadcrumb2.text = "MAKE TRANSFER - ACKNOWLEDGEMENT";
            this.view.btnMakeAnotherWireTransfer.text = "MAKE WIRE TRANSFER";
            this.view.btnViewSentTransactions.text = "ADD ANOTHER RECIPIENT";
            this.view.btnViewSentTransactions.onClick = this.RecentAction;
            this.view.btnMakeAnotherWireTransfer.onClick = this.MakeTransferAction;
          }
        }
        if (this.detailsJson.isOneTimeTransfer || this.detailsJson.isMakeTransfer) {
          this.view.lblRefrenceNumber.setVisibility(true);
          this.view.lblRefrenceNumberValue.setVisibility(true);
          this.view.lblRefrenceNumberValue.text = this.detailsJson.referenceNo;
        } else {
          this.view.lblRefrenceNumber.setVisibility(false);
          this.view.lblRefrenceNumberValue.setVisibility(false);
        }
      } else {
        this.view.flxSuccessMessage.setVisibility(false);
      }
      this.hideAll();
      this.view.flxConfirmDetails.setVisibility(true);
      this.view.confirmDetails.flxRecipientDetails.frame.height = this.view.confirmDetails.flxBankDetails.frame.height;
      this.view.forceLayout();
    },
    showActivateWireTransferScreen: function() {
      this.hideAll();
      this.view.flxActivateWireTransfer.setVisibility(true);
      this.view.forceLayout();
    },
    showWireTransfersScreen: function() {
      this.hideAll();
      this.view.flxWireTransfersWindow.setVisibility(true);
      this.showRightBar("WireTransfersWindow");
      this.view.forceLayout();
    },
    showWireTransferActivityScreen: function() {
      this.hideAll();
      this.view.flxWireTransferActivityWindow.setVisibility(true);
      this.showRightBar("WireTransfersWindow");
      this.view.forceLayout();
    },

    showAddRecipientsScreen: function() {
      this.hideAll();
      this.view.flxAddRecipientsWindow.setVisibility(true);
      this.view.breadcrumb.btnBreadcrumb2.text = "ADD RECIPIENT - DOMESTIC";
      this.disableButton(this.view.AddRecipientAccount.btnProceed);
      this.view.flxAddKonyAccount.setVisibility(false);
      this.view.flxAddNonKonyAccount.setVisibility(false);
      this.view.flxAddInternationalAccount.setVisibility(true);
      this.showRightBar("addRecipient");
      this.detailsJson.isDomesticVar = "true";
      this.detailsJson.isInternationalVar = "false";
    },

    showOneTimeTransferScreen: function(step) {
      this.hideAll();
      this.showRightBar("oneTimeTransfer");
    },

    enableStep1Proceed: function(recipientName) {
      if (recipientName !== null && recipientName !== "") this.btnAddRecipientsStep1(true);
      else this.btnAddRecipientsStep1(false);
    },
    btnAddRecipientsStep1: function(status) {
      if (status === true) {
        this.enableButton(this.view.AddRecipientAccount.btnProceed);
      } else {
        this.disableButton(this.view.AddRecipientAccount.btnProceed);
      }
    },
    enableStep1ProceedOneTimeTransfer: function(recipientName) {
      if (recipientName !== null && recipientName !== "") this.btnAddRecipientsStep1OneTimeTransfer(true);
      else this.btnAddRecipientsStep1OneTimeTransfer(false);
    },
    btnAddRecipientsStep1OneTimeTransfer: function(status) {
      if (status === true) {
        this.enableButton(this.view.oneTimeTransfer.btnProceed);
      } else {
        this.disableButton(this.view.oneTimeTransfer.btnProceed);
      }
    },
    DetailsJsonStatus: function() {
      this.detailsJson.isDomestic = false;
      this.detailsJson.isAcknowledgement = false;
      this.detailsJson.isInternationalEuro = false;
      this.detailsJson.isInternationalUS = false;
      this.detailsJson.isMakeTransfer = false;
      this.detailsJson.isOneTimeTransfer = false;
      this.detailsJson.isTransfer = false;
      this.detailsJson.isWireTransfer = false;
    },
    /**
     *  Disable button.
     */
    disableButton: function(button) {
      button.setEnabled(false);
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
    },
    /**
     * Enable button.
     */
    enableButton: function(button) {
      button.setEnabled(true);
      button.skin = "sknbtnLatoffffff15px";
      button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
    }
  };
});