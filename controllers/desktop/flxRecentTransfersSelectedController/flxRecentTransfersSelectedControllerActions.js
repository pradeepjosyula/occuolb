define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a74c7e232d64498dadced7789bac7c12: function AS_FlexContainer_a74c7e232d64498dadced7789bac7c12(eventobject, context) {
        var self = this;
        this.showUnselectedRow();
    },
    /** onClick defined for btnAction **/
    AS_Button_h3838e34bed24554abb63debdf3f646f: function AS_Button_h3838e34bed24554abb63debdf3f646f(eventobject, context) {
        var self = this;
        this.executeOnParent("viewTransactionReport");
    },
    /** onClick defined for btnRepeat **/
    AS_Button_a221ef718b9b4c47bfa795241ae0d628: function AS_Button_a221ef718b9b4c47bfa795241ae0d628(eventobject, context) {
        var self = this;
        this.executeOnParent("repeatTransaction");
    }
});