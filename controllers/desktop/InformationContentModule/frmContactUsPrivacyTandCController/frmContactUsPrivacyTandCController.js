define(['CommonUtilities'], function (CommonUtilities) {

    return {

        //Type your controller code here 
        willUpdateUI: function (viewModel) {
            if (viewModel === undefined) { } else {
                if (viewModel.viewType) this.showViewType(viewModel.viewType);
                if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu, viewModel.sideSubMenu);
                if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
                if (viewModel.showContactUs) this.showContactUs(viewModel.showContactUs);
                if (viewModel.showPrivacyPolicy) this.showPrivacyPolicy(viewModel.showPrivacyPolicy);
                if (viewModel.showLoadingIndicatorPrivacyPolicy) this.showLoadingIndicatorPrivacyPolicy(viewModel.showLoadingIndicatorPrivacyPolicy.view);
                if (viewModel.showTnC) this.showTnC(viewModel.showTnC);

            }
            this.view.customheader.flxTopmenu.topmenu.flxaccounts.skin = "copyslfbox1";
            this.view.customheader.flxTopmenu.topmenu.flxTransfersAndPay.skin = "copyslfbox1";
            this.AdjustScreen();
        },
        /**
        * Preshow method for the form to set UI
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - None
        * @returns {void} - None
        * @throws {void} -None
        */

        preShowfrmContactUs: function () {
            this.hideAll();
            this.view.customheader.forceCloseHamburger();
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxMenu.skin = "slFbox";
            this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknHoverTopmenu7f7f7pointer";
            this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
        },
        /**
        * Method to Show Terms and conditions basing on session token
        * @member of frmContactUsPrivacyTandCController
        * @param {JSON}viewModel - terms and conditions to be viewed
        * @returns {void} - None
        * @throws {void} -None
        */

        showTnC: function (viewModel) {
            this.preShowfrmContactUs();
            this.view.flxHeader.isVisible = true;
            this.view.flxMainContainer.isVisible = true;
            this.view.flxContactUs.isVisible = false;
            if (viewModel.serviceData === "error") {
                CommonUtilities.hideProgressBar(this.view);
                this.setServerError(true);
            } else {
                this.view.flxDowntimeWarning.setVisibility(false);
                if (viewModel.param === "postLoginView") {
                    this.showTnCPostLogin(viewModel);
                } else {
                    this.showTnCPreLogin(viewModel);
                }

            }
            this.AdjustScreen();
        },
        /**
    * Method to Show Terms and conditions before login
    * @member of frmContactUsPrivacyTandCController
    * @param {JSON}viewModel - terms and conditions to be viewed
    * @returns {void} - None
    * @throws {void} -None
    */
        showTnCPreLogin: function (viewModel) {
            this.view.flxMainContainer.top = "70dp";
            this.view.customheader.FlexContainer0e2898aa93bca45.height = "70dp";
            this.view.customheader.flxBottomContainer.height = "70dp";
            this.view.customheader.flxTopmenu.setVisibility(false);
            this.view.customheader.headermenu.flxMessages.isVisible = false;
            this.view.customheader.headermenu.flxVerticalSeperator1.isVisible = false;
            this.view.customheader.headermenu.flxResetUserImg.isVisible = false;
            this.view.customheader.headermenu.flxUserId.isVisible = false;
            this.view.customheader.flxSeperatorHor2.setVisibility(false);
            this.view.customheader.headermenu.flxWarning.isVisible = true;
            this.view.flxContactUs.setVisibility(false);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.termsAndConditions.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.TnC");
            this.view.termsAndConditions.rtxBody.text = viewModel.serviceData.records.length!==0 ? viewModel.serviceData.records[0].Description:"";
            this.view.flxPrivacyPolicy.isVisible = false;
            this.view.customheader.headermenu.btnLogout.text = kony.i18n.getLocalizedString("i18n.common.login");
            this.view.customheader.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
            this.view.customheader.headermenu.btnLogout.onClick = function () {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.presentationController.showLoginScreen();
            };

            this.view.forceLayout();
            CommonUtilities.hideProgressBar(this.view);
        },
        /**
        * Method to show TnC page after login
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - viewmodel that has reponse data in serviceData and param that tell whether it is prelogin view or postlogin view
        * @returns {void} - None
        * @throws {void} -None
        */
        showTnCPostLogin: function (viewModel) {
            this.view.flxMainContainer.top = "120dp";

            this.view.customheader.FlexContainer0e2898aa93bca45.height = "120dp";
            this.view.customheader.flxBottomContainer.height = "120dp";
            this.view.customheader.flxTopmenu.setVisibility(true);
            this.view.customheader.headermenu.flxMessages.isVisible = true;
            this.view.customheader.headermenu.flxVerticalSeperator1.isVisible = true;
            this.view.customheader.headermenu.flxResetUserImg.isVisible = true;
            this.view.customheader.headermenu.flxUserId.isVisible = true;
            this.view.customheader.flxSeperatorHor2.setVisibility(true);
            this.view.customheader.headermenu.flxWarning.isVisible = false;
            this.view.customheader.headermenu.btnLogout.text = kony.i18n.getLocalizedString("i18n.common.logout");
            this.view.customheader.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.logout");
            this.view.flxContactUs.setVisibility(false);
            this.view.flxPrivacyPolicy.setVisibility(false);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.termsAndConditions.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.TnC");
            this.view.termsAndConditions.rtxBody.text = viewModel.serviceData.records[0].Description;

            CommonUtilities.hideProgressBar(this.view);

            this.view.forceLayout();
            var self = this;
            this.view.customheader.headermenu.btnLogout.onClick = function () {
                
                self.view.flxPopup.isVisible = true;
                self.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
                self.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
                var height = self.view.flxHeader.frame.height + self.view.flxMainContainer.frame.height;
                self.view.flxPopup.height = height + "dp";
                self.view.flxPopup.left = "0%";
            };
            this.view.CustomPopup.btnYes.onClick = function () {
                
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                context = {
                    "action": "Logout"
                };
                authModule.presentationController.doLogout(context);
                self.view.flxPopup.left = "-100%";
            };
            this.view.CustomPopup.btnNo.onClick = function () {
                
                self.view.flxPopup.left = "-100%";
            };
            this.view.CustomPopup.flxCross.onClick = function () {
                
                self.view.flxPopup.left = "-100%";
            };

        },


        /**
        * Method to handle Server error 
        * @param {boolena} isError , error flag to show/hide error flex.
        * @member of {frmContactUsPrivacyTandCController}
        * @param {object} data - Service error object
        * @return {}
        * @throws {}
        */
        setServerError: function (isError) {
            var scopeObj = this;
            scopeObj.view.flxDowntimeWarning.setVisibility(isError);
            if (isError) {
                scopeObj.view.rtxDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.footerLinks.serverError");
            }
            scopeObj.updateProgressBarState(false);
            scopeObj.view.forceLayout();
        },
        /**
        * updateProgressBarState : Method to show or hide Loading progress bar
        * @param {boolena} isError , error flag to show/hide error flex.
        * @member of {frmContactUsPrivacyTandCController}
        * @param {boolean} isLoading - loading flag
        * @return {}
        * @throws {} 
        */
        updateProgressBarState: function (isLoading) {
            if (isLoading) {
                this.setServerError(false);
                CommonUtilities.showProgressBar(this.view);
            }
            else {
                CommonUtilities.hideProgressBar(this.view);
            }
        },

        /**
        * To show loading indicator
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - None
        * @returns {void} - None
        * @throws {void} -None
        */

        showLoadingIndicatorPrivacyPolicy: function (view) {
            CommonUtilities.showProgressBar(this.view);
            this.hideAll();
            this.showViewType(view);
            this.presenter.showPrivacyPolicyAfterLoading();
        },

        /**
        * Method to show privacy policies
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - None
        * @returns {void} - None
        * @throws {void} -None
        */

        showPrivacyPolicy: function (viewModel) {
            this.hideAll();
            this.preShowfrmContactUs();
            this.view.flxHeader.isVisible = true;
            this.view.flxMainContainer.isVisible = true;
            var self= this;
            
            this.view.flxContactUs.isVisible = false;
            if (viewModel.serviceData === "error") {
                self.view.flxDowntimeWarning.setVisibility(true);
                CommonUtilities.hideProgressBar(this.view);
                self.view.lblDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.footerLinks.serverError");
            } else {
                self.view.flxDowntimeWarning.setVisibility(false);
                CommonUtilities.hideProgressBar(this.view);
                self.showPrivacyPolicyView(viewModel);
            }
            this.AdjustScreen();
        },

        /**
        * Method to show privacy policy page 
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - viewmodel that has reponse data in serviceData and param that tell whether it is prelogin view or postlogin view
        * @returns {void} - None
        * @throws {void} -None
        */
        showPrivacyPolicyView: function (viewModel) {         
            this.view.privacyPolicy.lblHeading.text = kony.i18n.getLocalizedString("i18n.informationContent.privacyPolicy");
            this.view.flxPrivacyPolicy.setVisibility(true);
            this.view.privacyPolicy.rtxBody.text = viewModel.serviceData.records[0].Description;
            CommonUtilities.hideProgressBar(this.view);
            this.view.forceLayout();


        },

        /**
        * Method to hide all the flex of main body
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - None
        * @returns {void} - None
        * @throws {void} -None
        */
        hideAll: function () {
            this.view.flxContactUs.setVisibility(false);
            this.view.flxPrivacyPolicy.setVisibility(false);
            this.view.flxTermsAndConditions.setVisibility(false);
        },

        /**
        * Method for checking view type be it prelogin or postlogin
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - viewtype
        * @returns {void} - None
        * @throws {void} -None
        */
        showViewType: function (viewType) {
            CommonUtilities.showProgressBar(this.view);
            if (viewType === "preLogin") {
                this.showPreLoginView();
            } else {
                this.showPostLoginView();
            }
        },
        /**
        * method to show pre login view
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - None
        * @returns {void} - None
        * @throws {void} -None
        */

        showPreLoginView: function () {
            this.hideAll();
            this.view.flxMainContainer.top = "70dp";
            this.view.customheader.FlexContainer0e2898aa93bca45.height = "70dp";
            this.view.customheader.flxBottomContainer.height = "70dp";
            this.view.customheader.flxTopmenu.setVisibility(false);
            this.view.customheader.headermenu.flxMessages.isVisible = false;
            this.view.customheader.headermenu.flxVerticalSeperator1.isVisible = false;
            this.view.customheader.headermenu.flxResetUserImg.isVisible = false;
            this.view.customheader.headermenu.flxUserId.isVisible = false;
            this.view.customheader.flxSeperatorHor2.setVisibility(false);
            this.view.customheader.headermenu.flxWarning.isVisible = true;
            this.view.customheader.headermenu.btnLogout.text = kony.i18n.getLocalizedString("i18n.common.login");
            this.view.customheader.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
            this.view.customheader.headermenu.btnLogout.onClick = function () {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.presentationController.showLoginScreen();
            };
            this.view.forceLayout();
        },
        /**
     * method to show post login view
     * @member of frmContactUsPrivacyTandCController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} -None
     */

        showPostLoginView: function () {
            this.hideAll();
            this.view.flxMainContainer.top = "120dp";
            var self = this;
            this.view.customheader.FlexContainer0e2898aa93bca45.height = "120dp";
            this.view.customheader.flxBottomContainer.height = "120dp";
            this.view.customheader.flxTopmenu.setVisibility(true);
            this.view.customheader.headermenu.flxMessages.isVisible = true;
            this.view.customheader.headermenu.flxVerticalSeperator1.isVisible = true;
            this.view.customheader.headermenu.flxResetUserImg.isVisible = true;
            this.view.customheader.headermenu.flxUserId.isVisible = true;
            this.view.customheader.flxSeperatorHor2.setVisibility(true);
            this.view.customheader.headermenu.flxWarning.isVisible = false;
            this.view.customheader.headermenu.btnLogout.text = kony.i18n.getLocalizedString("i18n.common.logout");
            this.view.customheader.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.logout");
            this.view.customheader.headermenu.btnLogout.onClick = function () {
                
                self.view.flxPopup.isVisible = true;
                self.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
                self.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
                var height = self.view.flxHeader.frame.height + self.view.flxMainContainer.frame.height;
                self.view.flxPopup.height = height + "dp";
                self.view.flxPopup.left = "0%";
            };
            this.view.CustomPopup.btnYes.onClick = function () {
               
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                context = {
                    "action": "Logout"
                };
                authModule.presentationController.doLogout(context);
                self.view.flxPopup.left = "-100%";
            };
            this.view.CustomPopup.btnNo.onClick = function () {
                
                self.view.flxPopup.left = "-100%";
            };
            this.view.CustomPopup.flxCross.onClick = function () {
                
                self.view.flxPopup.left = "-100%";
            };
            this.view.forceLayout();
        },

        /**
        * Method to Show contactUs flex
        * @member of frmContactUsPrivacyTandCController
        * @param {JSON} - ContactUs records from backend
        * @returns {void} - None
        * @throws {void} -None
        */
        showContactUs: function (viewModel) {
            this.hideAll();
            this.view.flxDowntimeWarning.setVisibility(false);
            if (viewModel.status === "error") {

                CommonUtilities.hideProgressBar(this.view);
            } else if (viewModel.param === "preLoginView") {
                
                this.showPreLoginView();
            } else {

                this.showPostLoginView();
            }
            this.view.flxContactUs.isVisible = true;
            this.setContactUsData(viewModel.contactUs);
            this.AdjustScreen();
            CommonUtilities.hideProgressBar(this.view);

        },

        /**
        * Method to Populate data with contactUs data
        * @member of frmContactUsPrivacyTandCController
        * @param {JSON} - contactUs records from backend
        * @returns {void} - None
        * @throws {void} -None
        */
        setContactUsData: function (viewModel) {
            
            this.view.contactUs.rtxContactCustomerServiceMsg.text = "Our customer service is available to address your account and servicing need for various products";
            var segData = [];
            for (var data in viewModel) {
                var i,
                    tempPhone = "",
                    tempEmail = "";
                for (i in viewModel[data].Email) {
                    tempEmail = tempEmail + viewModel[data].Email[i] + "<br>";
                }
                for (i in viewModel[data].Phone) {
                    tempPhone = tempPhone + "Phone: " + viewModel[data].Phone[i] + "<br>";
                }
                segData.push({
                    imgDot: "pageoffdot.png",
                    lblEmailId: "Email Id:",
                    lblHeading: viewModel[data].heading,
                    rtxEmailId: tempEmail,
                    rtxPhoneNumber: tempPhone
                });
            }
            this.view.contactUs.segCustomerService.setData(segData);
        },

        /**
        * Method to Initialize HamburgerMenu
        * @member of frmContactUsPrivacyTandCController
        * @param {JSON} - SideMenuModel to map hamburger menu actions
        * @returns {void} - None
        * @throws {void} -None
        */
        updateHamburgerMenu: function (sideMenuModel, viewModel) {
            this.view.customheader.initHamburger(sideMenuModel, "About Us", viewModel);
        },

        /**
        * Method to Initialize TopBar Menu
        * @member of frmContactUsPrivacyTandCController
        * @param {JSON} - topBarModel to map topBar menu actions
        * @returns {void} - None
        * @throws {void} -None
        */
        updateTopBar: function (topBarModel) {
            this.view.customheader.initTopBar(topBarModel);
        },
        /**
        * method to render form 
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - None
        * @returns {void} - None
        * @throws {void} -None
        */
        postShowRenderBody: function () {
            var scopeObj = this;
            kony.timer.schedule("renderBody", scopeObj.renderBodyFunction, 2, false);
            this.AdjustScreen();
        },
        //UI Code
        /**
      * UI method to adjust alignments
      * @member of frmContactUsPrivacyTandCController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} -None
      */
        AdjustScreen: function () {
            var mainheight;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0)
                    this.view.flxFooter.top = mainheight + diff + "dp";
                else
                    this.view.flxFooter.top = mainheight + "dp";
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
        },
        /**
        * method to render form 
        * @member of frmContactUsPrivacyTandCController
        * @param {void} - None
        * @returns {void} - None
        * @throws {void} -None
        */
        renderBodyFunction: function () {
            if (this.view.flxContactUs.isVisible) {
                this.view.contactUs.renderBody();
            } if (this.view.flxPrivacyPolicy.isVisible) {
                this.view.privacyPolicy.renderBody();
            } if (this.view.flxTermsAndConditions.isVisible) {
                this.view.termsAndConditions.renderBody();
            }
            this.view.forceLayout();
        }

    };
});