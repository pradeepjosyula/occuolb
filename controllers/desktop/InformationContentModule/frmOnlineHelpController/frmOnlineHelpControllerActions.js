define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** postShow defined for frmOnlineHelp **/
    AS_Form_c4f8c3c70e87454aa48521219d5868be: function AS_Form_c4f8c3c70e87454aa48521219d5868be(eventobject) {
        var self = this;
        this.presenter.loadHamburger('frmOnlineHelp');
    },
    /** onTouchEnd defined for frmOnlineHelp **/
    AS_Form_bcdd23043a504fefa3823205f8287fea: function AS_Form_bcdd23043a504fefa3823205f8287fea(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});