define(['CommonUtilities'], function (CommonUtilities) {
  return {
    willUpdateUI: function (viewModel) {
      if (viewModel.ProgressBar) {
        if (viewModel.ProgressBar.show) {
          CommonUtilities.showProgressBar(this.view);
        }
        else {
          CommonUtilities.hideProgressBar(this.view);
        }
      }
      if (viewModel.serverError) {
        this.showServerError(viewModel.serverError);
      }
      if (viewModel.sideMenu) {
        this.updateHamburgerMenu(viewModel.sideMenu);
      }
      if (viewModel.topBar) {
        this.updateTopBar(viewModel.topBar);
      }
      if (viewModel.loadAccounts) {
        this.accountsListCallback(viewModel.loadAccounts);
      }
      if (viewModel.loanPayoff) {       
        this.loanPayOffInit(viewModel.loanPayoff);
        CommonUtilities.hideProgressBar(this.view);
      }
      if (viewModel.loanDue) {
        this.loanPayDueAmountInit(viewModel.loanDue);
        CommonUtilities.hideProgressBar(this.view);
      }
      if (viewModel.payCompleteMonthlyDue) {
        this.successPayDueAmount(viewModel.payCompleteMonthlyDue.data, viewModel.payCompleteMonthlyDue.referenceId);
      }
      if (viewModel.payCompleteDue) {
        this.successPayment(viewModel.payCompleteDue.data, viewModel.payCompleteDue.referenceId);
      }
      if (viewModel.payOtherAmount) {
        this.successPayOtherAmount(viewModel.payOtherAmount.data, viewModel.payOtherAmount.referenceId);
      }
      if (viewModel.populateAccountData) {
        this.populateFromAccountValues(viewModel.populateAccountData);
      }
      if (viewModel.updateToAccount) {
        this.updateToAccountDetails(viewModel.updateToAccount);
      }
      if (viewModel.updateFromAccount) {
        this.updateAccountDetails(viewModel.updateFromAccount);
      }
      if (viewModel.validateData) {
        this.validateData(viewModel.validateData);
      }
      if (viewModel.navigationToAccountDetails) {
        this.backToAccountDetailsCallback(viewModel.navigationToAccountDetails);
      }
      if (viewModel.newAccountSelection) {
        viewModel.newAccountSelection.principalBalance = CommonUtilities.formatCurrencyWithCommas(viewModel.newAccountSelection.principalBalance);
        viewModel.newAccountSelection.payOffCharge = CommonUtilities.formatCurrencyWithCommas(viewModel.newAccountSelection.payOffCharge);
        this.setDataToForm(viewModel.newAccountSelection);
      }
      this.view.breadcrumb.btnBreadcrumb2.skin="sknBtnLato3343A813PxBg0";
      this.view.breadcrumb.btnBreadcrumb2.hoverSkin="sknBtnLato3343A813PxBg0";
      this.AdjustScreen();
    },
    /**
      * Show UI for Loan Pay Off
      * @member frmPayDueAmountController
      * @param {void}  None
      * @returns {void} - None
      * @throws {void} - None
      */
    loanPayOffInit: function (data) {
      var scopeObj = this;
      this.loadAccounts();
      this.view.lblConfirmationTitle.text = kony.i18n.getLocalizedString("i18n.loan.loanPayoffPayment");
      this.view.lblAcknowledgementTitle.text = kony.i18n.getLocalizedString("i18n.loan.loanPayoffPayment");
      this.loanContext = "Loan Payoff";
      this.setDataToForm(data.accounts);
      if (data.fromAccount) {
        this.fromAccount = data.fromAccount;
      }
      this.view.flxDownTimeWarning.setVisibility(false);
      this.view.flxAcknowledgement.setVisibility(false);
      this.view.flxConfirmation.setVisibility(false);
      this.view.LoanPayOff.flxError.setVisibility(false);
      this.view.flxPayDueAmount.setVisibility(false);
      this.view.flxLoanPayOff.setVisibility(true);
      this.view.LoanPayOff.tbxOptional.text = " ";
      this.view.flxTermsAndConditions.setVisibility(true);
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = "LOAN PAY-OFF";
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
    },
    /**
     * Show UI for Pay Due Amount Flow
     * @member frmPayDueAmountController
     * @param {void}  None
     * @returns {void} - None
     * @throws {void} - None
     */
    loanPayDueAmountInit: function (data) {     
      this.loadAccounts();
      this.setDataToForm(data.accounts);
      this.initActionsLoanDue();
      if (data.fromAccount) {
        this.fromAccount = data.fromAccount;
      } 
      this.loanPayDueAmountUI();     
      this.loanContext = "Loan Due";   
      this.view.forceLayout();
    },
    /**
     * Set UI for Loan Pay Due Amount
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    loanPayDueAmountUI:function(){
      this.view.flxDownTimeWarning.setVisibility(false);
      this.view.flxTermsAndConditions.setVisibility(false);
      this.view.flxAcknowledgement.setVisibility(false);
      this.view.flxConfirmation.setVisibility(false);
      this.view.flxPayDueAmount.setVisibility(true);
      this.view.flxLoanPayOff.setVisibility(false);
      this.view.PayDueAmount.lblDueAmount.isVisible = false;
      this.view.PayDueAmount.tbxOptional.text = " ";
      this.view.lblConfirmationTitle.text = kony.i18n.getLocalizedString("i18n.loan.loanDuePayment");      
      this.view.lblAcknowledgementTitle.text = kony.i18n.getLocalizedString("i18n.loan.loanPayoffPayment");
      if (CommonUtilities.getConfiguration("modifyLoanPaymentAmount") === "false") {
        this.view.PayDueAmount.flxRadioPayOtherAmount.setVisibility(false);
      }
      else {
        this.view.PayDueAmount.flxRadioPayOtherAmount.setVisibility(true);
      }         
      this.showPayDueAmount();
      this.enableButton(this.view.PayDueAmount.btnPayAmount);
    },
    /**
     * Handle server error, shows serverFlex
     * @member frmPayDueAmountController
     * @param {object} serverError error
     * @returns {void} - None
     * @throws {void} - None
     */
    showServerError: function (serverError) {
      this.view.flxDownTimeWarning.setVisibility(true);
      this.view.rtxDowntimeWarning.setVisibility(true);
      this.view.rtxDowntimeWarning.text = serverError;
    },
    /**
      * Function to update hamburger menu
      * @member frmPayDueAmountController
      * @param {object}  sideMenuModel contains on Click listeners for hamburger buttons
      * @returns {void} - None
      * @throws {void} - None
      */
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel);
    },
    /**
      * Function to update top bar
      * @member frmPayDueAmountController
      * @param {object}  topBarModel contains on Click listeners for topmenu
      * @returns {void} - None
      * @throws {void} - None
      */
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },
    /**
     * Function initialize actions for Loans flow
     * @member frmPayDueAmountController
     * @param {void}  - None
     * @returns {void} - None
     * @throws {void} - None
     */
    initActionsLoanDue: function () {
      this.view.PayDueAmount.btnPayAmount.onClick = this.payDueAmountPay;
      this.view.PayDueAmount.btnCancel.onClick = this.payDueAmountCancel;
      this.view.PayDueAmount.tbxAmount.onBeginEditing = this.hideAmountError;
      this.view.PayDueAmount.CalendarSendDate.onTouchStart = this.hideCalendarError;
    },
    /**
     * Function to register Actions for Loans
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    initActions: function () {
      var scopeObj = this;
      this.view.PayDueAmount.tbxAmount.onKeyUp = this.amountTextboxAction;
      this.view.PayDueAmount.tbxAmount.onTextChange = this.view.PayDueAmount.tbxAmount.onEndEditing = function() {
        CommonUtilities.validateAndFormatAmount(scopeObj.view.PayDueAmount.tbxAmount);
      }
      this.view.CustomPopup1.btnNo.onClick = this.quitPopUpNo;
      this.view.CustomPopup1.btnYes.onClick = this.quitPopUpYes;
      this.view.btnViewAccountDetail.onClick = this.backToAccountDetails;
      this.view.btnBackToAccountSummary.onClick = this.presenter.backToAccount;
      this.view.btnBackToAccountDeatil.onClick = this.backToAccountDetails;
      this.view.LoanPayOff.listbxTo.onSelection = this.setDataOnNewSelection;
      this.view.LoanPayOff.flxCheckbox.onClick = this.onSelectionOfCheckbox;
      this.view.LoanPayOff.btnCancel.onClick = this.payDueAmountCancel;
      this.view.customheader.headermenu.btnLogout.onClick = this.showLogout;
      this.view.CustomPopup.btnYes.onClick = this.performLogout;
      this.view.CustomPopup.btnNo.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      };
    },
    /**
     * Function to hide Amount for Pay Due Amount flow
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    hideAmountError: function () {
      this.view.PayDueAmount.lblDueAmount.setVisibility(true);
      this.view.PayDueAmount.flxInfoDueAmount.setVisibility(false);
      CommonUtilities.removeDelimitersForAmount(this.view.PayDueAmount.tbxAmount);
    },
    /**
     * Function to Hide Calendar Error
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    hideCalendarError: function () {
      this.view.PayDueAmount.flxInfoDueDate.setVisibility(false);
      this.view.PayDueAmount.lblDueDate.setVisibility(true);
    },
    /**
     * Function to enable and disable Confirm button for Loan Pay Off
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    onSelectionOfCheckbox: function () {
      if (!CommonUtilities.isChecked(this.view.LoanPayOff.imgCheckbox)) {
        CommonUtilities.setCheckboxState(true, this.view.LoanPayOff.imgCheckbox);
        CommonUtilities.enableButton(this.view.LoanPayOff.btnConfirm);
      }
      else {
        CommonUtilities.setCheckboxState(false, this.view.LoanPayOff.imgCheckbox);
        CommonUtilities.disableButton(this.view.LoanPayOff.btnConfirm);
      }
    },
    /**
     * Function to show view based on parameter
     * @member frmPayDueAmountController
     * @param {Array} views 
     * @returns {void} - None
     * @throws {void} - None
     */
    showView: function (views) {
      this.view.flxMyPaymentAccounts.isVisible = false;
      this.view.flxPayDueAmount.isVisible = false;
      this.view.flxLoanPayOff.isVisible = false;
      this.view.flxPrimaryActions.isVisible = false;
      this.view.flxConfirmation.isVisible = false;
      this.view.flxAcknowledgement.isVisible = false;
      this.view.flxQuitPayment.isVisible = false;
      this.view.flxDownTimeWarning.isVisible = false;
      for (var i = 0; i < views.length; i++) {
        this.view[views[i]].isVisible = true;
      }
    },
    /**
     * Pre-show for frmPayDueAmount form
     * @member frmPayDueAmountController
     * @param {void} None  
     * @returns {void} - None
     * @throws {void} - None
     */
    frmPayDueAmountPreShow: function () {
      var scopeObj = this;
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
      this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.flxDownTimeWarning.setVisibility(false);
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountBreadcrumb");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      CommonUtilities.setCheckboxState(false, this.view.LoanPayOff.imgCheckbox);
      CommonUtilities.disableButton(this.view.LoanPayOff.btnConfirm);
      this.view.customheader.forceCloseHamburger();
      this.initActions();

    },
    /**
     * Function to change UI when radio buttons are toggled,Pay Due Amount
     * @member frmPayDueAmountController
     * @param {String} obj stores context for Pay Due amount - {Due/Other}
     * @returns {void} - None
     * @throws {void} - None
     */
    payDueAmountRadioButton: function (obj) {
      this.view.PayDueAmount.flxError.setVisibility(false);
      this.view.PayDueAmount.flxInfoDueDate.setVisibility(false);
      this.view.PayDueAmount.lblDueDate.setVisibility(true);
      this.view.PayDueAmount.flxInfoDueAmount.setVisibility(false);
      this.view.PayDueAmount.CalendarSendDate.dateFormat = "dd/MM/yyyy";
      this.view.PayDueAmount.CalendarSendDate.date = kony.os.date("dd/MM/yyyy");
      this.view.PayDueAmount.CalendarSendDate.dateFormat = "MM/dd/yyyy";
      if (obj === "Due") {
        if (this.view.PayDueAmount.imgRadioPayDueAmount.src === "icon_radiobtn.png") {
          this.enableButton(this.view.PayDueAmount.btnPayAmount);
          this.view.PayDueAmount.imgRadioPayDueAmount.src = "icon_radiobtn_active.png";
          this.view.PayDueAmount.imgRadioPayOtherAmount.src = "icon_radiobtn.png";
          this.view.PayDueAmount.tbxAmount.skin = "skntbx727272Lato15Opacity20";
          this.view.PayDueAmount.tbxAmount.setEnabled(false);
          this.view.PayDueAmount.lblDueAmount.isVisible = false;
          this.view.PayDueAmount.tbxAmount.text = this.dueAmount;
        }
      } else if (obj === "Other") {
        if (this.view.PayDueAmount.imgRadioPayOtherAmount.src === "icon_radiobtn.png") {
          this.disableButton(this.view.PayDueAmount.btnPayAmount);
          this.view.PayDueAmount.imgRadioPayDueAmount.src = "icon_radiobtn.png";
          this.view.PayDueAmount.imgRadioPayOtherAmount.src = "icon_radiobtn_active.png";
          this.view.PayDueAmount.tbxAmount.skin = "sknTbxLatoffffff15PxBorder727272opa20";
          this.view.PayDueAmount.tbxAmount.setEnabled(true);
          this.view.PayDueAmount.lblDueAmount.isVisible = true;
          this.view.PayDueAmount.tbxAmount.text = " ";
          this.view.PayDueAmount.lblDueAmount.text = (kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ": $" + this.dueAmount);
        }
      }
    },
    /**
      * Function to show Quit pop up on click of cancel for Pay Due Amount 
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    payDueAmountCancel: function () {
      var height = this.view.flxHeader.frame.height + this.view.flxContainer.frame.height;
      this.view.flxQuitPayment.height = height + "dp";
      this.view.flxQuitPayment.setVisibility(true);
    },
    /**
     * Triggers on click of Pay Amount Button for Pay Due Amount, Show confirmation after validating data
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    payDueAmountPay: function () {
      var scopeObj = this;
      var isAmountValid = true;
      
      var isDateBeforeDue = true;
      var data = {};
      isAmountValid = this.amountValidation(this.view.PayDueAmount.tbxAmount.text.trim(), this.balanceInFromAccount, this.dueAmount);
      isDateBeforeDue = this.dateValidation(this.view.PayDueAmount.CalendarSendDate.date, this.dueDate);
      this.view.forceLayout();
      if (isAmountValid && isDateBeforeDue) {
        if (this.view.PayDueAmount.imgRadioPayDueAmount.src === "icon_radiobtn_active.png") {
          data = {
            "fromAccount": scopeObj.view.PayDueAmount.listbxFrom.selectedKeyValue[1],
            "fromAccountID": scopeObj.view.PayDueAmount.listbxFrom.selectedKey,
            "toAccount": scopeObj.view.PayDueAmount.listbxTo.selectedKeyValue[1],
            "toAccountID": scopeObj.view.PayDueAmount.listbxTo.selectedKey,
            "amount": parseFloat(scopeObj.removeCurrency(scopeObj.dueAmount)),
            "date": scopeObj.view.PayDueAmount.CalendarSendDate.date,
            "description": scopeObj.view.PayDueAmount.tbxOptional.text.trim(),
            "payment": parseFloat(scopeObj.removeCurrency(scopeObj.view.PayDueAmount.tbxAmount.text.trim())),
            "penalty": parseFloat(scopeObj.removeCurrency(scopeObj.penalty)),
          };
          var tdate = kony.os.date("mm/dd/yyyy");
          if (this.getDateObject(data.date).getTime() <= this.getDateObject(tdate).getTime()) {
            data.isScheduled = "false";
          }
          else {
            data.isScheduled = "true";
          }
          if (scopeObj.getDateObject(data.date).getTime() > scopeObj.getDateObject(this.dueDate).getTime()) {
            this.showConfirmationPayOtherAmount(data);
          }
          else {
            this.showConfirmationPayDueAmount(data);
            this.view.confirmation.flxContainerPenaltyAmount.setVisibility(false);
            this.view.confirmation.confirmButtons.top = "0px";
          }
        }
        else {
           data = {
            "fromAccount": scopeObj.view.PayDueAmount.listbxFrom.selectedKeyValue[1],
            "fromAccountID": scopeObj.view.PayDueAmount.listbxFrom.selectedKey,
            "toAccount": scopeObj.view.PayDueAmount.listbxTo.selectedKeyValue[1],
            "toAccountID": scopeObj.view.PayDueAmount.listbxTo.selectedKey,
            "amount": parseFloat(scopeObj.removeCurrency(scopeObj.dueAmount)),
            "date": scopeObj.view.PayDueAmount.CalendarSendDate.date,
            "description": scopeObj.view.PayDueAmount.tbxOptional.text.trim(),
            "dueDate": scopeObj.dueDate,
            "payment": parseFloat(scopeObj.removeCurrency(scopeObj.view.PayDueAmount.tbxAmount.text.trim())).toFixed(2),
            "penalty": parseFloat(scopeObj.removeCurrency(scopeObj.penalty)),
            "remainingAmount": parseFloat(scopeObj.removeCurrency(scopeObj.dueAmount)) - parseFloat(scopeObj.removeCurrency(scopeObj.view.PayDueAmount.tbxAmount.text)),
          };
          data.remainingAmount = data.remainingAmount.toFixed(2);
          if (data.payment === data.amount) {
            this.view.confirmation.flxError.setVisibility(false);
            this.view.confirmation.confirmButtons.top = "0px";
          }
          else {
            this.view.confirmation.flxError.setVisibility(true);
            this.view.confirmation.lblError.text = kony.i18n.getLocalizedString("i18n.Error.partialPayment");
            this.view.confirmation.confirmButtons.top = "20px";
          }
          var currDate = kony.os.date("mm/dd/yyyy");
          if (this.getDateObject(data.date).getTime() <= this.getDateObject(currDate).getTime()) {
            data.isScheduled = "false";
          }
          else {
            data.isScheduled = "true";
          }
          this.showConfirmationPayOtherAmount(data);
          this.AdjustScreen();
          this.view.forceLayout();
        }
      }
    },
    /**
      *  Validate date and show error for Pay Due Amount
      * @member frmPayDueAmountController
      * @param {Date} date
      * @param {Date} dateDue
      * @returns {boolean} true/false
      * @throws {void} - None
      */
    dateValidation: function (date, dateDue) {
      var scopeObj = this;
      if (scopeObj.getDateObject(date).getTime() > scopeObj.getDateObject(dateDue).getTime()) {
        if (CommonUtilities.getConfiguration("loanPaymentAfterDueDateEnabled") === "false") {
          scopeObj.view.PayDueAmount.flxError.setVisibility(true);
          scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.Error.scheduledDateError");
          return false;
        }
        if (scopeObj.view.PayDueAmount.flxInfoDueDate.isVisible === false) {
          scopeObj.view.PayDueAmount.flxInfoDueDate.setVisibility(true);
          scopeObj.view.PayDueAmount.lblDueDate.setVisibility(false);
          return false;
        }
        else {
          return true;
        }
      }
      else {
        scopeObj.view.PayDueAmount.flxInfoDueDate.setVisibility(false);
        scopeObj.view.PayDueAmount.lblDueDate.setVisibility(true);
        return true;
      }
    },
    /**
     *  Validate amount and show error for Pay Due Amount
     * @member frmPayDueAmountController
     * @param {String} enteredAmount
     * @param {String} accountBalance
     * @param {String} dueAmount
     * @returns {boolean} - true/false
     * @throws {void} - None
     */
    amountValidation: function (enteredAmount, accountBalance, dueAmount) {
      var scopeObj = this;
      var numCommas = /^(?!0\.00)\d{1,3}(,\d{3})*(\.\d*)?$/;
      var num = /^\d*(\.\d+)?$/;
      var flagNum = enteredAmount.match(num);
      var flag = enteredAmount.match(numCommas);
      if (enteredAmount === "" || enteredAmount === undefined || (flagNum === null && flag === null) || parseFloat(enteredAmount) <= 0 || enteredAmount.length < 1) {
        scopeObj.view.PayDueAmount.flxError.setVisibility(true);
        scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.common.errorEnterAmount");
        return false;
      }
      enteredAmount = parseFloat(this.removeCurrency(enteredAmount));
      accountBalance = parseFloat(this.removeCurrency(accountBalance));
      dueAmount = parseFloat(this.removeCurrency(dueAmount));
      if (enteredAmount > dueAmount) {
        scopeObj.view.PayDueAmount.flxError.setVisibility(true);
        scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.Error.amountMoreThanDueAmount");
        return false;
      } else if (enteredAmount > accountBalance) {
        scopeObj.view.PayDueAmount.flxError.setVisibility(true);
        scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.common.errorInsufficientFunds");
        return false;
      } else if ((enteredAmount + parseFloat(scopeObj.penalty)) > accountBalance) {
        scopeObj.view.PayDueAmount.flxError.setVisibility(true);
        scopeObj.view.PayDueAmount.lblError.text = kony.i18n.getLocalizedString("i18n.Error.insufficientFunds") + " (" + kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ": " + this.currencySymbol + dueAmount + " , " + kony.i18n.getLocalizedString("i18n.loan.Penalty") + " : " + this.currencySymbol + scopeObj.penalty + ")";
        return false;
      } else {
        scopeObj.view.PayDueAmount.flxError.setVisibility(false);
        if (scopeObj.view.PayDueAmount.flxInfoDueAmount.isVisible === false && (dueAmount - enteredAmount) > 0) {
          scopeObj.view.PayDueAmount.flxInfoDueAmount.setVisibility(true);
          scopeObj.view.PayDueAmount.lblDueAmount.setVisibility(false);
          var dueDate = this.presenter.getFormattedDateString(this.dueDate);
          var tempDue = CommonUtilities.formatCurrencyWithCommas(scopeObj.view.PayDueAmount.tbxAmount.text);
          scopeObj.view.PayDueAmount.tbxAmount.text = tempDue.slice(1);
          scopeObj.view.PayDueAmount.lblInfoAmount.text = kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ": " + CommonUtilities.formatCurrencyWithCommas(dueAmount.toFixed(2)) + "." + kony.i18n.getLocalizedString("i18n.Error.payRemainingBalance") + " " + CommonUtilities.formatCurrencyWithCommas(parseFloat(dueAmount - enteredAmount).toFixed(2)) + " " + kony.i18n.getLocalizedString("i18n.common.onBefore") + " " + dueDate + " " + kony.i18n.getLocalizedString("i18n.Error.avoidPenalty");
          scopeObj.view.forceLayout();
        } else {
          return true;
        }
      }
    },
    /**
      *  Set confirmation values
      * @member frmPayDueAmountController
      * @param {JSON} data for left container
      * @param {JSON} extraData for right container
      * @param {JSON} actions for buttons
      * @returns {void} - None
      * @throws {void} - None
      */
    setConfirmationValues: function (data, extraData, actions) {
      var target = this.view.confirmation.flxLeft.widgets();
      if (data.Description) {
        this.view.confirmation.flxContainerPaymentDate.setVisibility(true);
      }
      else {
        this.view.confirmation.flxContainerPaymentDate.setVisibility(false);
      }
      var i = 0;
      var prop,key,value;
      for (prop in data) {
        key = target[i].widgets()[0].widgets()[0];
        value = target[i].widgets()[2].widgets()[0];
        key.text = prop;
        value.text = data[prop];
        i++;
      }
      if (Object.keys(extraData).length !== 0) {
        this.view.confirmation.flxPartialPayment.isVisible = true;
        target = this.view.confirmation.flxPartialPaymentdetails.widgets();
        i = 1;
        for (prop in extraData) {
          key = target[i].widgets()[0].widgets()[0];
          value = target[i].widgets()[1].widgets()[0];
          key.text = prop;
          value.text = extraData[prop];
          i++;
        }
        this.view.confirmation.confirmButtons.top = "0px";
      } else {
        this.view.confirmation.flxPartialPayment.isVisible = false;
        this.view.confirmation.confirmButtons.top = "20px";
      }

      this.view.confirmation.confirmButtons.btnCancel.onClick = actions.cancel;
      this.view.confirmation.confirmButtons.btnModify.onClick = actions.modify;
      if(CommonUtilities.isCSRMode()){
         this.view.confirmation.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();  
         this.view.confirmation.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode(); 
         this.view.confirmation.confirmButtons.btnConfirm.focusSkin = CommonUtilities.disableButtonSkinForCSRMode(); 
      }
      else{
      if (this.loanContext === "Loan Payoff") {
        this.view.confirmation.confirmButtons.btnConfirm.onClick = this.loanPayOffPayment;
      }
      else {
        this.view.confirmation.confirmButtons.btnConfirm.onClick = actions.confirm;
       }
      }
    },
    /**
    *  Function to set footer height
    * @member frmPayDueAmountController
    * @param {void} - None 
    * @returns {void} - None
    * @throws {void} - None
    */
    postShowPayDueAmount: function () {
      this.AdjustScreen();
      this.setFlowActions();
    },
    /**
     *  Set Screen height
     * @member frmPayDueAmountController
     * @param {void} - None 
     * @returns {void} - None
     * @throws {void} - None
     */
    AdjustScreen: function () {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0)
          this.view.flxFooter.top = mainheight + diff + "dp";
        else
          this.view.flxFooter.top = mainheight + "dp";
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },
    /**
    *  Set flow actions
     * @member frmPayDueAmountController
     * @param {void} - None 
     * @returns {void} - None
     * @throws {void} - None
     */
    setFlowActions: function () {
      var scopeObj = this;
      this.view.LoanPayOff.flxImageInfo.onClick = function () {
        if (scopeObj.view.LoanPayOff.AllForms.isVisible === false)
          scopeObj.view.LoanPayOff.AllForms.isVisible = true;
        else
          scopeObj.view.LoanPayOff.AllForms.isVisible = false;
      };
      this.view.LoanPayOff.AllForms.flxCross.onClick = function () {
        scopeObj.view.LoanPayOff.AllForms.isVisible = false;
      };
    },
    /**
    *  Set Acknowledgement Values
     * @member frmPayDueAmountController
     * @param {JSON} data
     * @param {JSON} details 
     * @param {JSON} extraData 
     * @returns {void} - None
     * @throws {void} - None
     */
    setAcknowledgementValues: function (data, details, extraData) {
      this.view.acknowledgment.lblTransactionMessage.text = data.message;
      this.view.acknowledgment.ImgAcknowledged.src = data.image;
      this.view.acknowledgment.lblRefrenceNumberValue.text = data.referenceNumber;
      this.view.acknowledgment.lblAccType.text = data.accountType;
      this.view.acknowledgment.lblBalance.text = data.amount;
      if (details.Description) {
        this.view.confirmDialog.flxContainerDescription.setVisibility(true);
      }
      else {
        this.view.confirmDialog.flxContainerDescription.setVisibility(false);
      }
      this.view.forceLayout();
      var target = this.view.confirmDialog.flxMain.widgets();
      var i = 2;
      for (var prop in details) {
        var key = target[i].widgets()[0].widgets()[0];
        var value = target[i].widgets()[1].widgets()[0];
        key.text = prop;
        value.text = details[prop];
        i++;
      }
      if (Object.keys(details).length > 5) {
        this.view.confirmDialog.flxContainerPenalty.isVisible = true;
        this.view.confirmDialog.flxContainerPartial.isVisible = true;
      } else {
        this.view.confirmDialog.flxContainerPenalty.isVisible = false;
        this.view.confirmDialog.flxContainerPartial.isVisible = false;
      }
      if (Object.keys(extraData).length !== 0) {
        this.view.confirmDialog.flxPartial.isVisible = true;
        target = this.view.confirmDialog.flxPartial.widgets();
        var i = 1;
        for (var prop in extraData) {
          var key = target[i].widgets()[0].widgets()[0];
          var value = target[i].widgets()[1].widgets()[0];
          key.text = prop;
          value.text = extraData[prop];
          i++;
        }
      } else {
        this.view.confirmDialog.flxPartial.isVisible = false;
      }
      this.view.forceLayout();
    },
    /**
    *  Show confirmation for Pay Due Amount
     * @member frmPayDueAmountController
     * @param {Object} obj stores transaction details
     * @returns {void} - None
     * @throws {void} - None
     */
    showConfirmationPayDueAmount: function (obj) {
      var scopeObj = this;
      this.view.confirmation.flxContainerPenaltyAmount.setVisibility(false);
      this.view.confirmation.flxContainerDescription.setVisibility(false);
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountConfirmation");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      var data = {
        "From": obj.fromAccount,
        "To": obj.toAccount,
        "Amount($)": CommonUtilities.formatCurrencyWithCommas(obj.amount).slice(1),
        "Date": obj.date,
        "Description": obj.description,
      };
      scopeObj = this;
      var actions = {
        "cancel": function () {
          scopeObj.showQuitPopUp();
        },
        "modify": function () {
          scopeObj.showPayDueAmount();
        },
        "confirm": function () {
          scopeObj.successData = obj;
          scopeObj.confirmPayDueAmount(obj, "payCompleteMonthlyDue");
        }
      };
      this.setConfirmationValues(data, {}, actions);
      this.showView(["flxConfirmation"]);
    },
    /**
    *  Confirm Pay Due Amount/Pay Other Amount
     * @member frmPayDueAmountController
     * @param {Object} obj stores transaction details
     * @param {function} action stores success callback 
     * @returns {void} - None
     * @throws {void} - None
     */
    confirmPayDueAmount: function (obj, action) {
      var data = {};
      CommonUtilities.showProgressBar(this.view);
      data.fromAccountID = obj.fromAccountID;
      data.toAccountID = obj.toAccountID;
      data.fromAccount = obj.fromAccount;
      data.toAccount = obj.toAccount;
      data.notes = obj.description;
      data.date = this.presenter.getBackendDate(obj.date, CommonUtilities.getConfiguration("frontendDateFormat"));
      if (this.view.PayDueAmount.imgRadioPayDueAmount.src === "icon_radiobtn_active.png") {
        data.amount = obj.amount;
      }
      else {
        data.amount = obj.payment;
      }
      if (this.getDateObject(data.date).getTime() > this.getDateObject(this.dueDate).getTime()) {
        data.penaltyFlag = "true";
        data.payoffFlag = "false";
      }
      data.isScheduled = obj.isScheduled;
      this.presenter.payLoanOff(data, action);
    },
    /**
    *  Success Callback for Pay Due Amount
     * @member frmPayDueAmountController
     * @param {Object} data
     * @param {String} response
     * @returns {void} - None
     * @throws {void} - None
     */
    successPayDueAmount: function (data, response) {
      var scopeObj = this;
      data = scopeObj.successData;
      var scheduledFlag = data.isScheduled;
      this.view.flxMainAcknowledgment.setVisibility(true);
      scopeObj.showAcknowledgementPayDueAmount(data, scheduledFlag);
      if (CommonUtilities.getConfiguration("showLoanUpdateDisclaimer") === "true") {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(true);
      }
      else {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(false);
      }
      this.view.acknowledgment.lblRefrenceNumberValue.text = response;
      this.presenter.fetchUpdatedAccountDetails(data.fromAccountID, "updateFromAccount");
      this.presenter.fetchUpdatedAccountDetails(data.toAccountID, "updateToAccount");
    },
    /**
    *  Success Callback for Pay Other Amount
     * @member frmPayDueAmountController
     * @param {Object} data
     * @param {String} response
     * @returns {void} - None
     * @throws {void} - None
     */
    successPayOtherAmount: function (data, response) {
      var scopeObj = this;
      this.view.flxMainAcknowledgment.setVisibility(true);
      data = scopeObj.successData;
      var scheduledFlag = data.isScheduled;
      scopeObj.showAcknowledgementPayOtherAmount(data, scheduledFlag);
      if (CommonUtilities.getConfiguration("showLoanUpdateDisclaimer") === "true") {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(true);
      }
      else {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(false);
      }
      this.view.acknowledgment.lblRefrenceNumberValue.text = response;
      this.presenter.fetchUpdatedAccountDetails(data.fromAccountID, "updateFromAccount");
      this.presenter.fetchUpdatedAccountDetails(data.toAccountID, "updateToAccount");
    },
    /**
     *  Set updated account value for From Account on acknowledgement screen 
      * @member frmPayDueAmountController
      * @param {Object} account - from Account
      * @returns {void} - None
      * @throws {void} - None
      */
    updateAccountDetails: function (account) {
      this.view.acknowledgment.lblAccType.text = account.accountName;
      this.view.acknowledgment.lblBalance.text = CommonUtilities.formatCurrencyWithCommas(account.availableBalance);
      this.view.confirmDialog.lblValue.text = account.accountName + " ..." + account.accountID.slice(-4) + " " + this.getDisplayBalance(account);
      CommonUtilities.hideProgressBar(this.view);
      this.view.forceLayout();
    },
    /**
    *  Set updated account value for To Account on acknowledgement screen  
     * @member frmPayDueAmountController
     * @param {Object} account - to Account
     * @returns {void} - None
     * @throws {void} - None
     */
    updateToAccountDetails: function (account) {
      this.view.confirmDialog.lblValueTo.text = account.accountName + " ..." + account.accountID.slice(-4) + " " + this.getDisplayToBalance(account);
      this.view.forceLayout();
    },
    /**
     *  Show confirmation for Pay other amount  
      * @member frmPayDueAmountController
      * @param {Object} obj stores trannsaction details
      * @returns {void} - None
      * @throws {void} - None
      */
    showConfirmationPayOtherAmount: function (obj) {
      var scopeObj = this;
      var extraData = {};
      this.view.confirmation.flxContainerPenaltyAmount.setVisibility(false);
      this.view.confirmation.flxContainerDescription.setVisibility(false);
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountConfirmation");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      var data = {
        "From": obj.fromAccount,
        "To": obj.toAccount,
        "Amount($)": CommonUtilities.formatCurrencyWithCommas(obj.amount).slice(1),
        "Date": scopeObj.presenter.getFormattedDateString(scopeObj.dueDate),
        "Description": obj.description,
      };
      if (scopeObj.getDateObject(obj.date).getTime() > scopeObj.getDateObject(this.dueDate).getTime()) {
        scopeObj.view.confirmation.flxError.setVisibility(true);
        scopeObj.view.confirmation.lblError.text = kony.i18n.getLocalizedString("i18n.loan.payingAfterDueDate");
        extraData = {
          "You are paying:": CommonUtilities.formatCurrencyWithCommas(obj.payment),
          "Due Date:": scopeObj.presenter.getFormattedDateString(scopeObj.dueDate),
          "Your Payment date:": obj.date,
          "Late Payment Penalty:": CommonUtilities.formatCurrencyWithCommas(obj.penalty),
        };
      }
      else {
        extraData = {
          "Your Bill Amount:": CommonUtilities.formatCurrencyWithCommas(obj.amount),
          "You are paying:": CommonUtilities.formatCurrencyWithCommas(obj.payment),
          "Balance Amount:": CommonUtilities.formatCurrencyWithCommas(obj.remainingAmount),
          "Date:": obj.date,
        };
      }
      scopeObj = this;
      var actions = {
        "cancel": function () {
          scopeObj.showQuitPopUp();
        },
        "modify": function () {
          scopeObj.showPayDueAmount();
        },
        "confirm": function () {
          scopeObj.successData = obj;
          scopeObj.confirmPayDueAmount(obj, "payOtherAmount");
        }
      };
      this.setConfirmationValues(data, extraData, actions);
      this.showView(["flxConfirmation"]);
    },
    /**
     *  Show acknowledgement for Pay Due amount
      * @member frmPayDueAmountController
      * @param {Object} obj stores trannsaction details
      * @param {boolean} scheduledFlag 
      * @returns {void} - None
      * @throws {void} - None
      */
    showAcknowledgementPayDueAmount: function (obj, scheduledFlag) {
      var scopeObj = this;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountAcknowledgement");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      var data = {
        "message": "Your transaction has been done successfully.  Payments may take 2-3 business days to be  reflected in your loan account ",
        "image": "success_green.png",
        "referenceNumber": " ",
        "accountType": " ",
        "amount": " ",
      };
      var details = {
        "From": obj.fromAccount,
        "To": obj.toAccount,
        "Amount($)": scopeObj.dueAmount,
        "Date": obj.date,
        "Description": obj.description,
      };
      if (scheduledFlag==="true") {
        data.message = "Your transaction has been scheduled for " + obj.date;
      }
      this.setAcknowledgementValues(data, details, {});

      this.showView(["flxAcknowledgement"]);
    },
    /**
      *  Show acknowledgement for Pay other amount
       * @member frmPayDueAmountController
       * @param {Object} obj stores trannsaction details
       * @param {boolean} scheduledFlag 
       * @returns {void} - None
       * @throws {void} - None
       */
    showAcknowledgementPayOtherAmount: function (obj, scheduledFlag) {
      var scopeObj = this;
      var extraData = {};
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = kony.i18n.getLocalizedString("i18n.loan.payDueAmountAcknowledgement");
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      var data = {
        "message": "Your transaction has been done successfully.  Payments may take 2-3 business days to be  reflected in your loan account ",
        "image": "success_green.png",
        "referenceNumber": " ",
        "accountType": " ",
        "amount": " ",
      };
      var details = {
        "From": obj.fromAccount,
        "To": obj.toAccount,
        "Amount($)": CommonUtilities.formatCurrencyWithCommas(obj.amount).slice(1),
        "Date": scopeObj.presenter.getFormattedDateString(scopeObj.dueDate),
        "Description": obj.description,
      };
      if (scopeObj.getDateObject(obj.date).getTime() < scopeObj.getDateObject(this.dueDate).getTime()) {
        extraData = {
          "Your Bill Amount:": CommonUtilities.formatCurrencyWithCommas(obj.amount),
          "You are paying:": CommonUtilities.formatCurrencyWithCommas(parseFloat(obj.payment)),
          "Balance Amount:": CommonUtilities.formatCurrencyWithCommas(obj.remainingAmount),
          "Date:": obj.date,
        };
      }
      else {
        extraData = {
          "You are paying:": CommonUtilities.formatCurrencyWithCommas(parseFloat(obj.payment)),
          "Due Date:": scopeObj.presenter.getFormattedDateString(scopeObj.dueDate),
          "Your Payment date:": scopeObj.presenter.getFormattedDateString(obj.date),
          "Late Payment Penalty:": CommonUtilities.formatCurrencyWithCommas(obj.penalty),
        };
      }
      if (scheduledFlag==="true") {
        data.message = "Your transaction has been scheduled for " + obj.date;
      }
      this.setAcknowledgementValues(data, details, extraData);

      this.showView(["flxAcknowledgement"]);
    },
    /**
   *  Show Loan Pay Off Confirmation
    * @member frmPayDueAmountController
    * @param {void} -None
    * @returns {void} - None
    * @throws {void} - None
    */
    showConfirmationLoanPayOff: function () {
      var scopeObj = this;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = "LOAN PAYOFF- CONFIRMATION";
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      this.view.flxBottom.setVisibility(false);
      var data = {
        "From": "Personal Checking….1234",
        "To": "My Car Loan Account…XXXX9870",
        "PayOff Amount": "$1000",
        "Actual Loan End Date": "07/15/2017",
        "Payment Date": "07/15/2017",
        "Penalty Amount": "$1000",
        "Note": "Car Loan EMI for July"
      };
      scopeObj = this;
      var actions = {
        "cancel": function () {
          scopeObj.showQuitPopUp();
        },
        "modify": function () {
          scopeObj.showLoanPayOff();
        },
        "confirm": function () {
          scopeObj.showAcknowledgementLoanPayOff();
        }
      };
      this.setConfirmationValues(data, {}, actions);
      this.showView(["flxConfirmation"]);
    },
    /**
     *  Show Loan Pay Off Acknowledgement
      * @member frmPayDueAmountController
      * @param {void} -None
      * @returns {void} - None
      * @throws {void} - None
      */
    showAcknowledgementLoanPayOff: function () {
      var scopeObj = this;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = "LOAN PAYOFF- CONFIRMATION";
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      this.showView(["flxAcknowledgement"]);
    },
    /**
     *  Show Pay Due Amount
      * @member frmPayDueAmountController
      * @param {void} -None
      * @returns {void} - None
      * @throws {void} - None
      */
    showPayDueAmount: function () {
      this.showView(["flxPayDueAmount", "flxMyPaymentAccounts", "flxPrimaryActions"]);
    },
    /**
     *  Show loan pay off
      * @member frmPayDueAmountController
      * @param {void} -None
      * @returns {void} - None
      * @throws {void} - None
      */
    showLoanPayOff: function () {
      var scopeObj = this;
      var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
      var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
      var text3 = "LOAN PAYOFF";
      this.view.breadcrumb.setBreadcrumbData([{ text: text1 }, { text: text2, callback: scopeObj.backToAccountDetails }, { text: text3 }]);
      this.view.flxBottom.setVisibility(true);
      this.showView(["flxLoanPayOff", "flxMyPaymentAccounts", "flxPrimaryActions"]);
      this.view.forceLayout();
      this.AdjustScreen();
    },
    /**
    *  Show quit popup
     * @member frmPayDueAmountController
     * @param {void} -None
     * @returns {void} - None
     * @throws {void} - None
     */
    showQuitPopUp: function () {
      var height = this.view.flxHeader.frame.height + this.view.flxContainer.frame.height;
      this.view.flxQuitPayment.height = height + "dp";
      this.view.flxQuitPayment.isVisible = true;
    },
    /**
    *  Triggered on No of Quit Pop up
     * @member frmPayDueAmountController
     * @param {void} -None
     * @returns {void} - None
     * @throws {void} - None
     */
    quitPopUpNo: function () {
      this.view.flxQuitPayment.setVisibility(false);
    },
    /**
    * Triggered on Yes of Quit Pop up
     * @member frmPayDueAmountController
     * @param {void} -None
     * @returns {void} - None
     * @throws {void} - None
     */
    quitPopUpYes: function () {
      this.quitPopUpNo();
      this.backToAccountDetails();
    },
    /**
     * Function to load all accounts
      * @member frmPayDueAmountController
      * @param {void} -None
      * @returns {void} - None
      * @throws {void} - None
      */
    loadAccounts: function () {
      var scopeObj = this;
      scopeObj.presenter.fetchCheckingAccounts();
    },
    /**
     * Callback to set masterdata for TO FROM account, to set payment accounts
      * @member frmPayDueAmountController
      * @param {Array} response
      * @returns {void} - None
      * @throws {void} - None
      */
    accountsListCallback: function (response) {
      this.initListboxSelectionAction(response);
      this.updateListBox(response);
      this.updateToListBox(response);
      this.updateTransferAccountList(response);
      this.presenter.fetchUpdatedAccountDetails(this.view.PayDueAmount.listbxFrom.selectedKey, "populateAccountData");
    },
    initListboxSelectionAction:function(accounts){
      this.view.PayDueAmount.listbxFrom.onSelection = this.updateFromAccountValue.bind(this,accounts);
      this.view.PayDueAmount.listbxTo.onSelection = this.updateToAccountValue.bind(this,accounts);
    },
    /**
     * Function to filter accounts for FROM ListBox,
      * @member frmPayDueAmountController
      * @param {Array} fromAccounts
      * @returns {void} - None
      * @throws {void} - None
      */
    updateListBox: function (fromAccounts) {
      var accounts = [];
      for (var i = 0, j = 0; i < fromAccounts.length; i++)
        if (fromAccounts[i].accountType !== "Loan" && fromAccounts[i].accountType !== "Mortgage" && fromAccounts[i].accountType !== "CreditCard") {
          accounts[j] = fromAccounts[i];
          j++;
        }
      this.view.LoanPayOff.listbxFrom.masterData = this.showAccountsForSelection(accounts);
      this.view.PayDueAmount.listbxFrom.masterData = this.showAccountsForSelection(accounts);
      if (this.fromAccount) {
        this.view.LoanPayOff.listbxFrom.selectedKey = this.fromAccount;
        this.view.PayDueAmount.listbxFrom.selectedKey = this.fromAccount;
        this.updateFromAccountValue(fromAccounts);
      }
    },
    /**
     * Function to filter accounts for TO ListBox
      * @member frmPayDueAmountController
      * @param {Array} fromAccounts
      * @returns {void} - None
      * @throws {void} - None
      */
    updateToListBox: function (fromAccounts) {
      var accounts = [];
      for (var i = 0, j = 0; i < fromAccounts.length; i++)
        if (fromAccounts[i].accountType === "Loan") {
          if (fromAccounts[i].principalBalance > 0) {
            accounts[j] = fromAccounts[i];
            j++;
          }
        }
      if (accounts.length > 0) {
        this.view.LoanPayOff.listbxTo.masterData = this.showAccountsForSelectionToBox(accounts);
        this.view.LoanPayOff.listbxTo.selectedKey = this.savedAccountID;
      }
      var accounts2 = [];
      for (var k = 0, l = 0; k < fromAccounts.length; k++)
        if (fromAccounts[k].accountType === "Loan" || fromAccounts[k].accountType === "Mortgage") {
          if (fromAccounts[k].currentAmountDue > 0 && fromAccounts[k].principalBalance > 0) {
            accounts2[l] = fromAccounts[k];
            l++;
          }
        }
      if (accounts2.length > 0) {
        this.view.PayDueAmount.listbxTo.masterData = this.showAccountsForSelectionToBox(accounts2);
        this.view.PayDueAmount.listbxTo.selectedKey = this.savedAccountID;

      }
    },
    /**
     * Function to return master data for "For" listBox
      * @member frmPayDueAmountController
      * @param {Array} presentAccounts eligible accounts
      * @returns {void} - None
      * @throws {void} - None
      */
    showAccountsForSelection: function (presentAccounts) {
      var list = [];
      for (var i = 0; i < presentAccounts.length; i++) {
        var tempList = [];
        tempList.push(presentAccounts[i].accountID);
        var tempAccountNumber = presentAccounts[i].accountID;
        tempList.push(presentAccounts[i].accountName + " ..." + tempAccountNumber.slice(-4) + " " + this.getDisplayBalance(presentAccounts[i]));
        list.push(tempList);
      }
      return list;
    },
    /**
     * Get display balance for To ListBox, based on context
      * @member frmPayDueAmountController
      * @param {Object} account 
      * @returns {void} - None
      * @throws {void} - None
      */
    getDisplayToBalance: function (account) {
      if (this.loanContext === "Loan Payoff") {
        return "(" + kony.i18n.getLocalizedString('i18n.loan.payOffAmount') + ":" + CommonUtilities.formatCurrencyWithCommas(account.principalBalance) + ")";
      }
      else if (this.loanContext === "Loan Due") {
        return "(" + kony.i18n.getLocalizedString('i18n.loan.dueAmount') + ":" + CommonUtilities.formatCurrencyWithCommas(account.currentAmountDue) + ")";
      }
    },
    /**
     * Function to return master data for To listBox
      * @member frmPayDueAmountController
      * @param {Array} presentAccounts eligible accounts
      * @returns {void} - None
      * @throws {void} - None
      */
    showAccountsForSelectionToBox: function (presentAccounts) {
      var list = [];
      for (var i = 0; i < presentAccounts.length; i++) {
        var tempList = [];
        tempList.push(presentAccounts[i].accountID);
        var tempAccountNumber = presentAccounts[i].accountID;
        tempList.push(presentAccounts[i].accountName + " ..." + tempAccountNumber.slice(-4) + " " + this.getDisplayToBalance(presentAccounts[i]));
        list.push(tempList);
      }
      return list;
    },
    /**
     * Function to get display balance in (Available: $123) format
      * @member frmPayDueAmountController
      * @param {Object} account 
      * @returns {void} - None
      * @throws {void} - None
      */
    getDisplayBalance: function (account) {
      return "(" + kony.i18n.getLocalizedString('i18n.common.available') + ":" + CommonUtilities.formatCurrencyWithCommas(account.availableBalance) + ")";
    },
    /**
     * Function to set Data initially
      * @member frmPayDueAmountController
      * @param {Object} data to initialize from account
      * @returns {void} - None
      * @throws {void} - None
      */
    setDataToForm: function (data) {
      var scopeObj = this;
      this.view.LoanPayOff.tbxOptional.maxTextLength = 150;
      this.view.PayDueAmount.tbxOptional.maxTextLength = 150;
      this.savedAccountID = data.accountID;
      this.flagPayOff = true;
      if (data.closingDate === null || data.closingDate === undefined || data.principalBalance === null || data.principalBalance === undefined || data.payOffCharge === null || data.payOffCharge === undefined) {
        this.flagPayOff = false;
      }
      this.view.LoanPayOff.tbxActualLoanEndDate.text = scopeObj.presenter.getFormattedDateString(data.closingDate);
      this.view.LoanPayOff.tbxLoanPayOffAmount.text = data.principalBalance;
      this.view.LoanPayOff.tbxPayoffPenality.text = data.payOffCharge;
      this.view.PayDueAmount.imgRadioPayDueAmount.src = "icon_radiobtn_active.png";
      this.view.PayDueAmount.imgRadioPayOtherAmount.src = "icon_radiobtn.png";
      this.view.PayDueAmount.tbxAmount.text = data.currentAmountDue;
      this.view.PayDueAmount.tbxAmount.skin = "skntbx727272Lato15Opacity20";
      this.view.PayDueAmount.tbxAmount.setEnabled(false);
      this.view.PayDueAmount.CalendarSendDate.dateFormat = "dd/MM/yyyy";
      this.view.PayDueAmount.CalendarSendDate.date = kony.os.date("dd/MM/yyyy");
      this.view.PayDueAmount.CalendarSendDate.dateFormat = "MM/dd/yyyy";
      this.view.PayDueAmount.flxInfoDueAmount.setVisibility(false);
      this.penalty = data.lateFeesDue;
      this.dueAmount = data.currentAmountDue;
      this.dueDate = data.dueDate;
      this.availableBalance = data.availableBalance;
      this.view.PayDueAmount.flxInfoDueDate.setVisibility(false);
      this.view.PayDueAmount.flxError.setVisibility(false);
      this.view.PayDueAmount.lblDueDate.text = "(" + kony.i18n.getLocalizedString("i18n.billPay.DueDate") + ": " + this.presenter.getFormattedDateString(data.dueDate) + ")";
      var amount1 = this.removeCurrency(data.principalBalance);
      var amount2 = this.removeCurrency(data.payOffCharge);
      this.fullPayOffAmount = Number(amount1) + Number(amount2);
      this.view.LoanPayOff.tbxActualLoanEndDate.setEnabled(false);
      this.view.LoanPayOff.tbxLoanPayOffAmount.setEnabled(false);
      this.view.LoanPayOff.tbxPayoffPenality.setEnabled(false);
      this.disableDaySelectionLoanPayOff(this.view.LoanPayOff.CalendarPyOn, this.view.LoanPayOff.tbxActualLoanEndDate.text);
      CommonUtilities.disableOldDaySelection(this.view.PayDueAmount.CalendarSendDate);
      this.view.LoanPayOff.CalendarPyOn.dateFormat = "dd/mm/yyyy";
      this.view.LoanPayOff.CalendarPyOn.date = kony.os.date("dd/mm/yyyy");
      this.view.LoanPayOff.CalendarPyOn.dateFormat = "mm/dd/yyyy";
      this.view.forceLayout();
    },
    /**
    * Function to disable date selectionm, Loan Pay Off
     * @member frmPayDueAmountController
     * @param {Object} widgetId 
     * @param {Date} toDate 
     * @returns {void} - None
     * @throws {void} - None
     */
    disableDaySelectionLoanPayOff: function (widgetId, toDate) {
      var date = new Date();
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      var yy = date.getFullYear();
      var date1 = toDate.split("/");
      widgetId.enableRangeOfDates([dd, mm, yy], [date1[1], date1[0], date1[2]], "skn", true);
    },
    /**
     * Pay Off enable button
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    enableButtonConfirm: function () {
      this.presenter.fetchUpdatedAccountDetails(this.view.LoanPayOff.listbxFrom.selectedKey, "validateData");
    },
    /**
    * Validate data for Pay Off
     * @member frmPayDueAmountController
     * @param {Object} account
     * @returns {void} - None
     * @throws {void} - None
     */
    validateData: function (account) {
      if (this.flagPayOff === false) {
        this.view.LoanPayOff.flxError.setVisibility(true);
        this.view.LoanPayOff.lblError.text = kony.i18n.getLocalizedString("i18n.loan.detailsIncorrectMsg");
      }
      else {
        if (account.availableBalance < this.fullPayOffAmount) {
          this.view.LoanPayOff.flxError.setVisibility(true);
          this.view.LoanPayOff.lblError.text = kony.i18n.getLocalizedString("i18n.common.errorInsufficientFunds");
        } else {
          if (!CommonUtilities.isChecked(this.view.LoanPayOff.imgCheckbox)) {
            this.view.LoanPayOff.flxError.setVisibility(true);
            this.view.LoanPayOff.lblError.text = kony.i18n.getLocalizedString("i18n.loan.termsandconditions");
          } else {
            this.view.LoanPayOff.flxError.setVisibility(false);
            this.navigateToConfirmation();
          }
        }
      }
      this.view.forceLayout();
    },
    /**
     * Funtion to get account object for selected account ID of From Listbox
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    updateFromAccountValue: function (frmAccounts) {
      var selectedKey = this.view.PayDueAmount.listbxFrom.selectedKey;
      var selectedAccount = frmAccounts.filter(function (accounts) {
        return accounts.accountID === selectedKey;
      })[0];
      this.populateFromAccountValues(selectedAccount);
    },
    /**
    * Function to update available balance for validation, when From account is selected
     * @member frmPayDueAmountController
     * @param {Object} account
     * @returns {void} - None
     * @throws {void} - None
     */
    populateFromAccountValues: function (account) {
      //get amount for validation
      this.balanceInFromAccount = account.availableBalance;
    },
    /**
     * Function to get updated account object for an accountID based on current context, callback to show account details
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    backToAccountDetails: function () {
      CommonUtilities.showProgressBar(this.view);
      var selectedKey;
      if (this.loanContext === "Loan Due") {
        selectedKey = this.view.PayDueAmount.listbxTo.selectedKey;
      }
      else if (this.loanContext === "Loan Payoff") {
        selectedKey = this.view.LoanPayOff.listbxTo.selectedKey;
      }
      this.presenter.fetchUpdatedAccountDetails(selectedKey, "navigationToAccountDetails");
    },
    /**
     * Call back for backToAccountDetails
      * @member frmPayDueAmountController
      * @param {Object} account
      * @returns {void} - None
      * @throws {void} - None
      */
    backToAccountDetailsCallback: function (account) {
      this.presenter.backToAccountDetails(account);
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
     * Function to get account object when To account is selected, Pay Due Amount
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    updateToAccountValue: function (frmAccounts) {
      var selectedKey = this.view.PayDueAmount.listbxTo.selectedKey;
      var selectedAccount = frmAccounts.filter(function (accounts) {
        return accounts.accountID === selectedKey;
      })[0];
      selectedAccount.currentAmountDue = CommonUtilities.formatCurrencyWithCommas(selectedAccount.currentAmountDue);
      selectedAccount.currentAmountDue = selectedAccount.currentAmountDue.slice(1);
      this.populateToAccountValues(selectedAccount);
    },
    /**
    * Function to populate and update values when To account is selected, Pay Due Amount
     * @member frmPayDueAmountController
     * @param {object} account
     * @returns {void} - None
     * @throws {void} - None
     */
    populateToAccountValues: function (account) {
      //due amount //due date //store due amount
      this.availableBalance = account.availableBalance;
      this.dueAmount = account.currentAmountDue;
      this.view.PayDueAmount.lblDueAmount.text = (kony.i18n.getLocalizedString("i18n.loan.dueAmount") + ": " + CommonUtilities.formatCurrencyWithCommas(this.dueAmount));
      this.dueDate = account.dueDate;
      this.view.PayDueAmount.lblDueDate.text = "(" + kony.i18n.getLocalizedString("i18n.billPay.DueDate") + ": " + this.presenter.getFormattedDateString(this.dueDate) + ")";
      this.penalty = account.lateFeesDue;
      //this.view.PayDueAmount.CalendarSendDate.date=kony.os.date(CommonUtilities.getConfiguration("frontendDateFormat"));
      this.view.PayDueAmount.CalendarSendDate.dateFormat = "dd/MM/yyyy";
      this.view.PayDueAmount.CalendarSendDate.date = kony.os.date("dd/MM/yyyy");
      this.view.PayDueAmount.CalendarSendDate.dateFormat = "MM/dd/yyyy";
      if (this.view.PayDueAmount.imgRadioPayDueAmount.src === "icon_radiobtn_active.png") {
        this.view.PayDueAmount.tbxAmount.text = account.currentAmountDue;
      }
      else {
        this.payDueAmountRadioButton("Due");
      }
    },
    /**
     * Function on click of Pay amount Button, Loan Pay Off
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    loanDataAfterConfirmation: function () {
      this.enableButtonConfirm();
    },
    /**
     * Navigate to Pay Off confirmation
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    navigateToConfirmation: function () {
      this.showConfirmationLoanPayOff();
      this.view.confirmation.lblValueAmount.text = this.view.LoanPayOff.tbxLoanPayOffAmount.text;
      this.view.confirmation.lblValueDate.text = this.view.LoanPayOff.tbxActualLoanEndDate.text;
      if (this.view.LoanPayOff.tbxPayoffPenality.text !== null && this.view.LoanPayOff.tbxPayoffPenality.text !== "") {
        this.view.confirmation.flxContainerPenaltyAmount.setVisibility(true);
        this.view.confirmation.lblValuePenaltyAmount.text = this.view.LoanPayOff.tbxPayoffPenality.text;
      }
      else {
        this.view.confirmation.flxContainerPenaltyAmount.setVisibility(false);
      }
      this.view.confirmation.lblValuePaymentDate.text = this.view.LoanPayOff.CalendarPyOn.date;
      if (this.view.LoanPayOff.tbxOptional.text !== null && this.view.LoanPayOff.tbxOptional.text !== "" && this.view.LoanPayOff.tbxOptional.text !== " ") {
        this.view.confirmation.flxContainerDescription.setVisibility(true);
        this.view.confirmation.lblValueDescription.text = this.view.LoanPayOff.tbxOptional.text;
      }
      else {
        this.view.confirmation.flxContainerDescription.setVisibility(false);
      }
      this.view.confirmation.lblValue.text = this.view.LoanPayOff.listbxFrom.selectedKeyValue[1];
      this.view.confirmation.lblValueTo.text = this.view.LoanPayOff.listbxTo.selectedKeyValue[1];
      this.view.forceLayout();
    },
    /**
     * Function to form data and confirm Pay Off
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    loanPayOffPayment: function () {
      CommonUtilities.showProgressBar(this.view);
      var data = {};
      data.isScheduled = "true";
      var tdate = kony.os.date("dd/mm/yyyy");
      if (this.getDateObject(this.view.confirmation.lblValuePaymentDate.text).getTime() <= this.getDateObject(tdate).getTime()) {
        data.isScheduled = "false";
      }
      data.fromAccountID = this.view.LoanPayOff.listbxFrom.selectedKey;
      data.toAccountID = this.view.LoanPayOff.listbxTo.selectedKey;
      data.fromAccount = this.view.confirmation.lblValue.text;
      data.toAccount = this.view.confirmation.lblValueTo.text;
      data.notes = this.view.confirmation.lblValueDescription.text;
      data.principalBalance = this.view.confirmation.lblValueAmount.text;
      data.actualLoanEndDate = this.view.confirmation.lblValueDate.text;
      data.payOffCharge = this.view.confirmation.lblValuePenaltyAmount.text;
      data.date = this.presenter.getBackendDate(this.view.confirmation.lblValuePaymentDate.text, CommonUtilities.getConfiguration("frontendDateFormat"));
      data.amount = this.fullPayOffAmount;
      data.penaltyFlag = "true";
      data.payoffFlag = "true";
      var action = "payCompleteDue";
      this.presenter.payLoanOff(data, action);
    },
    /**
     * Function to discard currency symbol
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    removeCurrency: function (amount) {

      if (amount === undefined || amount === null) return;
      if (amount[0] === '$') {
        amount = amount.slice(1);
      }
      var res = amount.split(",");
      var value = "";
      for (var i = 0; i < res.length; i++) {
        value = value + res[i];
      }
      return value;
    },
    /**
    * Callback for Pay Off Success
     * @member frmPayDueAmountController
     * @param {void} data
     * @param {String} response
     * @returns {void} - None
     * @throws {void} - None
     */
    successPayment: function (data, response) {
      this.showAcknowledgementLoanPayOff();
      this.view.flxMainAcknowledgment.setVisibility(true);
      if (data.notes !== null && data.notes !== "" && data.notes !== " ") {
        this.view.confirmDialog.flxContainerDescription.setVisibility(true);
      }
      else
        this.view.confirmDialog.flxContainerDescription.setVisibility(false);
      this.view.confirmDialog.flxPartial.setVisibility(false);
      this.presenter.fetchUpdatedAccountDetails(data.fromAccountID, "updateFromAccount");
      this.presenter.fetchUpdatedAccountDetails(data.toAccountID, "updateToAccount");
      if (data.isScheduled === "true") {
        this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.loan.transactionMsg")+" " + data.date;
        this.view.acknowledgment.flxBalance.setVisibility(false);
      } else {
        this.view.acknowledgment.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.loan.scheduleTransactionMsg");
        this.view.acknowledgment.flxBalance.setVisibility(true);
      }
      this.view.acknowledgment.lblRefrenceNumberValue.text = response;
      this.view.acknowledgment.lblBalance.text = response.balance;
      this.view.acknowledgment.lblAccType.text = response.accountName;
      this.view.confirmDialog.lblValue.text = data.fromAccount;
      this.view.confirmDialog.lblValueTo.text = data.toAccount;
      this.view.confirmDialog.lblValueDescription.text = data.notes;
      this.view.confirmDialog.lblValueAmount.text = data.principalBalance.slice(1);
      this.view.confirmDialog.lblValueDate.text = data.actualLoanEndDate;
      this.view.confirmDialog.lblValuePenaltyAmount.text = data.payOffCharge;
      this.view.confirmDialog.lblValuePayDate.text = data.date;
      if (CommonUtilities.getConfiguration("showLoanUpdateDisclaimer") === "true") {
        this.view.acknowledgment.lblTransactionMessage.setVisibility(true);
      }
      CommonUtilities.hideProgressBar(this.view);
      this.view.forceLayout();
    },
    /**
      * Data Map based on account type
       * @member frmPayDueAmountController
       * @param {object} accounts
       * @returns {void} - None
       * @throws {void} - None
       */
    createAccountSegmentsModel: function (accounts) {
      var accountTypeConfig = {
        'Savings': {
          skin: "sknFlx26D0CE",
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
        },
        'Checking': {
          skin: "sknFlx9060b7",
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance')
        },
        'CreditCard': {
          skin: "sknFlxF4BA22",
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
        },
        'Deposit': {
          skin: "sknFlx4a90e2Border1pxRound",
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance')
        },
        'Mortgage': {
          skin: 'sknFlx4a90e2Border1pxRound',
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
        },
        'Default': {
          skin: 'sknFlx26D0CE',
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        }
      };
      var balanceInDollars = function (amount) {
        return CommonUtilities.formatCurrencyWithCommas(amount);
      };
      return Object.keys(accountTypeConfig).map(function (type) {
        return accounts.filter(function (account) {
          return account.accountType === type;
        });
      }).map(function (accounts) {
        return accounts.map(function (account) {
          return {
            "flxLeft": {
              "skin": accountTypeConfig[account.accountType].skin
            },
            "lblLeft": " ",
            "lblAccountName": account.nickName || account.accountName,
            "lblBalance": balanceInDollars(account[accountTypeConfig[account.accountType].balanceKey]),
            "lblAvailableBalance": accountTypeConfig[account.accountType].balanceTitle
          };
        });
      }).reduce(function (p, e) {
        return p.concat(e);
      }, []);
    },
    /**
     * Function to populate Payment accounts
      * @member frmPayDueAmountController
      * @param {Object} accountModel
      * @returns {void} - None
      * @throws {void} - None
      */
    updateTransferAccountList: function (accountModel) {
      this.view.mypaymentAccounts.segMypaymentAccounts.widgetDataMap = {
        "lblLeft": "lblLeft",
        "lblAccountName": "lblAccountName",
        "flxLeft": "flxLeft",
        "lblBalance": "lblBalance",
        "lblAvailableBalance": "lblAvailableBalance"
      };
      this.view.mypaymentAccounts.segMypaymentAccounts.setData(this.createAccountSegmentsModel(accountModel));
      this.view.forceLayout();
    },
    /**
    * Update data for Pay Off when TO account selection is changed
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    setDataOnNewSelection: function () {
      this.presenter.fetchUpdatedAccountDetails(this.view.LoanPayOff.listbxTo.selectedKey, "newAccountSelection");
    },
    /**
     * Return Date object for timestamp date
      * @member frmPayDueAmountController
      * @param {Date} dateString
      * @returns {void} - None
      * @throws {void} - None
      */
    getDateObject: function (dateString) {
      var index = -1;
      index = dateString.indexOf("T");
      if (index != -1) {
        dateString = this.presenter.getFormattedDateString(dateString);
      }
      var dateparts = dateString.split("/");
      var date = new Date(parseInt(dateparts[2]), parseInt(dateparts[0]) - 1, parseInt(dateparts[1]));
      return date;
    },
    /**
     * Function to activate/deactivate Pay Due Amount button 
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    changeButtonState: function () {
      if (this.view.PayDueAmount.tbxAmount.text.trim() !== "") {
        this.enableButton(this.view.PayDueAmount.btnPayAmount);
      }
      else {
        this.disableButton(this.view.PayDueAmount.btnPayAmount);
      }
    },
    /**
      * Function to disable the button along with the disable button skin
      * @member frmPayDueAmountController
      * @param {Object} button widget Id
      * @returns {void} - None
      * @throws {void} - None
      */
    disableButton: function (button) {
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.setEnabled(false);
    },
    /**
      * Function to enable the button along with the enable button skin
      * @member frmPayDueAmountController
      * @param {Object} button widget Id
      * @returns {void} - None
      * @throws {void} - None
      */
    enableButton: function (button) {
      button.skin = "sknBtnNormalLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
      button.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.setEnabled(true);
    },
    /**
      * Function to perform action onKeyUp of PayDueAmount textbox
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    amountTextboxAction: function () {
      this.changeButtonState();
      CommonUtilities.validateAmountFieldKeyPress(this.view.PayDueAmount.tbxAmount);
    },
    /**
      * Function to show Logout flex
      * @member frmPayDueAmountController
      * @param {void} - None
      * @returns {void} - None
      * @throws {void} - None
      */
    showLogout: function () {
      var scopeObj = this;
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxContainer.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    },
    /**
     * Function to perform Logout, on confirmation
     * @member frmPayDueAmountController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} - None
     */
    performLogout: function () {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      var context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      this.view.flxLogout.left = "-100%";
    },
   /** 
     * Localized strings.
    */
    currencySymbol: kony.i18n.getLocalizedString("i18n.common.currencySymbol"),
  };
});