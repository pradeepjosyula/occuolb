define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxRadioPayDueAmount **/
    AS_FlexContainer_jb3bec1cb9da4fd881aa372cb37e3f4b: function AS_FlexContainer_jb3bec1cb9da4fd881aa372cb37e3f4b(eventobject) {
        var self = this;
        this.payDueAmountRadioButton("Due");
    },
    /** onClick defined for flxRadioPayOtherAmount **/
    AS_FlexContainer_a7df5e125bb64b27ad2145c10bf8b09f: function AS_FlexContainer_a7df5e125bb64b27ad2145c10bf8b09f(eventobject) {
        var self = this;
        this.payDueAmountRadioButton("Other");
    },
    /** onClick defined for btnConfirm **/
    AS_Button_f5d7c3ed4af04f9285d2926333ea33da: function AS_Button_f5d7c3ed4af04f9285d2926333ea33da(eventobject) {
        var self = this;
        this.loanDataAfterConfirmation();
    },
    /** onClick defined for flxTermsAndConditions **/
    AS_FlexContainer_a5f4c632edb7499b9d22b7620a4f8f6f: function AS_FlexContainer_a5f4c632edb7499b9d22b7620a4f8f6f(eventobject) {
        var self = this;
        this.showLoanPayOff();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_f1159265018c413fb008a253c33c686f: function AS_FlexContainer_f1159265018c413fb008a253c33c686f(eventobject) {
        var self = this;
        this.quitPopUpNo();
    },
    /** preShow defined for frmPayDueAmount **/
    AS_Form_g4f57f598ab34e358b6b3294048f52f5: function AS_Form_g4f57f598ab34e358b6b3294048f52f5(eventobject) {
        var self = this;
        this.frmPayDueAmountPreShow();
    },
    /** postShow defined for frmPayDueAmount **/
    AS_Form_cd94e734981b49cdbeb5ab4d34f4f525: function AS_Form_cd94e734981b49cdbeb5ab4d34f4f525(eventobject) {
        var self = this;
        this.postShowPayDueAmount();
    },
    /** onDeviceBack defined for frmPayDueAmount **/
    AS_Form_b0e7160a6b374e60be051b61112ea7d0: function AS_Form_b0e7160a6b374e60be051b61112ea7d0(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmPayDueAmount **/
    AS_Form_ced0930a179e4de8b1c410fe3ae6d697: function AS_Form_ced0930a179e4de8b1c410fe3ae6d697(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});