define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmCardManagement **/
    AS_Form_e6c0940ed634451a841fe1815961cbfa: function AS_Form_e6c0940ed634451a841fe1815961cbfa(eventobject) {
        var self = this;
        this.formPreShowFunction();
    },
    /** postShow defined for frmCardManagement **/
    AS_Form_cf48919e45714f268daa73ee7b928732: function AS_Form_cf48919e45714f268daa73ee7b928732(eventobject) {
        var self = this;
        this.PostShowfrmCardManagement();
    },
    /** onTouchEnd defined for frmCardManagement **/
    AS_Form_gd108bb6da8042b7b4ec54ad6ed88408: function AS_Form_gd108bb6da8042b7b4ec54ad6ed88408(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});