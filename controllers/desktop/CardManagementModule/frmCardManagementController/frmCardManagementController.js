define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants){
  return {
  
  /**
   * Globals for storing travel-notification,card-image, status-skin mappings.
   */
  notificationObject : {},
  cardImages: {},
  countries: {},
  states: {},
  cities:{},
  selectedCountry:{},
  statusSkinsLandingScreen: {},
  statusSkinsDetailsScreen: {},
shouldUpdateUI: function (viewModel) {
      return viewModel !== undefined && viewModel !== null;
  },
  willUpdateUI: function (cardManagementViewModel) {
    if(cardManagementViewModel.progressBar === true){
      CommonUtilities.showProgressBar(this.view);
    }else if (cardManagementViewModel.progressBar === false){
      CommonUtilities.hideProgressBar(this.view);
    }
    if(cardManagementViewModel.serverError){
      CommonUtilities.hideProgressBar(this.view);
      this.showServerError(cardManagementViewModel.serverError);
    }else{
      this.hideServerError();
    }
    if(cardManagementViewModel.serverDown){
      CommonUtilities.hideProgressBar(this.view);
      CommonUtilities.showServerDownScreen();
    }
    if(cardManagementViewModel.sideMenu) {
      this.updateHamburgerMenu(cardManagementViewModel.sideMenu);
    }
    if(cardManagementViewModel.topBar) {
      this.updateTopBar(cardManagementViewModel.topBar);
    }
    if(cardManagementViewModel.cards){
      this.showCards(cardManagementViewModel.cards);
    }   
	if (cardManagementViewModel.travelStatus) {
      this.showCardsStatus(cardManagementViewModel.travelStatus);
    }
    if(cardManagementViewModel.actionAcknowledgement){
      this.showCardOperationAcknowledgement(cardManagementViewModel.card, cardManagementViewModel.actionAcknowledgement);
    }
    if(cardManagementViewModel.securityQuestions){
      this.showSecurityQuestionsScreen(cardManagementViewModel.securityQuestions, cardManagementViewModel.card, cardManagementViewModel.action);
    }
    if(cardManagementViewModel.secureAccessCode){
      this.showSecureAccessCodeScreen(cardManagementViewModel.params, cardManagementViewModel.action);
    }
    if(cardManagementViewModel.incorrectSecureAccessCode){
      this.showIncorrectSecureAccessCodeFlex();
    }
    if(cardManagementViewModel.incorrectSecurityAnswers){
      this.showIncorrectSecurityAnswersFlex();
    }
    if(cardManagementViewModel.travelNotificationsList){
      this.showTravelNotifications(cardManagementViewModel.travelNotificationsList.TravelRequests);
    }
    if(cardManagementViewModel.notificationDeleted){
      this.deleteNotificationSuccess();
    }
    if(cardManagementViewModel.eligibleCards){
      this.showSelectCardScreen(cardManagementViewModel.eligibleCards);
    }
    if(cardManagementViewModel.AddNewTravelPlan){
      this.showAddNewTravelPlan(cardManagementViewModel);
    }
    if(cardManagementViewModel.notificationAcknowledgement){
      this.showTravelNotificationAcknowledgement(cardManagementViewModel.notificationAcknowledgement);
    }
    this.AdjustScreen();
  },

    
  /**
   * AdjustScreen - Method that sets the height of footer properly.
   * @member of {frmCardManagementController}
   * @param {} - NONE
   * @returns {VOID}
   * @throws {}
   */
  AdjustScreen: function() {
      var mainheight;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMain.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
          diff = diff - this.view.flxFooter.frame.height;
          if (diff > 0) 
              this.view.flxFooter.top = mainheight + diff + "dp";
          else
              this.view.flxFooter.top = mainheight + "dp";        
       } else {
          this.view.flxFooter.top = mainheight + "dp";
       }
      this.view.forceLayout();
  }, 

    /**
     * showServerError - Method to show error flex.
     * @member of {frmCardManagementController}
     * @param {String} - Error message to be displayed.
     * @returns {VOID}
     * @throws {}
     */
    showServerError: function(errorMsg){
      this.view.flxDowntimeWarning.setVisibility(true);
      this.view.rtxDowntimeWarning.text = errorMsg;
      this.AdjustScreen();
    },

    /**
     * hideServerError - Method to hide error flex.
     * @member of {frmCardManagementController}
     * @param {} - None.
     * @returns {VOID}
     * @throws {}
     */
    hideServerError: function(){
      this.view.flxDowntimeWarning.setVisibility(false);
    },

  /**
   * formPreShowFunction - Form lifecycle method.
   * @member of {frmCardManagementController}
   * @param {} - None.
   * @returns {VOID}
   * @throws {}
   */
formPreShowFunction:function(){
    this.initializeCards();
    this.initRightContainer();
    this.hideAllCardManagementViews();
    this.setLogoutEvent();
    this.setTravelNotificationActions();
    CommonUtilities.disableOldDaySelection(this.view.calFrom);
    CommonUtilities.disableOldDaySelection(this.view.calTo);
    CommonUtilities.disableButton(this.view.btnCardsContinue)
},
/**
   * setLogoutEvent - Method to set logout event.
   * @member of {frmCardManagementController}
   * @param {} - None.
   * @returns {VOID}
   * @throws {}
   */
 setLogoutEvent:function(){
    var scopeObj=this;
    this.view.customheader.headermenu.btnLogout.onClick = function(){  
    scopeObj.view.CustomPopupLogout.lblHeading.text=kony.i18n.getLocalizedString("i18n.common.logout");
    scopeObj.view.CustomPopupLogout.lblPopupMessage.text=kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
    var height=scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMain.frame.height + scopeObj.view.flxFooter.frame.height;
    scopeObj.view.flxLogout.height=height+"dp";
    scopeObj.view.flxLogout.left="0%";     
  } ;
  this.view.CustomPopupLogout.btnYes.onClick = function(){
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    var context = {
      "action": "Logout"
    };
    authModule.presentationController.doLogout(context);
    scopeObj.view.flxLogout.left = "-100%";      

  };
  this.view.CustomPopupLogout.btnNo.onClick = function(){ 
    scopeObj.view.flxLogout.left="-100%";
  };
  this.view.CustomPopupLogout.flxCross.onClick = function(){  
    scopeObj.view.flxLogout.left="-100%";
  };
},

setTravelNotificationActions:function(){
   this.showContactUsNavigation();   
},
/**
   * initializeCards - Method to initialize the globals.
   * @member of {frmCardManagementController}
   * @param {} - None.
   * @returns {VOID}
   * @throws {}
   */
  initializeCards: function(){
    this.initializeCardImages();
    this.initializeStatusSkins();
  },

    /**
     * initializeStatusSkins - Method to initialize the status skins in globals.
     * @member of {frmCardManagementController}
     * @param {} - None.
     * @returns {VOID}
     * @throws {}
     */
    initializeStatusSkins: function(){
      this.statusSkinsLandingScreen[OLBConstants.CARD_STATUS.Active] = OLBConstants.SKINS.CARDS_ACTIVE_STATUS_LANDING;
      this.statusSkinsLandingScreen[OLBConstants.CARD_STATUS.Locked] = OLBConstants.SKINS.CARDS_LOCKED_STATUS_LANDING;
      this.statusSkinsLandingScreen[OLBConstants.CARD_STATUS.ReportedLost] = OLBConstants.SKINS.CARDS_REPORTED_LOST_STATUS_LANDING;
      this.statusSkinsLandingScreen[OLBConstants.CARD_STATUS.ReplaceRequestSent] = OLBConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
      this.statusSkinsLandingScreen[OLBConstants.CARD_STATUS.CancelRequestSent] = OLBConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_LANDING;
      this.statusSkinsLandingScreen[OLBConstants.CARD_STATUS.Cancelled] = OLBConstants.SKINS.CARDS_CANCELLED_STATUS_LANDING;
      
      this.statusSkinsDetailsScreen[OLBConstants.CARD_STATUS.Active] = OLBConstants.SKINS.CARDS_ACTIVE_STATUS_DETAILS;
      this.statusSkinsDetailsScreen[OLBConstants.CARD_STATUS.Locked] = OLBConstants.SKINS.CARDS_LOCKED_STATUS_DETAILS;
      this.statusSkinsDetailsScreen[OLBConstants.CARD_STATUS.ReportedLost] = OLBConstants.SKINS.CARDS_REPORTED_LOST_STATUS_DETAILS;
      this.statusSkinsDetailsScreen[OLBConstants.CARD_STATUS.ReplaceRequestSent] = OLBConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_DETAILS;
      this.statusSkinsDetailsScreen[OLBConstants.CARD_STATUS.CancelRequestSent] = OLBConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_DETAILS;
      this.statusSkinsDetailsScreen[OLBConstants.CARD_STATUS.Cancelled] = OLBConstants.SKINS.CARDS_CANCELLED_STATUS_DETAILS;
    },

    /**
     * initializeCardImages - Method to initialize the card images in globals.
     * @member of {frmCardManagementController}
     * @param {} - None.
     * @returns {VOID}
     * @throws {}
     */
    initializeCardImages: function(){
      this.cardImages[OLBConstants.CARD_PRODUCT.PlatinumCredit] = OLBConstants.IMAGES.PLATINUM_CARD; 
      this.cardImages[OLBConstants.CARD_PRODUCT.GoldDebit] = OLBConstants.IMAGES.GOLDEN_CARD;
      this.cardImages[OLBConstants.CARD_PRODUCT.PremiumCredit] = OLBConstants.IMAGES.PREMIUM_CLUB_CREDIT; 
      this.cardImages[OLBConstants.CARD_PRODUCT.ShoppingCard] = OLBConstants.IMAGES.SHOPPING_CARD;
      this.cardImages[OLBConstants.CARD_PRODUCT.PetroCard] = OLBConstants.IMAGES.PETRO_CARD; 
      this.cardImages[OLBConstants.CARD_PRODUCT.FoodCard] = OLBConstants.IMAGES.EAZEE_FOOD_CARD;
    },

    /**
     * initRightContainer - Method to initialize the actions and tooltips for right side flex.
     * @member of {frmCardManagementController}
     * @param {} - None.
     * @returns {VOID}
     * @throws {}
     */
    initRightContainer: function() {
        this.view.lblActivateNewCard.text = kony.i18n.getLocalizedString("i18n.CardManagement.ContactUs");
        this.view.lblActivateNewCard.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.ContactUs");
        this.view.lblApplyForNewCard.text = kony.i18n.getLocalizedString("i18n.CardManagement.ApplyForNewCard");
        this.view.lblApplyForNewCard.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.ApplyForNewCard");
        var infoContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('InformationContentModule');
        this.view.flxActivateNewCard.onTouchEnd = infoContentModule.presentationController.showContactUsPage.bind(infoContentModule.presentationController);
    },

  /**
   * PostShowfrmCardManagement - Form lifecycle method.
   * @member of {frmCardManagementController}
   * @param {} - None.
   * @returns {VOID}
   * @throws {}
   */
PostShowfrmCardManagement:function(){
    this.AdjustScreen();
},
/**
   * getActionButton - Method to get a JSON for action button based on the action.
   * @member of {frmCardManagementController}
   * @param {Object, String} - card object and name of action (Lock/Unlock etc.,).
   * @returns {Object} - JSON with text and onclick function for the button.
   * @throws {}
   */
  getActionButton: function(card, action){
    return {
      'text': action,
      'toolTip': action,
      'onClick': this.getAction(card, action),
      'isVisible': true
    };
  },

    /**
     * getAction - Method that actually returns the action to the action.
     * @member of {frmCardManagementController}
     * @param {Object, String} - card object and name of action (Lock/Unlock etc.,).
     * @returns {function} - Action for the given name.
     * @throws {}
     */
  	getAction: function(card, action){
      switch(action){
        case OLBConstants.CARD_ACTION.Lock: { 
      		return this.lockCard.bind(this, card);
       	}
        case OLBConstants.CARD_ACTION.Unlock: { 
      		return this.unlockCard.bind(this, card);
       	}
        case OLBConstants.CARD_ACTION.Replace:{
          	return this.replaceCard.bind(this, card);
        }
        case OLBConstants.CARD_ACTION.Report_Lost:{
          	return this.reportLost.bind(this, card);
        }
        case OLBConstants.CARD_ACTION.Cancel:{
          	return this.cancelCard.bind(this, card);
        }
        case OLBConstants.CARD_ACTION.Change_Pin:{
          	return this.changePin.bind(this, card);
        }
      }
    },
  	/**
     * lockCard - Entry point for lock card flow.
     * @member of {frmCardManagementController}
     * @param {Object} - card object.
     * @returns {VOID}
     * @throws {}
     */
  	lockCard: function(card){
      this.showLockCardView();
      this.setCardDetails(card);
      this.showLockCardGuidelines(card);
      this.view.forceLayout();
      this.AdjustScreen();
    },
    /**
     * showLockCardView - Sets the UI for lock card flow.
     * @member of {frmCardManagementController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */
    showLockCardView: function(){
      this.hideAllCardManagementViews();
      this.hideAllCardManagementRightViews();
      this.view.flxCardVerification.setVisibility(true);
      this.view.CardLockVerificationStep.setVisibility(true);
      this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
      this.view.CardLockVerificationStep.flxDeactivateCard.setVisibility(true);
      this.view.flxTermsAndConditions.setVisibility(true);
      this.view.forceLayout();
    },
    /**
     * setCardDetails - Binds the card details.
     * @member of {frmCardManagementController}
     * @param {Object} - Card object.
     * @returns {VOID}
     * @throws {}
     */
    setCardDetails: function(card){
      var self = this;
      this.view.CardLockVerificationStep.cardDetails.lblCardName.text = card.productName;
      this.view.CardLockVerificationStep.cardDetails.lblCardStatus.text = card.cardStatus;
      this.view.CardLockVerificationStep.cardDetails.lblCardStatus.skin = self.statusSkinsDetailsScreen[card.cardStatus];
      this.view.CardLockVerificationStep.cardDetails.imgCard.src = this.getImageForCard(card.productName);
      this.view.CardLockVerificationStep.cardDetails.lblKey1.text = kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber")+":";
      this.view.CardLockVerificationStep.cardDetails.rtxValue1.text = card.cardNumber;
      this.view.CardLockVerificationStep.cardDetails.lblKey2.text = kony.i18n.getLocalizedString("i18n.CardManagement.validThrough")+":";
      this.view.CardLockVerificationStep.cardDetails.rtxValue2.text = card.validThrough;
      this.view.CardLockVerificationStep.cardDetails.lblKey3.text = card.cardType === OLBConstants.CARD_TYPE.Debit ? kony.i18n.getLocalizedString("i18n.CardManagement.WithdrawalLimit")+":" : kony.i18n.getLocalizedString("i18n.CardManagement.CreditLimit")+":";
      this.view.CardLockVerificationStep.cardDetails.rtxValue3.text = card.cardType === OLBConstants.CARD_TYPE.Debit ? card.dailyWithdrawalLimit : card.creditLimit;
      this.view.CardLockVerificationStep.cardDetails.flxDetailsRow4.setVisibility(card.cardType === OLBConstants.CARD_TYPE.Credit);
      this.view.CardLockVerificationStep.cardDetails.lblKey4.text = kony.i18n.getLocalizedString("i18n.CardManagement.AvailableCredit")+":";
      this.view.CardLockVerificationStep.cardDetails.rtxValue4.text = card.availableCredit;
    },
      
    /**
     * EnableTermsAndConditions - Method to enable terms and conditions on lock card.
     * @member of {frmCardManagementController}
     * @param {} -
     * @returns {VOID}
     * @throws {}
     */  
     EnableTermsAndConditionsForLockCards:function(){
      var self = this;
      this.view.CardLockVerificationStep.CardActivation.btnTermsAndConditions.onClick = function() {
        self.view.flxTermsAndConditionsPopUp.isVisible = true;
        if(CommonUtilities.isChecked(self.view.CardLockVerificationStep.CardActivation.imgChecbox)){
          CommonUtilities.setCheckboxState(true,self.view.imgTCContentsCheckbox);
        }else{
          CommonUtilities.setCheckboxState(false,self.view.imgTCContentsCheckbox);
        }
      };
      this.view.btnCancel.onClick = function() {
        self.view.flxTermsAndConditionsPopUp.isVisible = false;
      };
      this.view.flxClose.onClick = function() {
        self.view.flxTermsAndConditionsPopUp.isVisible = false;
      };
      this.view.flxTCContentsCheckbox.onClick = function() {
        CommonUtilities.toggleCheckBox(self.view.imgTCContentsCheckbox);
      }; 			
      this.view.btnSave.onClick = function() {
        if (CommonUtilities.isChecked(self.view.imgTCContentsCheckbox)) {
          CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
          CommonUtilities.setCheckboxState(true,self.view.CardLockVerificationStep.CardActivation.imgChecbox);
        } 
        else 
        {
          CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
          CommonUtilities.setCheckboxState(false,self.view.CardLockVerificationStep.CardActivation.imgChecbox);
        }
        self.view.flxTermsAndConditionsPopUp.isVisible = false;
      };  
    },  
    /**
     * showLockCardGuidelines - Shows the guidelines for Locking a card and sets flow actions.
     * @member of {frmCardManagementController}
     * @param {Object} - card object.
     * @returns {VOID}
     * @throws {}
     */
    showLockCardGuidelines: function(card) {
      var self = this;
      self.EnableTermsAndConditionsForLockCards();
      this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCard"));
      this.view.CardLockVerificationStep.CardActivation.lblWarning.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockingCard").replace('$CardType', card.cardType.toLowerCase()).replace('$CardNumber', card.cardNumber);
      this.view.CardLockVerificationStep.CardActivation.lblIns1.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline1");
      this.view.CardLockVerificationStep.CardActivation.lblIns2.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline2");
      this.view.CardLockVerificationStep.CardActivation.lblIns3.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline3");
      this.view.CardLockVerificationStep.CardActivation.lblIns4.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockedCardGuideline4");
      var buttonsJSON = {
        'btnConfirm': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
          'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
        },
        'btnModify': {
          'isVisible': false
        },
        'btnCancel': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
          'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel")
        },
      };
      this.alignConfirmButtons(buttonsJSON);
      CommonUtilities.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      CommonUtilities.setCheckboxState(false, this.view.CardLockVerificationStep.CardActivation.imgChecbox);
      this.view.CardLockVerificationStep.CardActivation.flxCheckbox.onTouchEnd = function(){
        CommonUtilities.toggleCheckBox(self.view.CardLockVerificationStep.CardActivation.imgChecbox);
        if(CommonUtilities.isChecked(self.view.CardLockVerificationStep.CardActivation.imgChecbox)){
        	CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
            CommonUtilities.setCheckboxState(true,self.view.imgTCContentsCheckbox);
        }else{
          	CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
            CommonUtilities.setCheckboxState(false,self.view.imgTCContentsCheckbox);
        }
      };
      var params = {
        'card': card
      };
	  this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function(){
        self.presenter.navigateToManageCards();
      };
      this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = self.showMFAScreen.bind(this, params, OLBConstants.CARD_ACTION.Lock);
    },
    /**
     * showMFAScreen - Shows the MFA Options available.
     * @member of {frmCardManagementController}
     * @param {Object, String} - card object and name of the action.
     * @returns {VOID}
     * @throws {}
     */
    showMFAScreen: function(params, action){
      var self = this;
      if(action === OLBConstants.CARD_ACTION.Lock){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardVerification"));        
      }else if(action === OLBConstants.CARD_ACTION.Change_Pin || action === OLBConstants.CARD_ACTION.Offline_Change_Pin){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinVerification"));        
      }else if(action === OLBConstants.CARD_ACTION.Unlock){
        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardVerification"));
      }else if(action === OLBConstants.CARD_ACTION.Report_Lost){
        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenVerification"));
      	this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardVerification");
      }else if(action === OLBConstants.CARD_ACTION.Replace){
        this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardVerification"));
      }
      this.hideServerError();
      this.view.flxTermsAndConditions.setVisibility(false);
      var selectedMFAOption = OLBConstants.MFA_OPTIONS.SECURE_ACCESS_CODE;
      self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadio.src = OLBConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
      self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadioOption2.src = OLBConstants.IMAGES.ICON_RADIOBTN;
      if(CommonUtilities.getConfiguration('isSecurityQuestionConfigured') === "true"){
        this.view.CardLockVerificationStep.flxUsernameVerificationOption2.setVisibility(true);
      }else{
        this.view.CardLockVerificationStep.flxUsernameVerificationOption2.setVisibility(false);
      }
      this.hideAllCardManagementRightViews();
      this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(true);
      CommonUtilities.enableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      this.view.CardLockVerificationStep.flxUsernameVerificationRadioOption1.onTouchEnd = function(){
        self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadio.src = OLBConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
        self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadioOption2.src = OLBConstants.IMAGES.ICON_RADIOBTN;
      	selectedMFAOption = OLBConstants.MFA_OPTIONS.SECURE_ACCESS_CODE;
      };
      this.view.CardLockVerificationStep.flxUsernameVerificationRadiobtnOption2.onTouchEnd = function(){
        self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadio.src = OLBConstants.IMAGES.ICON_RADIOBTN;
        self.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadioOption2.src = OLBConstants.IMAGES.ICON_RADIOBTN_ACTIVE;
      	selectedMFAOption = OLBConstants.MFA_OPTIONS.SECURITY_QUESTIONS;
      };
      var buttonsJSON = {
        'btnConfirm': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
          'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
        },
        'btnModify': {
          'isVisible': false
        },
        'btnCancel': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
          'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel")
        },
      };
      this.alignConfirmButtons(buttonsJSON);
      this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function(){
        self.presenter.navigateToManageCards();
      };
      this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function(){
        if(selectedMFAOption === OLBConstants.MFA_OPTIONS.SECURE_ACCESS_CODE){
          CommonUtilities.showProgressBar(self.view);
          self.presenter.sendSecureAccessCode(params, action);
        }else if(selectedMFAOption === OLBConstants.MFA_OPTIONS.SECURITY_QUESTIONS){
          CommonUtilities.showProgressBar(self.view);
          self.presenter.fetchSecurityQuestions(params, action);
        }
      };
      this.view.forceLayout();
      this.AdjustScreen();
    },
      
    /**
     * showSecureAccessCodeScreen - Shows the screen where user can enter secure access code and verify.
     * @member of {frmCardManagementController}
     * @param {Object, String} - card object and name of the action.
     * @returns {VOID}
     * @throws {}
     */
    showSecureAccessCodeScreen: function(params, action){
      var self = this;
      if(action === OLBConstants.CARD_ACTION.Lock){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardSecureAccessCode"));        
      }else if(action === OLBConstants.CARD_ACTION.Change_Pin || action === OLBConstants.CARD_ACTION.Offline_Change_Pin){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinSecureAccessCode"));        
      }else if(action === OLBConstants.CARD_ACTION.Unlock){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardSecureAccessCode"));        
      }else if(action === OLBConstants.CARD_ACTION.Report_Lost){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardSecureAccessCode"));        
      }else if(action === OLBConstants.CARD_ACTION.Replace){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardSecureAccessCode"));        
      }
      this.hideServerError();
      this.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(false);
      CommonUtilities.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(false);
      this.view.CardLockVerificationStep.flxVerifyBySecureAccessCode.setVisibility(true);
      this.view.CardLockVerificationStep.tbxCVV.text = "";
      this.view.CardLockVerificationStep.tbxCVV.secureTextEntry = true;
      this.view.CardLockVerificationStep.imgViewCVV.onTouchStart = this.toggleSecureAccessCodeMasking.bind(this, this.view.CardLockVerificationStep.tbxCVV);
      this.view.CardLockVerificationStep.tbxCVV.onKeyUp = function(){
        if(!self.isValidSecureAccessCode(self.view.CardLockVerificationStep.tbxCVV.text) || CommonUtilities.isCSRMode()){
          CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
        }else{
          CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
        }
      };
      var buttonsJSON = {
        'btnConfirm': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString("i18n.CardManagement.Verify"),
          'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Verify")
        },
        'btnModify': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString("i18n.CardManagement.ResendOTP"),
          'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.ResendOTP")
        },
        'btnCancel': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
          'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel")
        },
      };
      this.alignConfirmButtons(buttonsJSON);
      var resendOtpTimer;
      this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function(){
        clearTimeout(resendOtpTimer);
        self.showMFAScreen(params, action);
      };
      CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
      resendOtpTimer = setTimeout(
        function(){ 
          CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
        }, 5000);
      this.view.CardLockVerificationStep.confirmButtons.btnModify.onClick = function(){
        self.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(true);
        self.view.CardLockVerificationStep.lblWarningSecureAccessCode.text = kony.i18n.getLocalizedString("i18n.MFA.ResentOTPMessage");
        CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
        resendOtpTimer = setTimeout(
          function(){ 
            CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
          }, 5000);
      };
      if(CommonUtilities.isCSRMode()){
          this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
          this.view.CardLockVerificationStep.confirmButtons.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
          this.view.CardLockVerificationStep.confirmButtons.btnConfirm.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
      }
      else{
      this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function(){
        clearTimeout(resendOtpTimer);
        CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnModify);
        var enteredAccessCode = self.view.CardLockVerificationStep.tbxCVV.text;
        params.enteredAccessCode = enteredAccessCode;
        CommonUtilities.showProgressBar(self.view);
        self.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(false);
	    self.presenter.verifySecureAccessCode(params, action);
      };
      }
    },
    /**
     * showIncorrectSecureAccessCodeFlex: This function enables the incorrect OTP flex.
     * @member of {frmCardManagementController}
     * @param {} 
     * @return {VOID} - NONE 
     * @throws {}
     */  
    showIncorrectSecureAccessCodeFlex: function(){
      CommonUtilities.hideProgressBar(this.view);
      CommonUtilities.enableButton(this.view.CardLockVerificationStep.confirmButtons.btnModify);
      this.view.CardLockVerificationStep.lblWarningSecureAccessCode.setVisibility(true);
      this.view.CardLockVerificationStep.lblWarningSecureAccessCode.text = kony.i18n.getLocalizedString("i18n.MFA.EnteredSecureAccessCodeDoesNotMatch");
    },
    /**
     * showIncorrectSecureAnswersFlex: This function enables the incorrect security answers flex.
     * @member of {frmCardManagementController}
     * @param {} 
     * @return {VOID} - NONE 
     * @throws {}
     */  
    showIncorrectSecurityAnswersFlex: function(){
      CommonUtilities.hideProgressBar(this.view);
      this.view.CardLockVerificationStep.flxWarningMessage.setVisibility(true);
      this.view.CardLockVerificationStep.lblWarning.text = kony.i18n.getLocalizedString("i18n.MFA.EnteredSecurityQuestionsDoesNotMatch");
    },
    /**
     * toggleSecureAccessCodeMasking -This function shows the masked Secure Access Code on click of the eye icon
     * @member of {frmCardManagementController}
     * @param {Object} - The textbox widget of the Secure Access Code.  
     * @return {VOID} - NONE 
     * @throws {}
     */
    toggleSecureAccessCodeMasking: function(widgetId) {
       widgetId.secureTextEntry = !(widgetId.secureTextEntry);
    },
    
    /**
     * showSecurityQuestionsScreen - Shows the screen where user can enter secure access code and verify.
     * @member of {frmCardManagementController}
     * @param {Array, Object, String} - Security Questions array, params object and name of the action.
     * @returns {VOID}
     * @throws {}
     */
    showSecurityQuestionsScreen: function(securityQuestions, params, action){
      var self = this;
      if(action === OLBConstants.CARD_ACTION.Lock){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardSecurityQuestions"));        
      }else if(action === OLBConstants.CARD_ACTION.Change_Pin || action === OLBConstants.CARD_ACTION.Offline_Change_Pin){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinSecurityQuestions"));        
      }else if(action === OLBConstants.CARD_ACTION.Unlock){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardSecurityQuestions"));        
      }else if(action === OLBConstants.CARD_ACTION.Report_Lost){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardSecurityQuestions"));        
      }
      else if(action === OLBConstants.CARD_ACTION.Replace){
      	this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardSecurityQuestions"));        
      }
      this.view.CardLockVerificationStep.flxWarningMessage.setVisibility(false);
      CommonUtilities.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(false);
      this.view.CardLockVerificationStep.flxVerifyBySecureAccessCode.setVisibility(false);
      this.view.CardLockVerificationStep.flxVerifyBySecurityQuestions.setVisibility(true);
      this.view.CardLockVerificationStep.lblAnswerSecurityQuestion1.text = securityQuestions[0].Question;
      this.view.CardLockVerificationStep.lblAnswerSecurityQuestion2.text = securityQuestions[1].Question;
      this.view.CardLockVerificationStep.tbxAnswers1.text = "";
      this.view.CardLockVerificationStep.tbxAnswers2.text = "";
      this.view.CardLockVerificationStep.tbxAnswers1.onKeyUp = function(){
        if(self.view.CardLockVerificationStep.tbxAnswers1.text === "" || self.view.CardLockVerificationStep.tbxAnswers2.text === ""||CommonUtilities.isCSRMode()){
          CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
        }else{
          CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
        }
      };
      this.view.CardLockVerificationStep.tbxAnswers2.onKeyUp = function(){
        if(self.view.CardLockVerificationStep.tbxAnswers1.text === "" || self.view.CardLockVerificationStep.tbxAnswers2.text === ""||CommonUtilities.isCSRMode()){
          CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
        }else{
          CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
        }
      };
      var buttonsJSON = {
        'btnConfirm': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString("i18n.CardManagement.Verify"),
          'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Verify")
        },
        'btnModify': {
          'isVisible': false,
        },
        'btnCancel': {
          'isVisible': true,
          'text': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
          'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel")
        },
      };
      this.alignConfirmButtons(buttonsJSON);
      this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = this.showMFAScreen.bind(this, params, action);
	  if(CommonUtilities.isCSRMode()){
        this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
      }
	  else{
      this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function(){
        self.view.CardLockVerificationStep.flxWarningMessage.setVisibility(false);
        var questionAnswers = [];
        questionAnswers.push({
          'questionId': securityQuestions[0].SecurityQuestion_id,
          'customerAnswer': self.view.CardLockVerificationStep.tbxAnswers1.text
        }, {
          'questionId': securityQuestions[1].SecurityQuestion_id,
          'customerAnswer': self.view.CardLockVerificationStep.tbxAnswers2.text
        });
        params.questionAnswers = questionAnswers;
        CommonUtilities.showProgressBar(self.view);
	    self.presenter.verifySecurityQuestionAnswers(params, action);
      };
	  }
      CommonUtilities.hideProgressBar(self.view);
    },
    /**
     * showCardOperationAcknowledgement - Shows the acknowledgement screen based on the action.
     * @member of {frmCardManagementController}
     * @param {Object, String} - card object and name of the action.
     * @returns {VOID}
     * @throws {}
     */
    showCardOperationAcknowledgement: function(card, action){
      var self = this;
      CommonUtilities.hideProgressBar(self.view);
      this.hideAllCardManagementViews();
      this.view.ConfirmDialog.confirmButtons.setVisibility(false);
      this.view.flxAcknowledgment.setVisibility(true);
      this.view.btnRequestReplacement.setVisibility(false);
      switch(action){
        case OLBConstants.CARD_ACTION.Lock:{
      	  this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LockCardAcknowledgement"));        
          this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.LockCardLower");
          this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.AckMessage1");
      	  this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(true);
          this.view.Acknowledgement.lblUnlockCardMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.AckMessage2");
          break;
        }
        case OLBConstants.CARD_ACTION.Offline_Change_Pin :
        case OLBConstants.CARD_ACTION.Change_Pin:{
          this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangePinAcknowledgement"));        
          this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.ChangeCardPinLower");
          this.view.Acknowledgement.lblCardTransactionMessage.text = card.cardType === OLBConstants.CARD_TYPE.Debit ? kony.i18n.getLocalizedString("i18n.CardManagement.SuccessfulChangePinAckMessage"): kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulChangePinRequestAckMessage");
      	  this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
          break;
        }
        case OLBConstants.CARD_ACTION.Unlock:{
          this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardAcknowledgement"));        
          this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCardLower");
          this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.SuccessfulUnlockCardAckMessage");
      	  this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
          break;
        }
        case OLBConstants.CARD_ACTION.Report_Lost:{
          this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenCardAcknowledgement"));        
          this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolenLower");
          this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulRequestAckMessage");
      	  this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
          this.view.btnRequestReplacement.text = kony.i18n.getLocalizedString("i18n.CardManagement.RequestReplacement");
          this.view.btnRequestReplacement.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.RequestReplacement");
          this.view.btnRequestReplacement.setVisibility(true);
          this.view.btnRequestReplacement.onClick = function(){
          	self.replaceCard(card);
          };
          break;
        }
        case OLBConstants.CARD_ACTION.Replace:{
          this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardAcknowledgement"));        
          this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReplaceCardLower");
          this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString("i18n.CardManagement.SucessfulRequestAckMessage");
          this.view.Acknowledgement.lblUnlockCardMessage.setVisibility(false);
          break;
      }
    }
      this.view.Acknowledgement.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.CardManagement.Acknowledgement");
      this.view.ConfirmDialog.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.CardManagement.CardDetails")+":";
      this.view.ConfirmDialog.keyValueCardHolder.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.CardHolder")+":";
      this.view.ConfirmDialog.keyValueCardHolder.lblValue.text = card.cardHolder;
      this.view.ConfirmDialog.keyValueCardName.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber")+":";
      this.view.ConfirmDialog.keyValueCardName.lblValue.text = card.cardNumber;
      this.view.ConfirmDialog.keyValueValidThrough.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.ValidThrough")+":";
      this.view.ConfirmDialog.keyValueValidThrough.lblValue.text = card.validThrough;
      this.view.ConfirmDialog.keyValueServiceProvider.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.ServiceProvider")+":";
      this.view.ConfirmDialog.keyValueServiceProvider.lblValue.text = card.serviceProvider;
      if(card.cardType === OLBConstants.CARD_TYPE.Credit){
        this.view.ConfirmDialog.keyValueAvailableCredit.setVisibility(true);
        this.view.ConfirmDialog.keyValueCreditLimit.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.CreditLimit")+":";
        this.view.ConfirmDialog.keyValueCreditLimit.lblValue.text = card.creditLimit;
        this.view.ConfirmDialog.keyValueAvailableCredit.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.AvailableCredit")+":";
        this.view.ConfirmDialog.keyValueAvailableCredit.lblValue.text = card.availableCredit;
      }else if(card.cardType === OLBConstants.CARD_TYPE.Debit){
        this.view.ConfirmDialog.keyValueAvailableCredit.setVisibility(false);
        this.view.ConfirmDialog.keyValueCreditLimit.lblKey.text = kony.i18n.getLocalizedString("i18n.CardManagement.WithdrawalLimit")+":";
        this.view.ConfirmDialog.keyValueCreditLimit.lblValue.text = card.dailyWithdrawalLimit;
      }
      this.view.btnBackToCards.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.BackToCards");
      this.view.btnBackToCards.onClick = this.presenter.navigateToManageCards.bind(this.presenter);
      this.AdjustScreen();
    },
  	/**
      * replaceCard - Entry point for replace card flow.
      * @member of {frmCardManagementController}
      * @param {Object} - card object.
      * @returns {VOID}
      * @throws {}
      */
  	 replaceCard: function(card) {
      this.showCardReplacementView();
      this.setCardDetails(card);
      this.showCardReplacementGuildlines(card);
      this.view.forceLayout();
      this.AdjustScreen();
     },
	 /**
     * showCardReplacementView - Method to show replaced card details
     * @member of {frmCardManagementController}
     * @param {} 
     * @returns {VOID}
     * @throws {}
     */ 
    showCardReplacementView: function() {
    this.hideAllCardManagementViews();
    this.hideAllCardManagementRightViews();
    this.view.flxCardVerification.setVisibility(true);
    this.view.CardLockVerificationStep.setVisibility(true);
    this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
    this.view.CardLockVerificationStep.flxCardReplacement.setVisibility(true);
    this.view.CardLockVerificationStep.lblUpgrade.setVisibility(false);
    this.view.flxTermsAndConditions.setVisibility(true);
    this.view.forceLayout();
  },
  /**
     * showCardReplacementGuildlines - Method to show guidelines card replacement.
     * @member of {frmCardManagementController}
     * @param {Object} - card object 
     * @returns {VOID}
     * @throws {}
     */ 
    showCardReplacementGuildlines: function(card) {
      var self = this;
      this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.cardReplacement"));
      this.view.CardLockVerificationStep.WarningMessage.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline1");
      this.view.CardLockVerificationStep.WarningMessage.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline2");
      this.view.CardLockVerificationStep.WarningMessage.rtxWarningText3.text = kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline3");
      this.view.CardLockVerificationStep.WarningMessage.flxWarningText4.setVisibility(false);
      //TODO:The reason for replacement has to be implemented in 4.1
      this.view.CardLockVerificationStep.lblReason2.setVisibility(false);
      this.view.CardLockVerificationStep.lbxReason2.setVisibility(false);
      this.view.CardLockVerificationStep.lblUpgrade.setVisibility(false);
      this.view.CardLockVerificationStep.flxAddresslabel.setVisibility(true);
      this.view.CardLockVerificationStep.flxAddress.setVisibility(true);
      this.view.CardLockVerificationStep.tbxNoteOptional.text="";
      this.view.CardLockVerificationStep.tbxNoteOptional.maxTextLength = OLBConstants.NOTES_LENGTH;
      this.view.CardLockVerificationStep.lblAddress.text = kony.i18n.getLocalizedString("i18n.CardManagement.addressforcards");
	  this.view.CardLockVerificationStep.lblAddressCheckBox1.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.PrimaryAddress");
      this.view.CardLockVerificationStep.lblAddressCheckBox2.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.SecondaryAddress");
	  this.view.CardLockVerificationStep.lblAddressCheckBox3.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.SecondaryAddress");
	  this.view.CardLockVerificationStep.lblAddressCheckBox1.text = "R";
      this.view.CardLockVerificationStep.lblAddressCheckBox1.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_SELECTED;
      var addressObject = kony.mvc.MDAApplication.getSharedInstance().appContext.address;
      var addressArray = [];
      if(addressObject){
      addressObject.forEach(function(dataItem) {
          addressArray.push(dataItem.AddressLine1 + ", " + dataItem.AddressLine2 + ", " + dataItem.CityName + ", " + dataItem.CountryName + ", " + dataItem.ZipCode);
      });
    }
      if (addressArray[0]) {
          this.view.CardLockVerificationStep.rtxAddress1.setVisibility(true);
          this.view.CardLockVerificationStep.flxAddressCheckbox1.setVisibility(true);
          this.view.CardLockVerificationStep.rtxAddress1.text = addressArray[0];
      } else {
          this.view.CardLockVerificationStep.flxAddress.setVisibility(false);
          this.view.CardLockVerificationStep.flxAddresslabel.setVisibility(false);
          this.view.CardLockVerificationStep.rtxAddress1.setVisibility(false);
          this.view.CardLockVerificationStep.flxAddressCheckbox1.setVisibility(false);
      }
      if (addressArray[1]) {
          this.view.CardLockVerificationStep.rtxAddress2.setVisibility(true);
          this.view.CardLockVerificationStep.flxAddressCheckbox2.setVisibility(true);
          this.view.CardLockVerificationStep.rtxAddress2.text = addressArray[1];
      } else {
          this.view.CardLockVerificationStep.flxAddressCheckbox2.setVisibility(false);
          this.view.CardLockVerificationStep.rtxAddress2.setVisibility(false);
      }
      if (addressArray[2]) {
          this.view.CardLockVerificationStep.rtxAddress3.setVisibility(true);
          this.view.CardLockVerificationStep.flxAddressCheckbox3.setVisibility(true);
          this.view.CardLockVerificationStep.rtxAddress3.text = addressArray[2];
      } else {
          this.view.CardLockVerificationStep.flxAddressCheckbox3.setVisibility(false);
          this.view.CardLockVerificationStep.rtxAddress3.setVisibility(false);
      }
      var checkBoxArray = [];
      checkBoxArray.push({
          'flex1': this.view.CardLockVerificationStep.flxAddressCheckbox1,
          'flex2': this.view.CardLockVerificationStep.flxAddressCheckbox2,
          'flex3': this.view.CardLockVerificationStep.flxAddressCheckbox3
      });
      this.view.CardLockVerificationStep.flxAddressCheckbox1.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
      this.view.CardLockVerificationStep.flxAddressCheckbox2.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
      this.view.CardLockVerificationStep.flxAddressCheckbox3.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
      var buttonsJSON = {
          'btnConfirm': {
              'isVisible': true,
              'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
              'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
          },
          'btnModify': {
              'isVisible': false,
          },
          'btnCancel': {
              'isVisible': true,
			  'text': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
              'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
          },
      };
      this.alignConfirmButtons(buttonsJSON);
      this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = function() {
          self.presenter.navigateToManageCards();
      };
      CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function(){
        var params = {
          'card': card,
          'userName' : kony.mvc.MDAApplication.getSharedInstance().appContext.username,
          'CardAccountNumber' : card.maskedCardNumber,
          'CardAccountName' : card.productName,
          'AccountType' : OLBConstants.CARD_REQUEST_CODE.AccountType_Card,
          'RequestCode' : OLBConstants.CARD_REQUEST_CODE.Replacement,
          'Channel' : OLBConstants.Channel,
          'Address_id': self.getSelectedAddressId(addressObject, checkBoxArray),
          'AdditionalNotes' : self.view.CardLockVerificationStep.tbxNoteOptional.text
        };
        self.showMFAScreen.call(self, params, OLBConstants.CARD_ACTION.Replace);
      };
      this.AdjustScreen();
  },
  getSelectedAddressId : function(addressObject, checkBoxArray) {
    var i = 0;
    checkBoxArray.forEach(function(dataItem, index){
      if(dataItem.text === "R") i = index;
    });
    return addressObject[i].Address_id;
  },
  onRadioButtonSelection: function(radioButtons,selectedradioButton) {
     if(selectedradioButton.widgets()["0"].id==="lblAddressCheckBox1"){
      this.view.CardLockVerificationStep.lblAddressCheckBox1.text="R"; 
      this.view.CardLockVerificationStep.lblAddressCheckBox1.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_SELECTED;
      this.view.CardLockVerificationStep.lblAddressCheckBox2.text="L";
      this.view.CardLockVerificationStep.lblAddressCheckBox2.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
      this.view.CardLockVerificationStep.lblAddressCheckBox3.text="L"; 
      this.view.CardLockVerificationStep.lblAddressCheckBox3.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
     }
     if(selectedradioButton.widgets()["0"].id==="lblAddressCheckBox2"){
        this.view.CardLockVerificationStep.lblAddressCheckBox2.text="R"; 
        this.view.CardLockVerificationStep.lblAddressCheckBox2.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_SELECTED;
        this.view.CardLockVerificationStep.lblAddressCheckBox1.text="L"; 
        this.view.CardLockVerificationStep.lblAddressCheckBox1.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
        this.view.CardLockVerificationStep.lblAddressCheckBox3.text="L"; 
        this.view.CardLockVerificationStep.lblAddressCheckBox3.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
     }
     if(selectedradioButton.widgets()["0"].id==="lblAddressCheckBox3"){
         this.view.CardLockVerificationStep.lblAddressCheckBox3.text="R";  
         this.view.CardLockVerificationStep.lblAddressCheckBox3.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_SELECTED;
         this.view.CardLockVerificationStep.lblAddressCheckBox1.text="L"; 
         this.view.CardLockVerificationStep.lblAddressCheckBox1.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
         this.view.CardLockVerificationStep.lblAddressCheckBox2.text="L";
         this.view.CardLockVerificationStep.lblAddressCheckBox2.skin=OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
     }
},
  /**
   * unlockCard - Entry point for unlock card flow.
   * @member of {frmCardManagementController}
   * @param {Object} - card object.
   * @returns {VOID}
   * @throws {}
   */
  unlockCard: function(card){
    this.showUnlockCardGuidelines(card);
    this.view.forceLayout();
  },
    
  /**
   * showUnlockCardViewAndShowMFAScreen - Sets the UI for unlock card flow.
   * @member of {frmCardManagementController}
   * @param {} - NONE
   * @returns {VOID}
   * @throws {}
   */
  showUnlockCardViewAndShowMFAScreen: function(params, action) {
    this.hideAllCardManagementViews();
    this.hideAllCardManagementRightViews();
    this.view.flxActivateCard.setVisibility(false);
    this.view.flxCardVerification.setVisibility(true);
    this.view.CardLockVerificationStep.setVisibility(true);
    this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
    this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(true);
    this.view.flxTermsAndConditions.setVisibility(true);
    this.setCardDetails(params.card);
    this.showMFAScreen(params, OLBConstants.CARD_ACTION.Unlock);
    this.view.forceLayout();
  },
  showUnlockCardGuidelines: function(card) {
    var self = this;
    this.hideAllCardManagementViews();
    this.hideAllCardManagementRightViews();
    this.view.flxActivateCard.setVisibility(true);
    this.view.flxTermsAndConditions.setVisibility(true);
    self.EnableTermsAndConditionsForUnLockCards();
    this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.UnlockCard"));
    this.view.CardActivation.lblWarning.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockingCard").replace('$CardType', card.cardType.toLowerCase()).replace('$CardNumber', card.cardNumber);
    this.view.CardActivation.lblHeading2.text = kony.i18n.getLocalizedString("i18n.CardManagement.BenefitsOfUnlockingTheCard");
    this.view.CardActivation.lblIns1.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit1");
    this.view.CardActivation.lblIns2.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit2");
    this.view.CardActivation.lblIns3.text = kony.i18n.getLocalizedString("i18n.CardManagement.UnlockBenefit3");
    this.view.CardActivation.flxFour.setVisibility(false);
    CommonUtilities.disableButton(this.view.CardActivation.btnProceed);
    CommonUtilities.setCheckboxState(false, this.view.CardActivation.imgChecbox);
    this.view.CardActivation.flxCheckbox.onTouchEnd = function() {
      CommonUtilities.toggleCheckBox(self.view.CardActivation.imgChecbox);
      if (CommonUtilities.isChecked(self.view.CardActivation.imgChecbox)) {
        CommonUtilities.enableButton(self.view.CardActivation.btnProceed);
        CommonUtilities.setCheckboxState(true,self.view.imgTCContentsCheckbox);
      } else {
        CommonUtilities.disableButton(self.view.CardActivation.btnProceed);
        CommonUtilities.setCheckboxState(false,self.view.imgTCContentsCheckbox);
      }
    };
    this.view.CardActivation.btnCancel.onClick = this.presenter.navigateToManageCards.bind(this.presenter);
    this.view.CardActivation.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
    this.view.CardActivation.btnProceed.onClick = function() {
      var params = {
        'card': card
      };
      self.showUnlockCardViewAndShowMFAScreen.call(self, params);
    };
    this.view.forceLayout();
  },
  /**
   * EnableTermsAndConditionsForUnLockCards - Method to enable terms and conditions on unlock card.
   * @member of {frmCardManagementController}
   * @param {} -
   * @returns {VOID}
   * @throws {}
   */  
   EnableTermsAndConditionsForUnLockCards:function(){
    var self = this;
    this.view.CardActivation.btnTermsAndConditions.onClick = function() {
      self.view.flxTermsAndConditionsPopUp.isVisible = true;
      if(CommonUtilities.isChecked(self.view.CardActivation.imgChecbox)){
         CommonUtilities.setCheckboxState(true,self.view.imgTCContentsCheckbox);
      }else{
         CommonUtilities.setCheckboxState(false,self.view.imgTCContentsCheckbox);
      }
    };
    this.view.btnCancel.onClick = function() {
      self.view.flxTermsAndConditionsPopUp.isVisible = false;
    };
    this.view.flxClose.onClick = function() {
      self.view.flxTermsAndConditionsPopUp.isVisible = false;
    };
    this.view.flxTCContentsCheckbox.onClick = function() {
      CommonUtilities.toggleCheckBox(self.view.imgTCContentsCheckbox);
    }; 			
    this.view.btnSave.onClick = function() {
      if (CommonUtilities.isChecked(self.view.imgTCContentsCheckbox)) {
        CommonUtilities.setCheckboxState(true,self.view.CardActivation.imgChecbox);
        CommonUtilities.enableButton(self.view.CardActivation.btnProceed);
      } 
      else 
      {
        CommonUtilities.disableButton(self.view.CardActivation.btnProceed);
        CommonUtilities.setCheckboxState(false,self.view.CardActivation.imgChecbox);
      }
      self.view.flxTermsAndConditionsPopUp.isVisible = false;
    };  
  },    
  cancelCard: function(card){
    
  },
  reportLost: function(card){
    this.showReportLostView();
    this.setCardDetails(card);
    this.showReportLostGuidelines(card);
    this.view.forceLayout();
  },
  showReportLostView: function(){
    this.hideAllCardManagementViews();
    this.hideAllCardManagementRightViews();
    this.view.flxCardVerification.setVisibility(true);
    this.view.CardLockVerificationStep.setVisibility(true);
    this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
    this.view.CardLockVerificationStep.flxCardReplacement.setVisibility(true);
    this.view.flxTermsAndConditions.setVisibility(true);
    this.view.forceLayout();
  },
  showReportLostGuidelines: function(card){
    var self = this;
    this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.LostOrStolen"));
    this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolen");
    this.view.CardLockVerificationStep.WarningMessage.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline1");
    this.view.CardLockVerificationStep.WarningMessage.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline2");
    this.view.CardLockVerificationStep.WarningMessage.rtxWarningText3.text = kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline3");
    this.view.CardLockVerificationStep.WarningMessage.flxWarningText4.setVisibility(false);
    this.view.CardLockVerificationStep.lblReason2.setVisibility(true);
    this.view.CardLockVerificationStep.lblReason2.text = kony.i18n.getLocalizedString("i18n.CardManagement.PleaseEnterTheReasonMessage");
    this.view.CardLockVerificationStep.lbxReason2.setVisibility(true);
    var reasonsMasterData = [];
    reasonsMasterData.push([OLBConstants.CARD_REPORTLOST_REASON.LOST, OLBConstants.CARD_REPORTLOST_REASON.LOST], [OLBConstants.CARD_REPORTLOST_REASON.STOLEN, OLBConstants.CARD_REPORTLOST_REASON.STOLEN]);
    this.view.CardLockVerificationStep.lbxReason2.masterData = reasonsMasterData;
    this.view.CardLockVerificationStep.lbxReason2.selectedKey = OLBConstants.CARD_REPORTLOST_REASON.LOST;
    this.view.CardLockVerificationStep.tbxNoteOptional.text = "";
    this.view.CardLockVerificationStep.tbxNoteOptional.maxTextLength = OLBConstants.NOTES_LENGTH;
    this.view.CardLockVerificationStep.lblUpgrade.setVisibility(false);
    this.view.CardLockVerificationStep.flxAddresslabel.setVisibility(false);
    this.view.CardLockVerificationStep.flxAddress.setVisibility(false);
    var buttonsJSON = {
      'btnConfirm': {
        'isVisible': true,
        'text': kony.i18n.getLocalizedString('i18n.common.proceed'),
        'toolTip': kony.i18n.getLocalizedString('i18n.common.proceed')
      },
      'btnModify': {
        'isVisible': false
      },
      'btnCancel': {
        'isVisible': true,
        'text': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
        'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel")
      },
    };
    this.alignConfirmButtons(buttonsJSON);
    this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = this.presenter.navigateToManageCards.bind(this.presenter);
    this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function(){
      var params = {
        'card': card,
        'Reason': self.view.CardLockVerificationStep.lbxReason2.selectedKey,
        'notes': self.view.CardLockVerificationStep.tbxNoteOptional.text
      };
      self.showMFAScreen.call(self, params, OLBConstants.CARD_ACTION.Report_Lost);
    };
  },
  changePin: function(card){
    this.showChangePinView();
    this.setCardDetails(card);
    if(card.cardType === OLBConstants.CARD_TYPE.Credit){
      this.startOfflineChangePinFlow(card);
    }else if(card.cardType === OLBConstants.CARD_TYPE.Debit){
      this.startOnlineChangePinFlow(card);
    }
    this.view.forceLayout();
    this.AdjustScreen();
  },

  showChangePinView: function(){
    this.hideAllCardManagementViews();
    this.view.flxCardVerification.setVisibility(true);
    this.view.CardLockVerificationStep.setVisibility(true);
    this.view.CardLockVerificationStep.flxLeft.setVisibility(true);
    this.view.flxTermsAndConditions.setVisibility(true);
  },
  
  startOnlineChangePinFlow: function(card){
    var self = this;
    this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangeCardPin"));
    this.hideAllCardManagementRightViews();
    this.view.CardLockVerificationStep.flxConfirmPIN.setVisibility(true);
    this.view.CardLockVerificationStep.warning.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.CardManagement.OnlineChangePinGuideline1");
    this.view.CardLockVerificationStep.warning.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.CardManagement.OnlineChangePinGuideline2");
    this.view.CardLockVerificationStep.warning.flxWarningText3.setVisibility(false);
    this.view.CardLockVerificationStep.warning.flxWarningText4.setVisibility(false); 
    this.view.CardLockVerificationStep.lblReason.setVisibility(false);
    this.view.CardLockVerificationStep.lbxReason.setVisibility(false);
    this.view.CardLockVerificationStep.tbxCurrentPIN.text = "";
    this.view.CardLockVerificationStep.tbxNewPIN.text = "";
    this.view.CardLockVerificationStep.imgNewPIN.setVisibility(false);
    this.view.CardLockVerificationStep.tbxConfirmPIN.text = "";
    this.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(false);
    this.view.CardLockVerificationStep.tbxNote.text = "";
    this.view.CardLockVerificationStep.tbxNote.maxTextLength = OLBConstants.NOTES_LENGTH;
    this.view.CardLockVerificationStep.tbxConfirmPIN.secureTextEntry = false;
    CommonUtilities.disableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);          
    this.view.CardLockVerificationStep.tbxCurrentPIN.onKeyUp = function() {
      var enteredPin = self.view.CardLockVerificationStep.tbxCurrentPIN.text;
      if(self.isValidPin(enteredPin) && self.isValidPin(self.view.CardLockVerificationStep.tbxNewPIN.text) && self.view.CardLockVerificationStep.tbxNewPIN.text === self.view.CardLockVerificationStep.tbxConfirmPIN.text) {
        CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      } else{
        CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      }
    };
    this.view.CardLockVerificationStep.tbxNewPIN.onKeyUp = function() {
      var enteredPin = self.view.CardLockVerificationStep.tbxNewPIN.text;
      if(self.isValidPin(enteredPin) && self.isValidPin(self.view.CardLockVerificationStep.tbxCurrentPIN.text) && enteredPin === self.view.CardLockVerificationStep.tbxConfirmPIN.text) {
        CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      } else{
        CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      }
      if(self.isValidPin(enteredPin) && enteredPin === self.view.CardLockVerificationStep.tbxConfirmPIN.text){
        self.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(true);
        self.view.CardLockVerificationStep.imgConfirmPIN.src = 'success_green.png';
        self.view.forceLayout();
      }else{
        self.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(false);
      }
      if(self.isValidPin(enteredPin)) {
        self.view.CardLockVerificationStep.imgNewPIN.setVisibility(true);
        self.view.CardLockVerificationStep.imgNewPIN.src = 'success_green.png';
        self.view.forceLayout();
      } else{
        self.view.CardLockVerificationStep.imgNewPIN.setVisibility(false);
      }
    };
    this.view.CardLockVerificationStep.tbxConfirmPIN.onKeyUp = function() {
      var enteredPin = self.view.CardLockVerificationStep.tbxConfirmPIN.text;
      if(self.isValidPin(enteredPin) && self.isValidPin(self.view.CardLockVerificationStep.tbxCurrentPIN.text) && enteredPin === self.view.CardLockVerificationStep.tbxNewPIN.text) {
        CommonUtilities.enableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      } else{
        CommonUtilities.disableButton(self.view.CardLockVerificationStep.confirmButtons.btnConfirm);
      }
      if(self.isValidPin(enteredPin) && enteredPin === self.view.CardLockVerificationStep.tbxNewPIN.text) {
        self.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(true);
        self.view.CardLockVerificationStep.imgConfirmPIN.src = 'success_green.png';
        self.view.forceLayout();
      } else{
        self.view.CardLockVerificationStep.imgConfirmPIN.setVisibility(false);
      }
    };
    var buttonsJSON = {
      'btnConfirm': {
        'isVisible': true,
        'text': kony.i18n.getLocalizedString("i18n.common.proceed"),
        'toolTip': kony.i18n.getLocalizedString("i18n.common.proceed")
      },
      'btnModify': {
        'isVisible': false,
      },
      'btnCancel': {
        'isVisible': true,
        'text': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
        'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel")
      },
    };
    this.alignConfirmButtons(buttonsJSON);
    this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = this.presenter.navigateToManageCards.bind(this.presenter);
    this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function(){
      var params = {
        'card': card,
        'reason': self.view.CardLockVerificationStep.lbxReasonPinChange.selectedKey,
        'notes': self.view.CardLockVerificationStep.tbxNote.text,
      };
      self.showMFAScreen(params, OLBConstants.CARD_ACTION.Change_Pin);
    };
    this.view.forceLayout();
    this.AdjustScreen();
  }, 
  isValidPin: function(pin) {
    var regex = new RegExp('^[0-9]{6,6}$');
    if(regex.test(pin)){
        for (var i=1; i<pin.length; i++){
            if(Number(pin[i]) - 1 !== Number(pin[i-1])){
              return true;
            }
        }
    }
    return false;
  },
  startOfflineChangePinFlow: function(card){
    var self = this;
    var selectedOption = OLBConstants.CHANGE_PIN_OFFLINE_OPTION.EMAIL;
    this.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString("i18n.CardManagement.ChangeCardPin"));
    this.hideAllCardManagementRightViews();
    this.view.CardLockVerificationStep.flxChangeCardPin.setVisibility(true);
    this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.rtxWarningText1.text = kony.i18n.getLocalizedString("i18n.CardManagement.OfflineChangePinGuideline1");
    this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.rtxWarningText2.text = kony.i18n.getLocalizedString("i18n.CardManagement.OfflineChangePinGuideline2");
    this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.flxWarningText3.setVisibility(false);
    this.view.CardLockVerificationStep.Copywarning0b8a8390f76a040.flxWarningText4.setVisibility(false);
    var reasonsMasterData = [];
    reasonsMasterData.push([OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED, OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED]);
    reasonsMasterData.push([OLBConstants.CARD_CHANGE_PIN_REASON.FORGOT_PIN, OLBConstants.CARD_CHANGE_PIN_REASON.FORGOT_PIN]);
    this.view.CardLockVerificationStep.lbxReasonPinChange.masterData = reasonsMasterData;
    this.view.CardLockVerificationStep.lbxReasonPinChange.selectedKey = OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED;
    this.view.CardLockVerificationStep.tbxNotePinChange.text = "";
    this.view.CardLockVerificationStep.tbxNotePinChange.maxTextLength = OLBConstants.NOTES_LENGTH;
    this.view.CardLockVerificationStep.lblOption1.text = kony.i18n.getLocalizedString("i18n.CardManagement.EmailID");
    this.view.CardLockVerificationStep.lblOption2.text = kony.i18n.getLocalizedString("i18n.CardManagement.PhoneNo");
    this.view.CardLockVerificationStep.lblOption3.text = kony.i18n.getLocalizedString("i18n.CardManagement.PostalAddress");
    this.view.CardLockVerificationStep.lblCheckBox1.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_SELECTED;
    this.view.CardLockVerificationStep.lblCheckBox1.text = "R";
    this.view.CardLockVerificationStep.lblCheckBox2.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
    this.view.CardLockVerificationStep.lblCheckBox2.text = "L";
    this.view.CardLockVerificationStep.lblCheckBox3.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
    this.view.CardLockVerificationStep.lblCheckBox3.text = "L";
    this.view.CardLockVerificationStep.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredEmailID") + " " + self.getPrimaryContact(kony.mvc.MDAApplication.getSharedInstance().appContext.emailids);
    this.view.CardLockVerificationStep.flxCheckBox1.onTouchEnd = function(){
      self.view.CardLockVerificationStep.lblCheckBox1.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_SELECTED;
      self.view.CardLockVerificationStep.lblCheckBox1.text = "R";
      self.view.CardLockVerificationStep.lblCheckBox2.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
      self.view.CardLockVerificationStep.lblCheckBox2.text = "L";
      self.view.CardLockVerificationStep.lblCheckBox3.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
      self.view.CardLockVerificationStep.lblCheckBox3.text = "L";
      selectedOption = OLBConstants.CHANGE_PIN_OFFLINE_OPTION.EMAIL;
      self.view.CardLockVerificationStep.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredEmailID") + " " + self.getPrimaryContact(kony.mvc.MDAApplication.getSharedInstance().appContext.emailids);
    };
    this.view.CardLockVerificationStep.flxCheckBox2.onTouchEnd = function(){
      self.view.CardLockVerificationStep.lblCheckBox1.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
      self.view.CardLockVerificationStep.lblCheckBox1.text = "L";
      self.view.CardLockVerificationStep.lblCheckBox2.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_SELECTED;
      self.view.CardLockVerificationStep.lblCheckBox2.text = "R";
      self.view.CardLockVerificationStep.lblCheckBox3.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
      self.view.CardLockVerificationStep.lblCheckBox3.text = "L";
      selectedOption = OLBConstants.CHANGE_PIN_OFFLINE_OPTION.PHONE;
      self.view.CardLockVerificationStep.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredPhoneNo") + " " + self.getPrimaryContact(kony.mvc.MDAApplication.getSharedInstance().appContext.contactNumbers);
    };
    this.view.CardLockVerificationStep.flxCheckBox3.onTouchEnd = function(){
      self.view.CardLockVerificationStep.lblCheckBox1.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
      self.view.CardLockVerificationStep.lblCheckBox1.text = "L";
      self.view.CardLockVerificationStep.lblCheckBox2.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
      self.view.CardLockVerificationStep.lblCheckBox2.text = "L";
      self.view.CardLockVerificationStep.lblCheckBox3.skin = OLBConstants.SKINS.CARDS_RADIOBTN_LABEL_SELECTED;
      self.view.CardLockVerificationStep.lblCheckBox3.text = "R";
      selectedOption = OLBConstants.CHANGE_PIN_OFFLINE_OPTION.POSTAL_ADDRESS;
      self.view.CardLockVerificationStep.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredAddress") + " " + card.billingAddress;
    };
    var buttonsJSON = {
      'btnConfirm': {
        'isVisible': true,
        'text': kony.i18n.getLocalizedString("i18n.common.proceed"),
        'toolTip': kony.i18n.getLocalizedString("i18n.common.proceed")
      },
      'btnModify': {
        'isVisible': false,
      },
      'btnCancel': {
        'isVisible': true,
        'text': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel"),
        'toolTip': kony.i18n.getLocalizedString("i18n.CardManagement.Cancel")
      },
    };
    this.alignConfirmButtons(buttonsJSON);
    CommonUtilities.enableButton(this.view.CardLockVerificationStep.confirmButtons.btnConfirm);
    this.view.CardLockVerificationStep.confirmButtons.btnCancel.onClick = this.presenter.navigateToManageCards.bind(this.presenter);
    this.view.CardLockVerificationStep.confirmButtons.btnConfirm.onClick = function(){
      var params = {
        'card': card,
        'CardAccountNumber' : card.maskedCardNumber,
        'CardAccountName' : card.productName,
        'RequestReason': self.view.CardLockVerificationStep.lbxReasonPinChange.selectedKey,
        'AdditionalNotes': self.view.CardLockVerificationStep.tbxNotePinChange.text,
        'AccountType' : OLBConstants.CARD_REQUEST_CODE.AccountType_Card,
        'RequestCode' : OLBConstants.CARD_REQUEST_CODE.NewPin,
        'Channel' : OLBConstants.Channel,
        'userName' : kony.mvc.MDAApplication.getSharedInstance().appContext.username
      };
      if(selectedOption === OLBConstants.CHANGE_PIN_OFFLINE_OPTION.POSTAL_ADDRESS) {
        params['Address_id'] = self.getSelectedAddressId();
      } else if(selectedOption === OLBConstants.CHANGE_PIN_OFFLINE_OPTION.PHONE) {
        var contactNumbers = kony.mvc.MDAApplication.getSharedInstance().appContext.contactNumbers;
        params['communication_id'] = contactNumbers.filter(function(item){return item.isPrimary === "true"})[0].id;
      } else {
        var emailids = kony.mvc.MDAApplication.getSharedInstance().appContext.emailids
        params['communication_id'] = emailids.filter(function(item){return item.isPrimary === "true"})[0].id;
      }
      self.showMFAScreen(params, OLBConstants.CARD_ACTION.Offline_Change_Pin);
    };
    this.view.forceLayout();
    this.AdjustScreen();
  },
    
  /**
   * getImageForCard - Method to get the image for a given card product.
   * @member of {frmCardManagementController}
   * @param {String} - card product name.
   * @returns {String} - Name of image file.
   * @throws {}
   */
  getImageForCard: function(cardProductName){
    if(cardProductName)
      return this.cardImages[cardProductName];
    return OLBConstants.IMAGES.GOLDEN_CARD;
  },
    
  /**
   * getPrimaryContact: Returns the contact for which the isPrimary flag is true. Expects only one of the contacts to be primary. If there are more than one, returns the last.
   * @member of {frmCardManagementController}
   * @param {Array} - Array of contacts.  
   * @return {}
   * @throws {}
   */
getPrimaryContact: function(contacts) {
      var primaryContact = "";
      contacts.forEach(function(item) {
          if (item.isPrimary === "true") {
              primaryContact = item.Value;
          }
      });
      return primaryContact;
  },
  /**
   * showCardsNotAvailableScreen - Method to show no cards screen.
   * @member of {frmCardManagementController}
   * @param {} - 
   * @returns {VOID}
   * @throws {}
   */  
 showCardsNotAvailableScreen:function(){
     this.view.flxMyCardsView.setVisibility(true);
     this.view.myCards.segMyCards.setVisibility(false);
     this.view.myCards.flxNoCardsError.setVisibility(true);
     this.view.myCards.flxApplyForCards.setVisibility(true);
     this.view.myCards.lblNoCardsError.text = kony.i18n.getLocalizedString('i18n.CardsManagement.NocardsError');
     this.view.myCards.btnApplyForCard.onClick = function(){
     var naoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
         naoModule.presentationController.showNewAccountOpening();
     };
   }, 
  /**
   * setCardsData - Method that binds the cards data to the segment.
   * @member of {frmCardManagementController}
   * @param {Array} - Array of JSON objects of cards.
   * @returns {VOID}
   * @throws {}
   */
setCardsData : function(cards){
      if(cards.length <= 0){
         this.showCardsNotAvailableScreen();
      }
      else{
  var self=this;
  var dataMap={
    "btnAction1": "btnAction1",
    "btnAction2": "btnAction2",
    "btnAction3": "btnAction3",
    "btnAction4": "btnAction4",
    "btnAction5": "btnAction5",
    "btnAction6": "btnAction6",
    "btnAction7": "btnAction7",
    "btnAction8": "btnAction8",
    "flxActions": "flxActions",
    "flxBlankSpace1": "flxBlankSpace1",
    "flxBlankSpace2": "flxBlankSpace2",
    "flxCardDetails": "flxCardDetails",
    "flxCardHeader": "flxCardHeader",
    "flxCardImageAndCollapse": "flxCardImageAndCollapse",
    "lblCardsSeperator": "lblCardsSeperator",
    "flxCollapse": "flxCollapse",
    "flxDetailsRow1": "flxDetailsRow1",
    "flxDetailsRow10": "flxDetailsRow10",
    "flxDetailsRow2": "flxDetailsRow2",
    "flxDetailsRow3": "flxDetailsRow3",
    "flxDetailsRow4": "flxDetailsRow4",
    "flxDetailsRow5": "flxDetailsRow5",
    "flxDetailsRow6": "flxDetailsRow6",
    "flxDetailsRow7": "flxDetailsRow7",
    "flxDetailsRow8": "flxDetailsRow8",
    "flxDetailsRow9": "flxDetailsRow9",
    "flxMyCards": "flxMyCards",
    "flxMyCardsExpanded": "flxMyCardsExpanded",
    "flxRowIndicatorColor": "flxRowIndicatorColor",
          "lblIdentifier": "lblIdentifier",
          "lblSeparator1": "lblSeparator1",
          "lblSeparator2": "lblSeparator2",
    "lblSeperator": "lblSeperator",
    "imgCard": "imgCard",
    "imgCollapse": "imgCollapse",
    "lblCardHeader": "lblCardHeader",
    "lblCardStatus": "lblCardStatus",
      "lblTravelNotificationEnabled": "lblTravelNotificationEnabled",
    "lblKey1": "lblKey1",
    "lblKey10": "lblKey10",
    "lblKey2": "lblKey2",
    "lblKey3": "lblKey3",
    "lblKey4": "lblKey4",
    "lblKey5": "lblKey5",
    "lblKey6": "lblKey6",
    "lblKey7": "lblKey7",
    "lblKey8": "lblKey8",
    "lblKey9": "lblKey9",
    "rtxValue1": "rtxValue1",
    "rtxValue10": "rtxValue10",
    "rtxValue2": "rtxValue2",
    "rtxValue3": "rtxValue3",
    "rtxValue4": "rtxValue4",
    "rtxValue5": "rtxValue5",
    "rtxValue6": "rtxValue6",
    "rtxValue7": "rtxValue7",
    "rtxValue8": "rtxValue8",
    "rtxValue9": "rtxValue9",
    "segMyCardsExpanded": "segMyCardsExpanded"
  };
      var cardsSegmentData = [];
      var card = {};
    var travelStatusData = cards.status;
    for (var i = 0; i < cards.data.length; i++) {
      var dataItem = cards.data[i];
        card = {
            "lblCardsSeperator": {
              "text":".",
              "height":"105px"
            },
            "flxCollapse": {
              "onClick": self.changeRowTemplate
            },
            "flxRowIndicatorColor": {
              "height":"190Px",
              "skin":"sknFlxF4BA22"
            },
       "lblSeperator": ".",
            "lblSeparator1": ".",
            "lblSeparator2": ".",
        "imgCard": {
              "src": self.getImageForCard(dataItem.productName),
            },
         "imgCollapse": OLBConstants.IMAGES.ARRAOW_DOWN,
            "lblCardHeader": dataItem.productName,
            "lblCardStatus": {
              "text": dataItem.cardStatus,
              "skin": self.statusSkinsLandingScreen[dataItem.cardStatus]
            },
       "template": "flxMyCardsCollapsed",
            "flxDetailsRow1": {"isVisible":true},
            "flxDetailsRow2": {"isVisible":true},
            "flxDetailsRow3": {"isVisible":true},
            "flxDetailsRow4": {"isVisible":true},
            "flxDetailsRow5": {"isVisible":true},
            "flxDetailsRow6": {"isVisible":true},
            "flxDetailsRow7": {"isVisible":true},
            "flxDetailsRow8": {"isVisible":dataItem.secondaryCardHolder ? true: false},
            "flxDetailsRow9": {"isVisible":false},
            "lblKey1": kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber")+":",
       "lblKey9": kony.i18n.getLocalizedString("i18n.CardManagement.productName")+":",
       "lblKey2": kony.i18n.getLocalizedString("i18n.CardManagement.validThrough")+":",
       "lblKey3": dataItem.cardType === OLBConstants.CARD_TYPE.Debit ? kony.i18n.getLocalizedString("i18n.CardManagement.DailyWithdrawalLimit")+":": kony.i18n.getLocalizedString("i18n.CardManagement.CreditLimit")+":",
       "lblKey4": dataItem.cardType === OLBConstants.CARD_TYPE.Debit ? kony.i18n.getLocalizedString("i18n.CardManagement.AccountName")+":": kony.i18n.getLocalizedString("i18n.CardManagement.AvailableCredit")+":",
       "lblKey5": dataItem.cardType === OLBConstants.CARD_TYPE.Debit ? kony.i18n.getLocalizedString("i18n.CardManagement.AccountNumber")+":": kony.i18n.getLocalizedString("i18n.CardManagement.BillingAddress")+":",
       "lblKey6": kony.i18n.getLocalizedString("i18n.CardManagement.ServiceProvider")+":",
       "lblKey7": kony.i18n.getLocalizedString("i18n.CardManagement.CardHolder")+":",
       "lblKey8": kony.i18n.getLocalizedString("i18n.CardManagement.SecondaryHolder")+":",
       "rtxValue1": dataItem.cardNumber,
       "rtxValue9": dataItem.productName,
       "rtxValue2": dataItem.validThrough,
       "rtxValue3": dataItem.cardType === OLBConstants.CARD_TYPE.Debit ? dataItem.dailyWithdrawalLimit: dataItem.creditLimit,
       "rtxValue4": dataItem.cardType === OLBConstants.CARD_TYPE.Debit ? dataItem.accountName: dataItem.availableCredit,
       "rtxValue5": dataItem.cardType === OLBConstants.CARD_TYPE.Debit ? dataItem.maskedAccountNumber: dataItem.billingAddress,
         "rtxValue6": dataItem.serviceProvider,
       "rtxValue7": dataItem.cardHolder,
       "rtxValue8": dataItem.secondaryCardHolder,
         "lblTravelNotificationEnabled":cards.status[i].status.toLowerCase() ===  "yes" ? kony.i18n.getLocalizedString('i18n.CardManagement.TravelNotificationsEnabled') : "",
        };
          var actionButtonIndex;
          for (var index =0; index<dataItem.actions.length; index++) {
            actionButtonIndex = Number(index)+1;
            card['btnAction' + actionButtonIndex] = self.getActionButton(dataItem, dataItem.actions[index]);
          }
          cardsSegmentData.push(card);
      }
  this.view.myCards.segMyCards.widgetDataMap=dataMap;
  this.view.myCards.segMyCards.setData(cardsSegmentData);
      }
  this.view.myCards.segMyCards.setVisibility(true);
  this.view.forceLayout();
      this.AdjustScreen();
       CommonUtilities.hideProgressBar(this.view);
},

  alignConfirmButtons: function(buttonsJSON){
    if(buttonsJSON.btnModify.isVisible){
      this.view.CardLockVerificationStep.confirmButtons.btnCancel.left = '30.33%';
    }else{
      this.view.CardLockVerificationStep.confirmButtons.btnCancel.left = '53.41%';
    }
    this.view.CardLockVerificationStep.confirmButtons.btnConfirm.setVisibility(buttonsJSON.btnConfirm.isVisible);
    this.view.CardLockVerificationStep.confirmButtons.btnConfirm.text = buttonsJSON.btnConfirm.text;
    this.view.CardLockVerificationStep.confirmButtons.btnConfirm.toolTip = buttonsJSON.btnConfirm.toolTip;
    this.view.CardLockVerificationStep.confirmButtons.btnModify.setVisibility(buttonsJSON.btnModify.isVisible);
    this.view.CardLockVerificationStep.confirmButtons.btnModify.text = buttonsJSON.btnModify.text;
    this.view.CardLockVerificationStep.confirmButtons.btnModify.toolTip = buttonsJSON.btnModify.toolTip;
    this.view.CardLockVerificationStep.confirmButtons.btnCancel.setVisibility(buttonsJSON.btnCancel.isVisible);
    this.view.CardLockVerificationStep.confirmButtons.btnCancel.text = buttonsJSON.btnCancel.text;
    this.view.CardLockVerificationStep.confirmButtons.btnCancel.toolTip = buttonsJSON.btnCancel.toolTip;
  },
    /**
         * showCards - Method that hides all other flexes except the Cards segment.
         * @member of {frmCardManagementController}
         * @param {Array} - Array of JSON objects of cards.
         * @returns {VOID}
         * @throws {}
         */
        showCards: function(cards) {
            var self = this;
            this.hideAllCardManagementViews();
			this.showContactUsNavigation();
            this.view.flxMangeTravelPlans.onClick=self.fetchTravelNotifications;
            this.view.flxMyCardsView.setVisibility(true);
			this.view.myCards.segMyCards.setVisibility(true);
			this.view.flxApplyForNewCard.setVisibility(false);
			this.view.flxMyCards.setVisibility(true);
			this.view.myCards.setVisibility(true);	
			this.view.myCards.lblMyCardsHeader.text = kony.i18n.getLocalizedString("i18n.CardManagement.MyCards");
			this.view.forceLayout();				
            this.presenter.fetchCardsStatus(cards);
        },
        /**
         * showCardsStatus - Method that hides all other flexes except the Cards segment.
         * @member of {frmCardManagementController}
         * @param {Array} - Array of card ids.
         * @returns {VOID}
         * @throws {}
         */
        showCardsStatus: function(cards) {
            var self = this;
            this.setCardsData(cards);
            this.view.forceLayout();	
        },
		

/**
   * showTravelNotifications - Method to navigate to travel notifications.
   * @param {} - NONE
   * @returns {VOID}
   * @throws {}
   */
showTravelNotifications:function(travelNotifications){
    var self = this;
    this.hideAllCardManagementViews();
    this.view.flxMyCardsView.setVisibility(true);
    this.view.flxTravelPlan.setVisibility(false);
    this.view.flxMyCards.setVisibility(true);
    this.view.flxRightBar.setVisibility(true);
    this.view.lblManageTravelPlans.text = kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan');		
    this.view.flxMangeTravelPlans.onClick = self.navigateToAddTravelNotification.bind(this);
    this.setBreadCrumbAndHeaderDataTravelPlan()
    this.setTravelNotificationsData(travelNotifications);
},

navigateToAddTravelNotification:function(){
  var self = this;
  CommonUtilities.showProgressBar(this.view);
  self.presenter.AddNewTravelPlan();	
},
changeNotificationRowTemplate: function(){
  var index = this.view.myCards.segMyCards.selectedIndex;
  var rowIndex = index[1];
  var data = this.view.myCards.segMyCards.data;
  for(var i=0;i<data.length;i++)
  {
    if(i===rowIndex)
    {
      
      if(data[i].template === "flxTravelNotificationsCollapsed")
      {
        data[i].imgCollapse = OLBConstants.IMAGES.ARRAOW_UP;
        data[i].template = "flxTravelNotificationsExpanded";
      }
      else
      {
        data[i].imgCollapse = OLBConstants.IMAGES.ARRAOW_DOWN;
        data[i].template = "flxTravelNotificationsCollapsed";
      }
    }
    else
    {
      data[i].imgCollapse = OLBConstants.IMAGES.ARRAOW_DOWN;
      data[i].template = "flxTravelNotificationsCollapsed";
    }
  }  
  this.view.myCards.segMyCards.setData(data);
  this.view.forceLayout();
  this.AdjustScreen();
},
showAddNewTravelPlan:function(locationData){
  var self = this;
  if(locationData){
  if(locationData.country){
    this.setCountryObject(locationData.country);
  }
  if(locationData.states){
    this.setStatesObject(locationData.states);
  }
  if(locationData.city){
    this.setCitiesObject(locationData.city)
  }
}
  if(self.view.lblManageTravelPlans.text === kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan')) {
    self.view.flxMyCards.setVisibility(false);
    self.view.lblMyCardsHeader.text = kony.i18n.getLocalizedString('i18n.CardManagement.CreateTravelPlan');
    self.view.lblRequestID.setVisibility(false);
    self.view.lblRequestNo.setVisibility(false);
    self.view.flxTravelPlan.setVisibility(true);
    this.view.calFrom.dateFormat = kony.onlineBanking.configurations.getConfiguration("frontendDateFormat");
    this.view.calTo.dateFormat = kony.onlineBanking.configurations.getConfiguration("frontendDateFormat");
    this.view.flxOtherDestinations.setVisibility(false);
    CommonUtilities.disableButton(this.view.btnContinue);
    this.setDefaultDataForNotifications();
    this.view.calFrom.onSelection = self.blockFutureDateSelection.bind(self.view.calFrom);
    this.view.calTo.onSelection = self.validateTravelPlanData.bind(this);
    this.view.lblAnotherDestination.skin ='skn3343a8labellatoRegular40Pr';
    this.view.txtPhoneNumber.onKeyUp = self.validateTravelPlanData.bind(this);
    this.view.btnCancel1.text = kony.i18n.getLocalizedString('i18n.CardManagement.Cancel');
    this.view.btnCancel1.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.Cancel');
    this.view.btnCancel1.onClick = function(){
      CommonUtilities.showProgressBar(self.view);
      self.presenter.fetchTravelNotifications();
    }
    this.view.btnContinue.text = kony.i18n.getLocalizedString('i18n.common.continue');
    this.view.btnContinue.toolTip = kony.i18n.getLocalizedString('i18n.common.continue');
    this.view.btnContinue.onClick = function(){
      CommonUtilities.showProgressBar(self.view);
      self.getDetailsOfNotification(self.notificationObject.isEditFlow);	
      self.presenter.getEligibleCards();
    }    
    this.view.flxDestinationList.setVisibility(false);
    this.setFlowActions();
    this.view.forceLayout();
    this.AdjustScreen();
    CommonUtilities.hideProgressBar(this.view)
  }
},

  enableAddButton : function(){
    var self = this;
     if(this.view.segDestinations.data.length< CommonUtilities.getConfiguration('numberOfLocations')){
       this.view.lblAnotherDestination.skin ='skn3343a8labellatoRegular';
       this.view.flxAddFeatureRequestandimg.onClick = self.addAddressTOList;
     }
  },

  disableAddButton : function(){
      this.view.lblAnotherDestination.skin ='skn3343a8labellatoRegular40Pr';
      this.view.flxAddFeatureRequestandimg.onClick = null;
 },

  setDefaultDataForNotifications:function(){
    var self = this;
    if (this.notificationObject.isEditFlow === true) {
      this.view.flxOtherDestinations.setVisibility(true);
      this.view.lblRequestID.setVisibility(true);
      this.view.lblRequestNo.setVisibility(true);
      this.view.lblManageTravelPlans.text = kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan');		
      this.view.flxMangeTravelPlans.onClick = self.navigateToAddTravelNotification.bind(this);
      this.view.lblMyCardsHeader.text  = kony.i18n.getLocalizedString('i18n.CardManagement.editTravelPlan');
      self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.editTravelPlan'));
      CommonUtilities.enableButton(this.view.btnContinue);
  }
else{
      this.view.txtDestination.text = "";
      this.view.txtPhoneNumber.text = "";
      this.view.txtareaUserComments.text = "";
      this.view.segDestinations.setData([]);
      this.view.segDestinationList.setData([]);
      this.view.lblManageTravelPlans.text = kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans');		
      this.view.flxMangeTravelPlans.onClick = self.fetchTravelNotifications.bind(this);
      self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.CreateTravelPlan'));      
   }
},
addAddressTOList: function() {
  if(this.view.txtDestination.text!==""){
  this.view.flxOtherDestinations.setVisibility(true);
  var self = this;
  var dataMap = {
      "lblDestination": "lblDestination",
      "lblPlace": "lblPlace",
      "lblAnotherDestination": "lblAnotherDestination",
      "lblSeparator2": "lblSeparator2",
      "imgClose" : "imgClose"
  };
  var data = [{
      "lblDestination": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
      "lblPlace": this.view.txtDestination.text,
      "imgClose":{
      "onClick" : self.removeAddressFromList
      },
     "lblSeparator2": "a",
    }];	
      this.view.segDestinations.widgetDataMap = dataMap;
      this.view.segDestinations.addAll(data);
      this.view.txtDestination.text = "";
      self.disableAddButton();
      this.validateTravelPlanData();
  }
      this.view.forceLayout();
      this.AdjustScreen();
 },

removeAddressFromList:function(){
 var self = this; 
 var index = this.view.segDestinations.selectedIndex;
 if(index){
    this.view.segDestinations.removeAt(index[1]);  
 }
   self.validateTravelPlanData();			
}, 

blockFutureDateSelection:function(widgetId){
  var selectedDate = widgetId.date;
  CommonUtilities.blockFutureDate(this.view.calTo,selectedDate,60);
},

validateTravelPlanData:function(){
  var self = this;
  if(self.isPhoneNumberValid(this.view.txtPhoneNumber.text) && (this.view.segDestinations.data.length>0) && (CommonUtilities.getNumberOfDays(this.view.calFrom.date,this.view.calTo.date))<61){
   CommonUtilities.enableButton(this.view.btnContinue);
  }else{
    CommonUtilities.disableButton(this.view.btnContinue);
  }
},

isPhoneNumberValid: function (phoneNumber) {
  if (phoneNumber === "" || phoneNumber === null || phoneNumber === undefined)
    return true;
  else {
    var regex = new RegExp("^[0-9]");
    if (regex.test(phoneNumber) && phoneNumber.length === 10) {
      return true;
    } else
      return false;
  }
},

isInternationalPlan: function() {
  var userCountry;
  var Country = kony.mvc.MDAApplication.getSharedInstance().appContext.address;
  Country.forEach(function(dataItem) {
      if (dataItem.isPrimary === "true") userCountry = dataItem.CountryName;
  });
  var selectedCountries = this.notificationObject.locations.map(function(item){
    var arr = item.split(","); 
    return arr[0];
  });
  for (var key in selectedCountries) {
      if (selectedCountries[key] === userCountry) return false;
  }
  return true;
},

showSelectCardScreen : function(cards){
  var self = this;
  var internationEligibleCards = [];
  if(this.isInternationalPlan()){
     cards.forEach(function(item){
      if (item.isInternational == "true") 
         internationEligibleCards.push(item);
    });
  }
 if(internationEligibleCards.length>0){ 
    cards = internationEligibleCards;
 }
 if(cards.length>0){
    this.view.myCards.flxNoCardsError.setVisibility(false);
    this.view.flxMyCards.setVisibility(true);
    this.view.myCards.segMyCards.setVisibility(true);
    this.view.flxTravelPlan.setVisibility(false);
    var widgetDataMap = {
    "imgCard":"imgCard",
    "lblTravelNotificationEnabled" : "lblTravelNotificationEnabled",
    "flxCheckBox" : "flxCheckBox",
    "lblCheckBox" : "lblCheckBox",
    "flxCardDetails" : "flxCardDetails",
    "flxDetailsRow1" : "flxDetailsRow1",
    "flxDetailsRow2" : "flxDetailsRow2",
    "flxDetailsRow3" : "flxDetailsRow3",
    "lblKey1" : "lblKey1",
    "lblKey2" : "lblKey2",
    "lblKey3" : "lblKey3",
    "rtxValue1" : "rtxValue1",
    "rtxValue2" : "rtxValue2",
    "rtxValue3" : "rtxValue3",
    "lblSeparator2" : "lblSeparator2",
    "lblSeperator3" : "lblSeperator3",
    "lblCardsSeperator" : "lblCardsSeperator"
  };

  var cardSegData = [];
  var card = {};
  cards.forEach(function(dataItem){
      card = {
        "lblCardsSeperator": {
          "text": ".",
          "height": "105px"
        },
       "lblSeperator3" : {
         "text" : "a",
         "height" : "1"
       },
       "imgCard": {
        "src": self.getImageForCard(dataItem.cardProductName),
       },
       "flxCheckBox":{
         "onClick" : self.toggleSelectedCardCheckbox.bind(self),
       },
       "lblCheckBox" :{
         "text" : self.isCardSelected(dataItem)
       },
       "lblKey1" :{
         "text" :kony.i18n.getLocalizedString("i18n.CardManagement.cardName")+":",
       },
       "lblKey2" : {
         "text" : kony.i18n.getLocalizedString("i18n.CardManagement.CardNumber")+":",
       },
       "lblKey3" : {
         "text" : kony.i18n.getLocalizedString("i18n.CardManagement.internationalEligible")+":",
       },
       "rtxValue1" : {
        "text" :dataItem.cardProductName 
      },
      "rtxValue2" : {
        "text" :dataItem.maskedCardNumber
      },
      "rtxValue3" : {
        "text" :dataItem.isInternational
      },
      "template":"flxSelectFromEligibleCards"
    };
    cardSegData.push(card);
  });
  this.view.myCards.segMyCards.widgetDataMap = widgetDataMap;
  this.view.myCards.segMyCards.setData(cardSegData);
  this.view.flxEligibleCardsButtons.setVisibility(true);
  this.view.btnCardsCancel.onClick = this.showNotificationToModify.bind(this);
  self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.ManageTravelPlans'));        
  this.view.btnCardsContinue.onClick = function() {
      self.getSelectedCards();
      self.showConfirmationScreen(self.notificationObject);
  }
}else{
  this.view.myCards.flxNoCardsError.setVisibility(true);
  this.view.flxMyCards.setVisibility(true);
  this.view.myCards.segMyCards.setVisibility(false);
  this.view.flxTravelPlan.setVisibility(false);
}
  this.view.forceLayout();
  this.AdjustScreen();
  CommonUtilities.hideProgressBar(this.view); 
},

toggleSelectedCardCheckbox : function(){
  var self = this;
  var selectedIndex = this.view.myCards.segMyCards.selectedIndex[1];
  var data = this.view.myCards.segMyCards.data;
  for(var index=0;index<data.length;index++)
  {
    if(index===selectedIndex)
      {
        if(data[index].lblCheckBox.text === "C") {
           data[index].lblCheckBox.text = "D";
        }
        else {
         data[index].lblCheckBox.text = "C";
        }
  }  
  this.view.myCards.segMyCards.setData(data);
 }
  self.checkContinue(data);
},

checkContinue:function(data){
  var enable = false;
   data.forEach(function(dataItem){
    if(dataItem.lblCheckBox.text === "C")
       enable = true;
   })
   if(enable){
    CommonUtilities.enableButton(this.view.btnCardsContinue)
   }
   else{
    CommonUtilities.disableButton(this.view.btnCardsContinue)
   }  
},

isCardSelected:function(dataItem){
   var cardName = dataItem.cardProductName+" "+dataItem.maskedCardNumber;
   var selectedcards = this.notificationObject.selectedcards;
   var txt = "D";
   if(selectedcards){
    selectedcards.forEach(function(data){
        if(data.name+data.number === cardName)
          txt = "C";
     });
   }else {
     txt = "D";
   }
   return txt;  
},

showNotificationToModify : function(){
  this.view.lblRequestID.setVisibility(false);
  this.view.lblRequestNo.setVisibility(false);
  this.view.flxConfirm.setVisibility(false);
  this.view.flxMyCards.setVisibility(false);
  this.view.flxTravelPlan.setVisibility(true);
  this.view.flxOtherDestinations.setVisibility(true);
  this.view.flxMyCardsView.setVisibility(true);
  CommonUtilities.enableButton(this.view.btnCardsContinue);
  this.view.forceLayout();
  this.AdjustScreen();
},

getSelectedCards : function(){
   var cards = this.view.myCards.segMyCards.data;
   var selectedcards =[]
   cards.forEach(function(dataItem){
        if(dataItem.lblCheckBox.text==="C")
         selectedcards.push({"name":dataItem.rtxValue1.text,"number":dataItem.rtxValue2.text});
   });
   this.notificationObject.selectedcards = selectedcards;
},

getDetailsOfNotification : function(isEditFlow){
  var self = this;
  var selectedcards = self.notificationObject.selectedcards;
  var requestId = this.notificationObject.requestId;
  self.notificationObject = {
     'fromDate' : this.view.calFrom.date,
     'toDate' : this.view.calTo.date,
     'phone' : this.view.txtPhoneNumber.text,
     'notes' : this.view.txtareaUserComments.text,
     'locations' : self.getSelectedLocations()
   };
   if(selectedcards) {
      self.notificationObject.selectedcards = selectedcards;
   }
   if(isEditFlow){
      self.notificationObject.isEditFlow = isEditFlow;
      self.notificationObject.requestId = requestId
   }
   return self.notificationObject;
},

getSelectedLocations:function(){
  var locationArray = this.view.segDestinations.data;
  var selectedLocations = [];
  for(var key in locationArray ){
     selectedLocations.push(locationArray[key].lblPlace);
  }
  return selectedLocations;
},

showConfirmationScreen:function(data){
  var self = this;
  var cards = "";
  this.view.flxActivateCard.setVisibility(false);
  this.view.flxAcknowledgment.setVisibility(false);
  this.view.flxCardVerification.setVisibility(false);
  this.view.flxMyCardsView.setVisibility(false);
  this.view.flxConfirm.setVisibility(true);
  this.view.flxDestination2.setVisibility(false);
  this.view.flxDestination3.setVisibility(false);
  this.view.flxDestination4.setVisibility(false);
  this.view.flxDestination5.setVisibility(false);
  this.view.flxErrorMessage.setVisibility(false);
  this.view.flxConfirmHeading.setVisibility(false);
  this.view.flxDownload.setVisibility(false);
  self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.manageTravelPlanConfirmation'));          
  this.view.lblKey1.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedStartDate');
  this.view.rtxValue1.text = data.fromDate;
  this.view.lblKey2.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedEndDate');
  this.view.rtxValue2.text = data.toDate;
  this.view.lblDestination1.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination1');
  this.view.rtxDestination1.text = data.locations[0];
  if(data.locations[1]){
    this.view.flxDestination2.setVisibility(true);
    this.view.lblDestination2.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination2');
    this.view.rtxDestination2.text = data.locations[1];
  }
  if(data.locations[2]){
    this.view.flxDestination3.setVisibility(true);
    this.view.lblDestination3.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination3');
    this.view.rtxDestination3.text = data.locations[2];
  }
  if(data.locations[3]){
    this.view.flxDestination4.setVisibility(true);
    this.view.lblDestination4.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination4');
    this.view.rtxDestination4.text = data.locations[3];   
  }  
 if(data.locations[4]){
  this.view.flxDestination5.setVisibility(true); 
  this.view.lblDestination5.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination5');
  this.view.rtxDestination5.text = data.locations[4];
 }  
 this.view.lblKey5.text = kony.i18n.getLocalizedString('i18n.ProfileManagement.Phone');
 this.view.rtxValue5.text = data.phone;
 this.view.lblKey6.text = kony.i18n.getLocalizedString('i18n.CardManagement.AddInformation'); 
 this.view.rtxValue6.text = data.notes;
 this.view.lblKey4.text = kony.i18n.getLocalizedString('i18n.CardManagement.selectedCards');
 data.selectedcards.map(function(dataItem){
  cards = cards+dataItem.name+dataItem.number+"<br/>";
});
 this.view.rtxValueA.text = cards;
 this.view.btnCancelPlan.text = kony.i18n.getLocalizedString('i18n.CardManagement.Cancel');
 this.view.btnCancelPlan.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.Cancel');
 this.view.btnCancelPlan.onClick = function(){
   self.showPopUp();
}
 this.view.btnModify.text = kony.i18n.getLocalizedString('i18n.common.modifiy');
 this.view.btnModify.tooltip = kony.i18n.getLocalizedString('i18n.common.modifiy');
 this.view.btnModify.onClick = this.showNotificationToModify.bind(this);
 this.view.btnConfirm.text = kony.i18n.getLocalizedString('i18n.common.confirm');
 this.view.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirm');
 if(this.notificationObject.isEditFlow){
 this.view.btnConfirm.onClick = function(){
   CommonUtilities.showProgressBar(self.view);
   self.presenter.updateTravelNotification(self.notificationObject);
 }
}
else{
    this.view.btnConfirm.onClick = function(){
    CommonUtilities.showProgressBar(self.view);
    self.presenter.createTravelNotification(self.notificationObject);
  }
}
 this.view.forceLayout();
 this.AdjustScreen();
},

showPopUp:function(){
  var self = this;
  this.view.flxAlert.setVisibility(true);
  this.view.CustomAlertPopup.setVisibility(true);
  this.view.CustomAlertPopup.lblPopupMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.cancelCreateMsg');
  this.view.CustomAlertPopup.btnYes.onClick = function(){
    CommonUtilities.showProgressBar(self.view);
    self.view.CustomAlertPopup.setVisibility(false);
    self.view.flxAlert.setVisibility(false);
    self.notificationObject = {};
    self.presenter.fetchTravelNotifications(); 
  }
  this.view.CustomAlertPopup.btnNo.onClick = function(){
    self.view.CustomAlertPopup.setVisibility(false);
    self.view.flxAlert.setVisibility(false);
  }
  this.view.CustomAlertPopup.flxCross.onClick = function(){
    self.view.CustomAlertPopup.setVisibility(false);
    self.view.flxAlert.setVisibility(false);
  }
},

showTravelNotificationAcknowledgement: function(response){
   var self = this;
   var data = this.notificationObject;
   var cards = "";
   self.hideAllCardManagementViews();
   this.view.flxPrint.setVisibility(false);
   this.view.flxPrint.setVisibility(false);
   this.view.ConfirmDialog.keyValueCreditLimit.setVisibility(false);
   this.view.ConfirmDialog.keyValueAvailableCredit.setVisibility(false);
   this.view.flxAcknowledgment.setVisibility(true);
   self.setBreadCrumbAndHeaderDataForCardOperation(kony.i18n.getLocalizedString('i18n.CardManagement.manageTravelPlanAcknowledgement'));             
   this.view.lblCardAcknowledgement.text = kony.i18n.getLocalizedString('i18n.CardManagement.travelNotification');
   if(this.notificationObject.isEditFlow){
    this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.travelNotificationUpdateMsg');
    this.view.Acknowledgement.lblUnlockCardMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.requestId')+":"+this.notificationObject.requestId; 
   }
   else{
   this.view.Acknowledgement.lblCardTransactionMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.travelNotificationCreatationMsg');
   this.view.Acknowledgement.lblUnlockCardMessage.text = kony.i18n.getLocalizedString('i18n.CardManagement.requestId')+":"+response.request_id;
   }
   this.view.ConfirmDialog.confirmHeaders.lblHeading.text = kony.i18n.getLocalizedString('i18n.CardManagement.travelDetails');
   this.view.ConfirmDialog.flxDestination.setVisibility(true);
   this.view.ConfirmDialog.flxSelectCards.setVisibility(true);
   this.view.ConfirmDialog.flxDestination2.setVisibility(false);
   this.view.ConfirmDialog.flxDestination3.setVisibility(false);
   this.view.ConfirmDialog.flxDestination4.setVisibility(false);
   this.view.ConfirmDialog.flxDestination5.setVisibility(false);
   this.view.ConfirmDialog.keyValueCardHolder.lblKey.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedStartDate');
   this.view.ConfirmDialog.keyValueCardHolder.lblValue.text = data.fromDate;
   this.view.ConfirmDialog.keyValueCardName.lblKey.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedEndDate');
   this.view.ConfirmDialog.keyValueCardName.lblValue.text = data.toDate;
   this.view.ConfirmDialog.flxDestination1.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination1');
   this.view.ConfirmDialog.rtxDestination1.text = data.locations[0];
   if(data.locations[1]){
    this.view.ConfirmDialog.flxDestination2.setVisibility(true);
    this.view.ConfirmDialog.lblDestination2.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination2');
    this.view.ConfirmDialog.rtxDestination2.text = data.locations[1];
  }
  if(data.locations[2]){
    this.view.ConfirmDialog.flxDestination3.setVisibility(true);
    this.view.ConfirmDialog.lblDestination3.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination3');
    this.view.ConfirmDialog.rtxDestination3.text = data.locations[2];
  }
  if(data.locations[3]){
    this.view.ConfirmDialog.flxDestination4.setVisibility(true);
    this.view.ConfirmDialog.lblDestination4.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination4');
    this.view.ConfirmDialog.rtxDestination4.text = data.locations[3];   
  }  
 if(data.locations[4]){
  this.view.ConfirmDialog.flxDestination5.setVisibility(true); 
  this.view.ConfirmDialog.lblDestination5.text = kony.i18n.getLocalizedString('i18n.CardManagement.SelectedDestination5');
  this.view.ConfirmDialog.rtxDestination5.text = data.locations[4];
  }  
  this.view.ConfirmDialog.keyValueValidThrough.lblKey.text = kony.i18n.getLocalizedString('i18n.ProfileManagement.Phone');
  this.view.ConfirmDialog.keyValueValidThrough.lblValue.text = data.phone;
  this.view.ConfirmDialog.keyValueServiceProvider.lblKey.text = kony.i18n.getLocalizedString('i18n.CardManagement.AddInformation');
  this.view.ConfirmDialog.keyValueServiceProvider.lblValue.text = data.notes; 
  this.view.ConfirmDialog.lblKey6.text = kony.i18n.getLocalizedString('i18n.CardManagement.selectedCards');
  data.selectedcards.map(function(dataItem){
    cards = cards+dataItem.name+dataItem.number+"<br/>";
  });
  this.view.ConfirmDialog.rtxValueA.text = cards;
  this.view.btnBackToCards.setVisibility(true);
  this.view.btnRequestReplacement.setVisibility(true);
  this.view.btnRequestReplacement.text = kony.i18n.getLocalizedString('i18n.CardManagement.BackToCards');
  this.view.btnRequestReplacement.tooltip = kony.i18n.getLocalizedString('i18n.CardManagement.BackToCards');
  this.view.btnRequestReplacement.onClick = this.presenter.navigateToManageCards.bind(this.presenter);
  this.view.btnBackToCards.onClick = this.presenter.navigateToManageCards.bind(this.presenter);
  this.view.btnBackToCards.text = kony.i18n.getLocalizedString('i18n.CardManagement.goToTravelPlan');
  this.view.btnBackToCards.tooltip = kony.i18n.getLocalizedString('i18n.CardManagement.goToTravelPlan');
  this.view.btnBackToCards.onClick =function(){
  CommonUtilities.showProgressBar(self.view);
  self.presenter.fetchTravelNotifications();
  }
  this.notificationObject = {};
  CommonUtilities.hideProgressBar(this.view);
  this.view.forceLayout();
  this.AdjustScreen();
},

editTravelNotification:function(data){
  var self = this;
  this.notificationObject.requestId = data.notificationId;
  this.notificationObject.isEditFlow = true;
  self.navigateToAddTravelNotification();
  self.getBackendCards(data.cardNumber);
  this.view.lblRequestID.setVisibility(true);
  this.view.lblRequestNo.setVisibility(true);
  this.view.segDestinations.setVisibility(true);
  this.view.lblRequestID.text = kony.i18n.getLocalizedString('i18n.CardManagement.requestId');
  this.view.lblRequestNo.text = data.notificationId;
  CommonUtilities.disableOldDaySelection(this.view.calFrom, self.returnFrontendDate(data.startDate));
  CommonUtilities.disableOldDaySelection(this.view.calTo, self.returnFrontendDate(data.endDate));
  this.view.txtPhoneNumber.text = data.contactNumber;
  this.view.txtareaUserComments.text =  data.additionalNotes;
  this.setDestinations(data.destinations);
  this.view.forceLayout();
},

setDestinations:function(data){
  var self = this;
  var destinations = [];
  if(data){
    var dataMap = {
        "lblDestination": "lblDestination",
        "lblPlace": "lblPlace",
        "lblAnotherDestination": "lblAnotherDestination",
        "lblSeparator2": "lblSeparator2",
        "imgClose": "imgClose"
    };
    if (data.indexOf("-") > 0) {
      data = data.split("-");
      data.forEach(function (dataItem) {
        var segData = {
          "lblDestination": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
          "lblPlace": dataItem,
          "imgClose": {
            "onClick": self.removeAddressFromList.bind(self),
          },
          "lblSeparator2": "a",
        };
        destinations.push(segData);
      })
    }
    else {
      var segData = {
        "lblDestination": kony.i18n.getLocalizedString('i18n.CardManagement.destination'),
        "lblPlace": data,
        "imgClose": {
          "onClick": self.removeAddressFromList.bind(self),
        },
        "lblSeparator2": "a",
      };
      destinations.push(segData);
    }
    this.view.segDestinations.widgetDataMap = dataMap;
    this.view.segDestinations.setData(destinations);
}
    this.validateTravelPlanData();
},

getBackendCards : function(data){
 if(data){ 
    if(data.indexOf(","))
       this.notificationObject.selectedcards = data.split(",");
    else
       this.notificationObject.selectedcards = data;   
 }
},
  /**
   * showContactUsNavigation - Method to navigate to contactus.
   * @member of {frmCardManagementController}
   * @param {} - NONE.
   * @returns {VOID}
   * @throws {}
   */  
  showContactUsNavigation:function(){
    this.view.flxApplyForNewCard.setVisibility(false);
    this.view.lblActivateNewCard.onClick = function(){
        var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
        informationContentModule.presentationController.showContactUsPage();
    };
  },
/**
   * hideAllCardManagementViews - Method that hides all flexes in frmCardManagement.
   * @member of {frmCardManagementController}
   * @param {} - NONE.
   * @returns {VOID}
   * @throws {}
   */
hideAllCardManagementViews : function(){
  this.view.flxAcknowledgment.setVisibility(false);
  this.view.flxActivateCard.setVisibility(false);
  this.view.flxCardVerification.setVisibility(false);
  this.view.flxMyCardsView.setVisibility(false);
  this.view.flxTermsAndConditions.setVisibility(false);
  this.view.breadcrumb.setVisibility(false);
  this.view.flxTravelPlan.setVisibility(false);
  this.view.flxConfirm.setVisibility(false);
  this.view.myCards.flxNoCardsError.setVisibility(false);
  this.view.myCards.segMyCards.setVisibility(false);
  this.view.flxEligibleCardsButtons.setVisibility(false);
},
   setBreadCrumbAndHeaderDataTravelPlan: function(){
    var self=this;
    this.view.breadcrumb.setVisibility(true);
    this.view.breadcrumb.setBreadcrumbData([{
      'text':kony.i18n.getLocalizedString("i18n.CardManagement.MyCards"),
      'callback': self.presenter.fetchCardsList.bind(self.presenter)
    },{
      'text': kony.i18n.getLocalizedString("i18n.CardManagement.ManageTravelPlans"),
      'callback': null
    }]);
    this.view.myCards.lblMyCardsHeader.text = kony.i18n.getLocalizedString("i18n.CardManagement.ManageTravelPlans");
  },
  fetchTravelNotifications:function(){      
    CommonUtilities.showProgressBar(this.view);
    this.presenter.fetchTravelNotifications();
  },
  /**
    * Method to set to travel notifications data.
    * @param {} - NONE
    * @returns {VOID}
    * @throws {}
    */
   setTravelNotificationsData: function(travelNotifications) {
    var self = this;
    if (travelNotifications.length <= 0) {
        self.showNoTravelNotificationScreen();
    } else {
        self.view.myCards.segMyCards.setVisibility(true);
        var widgetDataMap = {
            "flxActions":"flxActions",
            "lblSeparator1": "lblSeparator1",
            "lblCardHeader": "lblCardHeader",
            "lblCardId": "lblCardId",
            "lblCardStatus": "lblCardStatus",
            "lblIdentifier": "lblIdentifier",
            "flxCollapse": "flxCollapse",
            "imgCollapse": "imgCollapse",
            "lblKey1": "lblKey1",
            "rtxValue1": "rtxValue1",
            "lblKey2": "lblKey2",
            "rtxValue2": "rtxValue2",
            "flxDestination1": "flxDestination1",
            "flxDestination2": "flxDestination2",
            "flxDestination3": "flxDestination3",
            "flxDestination4": "flxDestination4",
            "flxDestination5": "flxDestination5",
            "lblDestination1": "lblDestination1",
            "rtxDestination1": "rtxDestination1",
            "lblDestination2": "lblDestination2",
            "rtxDestination2": "rtxDestination2",
            "lblDestination3": "lblDestination3",
            "rtxDestination3": "rtxDestination3",
            "lblDestination4": "lblDestination4",
            "rtxDestination4": "rtxDestination4",
            "lblDestination5": "lblDestination5",
            "rtxDestination5": "rtxDestination5",
            "lblKey4": "lblKey4",
            "rtxValue4": "rtxValue4",
            "lblKey5": "lblKey5",
            "rtxValue5": "rtxValue5",
            "lblKey6": "lblKey6",
            "rtxValueA": "rtxValueA",
            "btnAction1":"btnAction1",
            "btnAction2":"btnAction2"
        };
        var segData = travelNotifications.map(function(dataItem) {
            var destinations = self.returnDestinationsArray(dataItem.destinations);
            return {
                "flxActions": {
                  "isVisible": true
                },
                "lblSeparator1": " ",
                "lblIdentifier": " ",
                "lblCardHeader": kony.i18n.getLocalizedString("i18n.CardManagement.requestId"),
                "lblCardId": dataItem.notificationId,
                "lblCardStatus": {
                    "text": dataItem.status,
                    "skin": self.statusSkinsLandingScreen[dataItem.status],
                },
                "flxCollapse":{
                  "onClick":self.changeNotificationRowTemplate
                },
                "imgCollapse": OLBConstants.IMAGES.ARRAOW_DOWN,
                "lblKey1": kony.i18n.getLocalizedString("i18n.CardManagement.travelStartDate"),
                "rtxValue1": self.returnFrontendDate(dataItem.startDate),
                "lblKey2": kony.i18n.getLocalizedString("i18n.CardManagement.travelEndDate"),
                "rtxValue2": self.returnFrontendDate(dataItem.endDate),                
                "flxDestination1": {
                  "isVisible": destinations[0] ? true : false
                },
                "lblDestination1": kony.i18n.getLocalizedString("i18n.CardManagement.destination1"),
                "rtxDestination1": destinations[0],
                "flxDestination2": {
                  "isVisible": destinations[1] ? true : false
                },
                "lblDestination2": kony.i18n.getLocalizedString("i18n.CardManagement.destination2"),
                "rtxDestination2": destinations[1],
                "flxDestination3": {
                  "isVisible": destinations[2] ? true : false
                },
                "lblDestination3": kony.i18n.getLocalizedString("i18n.CardManagement.destination3"),
                "rtxDestination3": destinations[2],
                "flxDestination4": {
                  "isVisible": destinations[3] ? true : false
                },
                "lblDestination4": kony.i18n.getLocalizedString("i18n.CardManagement.destination4"),
                "rtxDestination4": destinations[3],
                "flxDestination5": {
                  "isVisible": destinations[4] ? true : false
                },
                "lblDestination5": kony.i18n.getLocalizedString("i18n.CardManagement.destination5"),
                "rtxDestination5": destinations[4],
                "lblKey4": kony.i18n.getLocalizedString("i18n.LoginMFA.PhoneNo"),
                "rtxValue4": "phoneNumber",
                "lblKey5": kony.i18n.getLocalizedString("i18n.CardManagement.additionalInfo"),
                "rtxValue5": dataItem.additionalNotes,
                "lblKey6": kony.i18n.getLocalizedString("i18n.CardManagement.selectedCards"),
                "rtxValueA": self.returnCardDisplayName(dataItem.cardNumber),
                "btnAction1": {
                    "skin":dataItem.status===OLBConstants.CARD_STATUS.Cancelled?"sknBtnLato3343A813PxBg0CSR":"sknBtnLato3343a813px",
                    "text": kony.i18n.getLocalizedString("i18n.NAO.edit"),
                    "onClick": dataItem.status===OLBConstants.CARD_STATUS.Cancelled?null:self.editTravelNotification.bind(self,dataItem)
                },
                "btnAction2": {
                    "text": kony.i18n.getLocalizedString("i18n.common.delete"),
                    "onClick": self.deleteNotification.bind(self, dataItem.notificationId)
                },
                "template": "flxTravelNotificationsCollapsed"
            };
        });
        this.view.myCards.segMyCards.widgetDataMap = widgetDataMap;
        this.view.myCards.segMyCards.setData(segData);        
    }
    this.AdjustScreen();
    CommonUtilities.hideProgressBar(this.view);
},
returnFrontendDate:function(date){
  var x=date.split('-');
  var dateObj = new Date(x[0],x[1]-1,x[2]);
  var dateString = CommonUtilities.getFrontendDateString(dateObj,CommonUtilities.getConfiguration("frontendDateFormat"));
  return dateString;
},
  returnDestinationsArray:function(destinations){
    var array = [];
    destinations.split('-').forEach(function(destination){
      array.push(destination);
    })
    return array;
  },
  returnCardDisplayName:function(cards){
    return cards.replace(/,/g, "<br/>");
  },
  showNoTravelNotificationScreen:function(){
    this.view.myCards.flxNoCardsError.setVisibility(true);
    this.view.myCards.lblNoCardsError.text = kony.i18n.getLocalizedString('i18n.CardsManagement.NoTravelNotificationError');
    this.AdjustScreen();
  },
  deleteNotification:function(notificationId){
    var self=this;
    this.view.flxAlert.setVisibility(true);
    var height = this.view.customheader.frame.height + this.view.flxMain.frame.height + this.view.flxFooter.frame.height;
    this.view.flxAlert.height = height + "dp";
    this.view.CustomAlertPopup.lblPopupMessage.text=kony.i18n.getLocalizedString("i18n.CardManagement.deleteTravelMsg")+" "+notificationId+" ?";
    this.view.CustomAlertPopup.lblHeading.text=kony.i18n.getLocalizedString("i18n.common.delete");
    this.view.CustomAlertPopup.lblHeading.setFocus(true);
    this.view.CustomAlertPopup.btnYes.onClick = function(){      
      CommonUtilities.showProgressBar(this.view);
      self.presenter.deleteNotification(notificationId);
    };
    this.view.CustomAlertPopup.flxCross.onClick = function(){
      self.view.flxAlert.isVisible = false;
    };
    this.view.CustomAlertPopup.btnNo.onClick = function(){
      self.view.flxAlert.isVisible = false;
    };
    this.view.forceLayout();
  },
  deleteNotificationSuccess:function(){
    this.view.flxAlert.setVisibility(false);
    this.fetchTravelNotifications();
  },

	/**
     * hideAllCardManagementRightViews - Method that hides all right side flexes in frmCardManagement.
     * @member of {frmCardManagementController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */
	hideAllCardManagementRightViews : function(){
		this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(false);
		this.view.CardLockVerificationStep.flxVerifyBySecureAccessCode.setVisibility(false);
		this.view.CardLockVerificationStep.flxVerifyBySecurityQuestions.setVisibility(false);
		this.view.CardLockVerificationStep.flxDeactivateCard.setVisibility(false);
		this.view.CardLockVerificationStep.flxTravelNotification.setVisibility(false);
		this.view.CardLockVerificationStep.flxChangeCardPin.setVisibility(false);
		this.view.CardLockVerificationStep.flxConfirmPIN.setVisibility(false);
		this.view.CardLockVerificationStep.flxCardReplacement.setVisibility(false);
	},
	primaryButtonOnClick:function(){
		this.hideAllCardManagementRightViews();
		if (this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.currentScreen=="warning") {
			this.view.CardLockVerificationStep.flxVerifyByOptions.setVisibility(true);
			this.view.CardLockVerificationStep.confirmButtons.btnConfirm.text="PROCEED";
			this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.currentScreen="verification";
			this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text=this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.headerText+" - VERIFICATION";
		} else if(this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.currentScreen=="verification"){
			this.view.CardLockVerificationStep.confirmButtons.btnConfirm.text="VERIFY";
			if (this.view.CardLockVerificationStep.imgUsernameVerificationcheckedRadio.src=="icon_radiobtn_active.png") {
				this.view.CardLockVerificationStep.flxVerifyBySecureAccessCode.setVisibility(true);
				this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text=this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.headerText+" - SECURE ACCESS CODE";
				this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.currentScreen="secureAccessCode";
			} else {
				this.view.CardLockVerificationStep.flxVerifyBySecurityQuestions.setVisibility(true);
				this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text=this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.headerText+" - SECURITY QUESTIONS";
				this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.currentScreen="securityQuestion";
			}
		} else if (this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.currentScreen=="secureAccessCode"||this.view.CardLockVerificationStep.confirmHeaders.lblHeading.info.currentScreen=="securityQuestion") {
			this.showAcknowledgement();
		}
		this.view.forceLayout();
	},
	changeRowTemplate: function(){
		var index = this.view.myCards.segMyCards.selectedIndex;
		var rowIndex = index[1];
		var data = this.view.myCards.segMyCards.data;
		for(var i=0;i<data.length;i++)
		{
			if(i==rowIndex)
			{
				
				if(data[i].template == "flxMyCardsCollapsed")
				{
					data[i].imgCollapse = OLBConstants.IMAGES.ARRAOW_UP;
					data[i].template = "flxMyCardsExpanded";
				}
				else
				{
					data[i].imgCollapse = OLBConstants.IMAGES.ARRAOW_DOWN;
					data[i].template = "flxMyCardsCollapsed";
				}
			}
			else
			{
				data[i].imgCollapse = OLBConstants.IMAGES.ARRAOW_DOWN;
				data[i].template = "flxMyCardsCollapsed";
			}
		}  
		this.view.myCards.segMyCards.setData(data);
		this.view.forceLayout();
        this.AdjustScreen();
	},
    setBreadCrumbAndHeaderDataForCardOperation: function(header){
      this.view.breadcrumb.setVisibility(true);
      this.view.breadcrumb.setBreadcrumbData([{
        'text':kony.i18n.getLocalizedString("i18n.CardManagement.ManageCard"),
        'callback': this.presenter.navigateToManageCards.bind(this.presenter)
      },{
        'text': header,
        'callback': null
      }]);
      this.view.CardLockVerificationStep.confirmHeaders.lblHeading.text = header;
    },
	/**
     * isValidSecureAccessCode - Validates given secure access code.
     * @member of {frmCardManagementController}
     * @param {} - NONE.
     * @returns {VOID}
     * @throws {}
     */
    isValidSecureAccessCode: function(secureaccesscode){
      if(secureaccesscode.length !== OLBConstants.OTPLength) return false;  	
   	  var regex = new RegExp('^[0-9]+$');
      return regex.test(secureaccesscode);
    }, 

	/**
     * updateHamburgerMenu - Method that updates Hamburger Menu.
     * @member of {frmCardManagementController}
     * @param {} - NONE
     * @returns {VOID}
     * @throws {}
     */
	updateHamburgerMenu: function (sideMenuModel) {
      	this.view.customheader.initHamburger(sideMenuModel, "ACCOUNTS" ,"Card Management");
    },

  /**
   * updateTopBar - Method that updates Top Menu.
   * @member of {frmCardManagementController}
   * @param {} - NONE
   * @returns {VOID}
   * @throws {}
   */
  updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
  },

  toTitleCase: function(str)
  {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  },

  setFlowActions : function(){
    var scope=this;
    this.view.txtDestination.onKeyUp= function(){
      scope.showTypeAhead();
    };
    this.view.segDestinationList.onRowClick = function(){
      var rowNo = scope.view.segDestinationList.selectedRowIndex[1];
      var dest = scope.view.segDestinationList.data[rowNo].lblListBoxValues;
      scope.view.txtDestination.text = dest;
      scope.enableAddButton();
      scope.view.flxDestinationList.setVisibility(false);
      scope.view.txtDestination.setFocus();
    };
  },

  showTypeAhead : function(){
    var countryData=[];
    var newData={};
    var stateId;
    var countryId;
    var key;
    var dataMap={
      "flxCustomListBox":"flxCustomListBox",
      "lblListBoxValues":"lblListBoxValues"
    };
    var tbxText=this.toTitleCase(this.view.txtDestination.text);
    if(tbxText.length > 2){
      for(key in this.cities){
        if(this.cities[key].name.indexOf(''+tbxText) === 0){
          stateId=this.cities[key].state_id;
          countryId=this.cities[key].country_id;
          newData={
            "countryId" : countryId,
            "lblListBoxValues":  this.countries[countryId].name + ", " + this.states[stateId].name + ", " + this.cities[key].name ,
            "template":"flxCustomListBox"
          }; 
          countryData.push(newData);
        }
      }
      for(key in this.states){
        if(this.states[key].name.indexOf(''+tbxText) === 0){
          countryId=this.states[key].country_id;
          newData={
            "countryId" : countryId,
            "lblListBoxValues": this.countries[countryId].name + ", " + this.states[key].name,
            "template":"flxCustomListBox"
          };
          countryData.push(newData);
        }
      }
      for(key in this.countries){
        if(this.countries[key].name.indexOf(''+tbxText) === 0){
          newData={
            "countryId" : countryId,
            "lblListBoxValues": ""+this.countries[key].name,
            "template":"flxCustomListBox"
          };
          countryData.push(newData);
        }
      }
      this.view.segDestinationList.widgetDataMap=dataMap;
      this.view.segDestinationList.setData(countryData);
      this.view.flxDestinationList.setVisibility(true);
      this.view.forceLayout();
    }
    else{
      this.view.flxDestinationList.setVisibility(false);
    }
  },
  setCountryObject: function(countryList){
    var self = this
   countryList.forEach(
      function(element) {
         self.countries[element.id] = {"name" : element.Name}
       })	
  },
  
  setStatesObject : function(stateList){
  var self = this
  stateList.forEach(
      function(element) {
         self.states[element.id] = {"name" : element.Name ,"country_id":element.Country_id}
       })	
  },

  setCitiesObject : function(cityList){
    var self = this
    cityList.forEach(
        function(element) {
           self.cities[element.id] = {"name" : element.Name ,"state_id":element.Region_id
           ,"country_id":element.Country_id}
         })	
    
	},
  
};
});