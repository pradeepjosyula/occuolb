define(['CommonUtilities'],function(CommonUtilities){
  return{
    shouldUpdateUI: function (viewModel) {
      return viewModel !== undefined && viewModel !== null;
    },
    /**
         * Function to make changes to UI
         * Parameters: feedbackViewModel {Object}
         */
    willUpdateUI: function (feedbackViewModel) {
      if(feedbackViewModel.showProgressBar){
        this.showProgressBar();
      }
      else if(feedbackViewModel.hideProgressBar){
        this.hideProgressBar();
      }
      if (feedbackViewModel.onServerDownError) {
        this.showServerDownForm(feedbackViewModel.onServerDownError);
      }
      if(feedbackViewModel.submitFeedbackSuccess){
        this.submitFeedbackSuccessFlow();
      }
      if(feedbackViewModel.preLoginView){
        this.showPreLoginView();
      }
      if(feedbackViewModel.postLoginView){
        this.showPostLoginView();
      }
      if(feedbackViewModel.sideMenu) {
        this.updateHamburgerMenu(feedbackViewModel.sideMenu);
      }
      if(feedbackViewModel.topBar) {
        this.updateTopBar(feedbackViewModel.topBar);
      }
      this.AdjustScreen();   
    },

    /**
     * Methoid to handle Server errors.
     * Will navigate to serverdown page.
     */
    showServerDownForm: function (onServerDownError) {
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      authModule.presentationController.navigateToServerDownScreen();
    },

    /**
     * Hide Progress bar
     */
    hideProgressBar : function(data){
      CommonUtilities.hideProgressBar(this.view);
    },
    /**
     * Show Progress bar
     */
    showProgressBar : function(data){
      CommonUtilities.showProgressBar(this.view);
    },
    /**
      * Initialize HamburgerMenu
      * @param: sideMenuModel
      */
    updateHamburgerMenu: function (sideMenuModel) {
      this.view.customheader.initHamburger(sideMenuModel);
    },
    /**
      * Initialize TopBar
      * @param: topBarModel
      */
    updateTopBar: function (topBarModel) {
      this.view.customheader.initTopBar(topBarModel);
    },
    /**
     * Feedback Pre-Login View UI 
     */
    showPreLoginView:function(){
      this.view.flxHeaderPreLogin.setVisibility(true);
      this.view.flxHeaderPostLogin.setVisibility(false);
    },
    /**
     * Feedback Post-Login View UI 
     */
    showPostLoginView:function(){
      this.view.flxHeaderPreLogin.setVisibility(false);
      this.view.flxHeaderPostLogin.setVisibility(true);
    },

    //UI code
    postShowCustomerFeedback:function(){
      this.AdjustScreen();
      this.disableButton(this.view.Feedback.confirmButtons.btnConfirm);
    },
    //UI Code
    AdjustScreen: function() {
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.frame.height + this.view.flxMainContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
          this.view.flxFooter.top = mainheight + diff + "dp";
        else
          this.view.flxFooter.top = mainheight + "dp";        
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },       

    preShowCustomerFeedback: function() {
      this.feedbackRating=0;
      var feedback=this.view.Feedback;
      this.view.flxFeedbackAcknowledgement.setVisibility(false);
      this.view.flxAcknowledgementContainer.setVisibility(false);
      this.view.flxFeedbackContainer.setVisibility(true);
      this.view.flxFeedback.setVisibility(true);
      feedback.imgRating1.src = "feedback_icon.png";
      feedback.imgRating2.src = "feedback_icon.png";
      feedback.imgRating3.src = "feedback_icon.png";
      feedback.imgRating4.src = "feedback_icon.png";
      feedback.imgRating5.src = "feedback_icon.png";
      /////// start
    //this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlx4f2683Occu";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxFeedback.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblFeedback.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgFeedback.src = "feedback_purple.png";
      ///// End
      feedback.flxAddFeatureRequest.setVisibility(true);
      feedback.flxUserFeedback.setVisibility(false);
      feedback.LblAddFeatureRequest.toolTip = "ADD FEATURE REQUEST";
      feedback.confirmButtons.btnModify.toolTip = "CANCEL";
      feedback.confirmButtons.btnConfirm.toolTip = "SUBMIT";	  
      feedback.txtareaUserComments.text="";
      feedback.txtareaUserAdditionalComments.text="";
      this.view.forceLayout();
      this.showActions();
    },
    showActions: function() {
      var scopeObj = this;
      this.view.Feedback.flxImgInfoIcon.onClick = function() {
        if(scopeObj.view.Feedback.AllForms.isVisible === false)
          scopeObj.view.Feedback.AllForms.isVisible = true;
        else
          scopeObj.view.Feedback.AllForms.isVisible = false;
      };
      this.view.Feedback.AllForms.flxCross.onClick = function() {
        scopeObj.view.Feedback.AllForms.isVisible = false;
      };
      this.view.Feedback.flxRating1.onClick = function() {
        scopeObj.showRatingAction(1);
      };
      this.view.Feedback.flxRating2.onClick = function() {
        scopeObj.showRatingAction(2);
      };
      this.view.Feedback.flxRating3.onClick = function() {
        scopeObj.showRatingAction(3);
      };
      this.view.Feedback.flxRating4.onClick = function() {
        scopeObj.showRatingAction(4);
      };
      this.view.Feedback.flxRating5.onClick = function() {
        scopeObj.showRatingAction(5);
      };
      this.view.Feedback.confirmButtons.btnModify.onClick = function() {
        scopeObj.addCancelAction();
        scopeObj.AdjustScreen();
      };
      this.view.Feedback.confirmButtons.btnConfirm.onClick = function() {
        scopeObj.addSubmitAction();
      };
      this.view.Feedback.flxAddFeatureRequestandimg.onClick = function() {
        scopeObj.addFeatureRequestAction();
        scopeObj.AdjustScreen();
      };
      this.view.btnAddAnotherAccount.onClick = function() {
        scopeObj.btnAckDoneAction();
      };
      this.view.btnDone.onClick = function() {
        scopeObj.btnAckDoneAction();
      };
      this.view.acknowledgment.flxTakeSurvey.onClick = function() {
        scopeObj.presenter.showSurveyForm();
      };
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        scopeObj.view.CustomPopupLogout.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopupLogout.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height = scopeObj.view.flxHeaderPostLogin.frame.height + scopeObj.view.flxMainContainer.frame.height;
        scopeObj.view.flxLogout.height = height + "dp";
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopupLogout.btnYes.onClick = function () {
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopupLogout.btnNo.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      }
      this.view.CustomPopupLogout.flxCross.onClick = function () {
        scopeObj.view.flxLogout.left = "-100%";
      }
    },
    toggleCheckBox: function (imgCheckBox) {
      CommonUtilities.toggleCheckbox(imgCheckBox);
    },
    showRatingAction: function(val) {
      for(var i=1;i<=val;i++) {
        this.view.Feedback["imgRating" + i].src = "feedback_icon_filled.png";
      }
      for (i=(val+1);i<=5;i++) {
        this.view.Feedback["imgRating" + i].src = "feedback_icon.png";
      }
      this.view.CustomerFeedbackDetails.imgRating1.src = this.view.Feedback.imgRating1.src;
      this.view.CustomerFeedbackDetails.imgRating2.src = this.view.Feedback.imgRating2.src;
      this.view.CustomerFeedbackDetails.imgRating3.src = this.view.Feedback.imgRating3.src;
      this.view.CustomerFeedbackDetails.imgRating4.src = this.view.Feedback.imgRating4.src;
      this.view.CustomerFeedbackDetails.imgRating5.src = this.view.Feedback.imgRating5.src;
      this.enableButton(this.view.Feedback.confirmButtons.btnConfirm);
      var rating=val.toString();
      this.feedbackRating = rating;
      this.view.forceLayout();
    },
    showRatingActionCircle: function(val) {
      for(var i=1;i<=val;i++) { 
        this.view.FeedbackSurvey["imgRating" + i].src = "circle_blue_filled.png";
      }
      for(i=(val+1);i<=5;i++) {
        this.view.FeedbackSurvey["imgRating" + i].src = "circle_unfilled.png";
      }
      this.enableButton(this.view.FeedbackSurvey.confirmButtons.btnConfirm);
      this.view.forceLayout();
    },
    addFeatureRequestAction: function() {
      this.view.Feedback.flxAddFeatureRequest.setVisibility(false);
      this.view.Feedback.flxUserFeedback.setVisibility(true);
      this.view.forceLayout();
    },
    TakeSurveyAction: function() {
      this.view.flxFeedback.setVisibility(false);
      this.view.flxFeedbackMoneyTransferProcess.setVisibility(true);
      this.view.flxFeedbackAcknowledgement.setVisibility(false);
      this.view.flxAcknowledgementContainer.setVisibility(false);
    },
    addSubmitAction: function() {
      var self=this;
      self.showProgressBar();
      var feedback={
        'rating': self.feedbackRating,
        'description':self.view.Feedback.txtareaUserComments.text.trim(),
        'featureRequest':self.view.Feedback.txtareaUserAdditionalComments.text.trim(),
      };
      self.presenter.createFeedback(feedback);
    },
    submitFeedbackSuccessFlow:function(){
      var self=this;
      self.hideProgressBar();
      var customerFeedback=self.view.CustomerFeedbackDetails;
      var feedback=self.view.Feedback;
      customerFeedback.lblcomments.text=feedback.txtareaUserComments.text.trim();
      customerFeedback.lblAnswer1.text=feedback.txtareaUserAdditionalComments.text.trim();
      if(feedback.txtareaUserComments.text.trim()==""){
        customerFeedback.lblcomments.text=kony.i18n.getLocalizedString("i18n.common.none");
      }
      if(feedback.txtareaUserAdditionalComments.text.trim()==""){
        customerFeedback.lblAnswer1.text=kony.i18n.getLocalizedString("i18n.common.none");
      }
      self.view.flxFeedback.setVisibility(false);
      self.view.flxFeedbackAcknowledgement.setVisibility(false);
      self.view.flxAcknowledgementContainer.setVisibility(true);
      self.view.forceLayout();
    },
    addCancelAction: function() {
      var feedback=this.view.Feedback;
      if(feedback.imgRating1.src === "feedback_icon_filled.png") {
        feedback.imgRating1.src = "feedback_icon.png";
        feedback.imgRating2.src = "feedback_icon.png";
        feedback.imgRating3.src = "feedback_icon.png";
        feedback.imgRating4.src = "feedback_icon.png";
        feedback.imgRating5.src = "feedback_icon.png";
        this.disableButton(feedback.confirmButtons.btnConfirm);
        feedback.txtareaUserComments.text="";
        feedback.txtareaUserAdditionalComments.text="";
        feedback.flxAddFeatureRequest.setVisibility(true);
        feedback.flxUserFeedback.setVisibility(false);
        this.view.forceLayout();
      }
      else{
        this.showAccountModule();		
      }
    },
    btnAckDoneAction: function() {
      this.showAccountModule();
    },
    /**
    * Navigate to Accounts Dashboard
    * @member of frmCustomerFeedbackController
    * @param {void} - None
    * @returns {void} - None
    * @throws {void} - None
    */
    showAccountModule:function(){
      var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
      accountModule.presentationController.showAccountsDashboard();		
    },
    /**
     *  Disable button.
     */
    disableButton: function (button) {
      button.setEnabled(false);
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
    },
    /**
     * Enable button.
     */
    enableButton: function (button) {
      button.setEnabled(true);
      button.skin = "sknbtnLatoffffff15px";
      button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
    },
  }
});