define(["CommonUtilities", "OLBConstants"], function(CommonUtilities, OLBConstants) {
  return {
    willUpdateUI: function(naoViewModel) {
      if (naoViewModel.showProgressBar) {
        CommonUtilities.showProgressBar(this.view);
      } else if (naoViewModel.hideProgressBar) {
        CommonUtilities.hideProgressBar(this.view);
      }
      if (naoViewModel.serverError) {
        this.showServerError(naoViewModel.serverError);
      }
      if (naoViewModel.productSelection) {
        this.showProductSelection(naoViewModel.productSelection);
      }
      if (naoViewModel.performCreditCheck) {
        this.performCreditCheck(naoViewModel.performCreditCheck);
      }
      if (naoViewModel.showAcknowledgement) {
        this.showAcknowledgement(naoViewModel.showAcknowledgement);
      }
      if (naoViewModel.sideMenu) {
        this.updateHamburgerMenu(naoViewModel.sideMenu);
    }
    if (naoViewModel.topBar) {
        this.updateTopBar(naoViewModel.topBar);
    }
    if (naoViewModel.resetForm) {
        this.hideAll();
    }
    },

     /**
         * Initialize HamburgerMenu
         * @param: sideMenuModel
         */
        updateHamburgerMenu: function (sideMenuModel) {
          this.view.customheader.initHamburger(sideMenuModel, "Accounts");
      },
      /**
       * Initialize TopBar
       * @param: topBarModel
       */
      updateTopBar: function (topBarModel) {
          this.view.customheader.initTopBar(topBarModel);
      },

      /*
         * Function to show Logout flex
         */
        showLogout: function () {
          var scopeObj = this;
          scopeObj.view.CustomPopupLogout.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
          scopeObj.view.CustomPopupLogout.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
          var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMain.frame.height + scopeObj.view.flxFooter.frame.height;
          scopeObj.view.flxLogout.height = height + "dp";
          scopeObj.view.flxLogout.left = "0%";
          this.view.CustomPopupLogout.btnYes.onClick = this.performLogout.bind(this);
          this.view.CustomPopupLogout.btnNo.onClick = function () {
              scopeObj.view.flxLogout.left = "-100%";
          };
          this.view.CustomPopupLogout.flxCross.onClick = function () {
              scopeObj.view.flxLogout.left = "-100%";
          };
      },
      /**
       * Function to perform Logout, on confirmation
       */
      performLogout: function () {
          var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
          context = {
              "action": "Logout"
          };
          authModule.presentationController.doLogout(context);
          this.view.flxLogout.left = "-100%";
      },



    /**
     * Entry Point Method for Product Selection
     * @member frmNewUserOnBoardingController
     * @param {object} viewModel
     * @returns {void} - None
     * @throws {void} - None
     */
    showProductSelection: function(productSelectionViewModel) {
      var self = this;
      // this.view.flxAccountsView.setVisibility(true);
      this.showProductSelectionUI();
      // this.showTopMenu();
      // this.view.lblWelcome.text = kony.i18n.getLocalizedString("i18n.accounts.welcome") + " " + productSelectionViewModel.username + "!";
      this.setProductSelectionData(productSelectionViewModel.products);
      this.validateProductSelection();
      this.configureTermsAndConditions();
      this.view.btnContinueProceed.onClick = this.performCreditCheck.bind(this, this.saveProducts.bind(this, productSelectionViewModel.products));
    },

    configureTermsAndConditions: function () {
      var self = this;
      CommonUtilities.setCheckboxState(false,  this.view.imgTCContentsCheckbox);      
      self.view.flxTCContentsCheckbox.onClick = function () {
        CommonUtilities.toggleCheckBox(self.view.imgTCContentsCheckbox);
        self.validateProductSelection();
      };
      self.view.CopyflxTCCheckBox0h2854eb77a214b.onClick = function () {
        CommonUtilities.toggleCheckBox(self.view.CopyimgTCContentsCheckbox0h9889054d45745);
      };
      self.view.btnTermsAndConditions.onClick = this.showTermsAndConditions.bind(this);
      self.view.btnCancel.onClick = self.hideTermsAndConditions.bind(this);
      self.view.flxClose.onClick = self.hideTermsAndConditions.bind(this);
      self.view.btnSave.onClick = function () {
        CommonUtilities.setCheckboxState(CommonUtilities.isChecked(self.view.CopyimgTCContentsCheckbox0h9889054d45745), self.view.imgTCContentsCheckbox);
        self.view.flxTermsAndConditions.setVisibility(false);        
        self.validateProductSelection();
      }
    },

    showTermsAndConditions: function () {
      CommonUtilities.setCheckboxState(CommonUtilities.isChecked(this.view.imgTCContentsCheckbox), this.view.CopyimgTCContentsCheckbox0h9889054d45745);
      this.view.flxTermsAndConditions.setVisibility(true);
      this.view.flxTermsAndConditions.setFocus(true);
      this.view.flxTermsAndConditions.height = this.getTotalFormHeight();
      this.view.forceLayout();
    },

    hideTermsAndConditions: function () {
      this.view.flxTermsAndConditions.setVisibility(false);
      this.view.forceLayout();
    },


    getTotalFormHeight: function () {
      return this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height + "dp";
    }, 




    saveProducts: function (productList) {
        var selectedProductObjects = [];
        this.view.segProductsNAO.data.forEach(function (segementDataItem, index) {
            if (segementDataItem.lblCheckBox === "C") {
                selectedProductObjects.push(productList[index])                    
            }
        })
       this.presenter.saveUserProducts(selectedProductObjects);
    },


    showProductSelectionUI: function() {
      this.hideAll();
      this.view.flxAccounts.setVisibility(false);
      this.view.flxMainContainer.setVisibility(true);
      this.view.flxAccountsView.setVisibility(true);      
      this.AdjustScreen();
    },

    /**
     * Binds Products to Product Segment
     * @member frmNewUserOnBoardingController
     * @param {object} products model objects array
     * @returns {void} - None
     * @throws {void} - None
     */
    setProductSelectionData: function(products) {
      var self = this;
      var widgetDataMap = {
        imgflxAccounts: "imgflxAccounts",
        lblAccountsName: "lblAccountsName",
        lblFeature: "lblFeature",
        flxRule1: "flxRule1",
        lblRule1: "lblRule1",
        flxRule2: "flxRule2",
        imgRule1: "imgRule1",
        imgRule2: "imgRule2",
        imgRule3: "imgRule3",
        lblRule2: "lblRule2",
        flxRule3: "flxRule3",
        lblRule3: "lblRule3",
        lblMonthlyServiceFee: "lblMonthlyServiceFee",
        lblCheckBox: "lblCheckBox",
        lblAccounts: "lblAccounts",
        btnKnowMore: "btnKnowMore",
        flxCheckBox: "flxCheckBox"
      };
      var data = products.map(function(product, index) {
        return {
          imgflxAccounts: "nuo_savings.png",
          lblAccountsName: product.productName,
          lblFeature: kony.i18n.getLocalizedString("i18n.NUO.Features"),
          "lblRule1": product.productDescription,
          "lblRule2": product.features,
          "lblRule3": product.info,
          imgRule1: "pageoffdot.png",
          imgRule2: "pageoffdot.png",
          imgRule3: "pageoffdot.png",
          lblMonthlyServiceFee: product.rates,
          btnKnowMore: {
            text: kony.i18n.getLocalizedString("i18n.NUO.KnowMore"),
            onClick: self.showProductDetail.bind(this, product, index)
          },
          lblAccounts: kony.i18n.getLocalizedString("i18n.NUO.SlectProduct"),
          lblCheckBox: "D",
          flxCheckBox: {
            onClick: self.toggleProductSelectionCheckbox.bind(this, index)
          }
        };
      });
      this.view.segProductsNAO.widgetDataMap = widgetDataMap;
      this.view.segProductsNAO.setData(data);
      this.AdjustScreen();
    },

    /**
     * Show Product Detail
     * @member frmNewUserOnBoardingController
     * @param product NewUserProduct Model Object
     * @param index of which row is shown in segment
     * @returns {void} - None
     * @throws {void} - None
     */
    showProductDetail: function(product, index) {
      this.showProductDetailUI();
      var data = [
        product.productDescription,
        product.features,
        product.info,
        product.rates,
        product.features
      ];

      var widgetDataMap = {
        lblRule1: "lblRule1",
        imgRule1: "imgRule1"
      };

      var segData = data.map(function(text) {
        return {
          lblRule1: text,
          imgRule1: "pageoffdot.png"
        };
      });
      this.view.AccountsDescription.segFeatures.setData(segData);
      this.view.AccountsDescription.imgflxAccountsDescription.src =  "nuo_savings.png";
      this.view.AccountsDescription.btnAccountsDescriptionCancel.onClick = function() {
        this.showProductSelectionUI();
      }.bind(this);
      this.view.AccountsDescription.btnAccountsDescriptionProceed.onClick = function() {
        this.toggleProductSelectionCheckbox(index);
        this.showProductSelectionUI();
      }.bind(this);
      this.AdjustScreen();
    },

    validateProductSelection: function () {
        var count = this.view.segProductsNAO.data.filter(function (item) {
            return item.lblCheckBox === "C";
        }).length;
        if (count === 0 || !CommonUtilities.isChecked(this.view.imgTCContentsCheckbox)) {
            CommonUtilities.disableButton(this.view.btnContinueProceed);            
        }
        else {
            CommonUtilities.enableButton(this.view.btnContinueProceed);            
        }
    },


    /**
     * Show Product Detail UI
     * @member frmNewUserOnBoardingController
     * @param product Product Model Object
     * @returns {void} - None
     * @throws {void} - None
     */
    showProductDetailUI: function(product) {
      this.view.flxMainContainer.setVisibility(false);
      this.view.flxAccounts.setVisibility(true);
      this.AdjustScreen();
    },

    /**
     * Toggle Select Product Checkbox of Select Products Segment
     * @member frmNewUserOnBoardingController
     * @param index index of row
     * @returns {void} - None
     * @throws {void} - None
     */
    toggleProductSelectionCheckbox: function(index) {
      var data = this.view.segProductsNAO.data[index];
      data.lblCheckBox = data.lblCheckBox === "C" ? "D" : "C";
      this.view.segProductsNAO.setDataAt(data, index);
      this.validateProductSelection();
    },

    performCreditCheck: function (onYesPressed) {
        this.view.flxSubmitApplication.setVisibility(true);
        this.view.SubmitApplication.setFocus(true);                  
        this.view.SubmitApplication.btnNo.onClick = function () {
            this.view.flxSubmitApplication.setVisibility(false);
        }.bind(this);
        this.view.SubmitApplication.btnYes.onClick = function () {
          this.view.flxSubmitApplication.setVisibility(false);
          onYesPressed();
        }.bind(this);
        this.view.flxSubmitApplication.height = this.getTotalFormHeight();
        this.view.forceLayout();
    },

    showAcknowledgement: function (acknowledgementViewModel) {
        this.showAcknowledgementUI();
        this.setSelectedProductsData(acknowledgementViewModel.selectedProducts)
        this.view.AcknowledgementNAO.btnAckFundAccounts.onClick = function () {
            var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
            accountModule.presentationController.showAccountsDashboard();
        }
    },
    setSelectedProductsData: function (selectedProducts) {
        var widgetDataMap = {
            "flxSelectedProduct2": "flxSelectedProduct2",
            "imgSelectedProduct1": "imgSelectedProduct1",
            "lblSelectedProductName1": "lblSelectedProductName1",
            "lblSelectedDescriptonName1": "lblSelectedDescriptonName1",
            "imgSelectedProduct2": "imgSelectedProduct2",
            "lblSelectedProductName2": "lblSelectedProductName2",
            "lblSelectedDescriptionName2": "lblSelectedDescriptionName2"
        };
        this.view.AcknowledgementNAO.segProducts.widgetDataMap = widgetDataMap;
        var data = [];
        for (var i = 0; i < selectedProducts.length; i = i + 2 ) {
            var productLeft = selectedProducts[i];
            var productRight = selectedProducts[i+1];
            var item = {
                "imgSelectedProduct1": "nuo_savings.png",
                "lblSelectedProductName1": productLeft.productName,
                "lblSelectedDescriptonName1": "Your Account is now active"
            }
            if (productRight) {
                item.imgSelectedProduct2 = "nuo_savings.png";
                item.lblSelectedProductName2 = productRight.productName;
                item.flxSelectedProduct2 = {
                    isVisible: true
                }
                item.lblSelectedDescriptionName2 =  "Your Account is now active"
                
            }
            data.push(item);
        }
        this.view.AcknowledgementNAO.segProducts.setData(data);

    },

    showAcknowledgementUI: function () {
        this.hideAll();
        this.view.flxNAOAcknowledgement.setVisibility(true);
        this.view.AcknowledgementNAO.btnAckGoToAccounts.setVisibility(false);
        this.view.AcknowledgementNAO.btnAckFundAccounts.text = kony.i18n.getLocalizedString("i18n.NAO.GoToAccounts");
        this.view.AcknowledgementNAO.btnAckFundAccounts.toolTip = kony.i18n.getLocalizedString("i18n.NAO.GoToAccounts");
        this.AdjustScreen();
    },

    hideAll: function() {
        this.view.flxFundAccounts.setVisibility(false);
        this.view.flxCreditCheck.setVisibility(false);
        this.view.flxNAOAcknowledgement.setVisibility(false);
        this.view.flxReviewApplication.setVisibility(false);
        this.view.flxAccountsView.setVisibility(false);
      },
    postshowfrmNAO: function() {
      this.view.AcknowledgementNAO.segProducts.setData([]);
      this.view.BannerNUO.imgBanner.src = OLBConstants.IMAGES.BANNER_IMAGE;      
      this.view.customheader.headermenu.flxMessages.setVisibility(true);
      this.setFlowActions();
      this.view.customheader.headermenu.btnLogout.onClick = this.showLogout.bind(this);
    },
    setFlowActions: function() {
      var self = this;
      this.view.SubmitApplication.flxCross.onClick = function() {
        self.view.flxSubmitApplication.setVisibility(false);
      };
      this.view.SubmitApplication.btnNo.onClick = function() {
        self.view.flxSubmitApplication.setVisibility(false);
      };
      this.view.btnContinueProceed.onClick = function() {
        self.hideAll();
        self.view.flxReviewApplication.setVisibility(true);
      };
      this.view.ReviewApplicationNAO.btnBack.onClick = this.showSelectedProductsList;
      this.view.ReviewApplicationNAO.btnReviewApplicationSubmit.onClick = function() {
        self.view.flxSubmitApplication.setVisibility(true);
        self.view.flxSubmitApplication.setFocus(true);
      };
      this.view.SubmitApplication.btnYes.onClick = function() {
        self.view.flxSubmitApplication.setVisibility(false);
        self.hideAll();
        self.view.flxNAOAcknowledgement.setVisibility(true);
      };
      this.view.FundAccountsNAO.btnFundAccountGoToAccounts.onClick = this.NavigateToAccountsLandingForm;
      this.view.AcknowledgementNAO.btnAckGoToAccounts.onClick = this.NavigateToAccountsLandingForm;
      this.view.FundAccountsNAO.btnFundAccountProceed.onClick = function() {
        var nav = new kony.mvc.Navigation("frmTransfers");
        nav.navigate();
      };
      this.view.AcknowledgementNAO.btnAckFundAccounts.onClick = function() {
        self.hideAll();
        self.view.flxFundAccounts.setVisibility(true);
      };
      this.view.FundAccountsNAO.flxFundAccountRadiobtn1.onClick = function() {
        self.RadioBtnAction(
          self.view.FundAccountsNAO.lblRadioBtn1,
          self.view.FundAccountsNAO.lblRadioBtn2,
          self.view.FundAccountsNAO.lblRadioBtn3
        );
      };
      this.view.FundAccountsNAO.flxFundAccountRadioBtn2.onClick = function() {
        self.RadioBtnAction(
          self.view.FundAccountsNAO.lblRadioBtn2,
          self.view.FundAccountsNAO.lblRadioBtn1,
          self.view.FundAccountsNAO.lblRadioBtn3
        );
      };
      this.view.FundAccountsNAO.flxFundAccountRadioBtn3.onClick = function() {
        self.RadioBtnAction(
          self.view.FundAccountsNAO.lblRadioBtn3,
          self.view.FundAccountsNAO.lblRadioBtn1,
          self.view.FundAccountsNAO.lblRadioBtn1
        );
      };
      this.view.AccountsDescription.btnAccountsDescriptionProceed.onClick = this.showSelectedProductsList;
      this.view.AccountsDescription.btnAccountsDescriptionCancel.onClick = this.showSelectedProductsList;
    },
    RadioBtnAction: function(RadioBtn1, RadioBtn2, RadioBtn3) {
      if (RadioBtn1.text === "R") {
        RadioBtn1.text = " ";
        RadioBtn1.skin = "sknC0C0C020pxNotFontIcons";
        if (RadioBtn2 != " " || RadioBtn2 !== null) {
          RadioBtn2.text = "R";
          RadioBtn2.skin = "sknLblFontTypeIcon3343e820px";
        }
        if (RadioBtn3 != " " || RadioBtn3 !== null) {
          RadioBtn3.text = "R";
          RadioBtn3.skin = "sknLblFontTypeIcon3343e820px";
        }
      } else {
        if (RadioBtn2 != " " || RadioBtn2 !== null) {
          RadioBtn2.text = " ";
          RadioBtn2.skin = "sknC0C0C020pxNotFontIcons";
        }
        if (RadioBtn3 != " " || RadioBtn3 !== null) {
          RadioBtn3.text = " ";
          RadioBtn3.skin = "sknC0C0C020pxNotFontIcons";
        }
        RadioBtn1.text = "R";
        RadioBtn1.skin = "sknLblFontTypeIcon3343e820px";
      }
    },
    NavigateToAccountsLandingForm: function() {
      var nav = new kony.mvc.Navigation("frmAccountsLanding");
      nav.navigate();
    },
   
    showSelectedProductsList: function() {
      this.hideAll();
      this.view.flxAccounts.setVisibility(false);
      this.view.flxAccountsView.setVisibility(true);
      this.view.forceLayout();
    },
    /**
     *  Disable button.
     */
    disableButton: function(button) {
      button.setEnabled(false);
      button.skin = "sknBtnBlockedLatoFFFFFF15Px";
      button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
      button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
    },
    /**
     * Enable button.
     */
    enableButton: function(button) {
      button.setEnabled(true);
      button.skin = "sknbtnLatoffffff15px";
      button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
      button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
    },
    /**
     *AdjustScreen- function to adjust the footer
     * @member of frmNAOController
     * @param {void} - None
     * @returns {void} - None
     * @throws {void} -None
     */
    AdjustScreen: function() {
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight =
        this.view.customheader.frame.height +
        this.view.flxContainer.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) this.view.flxFooter.top = mainheight + diff + "dp";
        else this.view.flxFooter.top = mainheight + "dp";
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    }
  };
});
