define(['CommonUtilities'], function (CommonUtilities) {

  return{
  
  shouldUpdateUI: function (viewModel) {
    return viewModel !== undefined && viewModel !== null;
  },

  postShowPayaPerson: function(){
    this.AdjustScreen();
    this.setFlowActions();
  },
  //UI Code
  AdjustScreen: function(data) {
    if(data == undefined || data == null || data == ""){
      data = 0;
    }else{}
    var mainheight = 0;
    var screenheight = kony.os.deviceInfo().screenHeight;
    mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
    var diff = screenheight - mainheight;
    if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.frame.height;
        if (diff > 0) 
            this.view.flxFooter.top = mainheight + diff+ 30 + data + "dp";
        else
            this.view.flxFooter.top = mainheight + 30 + data + "dp";        
     } else {
        this.view.flxFooter.top = mainheight + 30 + data + "dp";
     }
    this.view.forceLayout();
  },  
  setFlowActions: function() {
    var scopeObj = this;
    this.view.confirmation.flximgInfoIcon.onClick = function() {
        if(scopeObj.view.AllFormsConfirm.isVisible === false)
          scopeObj.view.AllFormsConfirm.isVisible = true;
        else
          scopeObj.view.AllFormsConfirm.isVisible = false;
    };
    this.view.AllFormsConfirm.flxCross.onClick = function() {
        scopeObj.view.AllFormsConfirm.isVisible = false;
    };
    this.view.AllForms.flxCross.onClick = function() {
       scopeObj.view.AllForms.isVisible = false; 
    };
    this.view.Activatep2p.flximgInfoIcon.onClick = function() {
       if(scopeObj.view.AllForms.isVisible === false) {
          scopeObj.view.AllForms.isVisible = true; 
          scopeObj.view.AllForms.top = "365dp";
          scopeObj.view.AllForms.RichTextInfo = kony.i18n.getLocalizedString("i18n.PayAperson.ActivateP2PDefaultAcntDeposit");
       }
       else
          scopeObj.view.AllForms.isVisible = false; 
    };
    this.view.Activatep2p.flximgInfoIconsend.onClick = function() {
       if(scopeObj.view.AllForms.isVisible === false) {
          scopeObj.view.AllForms.isVisible = true; 
          scopeObj.view.AllForms.top = "425dp";
          scopeObj.view.AllForms.RichTextInfo = kony.i18n.getLocalizedString("i18n.PayAperson.ActivateP2PDefaultAcntSending");}
       else
          scopeObj.view.AllForms.isVisible = false; 
    };
    
  },
  /**
     * Function to make changes to UI
     * Parameters: p2pViewModel {Object}
     */
  willUpdateUI: function (p2pViewModel) {
    var scopeObj = this;
    if (p2pViewModel.topBar) {
      this.updateTopBar(p2pViewModel.topBar);
    }
    if (p2pViewModel.sideMenu) {
      this.updateHamburgerMenu(p2pViewModel.sideMenu);
    }
    if(p2pViewModel.ProgressBar){
      if(p2pViewModel.ProgressBar.show){
        CommonUtilities.showProgressBar(this.view);
      }
      else {
        CommonUtilities.hideProgressBar(this.view);
      }
    }
    if(p2pViewModel.paymentAccounts){
        this.bindMyPaymentAccountsData(p2pViewModel.paymentAccounts);
	    this.setPayFromData(p2pViewModel.paymentAccounts);
		if(p2pViewModel.sendPayemntAccounts)
        this.setSendPayemntAccounts(p2pViewModel.sendPayemntAccounts);
    }
    if(p2pViewModel.sendOrRequest){
      CommonUtilities.hideProgressBar(this.view);
      this.setSendRequestSegmentData(p2pViewModel.sendOrRequest);
      //Sent Or Request
      scopeObj.sentOrRequestSortMap = [
          { name: 'nickName', imageFlx: scopeObj.view.tableView.imgSortName, clickContainer: scopeObj.view.tableView.flxName }
      ];
      CommonUtilities.Sorting.setSortingHandlers(scopeObj.sentOrRequestSortMap, scopeObj.onSentOrRequestSortClickHandler, scopeObj);
      CommonUtilities.Sorting.updateSortFlex(scopeObj.sentOrRequestSortMap, p2pViewModel.sortInputs);
    }
    if(p2pViewModel.showAddRecipientAck){
      this.showAcknowledgementAddReciepient(p2pViewModel.payPersonJSON,p2pViewModel.status); 
    }
    if (p2pViewModel.myRequests) {
      CommonUtilities.hideProgressBar(this.view);
      this.setMyRequestsSegmentData(p2pViewModel.myRequests);
    }
    if(p2pViewModel.managePayeesData){
      this.setManageRecipientSegmentData(p2pViewModel.managePayeesData);
      //Manage recipient
      scopeObj.manageRecipientSortMap = [
        { name: 'nickName', imageFlx: scopeObj.view.tableView.imgSortName, clickContainer: scopeObj.view.tableView.flxName }
      ];
      CommonUtilities.Sorting.setSortingHandlers(scopeObj.manageRecipientSortMap, scopeObj.onMangeRecipientSortClickHandler, scopeObj);
      CommonUtilities.Sorting.updateSortFlex(scopeObj.manageRecipientSortMap, p2pViewModel.sortInputs);
   }
   if(p2pViewModel.showRequestMoneyAck)
   {
	    CommonUtilities.hideProgressBar(this.view);
        this.showAcknowledgementRequestMoney(p2pViewModel.requestObj,p2pViewModel.status);
   }
   if(p2pViewModel.showEditRecipientAck){
    this.showAcknowledgementEditReciepient(p2pViewModel.payPersonJSON);
  }
    if(p2pViewModel.showRequestSendMoneyAck)
     {
       CommonUtilities.hideProgressBar(this.view);
       this.showAcknowledgementSendMoney(p2pViewModel.requestObj,p2pViewModel.status,p2pViewModel.accountName,p2pViewModel.accountBalance);
     }
     if(p2pViewModel.pagination){
      this.updatePaginationValue(p2pViewModel.pagination);
    }
    if(p2pViewModel.sentTransactions){
      CommonUtilities.hideProgressBar(this.view);
      this.setSentSegmentData(p2pViewModel.sentTransactions, p2pViewModel.status);
      //Sorting config
      scopeObj.sentSortMap = [
        { name: 'FirstName', imageFlx: scopeObj.view.tableView.imgSentSortTo, clickContainer: scopeObj.view.tableView.flxSentTo },
        { name: 'transactionDate', imageFlx: scopeObj.view.tableView.imgSentSortDate, clickContainer: scopeObj.view.tableView.flxSentDate },
        { name: 'amount', imageFlx: scopeObj.view.tableView.imgSentSortAmount, clickContainer: scopeObj.view.tableView.flxSentAmount }
      ];
      CommonUtilities.Sorting.setSortingHandlers(scopeObj.sentSortMap, scopeObj.onSentSortClickHandler, scopeObj);
      CommonUtilities.Sorting.updateSortFlex(scopeObj.sentSortMap, p2pViewModel.sortInputs);
  }
  if (p2pViewModel.receivedTransactions) {
            CommonUtilities.hideProgressBar(this.view);
            this.setReceivedSegmentData(p2pViewModel.receivedTransactions, p2pViewModel.status);
            //Sorting config
            scopeObj.receivedSortMap = [
              { name: 'FirstName', imageFlx: scopeObj.view.tableView.imgSentSortTo, clickContainer: scopeObj.view.tableView.flxSentTo },
              { name: 'transactionDate', imageFlx: scopeObj.view.tableView.imgSentSortDate, clickContainer: scopeObj.view.tableView.flxSentDate },
              { name: 'amount', imageFlx: scopeObj.view.tableView.imgSentSortAmount, clickContainer: scopeObj.view.tableView.flxSentAmount }
            ];
            CommonUtilities.Sorting.setSortingHandlers(scopeObj.receivedSortMap, scopeObj.onReceivedSortClickHandler, scopeObj);
            CommonUtilities.Sorting.updateSortFlex(scopeObj.receivedSortMap, p2pViewModel.sortInputs);
  }
  if(p2pViewModel.showRemainderAck)
      {
        CommonUtilities.hideProgressBar(this.view);
        this.showAcknowledgementRequestMoney(p2pViewModel.requestObj,p2pViewModel.status);
      }
	if(p2pViewModel.PayAPersonViewActivity){
      this.showPayAPersonViewActivity(p2pViewModel.PayAPersonViewActivity);
    }
    if(p2pViewModel.noRecords){
      p2pViewModel.noRecords = false;
      this.showNoRecords();
    }
	if (p2pViewModel.searchPayAPerson) {
        CommonUtilities.hideProgressBar(this.view);
        this.setSendRequestSegmentData(p2pViewModel.searchPayAPerson.payAPersonData,true);
    }
    if (p2pViewModel.sendMoneyData) {
		CommonUtilities.hideProgressBar(this.view);
        this.onSendMoneyBtnClick(p2pViewModel.sendMoneyData);
    }
	if (p2pViewModel.requestMoneyData) {
		CommonUtilities.hideProgressBar(this.view);
        this.onRequestMoneyBtnClick(p2pViewModel.requestMoneyData);
    }
    if(p2pViewModel.p2pSettings){
        CommonUtilities.hideProgressBar(this.view);
        this.showP2PSettingsScreen(p2pViewModel);
    }
    if(p2pViewModel.showDeactivateP2PAcknowledgement){
        p2pViewModel.showDeactivateP2PAcknowledgement = false;
        this.showDeactivateP2PAcknowledgement();
      }
	if(p2pViewModel.preActivation){
       p2pViewModel.preActivation = false;
      this.preActivationScreen();
    }
    if(p2pViewModel.showActivation){
		p2pViewModel.showActivation = false;
		this.showActivateP2PScreen(p2pViewModel);
	}
    if(p2pViewModel.notEligible){
		p2pViewModel.notEligible = false;
		this.showNotEligibleScreen();
	}if(p2pViewModel.addRecipient){
	    	p2pViewModel.addRecipient = false;
	    	this.showAddRecipient();
	  	}
		this.AdjustScreen();
  },
  showNoRecords: function(){
        this.view.flxNoRecipient.setVisibility(true);
  		this.view.flxRightWrapper.setVisibility(true);
        this.showView(["flxMyPaymentAccounts", "flxAddRecipientButton", "flxNoRecipient", "flxWhatElse"]);
        this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson")
        }, {
            text: "NO RECIPIENTS"
        }]);
        this.view.noRecipients.btnSendMoney.setEnabled(false);
        this.disableButton(this.view.noRecipients.btnSendMoney);
        this.disableButton(this.view.noRecipients.btnRequestMoney);
        this.view.noRecipients.btnRequestMoney.setEnabled(false);

        CommonUtilities.hideProgressBar(this.view);
        this.AdjustScreen();
        this.view.forceLayout();
    },
  preActivationScreen : function(){
    var self = this;
	  this.view.OptionsAndProceed.setVisibility(true);
    this.view.flxNoRecipient.setVisibility(false);
	  this.setP2PBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson")
    }, {
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonActivation")
    }]);

    this.view.OptionsAndProceed.lblIns1.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelineOne");
    this.view.OptionsAndProceed.lblIns2.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelineTwo");
    this.view.OptionsAndProceed.lblIns3.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelineThree");
    this.view.OptionsAndProceed.lblIns4.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelineFour");
    this.view.OptionsAndProceed.lblHeader.text = kony.i18n.getLocalizedString("i18n.p2p.PayAPerson");
    this.view.OptionsAndProceed.flxWarning.setVisibility(false);
    this.view.OptionsAndProceed.lblHeading2.text = kony.i18n.getLocalizedString("i18n.p2p.activateP2pGuidelinesTitle");
    this.view.OptionsAndProceed.flxIAgree.setVisibility(true);
    this.view.OptionsAndProceed.imgChecbox.src = "unchecked_box.png";    
    this.view.OptionsAndProceed.btnProceed.onClick = function(){
      if(self.view.OptionsAndProceed.imgChecbox.src === "unchecked_box.png"){
		    self.view.OptionsAndProceed.flxWarning.setVisibility(true);
		    self.view.OptionsAndProceed.lblWarning.text = kony.i18n.getLocalizedString("i18n.p2p.TermsAndConditionsNotEnabled");
	    }
	    else{
          		var preferences = {};
          		preferences.showActivation = true;
	            self.presenter.loadP2PSettingsScreen(preferences);
	    }
    };
    this.view.OptionsAndProceed.btnCancel.setVisibility(false);
    this.showView(["flxOptionsAndProceed"]);
    CommonUtilities.hideProgressBar(this.view);
    this.AdjustScreen();
    this.view.forceLayout();
  },
  showDeactivateP2PAcknowledgement : function(){
    var self = this;
    var message = 	{
                      "message": kony.i18n.getLocalizedString("i18n.PayPerson.deactivateAcknowledgementMessage"),
                      "image":"success_green.png",
                    };
    
	var appContextReference = kony.mvc.MDAApplication.getSharedInstance().appContext;
    
    var details = {};
      details[kony.i18n.getLocalizedString("i18n.PayPerson.yourName")] = appContextReference.userProfiledata.userFirstName + " " + kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userLastName;
      details[kony.i18n.getLocalizedString("i18n.PayPerson.registeredPhone")]= appContextReference.userProfiledata.userPhoneNumber;
      details[kony.i18n.getLocalizedString("i18n.PayPerson.registeredEmail")]= appContextReference.userProfiledata.userEmail;
      details[kony.i18n.getLocalizedString("i18n.PayPerson.defaultAccountForDeposit")] = appContextReference.userProfiledata.default_to_account_p2p;
      details[kony.i18n.getLocalizedString("i18n.PayPerson.defaultAccountForSending")] = appContextReference.userProfiledata.default_from_account_p2p;
    
    this.setP2PBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
      callback: function (){ 
        CommonUtilities.showProgressBar(self.view);
        self.presenter.showSendOrRequestSegment(); }
    }, {
      text: kony.i18n.getLocalizedString("i18n.PayPerson.deactivateAcknowledgement"),
    }]);
    
	this.view.confirmation.flxPrimaryContact.setVisibility(false);
    this.view.btnAcknowledgementThree.text = kony.i18n.getLocalizedString("i18n.CustomerFeedback.DONE");
	this.setAcknowledgementValues(message,details,{},{});
	this.view.btnAcknowledgementOne.setVisibility(false);
    this.view.btnAcknowledgementTwo.setVisibility(false);
	this.view.btnAcknowledgementThree.setVisibility(true);
    
   	this.view.btnAcknowledgementThree.onClick = function(){
      var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule');
      accountsModule.presentationController.showAccountsDashboard();
    };
	CommonUtilities.hideProgressBar(this.view);
    this.AdjustScreen();
    this.showView(["flxAcknowledgement"]);
  },
  setSendPayemntAccounts : function(accounts)
  {
    this.view.tableView.listbxFromAccount.masterData = accounts;
  },
  updatePaginationValue: function(values){
    
    this.view.tableView.lblPagination.text = (values.offset+1) + " - " + (values.offset+values.limit) + " " + values.paginationText;
    if(values.offset >=10){
      this.view.tableView.imgPaginationPrevious.src = "pagination_back_active.png";
    }
    else{
      this.view.tableView.imgPaginationPrevious.src = "pagination_back_inactive.png";
    }
    if(values.limit < 10){
      this.view.tableView.imgPaginationNext.src = "pagination_next_inactive.png";
    }
    else{
      this.view.tableView.imgPaginationNext.src = "pagination_next_active.png";
    }
  },
  showAcknowledgementEditReciepient: function (payPersonJSON,result) {
    var self = this;
    var message = 	{
                      "message": kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientAckMessage"),
                      "image":"success_green.png",
                    };
    
    var phoneText = "";
        if (payPersonJSON["phone"]) {
            phoneText = phoneText + payPersonJSON["phone"];
        }
        if (payPersonJSON["secondaryPhoneNumber"]) {
            phoneText = phoneText + "<br>" + payPersonJSON["secondaryPhoneNumber"];
                    }
        var emailText = "";
        if (payPersonJSON["email"]) {
            emailText = emailText + payPersonJSON["email"];
        }
        if (payPersonJSON["secondaryEmail"]) {
            emailText = emailText + "<br>" + payPersonJSON["secondaryEmail"];
        }
        
    
    var details = {};
      details[kony.i18n.getLocalizedString("i18n.PayPerson.recipientName")] = payPersonJSON["firstName"];
      details[kony.i18n.getLocalizedString("i18n.PayPerson.nickName")]= payPersonJSON["nickName"];
      details[kony.i18n.getLocalizedString("i18n.PayPerson.phoneNumbers")]= phoneText;
      details[kony.i18n.getLocalizedString("i18n.PayPerson.emailAddress")] = emailText;
      details[kony.i18n.getLocalizedString("i18n.PayPerson.primaryContactForSending")] = payPersonJSON["primaryContactForSending"];
    
    this.setP2PBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
      callback: function (){ 
        CommonUtilities.showProgressBar(self.view);
        self.presenter.showSendOrRequestSegment(); }
    }, {
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient"),
      callback: function(){self.presenter.getManagePayeesData(); }
    },{
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientAcknowledgement")
    }]);
	this.view.confirmation.flxPrimaryContact.setVisibility(false);
    
    this.setAcknowledgementValues(message, details, {}, {
                    "btnOne": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoney"),
                        "action": function() {
                            kony.print("Send Money  in Edit recipient");
							self.presenter.onSendMoney(payPersonJSON);
                            //self.onSendMoneyBtnClick(payPersonJSON); 
                        }
                    },
                    "btnTwo": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
                        "action": function() {
                          	kony.print("request Money  in Edit recipient");
							self.presenter.onRequestMoney(payPersonJSON);
                            //self.onRequestMoneyBtnClick(payPersonJSON); 
                        }
                    },
                    "btnThree": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.BackToManageRecipient"),
                        "action": function() {
                            self.presenter.getManagePayeesData();
                        }
                    }
                });
	this.view.btnAcknowledgementOne.setVisibility(true);
    CommonUtilities.hideProgressBar(this.view);
    this.AdjustScreen();
    this.showView(["flxAcknowledgement"]);
  },
  /**
     * setP2PBreadcrumbData - sets the breadcrumb value across the pay a person .
     * @member of {frmPayAPersonController}  
     * @param {array} an array of JSONs of breadcrums with text and callback as fields. (can send an array of length 3 at max).
     * @returns {} 
     * @throws {}
     */
	setP2PBreadcrumbData:function(breadCrumbArray){
      var self = this;
      if(kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated === true){
        this.view.breadcrumb.setBreadcrumbData(breadCrumbArray);
      }
      else{
        for(breadCrumb in breadCrumbArray){
          breadCrumbArray[breadCrumb].callback = function(){ self.presenter.NavigateToPayAPerson(); };
        }
        this.view.breadcrumb.setBreadcrumbData(breadCrumbArray);
      }
    },
  showAcknowledgementAddReciepient: function (payPersonJSON,result) {
    var self = this;
    var message =	{
        "message":kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipientSuccessMessage"),
        "image":"success_green.png",
      };
    this.setP2PBreadcrumbData([{
    text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
    callback: function (){ 
        CommonUtilities.showProgressBar(self.view);
        self.presenter.showSendOrRequestSegment(); }
    }, {
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient"),
    },{
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipientAcknowledgement")
    }]);
    this.view.btnAcknowledgementTwo.setVisibility(true);
    this.view.btnAcknowledgementOne.setVisibility(true);
    this.view.btnAcknowledgementThree.setVisibility(true); 
  	this.view.confirmation.flxPrimaryContact.setVisibility(false);
  	
    var phoneText = "";
    if (payPersonJSON["phone"]) {
        phoneText = phoneText + payPersonJSON["phone"];
    }
    if (payPersonJSON["secondaryPhoneNumber"]) {
        phoneText = phoneText + "<br>" + payPersonJSON["secondaryPhoneNumber"];
                }
    var emailText = "";
    if (payPersonJSON["email"]) {
        emailText = emailText + payPersonJSON["email"];
    }
    if (payPersonJSON["secondaryEmail"]) {
        emailText = emailText + "<br>" + payPersonJSON["secondaryEmail"];
    }
        
    
    var details = {};
    details[kony.i18n.getLocalizedString("i18n.PayPerson.recipientName")] = payPersonJSON["firstName"];
    details[kony.i18n.getLocalizedString("i18n.PayPerson.nickName")]= payPersonJSON["nickName"];
    details[kony.i18n.getLocalizedString("i18n.PayPerson.phoneNumbers")]= phoneText;
    details[kony.i18n.getLocalizedString("i18n.PayPerson.emailAddress")] = emailText;
    details[kony.i18n.getLocalizedString("i18n.PayPerson.primaryContactForSending")] = payPersonJSON["primaryContactForSending"];
    
    this.setAcknowledgementValues(message, details, {}, {
          "btnOne":{
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoney"),
            "action":function(){
              kony.print("AddRecipient btnOneNoAction");
		 	  payPersonJSON["PayPersonId"] =  result.data.PayPersonId;
			  self.presenter.onSendMoney(payPersonJSON);
              //self.onSendMoneyBtnClick(payPersonJSON); 
              kony.print("AddRecipient btnOneNoAction");
            }
          },
          "btnTwo":{
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
            "action":function(){
               payPersonJSON["PayPersonId"] =  result.data.PayPersonId;
			   self.presenter.onRequestMoney(payPersonJSON);
               //self.onRequestMoneyBtnClick(payPersonJSON); 
            }
          },
          "btnThree":{
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.AddAnotherRecipient"),
            "action":function(){
              self.showAddRecipient();
            } 
          }
        });
  this.view.btnAcknowledgementOne.setVisibility(true);
    CommonUtilities.hideProgressBar(this.view);
    this.AdjustScreen();
    this.showView(["flxAcknowledgement"]);
  },
  frmPayAPersonPreShow: function () {
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    /// start
    this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
    this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
    this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
    //// End
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
	this.view.customheader.forceCloseHamburger();

    this.view.tableView.Search.isVisible = false;
    this.view.p2pReport.isVisible = false;
    this.view.p2pActivity.isVisible = false;
    this.view.btnAddRecipient.tooltip=kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient");
    this.view.btnSendMoneyNewRecipient.tooltip=kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyToNewRecipient");
   
    this.initActions();
    this.view.forceLayout();
  },
  initActions: function () {
    var scopeObj = this;
    this.view.flxRightWrapper.setVisibility(true);
	this.view.tableView.listbxFrequency.masterData = scopeObj.presenter.listboxFrequencies();
    this.view.tableView.listbxHowLong.masterData = scopeObj.presenter.listboxForHowLong();
	this.view.tableView.btnManageRecepient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient");
    this.view.AddRecipient.btnAddPhone.onClick = this.addSecondaryPhone;
    this.view.AddRecipient.btnAddEmail.onClick = this.addSecondaryEmail;
    this.view.tableView.btnSendRequest.onClick = function () {
      CommonUtilities.showProgressBar(scopeObj.view);
      scopeObj.presenter.showSendOrRequestSegment(scopeObj.presenter);
    }
    this.view.tableView.btnMyRequests.onClick = function () {
      CommonUtilities.showProgressBar(scopeObj.view);
      scopeObj.presenter.fetchRequests(scopeObj.presenter);
    }
    this.view.tableView.btnSent.onClick = function () {
      CommonUtilities.showProgressBar(scopeObj.view);
      scopeObj.presenter.showSentSegment();
    }
    this.view.tableView.btnRecieved.onClick = function() {
            CommonUtilities.showProgressBar(scopeObj.view);
            scopeObj.presenter.showReceivedSegment();
    };
    this.view.tableView.btnManageRecepient.onClick =  this.presenter.getManagePayeesData.bind(this.presenter);
	this.view.flxAddRecipientButton.btnSendMoneyNewRecipient.onClick = this.showSendMoneyToNewRecipient;
    this.view.customheader.headermenu.btnLogout.onClick = function () {

            kony.print("btn logout pressed");
            scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
            scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
            var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxContainer.frame.height;
      		scopeObj.view.flxLogout.height = height + "dp"; 
      		scopeObj.view.flxLogout.left = "0%";
        };
        this.view.CustomPopup.btnYes.onClick = function () {
            kony.print("btn yes pressed");
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            context = {
              "action": "Logout"
            };
            authModule.presentationController.doLogout(context);
            scopeObj.view.flxLogout.left = "-100%";
        };
        this.view.CustomPopup.btnNo.onClick = function () {
            kony.print("btn no pressed");
            scopeObj.view.flxLogout.left = "-100%";
        };
        this.view.CustomPopup.flxCross.onClick = function () {
            kony.print("btn no pressed");
            scopeObj.view.flxLogout.left = "-100%";
        };
        this.view.flxWhatElse.onClick = function(){
            scopeObj.showSecondaryActions();
        };
		if (CommonUtilities.getConfiguration("canSearchP2PPersons")==="true") {
			this.view.tableView.flxSearch.setVisibility(true);
			this.view.tableView.Search.btnConfirm.onClick = this.onSearchBtnClick.bind(this);
			this.view.tableView.flxSearch.onClick = this.toggleSearchBox.bind(this);
			this.view.tableView.Search.txtSearch.onKeyUp = this.onTxtSearchKeyUp.bind(this);
			this.view.tableView.Search.flxClearBtn.onClick = this.onSearchClearBtnClick.bind(this);
			this.view.tableView.Search.txtSearch.onDone = this.onP2PSearchDone.bind(this);
		 }else{
			 this.view.tableView.flxSearch.setVisibility(false);
		 }
		 if (CommonUtilities.getConfiguration("payApersonOneTimePayment")==="true") {
			 this.view.flxAddRecipientButton.btnSendMoneyNewRecipient.setVisibility(true);
			 this.view.flxAddRecipientButton.btnSendMoneyNewRecipient.onClick = this.showSendMoneyToNewRecipient;
		 }else{
			this.view.flxAddRecipientButton.btnSendMoneyNewRecipient.setVisibility(false);		 
        }
      scopeObj.initializeWhatElseFlex();
  },
  initializeWhatElseFlex: function(){
        var scopeObj = this;
        var whatElseWidgetDataMap = {
            "lblUsers": "lblUsers",
            "lblSeparator": "lblSeparator"
        };
        var whatElseWidgetData = [
            {
                "lblUsers": kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pTitle"),
                "lblSeparator": "."
            },
            {
                "lblUsers": kony.i18n.getLocalizedString("i18n.PayAPerson.P2PSettingsTitle"),
                "lblSeparator": "."
            }
        ];
        this.view.secondaryActions.segAccountTypes.widgetDataMap = whatElseWidgetDataMap;
        this.view.secondaryActions.segAccountTypes.setData(whatElseWidgetData);
        this.view.secondaryActions.segAccountTypes.onRowClick = function(){
            var selectedIndex = scopeObj.view.secondaryActions.segAccountTypes.selectedIndex;
            var selectedKey = scopeObj.view.secondaryActions.segAccountTypes.data[selectedIndex[1]]['lblUsers'];
            if(selectedKey == kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pTitle")){
                scopeObj.showP2PDeregister();
            }else if(selectedKey == kony.i18n.getLocalizedString("i18n.PayAPerson.P2PSettingsTitle")){
                scopeObj.showP2PSettings();
            }
        };
    },
    /* Navigates to the initial screen of Pay a person deregister */
    showP2PDeregister: function() {
        var self = this;
        this.view.flxNoRecipient.setVisibility(false);
        this.view.OptionsAndProceed.setVisibility(true);
		    this.setP2PBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson")
        }, {
          text: kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonDeregister")
        }]);
        this.view.OptionsAndProceed.flxWarning.setVisibility(false);
        this.view.OptionsAndProceed.lblHeader.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pTitle");
        this.view.OptionsAndProceed.lblHeading2.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pWarning");
        this.view.OptionsAndProceed.lblIns1.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pGuidelineOne");
        this.view.OptionsAndProceed.lblIns2.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pGuidelineTwo");
        this.view.OptionsAndProceed.lblIns3.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pGuidelineThree");
        this.view.OptionsAndProceed.lblIns4.text = kony.i18n.getLocalizedString("i18n.p2p.deactivateP2pGuidelineFour");
        this.view.OptionsAndProceed.flxIAgree.setVisibility(false);

        var previousState = this.view.flxTableView.isVisible ? "flxTableView" : "flxAddRecipient";
        this.view.flxTableView.setVisibility(false);
        this.view.flxAddRecipient.setVisibility(false);
        this.view.flxRightWrapper.setVisibility(false);

        this.view.OptionsAndProceed.btnProceed.onClick = function() {
          	CommonUtilities.showProgressBar(self.view);
			self.presenter.deactivateP2P();
        };
        this.view.OptionsAndProceed.btnCancel.onClick = function() {
            self.view.flxOptionsAndProceed.setVisibility(false);
            self.view[previousState].setVisibility(true);
            self.view.flxRightWrapper.setVisibility(true);
        };

        this.view.flxOptionsAndProceed.setVisibility(true);
        this.AdjustScreen();
        this.view.forceLayout();
    },
    showP2PSettings: function(){
		var preferences = {};
      	preferences.p2pSettings = true;
        this.presenter.loadP2PSettingsScreen(preferences);
    },
  /**
   * On SentOrRequest Sort Click handler
   */
  onSentOrRequestSortClickHandler: function (event, data) {
      var scopeObj = this;
      CommonUtilities.showProgressBar(scopeObj.view);
      scopeObj.presenter.getP2PPayeesList('sendOrRequest', data);
      scopeObj.setSearchFlexVisibility(false);
  },
  /**
   * On Mange Recipient Sort Click handler
   */
  onMangeRecipientSortClickHandler: function (event, data) {
      var scopeObj = this;
      CommonUtilities.showProgressBar(scopeObj.view);
      scopeObj.presenter.getP2PPayeesList('managePayeesData', data);
      scopeObj.setSearchFlexVisibility(false);
  },
  onSentSortClickHandler: function (event, data) {
      var scopeObj = this;
      CommonUtilities.showProgressBar(scopeObj.view);
      scopeObj.presenter.fetchSentTransactions(data);
      scopeObj.setSearchFlexVisibility(false);
  },
  onReceivedSortClickHandler: function (event, data) {
      var scopeObj = this;
      CommonUtilities.showProgressBar(scopeObj.view);
      scopeObj.presenter.fetchReceivedTransactions(data);
      scopeObj.setSearchFlexVisibility(false);
  },
  onP2PSearchDone : function()
	{
		if(this.view.tableView.Search.txtSearch.text.trim()!=="")
		  this.onSearchBtnClick();
	},
  onSearchBtnClick: function() {
        var scopeObj = this;
        var searchKeyword = scopeObj.view.tableView.Search.txtSearch.text.trim();
        if (searchKeyword.length >= 0 && scopeObj.prevSearchText !== searchKeyword) {
            CommonUtilities.showProgressBar(this.view);
            scopeObj.presenter.searchPayAPerson({
                'searchKeyword': searchKeyword
            });
            scopeObj.searchView = true;
            scopeObj.prevSearchText = '';
        }
    },
    toggleSearchBox: function(searchView) {
        if(this.view.tableView.Search.isVisible){
          this.AdjustScreen(-70);
        }
        else{
          this.AdjustScreen(100);
        }
        this.setSearchFlexVisibility(!this.view.tableView.Search.isVisible);
        if (this.view.tableView.flxTabs.btnSendRequest.skin === "sknBtnAccountSummaryUnselected" || this.searchView === true) {
            CommonUtilities.showProgressBar(this.view);
            if (searchView === false) this.presenter.showSendOrRequestSegment(this.presenter);
            else this.presenter.showSendOrRequestSegment(this.presenter, this.showSearchFlex);
        }
        if (!this.view.tableView.Search.isVisible) {
            this.searchView = false;
            this.prevSearchText = '';
        }
        this.view.forceLayout();
    },
    /**
     * Set search flex visibility.
     * @Input flag {boolean} true/flase.
     * 
     */
    setSearchFlexVisibility: function(flag) {
        if (typeof flag === "boolean") {
            this.view.tableView.Search.txtSearch.placeholder = kony.i18n.getLocalizedString("i18n.transfers.searchTransferPayees");
            this.view.tableView.imgSearch.src = flag ? "selecetd_search.png" : "search_blue.png";
            this.view.tableView.Search.setVisibility(flag);
            if (flag === true) {
                this.view.tableView.Search.txtSearch.text = '';
                this.view.tableView.Search.txtSearch.setFocus(true);
                this.disableSearch();
            }
        } else {
            kony.print("Invalid input.");
        }
    },
    /**
     * Disable search 
     */
    disableSearch: function() {
        this.disableButton(this.view.tableView.Search.btnConfirm);
        this.view.tableView.Search.flxClearBtn.setVisibility(false);
    },
    /**
     * Disable search 
     */
    enableSearch: function() {
        this.enableButton(this.view.tableView.Search.btnConfirm);
        this.view.tableView.Search.flxClearBtn.setVisibility(true);
        this.view.forceLayout();
    },
    /**
     * On Search text change
     */
    onTxtSearchKeyUp: function(event) {
        var scopeObj = this;
        var searchKeyword = scopeObj.view.tableView.Search.txtSearch.text.trim();
        if (searchKeyword.length > 0) {
            scopeObj.enableSearch();
        } else {
            scopeObj.disableSearch();
        }
    },
    /**
     * PayAPerson  Search clear button click handler.
     *
     */
    onSearchClearBtnClick: function() {
        var  scopeObj  =  this;
        scopeObj.view.tableView.Search.txtSearch.text  =  "";
        if  (this.searchView  ===  true) {
            CommonUtilities.showProgressBar(this.view);
            this.presenter.showSendOrRequestSegment(this.presenter, this.showSearchFlex);
        }
        this.searchView  =  false;
    },
  updateHamburgerMenu: function (sideMenuModel) {
    this.view.customheader.initHamburger(sideMenuModel);    
  },


  updateTopBar: function (topBarModel) {
    this.view.customheader.initTopBar(topBarModel);
  },


  setSkinActive: function (obj) {
    obj.skin = "sknBtnAccountSummarySelected";
  },
  setSkinInActive: function (obj) {
    obj.skin = "sknBtnAccountSummaryUnselected";
  },
  bindMyPaymentAccountsData: function (viewModel) {
        var scopeObj = this;
        if(viewModel && viewModel.length === 0) {
            this.view.flxMyPaymentAccounts.setVisibility(false);
            this.onError("No Payment Acccounts.");
            return;
        }
        
        this.view.flxMyPaymentAccounts.setVisibility(true);
        var dataMap = {
            "lblLeft": "lblLeft",
            "lblAccountName": "lblAccountName",
            "flxLeft": "flxLeft",
            "lblBalance": "lblBalance",
            "lblAvailableBalance": "lblAvailableBalance",
            "lblHiddenAccountNumber": "lblHiddenAccountNumber"
        };
        //TODO: need standard and global confirguration
        var AccountsConfig = {
            "Checking": {
                'skin': 'sknFlx9060b7',
                'lblAvailableBalance': 'Available Balance',
                'balanceType': 'availableBalance'
            },
            "Savings": {
                'skin': 'sknFlx26D0CE',
                'lblAvailableBalance': 'Available Balance',
                'balanceType': 'availableBalance'
            },
            "CreditCard": {
                'skin': 'sknFlxF4BA22',
                'lblAvailableBalance': 'Current Balance',
                'balanceType': 'currentBalance'
            },
            "Deposit": {
                'skin': 'sknFlx4a90e2',
                'lblAvailableBalance': 'Available Balance',
                'balanceType': 'availableBalance'
            },
            "Mortgage": {
                'skin': 'sknFlx8d6429',
                'lblAvailableBalance': 'Available Balance',
                'balanceType': 'availableBalance'
            },
            "Loan": {
                'skin': 'sknFlx26D0CEOpacity20',
                'lblAvailableBalance': 'Available Balance',
                'balanceType': 'availableBalance'
            }
        };

        viewModel = viewModel.map(function (account) {
            accountConfig = AccountsConfig[account.accountType];
            return {
                "flxLeft": {
                    "skin": accountConfig.skin
                },
                "lblLeft": {
                    "text": " "
                },
                "lblAccountName": account.accountName,
                "lblBalance": scopeObj.presenter.formatCurrency(account[accountConfig.balanceType]),
                "lblAvailableBalance": accountConfig.lblAvailableBalance,
                "lblHiddenAccountNumber": account.accountID
            };
        });

        this.view.mypaymentAccounts.segMypaymentAccounts.widgetDataMap = dataMap;
        this.view.mypaymentAccounts.segMypaymentAccounts.setData(viewModel);
        this.view.forceLayout();
    },
    showNextSendRequestSegment: function(){
        CommonUtilities.showProgressBar(this.view);
        this.presenter.showNextSendOrRequestSegment();
    },
    showPreviousSendRequestSegment: function(){
        CommonUtilities.showProgressBar(this.view);
        this.presenter.showPreviousSendOrRequestSegment();
    },
	showSendMoneyToNewRecipient: function() {
        var self = this;
        this.showAddRecipient();
        this.view.AddRecipient.btnAdd.text = kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoney");
        this.view.AddRecipient.btnCancel.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
        this.view.AddRecipient.btnAdd.onClick = function() {
            var payPerson = {};
            payPerson["context"] = "sendMoneyToNewRecipient";
            self.pickRecipientValuesForSendMoney(payPerson);
        };
        this.view.AddRecipient.btnCancel.onClick = function() {
            self.presenter.showSendOrRequestSegment(self.presenter);
        };
        this.AdjustScreen();
    },
    pickRecipientValuesForSendMoney: function(payPersonJSON) {
  		var self = this;
      var payeeName = this.view.AddRecipient.tbxRecipientValue.text;
      var nickName = this.view.AddRecipient.tbxNickNameValue.text;
      payPersonJSON["firstName"] = CommonUtilities.changedataCase(payeeName);
      payPersonJSON["lastName"] = nickName;
      payPersonJSON["nickName"] = nickName;
      if (self.isValidEmail(this.view.AddRecipient.tbxEmailValue1.text) && this.view.AddRecipient.tbxEmailValue1.text != "") payPersonJSON["email"] = this.view.AddRecipient.tbxEmailValue1.text;
      if (self.isValidEmail(this.view.AddRecipient.tbxEmailValue2.text) && this.view.AddRecipient.tbxEmailValue2.text != "") payPersonJSON["secondaryEmail"] = this.view.AddRecipient.tbxEmailValue2.text;
      if (self.isValidPhone(this.view.AddRecipient.tbxPhoneValue1.text) && this.view.AddRecipient.tbxPhoneValue1.text != "") payPersonJSON["phone"] = this.view.AddRecipient.tbxPhoneValue1.text;
      if (self.isValidPhone(this.view.AddRecipient.tbxPhoneValue2.text) && this.view.AddRecipient.tbxPhoneValue2.text != "") payPersonJSON["secondaryPhoneNumber"] = this.view.AddRecipient.tbxPhoneValue2.text;
      if (!self.isValidEmail(payPersonJSON["email"]) && self.isValidEmail(payPersonJSON["secondaryEmail"])) {
          payPersonJSON["email"] = payPersonJSON["secondaryEmail"];
          payPersonJSON["secondaryEmail"] = undefined;
      }
      if (!self.isValidPhone(payPersonJSON["phone"]) && self.isValidPhone(payPersonJSON["secondaryPhoneNumber"])) {
          payPersonJSON["phone"] = payPersonJSON["secondaryPhoneNumber"];
          payPersonJSON["secondaryPhoneNumber"] = undefined;
      }
       this.view.flxAddRecipient.setVisibility(false);
      this.onSendMoneyBtnClick(payPersonJSON);
    },
  setSendRequestSegmentData: function(data,paginationStatus) {
	var scopeObj =this;
	this.view.customheader.customhamburger.activateMenu("PayAPerson", "SendOrRequest");
    if(data.length !==0)
	{
		kony.print("in setSendRequestSegmentData");
    this.showTableView();
    this.setP2PBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
              CommonUtilities.showProgressBar(scopeObj.view);
              scopeObj.presenter.showSendOrRequestSegment();
            }
          }, {
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendRequestMoney")
    }]);

    this.view.flxRightWrapper.setVisibility(true);
    this.view.p2pActivity.setVisibility(false);
    this.view.tableView.setVisibility(true);
	  this.view.tableView.flxTableHeaders.setVisibility(true);
    this.view.tableView.flxTableHeaders.flxSendRequestWrapper.setVisibility(true);
    this.view.tableView.flxTableHeaders.flxSentWrapper.setVisibility(false);
    this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
    this.setSkinActive(this.view.tableView.flxTabs.btnSendRequest);
    this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
    this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
    this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
    this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
    this.view.tableView.flxPaginationNext.onClick = this.showNextSendRequestSegment.bind(this);
    this.view.tableView.flxPaginationPrevious.onClick = this.showPreviousSendRequestSegment.bind(this);
    this.view.tableView.flxPagination.setVisibility(true);
	if(paginationStatus)
		this.view.tableView.flxPagination.setVisibility(false);
	else
		this.view.tableView.flxPagination.setVisibility(true);
	
    var dataMap = {
      "btnRequest": "btnRequest",
      "btnSend": "btnSend",
      "flxColumn1": "flxColumn1",
      "flxColumn2": "flxColumn2",
      "flxColumn3": "flxColumn3",
      "flxDetails": "flxDetails",
      "flxDropdown": "flxDropdown",
      "flxIdentifier": "flxIdentifier",
      "flxName": "flxName",
      "flxPrimaryContact": "flxPrimaryContact",
      "flxRequestAction": "flxRequestAction",
      "flxRow": "flxRow",
      "flxSelectedRowWrapper": "flxSelectedRowWrapper",
      "flxSendAction": "flxSendAction",
      "flxSendRequest": "flxSendRequest",
      "flxSendRequestSelected": "flxSendRequestSelected",
      "flxSeperator": "flxSeperator",
      "imgDropdown": "imgDropdown",
      "lblFullName": "lblFullName",
      "lblFullName2": "lblFullName2",
      "lblName": "lblName",
      "lblPrimaryContact": "lblPrimaryContact",
      "lblRegisteredEmail1": "lblRegisteredEmail1",
      "lblRegisteredEmail2": "lblRegisteredEmail2",
      "lblRegisteredEmails": "lblRegisteredEmails",
      "lblRegisteredPhone": "lblRegisteredPhone",
      "lblRegisteredPhone1": "lblRegisteredPhone1",
      "lblRegisteredPhone2": "lblRegisteredPhone2",
      "lblSeparator": "lblSeparator",
      "lblSeparatorSelected": "lblSeparatorSelected",
      "imgRowSelected": "imgRowSelected"
    };
    var viewModel = data.map(function (dataItem) {
      return {
        "btnRequest": {
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Request"),
            "onClick": scopeObj.presenter.onRequestMoney.bind(scopeObj.presenter, dataItem)
          },
          "btnSend": {
            "text":kony.i18n.getLocalizedString("i18n.PayAPerson.Send"),
            "onClick" :  scopeObj.presenter.onSendMoney.bind(scopeObj.presenter, dataItem)
          },
        "imgDropdown": "arrow_down.png",
        "lblFullName": "Full Name:",
        "lblFullName2": dataItem.firstName,
        "lblName": dataItem.nickName ?  dataItem.nickName : dataItem.firstName,
        "lblPrimaryContact": dataItem.primaryContactForSending,
        "lblRegisteredEmail1": dataItem.email? dataItem.email : " ",
        "lblRegisteredEmail2": dataItem.secondaryEmail ? dataItem.secondaryEmail : " ",
        "lblRegisteredEmails": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredEmail"),
        "lblRegisteredPhone": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredPhone"),
        "lblRegisteredPhone1": dataItem.phone? dataItem.phone : " ",
        "lblRegisteredPhone2": dataItem.secondaryPhoneNumber2 ? dataItem.secondaryPhoneNumber2 : " ",
        "template": "flxSendRequest",
        "lblSeparator": ".",
        "lblSeparatorSelected": "lblSeparatorSelected",
        "imgRowSelected": "arrow_up.png"

      };
    });
    this.view.tableView.segP2P.widgetDataMap = dataMap;
    this.view.tableView.segP2P.setData(viewModel);
	  this.view.tableView.flxNoTransactions.setVisibility(false);
    this.showView(["flxTableView", "flxMyPaymentAccounts","flxWhatElse","flxAddRecipientButton"]);
	}else
	{
		 this.showNoRecipient();
	}
	CommonUtilities.hideProgressBar(this.view);
    this.view.forceLayout();
  },
  showNoRecipient: function () {
			this.view.tableView.flxPagination.setVisibility(false);
			this.view.tableView.flxNoTransactions.setVisibility(true);
			this.view.tableView.flxNoTransactions.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.NoRecords");
      this.showView(["flxTableView", "flxMyPaymentAccounts","flxWhatElse","flxAddRecipientButton"]);
			this.view.tableView.segP2P.setVisibility(false);
      this.AdjustScreen();
			this.view.forceLayout();
  },
   setPayFromData: function (data) {
    this.view.tableView.listbxDetails.masterData = this.generatePayFromData(data);
    this.view.tableView.listbxDetails.selectedKey = this.fromAccountNumber ? this.fromAccountNumber : this.view.tableView.listbxDetails.masterData[0][0];
  },
  generatePayFromData: function (data) {
    var list = [];
    this.accountinfo = [];
    for (var i = 0; i < data.length; i++) {
      var tempList = [];
      tempList.push(data[i].accountID);
      tempList.push(data[i].accountName);
      list.push(tempList);
    }
    for (i = 0; i < data.length; i++) {
      var temp = [];
      temp.push(data[i].accountID);
      temp.push(data[i].availableBalance);
      this.accountinfo.push(temp);
    }
    return list;
  },
  disableButton : function (button) {
    button.setEnabled(false);
    button.skin = "sknBtnBlockedLatoFFFFFF15Px";
    button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
    button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";     
  },
  enableButton : function(button){
    button.setEnabled(true);   
    button.skin = "sknbtnLatoffffff15px";
    button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
    button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";    
  },
  checkIfAllAreFilled:function(){
    if (this.view.tableView.tbxRequestMoneyAmount.text) {
      this.enableButton(this.view.tableView.btnRequestMoneySendRequest);
    }
    else {
      this.disableButton(this.view.tableView.btnRequestMoneySendRequest);
    }   
  },
  checkIfAllAreFilledInSendMoney :function()
  {
    if (this.view.tableView.tbxAmount.text) {
      this.enableButton(this.view.tableView.btnSendMoney);
    }
    else {
      this.disableButton(this.view.tableView.btnSendMoney);
    }  
  },
  onCancelRequestMoneyBtnClick: function(dataItem) {
        var self = this;
        var cancelObject = {
            "transactionId": dataItem.transactionId
        };
		self.view.CustomPopup1.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Quit");
		self.view.CustomPopup1.lblPopupMessage.text = kony.i18n.getLocalizedString("I18n.billPay.QuitTransactionMsg");
		self.showQuitScreen({
            "negative":function(){
              self.closeQuitScreen();
            },
            "positive": function(){
              self.closeQuitScreen();
			  CommonUtilities.showProgressBar(self.view);
              self.presenter.CancelRequestMoney(self.presenter, cancelObject);
            }
        });
        
    },
  onRemainderRequestMoneyBtnClick: function(dataItem) {
        var self = this;

        function cancelRemainder() {
            CommonUtilities.showProgressBar(self.view);
            self.presenter.fetchRequests(self.presenter);
        }

        function sendRemainder() {
            var constructRemainderObj = {
                "transactionId": dataItem.transactionId,
                "p2pRequiredDate": self.view.tableView.clndrRequiredDate.date,
                "transactionsNotes": self.view.tableView.tbxSendReminderOptional.text,
                "From":   "",
                "To":     dataItem.payPersonName,
                "amount": self.presenter.formatCurrency(dataItem.amount),
                "Note":  self.view.tableView.tbxSendReminderOptional.text,
				"p2pContact" : dataItem.p2pContact
            };
            self.presenter.sendRemainder(constructRemainderObj);
        }
        this.view.tableView.tbxPreviouslyReqDate.text = self.presenter.getFormattedDateString(dataItem.p2pRequiredDate);
        this.view.tableView.tbxRequestedFor.text = dataItem.amount;
        this.view.tableView.lblReqMoneyValue.text = dataItem.payPersonName;
        var emailMasterDrop = [];
        if (dataItem.p2pContact !== null) {
            var data = [];
            data.push(0);
            data.push(dataItem.p2pContact);
            emailMasterDrop.push(data);
        }
        this.view.tableView.listbxReqSentTo.masterData = emailMasterDrop;
        this.view.tableView.tbxSendReminderOptional.text = "";
        this.view.tableView.tbxPreviouslyReqDate.setEnabled(false);
        this.view.tableView.tbxRequestedFor.setEnabled(false);
        this.view.tableView.listbxReqSentTo.setEnabled(false);
       // this.view.tableView.clndrRequiredDate.date = kony.os.date("dd/mm/yyyy");
	   var dateFormate=CommonUtilities.getConfiguration('frontendDateFormat');
	   this.view.tableView.clndrRequiredDate.dateFormat=dateFormate;
	   CommonUtilities.disableOldDaySelection(this.view.tableView.clndrRequiredDate);
        this.view.tableView.btnSendReminderCancel.onClick = cancelRemainder;
        this.view.tableView.btnSendReminder.onClick = sendRemainder;
        self.showSendReminder();
        this.view.forceLayout();
    },

  onRequestMoneyBtnClick: function(dataItem) {
    var self = this;
    this.setSkinActive(this.view.tableView.flxTabs.btnSendRequest);
    this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
    this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
    this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
    this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
    this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");

	this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");
            this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
        }]);

    self.requestedData = dataItem;
    function confirmRequestMoney() {
      var formattedRequestMoneyPayment = {
        "From": self.view.tableView.listbxDetails.selectedkeyvalue[1],
        "To": self.view.tableView.lblRequestMoneyPayToValue.text,
        "Amount": self.presenter.formatCurrency((self.view.tableView.tbxRequestMoneyAmount.text+"").replace(/,/g,"")),
        "Date": self.view.tableView.clndrRequestMoneyRequiredDate.date,
        "Note": self.view.tableView.tbxRequestMoneyOptional.text
      };

	  var totalPayementMoney = self.view.tableView.tbxRequestMoneyAmount.text.replace(/,/g,"");
      var formtaedDate = CommonUtilities.sendDateToBackend(self.view.tableView.clndrRequestMoneyRequiredDate.date,CommonUtilities.getConfiguration('frontendDateFormat'),CommonUtilities.getConfiguration('backendDateFormat'));
      var constractutedRequestMoneyPayment = {
        "From": self.view.tableView.listbxDetails.selectedkeyvalue[1],
        "To": self.view.tableView.lblRequestMoneyPayToValue.text,
        "Amount": self.view.tableView.tbxRequestMoneyAmount.text,
        "Date": self.view.tableView.clndrRequestMoneyRequiredDate.date,
        "Note": self.view.tableView.tbxRequestMoneyOptional.text,
        "fromAccountNumber": self.view.tableView.listbxDetails.selectedkeyvalue[0],
        "p2pRequiredDate" : formtaedDate,
        "p2pContact": self.view.tableView.listbxRequestMoneySentTo.selectedkeyvalue[1],
        "personId": self.requestedData.PayPersonId,
        "amount" : totalPayementMoney,
        "transactionsNotes" : self.view.tableView.tbxRequestMoneyOptional.text
      };
      var scopeObj = this;
      var actions = {
        "cancel":function () {
          self.showQuitScreen({
            "negative":function(){
              self.closeQuitScreen();
            },
            "positive": function(){
              self.closeQuitScreen();
			  CommonUtilities.showProgressBar(self.view);
              self.presenter.showSendOrRequestSegment(self.presenter);
            }
          });
        },
        "modify":function () {
          self.view.flxConfirmation.setVisibility(false);
          self.view.flxTableView.setVisibility(true);
          self.view.flxMyPaymentAccounts.setVisibility(true);
          self.view.flxAddRecipientButton.setVisibility(true);
          self.view.flxWhatElse.setVisibility(true);
          self.view.forceLayout();
        },
        "confirm":function () {
          self.presenter.createRequestMoney(constractutedRequestMoneyPayment);
        },

      };
	  self.presenter.createRequestMoney(constractutedRequestMoneyPayment);
      //self.setConfirmationValues(formattedRequestMoneyPayment,actions,extraData);
      // self.showConfirmationSendMoney();
      // self.view.flxConfirmation.setVisibility(true);
      // self.view.tableView.flxRequestMoney.setVisibility(false);
    }
    if (dataItem.Amount !== null && dataItem.Amount !== "") {
      self.disableButton(this.view.tableView.btnRequestMoneySendRequest);
    } else {
      self.enableButton(this.view.tableView.btnRequestMoneySendRequest);
    }
    //self.disableButton(this.view.tableView.btnRequestMoneySendRequest);
    this.view.tableView.segP2P.setVisibility(false);
    this.view.tableView.tbxRequestMoneyAmount.text = "";
    this.view.tableView.tbxRequestMoneyOptional.text = "";
    //this.view.tableView.tbxAmount.text ="";
    var fullName = dataItem.firstName + " " + dataItem.lastName;
    //this.view.tableView.flxSendMoney.lblPaytoValue.text = fullName;
    this.view.tableView.lblRequestMoneyPayToValue.text = fullName;
    var emailMasterDrop = [];
    var sendRecep = self.checkRequriedValues(dataItem);
		for(i=0;i<sendRecep.length;i++)
		{
			if(sendRecep[i] !== null)
			{
				var data = [];
				data.push(i);
				data.push(sendRecep[i]);
				if(sendRecep[i] !== undefined)
				emailMasterDrop.push(data);		
			}		
		}

    function callOnKeyUp() {
      self.checkIfAllAreFilled();
    }

    function cancelRequestMoney() {
	  CommonUtilities.showProgressBar(self.view);
      self.presenter.showSendOrRequestSegment(self.presenter);
    }
    //this.view.tableView.flxSendMoney.flxSentBy.listbxSentby.masterData = emailMasterDrop;
   // this.view.tableView.clndrRequestMoneyRequiredDate.date = dataItem.sendOn ? dataItem.sendOn : kony.os.date("dd/MM/yyyy");
    this.view.tableView.clndrRequestMoneyRequiredDate.dateFormat="mm/dd/yyyy";
	CommonUtilities.disableOldDaySelection(this.view.tableView.clndrRequestMoneyRequiredDate,dataItem.sendOn);
    this.view.tableView.listbxRequestMoneySentTo.masterData = emailMasterDrop;
	this.view.tableView.listbxRequestMoneySentTo.selectedKey = this.view.tableView.listbxRequestMoneySentTo.masterData[0][0];
    this.view.tableView.tbxRequestMoneyAmount.onKeyUp = callOnKeyUp;
   this.view.tableView.tbxRequestMoneyAmount.onTextChange = function(){
        self.view.tableView.tbxRequestMoneyAmount.text = self.presenter.formatCurrency(self.view.tableView.tbxRequestMoneyAmount.text,true);
    };
    this.view.tableView.btnRequestMoneyCancel.onClick = cancelRequestMoney;
    this.view.tableView.btnRequestMoneySendRequest.onClick = confirmRequestMoney;
    this.view.tableView.flxRequestMoney.setVisibility(true);
	this.view.flxAcknowledgement.setVisibility(false);
    this.view.flxTableView.setVisibility(true);
    this.view.tableView.flxTableHeaders.setVisibility(false);
    this.view.flxRightWrapper.setVisibility(true);
    this.view.tableView.flxSendMoney.setVisibility(false);
    this.view.tableView.segP2P.setVisibility(false);
	this.view.flxAddRecipientButton.setVisibility(true);
    this.view.flxWhatElse.setVisibility(true);
    this.view.flxMyPaymentAccounts.setVisibility(true);
    this.view.tableView.flxPagination.setVisibility(false);
	this.view.confirmation.flxCheckBox.onclick = self.termsAndConditionsonsClick;
	this.view.confirmation.imgCheckBox.src = "unchecked_box.png";	
	this.disableButton(this.view.confirmation.confirmButtons.btnConfirm);
	this.view.tableView.Search.setVisibility(false);
    this.view.forceLayout();
  },
  termsAndConditionsonsClick : function()
	{
		var self = this;
		if(self.view.confirmation.imgCheckBox.src == "unchecked_box.png")
			{
			self.view.confirmation.imgCheckBox.src = "checked_box.png";	
			self.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
			}else
			{
			self.view.confirmation.imgCheckBox.src = "unchecked_box.png";	
			self.disableButton(self.view.confirmation.confirmButtons.btnConfirm);
			}
		self.view.forceLayout();
   },
  onSendMoneyBtnClick: function(dataItem) {
        var self = this;
        var scopeObj = this;
        this.setSkinActive(this.view.tableView.flxTabs.btnSendRequest);
        this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
        this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
        this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
        this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
        this.view.flxOptionsAndProceed.setVisibility(false);
        this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");
				
		this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");
            this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoney"),
        }]);

        self.sendPayeeObj = dataItem;

        function callOnKeyUp() {
            self.checkValidityP2PTransferForm();
        }

        function cancelRequestMoney() {
			CommonUtilities.showProgressBar(self.view);
            self.presenter.showSendOrRequestSegment(self.presenter);
        }

        function sendMoneyToPerson() {
        //var self = this; 
        self.view.flxBottom.setVisibility(false);
        self.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoney"),
        },{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyConfirmation"),
        }]);


            var formattedRequestMoneyPayment = {
                "From": self.view.tableView.listbxFromAccount.selectedkeyvalue[1],
                "To": self.view.tableView.lblPaytoValue.text,
                "Amount": self.presenter.formatCurrency((self.view.tableView.tbxAmount.text+"").replace(/,/g,"")),
                "Date": self.view.tableView.clndrSendOn.date,
                "Notes": self.view.tableView.tbxOptional.text,
                "Frequency": self.view.tableView.listbxFrequency.selectedkeyvalue[1]
            };
            var totalPayementMoney = self.view.tableView.tbxAmount.text;
            totalPayementMoney = totalPayementMoney.toString().replace(/,/g,"");
			var extraData;
          	 if (CommonUtilities.getConfiguration('serviceFeeFlag') === "true") {
				
				if ( dataItem.statusDescription != null && dataItem.statusDescription != "Successful") {
					 extraData = {};
					 self.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
				}else{
					var fee = CommonUtilities.getConfiguration('p2pServiceFee');
					extraData = {
                    "Fees": self.presenter.formatCurrency(fee),
                    "Total": self.presenter.formatCurrency((Number(totalPayementMoney) + Number(fee)))
                };
                totalPayementMoney = (Number(totalPayementMoney) + Number(fee));
				}
                
            } else {
                extraData = {};
				self.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
            }
            var constractutedSendMoneyPayment = {
                "From": self.view.tableView.listbxFromAccount.selectedkeyvalue[1],
                "To": self.view.tableView.lblPaytoValue.text,
                "Amount": self.view.tableView.tbxAmount.text,
                "Date": self.view.tableView.clndrSendOn.date,
                "Note": self.view.tableView.tbxOptional.text,
                "fromAccountNumber": self.view.tableView.listbxFromAccount.selectedkeyvalue[0],
                "frequencyType": self.view.tableView.listbxFrequency.selectedkeyvalue[1],
                "p2pContact": self.view.tableView.listbxSentby.selectedkeyvalue[1],
                "personId": self.sendPayeeObj.PayPersonId ? self.sendPayeeObj.PayPersonId : self.sendPayeeObj.personId,
                "amount": totalPayementMoney,
                "scheduledDate": self.view.tableView.clndrSendOn.date,
                "transactionsNotes": self.view.tableView.tbxOptional.text,
                "numberOfRecurrences": self.view.tableView.tbxNoOfRecurrences.text,
                "frequencyStartDate": self.view.tableView.clndrSendOn.date,
                "frequencyEndDate": self.view.tableView.flxCalenderEndingOn.clndrEndingOn.date,
                "hasHowLong": self.view.tableView.listbxHowLong.selectedKey,
				"context" : self.sendPayeeObj.context ?  self.sendPayeeObj.context : "",
				"isScheduled": "0"
            };
			if( dataItem.statusDescription != null && dataItem.statusDescription != "Successful")
			{
				constractutedSendMoneyPayment.transactionId = dataItem.transactionId;	
			}	
			if(dataItem.context != "")
			{
				constractutedSendMoneyPayment.payPersonObject = self.sendPayeeObj;
			}
            var scopeObj = this;
			self.view.CustomPopup1.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Quit");
			self.view.CustomPopup1.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.CancelAlert");
            var actions = {
                "cancel": function() {
                    self.showQuitScreen({
                        "negative": function() {
                            self.closeQuitScreen();
                        },
                        "positive": function() {
                            self.closeQuitScreen();
							CommonUtilities.showProgressBar(self.view);
                            self.presenter.showSendOrRequestSegment(self.presenter);
                        }
                    });
                },
                "modify": function() {
                    self.view.flxConfirmation.setVisibility(false);
                    self.view.flxTableView.setVisibility(true);
                    self.view.flxMyPaymentAccounts.setVisibility(true);
                    self.view.flxAddRecipientButton.setVisibility(true);
                    self.view.flxWhatElse.setVisibility(true);
                    self.view.forceLayout();
                },
                "confirm": function() {
                    self.presenter.createP2PSendMoney(constractutedSendMoneyPayment);
                    //self.presenter.createRequestMoney(constractutedRequestMoneyPayment);
                    //kony.print(constractutedRequestMoneyPayment);
                },
            };
            self.setConfirmationValues(formattedRequestMoneyPayment, actions, extraData);
        }
        this.view.tableView.segP2P.setVisibility(false);
        self.setSendMoneyUI(dataItem);
        //self.makeLayoutfrequencyOnce();
        this.view.tableView.flxTableHeaders.setVisibility(false);
        this.view.tableView.flxSendMoney.setVisibility(true);
        this.view.tableView.segP2P.setVisibility(false);
        this.view.flxAcknowledgement.setVisibility(false);
    	this.view.flxTableView.setVisibility(true);
        this.view.flxAddRecipientButton.setVisibility(true);
        this.view.flxWhatElse.setVisibility(true);
        this.view.flxMyPaymentAccounts.setVisibility(true);
    	this.view.flxRightWrapper.setVisibility(true);
        this.view.tableView.flxPagination.setVisibility(false);
        this.view.tableView.flxRequestMoney.setVisibility(false);
		this.view.tableView.Search.setVisibility(false);
        this.view.tableView.tbxAmount.onKeyUp = callOnKeyUp;
        this.view.tableView.tbxAmount.onTextChange = function(){
            self.view.tableView.tbxAmount.text = self.presenter.formatCurrency(self.view.tableView.tbxAmount.text,true);
        };
		this.view.tableView.tbxNoOfRecurrences.onKeyUp = callOnKeyUp;
        //this.view.tableView.btnCancel.onClick = cancelRequestMoney;
        this.view.tableView.listbxFrequency.onSelection = this.onFrequencyChanged.bind(this);
        this.view.tableView.listbxHowLong.onSelection = this.onHowLongChange.bind(this);
        this.view.tableView.btnSendMoney.onClick = sendMoneyToPerson;
        this.view.tableView.btnCancel.onClick = dataItem.onCancel || cancelRequestMoney;
		this.view.confirmation.flxCheckBox.onclick = self.termsAndConditionsonsClick;
		this.view.confirmation.imgCheckBox.src = "unchecked_box.png";	
		this.disableButton(this.view.confirmation.confirmButtons.btnConfirm);
    this.AdjustScreen();
        this.view.forceLayout();
    },
   onFrequencyChanged: function () {
    var self = this;
    self.getFrequencyAndFormLayout(this.view.tableView.listbxFrequency.selectedKey,
                                   this.view.tableView.listbxHowLong.selectedKey);
     self.checkValidityP2PTransferForm();
  },
  onHowLongChange: function () {
    var self = this;
    self.getForHowLongandFormLayout(this.view.tableView.listbxHowLong.selectedKey);
    self.checkValidityP2PTransferForm();
  },
  checkValidityP2PTransferForm: function() {
    var self = this;
    var re = new RegExp("^([0-9])+(\.[0-9]{1,2})?$");
    var disableCreateP2PButton = function() {
        self.disableButton(this.view.tableView.btnSendMoney);
    }.bind(this);
    if (this.view.tableView.tbxAmount.text === null || this.view.tableView.tbxAmount.text === "" || isNaN(this.view.tableView.tbxAmount.text.replace(/,/g, "")) || !re.test(this.view.tableView.tbxAmount.text.replace(/,/g, "")) || (parseFloat(this.view.tableView.tbxAmount.text.replace(/,/g, "")) <= 0)) {
        disableCreateP2PButton();
        return;
    }
    if (this.view.tableView.listbxFrequency.selectedKey !== "Once" && this.view.tableView.listbxHowLong.selectedKey === "NO_OF_RECURRENCES" && this.view.tableView.tbxNoOfRecurrences.text === "") {
        disableCreateP2PButton();
        return;
    }
    if (isNaN(this.view.tableView.tbxNoOfRecurrences.text) || parseInt(this.view.tableView.tbxNoOfRecurrences.text) <= 0) {
        disableCreateP2PButton();
        return;
    }
    self.enableButton(this.view.tableView.btnSendMoney);
},
    getFrequencyAndFormLayout: function(frequencyValue, howLangValue) {
        if (frequencyValue !== "Once" && howLangValue !== 'NO_OF_RECURRENCES') {
            this.makeLayoutfrequencyWeeklyDate();
        } else if (frequencyValue !== "Once" && howLangValue === 'NO_OF_RECURRENCES') {
            this.makeLayoutfrequencyWeeklyRecurrences();
        } else {
            this.makeLayoutfrequencyOnce();
        }
    },
    getForHowLongandFormLayout: function(value) {
        if (value === "DATE_RANGE") {
            this.makeLayoutfrequencyWeeklyDate();
        } else if (value === "NO_OF_RECURRENCES") {
            this.makeLayoutfrequencyWeeklyRecurrences();
        } else if (value === "CONTINUE_UNTIL_CANCEL") {
            this.makeLayoutfrequencyWeeklyCancel();
        }
    },
    makeLayoutfrequencyWeeklyDate: function() {
        this.view.tableView.lblHowLong.setVisibility(true);
        this.view.tableView.listbxHowLong.setVisibility(true);
		//this.view.tableView.flxCalender.setVisibility(true);
        this.view.tableView.lblNoOfRecurrences.text = kony.i18n.getLocalizedString("i18n.transfers.lblendingon");
        this.view.tableView.flxCalenderEndingOn.setVisibility(true);
		this.view.tableView.lblNoOfRecurrences.setVisibility(true);
		this.view.tableView.tbxNoOfRecurrences.setVisibility(false);
       // this.view.flxCalEndingOn.setVisibility(true);
       // this.view.lblNoOfRecOrEndingOn.text = kony.i18n.getLocalizedString("i18n.transfers.lblendingon");
       // this.view.lblNoOfRecOrEndingOn.setVisibility(true);
        this.view.forceLayout();
    },
    makeLayoutfrequencyWeeklyRecurrences: function() {
		this.view.tableView.lblHowLong.setVisibility(true);
        this.view.tableView.listbxHowLong.setVisibility(true);
        this.view.tableView.flxCalenderEndingOn.setVisibility(false);
		this.view.tableView.lblNoOfRecurrences.setVisibility(true);
		this.view.tableView.tbxNoOfRecurrences.setVisibility(true);
        this.view.tableView.lblNoOfRecurrences.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
       /*  this.view.lblForhowLong.setVisibility(true);
        this.view.lbxForHowLong.setVisibility(true);
        this.view.flxCalEndingOn.setVisibility(false);
        this.view.lblNoOfRecOrEndingOn.text = kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
        this.view.lblNoOfRecOrEndingOn.setVisibility(true);
        this.view.tbxNoOfRecurrences.setVisibility(true);
        this.view.forceLayout(); */
    },
    makeLayoutfrequencyWeeklyCancel: function() {
       /*  this.view.lblForhowLong.setVisibility(true);
        this.view.lbxForHowLong.setVisibility(true);
        this.view.flxCalEndingOn.setVisibility(false);
        this.view.lblNoOfRecOrEndingOn.setVisibility(false);
        this.view.tbxNoOfRecurrences.setVisibility(false);
        this.view.forceLayout(); */
    },
    makeLayoutfrequencyOnce: function() {
		this.view.tableView.lblHowLong.setVisibility(false);
        this.view.tableView.listbxHowLong.setVisibility(false);
        //this.view.flxCalEndingOn.setVisibility(false);
		this.view.tableView.lblNoOfRecurrences.setVisibility(false);
		this.view.tableView.tbxNoOfRecurrences.setVisibility(false);
		this.view.tableView.flxCalenderEndingOn.setVisibility(false);
        this.view.forceLayout();
    },
	
    checkRequriedValues : function(dataItem) {
		 return [dataItem.email, dataItem.secondaryEmail, dataItem.secondaryEmail2, dataItem.phone, dataItem.secondaryPhoneNumber, dataItem.secondaryPhoneNumber2,dataItem.payPersonPhone,dataItem.payPersonEmail];
	},
	
   setSendMoneyUI: function(dataItem) {		
        var self = this;
		var dateFormate = CommonUtilities.getConfiguration('frontendDateFormat');
            if (dataItem.statusDescription != "Successful") {
			    this.view.tableView.clndrSendOn.dateFormat = dateFormate;
                if (dataItem.scheduledDate) 
				{
				dataItem.scheduledDate = CommonUtilities.getFrontendDateString(dataItem.scheduledDate);
				this.view.tableView.clndrSendOn.date=dataItem.scheduledDate;
                CommonUtilities.disableOldDaySelection(this.view.tableView.clndrSendOn, dataItem.scheduledDate);
				}else{
				    this.view.tableView.clndrSendOn.date=kony.os.date(dateFormate);
					CommonUtilities.disableOldDaySelection(this.view.tableView.clndrSendOn, dataItem.scheduledDate);
				}

            } else {
                if (dataItem.frequencyStartDate) dataItem.frequencyStartDate = CommonUtilities.getFrontendDateString(dataItem.frequencyStartDate);
                this.view.tableView.clndrSendOn.dateFormat = dateFormate;
                CommonUtilities.disableOldDaySelection(this.view.tableView.clndrSendOn, dataItem.frequencyStartDate);
            }       
			if (dataItem.frequencyEndDate){ 
		    dataItem.frequencyEndDate = CommonUtilities.getFrontendDateString(dataItem.frequencyEndDate);
            this.view.tableView.flxCalenderEndingOn.clndrEndingOn.dateFormat = dateFormate;
            CommonUtilities.disableOldDaySelection(this.view.tableView.flxCalenderEndingOn.clndrEndingOn, dataItem.scheduledDate);
			}else{
				this.view.tableView.flxCalenderEndingOn.clndrEndingOn.dateFormat = dateFormate;
				this.view.tableView.flxCalenderEndingOn.clndrEndingOn.date=kony.os.date(dateFormate);
				CommonUtilities.disableOldDaySelection(this.view.tableView.flxCalenderEndingOn.clndrEndingOn);
			}
        this.view.tableView.tbxOptional.text = dataItem.transactionsNotes ? dataItem.transactionsNotes : "";
        this.view.tableView.tbxAmount.text = dataItem.amount ? Math.abs(dataItem.amount) : "";
        this.view.tableView.tbxNoOfRecurrences.text = dataItem.numberOfRecurrences ? dataItem.numberOfRecurrences : "";
        var fullName = dataItem.payPersonName ? dataItem.payPersonName : dataItem.firstName + " " + dataItem.lastName;
		var payAPersonDefaultAccountNumber = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.default_from_account_p2p;
		var defaultAccountNumber = dataItem.fromAccountNumber || payAPersonDefaultAccountNumber ||  this.view.tableView.listbxFromAccount.masterData[0][0];
		this.view.tableView.listbxFromAccount.selectedKey = defaultAccountNumber;
        this.view.tableView.lblPaytoValue.text = fullName;
		this.view.tableView.listbxFrequency.selectedKey = dataItem.frequencyType ? dataItem.frequencyType : this.view.tableView.listbxFrequency.masterData[0][0];
		if(dataItem.numberOfRecurrences != null && dataItem.numberOfRecurrences != "")
		{
		    this.view.tableView.listbxHowLong.selectedKey = "NO_OF_RECURRENCES";
		}else if(dataItem.frequencyStartDate != null && dataItem.frequencyStartDate != "" &&  dataItem.frequencyEndDate != null && dataItem.frequencyEndDate != "")
		{
			this.view.tableView.listbxHowLong.selectedKey = "ON_SPECIFIC_DATE";
		}
		
        var emailMasterDrop = [];
        var sendRecep = self.checkRequriedValues(dataItem);
        for (i = 0; i < sendRecep.length; i++) {
            if (sendRecep[i] != null) {
                var data = [];
                data.push(sendRecep[i]);
                data.push(sendRecep[i])
				if(sendRecep[i] !== undefined && sendRecep[i] !== "")
                  emailMasterDrop.push(data);
            }
        }
        this.view.tableView.listbxSentby.masterData = emailMasterDrop;
      if (dataItem.p2pContact)
       this.view.tableView.listbxSentby.selectedKey = dataItem.p2pContact ? dataItem.p2pContact : this.view.tableView.listbxSentby.masterData[0][0];
      if (dataItem.lblPrimaryContact != "" || dataItem.lblPrimaryContact != null || dataItem.lblPrimaryContact != undefined)
       this.view.tableView.listbxSentby.selectedKey = dataItem.lblPrimaryContact ? dataItem.lblPrimaryContact : this.view.tableView.listbxSentby.masterData[0][0];
      if (self.view.tableView.tbxAmount.text != "" && self.view.tableView.tbxAmount.text != null) self.enableButton(this.view.tableView.btnSendMoney);
        else self.disableButton(this.view.tableView.btnSendMoney);
		self.getFrequencyAndFormLayout(this.view.tableView.listbxFrequency.selectedKey, this.view.tableView.listbxHowLong.selectedKey);
        this.view.tableView.lblErrorAmount.setVisibility(false);
		this.view.forceLayout();
   },
   setMyRequestsSegmentData: function(data) {
    kony.print("in setMyRequestsSegmentData");
     this.view.customheader.customhamburger.activateMenu("PayAPerson", "MyRequests");
    if (data.length !== 0) {
        var self = this;
        this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.MyRequests")
        }]);
        this.showTableView();
        this.view.tableView.flxTableHeaders.setVisibility(true);
        this.view.tableView.flxTableHeaders.flxSendRequestWrapper.setVisibility(false);
        this.view.tableView.flxTableHeaders.flxSentWrapper.setVisibility(false);
        this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(true);
        this.view.tableView.segP2P.setVisibility(true);
        this.view.tableView.flxPagination.setVisibility(false);
        this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
        this.setSkinActive(this.view.tableView.flxTabs.btnMyRequests);
        this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
        this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
        this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
        var dataMap = {
            "btnCancel": "btnCancel",
            "btnReminder": "btnReminder",
            "flxAmount": "flxAmount",
            "flxBottomSeperator": "flxBottomSeperator",
            "flxCancelAction": "flxCancelAction",
            "flxColumn1": "flxColumn1",
            "flxColumn2": "flxColumn2",
            "flxDate": "flxDate",
            "flxDetails": "flxDetails",
            "flxDropdown": "flxDropdown",
            "flxIdentifier": "flxIdentifier",
            "flxMyRequests": "flxMyRequests",
            "flxMyRequestsSelected": "flxMyRequestsSelected",
            "flxReminderAction": "flxReminderAction",
            "flxSelectedRowWrapper": "flxSelectedRowWrapper",
            "flxSentTo": "flxSentTo",
            "flxSeperator": "flxSeperator",
            "imgDropdown": "imgDropdown",
            "lblAmount": "lblAmount",
            "lblDate": "lblDate",
            "lblNote": "lblNote",
            "lblNote1": "lblNote1",
            "lblRequiredBy": "lblRequiredBy",
            "lblRequiredBy1": "lblRequiredBy1",
            "lblSentTo": "lblSentTo",
            "lblTransactionHeader": "lblTransactionHeader",
            "lblSeparator": "lblSeparator",
            "lblSeparatorSelected": "lblSeparatorSelected",
            "imgRowSelected": "imgRowSelected"
        };
        var viewModel = [];
        var scopeObj = this;
        var sentRequests = [];
        var receivedRequests = [];

        data.map(function(dataItem){
            if(dataItem["transactionType"] == "Request"){
                sentRequests.push({
                        "btnCancel": {
                            "text": "CANCEL",
                            "onClick": scopeObj.onCancelRequestMoneyBtnClick.bind(scopeObj, dataItem)
                        },
                        "btnReminder": {
                            "text": "REMINDER",
                            "onClick": scopeObj.onRemainderRequestMoneyBtnClick.bind(scopeObj, dataItem)
                        },
                        "imgDropdown": "arrow_down.png",
                        "lblAmount": dataItem.amount ? self.presenter.formatCurrency(dataItem.amount) : "NA",
                        "lblDate": dataItem.p2pRequiredDate ? self.presenter.getFormattedDateString(dataItem.p2pRequiredDate) : "NA",
                        "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                        "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : "None",
                        "lblRequiredBy": kony.i18n.getLocalizedString("i18n.PayAPerson.RequiredBy"),
                        "lblRequiredBy1": dataItem.p2pRequiredDate ? self.presenter.getFormattedDateString(dataItem.p2pRequiredDate) : "NA",
                        "lblSentTo": dataItem.payPersonName ? dataItem.payPersonName : "NA",
                        "template": "flxMyRequests",
                        "lblSeparator": ".",
                        "lblSeparatorSelected": "lblSeparatorSelected",
                        "imgRowSelected": "arrow_up.png"
                    });
            }
            else{
                receivedRequests.push({
                        "btnCancel": {
                            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Pay"),
                            "onClick": null
                        },
                        "btnReminder": {
                            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Decline"),
                            "onClick": null
                        },
                        "imgDropdown": "arrow_down.png",
                        "lblAmount": dataItem.amount ? self.presenter.formatCurrency(dataItem.amount) : "NA",
                        "lblDate": dataItem.p2pRequiredDate ? self.presenter.getFormattedDateString(dataItem.p2pRequiredDate) : "NA",
                        "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                        "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : "None",
                        "lblRequiredBy": kony.i18n.getLocalizedString("i18n.PayAPerson.RequiredBy"),
                        "lblRequiredBy1": dataItem.p2pRequiredDate ? self.presenter.getFormattedDateString(dataItem.p2pRequiredDate) : "NA",
                        "lblSentTo": dataItem.payPersonName ? dataItem.payPersonName : "NA",
                        "template": "flxMyRequests",
                        "lblSeparator": ".",
                        "lblSeparatorSelected": "lblSeparatorSelected",
                        "imgRowSelected": "arrow_up.png"
                    });
            }
        });
        
        var sentHeader = {
            "lblTransactionHeader": kony.i18n.getLocalizedString("i18n.PayAPerson.SentRequests"),
            "lblSeparator": ".",
        };
        var receivedHeader = {
            "lblTransactionHeader": kony.i18n.getLocalizedString("i18n.PayAPerson.ReceivedRequests"),
            "lblSeparator": ".",
        };
        var sentRequestsSegmentData = [];
        var receivedRequestsSegmentData = [];

                        
        if(sentRequests.length > 0){
            sentRequestsSegmentData.push(sentHeader, sentRequests);
            viewModel.push(sentRequestsSegmentData);
        }
        if(receivedRequests.length>0){
            receivedRequestsSegmentData.push(receivedHeader, receivedRequests);
            viewModel.push(receivedRequestsSegmentData);
        } 
        
        
        //viewModel.push(sentRequestsSegmentData, receivedRequestsSegmentData);
        this.view.tableView.segP2P.widgetDataMap = dataMap;
        this.view.tableView.segP2P.setData(viewModel);
        this.view.tableView.flxNoTransactions.setVisibility(false);
		this.view.tableView.Search.setVisibility(false);
        this.showView(["flxTableView", "flxMyPaymentAccounts","flxWhatElse","flxAddRecipientButton"]);
    } else {
        this.view.tableView.flxNoTransactions.setVisibility(true);
        this.view.tableView.segP2P.setVisibility(false);
        this.view.tableView.flxPagination.setVisibility(false);
        this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
        this.showView(["flxTableView", "flxMyPaymentAccounts","flxWhatElse","flxAddRecipientButton"]);
        this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
        this.setSkinActive(this.view.tableView.flxTabs.btnMyRequests);
        this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
        this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
        this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
		this.view.tableView.Search.setVisibility(false);
    }
    this.view.forceLayout();
},
  showSentSegmentNextPage: function(){
        CommonUtilities.showProgressBar(this.view);
        this.presenter.showNextSentSegment();
    },
    showSentSegmentPreviousPage: function(){
        CommonUtilities.showProgressBar(this.view);
        this.presenter.showPreviousSentSegment();
    },
    showTransactionReport: function(transaction) {
        var self = this;
        if(transaction.viewReportLink){
            this.view.p2pReport.imgReport.src = transaction.viewReportLink;
            self.view.p2pReport.setVisibility(true);
            self.view.p2pReport.btnClose.onClick = function() {
                self.view.p2pReport.setVisibility(false);
            };
        }
    },
    /* Method used to set the Data for the sent tab, It accepts collection of records */
  setSentSegmentData: function(data, status) {
        kony.print("in setSentSegmentData");
		this.view.customheader.customhamburger.activateMenu("PayAPerson", "SentTransactions");
        if(data.length !== 0){
            var scopeObj = this;
            this.setP2PBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
                    callback: function() {
                      	CommonUtilities.showProgressBar(scopeObj.view);
                        scopeObj.presenter.showSendOrRequestSegment();
                        }
                    }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.Sent")
            }]);
            if(kony.mvc.MDAApplication.getSharedInstance().appContext.isactivated === false){
                this.view.flxRightWrapper.setVisibility(true); 
                this.showView([ "flxTableView", "flxMyPaymentAccounts"]);
                this.view.tableView.btnSendRequest.setEnabled(false);
                this.view.tableView.btnMyRequests.setEnabled(false);
                this.view.tableView.btnRecieved.setEnabled(false);
                this.view.tableView.btnManageRecepient.setEnabled(false);
            }
            else{
                this.showTableView();
            }
            this.view.tableView.flxTableHeaders.setVisibility(true);
            this.view.tableView.flxTableHeaders.flxSendRequestWrapper.setVisibility(false);
            this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.flxSentTo.lblSentTo.text = kony.i18n.getLocalizedString("i18n.PayAPerson.To");
            this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.flxSentUsing.lblSentUsing.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Using");
            this.view.tableView.flxTableHeaders.flxSentWrapper.setVisibility(true);
            this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.setVisibility(true);
            this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
            this.view.tableView.segP2P.setVisibility(true);
            this.view.tableView.flxPagination.setVisibility(true);
            this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
            this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
            this.setSkinActive(this.view.tableView.flxTabs.btnSent);
            this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
            this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
            this.view.tableView.flxPaginationNext.onClick = this.showSentSegmentNextPage.bind(this.presenter);
            this.view.tableView.flxPaginationPrevious.onClick = this.showSentSegmentPreviousPage.bind(this.presenter);
            var dataMap = {
                "btnModify": "btnModify",
                "btnViewActivity": "btnViewActivity",
                "flxAmount": "flxAmount",
                "flxBottomSeperator": "flxBottomSeperator",
                "flxColumn1": "flxColumn1",
                "flxColumn2": "flxColumn2",
                "flxColumn3": "flxColumn3",
                "flxColumn4": "flxColumn4",
                "flxDate": "flxDate",
                "flxDetails": "flxDetails",
                "flxDropdown": "flxDropdown",
                "flxIdentifier": "flxIdentifier",
                "flxModifyAction": "flxModifyAction",
                "flxRow": "flxRow",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxSentSelected": "flxSentSelected",
                "flxSeperator": "flxSeperator",
                "flxTo": "flxTo",
                "flxUsing": "flxUsing",
                "imgDropdown": "imgDropdown",
                "lblAmount": "lblAmount",
                "lblDate": "lblDate",
                "lblDeliveredOn": "lblDeliveredOn",
                "lblDeliveredOn1": "lblDeliveredOn1",
                "lblNote": "lblNote",
                "lblNote1": "lblNote1",
                "lblReferenceNo": "lblReferenceNo",
                "lblReferenceNumber1": "lblReferenceNumber1",
                "lblTo": "lblTo",
                "lblUsing": "lblUsing",
                "lblTransactionHeader": "lblTransactionHeader",
                "lblSeparator": "lblSeparator",
                "lblSeparatorSelected": "lblSeparatorSelected",
                "imgRowSelected": "imgRowSelected"
            };
            var pending = [];
            var posted = [];
			var self = this;
            for (var index in data) {
				        data[index].onCancel = function(){
                  self.presenter.fetchSentTransactions();
                };
                if (data[index].statusDescription === "Successful") posted.push(data[index]);
                else pending.push(data[index]);
            }
            var postedHeader = {
                "lblTransactionHeader": kony.i18n.getLocalizedString("i18n.PayAPerson.Posted"),
                "lblSeparator": "."
            };
			
            var postedTransactions = posted.map(function(dataItem) {
                var repeatButtonVisible = true;
                var repeatButtonOnclick = self.presenter.onSendMoney.bind(self.presenter, dataItem);
                if(dataItem.isPaypersonDeleted == "true"){
                    repeatButtonVisible = false;
                    repeatButtonOnclick = null;
                }
                return {
                    "btnModify": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Repeat"),
                        onClick: repeatButtonOnclick,
                        "isVisible": repeatButtonVisible,
                    },
                    "btnViewActivity": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.ViewReport"),
                        onClick: self.showTransactionReport.bind(self, dataItem)
                    },
                    "imgDropdown": "arrow_down.png",
                    "lblAmount": dataItem.amount ? scopeObj.presenter.formatCurrency(dataItem.amount) : "NA",
                    "lblDate": dataItem.transactionDate ? scopeObj.presenter.getFormattedDateString(dataItem.transactionDate) : "NA",
                    "lblDeliveredOn": kony.i18n.getLocalizedString("i18n.PayAPerson.DeliveredOn"),
                    "lblDeliveredOn1": dataItem.transactionDate ? scopeObj.presenter.getFormattedDateString(dataItem.transactionDate) : "NA",
                    "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                    "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : kony.i18n.getLocalizedString("i18n.PayAPerson.None"),
                    "lblReferenceNo": kony.i18n.getLocalizedString("i18n.PayAPerson.ReferenceNumber"),
                    "lblReferenceNumber1": dataItem.transactionId,
                    "lblTo": dataItem.payPersonName ? dataItem.payPersonName : "NA",
                    "lblUsing": dataItem.p2pContact ? dataItem.p2pContact : "NA",
                  	"lblSeparator": ".",
                    "lblSeparatorSelected": "lblSeparatorSelected",
                    "template": "flxSent",
                    "imgRowSelected": "arrow_up.png"
                };
            });
            var pendingHeader = {
                "lblTransactionHeader": kony.i18n.getLocalizedString("i18n.PayAPerson.Scheduled"),
                "lblSeparator": "."
            };
            var pendingTransactions = pending.map(function(dataItem) {
                return {
                    "btnModify": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Modify"),
                         onClick: self.presenter.onSendMoney.bind(self.presenter, dataItem)
                    },
                    "btnViewActivity": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.ViewActivity"),
                        onClick: self.showTransactionReport.bind(self, dataItem)
                    },
                    "imgDropdown": "arrow_down.png",
                    "lblAmount": dataItem.amount ? scopeObj.presenter.formatCurrency(dataItem.amount) : "NA",
                    "lblDate": dataItem.transactionDate ? scopeObj.presenter.getFormattedDateString(dataItem.transactionDate) : "NA",
                    "lblDeliveredOn": kony.i18n.getLocalizedString("i18n.PayAPerson.DeliveredOn"),
                    "lblDeliveredOn1": dataItem.transactionDate ? scopeObj.presenter.getFormattedDateString(dataItem.transactionDate) : "NA",
                    "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                    "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : kony.i18n.getLocalizedString("i18n.PayAPerson.None"),
                    "lblReferenceNo": kony.i18n.getLocalizedString("i18n.PayAPerson.ReferenceNumber"),
                    "lblReferenceNumber1": dataItem.transactionId,
                    "lblTo": dataItem.payPersonName ? dataItem.payPersonName : "NA",
                    "lblUsing": dataItem.p2pContact ? dataItem.p2pContact : "NA",
                  	"lblSeparator": ".",
                    "lblSeparatorSelected": ".",
                    "template": "flxSent",
                    "imgRowSelected": "arrow_up.png"
                };
            });
            var viewModel = [];
            var pendingViewModel = [];
            if (pendingTransactions.length > 0) {
                pendingViewModel.push(pendingHeader, pendingTransactions);
                viewModel.push(pendingViewModel);
            };
            var postedViewModel = [];
            if (postedTransactions.length > 0) {
                postedViewModel.push(postedHeader, postedTransactions);
                viewModel.push(postedViewModel);
            };
            this.view.tableView.segP2P.widgetDataMap = dataMap;
            this.view.tableView.segP2P.setData(viewModel);
			this.view.tableView.flxNoTransactions.setVisibility(false);
			this.view.tableView.Search.setVisibility(false);
      this.showView(["flxTableView", "flxMyPaymentAccounts","flxWhatElse","flxAddRecipientButton"]);
        }
        else{
            this.view.tableView.flxNoTransactions.setVisibility(true);
            this.view.tableView.segP2P.setVisibility(false);
            this.view.tableView.flxPagination.setVisibility(false);
            this.showView(["flxTableView", "flxMyPaymentAccounts","flxWhatElse","flxAddRecipientButton"]);
            this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
			this.view.tableView.Search.setVisibility(false);
            this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
            this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
            this.setSkinActive(this.view.tableView.flxTabs.btnSent);
            this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
            this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
        }
        this.view.forceLayout();
    },
  showNextReceivedSegment: function(){
        CommonUtilities.showProgressBar(this.view);
        this.presenter.showNextReceivedSegment();
    },
    showPreviousReceivedSegment: function(){
        CommonUtilities.showProgressBar(this.view);
        this.presenter.showPreviousReceivedSegment();
    },
    setReceivedSegmentData: function(data, status) {
        kony.print("in setReceivedSegmentData");
        var scopeObj = this;
        this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.Received"),
        }]);
        this.showTableView();
        this.view.flxRightWrapper.setVisibility(true);
        this.view.tableView.flxTableHeaders.setVisibility(true);
        this.view.tableView.flxTableHeaders.flxSendRequestWrapper.setVisibility(false);
        this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.flxSentTo.lblSentTo.text = kony.i18n.getLocalizedString("i18n.PayAPerson.From");
        this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.flxSentUsing.lblSentUsing.text = kony.i18n.getLocalizedString("i18n.PayAPerson.DepositedTo");
        this.view.tableView.flxTableHeaders.flxSentWrapper.setVisibility(true);
        this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
        this.view.tableView.flxTableHeaders.flxSentWrapper.flxSentRow.setVisibility(true);
        this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
        this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
        this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
        this.setSkinActive(this.view.tableView.flxTabs.btnRecieved);
        this.setSkinInActive(this.view.tableView.flxTabs.btnManageRecepient);
        this.view.tableView.flxNoTransactions.setVisibility(false);
        this.view.tableView.flxPaginationNext.onClick = null; //this.showNextReceivedSegment.bind(this.presenter);
        this.view.tableView.flxPaginationPrevious.onClick = null; //this.showNextReceivedSegment.bind(this.presenter);
        var dataMap = {
            "btnAction": "btnAction",
            "btnViewActivity": "btnViewActivity",
            "flxAction": "flxAction",
            "flxAmount": "flxAmount",
            "flxBottomSeperator": "flxBottomSeperator",
            "flxColumn1": "flxColumn1",
            "flxColumn3": "flxColumn3",
            "flxColumn4": "flxColumn4",
            "flxDate": "flxDate",
            "flxDepositedTo": "flxDepositedTo",
            "flxDetails": "flxDetails",
            "flxDropdown": "flxDropdown",
            "flxFrom": "flxFrom",
            "flxIdentifier": "flxIdentifier",
            "flxReceived": "flxReceived",
            "flxReceivedSelected": "flxReceivedSelected",
            "flxSelectedRowWrapper": "flxSelectedRowWrapper",
            "flxSeperator": "flxSeperator",
            "imgDropdown": "imgDropdown",
            "lblAmount": "lblAmount",
            "lblDate": "lblDate",
            "lblDepositedTo": "lblDepositedTo",
            "lblFrom": "lblFrom",
            "lblNote": "lblNote",
            "lblNote1": "lblNote1",
            "lblSentTo": "lblSentTo",
            "lblSentTo1": "lblSentTo1",
            "lblSeparator": "lblSeparator",
            "lblSeparatorSelected": "lblSeparatorSelected",
            "imgRowSelected": "imgRowSelected"
        };
        var viewModel = data.map(function(dataItem) {
            return {
                "btnAction": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Request"),
                    onClick: null
                },
                "btnViewActivity": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.ViewActivity"),
                    onClick: null
                },
                "imgDropdown": "arrow_down.png",
                "lblAmount": dataItem.amount ? scopeObj.presenter.formatCurrency(dataItem.amount) : "NA",
                "lblDate": dataItem.transactionDate ? scopeObj.presenter.getFormattedDateString(dataItem.transactionDate) : "NA",
                "lblDepositedTo": dataItem.toAccountName,
                "lblFrom": dataItem.payPersonName,
                "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : kony.i18n.getLocalizedString("i18n.PayAPerson.None"),
                "lblSentTo": "Sent To:",
                "lblSentTo1": dataItem.p2pContact,
                "lblSeparator": ".",
                "lblSeparatorSelected": ".",
                "template": "flxReceived",
                "imgRowSelected": "arrow_up.png"
            };
        });
        this.view.tableView.segP2P.widgetDataMap = dataMap;
        this.view.tableView.segP2P.setData(viewModel);
        this.showView(["flxTableView", "flxMyPaymentAccounts","flxWhatElse","flxAddRecipientButton"]);
		this.view.tableView.Search.setVisibility(false);
        this.view.forceLayout();
    },
    showPayAPersonViewActivity: function(selectedData) {
        var self = this;
        this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
        },{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.ViewActivity"),
        }]);
        this.view.tableView.setVisibility(false);
        this.view.p2pActivity.setVisibility(true);
        var dataMap = {
            "lblFrom": "lblFrom",
            "lblDate": "lblDate",
            "lblAmount": "lblAmount",
            "lblStatus": "lblStatus",
            "lblRunningStatus": "lblRunningStatus",
            "amountTransferredTillNow": "amountTransferredTillNow"
        };
        var data = selectedData.map(function(payee) {
            return {
                "lblFrom": payee.fromAccountName,
                "lblDate": self.presenter.getFormattedDateString(payee.transactionDate),
                "lblAmount": self.presenter.formatCurrency(payee.amount),
                "lblStatus": payee.statusDescription,
                "lblRunningStatus": self.presenter.formatCurrency(payee.fromAccountBalance),
                "amountTransferredTillNow": payee.amountTransferedTillNow,
            }
        });
        this.view.p2pActivity.lblAmountDeducted.text = data[0] ? this.presenter.formatCurrency(data[0]["amountTransferredTillNow"]) : this.presenter.formatCurrency(0);
        this.view.p2pActivity.segP2PActivity.widgetDataMap = dataMap;
        this.view.p2pActivity.segP2PActivity.setData(data);
        this.view.p2pActivity.btnBackToRecipients.onClick = function() {
            self.view.p2pActivity.setVisibility(false);
            self.view.tableView.setVisibility(true);
            self.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
        }]);
		self.AdjustScreen(170);
        };
        this.view.p2pActivity.btnClose.onClick = function() {
            self.view.p2pActivity.setVisibility(false);
            self.view.tableView.setVisibility(true);
            self.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
        }]);
        };
        CommonUtilities.hideProgressBar(this.view);
        this.view.forceLayout();
    },
  setP2PActivityDetails(selectedRecord){
	this.view.p2pActivity.lblToAccount.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ToAccount");
	  this.view.p2pActivity.lblAccountHolderName.text = selectedRecord["lblName"];
	this.view.p2pActivity.lblAccountHolderEmail.text= selectedRecord["email"];
	 this.view.p2pActivity.lblAccountHolderNumber.text = selectedRecord["phone"];
	  this.view.p2pActivity.lblAmountDeductedTitle.text = kony.i18n.getLocalizedString("i18n.PayAPerson.AmountTransferredTillNow");
  },
  setManageRecipientSegmentData: function(managePayeesData) {
    kony.print("in setManageRecipientSegmentData");
	this.view.customheader.customhamburger.activateMenu("PayAPerson", "ManageRecipients");
    var self = this;
    this.setP2PBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
        callback: function() {
            CommonUtilities.showProgressBar(self.view);
            self.presenter.showSendOrRequestSegment();
        }
    }, {
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient"),
    }]);
	
    this.showTableView();
    this.view.tableView.flxTableHeaders.setVisibility(true);
    this.view.tableView.flxTableHeaders.flxSendRequestWrapper.setVisibility(true);
    this.view.tableView.flxTableHeaders.flxSentWrapper.setVisibility(false);
    this.view.tableView.flxTableHeaders.flxMyRequestsWrapper.setVisibility(false);
    this.setSkinInActive(this.view.tableView.flxTabs.btnSendRequest);
    this.setSkinInActive(this.view.tableView.flxTabs.btnMyRequests);
    this.setSkinInActive(this.view.tableView.flxTabs.btnSent);
    this.setSkinInActive(this.view.tableView.flxTabs.btnRecieved);
    this.setSkinActive(this.view.tableView.flxTabs.btnManageRecepient);
	this.view.tableView.flxNoTransactions.setVisibility(false);
	this.view.tableView.Search.setVisibility(false);
    this.view.tableView.flxPaginationNext.onClick = this.presenter.getNextManagePayeesRecords.bind(this.presenter);
    this.view.tableView.flxPaginationPrevious.onClick = this.presenter.getPreviousManagePayeesRecords.bind(this.presenter);
    this.view.tableView.flxPagination.setVisibility(true);
    managePayeesData = managePayeesData.map(function(payeeRecord) {
        var btnDelete = {
            "onClick": function() {
                self.deleteP2PRecipient();
            },
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Delete"),
        };
        var btnEdit = {
            "onClick": function() {
                self.editP2PRecipient();
                kony.print("in setManagePayeeData method, clicked  :   Edit");
            },
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.Edit"),
        };
        var btnRequestMoney = {
            "onClick": function() {
                var data = self.view.tableView.segP2P.data;
                var index = self.view.tableView.segP2P.selectedIndex[1];
                var selectedData = data[index]; 
				 self.presenter.onRequestMoney(selectedData);
                //self.onRequestMoneyBtnClick(selectedData);  
                kony.print("in setManagePayeeData method, clicked  :   RequestMoney");
            },
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
        };
        var btnSendMoney = {
            "onClick": function() {
              var data = self.view.tableView.segP2P.data;
              var index = self.view.tableView.segP2P.selectedIndex[1];
              var selectedData = data[index]; 
			  self.presenter.onSendMoney(selectedData);
              //self.onSendMoneyBtnClick(selectedData)
                kony.print("in setManagePayeeData method, clicked  :   SendMoney");
            },
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoney"),
        };
        var btnViewActivity = {
            "onClick": function() {
              var data = self.view.tableView.segP2P.data;
              var index = self.view.tableView.segP2P.selectedIndex[1];
              var selectedData = data[index];
              CommonUtilities.showProgressBar(self.view);
		          self.setP2PActivityDetails(selectedData);
              self.presenter.getPayPersonViewActivity(selectedData["PayPersonId"]);
                kony.print("in setManagePayeeData method, clicked  :   ViewActivity");
            },
            "text": kony.i18n.getLocalizedString("i18n.PayAPerson.ViewActivity"),
        };
        return {
            "btnDelete": btnDelete,
            "btnEdit": btnEdit,
            "btnRequestMoney": btnRequestMoney,
            "btnSendMoney": btnSendMoney,
            "btnViewActivity": btnViewActivity,
            "flxBottomSeperator": "flxBottomSeperator",
            "flxColumn1": "flxColumn1",
            "flxColumn2": "flxColumn2",
            "flxColumn3": "flxColumn3",
            "flxDeleteAction": "flxDeleteAction",
            "flxDetails": "flxDetails",
            "flxDropdown": "flxDropdown",
            "flxEditAction": "flxEditAction",
            "flxIdentifier": "flxIdentifier",
            "ManageRecipient": "ManageRecipient",
            "ManageRecipientSelected": "ManageRecipientSelected",
            "flxName": "flxName",
            "flxPrimaryContact": "flxPrimaryContacy",
            "flxRow": "flxRow",
            "flxSelectedRowWrapper": "flxSelectedRowWrapper",
            "flxSeperator": "flxSeperator",
            "imgDropdown": "arrow_down.png",
            "lblName": payeeRecord.nickName ? payeeRecord.nickName : payeeRecord.firstName,
            "lblPrimaryContact": payeeRecord.primaryContactForSending,
            "lblRegisteredEmail1": payeeRecord.email?payeeRecord.email: "",
            "lblRegisteredEmail2": payeeRecord.secondaryEmail ? payeeRecord.secondaryEmail:"",
            "lblRegisteredEmails": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredEmail"),
            "lblRegisteredPhone": kony.i18n.getLocalizedString("i18n.PayAPerson.RegisteredPhone"),
            "lblRegisteredPhone1": payeeRecord.phone ? payeeRecord.phone: "",
            "lblRegisteredPhone2": payeeRecord.secondaryPhoneNumber? payeeRecord.secondaryPhoneNumber:"",
            "recipientID": payeeRecord.PayPersonId,
            "recipientNickName": payeeRecord.nickName,
            "lblSeparator": "lblSeparator",
          "lblSeparatorSelected": "lblSeparatorSelected",
            "imgRowSelected": "arrow_up.png",
            "template": "flxManageRecipient",
            //adding these additional fields for send and request money feature.
            "email":payeeRecord.email,
            "secondaryEmail":payeeRecord.secondaryEmail,
            "secodnaryPhone":payeeRecord.secondaryPhone,
            "firstName":payeeRecord.firstName,
            "lastName":payeeRecord.lastName,
            "nickName":payeeRecord.nickName,
            "phone":payeeRecord.phone,
            "PayPersonId":payeeRecord.PayPersonId
        };
    });
    var dataMap = {
        "btnDelete": "btnDelete",
        "btnEdit": "btnEdit",
        "btnRequestMoney": "btnRequestMoney",
        "btnSendMoney": "btnSendMoney",
        "btnViewActivity": "btnViewActivity",
        "flxBottomSeperator": "flxBottomSeperator",
        "flxColumn1": "flxColumn1",
        "flxColumn2": "flxColumn2",
        "flxColumn3": "flxColumn3",
        "flxDeleteAction": "flxDeleteAction",
        "flxDetails": "flxDetails",
        "flxDropdown": "flxDropdown",
        "flxEditAction": "flxEditAction",
        "flxIdentifier": "flxIdentifier",
        "ManageRecipient": "ManageRecipient",
        "ManageRecipientSelected": "ManageRecipientSelected",
        "flxName": "flxName",
        "flxPrimaryContact": "flxPrimaryContact",
        "flxRow": "flxRow",
        "flxSelectedRowWrapper": "flxSelectedRowWrapper",
        "flxSeperator": "flxSeperator",
        "imgDropdown": "imgDropdown",
        "lblName": "lblName",
        "lblPrimaryContact": "lblPrimaryContact",
        "lblRegisteredEmail1": "lblRegisteredEmail1",
        "lblRegisteredEmail2": "lblRegisteredEmail2",
        "lblRegisteredEmails": "lblRegisteredEmails",
        "lblRegisteredPhone": "lblRegisteredPhone",
        "lblRegisteredPhone1": "lblRegisteredPhone1",
        "lblRegisteredPhone2": "lblRegisteredPhone2",
        "lblSeparator": "lblSeparator",
      "lblSeparatorSelected": "lblSeparatorSelected",
        "imgRowSelected": "imgRowSelected"
    };
    this.view.tableView.segP2P.widgetDataMap = dataMap;
    this.view.tableView.segP2P.setData(managePayeesData);
    this.showView(["flxTableView", "flxMyPaymentAccounts","flxWhatElse","flxAddRecipientButton"]);
    CommonUtilities.hideProgressBar(this.view);
    this.view.forceLayout();
  },
  deleteP2PRecipient: function() {
        var self = this;
        var data = this.view.tableView.segP2P.data;
        var index = this.view.tableView.segP2P.selectedIndex[1];
        var selectedData = data[index];
        self.showQuitScreen({
          "negative":function(){
            self.closeQuitScreen();
          },
          "positive": function(){
            self.closeQuitScreen();
            self.presenter.deleteRecipient(selectedData);
          }
        }, kony.i18n.getLocalizedString("i18n.PayAPerson.Delete"), kony.i18n.getLocalizedString("i18n.PayAPerson.DeleteRecipientMessage"));
    },
  showAddRecipientWithContext: function(payAPersonJSON) {
    var self = this;
    this.showAddRecipient();
   /* if (payAPersonJSON["lblRegisteredPhone2"] !== undefined && payAPersonJSON["lblRegisteredPhone2"] !== null && payAPersonJSON["lblRegisteredPhone2"] !== "") {
        this.addSecondaryPhone();
        this.view.AddRecipient.tbxPhoneValue02.text = payAPersonJSON["lblRegisteredPhone2"];
    }
    if (payAPersonJSON["lblRegisteredEmail2"] !== undefined && payAPersonJSON["lblRegisteredEmail2"] !== null && payAPersonJSON["lblRegisteredEmail2"] !== "") {
        this.addSecondaryEmail();
        this.view.AddRecipient.tbxEmailValue02.text = payAPersonJSON["lblRegisteredEmail2"];
    }*/
    this.disableButton(this.view.AddRecipient.btnAdd);
    this.view.AddRecipient.tbxRecipientValue.onKeyUp = function() {
        self.checkIsValidInput();
    };
    this.view.AddRecipient.tbxPhoneValue1.onKeyUp = function() {
        self.checkIsValidInput();
    };
    this.view.AddRecipient.tbxEmailValue1.onKeyUp = function() {
        self.checkIsValidInput();
    };
    this.view.AddRecipient.tbxPhoneValue2.onKeyUp = function() {
        self.checkIsValidInput();
    };
    this.view.AddRecipient.tbxEmailValue2.onKeyUp = function() {
        self.checkIsValidInput();
    };
    this.view.AddRecipient.tbxRecipientValue.text = payAPersonJSON["lblName"];
    this.view.AddRecipient.tbxNickNameValue.text = payAPersonJSON["recipientNickName"];
    this.view.AddRecipient.tbxPhoneValue1.text = payAPersonJSON["lblRegisteredPhone1"];
    this.view.AddRecipient.tbxEmailValue1.text = payAPersonJSON["lblRegisteredEmail1"];
    this.view.AddRecipient.tbxPhoneValue2.text = payAPersonJSON["lblRegisteredPhone2"];
    this.view.AddRecipient.tbxEmailValue2.text = payAPersonJSON["lblRegisteredEmail2"];
    this.AdjustScreen();
    this.view.forceLayout();
},
    showConfirmationEditReceipient: function(payPersonJSON) {
        var self = this;
        this.view.flxBottom.setVisibility(false);
        var primaryContactForSending = [];
        var phoneText = "";
        if (payPersonJSON["phone"]) {
            phoneText = phoneText + payPersonJSON["phone"];
            primaryContactForSending = primaryContactForSending.concat([
                [payPersonJSON["phone"], payPersonJSON["phone"]]
            ]);
        }
        if (payPersonJSON["secondaryPhoneNumber"]) {
            phoneText = phoneText + "<br>" + payPersonJSON["secondaryPhoneNumber"];
            primaryContactForSending = primaryContactForSending.concat([
                [payPersonJSON["secondaryPhoneNumber"], payPersonJSON["secondaryPhoneNumber"]]
            ]);
        }
        var emailText = "";
        if (payPersonJSON["email"]) {
            emailText = emailText + payPersonJSON["email"];
            primaryContactForSending = primaryContactForSending.concat([
                [payPersonJSON["email"], payPersonJSON["email"]]
            ]);
        }
        if (payPersonJSON["secondaryEmail"]) {
            emailText = emailText + "<br>" + payPersonJSON["secondaryEmail"];
            primaryContactForSending = primaryContactForSending.concat([
                [payPersonJSON["secondaryEmail"], payPersonJSON["secondaryEmail"]]
            ]);
        }
        var data = {
            "Recipient Name": payPersonJSON["firstName"],
            "Nick Name": payPersonJSON["nickName"],
            "Phone Number(s)": phoneText,
            "Email Address(s)": emailText,
        };
        this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment()
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient"),
            callback: function() {
                self.presenter.getManagePayeesData();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientConfirmation")
        }]);
        this.view.confirmation.flxPrimaryContact.setVisibility(true);
        this.view.confirmation.lbxPrimaryContact.masterData = primaryContactForSending;
        var scopeObj = this;
        var actions = {
            "cancel": function() {
                scopeObj.showQuitScreen({
                    "negative": function() {
                        scopeObj.closeQuitScreen();
                    },
                    "positive": function() {
                        scopeObj.closeQuitScreen();
                        scopeObj.presenter.getManagePayeesData();
                    }
                });
            },
            "modify": function() {
                scopeObj.modifyRecpient(payPersonJSON);
            },
            "confirm": function() {
                scopeObj.setAcknowledgementValues([], [], [], {
                    "btnOne": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoney"),
                        "action": function() {
                            kony.print("AddRecipient btnOneNoAction");
                            scopeObj.showSendMoney(); //need to send JSON to send money function.
                        }
                    },
                    "btnTwo": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
                        "action": function() {
                            scopeObj.showRequestMoney(); //need to send JSON to request money function
                        }
                    },
                    "btnThree": {
                        "text": kony.i18n.getLocalizedString("i18n.PayAPerson.BackToManageRecipient"),
                        "action": function() {
                            scopeObj.presenter.getManagePayeesData();
                        }
                    }
                });
                CommonUtilities.showProgressBar(scopeObj.view);
                payPersonJSON["primaryContactForSending"] = scopeObj.view.confirmation.lbxPrimaryContact.selectedKey;
                scopeObj.presenter.editRecipient(payPersonJSON);
            },
        };
        this.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
        this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ConfirmmRecipientDetails")
        this.view.lblHeadingAddRecipient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.smallEditRecipient");
        this.setConfirmationValues(data, actions, {});
        this.postShowPayaPerson();
        this.AdjustScreen();
        this.showView(["flxConfirmation"]);
    },
    editRecipient: function() {
        var self = this;
        var payeeName = this.view.AddRecipient.tbxRecipientValue.text;
        var nickName = this.view.AddRecipient.tbxNickNameValue.text;
    
        payPersonJSON = {};
        payPersonJSON["id"] = this.view.lblHiddenValue.text;
        payPersonJSON["PayPersonId"] = this.view.lblHiddenValue.text;
        payPersonJSON["firstName"] = payeeName;
        payPersonJSON["lastName"] = nickName;
        payPersonJSON["nickName"] = nickName;
        if(self.isValidEmail(this.view.AddRecipient.tbxEmailValue1.text) && this.view.AddRecipient.tbxEmailValue1.text != "") payPersonJSON["email"] = this.view.AddRecipient.tbxEmailValue1.text;
        if(self.isValidEmail(this.view.AddRecipient.tbxEmailValue2.text) && this.view.AddRecipient.tbxEmailValue2.text != "") payPersonJSON["secondaryEmail"] = this.view.AddRecipient.tbxEmailValue2.text;
        if(self.isValidPhone(this.view.AddRecipient.tbxPhoneValue1.text) && this.view.AddRecipient.tbxPhoneValue1.text != "") payPersonJSON["phone"] = this.view.AddRecipient.tbxPhoneValue1.text;
        if(self.isValidPhone(this.view.AddRecipient.tbxPhoneValue2.text) && this.view.AddRecipient.tbxPhoneValue2.text != "") payPersonJSON["secondaryPhoneNumber"] = this.view.AddRecipient.tbxPhoneValue2.text;


        if(!self.isValidEmail(payPersonJSON["email"]) && self.isValidEmail(payPersonJSON["secondaryEmail"])){
            payPersonJSON["email"] = payPersonJSON["secondaryEmail"];
            payPersonJSON["secondaryEmail"] = undefined;
        }
        if(!self.isValidPhone(payPersonJSON["phone"]) && self.isValidPhone(payPersonJSON["secondaryPhoneNumber"])){
            payPersonJSON["phone"] = payPersonJSON["secondaryPhoneNumber"];
            payPersonJSON["secondaryPhoneNumber"] = undefined;
        }
        this.showConfirmationEditReceipient(payPersonJSON);
        this.AdjustScreen();
    },
    editP2PRecipient: function() {
        var self = this;
        var data = this.view.tableView.segP2P.data;
        var index = this.view.tableView.segP2P.selectedIndex[1];
        var selectedData = data[index];
        selectedData["lblName"] = selectedData["firstName"];
        this.view.lblHiddenValue.text = selectedData["recipientID"];
        this.showAddRecipientWithContext(selectedData);
        this.enableButton(this.view.AddRecipient.btnAdd);
        this.view.AddRecipient.btnAdd.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Update");
        this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientDetails");
        this.view.lblHeadingAddRecipient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.smallEditRecipient");
        this.view.AddRecipient.btnAdd.onClick = this.editRecipient.bind(this);
        this.view.AddRecipient.btnCancel.onClick = this.presenter.getManagePayeesData.bind(this.presenter);
        this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(self.view);
                self.presenter.showSendOrRequestSegment()
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient"),
        }]);
        this.AdjustScreen();
    },
  showView: function (views) {
    this.view.flxActivateP2P.setVisibility(false);
    this.view.flxAcknowledgement.setVisibility(false);
    this.view.flxAddRecipientButton.setVisibility(false);
    this.view.flxAddRecipient.setVisibility(false);
    this.view.flxTableView.setVisibility(false);
    this.view.flxMyPaymentAccounts.setVisibility(false);
    this.view.flxNoRecipient.setVisibility(false);
    this.view.flxOptionsAndProceed.setVisibility(false);
    this.view.flxPreviouslyAddedRecipients.setVisibility(false);
    this.view.flxConfirmation.setVisibility(false);
    this.view.flxWhatElse.setVisibility(false);

    for (var i = 0; i < views.length; i++) {
      this.view[views[i]].isVisible = true;
    }
    this.view.forceLayout();
  },
  showP2PSettingsScreen: function(p2pViewModel) {
    var self = this;
    var p2pAccounts = this.showAccountsForSelectionP2PSettings(p2pViewModel.paymentAccounts);
    var defaultToAccount = self.getMaskedAccountNameForID(p2pAccounts, kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.default_to_account_p2p);
    var defaultFromAccount = self.getMaskedAccountNameForID(p2pAccounts, kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.default_from_account_p2p);
    this.setP2PBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
        callback: function() {
            CommonUtilities.showProgressBar(self.view);
            self.presenter.showSendOrRequestSegment()
        }
    }, {
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
    }]);
    this.view.lblHeadingActivateP2P.text = kony.i18n.getLocalizedString("i18n.PayAPerson.P2PPaymentSettings");
    this.view.Activatep2p.lblHeader.text = kony.i18n.getLocalizedString("i18n.PayAPerson.P2PSettings");
    this.view.Activatep2p.lblNameValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userFirstName + " " + kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userLastName;
    this.view.Activatep2p.lblRegisteredPhoneValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userPhoneNumber;
    this.view.Activatep2p.lblRegisteredEmailValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userEmail;
    this.view.Activatep2p.lblDefaultDepositAccountValue.text = defaultToAccount;
    this.view.Activatep2p.lblDefaultSendingAccountValue.text = defaultFromAccount;
    this.showView(["flxActivateP2P"]);
    this.view.Activatep2p.btnCancel.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Back");
    this.view.Activatep2p.btnCancel.setVisibility(true);
    this.view.Activatep2p.btnCancel.onClick = function() {
        CommonUtilities.showProgressBar(self.view);
        self.presenter.showSendOrRequestSegment();
    };
    this.view.Activatep2p.btnConfirm.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Edit");
    this.view.Activatep2p.btnConfirm.onClick = function() {
        self.showEditP2PSettingsScreen(p2pViewModel);
    };
    //Setting UI for displaying existing settings
    this.view.Activatep2p.lblNameValue.setVisibility(true);
    this.view.Activatep2p.tbxNameValue.setVisibility(false);
    this.view.Activatep2p.lblRegisteredPhoneValue.setVisibility(true);
    this.view.Activatep2p.listbxRegisteredPhoneValue.setVisibility(false);
    this.view.Activatep2p.tbxRegisteredPhoneValue.setVisibility(false);
    this.view.Activatep2p.lblRegisteredEmailValue.setVisibility(true);
    this.view.Activatep2p.listbxRegisteredEmailValue.setVisibility(false);
    this.view.Activatep2p.tbxRegisteredEmailValue.setVisibility(false);
    this.view.Activatep2p.lblDefaultDepositAccountValue.setVisibility(true);
    this.view.Activatep2p.listbxAcccFrDeposit.setVisibility(false);
    this.view.Activatep2p.lblDefaultSendingAccountValue.setVisibility(true);
    this.view.Activatep2p.listbxAccFrSending.setVisibility(false);
    CommonUtilities.hideProgressBar(this.view);
    this.AdjustScreen();
    this.view.forceLayout();
},
showAccountsForSelectionP2PSettings: function(presentAccounts) {
    var list = [];
    for (i = 0; i < presentAccounts.length; i++) {
        var tempList = [];
        tempList.push(presentAccounts[i].accountID);
        var tempAccountNumber = presentAccounts[i].accountID;
        tempList.push(presentAccounts[i].accountName + " ..." + tempAccountNumber.slice(-4));
        list.push(tempList);
    }
    return list;
},
//Method to return masked accountID + accountName for a given account ID
getMaskedAccountNameForID: function(accounts, accountID) {
    var paymentAccount;
    for (index in accounts) {
        if (accounts[index][0] == accountID) {
            paymentAccount = accounts[index][1];
            break;
        }
    }
    return paymentAccount;
},
//Method to show edit settings screen
showEditP2PSettingsScreen: function(p2pViewModel) {
    var self = this;
    this.setP2PBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
        callback: function() {
            CommonUtilities.showProgressBar(self.view);
            self.presenter.showSendOrRequestSegment()
        }
    }, {
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
    }]);
    this.view.lblHeadingActivateP2P.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ModifyP2PPaymentSettings");
    this.view.Activatep2p.tbxNameValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userFirstName + " " + kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userLastName;
    this.view.Activatep2p.tbxRegisteredPhoneValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userPhoneNumber;
    this.view.Activatep2p.tbxRegisteredEmailValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userEmail;
    this.view.Activatep2p.listbxAcccFrDeposit.masterData = this.showAccountsForSelectionP2PSettings(p2pViewModel.paymentAccounts);
    this.view.Activatep2p.listbxAccFrSending.masterData = this.showAccountsForSelectionP2PSettings(p2pViewModel.paymentAccounts);
    this.view.Activatep2p.listbxAcccFrDeposit.selectedKey = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.default_to_account_p2p;
    this.view.Activatep2p.listbxAccFrSending.selectedKey = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.default_from_account_p2p;
    this.view.Activatep2p.btnCancel.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Cancel"),
        this.view.Activatep2p.btnCancel.setVisibility(true);
    this.view.Activatep2p.btnCancel.onClick = function() {
        CommonUtilities.showProgressBar(self.view);
        self.presenter.showSendOrRequestSegment();
    };
    this.view.Activatep2p.btnConfirm.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Confirm"),
        this.view.Activatep2p.btnConfirm.onClick = function() {
            self.presenter.updateP2PPreferencesForUser({
                "defaultFromAccount": self.view.Activatep2p.listbxAccFrSending.selectedKey,
                "defaultToAccount": self.view.Activatep2p.listbxAcccFrDeposit.selectedKey,
            });
        };
    //Updating UI for editing settings
    this.view.Activatep2p.lblNameValue.setVisibility(false);
    this.view.Activatep2p.tbxNameValue.setVisibility(true);
    this.view.Activatep2p.tbxNameValue.setEnabled(false);
    this.view.Activatep2p.lblRegisteredPhoneValue.setVisibility(false);
    this.view.Activatep2p.tbxRegisteredPhoneValue.setVisibility(true);
    this.view.Activatep2p.tbxRegisteredPhoneValue.setEnabled(false);
    this.view.Activatep2p.lblRegisteredEmailValue.setVisibility(false);
    this.view.Activatep2p.tbxRegisteredEmailValue.setVisibility(true);
    this.view.Activatep2p.tbxRegisteredEmailValue.setEnabled(false);
    this.view.Activatep2p.lblDefaultDepositAccountValue.setVisibility(false);
    this.view.Activatep2p.listbxAcccFrDeposit.setVisibility(true);
    this.view.Activatep2p.lblDefaultSendingAccountValue.setVisibility(false);
    this.view.Activatep2p.listbxAccFrSending.setVisibility(true);
    CommonUtilities.hideProgressBar(this.view);
    this.AdjustScreen();
    this.view.forceLayout();
},
  showActivateP2PScreen: function (p2pViewModel) {

    var self = this;
    this.setP2PBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson")
    }, {
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonActivation")
    },{
      text: kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonSettings")
    }]);


    this.view.lblHeadingActivateP2P.text = "Activation of P2p" //kony.i18n.getLocalizedString("i18n.PayAPerson.ModifyP2PPaymentSettings");
    this.view.Activatep2p.tbxNameValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userFirstName + " " + kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userLastName;
    this.view.Activatep2p.tbxRegisteredPhoneValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userPhoneNumber;
    this.view.Activatep2p.tbxRegisteredEmailValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.userProfiledata.userEmail;
    this.view.Activatep2p.listbxAcccFrDeposit.masterData = this.showAccountsForSelectionP2PSettings(p2pViewModel.paymentAccounts);
    this.view.Activatep2p.listbxAccFrSending.masterData = this.showAccountsForSelectionP2PSettings(p2pViewModel.paymentAccounts);
    this.view.Activatep2p.btnCancel.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Cancel"),
    this.view.Activatep2p.btnCancel.setVisibility(true);
    this.view.Activatep2p.btnCancel.onClick = function() {
    	self.showView(["flxOptionsAndProceed"]);
    };
    this.view.Activatep2p.btnConfirm.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Confirm"),
        this.view.Activatep2p.btnConfirm.onClick = function() {
            self.presenter.ActivateP2P({
                "defaultFromAccount": self.view.Activatep2p.listbxAccFrSending.selectedKey,
                "defaultToAccount": self.view.Activatep2p.listbxAcccFrDeposit.selectedKey,
            });
        };
    //Updating UI for editing settings
    this.view.Activatep2p.lblNameValue.setVisibility(false);
    this.view.Activatep2p.tbxNameValue.setVisibility(true);
    this.view.Activatep2p.tbxNameValue.setEnabled(false);
    this.view.Activatep2p.lblRegisteredPhoneValue.setVisibility(false);
    this.view.Activatep2p.tbxRegisteredPhoneValue.setVisibility(true);
    this.view.Activatep2p.tbxRegisteredPhoneValue.setEnabled(false);
    this.view.Activatep2p.lblRegisteredEmailValue.setVisibility(false);
    this.view.Activatep2p.tbxRegisteredEmailValue.setVisibility(true);
    this.view.Activatep2p.tbxRegisteredEmailValue.setEnabled(false);
    this.view.Activatep2p.lblDefaultDepositAccountValue.setVisibility(false);
    this.view.Activatep2p.listbxAcccFrDeposit.setVisibility(true);
    this.view.Activatep2p.lblDefaultSendingAccountValue.setVisibility(false);
    this.view.Activatep2p.listbxAccFrSending.setVisibility(true);
    CommonUtilities.hideProgressBar(this.view);
    this.showView(["flxActivateP2P"]);
    this.AdjustScreen();
    this.view.forceLayout();
  },

  showAccountsForSelection: function(presentAccounts) {
    var list = [];
    for (i = 0; i < presentAccounts.length; i++) {
      var tempList = [];
      tempList.push(presentAccounts[i].accountNumber);
      var tempAccountNumber=presentAccounts[i].accountNumber;
      tempList.push(presentAccounts[i].accountName + " XXX" + tempAccountNumber.slice(-4));
      list.push(tempList);
    }
    return list;
  },
  
    resetAddRecipientUI: function() {
        this.view.AddRecipient.tbxRecipientValue.text = "";
        this.view.AddRecipient.tbxNickNameValue.text = "";
        this.view.AddRecipient.tbxEmailValue1.text = "";
        this.view.AddRecipient.tbxEmailValue2.text = "";
        this.view.AddRecipient.tbxPhoneValue1.text = "";
        this.view.AddRecipient.tbxPhoneValue2.text = "";
        this.view.AddRecipient.CopytbxNickNameValue0d3080bbd2c5640.text = "";
        this.view.forceLayout();
    },
    addRecipient: function() {
        var self =  this;
		this.view.customheader.customhamburger.activateMenu("PayAPerson", "AddRecipient");
        var payeeName = this.view.AddRecipient.tbxRecipientValue.text;
        var nickName = this.view.AddRecipient.tbxNickNameValue.text;
        
        
        payPersonJSON = {};
        payPersonJSON["firstName"] = CommonUtilities.changedataCase(payeeName);
        payPersonJSON["lastName"] = nickName;
        payPersonJSON["nickName"] = nickName;
        if(self.isValidEmail(this.view.AddRecipient.tbxEmailValue1.text) && this.view.AddRecipient.tbxEmailValue1.text != "") payPersonJSON["email"] = this.view.AddRecipient.tbxEmailValue1.text;
        if(self.isValidEmail(this.view.AddRecipient.tbxEmailValue2.text) && this.view.AddRecipient.tbxEmailValue2.text != "") payPersonJSON["secondaryEmail"] = this.view.AddRecipient.tbxEmailValue2.text;
        if(self.isValidPhone(this.view.AddRecipient.tbxPhoneValue1.text) && this.view.AddRecipient.tbxPhoneValue1.text != "") payPersonJSON["phone"] = this.view.AddRecipient.tbxPhoneValue1.text;
        if(self.isValidPhone(this.view.AddRecipient.tbxPhoneValue2.text) && this.view.AddRecipient.tbxPhoneValue2.text != "") payPersonJSON["secondaryPhoneNumber"] = this.view.AddRecipient.tbxPhoneValue2.text;

        if(!self.isValidEmail(payPersonJSON["email"]) && self.isValidEmail(payPersonJSON["secondaryEmail"])){
            payPersonJSON["email"] = payPersonJSON["secondaryEmail"];
            payPersonJSON["secondaryEmail"] = undefined;
        }
        if(!self.isValidPhone(payPersonJSON["phone"]) && self.isValidPhone(payPersonJSON["secondaryPhoneNumber"])){
            payPersonJSON["phone"] = payPersonJSON["secondaryPhoneNumber"];
            payPersonJSON["secondaryPhoneNumber"] = undefined;
        }
            this.showConfirmationAddReceipient(payPersonJSON);
            this.AdjustScreen();
    },
    checkIsValidInput() {
            var self = this;
            var name = this.view.AddRecipient.tbxRecipientValue.text;
            if (name != undefined && name != null && name != "") {
                var isPhoneValid = false;
                var isEmailValid = false;
                var phone1 = this.view.AddRecipient.tbxPhoneValue1.text;
                var phone2 = this.view.AddRecipient.tbxPhoneValue2.text;
                var email1 = this.view.AddRecipient.tbxEmailValue1.text;
                var email2 = this.view.AddRecipient.tbxEmailValue2.text;

                if(self.isValidPhone(phone1) && self.isValidPhone(phone2) && self.isValidEmail(email1) && self.isValidEmail(email2)){
                        if(phone1 == "" && phone2 == "" && email1 == "" && email2 == ""  || (phone1 != "" && phone1 == phone2) || (email1 != "" && email1 == email2)){
                            self.disableButton(this.view.AddRecipient.btnAdd);
                        }
                        else
                            self.enableButton(this.view.AddRecipient.btnAdd);
                }
                else{
                    self.disableButton(this.view.AddRecipient.btnAdd);
                }
                
            } else {
                self.disableButton(this.view.AddRecipient.btnAdd);
            }
        },
    isValidEmail: function(email) {
        if(email == "") return true;
        if(email == null || email == undefined) return false;
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    isValidPhone: function(phone){
        if(phone == "") return true;
        if(phone == null || phone == undefined ) 
                return false;
        else{
            var regex = new RegExp("^[0-9]");
            if(regex.test(phone) && phone.length == 10){
                return true;
            }else
                return false;
        }
    },
  showAddRecipient: function() {
    var self = this;
    this.view.customheader.customhamburger.activateMenu("PayAPerson", "AddRecipient");
    this.view.AddRecipient.tbxRecipientValue.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterRecipientName");
    this.view.AddRecipient.tbxNickNameValue.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNickName");
    this.view.AddRecipient.tbxPhoneValue1.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterPhoneNumber");
    this.view.AddRecipient.tbxEmailValue1.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterEmail");
    this.view.AddRecipient.tbxPhoneValue2.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterPhoneNumber");
    this.view.AddRecipient.tbxEmailValue2.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterEmail");
    this.showView(["flxMyPaymentAccounts", "flxAddRecipient", "flxWhatElse"]);
    this.resetAddRecipientUI();
    this.disableButton(this.view.AddRecipient.btnAdd);
    this.view.AddRecipient.btnAdd.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Add");
    this.setP2PBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
        callback: function() {
            CommonUtilities.showProgressBar(self.view);
            self.presenter.showSendOrRequestSegment()
        }
    }, {
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient"),
    }]);
    
	this.view.AddRecipient.tbxRecipientValue.onKeyUp = function() {
        self.checkIsValidInput();
    }
    this.view.AddRecipient.tbxPhoneValue1.onKeyUp = function() {
        self.checkIsValidInput();
    }
    this.view.AddRecipient.tbxEmailValue1.onKeyUp = function() {
        self.checkIsValidInput();
    }
   this.view.AddRecipient.tbxPhoneValue2.onKeyUp = function() {
        self.checkIsValidInput();
    }
    this.view.AddRecipient.tbxEmailValue2.onKeyUp = function() {
        self.checkIsValidInput();
    } 
    this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterRecipientDetails")
    this.view.lblHeadingAddRecipient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.smallAddRecipient");
    this.view.AddRecipient.btnAdd.onClick = this.addRecipient.bind(this);
    this.view.AddRecipient.btnCancel.onClick = this.hideAddRecipient.bind(this);
    this.AdjustScreen();
},
  hideAddRecipient : function(){
    this.presenter.NavigateToPayAPerson();
  },
  showTableView: function () {
    this.view.flxBottom.setVisibility(true);
  	this.view.flxRightWrapper.setVisibility(true);
    this.showView(["flxAddRecipientButton", "flxTableView", "flxMyPaymentAccounts", "flxWhatElse"]);
    this.enableButton(this.view.noRecipients.btnSendMoney);
    this.enableButton(this.view.noRecipients.btnRequestMoney);
    //flex inside billpay
    this.view.tableView.flxSendMoney.setVisibility(false);
    this.view.tableView.flxSendReminder.setVisibility(false);
    this.view.tableView.flxRequestMoney.setVisibility(false);
    this.view.tableView.flxHorizontalLine2.setVisibility(false);
    this.view.tableView.flxTableHeaders.setVisibility(false);
    this.view.tableView.flxHorizontalLine3.setVisibility(false);
    this.view.tableView.segP2P.setVisibility(true);

    this.view.tableView.btnSendRequest.setEnabled(true);
    this.view.tableView.btnMyRequests.setEnabled(true);
    this.view.tableView.btnSent.setEnabled(true);
    this.view.tableView.btnRecieved.setEnabled(true);
    this.view.tableView.btnManageRecepient.setEnabled(true);
  },
  showNotEligibleScreen:function(){
    this.setP2PBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
    }, {
        text: kony.i18n.getLocalizedString("i18n.Common.NotEligible"),
    }]);
    this.view.OptionsAndProceed.flxDetails.isVisible = false;
    this.view.OptionsAndProceed.flxWarning.setVisibility(true);
    this.view.OptionsAndProceed.lblWarning.text = kony.i18n.getLocalizedString("i18n.p2p.notEligiblemsg");
    this.view.OptionsAndProceed.flxSeparator2.isVisible = false;
    this.view.OptionsAndProceed.flxActions.isVisible = false;
    this.showView(["flxOptionsAndProceed"]);
    CommonUtilities.hideProgressBar(this.view);
    this.AdjustScreen();
    this.view.forceLayout();
  },
  setConfirmationValues: function (data, actions, extraData) {
    var target = this.view.confirmation.flxKeyValues.widgets();
    var i = 0;
    for (var prop in data) {
      var key = target[i].widgets()[0].widgets()[0].widgets()[0];
      var value = target[i].widgets()[0].widgets()[2].widgets()[0];
      key.text = prop;
      value.text = data[prop];
      i++;  }
    if (Object.keys(data).length > 5) {
			if(Object.keys(data).length > 6)
			{
			 this.view.confirmation.flxKeyValues.keyValueSix.setVisibility(true)
             this.view.confirmation.flxKeyValues.keyValueFive.setVisibility(true)
			}else{
				this.view.confirmation.flxKeyValues.keyValueSix.setVisibility(false)
                this.view.confirmation.flxKeyValues.keyValueFive.setVisibility(true)
			}
            
    } else {
      this.view.confirmation.flxKeyValues.keyValueFive.setVisibility(false)
      this.view.confirmation.flxKeyValues.keyValueSix.setVisibility(false)
    }
    if (Object.keys(extraData).length !== 0) {
      kony.print("Extradata if");
      this.view.confirmation.flxTotal.isVisible = true;
      this.view.confirmation.flxCheckBox.isVisible = true;
      var target = this.view.confirmation.flxTotal.widgets();
      var i=0;
      for(var prop in extraData){
        var key = target[i].widgets()[0].widgets()[0].widgets()[0].widgets()[0];
        var value = target[i].widgets()[0].widgets()[0].widgets()[2].widgets()[0];
        key.text = prop;
        value.text = extraData[prop];
        i++;
      } } else {
        kony.print("Extradata else");
        this.view.confirmation.flxTotal.isVisible = false;
        this.view.confirmation.flxCheckBox.isVisible = false;
      } 

    this.view.confirmation.confirmButtons.btnCancel.onClick = actions.cancel;
    this.view.confirmation.confirmButtons.btnModify.onClick = actions.modify;
    this.view.confirmation.confirmButtons.btnConfirm.onClick = actions.confirm;
	this.postShowPayaPerson();	
    this.showView(["flxConfirmation"]);
  },

  setAcknowledgementValues: function (message, details, extraMessage, actions) {
    this.view.acknowledgment.lblTransactionMessage.text = message.message;
    this.view.acknowledgment.ImgAcknowledged.src = message.image;
    if(Object.keys(extraMessage).length !== 0){
      this.view.acknowledgment.flxBalance.setVisibility(true);
      this.view.acknowledgment.lblRefrenceNumber.setVisibility(true);
	  this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(true);
      this.view.acknowledgment.lblAccType.setVisibility(true);
      this.view.acknowledgment.lblBalance.setVisibility(true);
      this.view.acknowledgment.lblRefrenceNumberValue.text = extraMessage.referenceNumber;
      this.view.acknowledgment.lblAccType.text = extraMessage.accountType;
      this.view.acknowledgment.lblBalance.text = extraMessage.amount;
    }
    else{
      this.view.acknowledgment.flxBalance.setVisibility(false);
      this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(false);
      this.view.acknowledgment.lblRefrenceNumber.setVisibility(false);
      this.view.acknowledgment.lblAccType.setVisibility(false);
      this.view.acknowledgment.lblBalance.setVisibility(false);
    }
    var target = this.view.confirmDialog.flxMain.widgets();
    var i=2;
    for(var prop in details){
      var key = target[i].widgets()[0].widgets()[0];
      var value = target[i].widgets()[1].widgets()[0];
      key.text = prop;
      value.text = details[prop];
      i++;
    }
    if(Object.keys(details).length === 6){
      this.view.confirmDialog.flxContainerSix.isVisible = true;
	   this.view.confirmDialog.flxContainerSeven.isVisible = false;
      this.view.confirmDialog.flxContainerEight.isVisible = false;
    }
    else if(Object.keys(details).length === 7)
    {
      this.view.confirmDialog.flxContainerSix.isVisible = true;
      this.view.confirmDialog.flxContainerSeven.isVisible = true;
	  this.view.confirmDialog.flxContainerEight.isVisible = false;
    }
    else if(Object.keys(details).length === 8)
    {
      this.view.confirmDialog.flxContainerSix.isVisible = true;
      this.view.confirmDialog.flxContainerSeven.isVisible = true;
      this.view.confirmDialog.flxContainerEight.isVisible = true;
    }
    else{
      this.view.confirmDialog.flxContainerSix.isVisible = false;
      this.view.confirmDialog.flxContainerSeven.isVisible = false;
      this.view.confirmDialog.flxContainerEight.isVisible = false;
    }
    if(Object.keys(actions).length !== 0)
    {
      if(actions.btnOne.text === "null"){
        this.view.btnAcknowledgementOne.isVisible = false;
        this.view.btnAcknowledgementTwo.text = actions.btnTwo.text;
        this.view.btnAcknowledgementTwo.onClick = actions.btnTwo.action;
        this.view.btnAcknowledgementThree.text = actions.btnThree.text;
        this.view.btnAcknowledgementThree.onClick = actions.btnThree.action
      }
      else{        
        this.view.btnAcknowledgementOne.text = actions.btnOne.text;
        this.view.btnAcknowledgementOne.onClick = actions.btnOne.action;
        this.view.btnAcknowledgementTwo.text = actions.btnTwo.text;
        this.view.btnAcknowledgementTwo.onClick = actions.btnTwo.action;
        this.view.btnAcknowledgementThree.text = actions.btnThree.text;
        this.view.btnAcknowledgementThree.onClick = actions.btnThree.action;
      }
    }
  },
  modifyRecpient: function(payAPersonJSON) {
    this.view.breadcrumb.lblBreadcrumb3.text = ""
    this.showView(["flxMyPaymentAccounts", "flxAddRecipient", "flxWhatElse"]);
    this.view.AddRecipient.tbxRecipientValue.text = payAPersonJSON["firstName"];
    this.view.AddRecipient.tbxNickNameValue.text = payAPersonJSON["lastName"];
    this.view.AddRecipient.tbxPhoneValue1.text = payAPersonJSON["phone"];
    this.view.AddRecipient.tbxEmailValue1.text = payAPersonJSON["email"];
    if (this.isValidPhone(payAPersonJSON["secondaryPhoneNumber"])) 
        this.view.AddRecipient.tbxPhoneValue2.text = payAPersonJSON["secondaryPhoneNumber"];
    else
            this.view.AddRecipient.tbxPhoneValue2.text = "";
    if (this.isValidEmail(payAPersonJSON["secondaryEmail"]))
            this.view.AddRecipient.tbxEmailValue2.text = payAPersonJSON["secondaryEmail"];
    else
            this.view.AddRecipient.tbxEmailValue2.text = "";
    this.AdjustScreen();
},
showConfirmationAddReceipient: function(payPersonJSON) {
    var self = this;
    this.view.flxBottom.setVisibility(false);
    var primaryContactForSending = [];
    var phoneText = "";
    if (self.isValidPhone(payPersonJSON["phone"])) {
        phoneText = phoneText + payPersonJSON["phone"];
        primaryContactForSending = primaryContactForSending.concat([
            [payPersonJSON["phone"], payPersonJSON["phone"]]
        ]);
    }
    if (self.isValidPhone(payPersonJSON["secondaryPhoneNumber"])) {
        phoneText = phoneText + "<br>" + payPersonJSON["secondaryPhoneNumber"];
        primaryContactForSending = primaryContactForSending.concat([
            [payPersonJSON["secondaryPhoneNumber"], payPersonJSON["secondaryPhoneNumber"]]
        ]);
    }
    var emailText = "";
    if (self.isValidEmail(payPersonJSON["email"])) {
        emailText = emailText + payPersonJSON["email"];
        primaryContactForSending = primaryContactForSending.concat([
            [payPersonJSON["email"], payPersonJSON["email"]]
        ]);
    }
    if (self.isValidEmail(payPersonJSON["secondaryEmail"])) {
        emailText = emailText + "<br>" + payPersonJSON["secondaryEmail"];
        primaryContactForSending = primaryContactForSending.concat([
            [payPersonJSON["secondaryEmail"], payPersonJSON["secondaryEmail"]]
        ]);
    }
    var data = {};
        data[kony.i18n.getLocalizedString("i18n.PayPerson.recipientName")]= payPersonJSON["firstName"];
        data[kony.i18n.getLocalizedString("i18n.PayPerson.nickName")]= payPersonJSON["nickName"];
        data[kony.i18n.getLocalizedString("i18n.PayPerson.phoneNumbers")]= phoneText;
        data[kony.i18n.getLocalizedString("i18n.PayPerson.emailAddress")]= emailText;
    
    this.setP2PBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
        callback: function() {
            CommonUtilities.showProgressBar(self.view);
            self.presenter.showSendOrRequestSegment()
        }
    }, {
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient"),
    }, {
        text: kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipientConfirmation")
    }]);
    this.view.confirmation.flxPrimaryContact.setVisibility(true);
    this.view.confirmation.lbxPrimaryContact.masterData = primaryContactForSending;
    this.view.CustomPopup1.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Quit");
    this.view.CustomPopup1.lblPopupMessage.text = "Are you sure you want to undo 'Adding Recipient'?";
    var scopeObj = this;
    var actions = {
        "cancel": function() {
            scopeObj.showQuitScreen({
                "negative": function() {
                    scopeObj.closeQuitScreen();
                },
                "positive": function() {
                    scopeObj.closeQuitScreen();
                    scopeObj.showAddRecipient();
                }
            });
        },
        "modify": function() {
            scopeObj.modifyRecpient(payPersonJSON);
        },
        "confirm": function() {
            scopeObj.setAcknowledgementValues([], [], [], {
                "btnOne": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoney"),
                    "action": function() {
                        kony.print("AddRecipient btnOneNoAction");
                        scopeObj.onShowMoneyBtnClick(); //need to send JSON to send money function.
                    }
                },
                "btnTwo": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoney"),
                    "action": function() {
                        scopeObj.showRequestMoney(); //need to send JSON to request money function
                    }
                },
                "btnThree": {
                    "text": kony.i18n.getLocalizedString("i18n.PayAPerson.AddAnotherRecipient"),
                    "action": function() {
                        scopeObj.showAddRecipient(); //need to implement this
                    }
                }
            });
            CommonUtilities.showProgressBar(scopeObj.view);
            payPersonJSON["primaryContactForSending"] = scopeObj.view.confirmation.lbxPrimaryContact.selectedKey;
            scopeObj.presenter.createP2PPayee(payPersonJSON);
        },
    };
    this.AdjustScreen();
    this.enableButton(self.view.confirmation.confirmButtons.btnConfirm);
    this.view.AddRecipient.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ConfirmmRecipientDetails")
    this.view.lblHeadingAddRecipient.text = kony.i18n.getLocalizedString("i18n.PayAPerson.smallAddRecipient");
    this.setConfirmationValues(data, actions, {});
    this.postShowPayaPerson();
    this.showView(["flxConfirmation"]);
},
  showAcknowledgementSendMoney : function(requestObj,status,accountName,accountBalance)
	{
		var scopeObj = this;
		this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");
            this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(scopeObj.view);
                scopeObj.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyConfirmation"),
        },{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyAcknowledgement"),
        }]);
      
		var scopeObj = this;
		var message = {
			"message":kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyMessage")+requestObj.To,
			"image":"success_green.png",
		};		
		var extraMessage = {
			"referenceNumber" : status.data.referenceId ? status.data.referenceId : status.data.transactionId,
			 "accountType" : accountName,
			 "amount":scopeObj.presenter.formatCurrency(accountBalance)
		}	
		var details = {};
            details[kony.i18n.getLocalizedString("i18n.PayPerson.recipientName")]=requestObj.To;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.using")]=requestObj.p2pContact;
           	details[kony.i18n.getLocalizedString("i18n.PayPerson.amount")]=scopeObj.presenter.formatCurrency(requestObj.amount);
           	details[kony.i18n.getLocalizedString("i18n.PayPerson.requiredDate")]=requestObj.Date;
           	details[kony.i18n.getLocalizedString("i18n.PayAPerson.Note")]=requestObj.transactionsNotes;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.defaultAccountForSendingMoney")]= requestObj.From;
			details[kony.i18n.getLocalizedString("i18n.PayPerson.frequency")]=  requestObj.frequencyType;
        
		var actions = {
            "btnOne": {
                "text": "null",
                "action": function() {
                    kony.print("no function");
                }
            },
            "btnTwo": {
                "text": kony.i18n.getLocalizedString("i18n.transfers.makeanothertransfer"),
                "action": function() {
					CommonUtilities.showProgressBar(scopeObj.view);
					scopeObj.presenter.showSendOrRequestSegment(scopeObj.presenter);                 
                }
            },
            "btnThree": {
                "text": kony.i18n.getLocalizedString("i18n.payaperson.viewSentTrasactions"),
                "action": function() {
					 CommonUtilities.showProgressBar(scopeObj.view);
                     scopeObj.presenter.showSentSegment();
                }
            }
        };
		if(requestObj.context == "sendMoneyToNewRecipient"){		
		   requestObj.payPersonObject.primaryContactForSending = requestObj.p2pContact;
		   requestObj.payPersonObject.transactionId = status.data.referenceId;
		    if(requestObj.personId == undefined || requestObj.personId == null || requestObj.personId == "")
			{
				actions.btnOne.text = kony.i18n.getLocalizedString("i18n.payaperson.savereciepient");
				actions.btnOne.action = function() {
					CommonUtilities.showProgressBar(scopeObj.view);
					scopeObj.presenter.createP2PPayee(requestObj.payPersonObject);			
                };				
			}

		}
		this.setAcknowledgementValues(message, details, extraMessage, actions);
		this.showView(["flxAcknowledgement"]);		
    this.AdjustScreen();
	},
  changeDateFormat:function(date){
     	date=date.split("-");
		return new Date(date[1]+"/"+date[2]+"/"+date[0]);
	},
  showAcknowledgementRequestMoney: function (requestObj,status) {
    var scopeObj = this;
	
		this.view.tableView.tbxNoOfRecurrences.placeholder = kony.i18n.getLocalizedString("i18n.PayAPerson.EnterNumberOfRecurrences");
            this.setP2PBreadcrumbData([{
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.PayAPerson"),
            callback: function() {
                CommonUtilities.showProgressBar(scopeObj.view);
                scopeObj.presenter.showSendOrRequestSegment();
            }
        }, {
            text: kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoneyAcknowledgement"),
        }]);
		var message = {
			"message":kony.i18n.getLocalizedString("i18n.PayAPerson.RequestMoneyMessage"),
			"image":"success_green.png",
		};	
		var extraMessage = {
			
		}	
		var requestDate=scopeObj.changeDateFormat(requestObj.p2pRequiredDate);		
		var details = {};
            details[kony.i18n.getLocalizedString("i18n.PayPerson.recipientName")] =  requestObj.To;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.using")] = requestObj.p2pContact;
            details[kony.i18n.getLocalizedString("i18n.PayPerson.amount")] = scopeObj.presenter.formatCurrency(requestObj.amount);
            details[kony.i18n.getLocalizedString("i18n.PayPerson.requiredDate")] =  CommonUtilities.getFrontendDateString(requestDate,CommonUtilities.getConfiguration('frontendDateFormat'));
            details[kony.i18n.getLocalizedString("i18n.PayAPerson.Note")] =  requestObj.transactionsNote;
		
		if(requestObj.transactionId){ 
			
		}else{
			details["Default account for Receiving Money:"] = requestObj.From
		}
		var actions = {
            "btnOne": {
                "text": "null",
                "action": function() {
                    kony.print("no function");
                }
            },
            "btnTwo": {
                "text": kony.i18n.getLocalizedString("i18n.PayPerson.viewMyRequests"),
                "action": function() {
					CommonUtilities.showProgressBar(scopeObj.view);
                    scopeObj.presenter.fetchRequests(scopeObj.presenter);
                }
            },
            "btnThree": {
                "text": kony.i18n.getLocalizedString("i18n.PayPerson.sendNewRequests"),
                "action": function() {
					
					CommonUtilities.showProgressBar(scopeObj.view);
                    scopeObj.presenter.showSendOrRequestSegment(scopeObj.presenter);
                }
            }
        };
		this.setAcknowledgementValues(message, details, {}, actions);
		this.showView(["flxAcknowledgement"]);
  },
  showSendMoney: function () {
    this.showTableView();
    this.setSkinActive(this.view.tableView.flxTabs.btnSendRequest);
    this.setSkinActive(this.view.tableView.flxTabs.btnMyRequests);
    this.setSkinActive(this.view.tableView.flxTabs.btnSent);
    this.setSkinActive(this.view.tableView.flxTabs.btnRecieved);
    this.setSkinActive(this.view.tableView.flxTabs.btnManageRecepient);

    this.view.tableView.flxSendMoney.setVisibility(true);
    this.view.tableView.flxSendReminder.setVisibility(false);
    this.view.tableView.flxRequestMoney.setVisibility(false);
    this.view.tableView.flxHorizontalLine2.setVisibility(false);
    this.view.tableView.flxTableHeaders.setVisibility(false);
    this.view.tableView.flxHorizontalLine3.setVisibility(false);
    this.view.tableView.segP2P.setVisibility(false);
  },
  showRequestMoney: function () {
    this.showTableView();
    this.view.tableView.flxTabs.btnSendRequest.skin = "sknBtnAccountSummarySelected";
    this.view.tableView.flxTabs.btnMyRequests.skin = "sknBtnAccountSummaryUnselected";
    this.view.tableView.flxTabs.btnSent.skin = "sknBtnAccountSummaryUnselected";
    this.view.tableView.flxTabs.btnRecieved.skin = "sknBtnAccountSummaryUnselected";
    this.view.tableView.flxTabs.btnManageRecepient.skin = "sknBtnAccountSummaryUnselected";
    this.view.tableView.flxSendMoney.setVisibility(false);
    this.view.tableView.flxSendReminder.setVisibility(false);
    this.view.tableView.flxRequestMoney.setVisibility(true);
    this.view.tableView.flxHorizontalLine2.setVisibility(false);
    this.view.tableView.flxTableHeaders.setVisibility(false);
    this.view.tableView.flxHorizontalLine3.setVisibility(false);
    this.view.tableView.segP2P.setVisibility(false);
  },
  showSendReminder: function () {
    this.view.tableView.flxTabs.btnSendRequest.skin = "sknBtnAccountSummaryUnselected";
    this.view.tableView.flxTabs.btnMyRequests.skin = "sknBtnAccountSummarySelected";
    this.view.tableView.flxTabs.btnSent.skin = "sknBtnAccountSummaryUnselected";
    this.view.tableView.flxTabs.btnRecieved.skin = "sknBtnAccountSummaryUnselected";
    this.view.tableView.flxTabs.btnManageRecepient.skin = "sknBtnAccountSummaryUnselected";
    this.view.tableView.flxSendMoney.setVisibility(false);
    this.view.tableView.flxSendReminder.setVisibility(true);
    this.view.tableView.flxRequestMoney.setVisibility(false);
    this.view.tableView.flxHorizontalLine2.setVisibility(false);
    this.view.tableView.flxTableHeaders.setVisibility(false);
    this.view.tableView.flxHorizontalLine3.setVisibility(false);
    this.view.tableView.segP2P.setVisibility(false);
  },
  showQuitScreen: function(actions, header, message) {
        if(header)
            this.view.CustomPopup1.lblHeading.text = header;
        if(message)
            this.view.CustomPopup1.lblPopupMessage.text = message;
        this.view.CustomPopup1.btnNo.onClick = actions.negative;
        this.view.CustomPopup1.btnYes.onClick = actions.positive;
        var height = this.view.flxHeader.frame.height + this.view.flxContainer.frame.height;
        this.view.flxQuit.height = height + "dp";
        this.view.flxQuit.left = "0%";
        this.view.flxQuit.setVisibility(true);
    },
  closeQuitScreen: function () {
    this.view.flxQuit.setVisibility(false);
  },
  toggleSearch: function () {
    if (this.view.tableView.Search.isVisible === true) {
      this.view.tableView.imgSearch.src = "search_blue.png";
      this.view.tableView.Search.isVisible = false;
    } else {
      this.view.tableView.imgSearch.src = "selecetd_search.png";
      this.view.tableView.Search.isVisible = true;
    }
  },
  
  showSecondaryActions: function() {
    this.view.secondaryActions.top = this.view.flxWhatElse.frame.y + 70 + "dp";
    if (this.view.secondaryActions.isVisible == true) {
        this.view.imgDropdown.src = "arrow_down.png";
        this.view.secondaryActions.isVisible = false;
    } else {
        this.view.imgDropdown.src = "arrow_up.png";
        this.view.secondaryActions.isVisible = true;
        if(this.view.flxTableView.frame.height <= this.view.flxRightWrapper.frame.height)
           this.AdjustScreen(100);
    }
    this.view.secondaryActions.setFocus(true);
},
}
});