define(function() {

	return {
		enableSearchButton: function () {
			this.view.btnSearch.skin = "sknbtnLatoffffff15px";
			this.view.btnSearch.setEnabled(true);
			this.view.btnSearch.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
			this.view.btnSearch.focusSkin="sknBtnFocusLatoFFFFFF15Px";
		},
		disableSearchButton: function () {
			this.view.btnSearch.setEnabled(false);
			this.view.btnSearch.skin="sknBtnBlockedLatoFFFFFF15Px";
			this.view.btnSearch.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";  
			this.view.btnSearch.focusSkin="sknBtnBlockedLatoFFFFFF15Px"; 
		},
		showByDateWidgets: function () {
			this.view.lblByDate.setVisibility(true);
			this.view.flxByDate.setVisibility(true);
			this.view.flxBlankSpace.setVisibility(false);
		},
		hideByDateWidgets: function () {
			this.view.lblByDate.setVisibility(false);
			this.view.flxByDate.setVisibility(false);
			this.view.flxBlankSpace.setVisibility(false);
		},
		setSearchVisible: function (isVisible) {
			this.view.flxSeparatorSearch.setVisibility(isVisible);
			this.view.flxSearchContainer.setVisibility(isVisible);
			this.view.imgSearch.src = (isVisible || this.view.flxSearchResults.isVisible) ? "selecetd_search.png": "search_blue.png"  			
		},
		setSearchResultsVisible : function(isVisible) {
			this.view.imgSearch.src = (isVisible || this.view.flxSearchContainer.isVisible) ? "selecetd_search.png": "search_blue.png"
			this.view.flxSeparatorSearch.setVisibility(isVisible);
			this.view.flxSearchResults.setVisibility(isVisible);
		}
	};
});