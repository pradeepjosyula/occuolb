define(function() {

	return {
      
      //function to set message badge size as per number of digits 
      setMessageBadgeSize: function()
      {
        var numberOfMessages = parseInt(this.view.lblNewMessages.text);
        if(numberOfMessages<=99)
          {
            this.view.lblNewMessages.width="15dp";
            this.view.lblNewMessages.height="15dp";
          }
        else
          {
            
            this.view.lblNewMessages.width="20dp";
            this.view.lblNewMessages.height="20dp";
          }
      },
	  headerPreShow:function(){
          if(kony.onlineBanking.configurations.getConfiguration("enableAlertsIcon")==="true" && kony.mvc.MDAApplication.getSharedInstance().appContext.isUserLogged){
               this.view.flxMessages.isVisible=true;
          }
          else{
           		this.view.flxMessages.isVisible=false;
          }
        this.view.forceLayout();
      }
      

	};
});