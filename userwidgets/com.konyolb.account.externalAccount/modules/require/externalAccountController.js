define(['CommonUtilities'],function(CommonUtilities) {
    
 
	return {
		checkboxActionDomestic:function(){
          CommonUtilities.toggleCheckBox(this.view.imgchecked);
        },
      	checkboxActionInternational:function(){
          CommonUtilities.toggleCheckBox(this.view.imgcheckboxchecked);
        },
	};
});