define(['CommonUtilities'], function (CommonUtilities) {

  function getTotalHeight() {
    var height = 0;
    var widgets = kony.application.getCurrentForm().widgets();
    for (var i = 0; i <= 1; i++) {
      var widget = widgets[i];
      height += widget.frame.height;
    }
    return height;
  }

  return {
    initHamburger : function (sideMenuModel, menuIndex, subMenuIndex) {
      if(CommonUtilities.getConfiguration("billPayEnabled")==="true") {
        this.view.customhamburger.flxBillPay.setVisibility(true);
      } else {
        this.view.customhamburger.flxBillPay.setVisibility(true);
      }
      this.view.customhamburger.flxMyAccounts.onClick = sideMenuModel.contents.myAccounts.onSelect;
      this.view.customhamburger.flxStatements.onClick = sideMenuModel.contents.statements.onSelect;
      this.view.customhamburger.flxTransfersMoney.onClick = sideMenuModel.contents.transferMoney.onSelect;
      this.view.customhamburger.flxTransferHistory.onClick = sideMenuModel.contents.transferHistory.onSelect;
      this.view.customhamburger.flxExternalAccounts.onClick = sideMenuModel.contents.externalAccounts.onSelect;
      this.view.customhamburger.flxAddKonyAccounts.onClick = sideMenuModel.contents.addKonyAccounts.onSelect;
      this.view.customhamburger.flxAddNonKonyAccounts.onClick = sideMenuModel.contents.addNonKonyAccounts.onSelect;
      this.view.customhamburger.flxPayABill.onClick = sideMenuModel.contents.payABill.onSelect;
      this.view.customhamburger.flxAddPayee.onClick = sideMenuModel.contents.addPayee.onSelect;
      this.view.customhamburger.flxClose.onClick = sideMenuModel.closeHamburgerAction;
      this.view.flxHamburgerBack.onClick=sideMenuModel.closeHamburgerAction;
	  this.view.customhamburger.flxBillPayHistory.onClick = sideMenuModel.contents.billPayHistory.onSelect;
	  this.view.customhamburger.flxMyPayeeList.onClick =  sideMenuModel.contents.myPayeeList.onSelect;
      this.view.customhamburger.flxLocateUs.onClick =  sideMenuModel.contents.locateUs.onSelect;
	  this.view.customhamburger.flxAlerts.onClick = sideMenuModel.contents.alerts.onSelect;
	  this.view.customhamburger.flxMessages.onClick = sideMenuModel.contents.messages.onSelect;
	  this.view.customhamburger.flxNewMessages .onClick = sideMenuModel.contents.newMesssage.onSelect;
	  this.view.customhamburger.flxSendRequest.onClick = sideMenuModel.contents.SendRequest.onSelect;
      //this.view.customhamburger.flxMyRequests.onClick = sideMenuModel.contents.MyRequests.onSelect;
      this.view.customhamburger.flxHistory.onClick = sideMenuModel.contents.P2pHistory.onSelect;
      this.view.customhamburger.flxMyRecipients.onClick = sideMenuModel.contents.MyRecipients.onSelect;
      this.view.customhamburger.flxAddRecipients.onClick = sideMenuModel.contents.AddRecipient.onSelect;
    //  this.view.customhamburger.flxTerms.onClick = sideMenuModel.contents.termsAndConditions.onSelect;
     // this.view.customhamburger.flxPrivacyPolicy.onClick = sideMenuModel.contents.privacyPolicy.onSelect;
      if (menuIndex !== undefined && subMenuIndex !== undefined){
      this.view.customhamburger.activateMenu(menuIndex, subMenuIndex);
	  }
      if (sideMenuModel.isMainMenuVisible) {
        this.openHamburgerMenu();
      }
      if (!sideMenuModel.isMainMenuVisible) {
        this.closeHamburgerMenu();
      }
  
      this.view.forceLayout();
    },
    initTopBar: function (topBarModel) {
      if(CommonUtilities.getConfiguration("billPayEnabled")==="true") {
        this.view.topmenu.flxPayBills.setVisibility(true);
      } else {
        this.view.topmenu.flxPayBills.setVisibility(true);
      }
    if(CommonUtilities.getConfiguration("ispayAPersonEnabled")==="true") {
        this.view.topmenu.flxSendMoney.setVisibility(true);
      } else {
        this.view.topmenu.flxSendMoney.setVisibility(false);
      }
      /*if(kony.mvc.MDAApplication.getSharedInstance().appContext.userWireTransferData.isWireTransferEligible){
        this.view.topmenu.flxWireMoney.setVisibility(true);
      } else{
        this.view.topmenu.flxWireMoney.setVisibility(false);
      }*/
      this.view.headermenu.imgUserReset.src = topBarModel.userImage;
      this.view.lblUserEmail.text = topBarModel.email;
      this.updateTopBarUI(topBarModel);
      this.view.topmenu.flxMenu.onClick = topBarModel.menuAction;
      this.view.topmenu.flxaccounts.onClick = topBarModel.accountsAction;
      this.view.topmenu.flxTransfersAndPay.onClick = topBarModel.transfersAction;
      this.view.topmenu.flxTransferMoney.onClick = topBarModel.trasferMoneyAction;
      this.view.topmenu.flxPayBills.onClick = topBarModel.payBillAction;
      this.view.topmenu.flxSendMoney.onClick=topBarModel.payAPersonAction;
      this.view.topmenu.flxFeedbackimg.onClick=topBarModel.feedbackAction;
	  this.view.topmenu.flxWireMoney.onClick=topBarModel.wireTransferAction;
    },
    updateTopBarUI: function (topBarModel) {
      if (topBarModel.toggle == "accounts") {
        kony.print("accounts");
        this.view.topmenu.navigatetoAccounts();
      } else if (topBarModel.toggle == "contextualMenu") {
        kony.print("contextualMenu");
        this.view.topmenu.showContextualMenu();
      } else if (topBarModel.toggle == "transfers") {
        kony.print("transfers");
        this.view.topmenu.navigateToTransfers();
      } else if (topBarModel.toggle == "billPay") {
        kony.print("billPay");
        this.view.topmenu.navigateToBillPay();
      }
    },
    setUpLogout: function () {
      this.view.customhamburger.flxLogout.onClick = function () {
        var currentForm = kony.application.getCurrentForm();                    
          if ('CustomPopup' in  currentForm && 'flxLogout' in currentForm) {
            this.closeHamburgerMenu();
              currentForm.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
              currentForm.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
              var height = currentForm.widgets()[0].frame.height + currentForm.widgets()[1].frame.height;
              currentForm.flxLogout.setVisibility(true);              
              currentForm.flxLogout.height = height + "dp";
              currentForm.flxLogout.left = "0%";
              currentForm.CustomPopup.btnYes.onClick = function () {
                  var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                  var context = {
                    "action": "Logout"
                  };
                  authModule.presentationController.doLogout(context);
                  currentForm.flxLogout.left = "-100%";
                };
                currentForm.CustomPopup.btnNo.onClick = function () {
                  currentForm.flxLogout.left = "-100%";
                }
                currentForm.CustomPopup.flxCross.onClick = function () {
                  currentForm.flxLogout.left = "-100%";
                }
          }
          
      }.bind(this);
  },
    initHeader: function () {
      this.view.flxUserActions.isVisible = false;
      this.view.segUserActions.onRowClick = function (widget, section, rowIndex) {
        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
        if (rowIndex === 0) {
          profileModule.presentationController.showProfileSettings();
        }
		if (rowIndex === 1) {
          profileModule.presentationController.initializeUserProfileClass();
          profileModule.presentationController.showPreferredAccounts();
        }
		if (rowIndex === 2) {
          //var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
          //alertsModule.presentationController.showAlertsPage();
          profileModule.presentationController.initializeUserProfileClass();
          profileModule.presentationController.fetchAccountAlerts();
        }
      };
      this.view.headermenu.flxMessages.onClick = function(){
        var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
        alertsModule.presentationController.showAlertsPage();
      };
      
      this.view.topmenu.flxHelp.onClick = function(){
        var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
      	var formId = kony.application.getCurrentForm().id;
        informationContentModule.presentationController.showOnlineHelp(formId);
      }
      this.setUpLogout();      
    },
    openHamburgerMenu: function () {
      this.showBackFlex();
      var animationDefinition = {
        100: {
          "left": 0
        }
      };
      var animationConfiguration = {
        duration: 0.5,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function () {

        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      this.view.flxHamburger.animate(animationDef, animationConfiguration, callbacks);
    },
    closeHamburgerMenu: function () {
      this.hideBackFlex();
      var animationDefinition = {
        100: {
          "left": "-35.13%"
        }
      };
      var animationConfiguration = {
        duration: 0.5,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function () {

        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      this.view.flxHamburger.animate(animationDef, animationConfiguration, callbacks);
    },
    showBackFlex: function () {
      this.view.flxHamburgerBack.height = getTotalHeight() + "px";
      this.view.forceLayout();
    },
    hideBackFlex: function () {
      this.view.flxHamburgerBack.height = "0px";
      this.view.forceLayout();
    },
    showUserActions: function () {
      if (this.view.flxUserActions.isVisible === false) {
        this.view.headermenu.imgDropdown.src = "profile_dropdown_uparrow.png";        
        this.view.flxUserActions.isVisible = true;
      } else {
        this.view.headerMenu.imgDropdown.src = "profile_dropdown_arrow.png";
        this.view.flxUserActions.isVisible = false;
      }
      /**
      var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
      alertsModule.presentationController.initializeUserProfileClass();
      alertsModule.presentationController.fetchAccountAlerts();
      */
    },
    NavigateToCustomerFeedback: function() {
       var navObj = new kony.mvc.Navigation("frmCustomerFeedback");
       navObj.navigate();
    },
	forceCloseHamburger: function () {
      this.hideBackFlex();
      this.view.flxHamburger.left = "-35.13%";
      this.view.forceLayout();
    },
    postShowFunction: function () {
      this.view.flxUserActions.right = 180 + this.view.FlexContainer0e2898aa93bca45.frame.x + "dp";
      var showNewMessagesIcon = kony.mvc.MDAApplication.getSharedInstance().appContext.hasUnreadMessagesOrNotifications;
      if( showNewMessagesIcon || showNewMessagesIcon == undefined) {
        this.view.headermenu.lblNewMessages.setVisibility(true);
      } else {
        this.view.headermenu.lblNewMessages.setVisibility(false);
      }
    },
    showSelectedRow: function () {
      var index = kony.application.getCurrentForm().customheader.segUserActions.selectedRowIndex[1];
      if (index === 0) {
        var navObj = new kony.mvc.Navigation("frmProfileManagement");
        navObj.navigate();
      } else if (index === 1) {} else {
        var navObj1 = new kony.mvc.Navigation("frmNotificationsAndMessages");
        navObj1.navigate();
      }
    }
  };
});