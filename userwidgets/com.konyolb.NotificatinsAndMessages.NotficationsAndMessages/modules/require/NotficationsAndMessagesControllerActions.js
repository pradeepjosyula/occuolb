define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnNewMessage **/
    AS_Button_da79bb3e61f7468e8eed70f740bdeb9c: function AS_Button_da79bb3e61f7468e8eed70f740bdeb9c(eventobject) {
        var self = this;
        this.NewMessage();
    },
    /** onClick defined for btnNotifications **/
    AS_Button_e3323eeced5847fbab8fbfd23f60ada6: function AS_Button_e3323eeced5847fbab8fbfd23f60ada6(eventobject) {
        var self = this;
        this.setNotificationSegmentData();
    },
    /** onClick defined for btnMyMessages **/
    AS_Button_a0760bc3c08a4221ae50a28027d471fb: function AS_Button_a0760bc3c08a4221ae50a28027d471fb(eventobject) {
        var self = this;
        this.setMessagesSegmentData();
    }
});