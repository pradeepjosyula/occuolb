define(['CommonUtilities'], function(CommonUtilities) {
  var originalAmount = 0;

	return {
//      createACustomCalendarWidget:function(){
//      //The below function is the callback function for onSelection event
//      function onSelectionCallBck(calendar)
//      {
//       // alert("onSelection event triggered");
//      }



//      var calLayoutConf = {padding:[2,2,2,2], margin:[5,5,5,5],containerWeight:100, hExpand:true, vExpand:true};

//      var calPSPConf = {};

//      //Creating the Calendar.
     
//      var calBasicConf = {id: "calSelectedOn","width":"100%",isVisible:true, skin:"sknCalendarTransparent", dateFormat:"dd/MM/yyyy",
//                          viewType:constants.CALENDAR_VIEW_TYPE_GRID_POPUP, validStartDate:[01,01,2012], validEndDate:[31,12,2012],
//                          placeholder:"dd/MM/yyyy", calendarIcon:"calender.png", onSelection:onSelectionCallBck};
//      var Calendar= new kony.ui.Calendar(calBasicConf, calLayoutConf, calPSPConf);
     
     

//      //Reading the titleOnPopup property of calendar widget
//    //  alert("Calendar titleOnPopup ::"+Calendar.titleOnPopup);
//        if(this.view["calSelectedOn"]==undefined){
//      		this.view.flxCalendar.add(Calendar);
//        }
     
//    }
      getFrequencyAndFormLayout:function(frequencyValue, howLangValue)
        {
          
        if(frequencyValue === "Now") {
          this.makeLayoutfrequencyNow();          
        }       
        else if(frequencyValue!=="Once" && frequencyValue!=="Now")
          {
            this.makeLayoutfrequencyWeeklyDate();
          }
         else if(frequencyValue === "Twice_a_Month")
          {
            this.makeLayoutfrequencyTwiceMonth();
          }
        else
          {
            this.makeLayoutfrequencyOnce();
          }
      },
      getForHowLongandFormLayout:function(value)
      {
        
        
//           if(value==="ON_SPECIFIC_DATE")
//             {
//               this.makeLayoutfrequencyWeeklyDate();
//             }
//           else if(value==="NO_OF_RECURRENCES")
//             {
//               this.makeLayoutfrequencyWeeklyRecurrences();
//             }
//           else if(value==="CONTINUE_UNTIL_CANCEL")
//             {
//               this.makeLayoutfrequencyWeeklyCancel();
//             }
        
        if(value==="DATE_RANGE")
          {
            this.makeLayoutfrequencyWeeklyDate();
          }
        else if(value==="NO_OF_RECURRENCES")
          {
            this.makeLayoutfrequencyWeeklyRecurrences();
          }
        else if(value==="UNTILL_CANCELLED")
          {
            this.makeLayoutfrequencyWeeklyCancel();
          }
        
        
      },
      makeLayoutfrequencyWeeklyDate:function()
    {
      this.view.lblForhowLong.setVisibility(true);
      this.view.lbxForHowLong.setVisibility(false);
      this.view.flexswitchOff.setVisibility(true);
      this.view.flexswitchOn.setVisibility(true);
      this.view.flxCalEndingOn.setVisibility(true);
     // this.view.lblNoOfRecOrEndingOn.text=kony.i18n.getLocalizedString("i18n.transfers.lblendingon");
      this.view.lblNoOfRecOrEndingOn.text=kony.i18n.getLocalizedString("i18n.transfers.lblendingDate");
      this.view.lblNoOfRecOrEndingOn.setVisibility(true);
      this.view.tbxNoOfRecurrences.setVisibility(false);
      this.view.flxCalSendOn.setVisibility(false);
      //this.view.lblSendOn.setVisibility(true);
      //this.view.flxCalSendOn.setVisibility(false);
      this.view.lblSendOn.setVisibility(false);
      this.view.flxStartDate1.setVisibility(true);
      this.view.lblStart.setVisibility(true);
      this.view.flxlbltwicemonthly.setVisibility(false);
      this.view.forceLayout();      
    },
    makeLayoutfrequencyTwiceMonth: function(){
      this.view.lblForhowLong.setVisibility(true);
      this.view.lbxForHowLong.setVisibility(false);
      this.view.flexswitchOff.setVisibility(true);
      this.view.flexswitchOn.setVisibility(true);
      this.view.flxlbltwicemonthly.setVisibility(false);
      this.view.flxCalEndingOn.setVisibility(true);
     // this.view.lblNoOfRecOrEndingOn.text=kony.i18n.getLocalizedString("i18n.transfers.lblendingon");
      this.view.lblNoOfRecOrEndingOn.text=kony.i18n.getLocalizedString("i18n.transfers.lblendingDate");
      this.view.lblNoOfRecOrEndingOn.setVisibility(true);
      this.view.tbxNoOfRecurrences.setVisibility(false);
      this.view.flxCalSendOn.setVisibility(false);
      //this.view.lblSendOn.setVisibility(true);
      //this.view.flxCalSendOn.setVisibility(false);
      this.view.lblSendOn.setVisibility(false);
      this.view.flxStartDate1.setVisibility(true);
      this.view.lblStart.setVisibility(true);
      this.view.forceLayout();      
    },
    makeLayoutfrequencyNow : function()
    {
      this.view.lblForhowLong.setVisibility(false);
      this.view.lbxForHowLong.setVisibility(false);
      this.view.flexswitchOff.setVisibility(false);
      this.view.flexswitchOn.setVisibility(false);
      this.view.flxCalEndingOn.setVisibility(false);
      this.view.lblNoOfRecOrEndingOn.setVisibility(false);
      this.view.flxCalSendOn.setVisibility(false);
      this.view.lblSendOn.setVisibility(false);
      this.view.flxlbltwicemonthly.setVisibility(false);
      this.view.lblNoOfRecOrEndingOn.setVisibility(false);
      this.view.tbxNoOfRecurrences.setVisibility(false);
       this.view.flxStartDate1.setVisibility(false);
      this.view.lblStart.setVisibility(false);
      this.view.forceLayout();     
    },
      makeLayoutfrequencyWeeklyRecurrences:function()
    {
      this.view.lblForhowLong.setVisibility(true);
      this.view.lbxForHowLong.setVisibility(false);
      this.view.flexswitchOff.setVisibility(true);
      this.view.flexswitchOn.setVisibility(true);
      this.view.flxCalEndingOn.setVisibility(false);
      this.view.lblNoOfRecOrEndingOn.text=kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences");
      this.view.lblNoOfRecOrEndingOn.setVisibility(true);
      this.view.tbxNoOfRecurrences.setVisibility(true);
      //this.view.flxCalSendOn.setVisibility(true);
      this.view.flxCalSendOn.setVisibility(false);
      //this.view.lblSendOn.setVisibility(true);
      this.view.lblSendOn.setVisibility(false);
      this.view.flxlbltwicemonthly.setVisibility(false);
      this.view.flxStartDate1.setVisibility(true);
      this.view.lblStart.setVisibility(true);
      
      
      this.view.forceLayout();
    },
      makeLayoutfrequencyWeeklyCancel:function()
    {
      this.view.lblForhowLong.setVisibility(true);
      this.view.lbxForHowLong.setVisibility(false);
      this.view.flexswitchOff.setVisibility(true);
      this.view.flexswitchOn.setVisibility(true);
      this.view.flxCalEndingOn.setVisibility(false);
      this.view.lblNoOfRecOrEndingOn.setVisibility(false);
      this.view.tbxNoOfRecurrences.setVisibility(false);
      //this.view.flxCalSendOn.setVisibility(true);
      //this.view.lblSendOn.setVisibility(true);
      this.view.flxStartDate1.setVisibility(true);
      this.view.flxlbltwicemonthly.setVisibility(false);
      this.view.lblStart.setVisibility(true);
      this.view.forceLayout();      
    },
      makeLayoutfrequencyOnce:function()
    {
      this.view.flexswitchOff.setVisibility(false);
      this.view.flexswitchOn.setVisibility(false);
      this.view.lblForhowLong.setVisibility(false);
      this.view.lbxForHowLong.setVisibility(false);
      this.view.flxCalEndingOn.setVisibility(false);
      this.view.lblNoOfRecOrEndingOn.setVisibility(false);
      this.view.tbxNoOfRecurrences.setVisibility(false);
      this.view.flxCalSendOn.setVisibility(true);
      this.view.lblSendOn.setVisibility(true);
      this.view.flxStartDate1.setVisibility(false);      
      this.view.lblStart.setVisibility(false);
      this.view.flxlbltwicemonthly.setVisibility(false);
      this.view.forceLayout();
    },
       renderCalendarMakeTransfer: function()
      {
        var context1={"widget":this.view.flxCalRenderereSendOn,"anchor":"bottom"};       
        this.view.calSendOn.setContext(context1);
        var context2={"widget":this.view.flxCalRenderereEndingOn,"anchor":"bottom"};       
        this.view.calEndingOn.setContext(context2);
        var context3={"widget":this.view.flxCalRenderereStartOn,"anchor":"bottom"};       
        this.view.calStart.setContext(context3);
      },
      setAmount: function  (amount) {
        originalAmount = amount.toString();
      },
      getAmount: function () {
        return originalAmount;
      },
      formatAndShow: function () {
        if (/^(?!0\.00)\d{1,3}(,\d{3})*(\.\d*)?$/.test(originalAmount) || /^\d*(\.\d+)?$/.test(originalAmount)) {
          originalAmount = originalAmount.replace(/\,/g,"")
          originalAmount = parseFloat(originalAmount).toFixed(2);
          var formattedText = CommonUtilities.formatCurrencyWithCommas(originalAmount);
          this.view.tbxAmount.text = formattedText === undefined ? CommonUtilities.formatCurrencyWithCommas("0").substr(1) : formattedText.substr(1) ; 
        }
      }
    };
});