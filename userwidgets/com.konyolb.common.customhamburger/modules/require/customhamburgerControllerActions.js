define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxPrivacyPolicy **/
    AS_FlexContainer_ab07db5ee452440bbf80226108610658: function AS_FlexContainer_ab07db5ee452440bbf80226108610658(eventobject) {
        var self = this;
        window.open("https://www.orangecountyscu.org/globalassets/pdf-files/disclosures/disc-online-privacy-policy-07172017.pdf");
    },
    /** onClick defined for flxContactUs **/
    AS_FlexContainer_jdf580ae9d294e208d6ad9b54986a2b6: function AS_FlexContainer_jdf580ae9d294e208d6ad9b54986a2b6(eventobject) {
        var self = this;
        window.open("https://www.orangecountyscu.org/about-us/contact-us/");
    },
    /** preShow defined for customhamburger **/
    AS_FlexContainer_ad6fe612c8ad419fb59fcca54e0f9d3b: function AS_FlexContainer_ad6fe612c8ad419fb59fcca54e0f9d3b(eventobject) {
        var self = this;
        this.initHamburger();
    },
    /** postShow defined for customhamburger **/
    AS_FlexContainer_b0431e42e1f34c418a8dd4eba9cc938e: function AS_FlexContainer_b0431e42e1f34c418a8dd4eba9cc938e(eventobject) {
        var self = this;
        this.postShowHamburger();
    }
});