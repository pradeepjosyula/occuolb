define(['HamburgerConfig'], function (HamburgerConfig) {
    return {
        toggle: function (widget, imgArrow) {
            var scope = this;
            if (widget.frame.height > 0) {
                this.collapseAll();
            } else {
                this.collapseAll();
                if(imgArrow.src === "arrow_down.png"){
                   imgArrow.src = "arrow_up.png";
                   imgArrow.toolTip="Collapse";
                   this.view.forceLayout();
                }
                this.expand(widget);
            }

            // if (widget.frame.height === 0) {
            //     // if (this.expanded !== null) {
            //     //     this.rotate(this.expanded.imgArrow, 360);
            //     //     this.collapseWithoutAnimation(this.expanded.widget);
            //     // }
            //     // this.expanded = {
            //     //     widget: widget,
            //     //     imgArrow: imgArrow
            //     // };
            //     this.rotate(imgArrow, 180);
            //     this.expand(widget);
            // } else {
            //     if (this.expanded.widget === widget) {
            //         this.expanded = null;
            //     }
            //     this.rotate(imgArrow, 360);
            //     this.collapse(widget);
            // }
        },

        collapseWithoutAnimation: function (widget) {
            widget.height = 0;
            this.view.forceLayout();
        },
        expandWithoutAnimation: function (widget) {
            widget.height = widget.widgets().length * 60;
            this.view.forceLayout();
        },

        checkLogoutPosition: function () {
            var top = "";
            kony.print(this.view.flxMenu.frame);
            kony.print(this.view.flxLogout.frame);
            var logoutY = this.view.flxLogout.frame.y;
            var menuHeight = this.view.flxMenu.frame.height;
            if (menuHeight > this.logoutY) {
                top = menuHeight + 'px';
            } else {
                top = 80 + "%";
            }
            var scope = this;
            var animationDefinition = {
                100: {
                    "top": top
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function () {

                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            this.view.flxLogout.animate(animationDef, animationConfiguration, callbacks);
        },

        rotate: function (image, angle) {
            var scope = this;
            var rotateTransform = kony.ui.makeAffineTransform();
            rotateTransform.rotate(angle);
            var animDefinitionOne = {
                100: {
                    "transform": rotateTransform
                }
            };
            var callbacks = {
                animationEnd: function () {
                    scope.view.forceLayout();
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            animDef = kony.ui.createAnimation(animDefinitionOne);
            image.animate(animDef, animationConfiguration, callbacks);
        },

        initHamburger: function () {
            for (var i = 0; i < HamburgerConfig.length; i++) {
                var menuObject = HamburgerConfig[i];
                this.view[menuObject.menu].onClick = this.getMenuHandler(this.view[menuObject.subMenu.parent], this.view[menuObject.image]);
            }
        },
        collapseAll: function () {
            for (var i = 0; i < HamburgerConfig.length; i++) {
                var menuObject = HamburgerConfig[i];
                this.collapseWithoutAnimation(this.view[menuObject.subMenu.parent]);
                if(this.view[menuObject.image].src === "arrow_up.png"){
                   this.view[menuObject.image].src = "arrow_down.png";
                    this.view[menuObject.image].toolTip = "Expand";
                }
               this.view.forceLayout();
            }
        },
        resetSkins: function () {
            for (var i = 0; i < HamburgerConfig.length; i++) {
                var menuObj = HamburgerConfig[i];
                for (var j = 0; j < menuObj.subMenu.children.length; j++) {
                    var element = menuObj.subMenu.children[j].widget;
//                     this.view[element].skin = "sknFlxf6f6f6";
//                     this.view[element].hoverSkin = "sknFlxHoverHamburger";
                }

            }
        },
        activateMenu: function (parentId, childId) {
            var menuObject = HamburgerConfig.filter(function (menuItem) {
                return menuItem.id.toLowerCase() === parentId.toLowerCase()
            })[0];
            if (menuObject) {
                this.collapseAll();
                this.resetSkins();
                this.expandWithoutAnimation(this.view[menuObject.subMenu.parent]);
                if (childId) {
                    var childObject = menuObject.subMenu.children.filter(function (childItem) {
                        return childItem.id.toLowerCase() === childId.toLowerCase();
                    })[0];
                    if (childObject) {
                        this.view[childObject.widget].skin = "sknMenuSelected";
                        this.view[childObject.widget].hoverSkin = "sknMenuSelected"; 
                    }
                }
            }
          
        },
        setNavigationActions: function () {
            this.view.flxMyAccounts.onClick = function () {
                //  scopeObj.clearFocus();
                //    scopeObj.view.flxMyAccounts.skin=sknMenuSelected;
                var navObj = new kony.mvc.Navigation("frmAccountsLanding");
                navObj.navigate();
            };
            this.view.flxTransfersMoney.onClick = function () {
                //  scopeObj.view.flxTransfersMoney.skin=sknMenuSelected;
                var navObj = new kony.mvc.Navigation("frmTransfers");
                navObj.navigate();
            };
            this.view.flxTransferHistory.onClick = function () {
                //    scopeObj.view.flxTransferHistory.skin=sknMenuSelected;
                var navObj = new kony.mvc.Navigation("frmTransfers");
                navObj.navigate();
            };
            this.view.flxExternalAccounts.onClick = function () {
                //  scopeObj.view.flxExternalAccounts.skin=sknMenuSelected;
                var navObj = new kony.mvc.Navigation("frmTransfers");
                navObj.navigate();
            };
            this.view.flxAddKonyAccounts.onClick = function () {
                //  scopeObj.view.flxAddKonyAccounts.skin=sknMenuSelected;
//                 var navObj = new kony.mvc.Navigation("frmAddInternalAccount");
//                 navObj.navigate();
               var transfermod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
     			transfermod.presentationController.navigateToTransfer();
            };
            this.view.flxAddNonKonyAccounts.onClick = function () {
                //scopeObj.view.flxAddNonKonyAccounts.skin=sknMenuSelected;
//                 var navObj = new kony.mvc.Navigation("frmAddExternalAccount");
//                 navObj.navigate();
               var transfermod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
     			transfermod.presentationController.navigateToTransfer();
            };
            this.view.flxPayABill.onClick = function () {
                // scopeObj.view.flxPayABill.skin=sknMenuSelected;
                //var obj={"tabname":"payabill"};
                var navObj = new kony.mvc.Navigation("frmBillPay");
                navObj.navigate();
            };
            this.view.flxBillPayHistory.onClick = function () {
                // scopeObj.view.flxBillPayHistory.skin=sknMenuSelected;
                //var obj={"tabname":"history"};
                var navObj = new kony.mvc.Navigation("frmBillPay");
                navObj.navigate();
            };
            this.view.flxMyPayeeList.onClick = function () {
                // scopeObj.view.flxMyPayeeList.skin=sknMenuSelected;
                //var obj={"tabname":"payees"};
                var navObj = new kony.mvc.Navigation("frmBillPay");
                navObj.navigate();
            };
            this.view.flxAddPayee.onClick = function () {
                //scopeObj.view.flxAddPayee.skin=sknMenuSelected;
                var navObj = new kony.mvc.Navigation("frmAddPayee");
                navObj.navigate();
            };
            this.view.flxLocateUs.onClick = function () {
                var locateUsmod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
     			locateUsmod.presentationController.showLocateUsPage();
            };
        },
        getLayoutProperties: function () {
            kony.print(this.view.flxLogout.frame);
            this.logoutY = this.view.flxLogout.frame.y;
        },
        getMenuHandler: function (subMenu, collapseImage) {
            return function () {
                this.toggle(subMenu, collapseImage);
            }.bind(this);
        },
        expand: function (widget) {
            var scope = this;
            var animationDefinition = {
                100: {
                    "height": widget.widgets().length * 60
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function () {
                    // scope.checkLogoutPosition();
                    scope.view.forceLayout();
                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            widget.animate(animationDef, animationConfiguration, callbacks);
        },

        fixHamburgerheight: function () {
            this.view.height = kony.os.deviceInfo().screenHeight;
        },

        collapse: function (widget) {
            var scope = this;
            var animationDefinition = {
                100: {
                    "height": 0
                }
            };
            var animationConfiguration = {
                duration: 0.5,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var callbacks = {
                animationEnd: function () {
                    // scope.checkLogoutPosition();
                    scope.view.forceLayout();

                }
            };
            var animationDef = kony.ui.createAnimation(animationDefinition);
            widget.animate(animationDef, animationConfiguration, callbacks);
        },
      	postShowHamburger: function () {
          this.view.flxMenu.height = kony.os.deviceInfo().screenHeight + "px";
          this.view.forceLayout();
        }


    };
});