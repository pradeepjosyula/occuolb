define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_f75ec4f85ea34a63b092dad0826baa7c: function AS_FlexContainer_f75ec4f85ea34a63b092dad0826baa7c(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** postShow defined for topmenu **/
    AS_FlexContainer_b18a1ce8b88c4fea99c1dc1c82736e74: function AS_FlexContainer_b18a1ce8b88c4fea99c1dc1c82736e74(eventobject) {
        var self = this;
        this.fixContextualMenu();
        this.actionRegister();
    }
});