define(function() {

	return {
      actionRegister: function(){	//for non "MDA form"
        var scopeObj=this;
        if(kony.application.getCurrentForm().id==="frmAccountsLanding" && kony.application.getCurrentForm().id==="frmAccountsDetails" && kony.application.getCurrentForm().id==="frmBillPay")
          {
            //so that it pretends to be non clickable
          }
          else
          {
		    this.view.flxaccounts.onClick = function(){
				var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
              accountModule.presentationController.showAccountsDashboard();
              scopeObj.navigatetoAccounts();
                //this.view.flxTransfersAndPay.skin="slFbox";
           };
            this.view.flxFeedback.onClick = function() {
				var feedbackModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeedbackModule");
                feedbackModule.presentationController.showFeedback();

            };
            this.view.flxPayBills.onTouchEnd = function(){
              	scopeObj.navigateToBillPay();
            };
            this.view.flxTransfersAndPay.onClick = function(){
				scopeObj.showContextualMenu();
            };
            this.view.flxTransferMoney.onTouchEnd = function(){
				var transfersModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transfersModule.presentationController.showTransferScreen();
				scopeObj.navigateToTransfers();
                
      	   };         
          }
		},
      navigateToBillPay: function () {
        this.view.flxContextualMenu.isVisible = false;
        this.view.flxMenu.skin="slFbox";
        this.view.flxaccounts.skin="slFbox";
        this.view.flxTransfersAndPay.skin="sknFlxFFFFFbrdr3343a8";
        this.view.flxSeperator3.setVisibility(true);
        this.view.forceLayout();
     },
      showContextualMenu:function()
      {
        this.view.flxSeperator3.setVisibility(false);
        if(this.view.flxContextualMenu.isVisible === true)
          {
            this.view.flxTransfersAndPay.skin="slFbox";
            this.view.flxContextualMenu.isVisible =false;
            this.view.forceLayout(); 
          }
        else
          {
            this.view.flxContextualMenu.isVisible =true;
            this.view.flxTransfersAndPay.skin="sknFlxebebeb";
            this.view.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
            this.view.imgTransfers.src = "sendmoney_purple.png";
            this.view.forceLayout(); 
          }
        
      },
      
      navigateToTransfers: function () {
        this.view.flxContextualMenu.isVisible = false;
        this.view.flxMenu.skin="slFbox";
        this.view.flxaccounts.skin="slFbox";
        this.view.flxTransfersAndPay.skin="sknFlxFFFFFbrdr3343a8";
        this.view.flxSeperator3.setVisibility(true);
        this.view.forceLayout();
    },
      navigatetoAccounts: function () {
        this.view.flxMenu.skin="slFbox";
        this.view.flxTransfersAndPay.skin="slFbox";
        this.view.lblTransferAndPay.skin = "sknLblGtmffffffOccu";
        this.view.imgTransfers.src = "sendmoney.png";
        this.view.flxaccounts.skin="sknFlxFFFFFbrdr3343a8";
        this.view.flxSeperator3.setVisibility(false);
        this.view.forceLayout();
    },
//    	transferSkin:function(){
//       if(this.view.flxContextualMenu.isVisible === true){
//       this.view.flxContextualMenu.isVisible = false;
//         if(this.view.btnAccounts.skin=="sknBtnTopmenuFocusAccounts")
//           	this.view.btnTransfers.skin="sknBtnTopmenu2";
//         else
//       		this.view.btnTransfers.skin="sknBtnTopmenuFocusTransfers";
//     }else{
//       this.view.flxContextualMenu.isVisible = true;
//       this.view.btnTransfers.skin="sknBtnTopmenuEBEBEBTransfers";
//     }
   // },
     fixContextualMenu: function () {
     var flex_menu_width=20+this.view.lblMenu.frame.width+30;
     this.view.flxMenu.width=flex_menu_width+"dp";
     var flex_accounts_width=25+this.view.lblAccounts.frame.width+50;
     this.view.flxaccounts.width=flex_accounts_width+"dp";
     var flex_transfers_width=25+this.view.lblTransferAndPay.frame.width+50;
     this.view.flxTransfersAndPay.width=flex_transfers_width+"dp";
     var left_for_contextual_menu=flex_transfers_width+1;
     left_for_contextual_menu="-"+left_for_contextual_menu+"dp";
       kony.print("left:"+left_for_contextual_menu);
     this.view.flxContextualMenu.left=left_for_contextual_menu;
     this.view.flxContextualMenu.width=flex_transfers_width+"dp"; 
     this.view.flxMenusMain.forceLayout();
    }
 
	};
});