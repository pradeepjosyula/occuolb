define(function() {
	
	return {
      
      /**
        * Method to laad Information Module and show Locate us
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
		showLocateUsPage : function() {
          var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModule");
          locateUsModule.presentationController.showLocateUsPage();
        },
      
        /**
        * Method to laad Information Module and show FAQs
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      	showFAQs : function(){
        var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
        InformationContentModule.presentationController.showFAQs();
        },
      	 /**
        * Method to laad Information Module and show terms and conditions page
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      	showTermsAndConditions:function(){
          var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
          InformationContentModule.presentationController.showTermsAndConditions();
        },
      	        /**
        * Method to laad Information Module and show ContactUs Page.
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */

      	showContactUsPage:function(){
          var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
          InformationContentModule.presentationController.showContactUsPage();
        },
              /**
        * Method to laad Information Module and show privacy policy page.
        * @memberof customFooterController
        * @param {void}  - None
        * @returns {void} - None. 
        * @throws Exception - None
        */
      
      	showPrivacyPolicyPage:function(){
       	var InformationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
         InformationContentModule.presentationController.showPrivacyPolicyPage();
    	}
      	
	};
});