define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnLocateUs **/
    AS_Button_e8cb8d9f33b14a0d9835988b144e3bdf: function AS_Button_e8cb8d9f33b14a0d9835988b144e3bdf(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnPrivacyNote **/
    AS_Button_bce8b6d134e84691a5dbbc00df13e637: function AS_Button_bce8b6d134e84691a5dbbc00df13e637(eventobject) {
        var self = this;
        window.open("https://www.orangecountyscu.org/globalassets/pdf-files/disclosures/disc-occu-privacy-notice-federal.pdf");
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_b42de0fc287f46a6a1926c05ebb67938: function AS_Button_b42de0fc287f46a6a1926c05ebb67938(eventobject) {
        var self = this;
        //this.showPrivacyPolicyPage();
        window.open("https://www.orangecountyscu.org/globalassets/pdf-files/disclosures/disc-online-privacy-policy-07172017.pdf");
    },
    /** onClick defined for btnContactUs **/
    AS_Button_ae3b2f12740d495aa7baa953fc1a1aa2: function AS_Button_ae3b2f12740d495aa7baa953fc1a1aa2(eventobject) {
        var self = this;
        //this.showContactUsPage();
        window.open("https://www.orangecountyscu.org/about-us/contact-us/");
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_b6e3a15908934b27b2bcfb2e61ddf498: function AS_Button_b6e3a15908934b27b2bcfb2e61ddf498(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_h603cb7a68a448f7b17b4f28daf92f4e: function AS_Button_h603cb7a68a448f7b17b4f28daf92f4e(eventobject) {
        var self = this;
        this.showFAQs();
    }
});