function hidePopups() {

  var currFormObj = kony.application.getCurrentForm();
  var currForm = currFormObj.id;
  if (currForm === "frmAccountsDetails") {
    currFormObj.accountTypes.isVisible = false;
    currFormObj.moreActions.isVisible = false;
    if(currFormObj.imgAccountTypes.src === "arrow_up.png")
      currFormObj.imgAccountTypes.src = "arrow_down.png";
    if(currFormObj.imgSecondaryActions.src === "arrow_up.png")   
      currFormObj.imgSecondaryActions.src = "arrow_down.png";
    currFormObj.accountInfo.isVisible = false;
    currFormObj.AllForms.setVisibility(false);
  } else if (currForm === "frmScheduledTransactions") {
    currFormObj.accountTypes.isVisible = false;
    if(currFormObj.imgAccountTypes.src === "arrow_up.png")
      currFormObj.imgAccountTypes.src = "arrow_down.png";
    currFormObj.accountInfo.isVisible = false;
  } else if (currForm === "frmAccountsLanding") {
    currFormObj.accountListMenu.top = 280;
    currFormObj.accountListMenu.isVisible = false;
  }else if(currForm === "frmPayAPerson"){
    currFormObj.secondaryActions.isVisible = false;
    if(currFormObj.imgDropdown.src === "arrow_up.png")
      currFormObj.imgDropdown.src = "arrow_down.png";
    currFormObj.AllFormsConfirm.isVisible = false;
    currFormObj.AllForms.isVisible = false;   
  } else if (currForm === "frmLogin") {
    if(currFormObj.imgDropdown.src === "arrow_up.png")
      currFormObj.imgDropdown.src = "arrow_down.png";
    currFormObj.flxLanguagePicker.isVisible = false;
    currFormObj.AllForms.setVisibility(false);
  }
  else if(currForm === "frmAddExternalAccount") {
    currFormObj.externalAccount.AllForms.setVisibility(false);  
    currFormObj.AllForms.setVisibility(false);     
  }
  else if(currForm === "frmAddInternalAccount") {
    currFormObj.internalAccount.AllForms.setVisibility(false);
    currFormObj.AllForms.setVisibility(false);     
  }
  else if(currForm === "frmBillPay") {
    currFormObj.AllForms.isVisible = false;     
  }
  else if(currForm == "frmNotificationsAndMessages"){
    currFormObj.customheader.flxUserActions.isVisible=false;
    currFormObj.AllForms.isVisible = false;
  }
  else if(currForm == "frmPayDueAmount"){
    currFormObj.customheader.flxUserActions.isVisible=false;
    currFormObj.LoanPayOff.AllForms.isVisible = false;
  }
  else if(currForm == "frmEnrollNow") {
    currFormObj.AllForms.setVisibility(false);
  }
  else if(currForm == "frmProfileManagement") {
    currFormObj.settings.AllForms1.isVisible = false;
    currFormObj.settings.AllForms.isVisible = false;
  }
  else if(currForm == "frmCustomerFeedback") {
    currFormObj.Feedback.AllForms.isVisible = false;
  }
  else if(currForm == "frmAddPayee"){
    currFormObj.flxPayeeList.setVisibility(false);
    currFormObj.oneTimePay.flxPayeeList.setVisibility(false);
  }
  //function to adjust visibility of contextual menu as per requirement.
  if (currForm !== "frmLogin" && currForm !== "frmEnrollNow") {
    if (currFormObj.customheader.topmenu.flxContextualMenu.isVisible === true) {
      currFormObj.customheader.topmenu.flxContextualMenu.isVisible = false;
      if ((currForm === "frmAccountsDetails") || (currForm === "frmAccountsLanding") || (currForm === "frmLocateUs")  ) {
        //currFormObj.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";

        currFormObj.customheader.topmenu.flxTransfersAndPay.skin="slFbox";
        currFormObj.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtmffffffOccu";
        currFormObj.customheader.topmenu.imgTransfers.src = "sendmoney.png"; 


      } else {
        currFormObj.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
        currFormObj.customheader.topmenu.flxSeperator3.isVisible = true;
      }
    }
    if(currFormObj.customheader.headermenu.imgDropdown.src === "profile_dropdown_uparrow.png")
      currFormObj.customheader.headermenu.imgDropdown.src = "profile_dropdown_arrow.png";
    currFormObj.customheader.flxUserActions.isVisible = false;
  }

}