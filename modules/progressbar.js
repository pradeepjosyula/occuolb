kony = kony || {};
kony.olb = kony.olb || {};
kony.olb.utils = kony.olb.utils || {};

kony.olb.utils.showProgressBar = function (view) {
    if(view === null || view === undefined) {
        return;
    }
    if ('flxLoading' in view) {
        view['flxLoading'].setVisibility(true);
        view.forceLayout();    
    }
    else {
        kony.print("No Progress bar available in the form. Add a progress bar with widget name flxLoading");
    }
}

kony.olb.utils.hideProgressBar = function (view) {
    if(view === null || view === undefined) {
        return;
    }
    if ('flxLoading' in view && view['flxLoading'].isVisible) {
        view['flxLoading'].setVisibility(false);
        view.forceLayout();
    }
}