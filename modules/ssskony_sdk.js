//Type your code here
function konyNetworkProvider() {

	this.post = function(url, params, headers, successCallback, failureCallback, konyContentType, options) {
        //Appending global params
        if(kony.sdk.isNullOrUndefined(params))
            params = {};
      if(konyRef){
       url = konyRef.appendGlobalParams(url, headers, params); 
      }
        
		if (typeof(XMLHttpRequest) !== 'undefined') {
			konyXMLHttpRequest(url, params, headers, "POST", konyContentType, successCallback, failureCallback, options);
		} else {
			konyNetHttpRequest(url, params, headers, "POST", konyContentType, successCallback, failureCallback, options);
		}
	};
	this.put = function(url, params, headers, successCallback, failureCallback, konyContentType, options) {
        //Appending global params
        if(kony.sdk.isNullOrUndefined(params))
            params = {};
        if(konyRef){
       url = konyRef.appendGlobalParams(url, headers, params); 
      }
      
		if (typeof(XMLHttpRequest) !== 'undefined') {
			konyXMLHttpRequest(url, params, headers, "PUT", konyContentType, successCallback, failureCallback, options);
		} else {
			konyNetHttpRequest(url, params, headers, "PUT", konyContentType, successCallback, failureCallback, options);
		}
	};
	this.invokeDeleteRequest = function(url, params, headers, successCallback, failureCallback, konyContentType, options) {
        //Appending global params
        if(kony.sdk.isNullOrUndefined(params))
            params = {};
        if(konyRef){
       url = konyRef.appendGlobalParams(url, headers, params); 
      }
      
		if (typeof(XMLHttpRequest) !== 'undefined') {
            konyXMLHttpRequest(url, params, headers, "DELETE", konyContentType, successCallback, failureCallback, options);
		} else {
			konyNetHttpRequest(url, params, headers, "DELETE", konyContentType, successCallback, failureCallback, options);
		}
	};
    //postSync will only work for Richclients like Android,IOS
	this.postSync = function(url,param,headers){
        //Appending global params
        if(kony.sdk.isNullOrUndefined(param))
            param = {};
        if(konyRef){
       url = konyRef.appendGlobalParams(url, headers, params); 
      }
      
	  	return konyNetHttpRequestSync(url,param,headers);
	};

	this.get = function(url, params, headers, successCallback, failureCallback, konyContentType, options) {
        //Appending global params
		if(konyRef){
       url = konyRef.appendGlobalParams(url, headers, params); 
      }
      
		if (typeof(XMLHttpRequest) !== 'undefined') {
			konyXMLHttpRequest(url, params, headers, "GET", konyContentType, successCallback, failureCallback, options);
		} else {
			konyNetHttpRequest(url, params, headers, "GET", konyContentType, successCallback, failureCallback, options);
		}
	}
}
