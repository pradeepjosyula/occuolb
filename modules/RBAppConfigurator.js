//Type your code here
kony = kony || {};
kony.servicesapp = kony.servicesapp || {};

/**
  *Inputparams :backendUrl 
   loads different types of configurations
*/
kony.servicesapp.loadAndConfigureApp = function(backendUrl, success) {
    backendUrl = backendUrl + "/services/data/v1";
    var contextObject = {};
    contextObject.app_id = "BaseRBMDA";
	contextObject.channels=kony.os.deviceInfo().name;
  	
  	contextObject.device=kony.os.deviceInfo().model;

    var appController = AppConfigurationController.getInstance(backendUrl);
    appController.getConfigurations(contextObject, function(configurationObject,type) { 
      	if(type=="PREFERENCE")
        PreferenceConfigHandeller.getInstance().loadConfigurationBasedPreferences(configurationObject);
        
        success();
    }, true);
};

/**
  *to fetch skin configurations
 */
var SkinConfigHandller = (function() {
    var instance;

    function createInstance() {
        this.loadConfigurationBasedSkinsAndApply = function(configurationObject) {
            try {
                var jsonString = configurationObject.getConfigurationsBasedOnType("SKIN"); //JSON.stringify(theme);
              
                function onsuccesscallback() {}

                function onerrorcallback() {}
                for (var c in jsonString) {
                    jsonString[c] = JSON.parse(jsonString[c]);
                }
                var date = new Date();
                kony.theme.createThemeFromJSONString(JSON.stringify(jsonString), date.toISOString(), onsuccesscallback, onerrorcallback);
                kony.theme.setCurrentTheme(date.toISOString(), onsuccesscallback, onerrorcallback);

            } catch (e) {
                alert("error in skin handler");
            }
        };
    }
    return {
        getInstance: function() {
            if (!instance) instance = new createInstance();
            return instance;
        }
    }
})();
kony.servicesapp.skinConfigHandler=SkinConfigHandller;

/**
 *to fetch image configurations
 */
var ImageAssetConfigHandler = (function() { //ImageAssetConfigHandler
    var instance;
	 
    function createInstance() {
        var imagesObject;
        this.loadConfigurationBasedImages = function(configurationObject) {
            var imagesObject = configurationObject.getConfigurationsBasedOnType("IMAGE");

            function fetchImage(image) {
                var httpclient = new kony.net.HttpRequest();
                httpclient.open(constants.HTTP_METHOD_GET, imagesObject[image]);
                httpclient.send();
                httpclient.onReadyStateChange = function() {
                    if (this.readyState == constants.HTTP_READY_STATE_DONE) {
                        var rb = httpclient.response;
                      	rb=httpclient.responseText;
                        var myfile = new kony.io.File(kony.io.FileSystem.getDataDirectoryPath() + "/" + image + ".jpg");
                        myfile.write(rb, true);
                      	
                    }
               }
            }
            for (var image in imagesObject) {
                fetchImage(image);
            }
            //});
        };
        this.getImage = function(imageName) {
            var imageFile = kony.io.FileSystem.getFile(kony.io.FileSystem.getDataDirectoryPath() + "/" + imageName + ".jpg");
            return imageFile.read() != undefined ? imageFile.read() : null;
            
        };

    }
    return {
        getInstance: function() {
            if (!instance) instance = new createInstance();
            return instance;
        }
    }
})();
kony.servicesapp.imageConfigHandler=ImageAssetConfigHandler;

/**
 *to fetch preferences configurations
 */
var PreferenceConfigHandeller = (function() {
    var instance;

    function createInstance() {
        var preferences = {};
      	
        this.loadConfigurationBasedPreferences = function(configurationObject) {
            preferences = configurationObject.getConfigurationsBasedOnType("PREFERENCE");
            
          
        };
        this.getPreferenceValue = function(key) {
            return preferences[key] != undefined ? preferences[key] : null;
        }
    }
    return {
        getInstance: function() {
            if (!instance) instance = new createInstance();
            return instance;
        }
    }

})();
kony.servicesapp.preferenceConfigHandler=PreferenceConfigHandeller;