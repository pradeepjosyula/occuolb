define(function () {

  return {

    bindMapData : function(data) {
      kony.print("bindMapData -> "+JSON.stringify(data));
      var scopeObj = this;
      var mapData = data;
      if(mapData.length>1) {
        mapData = JSON.parse(JSON.stringify(data));
        mapData.map(function(dataItem){
          dataItem.calloutData.flxLocationDetails.onClick = function() {
            scopeObj.view.LocateUs.mapLocateUs.dismissCallout();
            scopeObj.onRowClickSegment(dataItem);
            scopeObj.pinToSegment(dataItem);
            scopeObj.showBranchDetails();
          };
          return dataItem;
        });
        mapData.push(this.getCurrentLocationData());
      }
      this.view.LocateUs.flxMap.setVisibility(true);
      this.view.LocateUs.flxBranchDetails.setVisibility(false);
      this.view.LocateUs.flxHideMap.setVisibility(false);
      this.view.LocateUs.mapLocateUs.calloutTemplate = "flxDistanceDetails";
      this.view.LocateUs.mapLocateUs.zoomLevel = 12;
      this.view.LocateUs.mapLocateUs.widgetDataMapForCallout = {
        "lblBranchName": "lblBranchName",
        "lblBranchAddressOneLine": "lblBranchAddressOneLine",
        "lblBranchAddress2": "lblBranchAddress2",
        "lblOpen": "lblOpen",
        "lblClosed": "lblClosed",
        "imgGo": "imgGo",
        "flxLocationDetails":"flxLocationDetails",
      };
      this.view.LocateUs.mapLocateUs.locationData = mapData;
    },

    searchData:function(searchText){
      var scopeObj=this;
      var queryParams = {
        "query" : searchText.trim()
      };
      scopeObj.presenter.getSearchBranchOrATMList(queryParams);
    },

    updateSearchSegmentResult : function(locationListViewModel){
      kony.print("updateSearchSegmentResult locationListViewModel -> "+JSON.stringify(locationListViewModel));
      var scopeObj=this;
      if(locationListViewModel.length === 0 || locationListViewModel[0].latitude === null ) {
        scopeObj.noSearchResultUI();
      } else {
        var data = locationListViewModel.map(function(dataItem) {
          return {
            "lat": dataItem.latitude,
            "lon": dataItem.longitude,
            "locationId" : dataItem.locationId,
            "showcallout" : true,
            "image" : dataItem.type === "BRANCH" ? "bank_icon_blue.png" : "atm_icon_blue.png",
            "calloutData": {
              "lblBranchName" : dataItem.informationTitle,
              "lblBranchAddressOneLine" : dataItem.addressLine1,
              "lblBranchAddress2" : dataItem.addressLine2,
              "lblFormattedAddress" : dataItem.formattedAddress,
              "lblOpen" : {
                "text": "OPEN",
                "isVisible": dataItem.status === "OPEN" ? true : false
              },
              "lblClosed" :  {
                "text": "CLOSED",
                "isVisible": dataItem.status === "OPEN" ? false : true
              },
              "imgGo" : "arrow_left_grey.png",
              "flxLocationDetails":{
                "onClick" : function () {
                  scopeObj.view.LocateUs.mapLocateUs.dismissCallout();
                  scopeObj.onRowClickSegment(dataItem);
                  scopeObj.pinToSegment(dataItem);
                  scopeObj.showBranchDetails();
                }
              }
            },
            "imgBuildingType": dataItem.type === "BRANCH" ? "bank_icon.png" : "transaction_type_withdrawl.png",
            "imgGo": "arrow_left_grey.png",
            "imgIndicator":{
              "src":"accounts_sidebar_blue.png",
              "isVisible":false,
            },
            "lblAddress": dataItem.addressLine2,
            "lblClosed": {
              "text":"CLOSED",
              "isVisible" : dataItem.status === "OPEN" ? false : true
            },
            "lblName": dataItem.informationTitle,
            "lblOpen": {
              "text":"OPEN",
              "isVisible":dataItem.status === "OPEN" ? true : false
            },
            "lblContact1": dataItem.phoneNumber,
            "lblServices": dataItem.services,
            "lblDistance": dataItem.distance,
            "lblDistanceUnit": dataItem.distanceUnit,
            "lblSeperator": " ",
            "flxSearchResults":{
              "skin":"sknFlexF9F9F9",
              "hoverSkin": "sknsegf9f9f9hover"
            },
            "template":"flxSearchResults",
          };
        });
        this.bindSearchSegmentData(data);        
        this.bindMapData(data);
      }
    },

    onRowClickSegment: function(obj) {
      kony.olb.utils.showProgressBar(this.view);
      kony.print("obj-> "+JSON.stringify(obj));
      var self = this;
      this.view.LocateUs.flxMap.isVisible = false;
      this.view.LocateUs.flxBranchDetails.isVisible = true;
      var services = obj.lblServices;
      var servicesArr = [];
      if(services !== "" && services !== null) servicesArr = services.split("||");

      this.view.LocateUs.lblBranchName2.text = obj.calloutData.lblBranchName.trim();
      this.view.LocateUs.lblDistanceAndTimeFromUser.text = obj.lblDistance + " " + obj.lblDistanceUnit;
      this.view.LocateUs.lblAddressLine1.text = obj.calloutData.lblBranchAddressOneLine.trim();
      this.view.LocateUs.lblAddressLine2.text = obj.calloutData.lblBranchAddress2.trim();

      this.view.LocateUs.lblBranchName3.text = obj.calloutData.lblBranchAddressOneLine.trim();

      if(obj.lblContact1 != "null" && obj.lblContact1 !== null)
        this.view.LocateUs.lblPhoneNumber1.text = obj.lblContact1;

      if(servicesArr.length <=0 ){
        this.view.LocateUs.lblServicesAvailable.isVisible = false;
        this.view.LocateUs.flxService1.isVisible = false;
        this.view.LocateUs.flxService2.isVisible = false;
        this.view.LocateUs.flxService3.isVisible = false;
        this.view.LocateUs.flxService4.isVisible = false;
      }else{
        this.view.LocateUs.flxService1.isVisible = true;
        this.view.LocateUs.lblService1.text = servicesArr[0].trim();
        if(servicesArr[1] !== undefined){
          this.view.LocateUs.flxService2.isVisible = true;
          this.view.LocateUs.lblService2.text = servicesArr[1].trim();
        }else {
          this.view.LocateUs.flxService2.isVisible = false;
        }
        if(servicesArr[2] !== undefined){
          this.view.LocateUs.flxService3.isVisible = true;
          this.view.LocateUs.lblService3.text = servicesArr[2].trim();
        }else{
          this.view.LocateUs.flxService3.isVisible = false;
        }
        if(servicesArr[3] !== undefined){
          this.view.LocateUs.flxService4.isVisible = true;
          this.view.LocateUs.lblService4.text = servicesArr[3].trim();
        }else{
          this.view.LocateUs.flxService4.isVisible = false;
        }
      }
      this.view.LocateUs.forceLayout();
      //var a = { "ProgressBar": {show : true}  };
      kony.olb.utils.hideProgressBar(this.view);
    },

    setFlowActions: function() {
      var scopeObj=this;
      this.view.LocateUs.flxSearchImage.onClick=function(){
        scopeObj.performSearch();      
      };
      this.view.LocateUs.flxCloseIcon.onTouchEnd=function(){
        scopeObj.view.LocateUs.tbxSearchBox.text="";
      };
      this.view.LocateUs.tbxSearchBox.onDone=function(){
        scopeObj.performSearch();      
      };
      this.view.LocateUs.imgMyLocation.onTouchEnd = function() {
        scopeObj.gotoCurrentLocation();
      };
      this.view.LocateUs.segResults.onRowClick= function(obj){
        scopeObj.onRowClickSegment(obj.selecteditems[0]);
        scopeObj.showBranchDetails();
        scopeObj.changeResultsRowSkin();
      };

      this.view.LocateUs.flxViewAndFilters.onClick= function(){
        scopeObj.showViewsAndFilters();
        scopeObj.showMap();
        scopeObj.view.LocateUs.flxHideMap.setVisibility(true);
        // scopeObj.setViewsAndFilterSegmentData();
      };
      this.view.LocateUs.flxRedoSearch.onClick= function(){
        scopeObj.showRedoSearch(false);
      };
      this.view.LocateUs.flxViewsAndFiltersClose.onClick= function(){
        scopeObj.cancelFilters();
        scopeObj.showSearch();
        scopeObj.showMap();
        scopeObj.view.LocateUs.flxHideMap.setVisibility(false);
      };
      this.view.LocateUs.btnCancelFilters.onClick= function(){
        scopeObj.cancelFilters();
        scopeObj.view.LocateUs.flxHideMap.setVisibility(false);
      };
      this.view.LocateUs.btnApplyFilters.onClick= function(){
        scopeObj.applyFilters();
        scopeObj.view.LocateUs.flxHideMap.setVisibility(false);
      };
      this.view.LocateUs.imgCheckBox1.onClick= function(){
        scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox1);
      };
      this.view.LocateUs.imgCheckBox2.onClick= function(){
        scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox2);
      };
      this.view.LocateUs.imgCheckBox3.onClick= function(){
        scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox3);
      };
      this.view.LocateUs.imgCheckBox4.onClick= function(){
        scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox4);
      };
      this.view.LocateUs.imgCheckBox5.onClick= function(){
        scopeObj.toggleCheckBox(scopeObj.view.LocateUs.imgCheckBox5);
      };
      this.view.LocateUs.flxRadioAtm.onTouchEnd= function(){
        scopeObj.selectedView="ATM";
        scopeObj.view.LocateUs.lblViewType.text=kony.i18n.getLocalizedString("i18n.locateus.view")+": "+kony.i18n.getLocalizedString("i18n.locateus.OnlyAtm");
        //scopeObj.selectRadioButton(scopeObj.view.LocateUs.imgRadioButtonAtm);
        scopeObj.selectLocationFilterButton(scopeObj.view.LocateUs.btnAtm);
        scopeObj.filterSearchData();
      };
      this.view.LocateUs.flxRadioBranch.onTouchEnd= function(){
        scopeObj.selectedView="BRANCH";
        scopeObj.view.LocateUs.lblViewType.text=kony.i18n.getLocalizedString("i18n.locateus.view")+": "+kony.i18n.getLocalizedString("i18n.locateus.OnlyBranches");
        //scopeObj.selectRadioButton(scopeObj.view.LocateUs.imgRadioButtonBranch);
        scopeObj.selectLocationFilterButton(scopeObj.view.LocateUs.btnBranch);
        scopeObj.filterSearchData();
      };
      this.view.LocateUs.flxRadioAll.onTouchEnd= function(){
        scopeObj.selectedView="ALL";
        scopeObj.view.LocateUs.lblViewType.text=kony.i18n.getLocalizedString("i18n.locateus.view")+": "+kony.i18n.getLocalizedString("i18n.locateus.Branches&Atms");
        //scopeObj.selectRadioButton(scopeObj.view.LocateUs.imgRadioButtonAll);
        scopeObj.selectLocationFilterButton(scopeObj.view.LocateUs.btnBoth);
        scopeObj.filterSearchData();
      };
      this.view.LocateUs.btnBackToMap.onClick= function(){
        scopeObj.clearSelectedIndicator(); 
        scopeObj.showSearch();
        scopeObj.showMap();
        var locationData = {
          lat: scopeObj.globalMapLat, 
          lon: scopeObj.globalMapLong,
        };
        scopeObj.view.LocateUs.mapLocateUs.navigateToLocation(locationData, false, false); 
        scopeObj.view.LocateUs.mapLocateUs.zoomLevel = 15;
        scopeObj.view.forceLayout();
      };
      this.view.LocateUs.btnProceed.onClick= function(){
        scopeObj.showDirections();
        scopeObj.showMap();
      };
      this.view.LocateUs.flxCancelFilter1.onClick = function() {
        scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName1.text);
      };
      this.view.LocateUs.flxCancelFilter2.onClick = function() {
        scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName2.text);
      };
      this.view.LocateUs.flxCancelFilter3.onClick = function() {
        scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName3.text);
      };
      this.view.LocateUs.flxCancelFilter4.onClick = function() {
        scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName4.text);
      };
      this.view.LocateUs.flxCancelFilter5.onClick = function() {
        scopeObj.removeFilter(scopeObj.view.LocateUs.lblFilterName5.text);
      };
      this.view.LocateUs.flxBack.onClick= function(){
        scopeObj.showBranchDetails();
        scopeObj.showSearch();
      };
      this.view.LocateUs.btnBackToSearch.onClick= function(){
        scopeObj.showViewsAndFilters();
      };
      this.view.LocateUs.flxZoomIn.onClick= function(){
        //  scopeObj.showRedoSearch(true);
        var zoom=scopeObj.view.LocateUs.mapLocateUs.zoomLevel;
        zoom++;
        scopeObj.view.LocateUs.mapLocateUs.zoomLevel=zoom;
      };
      this.view.LocateUs.flxZoomOut.onClick= function(){
        // scopeObj.showRedoSearch(true);
        var zoom=scopeObj.view.LocateUs.mapLocateUs.zoomLevel;
        zoom--;
        scopeObj.view.LocateUs.mapLocateUs.zoomLevel=zoom;
      };
      this.view.LocateUs.flxShare.onClick= function(){
        scopeObj.showShareDirection();
      };
      this.view.LocateUs.btnClearAll.onClick= function(){
        scopeObj.searchTextClearAndRefresh();
        scopeObj.clearAll();
        scopeObj.segSetFilters();
        scopeObj.view.LocateUs.btnClearAll.setVisibility(false);

      };
      this.view.LocateUs.btnShare.onClick= function(){
        scopeObj.showShareDirection();
      };
      this.view.LocateUs.btnBackToDetails.onClick= function(){
        scopeObj.showBranchDetails();
        scopeObj.showSearch();
      };
      this.view.imgShareClose.onTouchEnd = function(){
        scopeObj.hideShareDirection();
      };
      this.view.btnShareCancel.onClick= function(){
        scopeObj.hideShareDirection();
      };
      this.view.btnShareSend.onClick= function(){
        var selKey=scopeObj.view.lbxSendMapTo.selectedKey;
        if(selKey!="key1"){
          kony.print("sharing directions to selected options");
        }
        else{
          kony.print("share button disabled");
        }
        scopeObj.hideShareDirection();
      };
      this.view.lbxSendMapTo.onSelection= function(){
        scopeObj.showTextbox();
      };
      this.view.LocateUs.mapLocateUs.onPinClick=scopeObj.pinCallback;
    },

    selectLocationFilterButton :function(btnFilter){
      this.view.LocateUs.btnBoth.skin="sknBtnf7f7f7";
      this.view.LocateUs.btnBranch.skin="sknBtnf7f7f7";
      this.view.LocateUs.btnAtm.skin="sknBtnf7f7f7";
      btnFilter.skin="sknBtnf7f7f7Select";
    },

    getSearchBranchOrATMListErrorCallback : function() {
      this.searchResultData = [];
      this.noSearchResultUI();
      kony.olb.utils.hideProgressBar(this.view);
    },    
    bindSearchSegmentData : function(data) {
      var scopeObj=this;
      scopeObj.searchResultUI();
      var segmentDataMap = {
        "flxDetails": "flxDetails",
        "flxDistanceAndGo": "flxDistanceAndGo",
        "flxIndicator": "flxIndicator",
        "flxSearchResults": "flxSearchResults",
        "imgBuildingType": "imgBuildingType",
        "imgGo": "imgGo",
        "imgIndicator": "imgIndicator",
        "lblAddress": "lblAddress",
        "lblClosed": "lblClosed",
        "lblName": "lblName",
        "lblOpen": "lblOpen",
        "lblSeperator": "lblSeperator"
      };
      this.view.LocateUs.segResults.widgetDataMap = segmentDataMap;
      this.view.LocateUs.segResults.removeAll();
      this.view.LocateUs.segResults.setData(data);
    },
    segSetFilters: function(){
      var scopeObj=this;
      var filterWidget;
      var filterCloseBtn;
      var filterNum=1;
      this.selectedServicesList=[];
      scopeObj.prevData=[];
      var data = this.view.LocateUs.segViewsAndFilters.data;
      var l= data.length;
      for(var i = 0; i < l; i++) {
        scopeObj.prevData.push(data[i].imgCheckBox.src);
        if (data[i].imgCheckBox.src=="checked_box.png"){
          filterlblWidget="lblFilterName"+filterNum;
          filterCloseBtn="flxCancelFilter"+filterNum;
          var lbl = data[i].lblOption;
          this.selectedServicesList.push(lbl);
          this.view.LocateUs[filterlblWidget].text=lbl;
          this.view.LocateUs[filterlblWidget].setVisibility(true);
          this.view.LocateUs[filterCloseBtn].setVisibility(true);
          filterNum++;
        }
      }
      if(filterNum>1) {
        this.view.LocateUs.btnClearAll.setVisibility(true);
      }
      if(filterNum === 1){
        this.view.LocateUs.flxFilters.height= "0dp";
        this.view.LocateUs.btnClearAll.setVisibility(false);
      }
      if(filterNum === 2 || filterNum===3){
        this.view.LocateUs.flxFilters.height= "42dp";
        this.view.LocateUs.btnClearAll.top = "90dp";
      }
      if(filterNum === 4 || filterNum===5){
        this.view.LocateUs.flxFilters.height= "74dp";
        this.view.LocateUs.btnClearAll.top = "122dp";     
      }
      if(filterNum === 6){
        this.view.LocateUs.flxFilters.height= "104dp"; 
        this.view.LocateUs.btnClearAll.top = "160dp";    
      }
      for(var j = filterNum; j < 6; j++) {
        filterlblWidget="lblFilterName"+j;
        filterCloseBtn="flxCancelFilter"+j;
        this.view.LocateUs[filterlblWidget].setVisibility(false);
        this.view.LocateUs[filterCloseBtn].setVisibility(false);
      }
    },

    //Do call on post show of form to draw polyline of pin on extreme corners
    drawPloyLineFACC: function(mapData) {
      try {
        var polyLineData = this.getPolyLineCorners(mapData);
        frmCareCenterList.MapList.addPolyline({
          id: "polyid1",
          locations:  polyLineData,
          polylineConfig: {
            lineColor: "0x00000000",
            lineWidth: "1"
          }
        });
        kony.print("Polyline drawn");
      } catch(e) {
        kony.print("Sorry, Exception: "+e);
      }
    },

    getPolyLineCorners: function(listOfPins){
      var pins = listOfPins;
      var topIndex = 0;
      var bottomIndex = 0;
      var rightIndex = 0;
      var leftIndex = 0;

      var topLat = 0;
      var bottomLat = 0;
      var rightLat = 0;
      var leftLat = 0;

      var topLon = 0;
      var bottomLon = 0;
      var rightLon = 0;
      var leftLon = 0;

      kony.print("Before\n Top=>("+topLat+","+topLon+")\n Bottom=>("+bottomLat+","+bottomLon+")\n Right=>("+rightLat+","+rightLon+")\n Left=>("+leftLat+","+leftLon+")");

      for(var i=0; i<pins.length; i++){
        var pin = pins[i];
        var lat = pin.lat;
        var lon = pin.lon;

        if(topLat == 0){ 
          topLat = lat;
          topLon = lon;
          topIndex = i;
        }else{
          if(lat > topLat){
            topLat = lat;
            topLon = lon;
            topIndex = i;
          }
        }

        if(bottomLat == 0){ 
          bottomLat = lat;
          bottomLon = lon;
          bottomIndex = i;
        }else{
          if(lat < bottomLat){
            bottomLat = lat;
            bottomLon = lon;
            bottomIndex = i;
          }
        }

        if(rightLon == 0){ 
          rightLon = lon;
          rightLat = lat;
          rightIndex = i;
        }else{
          if(lon < rightLon){
            rightLon = lon;
            rightLat = lat;
            rightIndex = i;
          }
        }

        if(leftLon == 0){ 
          leftLon = lon;
          leftLat = lat;
          leftIndex = i;
        }else{
          if(lon > leftLon){
            leftLon = lon;
            leftLat = lat;
            leftIndex = i;
          }
        }
      }

      kony.print("After\n Top=>("+topLat+","+topLon+")\n Bottom=>("+bottomLat+","+bottomLon+")\n Right=>("+rightLat+","+rightLon+")\n Left=>("+leftLat+","+leftLon+")");
      kony.print(" Top Index="+topIndex+"\n Bottom Index="+bottomIndex+"\n Left Index="+leftIndex+"\n Right Index="+rightIndex);

      if(pins.length <= 10){
        var topLatNew = topLat;
        topLatNew = Number(topLatNew);
        topLatNew.toString();
        topLat = topLatNew;
        kony.print("topLat====>"+topLatNew);
      }

      var polyLineData = [{
        lat: topLat,
        lon: topLon
      },{
        lat: leftLat,
        lon: leftLon
      },{
        lat: bottomLat,
        lon: bottomLon
      },{
        lat: rightLat,
        lon: rightLon
      }];

      return polyLineData;
    }
  };
});