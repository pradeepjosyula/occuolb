define([], function () {
  var toDateFormat = function (DateObj, format) {
    var dd = DateObj.getDate();
    var mm = DateObj.getMonth() + 1; //January is 0!
    var yyyy = DateObj.getFullYear();

    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }

    if(format === "dd/mm/yyyy") {
      return dd + '/' + mm + '/' + yyyy;
    }else if(format === "mm/dd/yyyy"){
      return mm + '/' + dd + '/' + yyyy;
    }
    else {
      var dateArr = DateObj.toDateString().split(' ');
      var day = dateArr[2];
      var month = dateArr[1];
      var year = dateArr[3];
      return day + ' ' + month + ' ' + year;
    }

  };

    var getDate = function (accountModelDate) {
        return new Date(accountModelDate);
    };
    /**
     * @param {String} value string value of date from backend
     * @param {String} dateformat excpected format Ex: dd/mm/yyyy
     * @returns {String} date formated for UI
     */
    var formatDate = function (value, format) {
        if (typeof value === 'string') {
            var dateObject = getDate(value);
            if ( !isNaN(dateObject.getTime())){
                return toDateFormat(dateObject, format);
            }else{
                return 'Invalid Date';
            }
        } else {
            return null;
        }
    };

    return formatDate;
});