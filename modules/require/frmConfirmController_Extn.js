define(['commonUtilities','OLBConstants'],function(commonUtilities,OLBConstants){
   
    return {
    updateTransferConfirmDetails: function (viewModelTransferConfrm) {
      var viewModel = viewModelTransferConfrm.transferData;
    var scopeObj = this;
    this.preShowFrmConfirmSinglePay();  
    this.view.flxPopup.setVisibility(false);    
    this.view.confirmDialog.flxdeliverby.setVisibility(false);
	this.view.confirmDialog.keyValueFrequency.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"); 
    this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfer')}, {text:kony.i18n.getLocalizedString("i18n.transfers.confirmTransfer")}]);
	this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.confirmTransfer");
    this.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString('i18n.transfer.QuitTransfe');
    this.view.lblConfirmBillPay.text = kony.i18n.getLocalizedString('i18n.transfers.confirmTransfer');
    this.view.confirmDialog.keyValueFrom.lblValue.text = viewModel.accFromKey; //viewModel.getAccountName(viewModel.accountFrom) + '....'+ viewModel.getLastFourDigit(viewModel.accountFrom.accountID);
    this.view.confirmDialog.keyValueTo.lblValue.text = viewModel.accToKey;//viewModel.accountTo.accountID ? (viewModel.getAccountName(viewModel.accountTo) + '....'+ viewModel.getLastFourDigit(viewModel.accountTo.accountID)) : viewModel.accountTo.nickName;
    if(viewModel.accToKey === kony.i18n.getLocalizedString('i18n.transfers.newrecipient')){
      this.view.confirmDialog.keyValueTo.lblValue.text = viewModel.recLastName +" | "+viewModel.recMemshpID+" | "+viewModel.recShareID;
      viewModel.accToKey = viewModel.recLastName +" | "+viewModel.recMemshpID+" | "+viewModel.recShareID;
      viewModel.ToaccountID = viewModel.recMemshpID; //toAccountNumber,
      viewModel.ToProductType = "S";
      viewModel.ToProductID = viewModel.recShareID;
	  viewModel.ToAccountName = viewModel.recLastName;
    }
    this.view.confirmDialog.keyValueAmount.lblValue.text = this.presenter.formatCurrency(viewModel.amount, true);
      if ((viewModel.ToProductType === "L"|| viewModel.ToProductType === "M") && viewModel.selectedDate == "Payment Date"){
				this.view.confirmDialog.keyValuePaymentDate.lblValue.text = viewModel.ToPaymentDate;
                viewModel.frequencyKey = "Once";
                viewModel.frequency = "i18n.transfers.frequency.once";
			}else{
				this.view.confirmDialog.keyValuePaymentDate.lblValue.text = viewModel.sendOnDate;
			}
    this.view.confirmDialog.keyValueFrequency.lblValue.text = kony.i18n.getLocalizedString(viewModel.frequency);
    this.view.confirmDialog.keyValueNote.lblValue.text = viewModel.notes;
    if (viewModel.frequencyKey !== "Once" && viewModel.frequencyKey !== "Now" && (viewModel.indefinetly === true || viewModel.indefinetly === "true")) {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
      this.view.confirmDialog.keyValueFrequencyType.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblIndefinetly");
      this.view.confirmDialog.keyValueFrequencyType.lblValue.text = kony.i18n.getLocalizedString("i18n.transfers.lblIndefinetlyValue");
      this.view.confirmDialog.keyValuePaymentDate.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.startDate");
    }
    else if (viewModel.frequencyKey !== "Once" && viewModel.frequencyKey !== "Now") {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
      this.view.confirmDialog.keyValueFrequencyType.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblendingon");
      this.view.confirmDialog.keyValueFrequencyType.lblValue.text = viewModel.endOnDate;      
      this.view.confirmDialog.keyValuePaymentDate.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.startDate");
    }
    else {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
      this.view.confirmDialog.keyValuePaymentDate.lblKey.text = kony.i18n.getLocalizedString("i18n.billPay.PaymentDate");
    }
    this.view.confirmDialog.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmTransaction');
    this.view.confirmDialog.confirmButtons.btnConfirm.onClick = function () {
      kony.olb.utils.showProgressBar(this.view);      
      this.presenter.createTransfer(viewModel);
    }.bind(this);
    this.view.confirmDialog.confirmButtons.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.common.cancelTransaction');
    this.view.confirmDialog.confirmButtons.btnCancel.onClick = this.showTransferCancelPopup.bind(this, function () {
      viewModel.cancelTransaction(viewModel);
    })
    this.view.confirmDialog.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');
    this.view.confirmDialog.confirmButtons.btnModify.onClick = function () {
      this.presenter.modifyTransaction(viewModel);
    }
  },
  initActions: function () {
        this.view.customheader.topmenu.flxMenu.skin="slFbox";
        this.view.customheader.topmenu.flxaccounts.skin="sknFlx4f2683Occu";
        this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
        this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
        this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
        this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
        this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png"; 
         this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
         this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
         this.view.customheader.topmenu.flxaccounts.skin="sknHoverTopmenu7f7f7pointer"; 
    var scopeObj = this;
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopupLogout.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopupLogout.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainContainer.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    };

    this.view.CustomPopupLogout.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopupLogout.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";

    }
    this.view.CustomPopupLogout.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    }
  },
  setDataForConfirmBulkPay: function(bulkPayRecords){
    this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
    bulkPayWidgetDataMap = {
      "lblPaymentAccount": "lblPaymentAccount",
      "lblPaymentAccountType": "lblPaymentAccountType",
      "lblPayee": "lblPayee",
      "lblSendOn": "lblSendOn",
      "lblFrequency" : "lblFrequency",
      "lblAmount": "lblAmount",
  };
  var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
  this.view.confirmButtons.btnCancel.onClick = billPayModule.presentationController.allPayeeData.bind(this.presenter);
  this.view.confirmButtons.btnModify.onClick = billPayModule.presentationController.modifyBulkPay.bind(this.presenter, bulkPayRecords.bulkPayRecords);
  this.view.confirmButtons.btnConfirm.onClick = billPayModule.presentationController.createBulkPayments.bind(this.presenter, bulkPayRecords.records);
  this.view.lblAmountValue.text =  bulkPayRecords.totalSum;
  this.view.segBill.widgetDataMap = bulkPayWidgetDataMap;
  this.view.segBill.setData(bulkPayRecords.records);
  this.view.forceLayout();
  },
    };
});