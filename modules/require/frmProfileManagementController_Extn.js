define(['CommonUtilities','CommonUtilitiesOCCU'],function (CommonUtilities,CommonUtilitiesOCCU) {
  var widgetsMap = [{
    menu: "flxProfileSettings",
    subMenu: {
      parent: "flxProfileSettingsSubMenu",
      children: [
        {
          "configuration": "profileSettingsEnable",
          "widget":"flxProfile"
        }, 
        {
          "configuration": "phoneSettingsEnable",
          "widget":"flxPhone"
        }, 
        {
          "configuration": "emailSettingsEnable",
          "widget":"flxEmail"
        }, 
        {
          "configuration": "addressSettingsEnable",
          "widget":"flxAddress"
        },
        {
          "configuration": "usernameAndPasswordSettingsEnable",
          "widget":"flxUsernameAndPassword"
        }
      ]
    },
    image: "imgProfileSettingsCollapse"
  },
                    {
                      menu: "flxSecuritySettings",
                      subMenu: {
                        parent: "flxSecuritySettingsSubMenu",
                        children: 
                        [
                          {
                            "configuration": "securityQuestionsSettingsEnable",
                            "widget":"flxSecurityQuestions"
                          }, 
                          {
                            "configuration": "secureAccessCodeSettingsEnable",
                            "widget":"flxSecureAccessCode"
                          }
                        ]
                      },
                      image: "imgSecuritySettingsCollapse"
                    },
                    {
                      menu: "flxAccountSettings",
                      subMenu: {
                        parent: "flxAccountSettingsSubMenu",
                        children: [
                          {
                            "configuration": "enablePreferredAccounts",
                            "widget":"flxAccountPreferences"
                          }, 
                          {
                            "configuration": "enableDefaultAccounts",
                            "widget":"flxSetDefaultAccount"
                          }
                        ]
                      },
                      image: "imgAccountSettingsCollapse"
                    },
                    {
                      menu: "flxAlerts",
                      subMenu: {
                        parent: "flxAlertsSubMenu",
                        children: [
                          {
                            "configuration":"",
                            "widget":"flxTransactionAndPaymentsAlerts"
                          },
                          {
                            "configuration":"",
                            "widget":"flxAccountAlerts"
                          },
                          {
                            "configuration":"",
                            "widget":"flxSecurityAlerts"
                          },
                          {
                            "configuration":"",
                            "widget":"flxPromotionalAlerts"
                          },
                        ]
                      },
                      image: "imgAlertsCollapse"
                    }
                   ];
  var primaryState = {};
  return {
    /**
    * Method to manupulate the UI of the screen
    * @member of frmProfileManagementController_Extn
    * @param {void} - None
    * @returns {void} - None
    * @throws {void} -None
    */
    /*
    postShowProfileManagement: function(){
      this.setFlowActions();
      this.setAlertsFlowActions();
      this.disableButton(this.view.settings.btnEditAccountsSave);
      this.AdjustScreen();
    }, */

    postShowProfileManagement: function () {
      try{
        kony.print("Inside postShowProfileManagement");
        this.setAlertsFlowActions();
        this.view.flxTCContentsCheckbox.onClick=this.toggleTC;
        this.view.flxClose.onClick=this.closeTermsAndConditions;
        this.view.btnCancel.onClick=this.closeTermsAndConditions;
        this.view.btnSave.onClick=this.onSaveTnC;
        if (kony.os.deviceInfo().screenHeight >= "900") {
          var mainheight = 0;
          var screenheight = kony.os.deviceInfo().screenHeight;
          mainheight = this.view.customheader.frame.height;
          mainheight = mainheight + this.view.flxContainer.frame.height;
          var diff = screenheight - mainheight;

          if (diff > 0)
            this.view.flxFooter.top = diff + "dp";
        }
        this.view.settings.imgAddRadioBtnUS.src = "icon_radiobtn_active.png";
        this.view.settings.imgAddRadioBtnInternational.src = "icon_radiobtn.png";
        this.presenter.loadHamburger('frmProfileManagement');
      }catch(err){
        kony.print("Exception in postShowProfileManagement-> "+err);
      }
    },

    willUpdateUI: function (viewModel) {
      if (viewModel === undefined) {
        kony.print("No viewModel");
      } else {
        if (viewModel.isLoading !== undefined) this.changeProgressBarState(viewModel.isLoading);
        if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
        if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
        if (viewModel.showDefautUserAccounts) this.showDefaultUserAccount(viewModel.showDefautUserAccounts);
        if (viewModel.getAccountsList) this.showAccountsList(viewModel.getAccountsList);
        if (viewModel.getPreferredAccountsList) this.showPreferredAccountsList(viewModel.getPreferredAccountsList);
        if (viewModel.errorEditPrefferedAccounts) this.onPreferenceAccountEdit(viewModel.errorEditPrefferedAccounts);
        if (viewModel.userProfile) this.updateUserProfileSetttingsView(viewModel.userProfile);
        if (viewModel.emailList) this.updateEmailList(viewModel.emailList);
        if (viewModel.emails) this.setEmailsToLbx(viewModel.emails);
        if (viewModel.phoneList) this.updatePhoneList(viewModel.phoneList);
        if (viewModel.addressList) this.updateAddressList(viewModel.addressList);
        if (viewModel.addPhoneViewModel) this.updateAddPhoneViewModel(viewModel.addPhoneViewModel);
        if (viewModel.editPhoneViewModel) this.updateEditPhoneViewModel(viewModel.editPhoneViewModel);      
        if (viewModel.showVerificationByChoice) {this.showUsernameVerificationByChoice();
                                                 CommonUtilities.hideProgressBar(this.view);}
        if (viewModel.requestOtp) this.enterOtp();
        if (viewModel.verifyOtp) this.updateRequirements();
        if (viewModel.verifyQuestion) this.updateRequirements();
        if (viewModel.update) this.acknowledge();
        if (viewModel.securityAccess) this.showSecureAccessOptions(viewModel.securityAccess);
        if (viewModel.secureAccessOption) this.presenter.checkSecureAccess();
        if (viewModel.SecurityQuestionExists) this.showSecurityQuestions(viewModel.SecurityQuestionExists);
        if (viewModel.answerSecurityQuestion) this.showSecurityVerification(viewModel.answerSecurityQuestion);
        if (viewModel.passwordExists) this.showExistingPasswordError();
        if (viewModel.requestOtpError) this.showRequestOtpError();
        if (viewModel.verifyOtpServerError) this.showVerifyOtpServerError();
        if (viewModel.verifyQuestionAnswerError) this.showVerifyQuestionAnswerError();
        if (viewModel.verifyQuestionServerAnswerError) this.showVerifyQuestionAnswerServerError();
        if (viewModel.getAnswerSecurityQuestionError) this.showGetAnswerSecurityQuestionError();
        if (viewModel.updateUsernameError) this.showUpdateUsernameError();
        if (viewModel.updateUsernameServerError) this.showUpdateUsernameServerError();
        if (viewModel.SecurityQuestionExistsError) this.showSecurityQuestionExistsError();
        if (viewModel.updateSecurityQuestionError) this.showUpdateSecurityQuestionError();
        if (viewModel.fetchSecurityQuestionsError) this.showFetchSecurityQuestionsError();
        if (viewModel.checkSecurityAccessError) this.showCheckSecurityAccessError();
        if (viewModel.verifyOtpError) this.showVerifyOtpError();
        if (viewModel.addNewAddress) this.showAddNewAddressForm(viewModel.addNewAddress);      
        if (viewModel.editAddress) this.showEditAddressForm(viewModel.editAddress);
        if (viewModel.secureAccessOptionError) this.showSecureAccessOptionError();
        if (viewModel.passwordExistsServerError) this.showPasswordExistsServerError();
        if (viewModel.passwordServerError) this.showPasswordServerError();
        if (viewModel.emailError) this.showEmailError(viewModel.emailError);
        if (viewModel.phoneDetails) this.showPhoneDetails(viewModel.phoneDetails);    
        if(viewModel.editEmailError) this.showEditEmailError(viewModel.editEmailError);  
        if(viewModel.AccountAlerts) this.showAccountAlerts(viewModel.AccountAlerts);
        if(viewModel.ModifyAccountAlert) this.saveAccountAlertCallBack(viewModel.ModifyAccountAlert);
        this.AdjustScreen();
        this.view.forceLayout();
      }
    },

    setAlertsFlowActions:function(){
      //functions for menu flow
      kony.print("Inside setAlertsFlowActions function");
      var scopeObj=this;
      /*
      this.view.settings.flxTransactionAndPaymentsAlerts.onClick=function(){
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.text=kony.i18n.getLocalizedString("i18n.Alerts.TransactionAndPaymentAlertSettings");
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info={"currentScreen":"disabled","isEnabled":false,"settings":"tAndP"};
        scopeObj.showViews(["flxTransactionalAndPaymentsAlertsWrapper"]);
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      }; */
      this.view.settings.flxAccountAlerts.onClick=function(){
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.text= "Account Alert";
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info={"currentScreen":"enabled","isEnabled":true,"settings":"security"};
        //scopeObj.showViews(["flxSecurityAlertsWrapper"]);
        scopeObj.presenter.fetchAccountAlerts();
        scopeObj.activateMenu(3,1);
        scopeObj.setSeperatorHeight();
      };

      this.view.settings.flxSecurityAlerts.onClick = function(){
        /*
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.text=kony.i18n.getLocalizedString("i18n.Alerts.SecurityAlertSettings");
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info={"currentScreen":"enabled","isEnabled":true,"settings":"security"};
        scopeObj.view.settings.securityAlerts.rtxAlertsWarning.text="We are providing these messages as mandatory alerts. From security point of view you can not switch it off.";
        scopeObj.view.settings.securityAlerts.flxAlertsWarningWrapper.left="20px";
        scopeObj.showViews(["flxSecurityAlertsWrapper"]);
        //scopeObj.presenter.fetchSecurityAlerts();
        scopeObj.showSecurityAlerts();
        scopeObj.setSeperatorHeight();
        */
      };
      /*
      this.view.settings.flxPromotionalAlerts.onClick=function(){
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.text=kony.i18n.getLocalizedString("i18n.Alerts.PromotionalAlertSettings");
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info={"currentScreen":"enabled","isEnabled":true,"settings":"promotional"};
        scopeObj.showViews(["flxPromotionalAlertsWrapper"]);
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      }; */

      //transcactional and payment alerts button actions
      this.view.settings.transactionalAndPaymentsAlerts.btnModifyAlerts.onClick=function(){
        /*
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
        */
      };
      this.view.settings.transactionalAndPaymentsAlerts.btnSave.onClick=function(){
        if (scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen==="confirm disable") {
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled=false;
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="disabled";
        } else {
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="enabled";
        }
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.btnEnableAlerts.onClick=function(){
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled=true;
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.btnCancel.onClick=function(){
        if (scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.isEnabled===false) {
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="disabled";
        } else {
          scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="enabled";
        }
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.flxEnableAlertsCheckBox.onClick=function(){
        scopeObj.view.settings.transactionalAndPaymentsAlerts.lblAlertsHeading.info.currentScreen="confirm disable";
        scopeObj.showTAndPAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.transactionalAndPaymentsAlerts.flxChannel1.onClick=function(){
        scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.transactionalAndPaymentsAlerts);
      };
      this.view.settings.transactionalAndPaymentsAlerts.flxChannel2.onClick=function(){
        scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.transactionalAndPaymentsAlerts);
      };
      this.view.settings.transactionalAndPaymentsAlerts.flxChannel3.onClick=function(){
        scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.transactionalAndPaymentsAlerts);
      };

      //Account Alerts
      this.view.settings.flxAlertsStatusCheckbox.onClick=function(){
        scopeObj.toggleAlertStatusCheckbox();
      };
      this.view.WhatisThisCustomPopup.btnNo.onClick = function(){
        scopeObj.view.flxPopup.setVisibility(false);
      };
      this.view.WhatisThisCustomPopup.flxCross.onClick = function(){ 
        scopeObj.view.flxPopup.setVisibility(false);
      };
      this.view.WhatisThisCustomPopup.btnYes.onClick = function(){
        if(scopeObj.view.WhatisThisCustomPopup.btnYes.text == "OK"){
          scopeObj.view.flxPopup.setVisibility(false);
          var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          alertsModule.presentationController.initializeUserProfileClass();
          alertsModule.presentationController.fetchAccountAlerts();
        }else { //YES
          scopeObj.onClickYesWhatisThisPopup();
        }
      };
      this.view.settings.tbxAdditionalFieldValue.onEndEditing = function(){
        scopeObj.setEnableDisableSaveButton();
      };
      this.view.settings.flxChannel1.onClick=function(){
        scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings);
      };
      this.view.settings.flxChannel2.onClick=function(){
        scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings);
      };
      this.view.settings.flxChannel3.onClick=function(){
        scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings);
      };
      this.view.settings.btnAlertCancel.onClick = function(){
        scopeObj.cancelAccountAlert();
      };
      this.view.settings.btnAlertSave.onClick = function(){
        scopeObj.saveAccountAlert();
      };

      //security alerts button actions
      this.view.settings.securityAlerts.btnModifyAlerts.onClick=function(){
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showSecurityAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.securityAlerts.btnSave.onClick=function(){
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info.currentScreen="enabled";
        scopeObj.showSecurityAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.securityAlerts.btnCancel.onClick=function(){
        scopeObj.view.settings.securityAlerts.lblAlertsHeading.info.currentScreen="enabled";
        scopeObj.showSecurityAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.securityAlerts.flxChannel1.onClick=function(){
        scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.securityAlerts);
      };
      this.view.settings.securityAlerts.flxChannel2.onClick=function(){
        scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.securityAlerts);
      };
      this.view.settings.securityAlerts.flxChannel3.onClick=function(){
        scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.securityAlerts);
      };

      //promotional alerts button actions
      this.view.settings.promotionalAlerts.btnModifyAlerts.onClick=function(){
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.btnSave.onClick=function(){
        if (scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen==="confirm disable") {
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled=false;
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="disabled";
        } else {
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="enabled";
        }
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.btnEnableAlerts.onClick=function(){
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled=true;
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="modify";
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.btnCancel.onClick=function(){
        if (scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.isEnabled===false) {
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="disabled";
        } else {
          scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="enabled";
        }
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.flxEnableAlertsCheckBox.onClick=function(){
        scopeObj.view.settings.promotionalAlerts.lblAlertsHeading.info.currentScreen="confirm disable";
        scopeObj.showPromotionalAlerts();
        scopeObj.setSeperatorHeight();
      };
      this.view.settings.promotionalAlerts.flxChannel1.onClick=function(){
        scopeObj.toggleChannel1Checkboxes(scopeObj.view.settings.promotionalAlerts);
      };
      this.view.settings.promotionalAlerts.flxChannel2.onClick=function(){
        scopeObj.toggleChannel2Checkboxes(scopeObj.view.settings.promotionalAlerts);
      };
      this.view.settings.promotionalAlerts.flxChannel3.onClick=function(){
        scopeObj.toggleChannel3Checkboxes(scopeObj.view.settings.promotionalAlerts);
      };
    },

    activateMenu: function (parentIndex, childrenIndex) {
      //kony.print("widgetsMap-> "+JSON.stringify(widgetsMap));
      var menuObject = widgetsMap[parentIndex];
      //this.collapseAll();
      this.expandWithoutAnimation(this.view.settings[menuObject.subMenu.parent]);
      this.view.settings[menuObject.subMenu.children[childrenIndex].widget].skin = "sknMenuSelectedOCCU";
      this.view.settings[menuObject.subMenu.children[childrenIndex].widget].hoverSkin = "sknMenuSelectedOCCU";
    },
    /**
    * Method to show the list of accounts
    * @member of frmProfileManagementController
    * @param {JSON} viewModel - JSON of Accounts
    * @returns {void} - None
    * @throws {void} -None
    */
    showAccountAlerts: function(viewModel) {
      //var scopeObj = this;
      kony.print("viewModel.AccountAlerts-> "+JSON.stringify(viewModel));
      var accountNumberMask = function (accountNumber) {
        var stringAccNum = '' + accountNumber;
        var index = stringAccNum.length - 4;
        var accountNum = stringAccNum.substring(index,stringAccNum.length);
        accountNum = 'X'+accountNum;
        return accountNum;
      };
      try{
        if(viewModel.length>0){
          var segData = [];
          this.view.settings.lblNoAlertsAvailable.setVisibility(false);
          this.view.settings.lblAlertsHeading.text = "ACCOUNT ALERTS";
          for(var i=0, len = viewModel.length; i<len; i++){
            if(viewModel[i].accountOwner !== undefined){
              var accountNumber = viewModel[i].accountNumber;
              var accountOwnerFormatted = viewModel[i].accountOwner + " | " + accountNumberMask(accountNumber);
              var accountHeader = {
                "lblHeader": accountOwnerFormatted
              };
              var segRowData = [];
              if(viewModel[i].subaccounts){
                var subAccounts = viewModel[i].subaccounts;
                for(var j=0, subAccountsLength = subAccounts.length; j<subAccountsLength; j++){
                  var availableAlertsArr = subAccounts[j].availableAlerts;
                  var prodId = subAccounts[j].productId;
                  var accName = subAccounts[j].accountName;
                  var activeAlertsArr = "", alertCount;
                  if(subAccounts[j].activeAlerts) activeAlertsArr = subAccounts[j].activeAlerts;
                  if(activeAlertsArr.length > 0){
                    var count = "("+activeAlertsArr.length+")";
                    alertCount = {
                      "text": count,
                      "isVisible": true
                    };
                  }else{
                    alertCount = {
                      "text": "",
                      "isVisible": false
                    };
                  }
                  var accountObj = {
                    "lblAccountName": accName,
                    "lblAccountNumber": accountNumber, 
                    "lblAlertsCount": alertCount,
                    "imgChevron": "arrow_left_grey.png",
                    "lblProductId": prodId,
                    "lblAvailableAlerts": availableAlertsArr,
                    "lblActiveAlerts": activeAlertsArr,
                    "lblSeparator": "",
                    "onAlertAccountClick": this.onSegAccountsListRowClick
                  };
                  segRowData.push(accountObj);
                } //inner for loop close
              }
              segData.push([accountHeader, segRowData]);
            }
          } //for loop close
          kony.print("segData after push is " + JSON.stringify(segData));
          this.view.settings.flxAccountsList.setVisibility(true);
          this.view.settings.flxSelectedAccountAlert.setVisibility(false);
          this.view.settings.flxAccountAlertDetail.setVisibility(false);
          this.view.settings.segAccountsList.removeAll();
          this.view.settings.segAccountsList.setData(segData);
          //this.view.forceLayout();
        }else{
          this.view.settings.lblNoAlertsAvailable.setVisibility(true);
          this.view.settings.flxAccountsList.setVisibility(false);
          this.view.settings.flxSelectedAccountAlert.setVisibility(false);
          this.view.settings.flxAccountAlertDetail.setVisibility(false);
        }
        this.activateMenu(3,1);
        CommonUtilities.hideProgressBar(this.view);
      }catch(err){
        kony.print("Exception in showAccountAlerts-> "+err);
      }
    },

    onSegAccountsListRowClick : function(selItem){
      try{
        var getAccountType = function(prodId){
          var key = "S";
          if(prodId == "0001" || prodId == "0008" || prodId == "0060") key = "S";
          if(prodId == "0146") key = "CC";
          return key;
        };
        
        var index = this.view.settings.segAccountsList.selectedIndex[1];
        var secindex = this.view.settings.segAccountsList.selectedIndex[0];
        var selectedSegdata = this.view.settings.segAccountsList.data[secindex][1][index];
        kony.print("index "+index+" secindex: "+secindex+"\n selItem: "+JSON.stringify(selectedSegdata));

        this.view.settings.flxAccountsList.setVisibility(false);
        this.view.settings.flxSelectedAccountAlert.setVisibility(true);
        this.view.settings.flxAccountAlertDetail.setVisibility(false);

        var availableAlertsArr = JSON.parse(selectedSegdata.lblAvailableAlerts);
        var activeAlerts = selectedSegdata.lblActiveAlerts;
        var segRowData = [];
        //var accountTypeKey = CommonUtilitiesOCCU.getAccountTypeKey(selectedSegdata.accountType);
        var accountTypeKey = getAccountType(selectedSegdata.lblProductId);
        for(var i=0, len = availableAlertsArr.length; i<len; i++){
          var alertName = this.translateTypeToText(availableAlertsArr[i]);
          var activeAlertObj = "", notifyValue;
          if(activeAlerts){
            for(var j=0, counts=activeAlerts.length; j<counts; j++){
              if(availableAlertsArr[i] == activeAlerts[j].type){
                activeAlertObj = activeAlerts[j];
                break;
              }
            }
          }
          if(activeAlertObj === undefined){
            notifyValue = {
              "text": "",
              "notifyText":"",
              "notifyValueFormatted":"",
              "isVisible": false
            };
          }else{
            var notifyVal = "", notifyText = "", notifyValFormatted = "";
            var type = activeAlertObj.type;
            if(type == "1" || type == "7" || type == "19" || type == "23"){
              notifyText = "Notify Amount";
              notifyValFormatted = "$"+activeAlertObj.notifyAmount;
              notifyVal = activeAlertObj.notifyAmount;
            } else if(type == "6"){
              notifyText = "Check Number";
              notifyVal = activeAlertObj.checkNumber;
              notifyValFormatted = notifyVal;
            } else if(type == "10" || type == "14" || type == "18"){
              var days = Number(activeAlertObj.days);
              var currDate = new Date();
              currDate.setDate(currDate.getDate() + days); 
              var day = currDate.getDate();
              var month = currDate.getMonth() + 1;
              var year = currDate.getFullYear();
              notifyValFormatted = month+"/"+day+"/"+year;
              notifyText = "Days";
              notifyVal = days;
            }
            kony.print("notifyVal-> "+notifyVal+" notifyValFormatted-> "+notifyValFormatted);
            if(notifyVal === ""){
              notifyValue = {
                "text": "",
                "notifyText":notifyText,
                "notifyValue":"",
                "isVisible": false
              };
            }else{
              notifyValue = {
                "text": notifyValFormatted,
                "notifyText":notifyText,
                "notifyValue":notifyVal,
                "isVisible": true
              };
            }
          }

          var alertObj = {
            "lblAlertName": alertName,
            "lblNotifyValue": notifyValue,
            "imgChevron": "arrow_left_grey.png",
            "lblAccountNumber": selectedSegdata.lblAccountNumber,
            "lblProductId": selectedSegdata.lblProductId,
            "lblProductType": accountTypeKey,
            "lblAlertType": availableAlertsArr[i],
            "lblActiveAlert": activeAlertObj,
            "lblSeparator": "",
            "onAlertClick": this.onSelectAlert
          };
          segRowData.push(alertObj);
        } //for loop close
        this.view.settings.segSelectAccountAlerts.removeAll();
        this.view.settings.segSelectAccountAlerts.setData(segRowData);
        kony.print("segRowData after push ####### " + JSON.stringify(segRowData));
      }catch(err){
        kony.print("Exception in onSegAccountsListRowClick: "+err);
      }
    },

    onSelectAlert : function(){
      try{
        var index = this.view.settings.segSelectAccountAlerts.selectedIndex[1];
        //var secindex = this.view.settings.segSelectAccountAlerts.selectedIndex[0];
        var selectedSegRowdata = this.view.settings.segSelectAccountAlerts.data[index];
        kony.print("onSelectAlert ::: index "+index+"\n selectedSegdata: "+JSON.stringify(selectedSegRowdata));
        this.view.settings.flxAccountsList.setVisibility(false);
        this.view.settings.lblErrorMessage.setVisibility(false);
        this.view.settings.flxSelectedAccountAlert.setVisibility(false);
        this.view.settings.flxAccountAlertDetail.setVisibility(true);
        this.view.settings.btnAlertSave.setEnabled(false);
        this.view.settings.btnAlertSave.skin = "sknBtnBg4f2683Font15PxDisbaleOCCU"; //disabled skin
        this.view.settings.lblAlertsHeading.text = selectedSegRowdata.lblAlertName; //Header
        
        var type = Number(selectedSegRowdata.lblAlertType);
        
        if(selectedSegRowdata.lblActiveAlert){
          var activeAlertDetails = selectedSegRowdata.lblActiveAlert;
          //{"type":"0","subtype":"D","notifyAmount":"3.76","days":"27","checkNumber":"785584","text":"true","email":"true","push":"true"}
          //var type = Number(activeAlertDetails.type); //subtype = activeAlertDetails.subtype;
          var subField = this.getSubFieldName(type,activeAlertDetails.subtype);
          var subFieldArr = subField.split("$");
          this.view.settings.lblAccountAlertSubtype.text = subFieldArr[1];
          this.view.settings.flxAlertsStatusCheckbox.setVisibility(true);
          this.view.settings.lblAlertsStatusCheckBox.text = "C"; //Enabled
          this.view.settings.lblHiddenAlertSubType.text = activeAlertDetails.subtype;
          
          var additionalField = selectedSegRowdata.lblNotifyValue;
          var additionalFieldValue = "",additionalFieldType = "";
          if(additionalField.isVisible){
            additionalFieldValue = additionalField.notifyValue;
            additionalFieldType = additionalField.notifyText;
            this.view.settings.flxAdditionalField.setVisibility(true);
            this.view.settings.lblAdditionalFieldName.text = additionalField.notifyText;
            this.view.settings.tbxAdditionalFieldValue.text = additionalField.notifyValue;
          }else{
            this.view.settings.flxAdditionalField.setVisibility(false);
            this.view.settings.lblAdditionalFieldName.text = "";
            this.view.settings.tbxAdditionalFieldValue.text = "";
          }
          this.view.settings.flxAlertsCheckBoxChannels.setVisibility(true);
          //var textMsg="", emailOption="",pushOption="";
          if(activeAlertDetails.text == "true"){
            this.view.settings.lblCheckBoxChannel1.text = "C";
          }else{
            this.view.settings.lblCheckBoxChannel1.text = "D";
          }
          if(activeAlertDetails.email == "true"){
            this.view.settings.lblCheckBoxChannel2.text = "C";
          }else{
            this.view.settings.lblCheckBoxChannel2.text = "D";
          }
          if(activeAlertDetails.push == "true"){
            this.view.settings.lblCheckBoxChannel3.text = "C";
          }else{
            this.view.settings.lblCheckBoxChannel3.text = "D";
          }
          primaryState = {
            "text":this.view.settings.lblCheckBoxChannel1.text,
            "email": this.view.settings.lblCheckBoxChannel2.text,
            "push": this.view.settings.lblCheckBoxChannel3.text,
            "additionalFieldType": additionalFieldType,
            "additionalField": additionalFieldValue
          };
        }else{
          var subFieldArray = (this.getSubFieldName(type,"")).split("$");
          this.view.settings.lblHiddenAlertSubType.text = subFieldArray[0];
          this.view.settings.lblAccountAlertSubtype.text = subFieldArray[1];
          this.view.settings.flxAlertsStatusCheckbox.setVisibility(true);
          this.view.settings.lblAlertsStatusCheckBox.text = "D"; //Disabled
          this.view.settings.flxAdditionalField.setVisibility(false);
          this.view.settings.flxAlertsCheckBoxChannels.setVisibility(false);
        }
        this.view.forceLayout();
      }catch(err){
        kony.print("Exception in onSelectAlert-> "+err);
      }
    },

    translateTypeToText : function(num){
      var str = "";
      switch(num){
        case 0: str = "Regulation D Transaction Alert";
          break;
        case 1: str = "Low Balance Alert";
          break;
        case 2: str = "Non-Sufficient Funds (NSF) Alert";
          break;
        case 3: str = "Courtesy Pay (CP) Alert";
          break;
        case 4: str = "Overdraw Transfer (ODT) Alert";
          break;
        case 5: str = "Direct Deposit Received Alert";
          break;
        case 6: str = "Specific Check Number Cleared Alert";
          break;
        case 7: str = "Large Debit Card or ATM Transaction Alert";
          break;
        case 8: str = "Automatic Withdrawal Alert";
          break;
        case 9: str = "Insufficient Funds Alert (Combines NSF, CP, & ODT)";
          break;
        case 10: str = "Loan Payment Due Alert";
          break;
        case 14: str = "Maturing Certificate Alert";
          break;
        case 15: str = "Online Banking Share Transaction Alert";
          break;
        case 16: str = "Line of Credit Advance Alert";
          break;
        case 17: str = "Loan Payment Made Alert";
          break;
        case 18: str = "Mortgage Payment Due Alert";
          break;
        case 19: str = "Low Available Line of Credit (Loan) Alert";
          break;
        case 20: str = "Mailing Address Change Alert";
          break;
        case 21: str = "E-Mail Address Change Alert";
          break;
        case 23: str = "Large Withdrawal Alert";
          break;
        case 24: str = "Draft Withdrawal Alert";
          break;
        case 25: str = "Loan Past Due Alert";
          break;
        case 26: str = "Daily Balance Alert";
          break;
        case 27: str = "Interest Rate Change Alert";
          break;
        case 28: str = "Dividend Rate Change Alert";
          break;
        case 29: str = "Overdraft Tolerance Added Alert";
          break;
        case 30: str = "Overdraft Tolerance Revoked Alert";
          break;
        case 31: str = "Overdraft Tolerance Changed Alert";
          break;
        case 32: str = "Low Available Balance Alert";
          break;
        case 33: str = "Share Transfer Alert";
          break;
        case 99: str = "Phone Number Change Alert";
          break;
      }
      return str;
    },

    getSubFieldName : function(type,subtype){
      var str = "";
      switch(type){
        case 0: str = "A$"+"Notify when limit is reached"; //A
          break;
        case 1: 
          if(subtype == "B") str = "B$"+"Notify for each withdrawal";
          else if(subtype == "D") str = "D$"+"Notify for any transaction";
          else str = "A$"+"Notify first time only";
          break;
        case 2: str = "B$"+"Notify for each NSF"; //B
          break;
        case 3: str = "B$"+"Notify for each Courtesy Pay"; //B
          break;
        case 4: str = "B$"+"Notify for each Overdraw Transfer"; //B
          break;
        case 5: str = "C$"+"Notify for each Direct Deposit"; //C
          break;
        case 6: str = "A$"+"Notify only first time"; //A
          break;
        case 7: str = "B$"+"Notify for each large debit card purchase"; //B
          break;
        case 8: str = "A$"+"Notify for each withdrawal"; //A
          break;
        case 9: str = "B$"+"Notify for each transaction"; //B
          break;
        case 10: str = "P$"+"Notify when payment is due"; //P
          break;
        case 14: str = "A$"+"Notify before certificate matures"; //A
          break;
        case 15:
          if(subtype == "G") str = "G$"+"Notify for withdrawals only";
          else if(subtype == "H") str = "H$"+"Notify for any share transaction";
          else str = "F$"+"Notify for deposits only";
          break;
        case 16: str = "B$"+"Notify for each advance transaction"; //B
          break;
        case 17: str = "B$"+"Notify for each loan payment"; //B
          break;
        case 18: str = "P$"+"Notify when payment is due"; //P
          break;
        case 19:
          if(subtype == "B") str = "B$"+"Notify for each withdrawal";
          else if(subtype == "D") str = "D$"+"Notify for any transaction";
          else str = "A$"+"Notify only first time";
          break;
        case 20: str = "E$"+"Notify for each change"; //E
          break;
        case 21: str = "E$"+"Notify for each change"; //E
          break;
        case 23: str = "B$"+"Notify for each withdrawal"; //B
          break;
        case 24: str = "B$"+"Notify for each draft withdrawal"; //B
          break;
        case 25: str = "A$"+"Notify only once"; //A
          break;
        case 26: str = "S$"+"Notify for each selected share (once daily)"; //S
          break;
        case 27: str = "E$"+"Notify for each rate change"; //E
          break;
        case 28: str = "E$"+"Notify for each rate change"; //E
          break;
        case 29: str = "E$"+"Notify each time"; //E
          break;
        case 30: str = "E$"+"Notify each time"; //E
          break;
        case 31: str = "E$"+"Notify each time"; //E
          break;
        case 32:
          if(subtype == "B") str = "B$"+"Notify for each withdrawal";
          else if(subtype == "D") str = "D$"+"Notify for any transaction";
          else str = "A$"+"Notify first time only";
          break;
        case 33: str = "E$"+"Notify for each transfer"; //E
          break;
        case 99: str = "E$"+"Notify for each change"; //E
          break;
      }
      return str;
    },

    toggleAlertStatusCheckbox: function(){
      if(this.view.settings.lblAlertsStatusCheckBox.text == "C"){
        this.view.WhatisThisCustomPopup.flxHeader.setVisibility(true);
        this.view.WhatisThisCustomPopup.lblHeading.text = "Confirmation";
        this.view.WhatisThisCustomPopup.lblPopupMessage.text = "Are you sure you want to delete the alert?";
        this.view.WhatisThisCustomPopup.btnYes.centerX = "";
        this.view.WhatisThisCustomPopup.btnYes.left = "5.48%";
        this.view.WhatisThisCustomPopup.btnYes.text = "YES";
        this.view.WhatisThisCustomPopup.btnYes.toolTip = "YES";
        this.view.WhatisThisCustomPopup.flxCross.setVisibility(true);
        this.view.WhatisThisCustomPopup.btnNo.setVisibility(true);
        this.view.flxPopup.setVisibility(true);
      }else{
        this.enableAccountAlert();
      } 
    },

    toggleChannel1Checkboxes: function(alertWidget){
      if(alertWidget.lblCheckBoxChannel1.text == "C") alertWidget.lblCheckBoxChannel1.text = "D";
      else alertWidget.lblCheckBoxChannel1.text = "C";
      this.setEnableDisableSaveButton();
    },

    toggleChannel2Checkboxes: function(alertWidget){
      if(alertWidget.lblCheckBoxChannel2.text == "C") alertWidget.lblCheckBoxChannel2.text = "D";
      else alertWidget.lblCheckBoxChannel2.text = "C";
      this.setEnableDisableSaveButton();
    },

    toggleChannel3Checkboxes: function(alertWidget){
      if(alertWidget.lblCheckBoxChannel3.text == "C") alertWidget.lblCheckBoxChannel3.text = "D";
      else alertWidget.lblCheckBoxChannel3.text = "C";
      this.setEnableDisableSaveButton();
    },

    setEnableDisableSaveButton: function(){
      //kony.print("primaryStateObj-> "+JSON.stringify(primaryState));
      if(primaryState.text != this.view.settings.lblCheckBoxChannel1.text){
        this.view.settings.btnAlertSave.setEnabled(true);
        this.view.settings.btnAlertSave.skin = "sknBtnBg4f2683Font15PxOCCU";
        return;
      }
      if(primaryState.email != this.view.settings.lblCheckBoxChannel2.text){
        this.view.settings.btnAlertSave.setEnabled(true);
        this.view.settings.btnAlertSave.skin = "sknBtnBg4f2683Font15PxOCCU";
        return;
      }
      if(primaryState.push != this.view.settings.lblCheckBoxChannel3.text){
        this.view.settings.btnAlertSave.setEnabled(true);
        this.view.settings.btnAlertSave.skin = "sknBtnBg4f2683Font15PxOCCU";
        return;
      }
      if(primaryState.additionalField != this.view.settings.tbxAdditionalFieldValue.text){
        var txt = this.view.settings.tbxAdditionalFieldValue.text;
        if((primaryState.additionalFieldType == "Check Number" && (Number(txt) >= 0 && Number(txt) <= 9999999999)) || 
           (primaryState.additionalFieldType == "Notify Amount" && (Number(txt) >= 0 && Number(txt) <=  999999999999.99)) ||
           (primaryState.additionalFieldType == "Days" && (Number(txt) >= 1 && Number(txt) <= 365))){

          this.view.settings.btnAlertSave.setEnabled(true);
          this.view.settings.btnAlertSave.skin = "sknBtnBg4f2683Font15PxOCCU";
          return;
        }
      }
      this.view.settings.btnAlertSave.setEnabled(false);
      this.view.settings.btnAlertSave.skin = "sknBtnBg4f2683Font15PxDisbaleOCCU";
    },

    cancelAccountAlert : function(){
      this.view.settings.flxAccountsList.setVisibility(false);
      this.view.settings.flxSelectedAccountAlert.setVisibility(true);
      this.view.settings.flxAccountAlertDetail.setVisibility(false);
    },

    saveAccountAlert : function(expDate){
      try{
        var index = this.view.settings.segSelectAccountAlerts.selectedIndex[1];
        var selectedSegRowdata = this.view.settings.segSelectAccountAlerts.data[index];
        kony.print("saveAccountAlert index "+index+"\n selectedSegdata: "+JSON.stringify(selectedSegRowdata));
        var activeAlertDetails = selectedSegRowdata.lblActiveAlert;
        
        //{"type":"0","subtype":"D","notifyAmount":"3.76","days":"27","checkNumber":"785584","text":"true","email":"true","push":"true"}
        var params = {}, checkNum="", days="", notifyamount="";
        var type = Number(activeAlertDetails.type); //subtype = activeAlertDetails.subtype;
        //this.view.settings.lblAlertsStatusCheckBox.text = "C"; //Enabled
        if(type == 1 || type == 7 || type == 19 || type == 23){
          checkNum = activeAlertDetails.checkNumber;
          days = activeAlertDetails.days;
          if(this.view.settings.flxAdditionalField.isVisible){
            kony.print("amount: "+this.view.settings.tbxAdditionalFieldValue.text);
            notifyamount = this.view.settings.tbxAdditionalFieldValue.text;
          }else{
            notifyamount = activeAlertDetails.notifyAmount;
          }
        } else if(type == 6){
          notifyamount = activeAlertDetails.notifyAmount;
          days = activeAlertDetails.days;
          if(this.view.settings.flxAdditionalField.isVisible){
            checkNum = this.view.settings.tbxAdditionalFieldValue.text;
          }else{
            checkNum = activeAlertDetails.checkNumber;
          }
        } else if(type == 10 || type == 14 || type == 18){
          notifyamount = activeAlertDetails.notifyAmount;
          checkNum = activeAlertDetails.checkNumber;
          if(this.view.settings.flxAdditionalField.isVisible){
            days = this.view.settings.tbxAdditionalFieldValue.text;
          }else{
            days = activeAlertDetails.days;
          }
        }
        params.accountNumber = selectedSegRowdata.lblAccountNumber;
        params.productId = selectedSegRowdata.lblProductId;
        params.productType = selectedSegRowdata.lblProductType;
        params.checkNumber = checkNum;
        params.notifyAmount = notifyamount;
        params.days = days;
        params.type = activeAlertDetails.type;
        params.subtype = activeAlertDetails.subtype; //this.view.settings.lblHiddenAlertSubType.text;
        if(expDate) params.expirationDate = expDate;
        else params.expirationDate = "";

        if(this.view.settings.lblCheckBoxChannel1.text == "C") params.text = "true";
        else params.text = "false";
        if(this.view.settings.lblCheckBoxChannel2.text == "C") params.email = "true";
        else params.email = "false";
        if(this.view.settings.lblCheckBoxChannel3.text == "C") params.push = "true";
        else params.push = "false";

        var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
        alertsModule.presentationController.modifyAccountAlert(params);
      }catch(err){

      }
    },
	
    enableAccountAlert : function(){
      var index = this.view.settings.segSelectAccountAlerts.selectedIndex[1];
      var selectedSegRowdata = this.view.settings.segSelectAccountAlerts.data[index];
      var params = {};
      //mandatory parameters
      params.accountNumber = selectedSegRowdata.lblAccountNumber;
      params.productId = selectedSegRowdata.lblProductId;
      params.productType = selectedSegRowdata.lblProductType;
      params.type = selectedSegRowdata.lblAlertType;
      params.subtype = this.view.settings.lblHiddenAlertSubType.text;
      params.text = "false";
      params.email = "false";
      params.push = "false";
      
      //optional parameters
      params.checkNumber = "";
      params.notifyAmount = "";
      params.days = "";
      params.expirationDate = "";
      
      var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
      alertsModule.presentationController.modifyAccountAlert(params);
    },
    
    onClickYesWhatisThisPopup: function() {
      this.view.flxPopup.setVisibility(false);
      var date = new Date(); //Current Date
      var day = date.getDate();
      var month = date.getMonth() + 1;
      var year = date.getFullYear();
      var expDate = month+"/"+day+"/"+year;
      kony.print("Current date-> "+expDate);
      this.saveAccountAlert(expDate);
      //this.cancelAccountAlert();
    },

    saveAccountAlertCallBack : function(response){
      try{
        kony.print("inside saveAccountAlertCallBack: "+JSON.stringify(response));
        if(response[0].success == "false"){
          this.view.settings.lblErrorMessage.setVisibility(true);
          this.view.settings.lblErrorMessage.text = response[0].errmsg;
        }else{
          this.view.settings.lblErrorMessage.setVisibility(false);
          if(this.view.settings.lblAlertsStatusCheckBox.text == "C") this.view.settings.lblAlertsStatusCheckBox.text = "D";
          else this.view.settings.lblAlertsStatusCheckBox.text = "C";
          //this.cancelAccountAlert(); //navigate to previous screen
          this.view.WhatisThisCustomPopup.flxHeader.setVisibility(false);
          this.view.WhatisThisCustomPopup.lblHeading.text = "";
          this.view.WhatisThisCustomPopup.lblPopupMessage.text = "Alert updated successfully.";
          this.view.WhatisThisCustomPopup.btnYes.centerX = "50%";
          this.view.WhatisThisCustomPopup.btnYes.text = "OK";
          this.view.WhatisThisCustomPopup.btnYes.toolTip = "OK";
          this.view.WhatisThisCustomPopup.btnNo.setVisibility(false);
          this.view.WhatisThisCustomPopup.flxCross.setVisibility(false);
          this.view.flxPopup.setVisibility(true);
          //navigate to Account list screen after refresh
          //var alertsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
          //alertsModule.presentationController.initializeUserProfileClass();
          //alertsModule.presentationController.fetchAccountAlerts();
        }
        CommonUtilities.hideProgressBar(this.view);
      }catch(err){
        kony.print("Exception in saveAccountAlertCallBack-> "+err);
      }
    }

  };
});