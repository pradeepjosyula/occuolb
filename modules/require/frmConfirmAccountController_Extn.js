define(['commonUtilities','OLBConstants'],function(commonUtilities,OLBConstants){
   
    return {
        
       
  willUpdateUI: function(viewModel){
     kony.olb.utils.hideProgressBar(this.view);
    for(var i=0;i<viewModel.length;i++){
     if (viewModel[i].sideMenu) this.updateHamburgerMenu(viewModel[i].sideMenu,viewModel);
     if (viewModel[i].topBar) this.updateTopBar(viewModel[i].topBar);
     if(viewModel[i].internalAccount){
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);      
      var data= viewModel[i].internalAccount;
	  this.view.confirmDialogAccounts.flxDileveryBy.isVisible=false;
        this.view.flxEditHolisticAccount.isVisible=false;
        this.view.flxConfirmContainer.isVisible=true;
	  this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountHeading');
	  this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfer')}, {text:kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountConfirm")}]);
	  this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountConfirm");
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.AddInternal.lblAccountNumberAgainKA');
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text = kony.i18n.getLocalizedString('i18n.AddInternal.lblBeneficiaryNameKA');
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text = kony.i18n.getLocalizedString('i18n.AddInternal.lblAccountNickNameKA');
     // this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.bankName;
     // this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = data.accountType;i18n.AddInternal.lblAccountNickNameKA
      this.view.confirmDialogAccounts.keyValueBankName.setVisibility(false);
  	  this.view.confirmDialogAccounts.keyValueAccountType.setVisibility(false);
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
       this.view.confirmDialogAccounts.keyValueBenificiaryName.setVisibility(true);
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.reAccountNumber;
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =data.beneficiaryName;
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.nickName ;
      this.view.confirmDialogAccounts.keyValueCountryName.isVisible=false;
    }
    if(viewModel[i].domesticAccount){
      var data= viewModel[i].domesticAccount;
      this.view.confirmDialogAccounts.keyValueBankName.setVisibility(false);
       this.view.confirmDialogAccounts.keyValueBenificiaryName.setVisibility(false);
  	  this.view.confirmDialogAccounts.keyValueAccountType.setVisibility(true);
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
	  this.view.confirmDialogAccounts.flxDileveryBy.isVisible=false;
        this.view.flxEditHolisticAccount.isVisible=false;
        this.view.flxConfirmContainer.isVisible=true;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true); 
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.accounts.routingNumber');      
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = data.routingNumber;
	  this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfer')}, {text:kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm")}]);
	this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm");
    this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.accountNumber');
      this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.benificiaryName');
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.accountNickName');
    // this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.bankName;
     this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = data.accountType;
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.accountNumber;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = data.routingNumber;
     // this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =data.beneficiaryName;
      this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.nickName ;
      this.view.confirmDialogAccounts.keyValueCountryName.isVisible=false;
    }
  if(viewModel[i].internationalAccount){
       var data= viewModel[i].internationalAccount;
	   this.view.confirmDialogAccounts.flxDileveryBy.isVisible=false;
        this.view.flxEditHolisticAccount.isVisible=false;
        this.view.flxConfirmContainer.isVisible=true;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);                  
       this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString('i18n.transfers.swiftCode');
       this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = data.swiftCode;
       this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
       this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfer')}, {text:kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm")}]);
	    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
        this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm");
      // this.view.confirmDialogAccounts.keyValueBankName.lblValue.text = data.bankName;
      // this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = data.accountType;
       this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = data.reAccountNumber;
       this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =data.beneficiaryName;
       this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = data.nickName ;
       this.view.confirmDialogAccounts.keyValueCountryName.isVisible=false;
  }}
    this.presenter.dataForNextForm(viewModel);
    this.AdjustScreen();
    this.view.forceLayout();
  },
      
      
preShowfrmConfirmAccount: function () {
    var scopeObj = this;
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin="sknFlx4f2683Occu";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
    this.view.confirmDialogAccounts.confirmButtons.btnConfirm.setEnabled(true);
   // this.view.confirmDialogAccounts.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmAccountDetails');
   // this.view.confirmDialogAccounts.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyAccountDetails');
    //this.view.confirmDialogAccounts.confirmButtons.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.common.CancelAccount');  
    this.view.flxCancelPopup.setVisibility(false);
    this.view.customheader.forceCloseHamburger();
    
    //SET ACTIONS:
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainContainer.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";      
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CustomPopup.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };

  },

    };
});