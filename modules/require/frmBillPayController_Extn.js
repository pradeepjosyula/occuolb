define(['CommonUtilities', 'OLBConstants','CommonUtilitiesOCCU'], function (CommonUtilities, OLBConstants,CommonUtilitiesOCCU) {
   
    return {
        
          frmBillPayPreShow: function () {
      this.view.customheader.forceCloseHamburger();
      this.view.tableView.Search.txtSearch.width = "100%";
      this.view.tableView.imgSearch.src = "search_blue.png";
      this.view.flxActivatebillpays.setVisibility(false);
      this.view.flxBillPay.setVisibility(true);
      this.view.tableView.Search.setVisibility(false);
      this.view.flxBillPayManagePayeeActivity.setVisibility(false);
      this.view.flxViewEbill.setVisibility(false);
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      /// Strat
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
      /// End
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      this.view.tableView.flxBillPayAllPayees.setVisibility(true);
      this.view.tableView.flxBillPayScheduled.setVisibility(false);
      this.view.tableView.tableSubHeaderHistory.setVisibility(false);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false); 
      this.view.tableView.flxPayFrom.setVisibility(true);
      this.view.tableView.tableTabs.flxTabsChecking.setVisibility(true);    
      this.view.tableView.flxHorizontalLine3.setVisibility(true); 
      this.view.tableView.lblDueDate.text = kony.i18n.getLocalizedString("i18n.billPay.DueDate");            
      this.view.tableView.tableTabs.BtnAll.text = kony.i18n.getLocalizedString("i18n.Bills.TabPayBills");	  
	  this.view.tableView.tableTabs.btnTransfers.text = kony.i18n.getLocalizedString("i18n.Bills.TabeBills");	
	  this.view.tableView.tableTabs.btnDeposits.text = kony.i18n.getLocalizedString("i18n.Bills.TabScheduledBills");
	  this.view.tableView.tableTabs.btnChecks.text = kony.i18n.getLocalizedString("i18n.Bills.TabPostedBills");
	  this.view.tableView.tableTabs.btnWithdraws.text = kony.i18n.getLocalizedString("i18n.Bills.TabManagePayee");            
      this.view.mypaymentAccounts.lblHeading.text = kony.i18n.getLocalizedString("i18n.BillPay.lblMyPaymentAccounts");
      //this.resetSingeBillPay();
      var self = this;
      //this.view.flxOneTImePayment.setVisibility(false);
      this.setServerError(false);
      this.view.btnMakeonetimepayment.onClick = this.onClickOneTimePayement;
      this.view.btnAddPayee.onClick = this.onClickAddPayee;
      this.view.forceLayout();
    },
      
    willUpdateUI: function (billPayViewModel) {

      if(billPayViewModel.ProgressBar){
        if(billPayViewModel.ProgressBar.show){
          this.showProgressBar();
        }
        else {
          this.hideProgressBar();
        }
      } 
      else if (billPayViewModel.onServerDownError) {
        this.showServerDownForm(billPayViewModel.onServerDownError);
      }
      else if (billPayViewModel.serverError) {
        this.hideProgressBar();
        this.setServerError(true);
      }
      else {
        this.setServerError(false);
        if (billPayViewModel.sideMenu) {
          this.updateHamburgerMenu(billPayViewModel.sideMenu);
        }
        if (billPayViewModel.topBar) {
          this.updateTopBar(billPayViewModel.topBar);
        }
        if (billPayViewModel.myPaymentAccounts) {
          this.hideProgressBar();                
          this.bindMyPaymentAccountsData(billPayViewModel.myPaymentAccounts);
          this.setPayFromData(billPayViewModel.myPaymentAccounts);
          if(kony.mvc.MDAApplication.getSharedInstance().appContext.billpayeligibility === "Not Activated") {
            this.setAccountsForActivationScreen(billPayViewModel.myPaymentAccounts);  
          }
        }
        if (billPayViewModel.showDeactivatedView) {
          this.showBillPayActivationScreen();
        }
        if (billPayViewModel.showNotEligibleView) {
          this.showBillPayActivationNotEligibileScreen();
        }
        if(billPayViewModel.BulkPay){
          this.setAllPayeesSegmentData(billPayViewModel);
        }
        if(billPayViewModel.modifyBulkPay){
          this.setAllPayeesUI();
          this.setModifyBulkPayData(billPayViewModel.modifyBulkPay);
        }
        if (billPayViewModel.managePayee) {                
          this.setManagePayeeUI(billPayViewModel);
          this.hideProgressBar();
        }
        if (billPayViewModel.billerCategories) {  
          this.setDataForAccountCategories(billPayViewModel.billerCategories);
        }
        if (billPayViewModel.scheduledBills) {
          this.bindScheduleBillsSegment(billPayViewModel.scheduledBills,billPayViewModel.config);
          this.hideProgressBar();                
        }
        if (billPayViewModel.navigateToPayBill) {
          this.setDataForPayABill(billPayViewModel.navigateToPayBill);
          this.hideProgressBar();                
        }
        if (billPayViewModel.deleteSchedule) {    
          //TODO : show Confirmation message / failure message.
          if (billPayViewModel.deleteSchedule.successData) {
            this.setScheduledFlex();
          }
          if (billPayViewModel.deleteSchedule.failureData) {
            //alert(billPayViewModel.deleteSchedule.failureData.errmsg); //TODO : Error handeling 
          }
          this.hideProgressBar();            
        }
        if (billPayViewModel.payABillData) {
          this.setDataForPayABill(billPayViewModel.payABillData);
        }
        if (billPayViewModel.payABill) {
          this.setDataForPayABill(billPayViewModel.payABill);
        }
        if (billPayViewModel.payABillWithContext) {
          this.setDataForPayABill(billPayViewModel.payABillWithContext.data, billPayViewModel.payABillWithContext.context);
        }

        if (billPayViewModel.intialView) {
          this.showIntialBillPayUI(billPayViewModel.intialView);
        }
        if (billPayViewModel.billpayHistory) {
          if (billPayViewModel.billpayHistory.sender) {
            this.setHistoryUI();
          }
          else {
            this.binduserBillPayHistory(billPayViewModel);
          }
        }

        if (billPayViewModel.payeeActivities) {   
          this.showPayeeViewActivities(billPayViewModel.payeeActivities);
          this.hideProgressBar();             
        }
        if(billPayViewModel.allPayees) {      
          this.populatePayeesinListBox(billPayViewModel.allPayees);
          this.hideProgressBar();          
        }
        if (billPayViewModel.showOneTimePayment) {
          var dataObj=billPayViewModel.showOneTimePayment;
          this.setDataForShowOneTimePayment(dataObj.data,dataObj.sender);
          this.hideProgressBar();                
        }
        if (billPayViewModel.searchBillPayPayees) {
          this.setDataForSearchPayees(billPayViewModel.searchBillPayPayees);
          this.hideProgressBar();                
        }
        if (billPayViewModel.payeeaccountList) {
          this.setAccountsToLbx(billPayViewModel);
        }
        if (billPayViewModel.totalActivatedEbills) { //bind total e-bills amount
          //this.hideProgressBar();  //double progress bar
          this.bindTotalEbillAmountDue(billPayViewModel.totalActivatedEbills);
        }
        if (billPayViewModel.dueBills && billPayViewModel.ignorePaymentDue !== true) { //bind payment due bills
          this.bindPaymentDueSegment(billPayViewModel.dueBills, billPayViewModel.config);
          this.hideProgressBar(); 
        }
        if(billPayViewModel.hideAllFlx){
          this.hideAllFlx();
        }

      }
      this.AdjustScreen();
    },
    setAccountsToLbx: function(viewModel) {
        var data = [];
        var accData = viewModel.payeeaccountList
        var list = [];
        var getLastFourDigit = function (numberStr) {
          if (numberStr.length < 4) return numberStr;
          return numberStr.slice(-4);
        };
        if (accData !== null && accData !== undefined && accData.length > 0) {
            for (var i = 0; i < accData.length; i++) {
                list = [];
                list.push(accData[i].accountID+"@"+accData[i].accountHolder+"@"+accData[i].accountName+"@"+accData[i].accountType+"@"+accData[i].shareId);
                list.push(accData[i].accountName + " | " + accData[i].accountType + " " + accData[i].shareId + "  |  " + accData[i].accountHolder + " | " + ' X' + getLastFourDigit(accData[i].accountID));
                data.push(list);
            }
        }       
        this.view.tableView.lstbxPayFrom.masterData = data;
        //this.view.tableView.lstbxPayFrom.onSelection = this.onSelectionPayeeAccount.bind(this,viewModel);  
        this.view.tableView.flxPayFrom.setVisibility(true);
    },
    onSelectionPayeeAccount:function(viewModel){      
       viewModel.preferredAccountNumber = this.view.tableView.lstbxPayFrom.selectedKey;
       var selectedval =  this.view.tableView.lstbxPayFrom.selectedValue;
       if(selectedval !== null && selectedval !== "" && selectedval !==undefined) {
        viewModel.preferredAccountName = selectedval.split("@")[0];
        viewModel.preferredAccountType = selectedval.split("@")[1];
       }
    },
    showNoPayementDetails: function (viewModel) {
      var scopeObj = this;
      if (viewModel) {
        scopeObj.view.tableView.flxNoPayment.setVisibility(true);
        scopeObj.view.tableView.segmentBillpay.setVisibility(false);
        scopeObj.view.tableView.flxBillPayAllPayees.setVisibility(false);
        scopeObj.view.tableView.flxPayFrom.setVisibility(false);
        scopeObj.view.tableView.tableTabs.flxTabsChecking.setVisibility(false);
        scopeObj.view.tableView.flxHorizontalLine3.setVisibility(false); 
        scopeObj.setPagination({ show : false});
        scopeObj.view.tableView.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString(viewModel.noPaymentMessageI18Key);
        if (viewModel.showActionMessageI18Key) {
          scopeObj.view.tableView.lblScheduleAPayment.setVisibility(true);
          scopeObj.view.tableView.lblScheduleAPayment.text = kony.i18n.getLocalizedString(viewModel.showActionMessageI18Key);
          scopeObj.view.tableView.flxSchedulePayment.onClick = this.showPayABill();
        } else if(viewModel.showPayeeActionMessage){
          scopeObj.view.tableView.lblScheduleAPayment.setVisibility(true);
          scopeObj.view.tableView.flxSchedulePayment.onClick = this.donothing(); //onClickAddPayee
          scopeObj.view.tableView.lblScheduleAPayment.text = kony.i18n.getLocalizedString(viewModel.showPayeeActionMessage);
        } else {
          scopeObj.view.tableView.lblScheduleAPayment.setVisibility(false);
        }
      }
    },
    donothing : function(){
        kony.print("functionality not included now");
    },
    createNewBulkPayData: function(data) {
      var lblPayeeDate;
      var lblDueDate;
      var lblBill;
      var btnEbill;
      var btnViewEbill;
      var lblPayee;
      var btnActivateEBill;
      var btnViewEbill;
      var txtAmount;
      var scopeObj = this;
      var self = this;
      lblPayee = (data.payeeNickName!== null && data.payeeNickName !== undefined && data.payeeNickName !== "null" && data.payeeNickName !== "")? data.payeeNickName :  data.payeeName;
      if(lblPayee !== null && lblPayee !== undefined && lblPayee !== "null"){
        kony.print("value does npot exists");
      } else {
        lblPayee = "";
      }
      var nonValue = kony.i18n.getLocalizedString("i18n.common.none");
      if(data.lastPaidAmount !== null && data.lastPaidAmount !== "" && data.lastPaidAmount !== "null" && data.lastPaidAmount !== undefined){
        lblPayeeDate = kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + (self.presenter.formatCurrency(data.lastPaidAmount) || nonValue) + " " + kony.i18n.getLocalizedString("i18n.common.on") + " " + this.presenter.getDateFromDateString(data.lastPaidDate);
      } else {
        lblPayeeDate = kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + kony.i18n.getLocalizedString("i18n.billPay.noPaymentActivity");
      }
      if(data.billDueDate !== null && data.billDueDate!== "" && data.billDueDate !== "null" && data.billDueDate !== undefined){
        lblDueDate = data.billDueDate ? this.presenter.getDateFromDateString(data.billDueDate) : nonValue;
      } else {
        lblDueDate = nonValue;
      }
      if(data.dueAmount !== null && data.dueAmount !== "" && data.dueAmount !== "null" && data.dueAmount !== undefined){
       lblBill = data.dueAmount ? self.presenter.formatCurrency(data.dueAmount) : nonValue;
      } else {
       lblBill = nonValue;
      }
      txtAmount = {
          "placeholder": "Enter Amount",
          "text": ""
      };      
      var lblNotes = data.notes ? data.notes : '';
      var calSendOn = {
        "dateComponents": data.sendOn ? this.getDateComponents(data.sendOn) : this.getDateComponents(kony.os.date(CommonUtilities.getConfiguration('frontendDateFormat'))),
        "dateFormat":"MM/dd/yyyy",
        "validStartDate":self.getvalidStartDate(),
      };
      var calEndDate = {
        "dateComponents":this.getDateComponents(lblDueDate),
        "dateFormat":"MM/dd/yyyy",
        "validStartDate":self.getvalidStartDate(),
      };
        if (data.eBillStatus === "Eligible") {
          var eBillViewModel = {
            billGeneratedDate: data.billGeneratedDate,
            amount: data.dueAmount,
            ebillURL: data.ebillURL
          };
          btnEbill = {
            "text": "SOME TEXT",
            "skin": "sknbtnebillactive",
            "onClick": (data.billid === undefined || data.billid === null || data.billid == "0") ? null : scopeObj.viewEBill.bind(scopeObj, {
              "billGeneratedDate": scopeObj.presenter.getDateFromDateString(data.billGeneratedDate),
              "amount": data.dueAmount,
              "ebillURL": data.ebillURL
            })
          };
          lblDueDate = {
            "text": lblDueDate,
            "isVisible": true
          }; 
          lblBill = {
            "text": lblBill,
            "isVisible": true
          };
          btnActivateEBill = {
            "isVisible": false,
            "text": "ACTIVATE",
            "onClick": null,
          };
        } else {
          btnEbill = {
            "text": "SOME TEXT",
            "skin": "sknBtnImgInactiveEbill",
            "onClick": null
          };
          lblDueDate = {
            "text": lblDueDate,
            "isVisible": false
          };     
          lblBill = {
            "text": lblBill,
            "isVisible": false
          };
           if(data.optionActivate === true || data.optionActivate === "true"){
            btnActivateEBill = {
                "isVisible": true,
                "text": "ACTIVATE",
                "onClick": self.activateallPayeesEBill,
              };
           } else{
            btnActivateEBill = {
                "isVisible": false,
                "text": "ACTIVATE",
                "onClick": null,
            };
           }
      }
      return {
        "payeeId": data.payeeId,
        "billid": data.billid,
        "payeeAccountNumber": data.payeeAccountNumber,
        "lblSeparatorBottom": {
          "text": " "
        },
        "lblIdentifier": {
          "text": " ",
          "skin": "skin Name"
        },
        "imgDropdown": "arrow_down.png",
        "lblPayee": lblPayee,
        "lblPayeeDate": lblPayeeDate,
        "btnEbill": btnEbill,
        "lblDueDate": lblDueDate,
        "lblBill": lblBill,
        "txtAmount": txtAmount,
        "lblSeparator": "A",
        "lblPayFrom": kony.i18n.getLocalizedString("i18n.billPay.PayFrom"),
        "lblSendOn": kony.i18n.getLocalizedString("i18n.billPay.sendOn"),
        "lblEndDate": "End Date",
        "lblindefinitely": "Indefinitely",
        "lblFrequency": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
        "lblFrequencyList": "Once",
		"lblEndDateVal": data.sendOn ? this.getDateComponents(data.sendOn) : this.getDateComponents(kony.os.date(CommonUtilities.getConfiguration('frontendDateFormat'))),
        "lblPayFromList": self.presenter.viewModel.preferredAccountName ,
        "imgListBoxDropdown": "arrrow_down.png",
        "calSendOn": calSendOn,
        "calEndDate": calEndDate,
        "lblNotes": kony.i18n.getLocalizedString("i18n.billPay.Notes"),
        "lblCategory": kony.i18n.getLocalizedString("i18n.billPay.category"),
        "txtNotes": {
          "placeholder": kony.i18n.getLocalizedString("i18n.common.optional"),
          "text": ""
        },
        "switchindefinitely":1,
        "lblCategoryList": "Phone Bill",
        "imgCategoryListDropdown": "arrrow_down.png",
        "btnViewEBill": btnViewEbill,
        "lblLineOne": "1",
        "lblLineTwo": "2",
        "accountNumber":data.accountNumber,
        "payeeNickName":data.nickName,
        "payeeName":data.payeeName,
        "template": "flxBillPayAllPayees",
        "btnActivateEbill": btnActivateEBill,
        "lblDollar":kony.i18n.getLocalizedString("i18n.common.currencySymbol"),
        "preferredAccountNumber": self.presenter.viewModel.preferredAccountNumber,
      }
    },
    setAllPayeesSegmentData: function(billPayViewModel) {
      if(!billPayViewModel){
        this.presenter.showBillPayData();
        return;
      }
      this.setAllPayeesUI();
      this.setBottomDefaultValues();
      var scopeObj = this;
      if(billPayViewModel) { 
        if (billPayViewModel.BulkPay.length > 0) {          
          if(billPayViewModel.noOfRecords)
          {
            scopeObj.setPagination({
              'show': true,
              'offset': billPayViewModel.noOfRecords.offset,
              'limit': billPayViewModel.noOfRecords.limit,
              'recordsLength': billPayViewModel.BulkPay.length,
              'text': kony.i18n.getLocalizedString("i18n.billpay.payees")
            });  
          }
          this.view.btnConfirm.onClick = this.validateBulkPayData.bind(this);
          this.view.btnConfirm.setVisibility(true);
          this.getNewBulkPayData(billPayViewModel);
          var dataMap = {
            "lblLineOne": "lblLineOne",
            "lblLineTwo": "lblLineTwo",
            "lblIdentifier": "lblIdentifier",
            "imgDropdown": "imgDropdown",
            "lblPayee": "lblPayee",
            "lblPayeeDate": "lblPayeeDate",
            "btnEbill": "btnEbill",
            "lblDueDate": "lblDueDate",
            "lblBill": "lblBill",
            "txtAmount": "txtAmount",
            "lblSeparator": "lblSeparator",
            "lblPayFrom": "lblPayFrom",
            "lblSendOn": "lblSendOn",
            "lblEndDate": "lblEndDate",
            "lblFrequency": "lblFrequency",
			"lblNumOfRecurrences": "lblNumOfRecurrences",
			"txtNumberOfRecurrences": "txtNumberOfRecurrences",
            "lblEndDateVal": "lblEndDateVal",
            "lblPayFromList": "lblPayFromList",
            "lblindefinitely": "lblindefinitely",
            "switchindefinitely": "switchindefinitely",
            "lblFrequencyList": "lblFrequencyList",
            "imgListBoxDropdown": "imgListBoxDropdown",
            "calSendOn": "calSendOn",
            "calEndDate": "calEndDate",
            "lblNotes": "lblNotes",
            "lblCategory": "lblCategory",
            "txtNotes": "txtNotes",
            "lblCategoryList": "lblCategoryList",
            "imgCategoryListDropdown": "imgCategoryListDropdown",
            "btnViewEBill": "btnViewEBill",
            "lblDollar":"lblDollar",
            "lblSeparatorBottom": "lblSeparatorBottom",
            "btnActivateEbill": "btnActivateEbill"
          };
          var d = new Date().getFullYear() + "";
          this.view.tableView.segmentBillpay.widgetDataMap = dataMap;
          this.view.tableView.segmentBillpay.setData(billPayViewModel.formattedBulkBillPayRecords);
          this.view.tableView.segmentBillpay.setVisibility(true);
          CommonUtilities.Sorting.setSortingHandlers(scopeObj.allPayeesSortMap, scopeObj.onAllPayeeSortClickHanlder, scopeObj);
          CommonUtilities.Sorting.updateSortFlex(scopeObj.allPayeesSortMap, billPayViewModel.config, scopeObj);
          scopeObj.view.tableView.imgSortDatee.setVisibility(false);
          scopeObj.view.tableView.flxDate.onClick = null; }
          else {
          scopeObj.showNoPayementDetails({
            noPaymentMessageI18Key: "i18n.Bills.NoPayeesDetails",
            showPayeeActionMessage: "i18n.Bills.NoPayeesAddPayee"
          });
        }
      } else {
        scopeObj.showNoPayementDetails({
          noPaymentMessageI18Key: "i18n.Bills.NoPayeesDetails",
          showPayeeActionMessage: "i18n.Bills.ClickToAddPayee"
        });
      }
      this.hideProgressBar();
    },
    setPayABillUI: function () {
      this.view.btnConfirm.setVisibility(false);
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.topmenu.transferpay")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.topmenu.transferpay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.payABill.isVisible = true;
      this.view.flxOneTImePayment.setVisibility(false);
      //this.view.btnConfirm.setVisibility(false);
      this.view.tableView.isVisible = false;
      this.view.tableView.Search.setVisibility(false);
      this.view.forceLayout();
    },
    setAllPayeesUI: function () {
      this.selectedTab = "allPayees";
      // this.presenter.showBillPayData();
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.topmenu.transferpay")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.topmenu.transferpay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.setSkinActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
      this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
      this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
      this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
      this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
      this.view.tableView.isVisible = true;
      this.view.payABill.setVisibility(false);
      this.view.flxBillPay.setVisibility(true);
      this.view.flxPagination.setVisibility(true);
      this.view.flxBillPayManagePayeeActivitymodified.setVisibility(false);
      this.view.transferActivitymodified.setVisibility(false);
      this.view.transferActivitymodified.flxSegmentBillPay.setVisibility(false);
      this.view.tableView.flxBillPayAllPayees.setVisibility(true);
      this.view.tableView.flxPayFrom.setVisibility(true);
      this.view.tableView.tableTabs.flxTabsChecking.setVisibility(true);
      this.view.tableView.flxHorizontalLine3.setVisibility(true);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
      this.view.tableView.flxBillPayScheduled.setVisibility(false);
      this.view.flxOneTImePayment.setVisibility(false);
      this.view.flxActivatebillpays.setVisibility(false);
      this.view.btnConfirm.setVisibility(true);
      this.view.tableView.Search.setVisibility(false);
      this.view.tableView.flxNoPayment.setVisibility(false);
      this.view.tableView.flxBillPayAllPayees.setVisibility(true);
      this.view.tableView.tableSubHeaderHistory.setVisibility(false);
      this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
      this.view.tableView.lblDueDate.text = kony.i18n.getLocalizedString("i18n.billPay.DueDate");
      this.view.forceLayout();

    },
    showConfirmBulkPay: function() {
      var self = this;
      var bulkPayRecords = this.view.tableView.segmentBillpay.data;
      self.view.payABill.flxError.setVisibility(false);
      self.view.flxOneTImePayment.setVisibility(false);
      var selAccountID = "";
      var selAccountName = "";
      var selAccountType = "";
      var getLastFourDigit = function(numberStr) {
        if (numberStr.length < 4) return numberStr;
        return numberStr.slice(-4);
      };
       var selectedval =  this.view.tableView.lstbxPayFrom.selectedKeyValue[0];
       if(selectedval !== null && selectedval !== "" && selectedval !==undefined) {
        var accarr = selectedval.split("@");
        selAccountID = accarr[0]+"-"+CommonUtilitiesOCCU.getAccountTypeKey(accarr[3])+"-"+accarr[4];
        selAccountName =  accarr[1]+"("+'X' + getLastFourDigit(accarr[0])+ ")";
        selAccountType = accarr[2]+"("+CommonUtilitiesOCCU.getAccountTypeKey(accarr[3])+accarr[4]+")";
       }
      
      var records = [];
      var totalSum = 0;
      var index;

      function getDateFromConfiguration(dateComponents) {
        var dateFormat = CommonUtilities.getConfiguration('frontendDateFormat')
        if (dateFormat == 'dd/mm/yyyy') {
          return dateComponents.join("/");
        } else if (dateFormat == 'mm/dd/yyyy') {
          return dateComponents[1] + "/" + dateComponents[0] + "/" + dateComponents[2];
        }
      }
      for (index = 0; index < bulkPayRecords.length; index++) {
        var txtAmount = bulkPayRecords[index].txtAmount.text;
        if (txtAmount != '' && txtAmount != null && (parseFloat(txtAmount.replace(/,/g, "")) > 0)) {
          totalSum = totalSum + parseFloat(bulkPayRecords[index].txtAmount.text.replace(/,/g, ""));
          records.push({
            "lblPayee": bulkPayRecords[index].lblPayee,
            "lblSendOn": getDateFromConfiguration(bulkPayRecords[index].calSendOn.dateComponents),
            "lblDeliverBy": getDateFromConfiguration(bulkPayRecords[index].calEndDate.dateComponents),
            "lblAmount": self.presenter.formatCurrency((bulkPayRecords[index].txtAmount.text).replace(/,/g,"")),
            "lblPaymentAccount": selAccountName,
            "payeeId": bulkPayRecords[index].payeeId !== undefined ? bulkPayRecords[index].payeeId : bulkPayRecords[index].lblPayeeId,
            "billid": bulkPayRecords[index].billId !== undefined ? bulkPayRecords[index].billId : bulkPayRecords[index].lblBillId,
            "payeeAccountNumber": selAccountID,
            "accountNumber": selAccountID,
            "lblFrequency":bulkPayRecords[index].lblFrequencyList,
            "lblPaymentAccountType":selAccountType
          });
        }
      }
      billPayData = {};
      billPayData.records = records;
      billPayData.totalSum = self.presenter.formatCurrency(totalSum);
      billPayData.bulkPayRecords = bulkPayRecords;
      self.presenter.setDataForBulkBillPayConfirm(billPayData);
      self.view.payABill.flxError.setVisibility(false);
      this.view.forceLayout();
    },
    setSkinActiveAllPayees: function (obj) {
      obj.skin = "sknBtnAccountSummarySelected";
    },
    setServerError: function (isError) {
      var scopeObj = this;
      scopeObj.view.flxDowntimeWarning.setVisibility(isError);
      if (isError) {
        scopeObj.view.rtxDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
        scopeObj.view.flxMyPaymentAccounts.top = '130dp';
        scopeObj.view.flxBillPay.top = '130dp';
        scopeObj.view.flxTotalEbillAmountDue.top = '486dp';
        scopeObj.view.flxaddpayeemakeonetimepayment.top = '619dp';
      } else {
        scopeObj.view.flxMyPaymentAccounts.top = '0dp';
        //scopeObj.view.flxMyPaymentAccounts.top = '30dp';
        scopeObj.view.flxBillPay.top = '30dp';
        scopeObj.view.flxTotalEbillAmountDue.top = '20dp';
        //scopeObj.view.flxTotalEbillAmountDue.top = '386dp';
        scopeObj.view.flxaddpayeemakeonetimepayment.top = '0dp';
        //scopeObj.view.flxaddpayeemakeonetimepayment.top = '519dp';
      }
      scopeObj.view.forceLayout();
    },
      
    validateBulkPayData: function() {
      var data = this.view.tableView.segmentBillpay.data;
      var self = this;
      var isInputValid = false;
      var isValidRecordFound = false;
      for (record in data) {
        if (data[record].txtAmount.text !== '' && data[record].txtAmount.text !== null && parseFloat(data[record].txtAmount.text.replace(/,/g, "")) > 0) {
          isInputValid = true;
          isValidRecordFound = true;
        } else if (isNaN(data[record].txtAmount.text)) {
          isInputValid = false;
          break;
        }
      }
      var selectedval =  this.view.tableView.lstbxPayFrom.selectedKeyValue[0];      
      if (isInputValid) {        
        if(selectedval !== null && selectedval !== "" && selectedval !==undefined && selectedval !== "null"  && selectedval !== "Select") {
          self.hideErrorFlex();
          self.showConfirmBulkPay();
        } else {
          self.showErrorFlex(kony.i18n.getLocalizedString("i18n.Bills.selectAccount"));
        }        
      }
      else if(isValidRecordFound == false){
        self.showErrorFlex(kony.i18n.getLocalizedString("i18n.billPay.EnterAValidInput"));
      }
      else{
        self.showErrorFlex(kony.i18n.getLocalizedString("i18n.billPay.EnterAValidInput"));
      }
    },
      
    };
});