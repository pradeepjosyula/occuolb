define(['CommonUtilitiesOCCU','CommonUtilities','OLBConstants'], function (CommonUtilitiesOCCU,CommonUtilities, OLBConstants) {
   var windowsOpenSt = "";
   return{
     getAccountDeatilsAllAccountsViewModel:function (accounts) {
            var scopeObj = this;
            var createAccountViewModal = function (account) {
                return {
                    //construct the view modal for the account 
                    accountName: CommonUtilities.getAccountName(account),
                    accountNumber: displayAccountNumber(account.accountID),
                    type: account.accountType,
                    accountOwner: account.accountHolder +" | "+ account.accountID,    
                    accountshareID : account.accountName+" | "+account.accountType+"-"+account.shareId,
                    onAccountSelection: function () {
                        scopeObj.updateLoadingForCompletePage({
                            isLoading : true,
                            serviceViewModels : scopeObj.serviceViewModels[scopeObj.activeForm]
                        });
                        return scopeObj.showAccountDetails(account);
                    }
                };
            };
            return accounts.map(createAccountViewModal);
        },
  
	handleDateFormat: function(date) {
      if(date === undefined || date === null || date === ''){
        return null;
      }
      if(date instanceof Date){
        return ((date.getMonth()+1) < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1) +"/"+(date.getDate() < 10 ? '0' + date.getDate() : date.getDate())+"/"+date.getFullYear();    
      } else {
        var dateObj  = new Date(date); 
        return ((dateObj.getMonth()+1) < 10 ? '0' + (dateObj.getMonth()+1) : dateObj.getMonth()+1)+"/"+(dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate())+"/"+dateObj.getFullYear();         
      }
    },
    
    configureActionsForTags : function (searchViewModel) {
    var scopeObj = this;
    var tagConfig = [
      {
        actionOn: 'flxCancelKeyword',
        hide: ['lblKeywordTitle', 'lblKeywordValue'],
        clearPropertiesFromViewModel: [
          {propertyName: 'keyword', resetValue: ''}
        ],
        value: {label: 'lblKeywordValue', computedValue: function () {
          if (searchViewModel.keyword === "") {
            return null;
          }
          return searchViewModel.keyword;
        }}
      },
      {
        actionOn: 'flxCancelType',
        hide: ['lblTypeValue', 'lblTypeTitle'],
        clearPropertiesFromViewModel: [
          {propertyName: 'transactionTypeSelected', resetValue: OLBConstants.BOTH}
        ],
        value: {label: 'lblTypeValue', computedValue: function () {
          //if (searchViewModel.transactionTypeSelected === OLBConstants.BOTH) {
          //  return null;
         // }
          return searchViewModel.getTransactionTypeFromKey(searchViewModel.transactionTypeSelected);
        }}
      },
      {
        actionOn: 'flxCancelAmountRange',
        hide: ['lblAmountRangeTitle', 'lblAmountRangeValue'],
        clearPropertiesFromViewModel: [
          {propertyName: 'fromAmount', resetValue: ''},
          {propertyName: 'toAmount', resetValue: ''}
        ],
        value: {label: 'lblAmountRangeValue', computedValue: function () {
          if (searchViewModel.fromAmount === "" || searchViewModel.toAmount === "") {
            return null;
          }
          return searchViewModel.fromAmount + " to " + searchViewModel.toAmount;
        }}
      },
      {
        actionOn: 'flxCancelDateRange',
        hide: ['lblDateRangeTitle','lblDateRangeValue'],
        clearPropertiesFromViewModel: [
          {propertyName: 'timePeriodSelected', resetValue: OLBConstants.ANY_DATE}
        ],
        value: {label: 'lblDateRangeValue', computedValue: function () {
         // if (searchViewModel.timePeriodSelected === OLBConstants.ANY_DATE) {
          //  return null;
          //}
        //  else 
            if(searchViewModel.timePeriodSelected === OLBConstants.CUSTOM_DATE_RANGE) {
            return searchViewModel.fromDate + " " +  kony.i18n.getLocalizedString('i18n.common.to') + " " +  searchViewModel.toDate;
          }
          return searchViewModel.getTimePeriodFromKey(searchViewModel.timePeriodSelected);
        }}
      },
      {
        actionOn: 'flxCancelCheckNumber',
        hide: ['lblCheckNumberTitle','lblCheckNumberValue'],
        clearPropertiesFromViewModel: [
          {propertyName: 'fromCheckNumber', resetValue: ''},
          {propertyName: 'toCheckNumber', resetValue: ''}
        ],
        value: {label: 'lblCheckNumberValue', computedValue: function () {
          if (searchViewModel.fromCheckNumber === "" || searchViewModel.toCheckNumber === "") {
            return null;
          }
          return searchViewModel.fromCheckNumber + " " +  kony.i18n.getLocalizedString('i18n.common.to') + " " + searchViewModel.toCheckNumber;
        }}
      }
    ];

    function generateClickListenerForTag (config) {
      return function () {
        hideTag(config);
         scopeObj.tagState.decrement();            
        config.clearPropertiesFromViewModel.forEach(function (property) {
          searchViewModel[property.propertyName] = property.resetValue;
        });
        scopeObj.updateProgressBarState(true);
        if (scopeObj.tagState.visible === 0) {
          searchViewModel.clearSearch(searchViewModel);
        }
        else {
          searchViewModel.performSearch(searchViewModel);        
        }
        scopeObj.view.transactions.forceLayout();
      };
    }

    function hideTag (config) {
      scopeObj.view.transactions[config.actionOn].setVisibility(false);
      config.hide.forEach(function (widgetToHide) {
        scopeObj.view.transactions[widgetToHide].setVisibility(false);
      });
      scopeObj.view.transactions.forceLayout();
    }

    function showTag (config) {
      scopeObj.view.transactions[config.actionOn].setVisibility(true);
      config.hide.forEach(function (widgetToHide) {
        scopeObj.view.transactions[widgetToHide].setVisibility(true);
      });
      scopeObj.view.transactions.forceLayout();  
      scopeObj.tagState.increment();    
    }

    this.tagState.visible = 0;    
    tagConfig.forEach(function (config) {
      if (config.value.computedValue() === null){
        hideTag(config);
      }
      else {
        showTag(config);
        scopeObj.view.transactions[config.actionOn].onClick = generateClickListenerForTag(config);
        scopeObj.view.transactions[config.value.label].text = config.value.computedValue();
      }
    });



  }, 
     
     
     
     
    willUpdateUI: function (viewModel) {
      if (viewModel) {
        //new code
        if(viewModel.checkDetails){
          this.view.CheckImage.lblPostDateValue.text = this.handleDateFormat(kony.application.getCurrentForm().transactions.segTransactions.selectedItems[0].lblDate);
          this.view.CheckImage.lblAmountValue.text = kony.application.getCurrentForm().transactions.segTransactions.selectedItems[0].lblAmount;
          this.view.CheckImage.imgCheckImage.base64 = viewModel.checkDetails.imgCheck1;
          this.view.CheckImage.imgCheckImage.base64 = viewModel.checkDetails.imgCheck2;
        	this.view.flxCheckImage.setVisibility(true);
        }
        
        if (viewModel.progressBar !== undefined){
          this.updateProgressBarState(viewModel.progressBar);
        } else if (viewModel.serverError)  {
          this.setServerError(viewModel.serverError);
        } else {
          if (viewModel.accountList) {
            this.updateAccountList(viewModel.accountList);
          }
          if (viewModel.accountDetails) {
            setTimeout(this.alignAccountTypesToAccountSelectionImg, 500);
            this.showAccountSummary();
            this.showAccountDetailsScreen();
            this.updateAccountTransactionsTypeSelector(viewModel.accountDetails.accountInfo.accountType);
            this.updateAccountDetails(viewModel.accountDetails);
          }
          if(viewModel.showDownloadStatement){
            this.downloadUrl(viewModel.showDownloadStatement);
          }
          if (viewModel.action==="Account Details Success") {
            this.setDataToLoanOnModify(viewModel.data[0],viewModel.data[1]);
          }
          if (viewModel.action==="Navigation To AccountDetails"){
            this.backToAccountDetailsCallback(viewModel.data);
          }
          if (viewModel.action==="Navigation To eStatements"){
            this.showViewStatements();
          }
          if (viewModel.transactionDetails) {
            this.highlightTransactionType(viewModel.transactionDetails.showingType);
            if (viewModel.transactionDetails.renderTransactions) {
              this.updateTransactions(viewModel.transactionDetails);
              this.updatePaginationBar(viewModel.transactionDetails);
            }
            this.updateSearchTransactionView(viewModel.transactionDetails.searchViewModel)
          	if (viewModel.transactionDetails.renderTransactionTabs == 0) {
                            this.view.transactions.flxTabsChecking.isVisible = true;
                            this.view.transactions.flxTabsCredit.isVisible = false;
                            this.view.transactions.flxTabsDeposit.isVisible = false;
                            this.view.transactions.flxTabsLoan.isVisible = false;
                            //this.view.transactions.flxSortType.isVisible = true;
                            //this.view.transactions.lblSortType.isVisible = true;
                            //this.view.transactions.imgSortType.isVisible = true;
                            this.view.transactions.flxPagination.isVisible = true;
                            this.view.transactions.flxSegmentContainer.isVisible = true;
                            this.view.transactions.segTransactions.isVisible = true;
                            this.view.transactions.flxSort.isVisible = true;
                            this.view.transactions.flxSearchContainer.isVisible = false;
                            //this.view.transactions.flxSearchNew.isVisible = false;
                            this.view.transactions.flxSearchResults.isVisible = false;
                            this.view.transactions.flxSeparatorSearch.isVisible = false;
                            this.view.transactions.flxNoTransactions.isVisible = false;
                            this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;
                        } else if (viewModel.transactionDetails.renderTransactionTabs == 1) {
                            this.view.transactions.flxTabsChecking.isVisible = true;
                            this.view.transactions.flxTabsCredit.isVisible = false;
                            this.view.transactions.flxTabsDeposit.isVisible = false;
                            this.view.transactions.flxTabsLoan.isVisible = false;
                            //this.view.transactions.flxSortType.isVisible = false;
                            //this.view.transactions.lblSortType.isVisible = false;
                            //this.view.transactions.imgSortType.isVisible = false;
                            this.view.transactions.flxPagination.isVisible = true;
                            this.view.transactions.flxSegmentContainer.isVisible = true;
                            this.view.transactions.segTransactions.isVisible = true;
                            this.view.transactions.flxSort.isVisible = true;
                            this.view.transactions.flxSearchContainer.isVisible = false;
                            //this.view.transactions.flxSearchNew.isVisible = false;
                            this.view.transactions.flxSearchResults.isVisible = true;
                            this.view.transactions.flxSeparatorSearch.isVisible = false;
                            this.view.transactions.flxNoTransactions.isVisible = false;
                            this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;
                        } else if (viewModel.transactionDetails.renderTransactionTabs == 2) {
                            this.view.transactions.flxTabsChecking.isVisible = true;
                            this.view.transactions.flxTabsCredit.isVisible = false;
                            this.view.transactions.flxTabsDeposit.isVisible = false;
                            this.view.transactions.flxTabsLoan.isVisible = false;
                            //this.view.transactions.flxSortType.isVisible = false;
                            //this.view.transactions.lblSortType.isVisible = false;
                            //this.view.transactions.imgSortType.isVisible = false;
                            this.view.transactions.flxPagination.isVisible = true;
                            this.view.transactions.flxSegmentContainer.isVisible = true;
                            this.view.transactions.segTransactions.isVisible = true;
                            this.view.transactions.flxSort.isVisible = true;
                            this.view.transactions.flxSearchContainer.isVisible = true;
                            //this.view.transactions.flxSearchNew.isVisible = false;
                            this.view.transactions.flxSearchResults.isVisible = false;
                            this.view.transactions.flxSeparatorSearch.isVisible = false;
                            this.view.transactions.flxNoTransactions.isVisible = false;
                            this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;
                        } else {
                            this.view.transactions.flxNoTransactions.isVisible = true;
                           this.view.transactions.flxPagination.isVisible = false;
                            this.view.transactions.flxSegmentContainer.isVisible = true;
                            this.view.transactions.segTransactions.isVisible = false;
                            this.view.transactions.flxSearchNew.isVisible = true;
                          	this.view.transactions.flxSort.isVisible = true;
							//this.view.transactions.flxSearchResults.isVisible = true;
                        }
          }
          if (viewModel.sideMenu) {
            this.updateHamburgerMenu(viewModel.sideMenu);
          }
          if (viewModel.sideMenuSt) {
            this.updateHamburgerMenuSt(viewModel.sideMenuSt);
          }
          if (viewModel.topBar) {
            this.updateTopBar(viewModel.topBar);
          }  
        }      
      }
      this.AdjustScreen();
    },    
  updateHamburgerMenu: function (sideMenuModel) {
       this.view.customheader.initHamburger(sideMenuModel, "Accounts", "My Accounts");
  },
  updateHamburgerMenuSt: function (sideMenuModel) {
       this.view.customheader.initHamburger(sideMenuModel, "Accounts", "Statements");
       this.view.customheader.customhamburger.activateMenu("ACCOUNTS", "Statements");
  },
  preshowFrmAccountDetails: function () {
    var scopeObj = this;
    this.view.customheader.forceCloseHamburger();
    this.view.accountInfo.isVisible = false;
    this.view.accountTypes.isVisible = false;
    this.view.moreActions.isVisible = false;
    this.view.flxEditRule.isVisible = false;
    this.view.forceLayout();
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    /// Start
    this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "slFbox";
    this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtmffffffOccu";
    this.view.customheader.topmenu.imgTransfers.src = "sendmoney.png";
    this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.lblFeedback.skin = "sknLblGtmffffffOccu";
    this.view.customheader.topmenu.imgFeedback.src = "feedback.png";
    //this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    /// End
    if(CommonUtilities.getConfiguration("printingTransactionDetails")==="true"){
      this.view.transactions.imgPrintIcon.setVisibility(true);
      this.view.transactions.imgPrintIcon.onTouchStart = this.onClickPrint;
    }
    else this.view.transactions.imgPrintIcon.setVisibility(false);
    this.view.forceLayout();

    this.view.breadcrumb.setBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.accounts.accountsTitle")
    }, {
      text: kony.i18n.getLocalizedString("i18n.accounts.accountDetails")
    }]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.accounts.accountsTitle");
	this.view.breadcrumb.lblBreadcrumb2.toolTip= kony.i18n.getLocalizedString("i18n.accounts.accountDetails");
    
    //SET ACTIONS:
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainWrapper.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CheckImage.flxImgCancel.onClick = function() {
      scopeObj.view.flxCheckImage.setVisibility(false);
    };
    this.view.CheckImage.flxFlip.onClick = function() {
      if (scopeObj.view.CheckImage.imgCheckImage.src === "check_img.png") scopeObj.view.CheckImage.imgCheckImage.src = "check_back.png";
      else scopeObj.view.CheckImage.imgCheckImage.src = "check_img.png";
    };
    this.view.flxCheckImage.setVisibility(false);
    this.view.CustomPopup.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";

    };
    this.view.editRule.btnCancel.onClick = function () {
      scopeObj.view.flxEditRule.isVisible = false;
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.flxAccountTypes.onClick = function () {
      kony.print("flxAccountTypes clicked");
      scopeObj.showAccountTypes();
    };
    this.view.imgAccountTypes.onTouchStart = function () {
      kony.print("imgAccountTypes onTouchStart");
      scopeObj.showAccountTypes();
    };    
    this.view.btnViewAccountInfo.onClick = function () {
      scopeObj.showAccountInfo();
    };
    this.view.flxSecondaryActions.onClick = function () {
      scopeObj.showMoreActions();
    };
    this.view.accountSummary.btnAccountSummary.onClick = function () {
      scopeObj.showAccountSummary();
    };
    this.view.accountSummary.btnBalanceDetails.onClick = function () {
      scopeObj.showBalanceDetails();
    };
    this.view.transactions.flxDownload.onClick = function () {
      scopeObj.view.flxDownloadTransaction.isVisible = true;
    };

    this.view.downloadTransction.imgClose.onTouchEnd = function () {
      scopeObj.view.flxDownloadTransaction.isVisible = false;
    };
//     this.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.onClick = function(){
//       scopeObj.noOfClonedChecks++;
//       var newCheck=scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.clone(scopeObj.noOfClonedChecks);
//       scopeObj.view.StopCheckPaymentSeriesMultiple.flxStopCheckPayments.addAt(newCheck,3);
//     };
    this.view.downloadTransction.btnCancel.onClick = function () {
      scopeObj.view.flxDownloadTransaction.isVisible = false;
      scopeObj.presenter.fetchUpdatedAccountDetails(scopeObj.accID);
      scopeObj.showAccountDetailsScreen();
    };
  },

    setRightSideActions : function(viewModel){
      var scopeObj = this;
      var dataItem , j;
      viewModel = viewModel || [];
      // Right action buttonas
      scopeObj.rightActionButtons = [
        scopeObj.view.btnScheduledTransfer,
        scopeObj.view.btnMakeTransfer,
        scopeObj.view.btnPayABill
      ];
      // Right action saparators
      scopeObj.rightActionSeparator = [
        scopeObj.view.flxSeparatorPrimaryActions,
        scopeObj.view.flxSeparatorPrimaryActionsTwo
      ];
      scopeObj.resetRightSideActions(); //Reset Actions
      if(viewModel.length === 0) {
        scopeObj.view.flxPrimaryActions.setVisibility(false);
      }
      else {
        scopeObj.view.flxPrimaryActions.setVisibility(true);
        for (var i = 0; i < 2 && viewModel.length >= 1; i++) {
          dataItem = viewModel[i];
          var displayName = dataItem.displayName;
          displayName= displayName.toUpperCase();
          scopeObj.rightActionButtons[i].text = displayName;
          scopeObj.rightActionButtons[i].setVisibility(true);
          scopeObj.rightActionButtons[i].onClick = dataItem.action;
          scopeObj.rightActionButtons[i].toolTip = dataItem.displayName;
          j = i-1;
          if(j>=0 && j <scopeObj.rightActionSeparator.length) {
            scopeObj.rightActionSeparator[j].setVisibility(true);
          }
        }
      }
    },

    updateAccountDetails: function (accountDetailsModel) {
      var scopeObj = this;
      //modified for occu project
      //this.view.accountSummary.flxIdentifier.skin = this.getSkinForAccount(accountDetailsModel.accountType);
      var hexcolor = accountDetailsModel.accountSummary.hexcolor;
      hexcolor = hexcolor.substring(1,hexcolor.length);
      this.view.accountSummary.flxIdentifier.skin = "skin"+hexcolor;
      var getLastFourDigit = function (numberStr) {
        if (numberStr.length < 4) return numberStr;
        return numberStr.slice(-4);
      };
      this.accountDetailsModel = accountDetailsModel;
      //this.view.lblAccountTypes.text = this.view.lblAccountTypes.toolTip = accountDetailsModel.accountInfo.accountName +
      // ' xxxxxxxxxxxx' + getLastFourDigit(accountDetailsModel.accountInfo.accountNumber);

      //updated lblAccountsType for OCCU
      this.view.lblAccountTypes.text = this.view.lblAccountTypes.toolTip =  accountDetailsModel.accountInfo.accountName +'  |  '+ accountDetailsModel.accountInfo.accountType +"  "+accountDetailsModel.accountInfo.shareId +"  |  "+ accountDetailsModel.accountInfo.accountHolder +"  |  "+ ' X ' + getLastFourDigit(accountDetailsModel.accountInfo.accountID) //accountDetailsModel.accountInfo.accountName +' | '+ ' X' + getLastFourDigit(accountDetailsModel.accountInfo.accountNumber);
      //this.view.lblAccountTypes.text = this.view.lblAccountTypes.toolTip = accountDetailsModel.accountInfo.accountName +
      // ' xxxxxxxxxxxx' + getLastFourDigit(accountDetailsModel.accountInfo.accountNumber);

      this.view.AllForms.lblInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.Information");
      if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.SAVING) {
        this.view.accountSummary.flxExtraField.isVisible = false;
        this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
        this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
        this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.currentBalance;
        this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingDeposit");
        this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.pendingDeposit;
        this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingWithdrawals");
        this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.accountSummary.pendingWithdrawals;


        this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
        this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.dividendRate;
        this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
        this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.dividendPaidYTD;
        this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastDividendPaid");
        this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.balanceAndOtherDetails.lastDividendPaid;
        this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paidOn");
        this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.paidOn;
        this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalCredits");
        this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.totalCredit;
        this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalDebts");
        this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.totalDebits;
        this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
        //Added new label for currentBalance for OCCU
        this.view.accountSummary.lblCurrentBalanceTitleNew.text = accountDetailsModel.accountSummary.productBalanceType;
        if(accountDetailsModel.accountSummary.productBalance == null || accountDetailsModel.accountSummary.productBalance == undefined){
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }else{
        this.view.accountSummary.lblCurrentBalanceValueNew.text = accountDetailsModel.accountSummary.productBalance;
        }
        this.view.accountSummary.lblAvailableBalanceTitle.text = accountDetailsModel.accountSummary.balanceType;
        //this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
        if(accountDetailsModel.accountSummary.availableBalance == undefined){
          this.view.accountSummary.lblAvailableBalanceValue.text = " ";
        }else{
          this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
        }
        this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.SavingCurrentAccount");
        this.view.accountSummary.flxDummyOne.isVisible = false;
        this.view.accountSummary.flxDummyTwo.isVisible = false;
      }
      if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.CREDITCARD) {
        this.view.accountSummary.flxExtraField.isVisible = false;
        this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndBillDetail");

        this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
        this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.currentBalance;
        this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
        this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.currentDueAmount;
        this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.creditLimit");
        this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.accountSummary.creditLimit;
        this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
        //OCCU for acccount Summary
        this.view.accountSummary.lblAvailableBalanceTitle.text = accountDetailsModel.accountSummary.balanceType;
        if(accountDetailsModel.accountSummary.availableCredit == null ||accountDetailsModel.accountSummary.availableCredit == undefined){
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }else{
        this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableCredit;
        }
        //this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableCredit;

        this.view.accountSummary.lblCurrentBalanceTitleNew.text = accountDetailsModel.accountSummary.productBalanceType;
        if(accountDetailsModel.accountSummary.productBalance == null || accountDetailsModel.accountSummary.productBalance == undefined){
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }else{
        this.view.accountSummary.lblCurrentBalanceValueNew.text = accountDetailsModel.accountSummary.productBalance;
        }

        if(accountDetailsModel.accountSummary.availableBalance == null ||accountDetailsModel.accountSummary.availableBalance == undefined){
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }else{
        this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
        }

        this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
        this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.accountSummary.currentDueAmount;
        this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.minimumDueAmount");
        this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.minimumDueAmount;
        this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paymentDueDate");
        this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.paymentDueDate;
        this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastStatementBalance");
        this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.lastStatementBalance;
        this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentAmount");
        this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.balanceAndOtherDetails.lastPaymentAmount;
        this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentDate");
        this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.lastPaymentDate;
        this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.rewardBalance");
        this.view.accountSummary.lblDummyOneValue.text = accountDetailsModel.balanceAndOtherDetails.rewardsBalance;
        this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
        this.view.accountSummary.lblDummyTwoValue.text = accountDetailsModel.balanceAndOtherDetails.interestRate;
        this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.CreditCard");

        this.view.accountSummary.flxDummyOne.isVisible = true;
        this.view.accountSummary.flxDummyTwo.isVisible = true;
      }
      if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.LOAN || accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.MORTGAGE) {
        this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestDetail");

        this.view.accountSummary.flxExtraField.isVisible = false;
        this.view.accountSummary.lblExtraFieldTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.principalBalance");
        this.view.accountSummary.lblExtraFieldValue.text = accountDetailsModel.accountSummary.principalBalance;
        this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.principalAmount");
        this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.principalAmount;
        this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
        this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.currentDueAmount;
        this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.nextPaymentDueDate");
        this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.balanceAndOtherDetails.paymentDueDate;

        this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
        this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.interestRate;
        this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestPaidYTD");
        this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.interestPaidYTd;
        this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestPaidLastYear");
        this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.interestPaidLastYear;
        this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentAmount");
        this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.lastPaymentAmount;

        this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastPaymentDate");
        this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.balanceAndOtherDetails.lastPaymentDate;
        this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.originalAmount");
        this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.originalAmount;
        this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.originalDate");
        this.view.accountSummary.lblDummyOneValue.text = accountDetailsModel.balanceAndOtherDetails.originalDate;
        this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.payoffAmount");
        this.view.accountSummary.lblDummyTwoValue.text = accountDetailsModel.accountSummary.principalBalance;
        this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
        //For AccountSummary OCCU
        //this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.outstandingBalance");
        if(accountDetailsModel.accountSummary.availableBalance == null ||accountDetailsModel.accountSummary.availableBalance == undefined){
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }else{
        this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
        }
        this.view.accountSummary.lblAvailableBalanceTitle.text = accountDetailsModel.accountSummary.balanceType;
        //this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableCredit;
        this.view.accountSummary.lblCurrentBalanceTitleNew.text = accountDetailsModel.accountSummary.productBalanceType;
        if(accountDetailsModel.accountSummary.productBalance == null || accountDetailsModel.accountSummary.productBalance == undefined){
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }else{
        this.view.accountSummary.lblCurrentBalanceValueNew.text = accountDetailsModel.accountSummary.productBalance;
        }

        this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.Loan");
        this.view.accountSummary.flxDummyOne.isVisible = true;
        this.view.accountSummary.flxDummyTwo.isVisible = true;
      }
      if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.DEPOSIT) {
        this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestAndMaturityDetail");

        this.view.accountSummary.flxExtraField.isVisible = true;
        this.view.accountSummary.lblExtraFieldTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
        this.view.accountSummary.lblExtraFieldValue.text = accountDetailsModel.accountSummary.currentBalance;
        this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestEarned");
        this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.interestEarned;
        this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityDate");
        this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.maturityDate;
        this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.term");
        this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.accountSummary.paymentTerm;

        this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
        this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.dividendRate;
        this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
        this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.dividendPaidYTD;
        this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendLastPaidAmount");
        this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.dividendLastPaidAmount;
        this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendLastPaidDate");
        this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.dividendLastPaidDate;

        this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityDate");
        this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.accountSummary.maturityDate;
        this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityAmount");
        this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.maturityAmount;
        this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.maturityOption");
        this.view.accountSummary.lblDummyOneValue.text = accountDetailsModel.accountSummary.maturityOption;
        this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.Deposit");
        this.view.accountSummary.flxDummyOne.isVisible = true;
        this.view.accountSummary.flxDummyTwo.isVisible = false;

        this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
        //Code For AccountSummary OCCU
        //this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
        if(accountDetailsModel.accountSummary.availableBalance == null ||accountDetailsModel.accountSummary.availableBalance == undefined){
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }else{
        this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
        }
        this.view.accountSummary.lblCurrentBalanceTitleNew.text = accountDetailsModel.accountSummary.productBalanceType;
        if(accountDetailsModel.accountSummary.productBalance == null || accountDetailsModel.accountSummary.productBalance == undefined){
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }else{
        this.view.accountSummary.lblCurrentBalanceValueNew.text = accountDetailsModel.accountSummary.productBalance;
        }
        this.view.accountSummary.lblAvailableBalanceTitle.text = accountDetailsModel.accountSummary.balanceType;
      }
      if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.CHECKING || accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.CURRENT) {
        this.view.accountSummary.flxExtraField.isVisible = false;
        this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
        this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
        this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.currentBalance;
        this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingDeposit");
        this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.pendingDeposit;
        this.view.accountSummary.lblPendingWithdrawalsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.pendingWithdrawals");
        this.view.accountSummary.lblPendingWithdrawalsValue.text = accountDetailsModel.accountSummary.pendingWithdrawals;

        this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalCredits");
        this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.totalCredit;
        this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.totalDebts");
        this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.totalDebits;
        this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.bondInterestLastYear");
        this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.bondInterestLastYear;
        this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidLastYear");
        this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.dividendPaidYTD;

        this.view.accountSummary.lblLastDividentPaidTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendRate");
        this.view.accountSummary.lblLastDividentPaidValue.text = accountDetailsModel.balanceAndOtherDetails.dividendRate;
        this.view.accountSummary.lblPaidOnTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dividendPaidYDT");
        this.view.accountSummary.lblPaidOnValue.text = accountDetailsModel.balanceAndOtherDetails.dividendPaidYTD;
        this.view.accountSummary.lblDummyOneTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.lastDividendPaid");
        this.view.accountSummary.lblDummyOneValue.text = accountDetailsModel.balanceAndOtherDetails.lastDividendPaid;
        this.view.accountSummary.lblDummyTwoTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.paidOn");
        this.view.accountSummary.lblDummyTwoValue.text = accountDetailsModel.balanceAndOtherDetails.paidOn;

        this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
        this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
        if(accountDetailsModel.accountSummary.availableBalance == null ||accountDetailsModel.accountSummary.availableBalance == undefined){
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }else{
        this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
        }
        this.view.AllForms.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.iIcon.accounts.SavingCurrentAccount");

        this.view.accountSummary.flxDummyTwo.isVisible = true;
        this.view.accountSummary.flxDummyTwo.isVisible = true;
      }
      if (accountDetailsModel.accountInfo.accountType == OLBConstants.ACCOUNT_TYPE.LINE_OF_CREDIT) {
        this.view.accountSummary.btnBalanceDetails.text = kony.i18n.getLocalizedString("i18n.accountDetail.balanceAndInterestDetail");
        this.view.accountSummary.flxExtraField.isVisible = false;
        this.view.accountSummary.flxPendingWithdrawals.isVisible = false;
        this.view.accountSummary.lblCurrentBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
        this.view.accountSummary.lblCurrentBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
        this.view.accountSummary.lblPendingDepositsTitle.text = kony.i18n.getLocalizedString("i18n.accounts.currentBalance");
        this.view.accountSummary.lblPendingDepositsValue.text = accountDetailsModel.accountSummary.currentBalance;

        this.view.accountSummary.lblTotalCreditsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.interestRate");
        this.view.accountSummary.lblTotalCreditsValue.text = accountDetailsModel.balanceAndOtherDetails.interestRate;
        this.view.accountSummary.lblTotalDebtsTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.regularPaymentAmount");
        this.view.accountSummary.lblTotalDebtsValue.text = accountDetailsModel.balanceAndOtherDetails.regularPaymentAmount;
        this.view.accountSummary.lblDividentRateTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.currentDueAmount");
        this.view.accountSummary.lblDividentRateValue.text = accountDetailsModel.balanceAndOtherDetails.currentDueAmount;
        this.view.accountSummary.lblDividentRateYTDTitle.text = kony.i18n.getLocalizedString("i18n.accountDetail.dueDate");
        this.view.accountSummary.lblDividentRateYTDValue.text = accountDetailsModel.balanceAndOtherDetails.paymentDueDate;

        this.view.accountSummary.flxBalanceDetailsRight.isVisible = false;
        this.view.accountSummary.lblAsOf.text = accountDetailsModel.accountSummary.asOfDate;
        //Code for Account Summary
        //this.view.accountSummary.lblAvailableBalanceTitle.text = kony.i18n.getLocalizedString("i18n.accounts.availableBalance");
        if(accountDetailsModel.accountSummary.availableBalance == null ||accountDetailsModel.accountSummary.availableBalance == undefined){
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }else{
        this.view.accountSummary.lblAvailableBalanceValue.text = accountDetailsModel.accountSummary.availableBalance;
        }
        this.view.accountSummary.lblCurrentBalanceTitleNew.text = accountDetailsModel.accountSummary.productBalanceType;
        if(accountDetailsModel.accountSummary.productBalance == null || accountDetailsModel.accountSummary.productBalance == undefined){
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }else{
        this.view.accountSummary.lblCurrentBalanceValueNew.text = accountDetailsModel.accountSummary.productBalance;
        }
        this.view.accountSummary.lblAvailableBalanceTitle.text = accountDetailsModel.accountSummary.balanceType;

        this.view.accountSummary.flxDummyOne.isVisible = false;
        this.view.accountSummary.flxDummyTwo.isVisible = false;
        this.view.accountSummary.flxPaidOn.isVisible = false;
      }
      this.setRightSideActions(accountDetailsModel.rightSideActions); //Set Right side actions
      this.setSecondayActions(accountDetailsModel.secondaryActions); //Set Secondary actions

      this.updateAccountInfo(accountDetailsModel);
      this.view.forceLayout();

    },
    //for Transactions
    TransactionTabActions: function(){
      var scopeObj = this;
      scopeObj.view.transactions.btnDepositsChecking.onClick =  function(){
        scopeObj.presenter.showDepositsExt();
      }
      scopeObj.view.transactions.btnAllChecking.onClick =  function(){
        scopeObj.presenter.showAllExt();
      }
      scopeObj.view.transactions.btnTransfersChecking.onClick =  function(){
        scopeObj.presenter.showTransfersExt();
      }
      scopeObj.view.transactions.btnChecksChecking.onClick =  function(){
        scopeObj.presenter.showChecksExt();
      }
      scopeObj.view.transactions.btnWithdrawsChecking.onClick =  function(){
        scopeObj.presenter.showWithdrawalsExt();
      }
      scopeObj.view.transactions.btnPurchasesCredit.onClick =  function(){
        scopeObj.presenter.showPurchaseExt();
      }
      scopeObj.view.transactions.btnPaymentsCredit.onClick =  function(){
        scopeObj.presenter.showPaymentExt();
      }
      scopeObj.view.transactions.btnInterestDeposit.onClick =  function(){
        scopeObj.presenter.showInterestExt();
      }


    },
    updateAccountTransactionsTypeSelector: function (accountType) {
      var accountTypeConfig = {
        'Savings': function () {
          this.view.transactions.flxTabsChecking.isVisible = true;
          this.view.transactions.flxTabsCredit.isVisible = false;
          this.view.transactions.flxTabsDeposit.isVisible = false;
          this.view.transactions.flxTabsLoan.isVisible = false;
          this.view.transactions.flxSortType.isVisible = true;
          this.view.transactions.lblSortType.isVisible = true;
          //this.view.transactions.imgSortType.isVisible = true;
          this.view.accountSummary.flxExtraField.isVisible = false;
          this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;
        },
        'Current': function () {
          this.view.transactions.flxTabsChecking.isVisible = true;
          this.view.transactions.flxTabsCredit.isVisible = false;
          this.view.transactions.flxTabsDeposit.isVisible = false;
          this.view.transactions.flxTabsLoan.isVisible = false;
          this.view.transactions.flxSortType.isVisible = false;
          this.view.accountSummary.flxExtraField.isVisible = false;
          this.view.transactions.lblSortType.isVisible = false;
          this.view.transactions.imgSortType.isVisible = false;
          this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;
        },
        'CreditCard': function () {
          this.view.transactions.flxTabsChecking.isVisible = false;
          this.view.transactions.flxTabsCredit.isVisible = false;
          this.view.transactions.flxTabsDeposit.isVisible = false;
          this.view.transactions.flxTabsLoan.isVisible = false;
          this.view.transactions.flxSortType.isVisible = false;
          this.view.transactions.lblSortType.isVisible = false;
          this.view.transactions.imgSortType.isVisible = false;
          this.view.transactions.flxPagination.isVisible = false;
          this.view.transactions.flxSegmentContainer.isVisible = true;
          this.view.transactions.flxSort.isVisible = false;
          this.view.transactions.flxSearchContainer.isVisible = false;
          this.view.transactions.flxSearchNew.isVisible = false;
          this.view.transactions.flxSearchResults.isVisible = false;
          this.view.transactions.flxSeparatorSearch.isVisible = false;
          this.view.transactions.flxNoTransactions.isVisible = false;
          this.view.transactions.flxCreditCardMortgageMessage.isVisible = true;
        },
        'Mortgage': function () {
          this.view.transactions.flxTabsChecking.isVisible = false;
          this.view.transactions.flxTabsCredit.isVisible = false;
          this.view.transactions.flxTabsDeposit.isVisible = false;
          this.view.transactions.flxTabsLoan.isVisible = false;
          this.view.transactions.flxSortType.isVisible = false;
          this.view.transactions.lblSortType.isVisible = false;
          this.view.transactions.imgSortType.isVisible = false;
          this.view.transactions.flxPagination.isVisible = false;
          this.view.transactions.flxSegmentContainer.isVisible = true;
          this.view.transactions.flxSort.isVisible = false;
          this.view.transactions.flxSearchContainer.isVisible = false;
          this.view.transactions.flxSearchNew.isVisible = false;
          this.view.transactions.flxSearchResults.isVisible = false;
          this.view.transactions.flxSeparatorSearch.isVisible = false;
          this.view.transactions.flxNoTransactions.isVisible = false;
          this.view.transactions.flxCreditCardMortgageMessage.isVisible = true;
        },
        'Loan': function () {
          this.view.transactions.flxTabsChecking.isVisible = false;
          this.view.transactions.flxTabsCredit.isVisible = false;
          this.view.transactions.flxTabsDeposit.isVisible = false;
          this.view.transactions.flxTabsLoan.isVisible = true;
          this.view.transactions.flxSortType.isVisible = false;
          this.view.transactions.lblSortType.isVisible = false;
          this.view.transactions.imgSortType.isVisible = false;
          this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;

        },
        'Deposit': function () {
          this.view.transactions.flxTabsChecking.isVisible = false;
          this.view.transactions.flxTabsCredit.isVisible = false;
          this.view.transactions.flxTabsDeposit.isVisible = true;
          this.view.transactions.flxTabsLoan.isVisible = false;
          this.view.transactions.flxSortType.isVisible = false;
          this.view.transactions.lblSortType.isVisible = false;
          this.view.transactions.imgSortType.isVisible = false;
          this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;
        },
        'Checking': function () {
          this.view.transactions.flxTabsChecking.isVisible = true;
          this.view.transactions.flxTabsCredit.isVisible = false;
          this.view.transactions.flxTabsDeposit.isVisible = false;
          this.view.transactions.flxTabsLoan.isVisible = false;
          this.view.transactions.flxSortType.isVisible = true;
          this.view.accountSummary.flxExtraField.isVisible = false;
          this.view.transactions.lblSortType.isVisible = true;
          //this.view.transactions.imgSortType.isVisible = true;
          this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;
        },
        'AccountTypeNotFound': function () {
          this.view.transactions.flxTabsChecking.isVisible = false;
          this.view.transactions.flxTabsCredit.isVisible = false;
          this.view.transactions.flxTabsDeposit.isVisible = false;
          this.view.transactions.flxTabsLoan.isVisible = true;
          this.view.transactions.flxSortType.isVisible = false;
          this.view.accountSummary.flxExtraField.isVisible = false;
          this.view.transactions.lblSortType.isVisible = false;
          this.view.transactions.imgSortType.isVisible = false;
          this.view.transactions.flxCreditCardMortgageMessage.isVisible = false;
        }
      };
      if (accountTypeConfig[accountType]) {
        accountTypeConfig[accountType].bind(this)();
      } else {
        kony.print('In Config, Could Not Find Account Type : ', accountType);
        accountTypeConfig.AccountTypeNotFound.bind(this)();
      }
    },
    
     highlightTransactionType: function(transactionType) {
       var skins = {
         selected: 'sknBtnAccountSummarySelected',
         unselected: 'sknBtnAccountSummaryUnselected',
         hover: 'sknBtnAccountSummarySelected',
         hovered: 'sknBtnAccountSummaryUnselected'
       };
       var transactionTypeToButtonIdMap = {
         'All': ['btnAllChecking', 'btnAllCredit', 'btnAllDeposit', 'btnAllLoan'],
         'Checks': ['btnChecksChecking'],
         'Deposits': ['btnDepositsChecking', 'btnDepositDeposit'],
         'Transfers': ['btnTransfersChecking'],
         'Withdrawals': ['btnWithdrawsChecking', 'btnWithdrawDeposit'],
         'Payments': ['btnPaymentsCredit'],
         'Purchases': ['btnPurchasesCredit'],
         'Interest': ['btnInterestDeposit']
       };
       var tabs = [this.view.transactions.flxTabsChecking,
                   this.view.transactions.flxTabsCredit,
                   this.view.transactions.flxTabsDeposit,
                   this.view.transactions.flxTabsLoan
                  ];
       var currentTab = tabs.find(function(tab) {
         return tab.isVisible === true;
       });
       if (transactionTypeToButtonIdMap[transactionType]) {
         var isForTransactionType = function(button) {
           return transactionTypeToButtonIdMap[transactionType].some(function(buttonId) {
             return buttonId === button.id;
           });
         };
         var updateButtonSkin = function(button) {
           if (isForTransactionType(button)) {
             button.skin = skins.selected;
             button.hoverSkin = skins.hover;
           } else {
             button.skin = skins.unselected;
             button.hoverSkin = skins.hovered;
           }
         };
         var getButtonsFrom = function(tab) {
           var hasText = function(text) {
             return function(fullText) {
               return fullText.indexOf(text) > -1;
             };
           };
           var getValueFrom = function(object) {
             return function(key) {
               return object[key];
             };
           };
           return Object.keys(tab).filter(hasText('btn')).map(getValueFrom(tab));
         };
         getButtonsFrom(currentTab).forEach(updateButtonSkin);
       } else {
         kony.print(transactionType + ' not found in config.');
       }
     },
     
    updateTransactions: function (transactionDetails) {
    var controller = this;
    //for occu
    //this.accountSummaryDetailsNew();
    if(this.presenter.viewModel.transactionsViewModel.recentTransactions.length > 0){
   
      if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1== null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1 == undefined){
				this.view.accountSummary.flxInfoNew.isVisible = false;
				this.view.accountSummary.lblCurrentBalanceTitleNew.text = "";
				}else{
                this.view.accountSummary.lblCurrentBalanceTitleNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1;
				}
        if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1!=null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1 != undefined){
        this.view.accountSummary.lblCurrentBalanceValueNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1;
        }else{
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }
		
       if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2==null ||this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2 == undefined){
				this.view.accountSummary.flxInfoNew.isVisible = false;
				this.view.accountSummary.lblAvailableBalanceTitle.text = "";
				}else{
                this.view.accountSummary.lblAvailableBalanceTitle.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2;
				}
        if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2!=null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2 != undefined){
        this.view.accountSummary.lblAvailableBalanceValue.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2;
        }else{
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }   
    }
    
    this.view.transactions.flxPagination.setVisibility(true);
    this.adjustUIForTransactions(transactionDetails.pendingTransactions.length > 0 || transactionDetails.recentTransactions.length > 0);
    var createSegmentSection = function (sectionHeaderText, transactions) {
      return [{
          "lblTransactionHeader": sectionHeaderText,
          "lblSeparator": "."
        },
        transactions.map(controller.createTransactionSegmentModel),
      ];
    };
    var pendingTransactionsSection = createSegmentSection(kony.i18n.getLocalizedString('i18n.accounts.pending'), transactionDetails.pendingTransactions);
    var postedTransactionsSection = createSegmentSection(kony.i18n.getLocalizedString('i18n.accounts.posted'), transactionDetails.recentTransactions);
    var transactionsExistInSection = function (section) {
      return section[1] && section[1].length && section[1].length > 0;
    };
    this.view.transactions.segTransactions.widgetDataMap={
      			"btnCheckNumber":"btnCheckNumber",
                "flxStopCheck": "flxStopCheck",
                "btnStopCheck": "btnStopCheck",
                "btnRepeat":"btnRepeat",
                "lblNoteTitle":"lblNoteTitle",
                "lblNoteValue":"lblNoteValue",
                "lblFrequencyTitle":"lblFrequencyTitle",
                "lblFrequencyValue":"lblFrequencyValue",
                "lblRecurrenceTitle":"lblRecurrenceTitle",
                "lblRecurrenceValue":"lblRecurrenceValue",
                "btnDisputeTransaction": "btnDisputeTransaction",
                "btnEditRule": "btnEditRule",
                "btnPrint": "btnPrint",
                "cbxRememberCategory": "cbxRememberCategory",
      			"flxCheckContainer": "flxCheckContainer",
                "flxActions": "flxActions",
                "flxActionsWrapper": "flxActionsWrapper",
                "flxAmount": "flxAmount",
                "flxBalance": "flxBalance",
                "flxCategory": "flxCategory",
                "flxDate": "flxDate",
                "flxDescription": "flxDescription",
                "flxDetail": "flxDetail",
                "flxDetailData": "flxDetailData",
                "flxDetailHeader": "flxDetailHeader",
                "flxDropdown": "flxDropdown",
                "flxIdentifier": "flxIdentifier",
                "flxInformation": "flxInformation",
                "flxLeft": "flxLeft",
                "flxMemo": "flxMemo",
                "flxRight": "flxRight",
                "flxSegTransactionHeader": "flxSegTransactionHeader",
                "flxSegTransactionRowSavings": "flxSegTransactionRowSavings",
                "flxSegTransactionRowSelected": "flxSegTransactionRowSelected",
                "flxSegTransactionRowWrapper": "flxSegTransactionRowWrapper",
                "flxSelectedRowWrapper": "flxSelectedRowWrapper",
                "flxToData": "flxToData",
                "flxToHeader": "flxToHeader",
                "flxType": "flxType",
                "flxTypeData": "flxTypeData",
                "flxTypeHeader": "flxTypeHeader",
                "flxWithdrawalAmountData": "flxWithdrawalAmountData",
                "flxWithdrawalAmountHeader": "flxWithdrawalAmountHeader",
                "flxWrapper": "flxWrapper",
                "imgCategoryDropdown": "imgCategoryDropdown",
                "imgDropdown": "imgDropdown",
                "imgType": "imgType",
                "lblType": "lblType",
                "lblAmount": "lblAmount",
                "lblBalance": "lblBalance",
                "lblCategory": "lblCategory",
                "lblDate": "lblDate",
                "lblDescription": "lblDescription",
                "lblIdentifier": "lblIdentifier",
                "lblSeparator": "lblSeparator",
                "lblSeparatorActions": "lblSeparatorActions",
                "lblSeparatorDetailData": "lblSeparatorDetailData",
                "lblSeparatorDetailHeader": "lblSeparatorDetailHeader",
                "lblToTitle": "lblToTitle",
                "lblToValue": "lblToValue",
                "lblTransactionHeader": "lblTransactionHeader",
                "lblTypeTitle": "lblTypeTitle",
                "lblTypeValue": "lblTypeValue",
                "lblWithdrawalAmountTitle": "lblWithdrawalAmountTitle",
                "lblWithdrawalAmountValue": "lblWithdrawalAmountValue",
                "txtMemo": "txtMemo",

                // checks datamap
                "CopyflxToHeader0g61ceef5594d41": "CopyflxToHeader0g61ceef5594d41",
                "CopylblToTitle0a2c47b22996e4f": "CopylblToTitle0a2c47b22996e4f",
                "flxBankName1": "flxBankName1",
                "flxBankName2": "flxBankName2",
                "flxCash": "flxCash",
                "flxCheck1": "flxCheck1",
                "flxCheck1Ttitle": "flxCheck1Ttitle",
                "flxCheck2": "flxCheck2",
                "flxCheck2Ttitle": "flxCheck2Ttitle",
                "flxCheckImage": "flxCheckImage",
                "flxCheckImage2Icon": "flxCheckImage2Icon",
                "flxCheckImageIcon": "flxCheckImageIcon",
                "flxRememberCategory": "flxRememberCategory",
                "flxSegCheckImages": "flxSegCheckImages",
                "flxTotal": "flxTotal",
                "flxTotalValue": "flxTotalValue",
                "flxWithdrawalAmount": "flxWithdrawalAmount",
                "flxWithdrawalAmountCash": "flxWithdrawalAmountCash",
                "flxWithdrawalAmountCheck1": "flxWithdrawalAmountCheck1",
                "flxWithdrawalAmountCheck2": "flxWithdrawalAmountCheck2",
                "imgCheckimage": "imgCheckimage",
                "imgCheckImage1Icon": "imgCheckImage1Icon",
                "imgCheckImage2Icon": "imgCheckImage2Icon",
                "imgRememberCategory": "imgRememberCategory",
                "lblBankName1": "lblBankName1",
                "lblBankName2": "lblBankName2",
                "lblCheck1Ttitle": "lblCheck1Ttitle",
                "lblCheck2Ttitle": "lblCheck2Ttitle",
                "lblRememberCategory": "lblRememberCategory",
                "lblSeparator2": "lblSeparator2",
                "lblSeperatorhor1": "lblSeperatorhor1",
                "lblSeperatorhor2": "lblSeperatorhor2",
                "lblSeperatorhor3": "lblSeperatorhor3",
                "lblSeperatorhor4": "lblSeperatorhor4",
                "lblSeperatorhor5": "lblSeperatorhor5",
                "lblTotalValue": "lblTotalValue",
                "lblWithdrawalAmount": "lblWithdrawalAmount",
                "lblWithdrawalAmountCash": "lblWithdrawalAmountCash",
                "lblWithdrawalAmountCheck1": "lblWithdrawalAmountCheck1",
                "lblWithdrawalAmountCheck2": "lblWithdrawalAmountCheck2",
                "segCheckImages" : "segCheckImages",
                "txtFieldMemo": "txtFieldMemo"
            };
    this.view.transactions.segTransactions.setData([pendingTransactionsSection, postedTransactionsSection].filter(transactionsExistInSection));
    CommonUtilities.Sorting.updateSortFlex(this.transactionsSortMap, transactionDetails.config);
    this.view.forceLayout();
    },
     
    createTransactionSegmentModel: function(transaction) {
      var self = this;
      var transactionType = transaction.type;
      var isRepeatableTransaction = (transaction.accountType === OLBConstants.ACCOUNT_TYPE.SAVING || transaction.accountType === OLBConstants.ACCOUNT_TYPE.CHECKING); 
      var isFeesOrInterestTransaction = (transactionType === OLBConstants.TRANSACTION_TYPE.FEES || transactionType === OLBConstants.TRANSACTION_TYPE.INTERESTCREDIT || transactionType === OLBConstants.TRANSACTION_TYPE.INTERESTDEBIT); 
      function getMappings(context) {
        if (context == "btnDisputeTransaction") {
          if (isRepeatableTransaction && CommonUtilities.getConfiguration("editDisputeATransaction") === "true") {
            return {
              "text": self.getDataByType(transaction.accountType).transactionText,
              "isVisible": false // Made false as it's Not in scope R4
            };
          } else {
            return {
              "isVisible": false,
            };
          }
        }
        if (context == "btnPrint") {
          if (CommonUtilities.getConfiguration("printingTransactionDetails") === "true") {
            return {
              "text": kony.i18n.getLocalizedString('i18n.accounts.print'),
              "onClick": function() {
                self.onClickPrintRow();
              }
            };
          } else {
            return {
              "isVisible": false,
            };
          }
        }
        if (context == "nickName") {
          return self.getDataByType(transaction.accountType, transaction).ToValue;
        }
      }
      if (transaction.showTransactionIcons) {
        this.view.transactions.lblSortType.isVisible = true;
      } else {
        this.view.transactions.lblSortType.isVisible = false;
      }
      var accountType = transaction.accountType;

      function btnRepeatActions() {
        if (isRepeatableTransaction && !isFeesOrInterestTransaction && transaction.statusDescription == 'successful') {
          return {
            "text": kony.i18n.getLocalizedString('i18n.accounts.repeat'),
            "toolTip": kony.i18n.getLocalizedString('i18n.accounts.repeat'),
            "isVisible": true,
            "onClick": function() {
              kony.print("Repeat Pressed");
              self.onBtnRepeat(transaction);
            }
          };
        } else {
          return {
            "text": kony.i18n.getLocalizedString('i18n.accounts.repeat'),
            "isVisible": false,
            "onClick": function() {
              kony.print("Repeat Pressed");
            }
          };
        }
      }
      // if (transaction.type == "Checks" || transaction.type == "CheckWithdrawal" || transaction.type == "CheckDeposit") {
      if ((transaction.type == "Withdrawal" || transaction.type == "Deposit") && (transaction.transaction.transactionSource == "check")) {
        var dataMap = {
          //"lblRecurrenceTitle": "Recurrence",
          "lblIdentifier": "",
          /*"imgRememberCategory": {
                "src": "unchecked_box.png",
                "isVisible": "false"
            },
            "flxRememberCategory" : {"isVisible" : "false"},
            "lblRememberCategory": {
                "text": "Remember Category",
                "isVisible": "false"
            },*/
          "btnCheckNumber": {
            "isVisible": this.getDataByType(transaction.accountType).withdrawFlag,
            "text": transaction.transaction.checkNumber
          },
          "btnStopCheck": {
            "isVisible": this.getDataByType(transaction.accountType).withdrawFlag,
          },
          "flxCheck2" : {
            "isVisible" : transaction.frontImage2 ? true : false
          },
          "flxCash" : {
            isVisible : transaction.cashAmount ? true : false
          },            
          "btnPrint": getMappings("btnPrint"),
          "btnEditRule": {
            "isVisible": "false"
          },
          //"btnDisputeTransaction": {"isVisible": "false"},
          "btnRepeat": {
            "isVisible": "false"
          },
          "lblTypeTitle": kony.i18n.getLocalizedString('i18n.CheckImages.Bank'),
          "lblWithdrawalAmountTitle": kony.i18n.getLocalizedString('i18n.accounts.withdrawalAmount'),
          "lblWithdrawalAmountValue" : (transaction.type == "Withdrawal" || transaction.type == "withdraw") ? "-"+transaction.amount : transaction.amount,
          "CopylblToTitle0a2c47b22996e4f": kony.i18n.getLocalizedString('i18n.CheckImages.Cash'),
          //"imgCheckimage": {"text": "C"},
          "imgCheckImage1Icon": {
            "text": "V"
          },
          "imgCheckImage2Icon": {
            "text": "V"
          },
          "lblType": {
            "text": transaction.type,
            "isVisible": transaction.showTransactionIcons
          },
          "lblSeparator": "lblSeparator",
          "lblSeparator2": "lblSeparator2",
          "lblSeparatorActions": "lblSeparatorActions",
          "lblSeperatorhor1": "lblSeperatorhor1",
          "lblSeperatorhor2": "lblSeperatorhor2",
          "lblSeperatorhor3": "lblSeperatorhor3",
          "lblSeperatorhor4": "lblSeperatorhor4",
          "lblSeperatorhor5": "lblSeperatorhor5",
          "lblTotalValue": kony.i18n.getLocalizedString('i18n.CheckImages.Total'),
          "lblToTitle": kony.i18n.getLocalizedString('i18n.CheckImages.Checks/cash'),
          "lblCheck1Ttitle": transaction.checkNumber1,
          "lblCheck2Ttitle": transaction.checkNumber2,
          "flxCheckImageIcon": {
            "onClick": function() {
              self.showCheckImage(transaction.frontImage1, transaction.backImage1, transaction.date, transaction.withdrawlAmount1, transaction.memo)
            }
          },
          "flxCheckImage2Icon": {
            "onClick": function() {
              self.showCheckImage(transaction.frontImage2, transaction.backImage2, transaction.date, transaction.withdrawlAmount2, transaction.memo)
            }
          },
          "lblBankName1": transaction.bankName1,
          "lblBankName2": transaction.bankName2,
          "lblWithdrawalAmountCheck1": transaction.withdrawlAmount1,
          "lblWithdrawalAmountCheck2": transaction.withdrawlAmount2,
          "lblWithdrawalAmountCash": transaction.cashAmount,
          "lblWithdrawalAmount": (transaction.type == "Withdrawal" || transaction.type == "withdraw") ? "-"+transaction.amount : transaction.amount,
          "txtFieldMemo": {
            "placeholder": kony.i18n.getLocalizedString('i18n.CheckImages.MemoOptional'),              
            "isVisible": true,
            "text":transaction.memo
          },
          "imgDropdown": {
            "src": "arrow_down.png",
          },
          "flxDropdown": "flxDropdown",
          "lblDate": CommonUtilities.formatDate(transaction.date,'mm/dd/yyyy'),
          "lblTypeValue": transaction.type,
          "lblDescription": transaction.description,
          "lblAmount": (transaction.type == "Withdrawal" || transaction.type == "withdraw") ? "-"+transaction.amount : transaction.amount,
          "lblBalance": transaction.balance,
          "template": "flxSegTransactionRowSavings"
        };
      } else {      
        dataMap = {
          "lblNoteTitle": { "text": kony.i18n.getLocalizedString('i18n.accounts.Note'),
                           "isVisible": isFeesOrInterestTransaction ? false: true 
                          },
          "lblNoteValue": { 
            "text": transaction.notes,
            "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblFrequencyTitle": { "text": kony.i18n.getLocalizedString('i18n.accounts.frequency'),
                                "isVisible": isFeesOrInterestTransaction ? false: true 
                               },
          "lblFrequencyValue": { "text": transaction.frequencyType,
                                "isVisible": isFeesOrInterestTransaction ? false: true 
                               },
          "lblRecurrenceTitle": { "text": kony.i18n.getLocalizedString('i18n.accounts.recurrence'),
                                 "isVisible": isFeesOrInterestTransaction ? false: true 
                                },
          "lblDescription": transaction.description,
          "lblRecurrenceValue": { 
            "text": transaction.numberOfRecurrences,
            "isVisible": isFeesOrInterestTransaction ? false: true 
          },
          "lblIdentifier": "A",
          "imgDropdown": {
            "src": "arrow_down.png",
          },
          "flxDropdown": "flxDropdown",
          "lblDate": CommonUtilities.formatDate(transaction.date,'mm/dd/yyyy'),
          /* "imgType": {
             "isVisible": transaction.showTransactionIcons, //hidden as per requirement.
             "isVisible": false,
             "src": this.getTransactionIconFor(transaction.type),
             "toolTip":transaction.type
           }, */
          "lblType": {
            "text": transaction.type,
            "isVisible": transaction.showTransactionIcons
          },
          "flxRememberCategory": {
            "isVisible": this.getDataByType(transaction.accountType).rememberFlag
          },
          "imgRememberCategory": {
            "src": "unchecked_box.png",
            "isVisible": this.getDataByType(transaction.accountType).rememberFlag
          },
          "lblRememberCategory": {
            "text": kony.i18n.getLocalizedString('i18n.accounts.rememberCategory'),
            "isVisible": this.getDataByType(transaction.accountType).rememberFlag
          },
          "lblCategory": " ",
          "imgCategoryDropdown": " ",
          "lblAmount": (transaction.type == "Withdrawal" || transaction.type == "withdraw") ? "-"+transaction.amount : transaction.amount,
          "lblBalance": transaction.balance,
          "lblSeparator": "lblSeparator",
          "lblSeparator2": "lblSeparator2",
          "btnPrint": getMappings("btnPrint"),
          "btnEditRule": {
            "text": this.getDataByType(transaction.accountType).editText
          },
          "btnDisputeTransaction": getMappings("btnDisputeTransaction"),
          "btnRepeat": btnRepeatActions(),
          "lblfrmAccountNumber": transaction.fromAcc,
          "lblSeparatorActions": "lblSeparatorActions",
          "lblTypeTitle": this.getDataByType(transaction.accountType).typeText,
          /*"lblToTitle": { "text": this.getDataByType(transaction.accountType).ToText,
                         "isVisible": isFeesOrInterestTransaction ? false: true 
                        },*/
      
          "fromAccountName": transaction.fromAccountName,
          "lblWithdrawalAmountTitle": {
            "isVisible": this.getDataByType(transaction.accountType).withdrawFlag,
            "text": kony.i18n.getLocalizedString('i18n.accounts.withdrawalAmount'),
          },
          "lblSeparatorDetailHeader": "lblSeparatorDetailHeader",
          "lblTypeValue": transaction.type,
          /*"lblToValue": { "text": getMappings("nickName"),
                         "isVisible": isFeesOrInterestTransaction ? false: true 
                        },*/
          "lblWithdrawalAmountValue": {
            "isVisible": this.getDataByType(transaction.accountType).withdrawFlag,
            "text": (transaction.type == "Withdrawal" || transaction.type == "withdraw") ? "-"+transaction.amount : transaction.amount
          },
          "lblExternalAccountNumber": transaction.externalAccountNumber,
          "lblSeparatorDetailData": "lblSeparatorDetailData",
          "txtMemo": transaction.notes,
          "toAcc": transaction.toAcc,
          "externalAcc": transaction.externalAccountNumber,
          "template": "flxSegTransactionRowSavings"
        };
      }
      //dataMap.flxDropdown = {
      //    "onClick": this.onClickToggle
      //} // toggle rows is handling in segment controller
      return dataMap;
    },
    showCheckImage: function(imgCheck1, imgCheck2, postDate, amount, memo) {
    var self= this;
    this.view.CheckImage.lblPostDateValue.text = postDate;
    this.view.CheckImage.lblAmountValue.text = amount;
    this.view.CheckImage.lblMemoValue.text = memo;
    this.view.CheckImage.imgCheckImage.base64 = viewModel.checkDetails.imgCheck1;
    this.view.flxCheckImage.setVisibility(true);
    this.view.CheckImage.flxFlip.onClick = function () {
      if (self.view.CheckImage.imgCheckImage.base64 === viewModel.checkDetails.imgCheck1)
        self.view.CheckImage.imgCheckImage.base64 = viewModel.checkDetails.imgCheck2;
      else
        self.view.CheckImage.imgCheckImage.base64 = viewModel.checkDetails.imgCheck1;
    };
    this.view.forceLayout();
	},
	setFlowActions: function() {
     var scopeObj = this;
     this.view.btnViewDisputedTransactions.onClick = this.DisputedTransactionTabAction;
     this.view.btnViewDisputedChecks.onClick = this.DisputedChecksTabAction;
     this.view.MyRequestsTabs.btnAddNewStopCheckPayments.onClick = this.showStopCheckPaymentsSeriesMultiple;
     this.view.StopCheckPaymentSeriesMultiple.flxRadioBtn1.onClick = function() {
        if (scopeObj.view.oneTimeTransfer.imgRadioBtn1.src === "radiobtn_active_small.png") {
			scopeObj.view.oneTimeTransfer.imgRadioBtn2.src = "radiobtn_active_small.png";
			scopeObj.view.oneTimeTransfer.imgRadioBtn1.src = "icon_radiobtn.png";
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(false);
            scopeObj.view.forceLayout();
		} else {
			scopeObj.view.oneTimeTransfer.imgRadioBtn2.src = "icon_radiobtn.png";
			scopeObj.view.oneTimeTransfer.imgRadioBtn1.src = "radiobtn_active_small.png";
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(true);
            scopeObj.view.forceLayout();
		}
     };
     this.view.StopCheckPaymentSeriesMultiple.flxRadioBtn2.onClick = function() {
        if (scopeObj.view.oneTimeTransfer.imgRadioBtn2.src === "radiobtn_active_small.png") {
			scopeObj.view.oneTimeTransfer.imgRadioBtn1.src = "radiobtn_active_small.png";
			scopeObj.view.oneTimeTransfer.imgRadioBtn2.src = "icon_radiobtn.png";
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(true);
            scopeObj.view.forceLayout();
		} else {
			scopeObj.view.oneTimeTransfer.imgRadioBtn1.src = "icon_radiobtn.png";
			scopeObj.view.oneTimeTransfer.imgRadioBtn2.src = "radiobtn_active_small.png";
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSingleMultiplechecksWrapper1.setVisibility(false);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxSeriesOfChecksWrapper.setVisibility(true);
            scopeObj.view.StopCheckPaymentSeriesMultiple.flxAddAnotherCheck.setVisibility(false);
            scopeObj.view.forceLayout();
		}
     };
    this.view.StopCheckPaymentSeriesMultiple.flxTCContentsCheckbox.onClick = function()
    {
       var flag = scopeObj.toggleCheckBox(scopeObj.view.StopCheckPaymentSeriesMultiple.imgTCContentsCheckbox);
       if(flag === 0){
         scopeObj.disableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
       }
      else{
         scopeObj.enableButton(scopeObj.view.StopCheckPaymentSeriesMultiple.btnProceed);
      }
    };
    this.view.StopCheckPaymentSeriesMultiple.btnProceed.onClick = function() {
        var nav = new kony.mvc.Navigation("frmConfirm");
	    nav.navigate();
    };
    this.view.StopCheckPaymentSeriesMultiple.btnCancel.onClick = function() {
        // Goes To previous page
    };
    this.view.CancelStopCheckPayments.flxCross.onClick = function(){
       scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
    };
    this.view.CancelStopCheckPayments.btnNo.onClick = function() {
       scopeObj.view.flxLogoutStopCheckPayment.setVisibility(false);
    };
    
    //UI Related Code
    this.view.flxChart.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flexCategorizedMonthlySpending.setVisibility(true);
       scopeObj.setBreadCrumbData();
       scopeObj.view.CategorizedMonthlySpending.CommonHeader.btnRequest.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.flxDonutChart.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flexCategorizedMonthlySpending.setVisibility(true);
       scopeObj.setBreadCrumbData();
       scopeObj.view.CategorizedMonthlySpending.CommonHeader.btnRequest.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.CommonHeader.btnRequest.onClick = function() {   
       scopeObj.hideAll();
       scopeObj.view.flxUncategorizedTransactions.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.text = "BULK UPDATE";
       scopeObj.view.TransactionsUnCategorized.flxSort.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxPagination.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSeparatorSort.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxButtons.setVisibility(false);
       scopeObj.setBreadCrumbData();
       scopeObj.setTransactionData();
       scopeObj.view.forceLayout();
    };
    this.view.TransactionsUnCategorized.CommonHeader.btnRequest.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flxUncategorizedTransactions.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSort.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxPagination.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSeparatorSort.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.flxButtons.setVisibility(true);
       scopeObj.setBulkUpdateTransactionData();
       scopeObj.view.forceLayout();
    };
    this.view.TransactionsUnCategorized.btnCancel.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flxUncategorizedTransactions.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.text = "BULK UPDATE";
       scopeObj.view.TransactionsUnCategorized.CommonHeader.btnRequest.toolTip = "BULK UPDATE";
       scopeObj.view.TransactionsUnCategorized.flxSort.setVisibility(true);
       scopeObj.view.TransactionsUnCategorized.flxSortBulkUpdate.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxNoTransactions.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxPagination.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxSeparatorSort.setVisibility(false);
       scopeObj.view.TransactionsUnCategorized.flxButtons.setVisibility(false);
       scopeObj.setTransactionData();
       scopeObj.view.forceLayout();
    };
    this.view.TransactionsUnCategorized.btnDownload.onClick = function() {
       scopeObj.view.flxPFMAssignCategory.setVisibility(true);
       scopeObj.view.forceLayout();
    };
    this.view.AssignCategory.flxCross.onClick = function() {
       scopeObj.view.flxPFMAssignCategory.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.AssignCategory.btnCancel.onClick = function() {
       scopeObj.view.flxPFMAssignCategory.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.AssignCategory.btnDownload.onClick = function() {
       scopeObj.hideAll();
       scopeObj.view.flxPFMContainers.setVisibility(true);
       scopeObj.view.flxPFMAssignCategory.setVisibility(false);
       scopeObj.view.forceLayout();
    };
    this.view.TransactionsUnCategorized.flxCheckbox.onClick = function() {
       if(scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src === "checked_box.png") {
        scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "unchecked_box.png"; 
        var data = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.data;
        for(i=0;i<data.length;i++)
         {
           data[i].imgCheckBox = "unchecked_box.png";
         }  
      	kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.setData(data);   
       }
       else {
        var data1 = kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.data;
        scopeObj.view.TransactionsUnCategorized.flxCheckbox.imgCheckBox.src = "checked_box.png"; 
         for(i=0;i<data1.length;i++)
         {
           data1[i].imgCheckBox = "checked_box.png";
         }  
      	kony.application.getCurrentForm().TransactionsUnCategorized.segTransactions.setData(data1);   
       }
    };
    this.view.AllForms.flxCross.onClick = function() {
       scopeObj.view.AllForms.setVisibility(false);
    };
    this.view.accountSummary.flxInfo.onClick = function() {
      if(scopeObj.view.AllForms.isVisible === false){
        scopeObj.view.AllForms.left ="40.5%";
         scopeObj.view.AllForms.setVisibility(true);
      }
      else{
         scopeObj.view.AllForms.setVisibility(false);
      }
    };
          this.view.accountSummary.flxInfoNew.onClick = function() {
      if(scopeObj.view.AllForms.isVisible === false){
        scopeObj.view.AllForms.left ="10.5%";
         scopeObj.view.AllForms.setVisibility(true);
      }
      else{
         scopeObj.view.AllForms.setVisibility(false);
      }
    };
    },
    
    //code for statements
    initViewStatements: function(response) {
            this.updateProgressBarState(true);
            this.setYearsToListBox();
            this.updateStatmntTypeListBox();
            this.updateListBox(response);
            this.view.ViewStatements.confirmButtons.btnModify.onClick = this.backToAccounts;
            this.view.ViewStatements.confirmButtons.btnConfirm.onClick = this.backToAccountDetails;            
            this.view.forceLayout();
        },
        updateListBox: function(fromAccounts) {
            var scoobj = this;
            var accounts = [{}];
            for (var i = 0, j = 0; i < fromAccounts.length; i++)
                if (fromAccounts[i].isPrimaryOwner === "true") {
                    accounts[j] = fromAccounts[i];
                    j++;
                }
            this.view.ViewStatements.lstSelectAccount.masterData = this.showAccountsForSelection(accounts);           
            this.view.ViewStatements.lstSelectAccount.selectedKey = "Select";
            accountStSelKey = this.view.ViewStatements.lstSelectAccount.selectedKey;
            this.view.ViewStatements.lstSelectAccount.onSelection = this.onSelectionOfNewAccount;           
            this.view.forceLayout();
            var accountID = this.view.ViewStatements.lstSelectAccount.selectedKey;
            var year = this.view.ViewStatements.lblSelectYear.selectedKey;            
            //this.presenter.fetchStatementsList(this.getStatementListForSelectedType, accountID, year);
            this.view.ViewStatements.lblNoStatements.setVisibility(false);
            this.view.ViewStatements.SegStatementList.setVisibility(false);
			this.updateProgressBarState(false);
        },
        showAccountsForSelection: function(presentAccounts) {
          var list = [];
          var acclist = {};
          list.push(["Select","Select"]);
          for (i = 0; i < presentAccounts.length; i++) {
            var tempList = [];
            tempList.push(presentAccounts[i].accountID);
            var tempAccountNumber=presentAccounts[i].accountID;
            var name= CommonUtilities.getAccountDisplayName(presentAccounts[i]);
            if(acclist !== null && acclist[name] !== null && acclist[name] !== undefined) {
              kony.print("already exists");
            } else {
             // if(CommonUtilitiesOCCU.getAccountTypeKey(presentAccounts[i].accountType) === "S"){
                tempList.push(name);
                list.push(tempList);
                acclist[name] = "true";
              //}
            } 
          }
          return list;
        }, 
        setYearsToListBox: function() {
            var date = new Date();
            var yy = date.getFullYear();
            var list = [];
            for (var i = 0; i < 2; i++, yy--) {
                var tempList = [];
                tempList.push(yy);
                if(yy == date.getFullYear()){
                tempList.push(kony.i18n.getLocalizedString("i18n.accounts.statments.currentyear") +" ("+yy+")");
                } else {
                tempList.push(yy); 
                }
                list.push(tempList);
            }
            this.view.ViewStatements.lblSelectYear.masterData = list;
            var accountID = this.view.ViewStatements.lstSelectAccount.selectedKey;
            var year = this.view.ViewStatements.lblSelectYear.selectedKey;
            yearStSelKey = this.view.ViewStatements.lblSelectYear.selectedKey;
            this.view.ViewStatements.lblSelectYear.onSelection=this.onSelectionOfStYear;
            //this.presenter.fetchStatementsList(this.getStatementListForSelectedType, accountID, year);
        },
        onSelectionOfStYear: function(){
            var accountID = this.view.ViewStatements.lstSelectAccount.selectedKey;
            var year = this.view.ViewStatements.lblSelectYear.selectedKey;
            if(yearStSelKey !== null && yearStSelKey !== undefined && yearStSelKey === year) {
              kony.print("Same key selected");
            } else {
              this.presenter.fetchStatementsList(this.getStatementListForSelectedType, accountID, year);
            }
            yearStSelKey = year;
        },
        onSelectionOfNewAccount: function() {
            var date = new Date();
            var year = date.getFullYear();
            this.view.ViewStatements.lblSelectYear.selectedKey = year;
            yearStSelKey = year;
            //this.setMonthsData();
            var accountID = this.view.ViewStatements.lstSelectAccount.selectedKey;
            year = this.view.ViewStatements.lblSelectYear.selectedKey;
            var seldocType = this.view.ViewStatements.lstSelectType.selectedKey;
            if(accountID === "Select" || seldocType === "Select"){
			   this.view.ViewStatements.SegStatementList.setData([]);
			   this.view.ViewStatements.lblNoStatements.setVisibility(false);
               this.view.ViewStatements.SegStatementList.setVisibility(false);
			   this.updateProgressBarState(false);	
			} else if(accountStSelKey !== null && accountStSelKey !== undefined && accountStSelKey === accountID) {
              kony.print("Same key selected");
            } else {
              this.presenter.fetchStatementsList(this.getStatementListForSelectedType, accountID, year);
            }
            accountStSelKey = accountID;
        },
        updateStatmntTypeListBox: function() {
            kony.print("statements list, assign listbox");
            var statementTypes = {};
            var listBoxStatements = [];
            listBoxStatements.push(["Select",kony.i18n.getLocalizedString("i18n.accounts.statments.select")]);
            listBoxStatements.push(["statement",kony.i18n.getLocalizedString("i18n.accounts.statments.estatement")]);
            listBoxStatements.push(["Credit Card Statement",kony.i18n.getLocalizedString("i18n.accounts.statments.creditstatement")]);
            listBoxStatements.push(["tax",kony.i18n.getLocalizedString("i18n.accounts.statments.taxFormstatement")]);
            listBoxStatements.push(["eNotice",kony.i18n.getLocalizedString("i18n.accounts.statments.enoticestatement")]);
            /*if (Statements !== null && Statements !== undefined && Statements.length > 0) {
                for (var i = 0; i < Statements.length; i++) {
                    if (listBoxStatements !== null && listBoxStatements !== undefined && statementTypes[Statements[i].docType] !== null && statementTypes[Statements[i].docType] !== undefined) {
						
					}
					else {
                        statementTypes[Statements[i].docType] = "true";
                        listBoxStatements.push([Statements[i].docType, Statements[i].docType]);
                    }
                }
                this.view.ViewStatements.lstSelectType.masterData = listBoxStatements;
                this.view.ViewStatements.lstSelectType.selectedKey = Statements[0].docType;
            } else {
                this.view.ViewStatements.lstSelectType.masterData = listBoxStatements;
            }*/
            this.view.ViewStatements.lstSelectType.masterData = listBoxStatements;
            this.view.ViewStatements.lstSelectType.selectedKey = "Select";
            selStStamntType =  this.view.ViewStatements.lstSelectType.selectedKey;
            this.view.ViewStatements.lstSelectType.onSelection = this.onSelectionOfStType;
        },
        onSelectionOfStType: function(){
            var accountID = this.view.ViewStatements.lstSelectAccount.selectedKey;
            var year = this.view.ViewStatements.lblSelectYear.selectedKey;
            var seldocType = this.view.ViewStatements.lstSelectType.selectedKey;
            if(seldocType === "Select" || accountID === "Select"){
			   this.view.ViewStatements.SegStatementList.setData([]);
			   this.view.ViewStatements.lblNoStatements.setVisibility(false);
               this.view.ViewStatements.SegStatementList.setVisibility(false);
			   this.updateProgressBarState(false);	
			}
            else if(selStStamntType !== null && selStStamntType !== undefined && selStStamntType === seldocType){
              kony.print("same key selected");
            } else {
              this.presenter.fetchStatementsList(this.getStatementListForSelectedType, accountID, year);
            }
            selStStamntType = seldocType;
        },
        getStatementListForSelectedType: function(Statements) {
            var statementsWidgetDataMap = {
                "btnStatement1": "btnStatement1",
                "flxMonthStatements": "flxMonthStatements",
                "lblSeparator": "lblSeparator",
                "imgdownload":"imgdownload",
                "flxdownload":"flxdownload"
            };
            this.view.ViewStatements.SegStatementList.widgetDataMap = statementsWidgetDataMap;
            var accountID = this.view.ViewStatements.lstSelectAccount.selectedKey;
            var year = this.view.ViewStatements.lblSelectYear.selectedKey;
            var seldocType = this.view.ViewStatements.lstSelectType.selectedKey;
            var StatementsList = [];
            if(Statements !== null && Statements !== undefined && Statements.length >0){
              for (var i = 0; i < Statements.length; i++) {
                  if (seldocType === Statements[i].docType) {
                      var list = {
                          "btnStatement1": {
                              "text": Statements[i].StatementDescription + " dated " + Statements[i].docDate,
                              "onClick": this.onClickSegSatement.bind(this, Statements[i].docId,Statements[i].StatementDescription),
                          },
                          "imgdownload" : {"src": "icon_download.png"},
                          "flxdownload": {"onClick": this.onClickSegSatDownload.bind(this, Statements[i].docId,Statements[i].StatementDescription)},
                          "flxMonthStatements": "flxMonthStatements",
                          "lblSeparator": "lblSeparator"
                      };
                      StatementsList.push(list);
                  }
              }
                this.view.ViewStatements.SegStatementList.setData(StatementsList);
				if(StatementsList !== null && StatementsList !== undefined && StatementsList.length > 0){					 
					this.view.ViewStatements.lblNoStatements.setVisibility(false);
					this.view.ViewStatements.SegStatementList.setVisibility(true);
				} else {
					this.view.ViewStatements.lblNoStatements.setVisibility(true);
                    this.view.ViewStatements.SegStatementList.setVisibility(false);
				}
            } else {
             this.view.ViewStatements.SegStatementList.setData(StatementsList);
             this.view.ViewStatements.lblNoStatements.setVisibility(true);
             this.view.ViewStatements.SegStatementList.setVisibility(false);
            }
            this.view.forceLayout();
            this.updateProgressBarState(false);
        },
        onClickSegSatDownload: function(docId,docDesc) {
            this.presenter.fetchStatementsDoc(this.getStatementDocandDownload, docId,docDesc);
        },
        onClickSegSatement: function(docId,docDesc) {
            windowsOpenSt = window.open('', '');
            this.presenter.fetchStatementsDoc(this.getStatementDocandDisplay, docId,docDesc);
        },
        getStatementDocandDownload: function(statementdoc,docDesc) {
            this.updateProgressBarState(false);
            var scopeObj = this;
            kony.print("statement doc is " + statementdoc);
            var docbufferVal = statementdoc[0].buffer;           
			var pdf = 'data:application/octet-stream;base64,'+docbufferVal;
			var element = document.createElement('a');
			element.setAttribute('href', pdf);
			element.setAttribute('download', docDesc);
			element.style.display = 'none';
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);		
        },
        getStatementDocandDisplay: function(statementdoc,docDesc) {
            var scopeObj = this;
            kony.print("statement doc is " + statementdoc);
            var docbufferVal = statementdoc[0].buffer;			
			this.updateProgressBarState(false);
            /*var text1 = kony.i18n.getLocalizedString("i18n.topmenu.accounts");
            var text2 = kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
            var text3 = kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS");
            this.view.breadcrumb.setBreadcrumbData([{
                text: text1
            }, {
                text: text2,
                callback: scopeObj.backToAccountDetails
            }, {
                text: text3,
                callback: scopeObj.backToAccountDetails
            }]);
            this.view.flxMainWrapper.isVisible = true;
            this.view.flxMain.isVisible = true;
            this.view.flxViewStatements.isVisible = false;
            this.view.flxViewDocuments.isVisible = true;
            var brwser = this.view.ViewDocument.bwrstatement;
            if (brwser !== null) {
                brwser.htmlString = "<html>\n<body>\n<div id=\"pdf\" >\n<object data=\"data:application/pdf;base64," + docbufferVal + "\"" + "width=\"100%\" height=\"1000%\">\n</div>\n</body>\n</html>";
                brwser.reload();
            }
            this.view.forceLayout();*/
			var objbuilder = '';
				objbuilder += ('<object width="100%" height="100%" data="data:application/pdf;base64,');
				objbuilder += (docbufferVal);
				objbuilder += ('" type="application/pdf" class="internal">');
				objbuilder += ('<embed src="data:application/pdf;base64,');
				objbuilder += (docbufferVal);
				objbuilder += ('" type="application/pdf"  />');
				objbuilder += ('</object>');

				//var win = window.open("#","_blank");
				var title = docDesc;
				windowsOpenSt.document.write('<html><title>'+ title +'</title><body style="margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px;">');
				windowsOpenSt.document.write(objbuilder);
				windowsOpenSt.document.write('</body></html>');
                windowsOpenSt.document.close();
				//layer = jQuery(win.document);
        },
   showViewStatements: function(){
    var scopeObj = this;
    var text1=kony.i18n.getLocalizedString("i18n.topmenu.accounts");
    var text2=kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
    var text3=kony.i18n.getLocalizedString("i18n.ViewStatements.STATEMENTS");
    this.view.breadcrumb.setBreadcrumbData([{text:text1},{text:text3}]);	
    //this.format=this.view.downloadTransction.lbxSelectFormat.selectedKeyValue[1];
    this.view.ViewStatements.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.ViewStatements.GoToAccountSummary"); 
    this.view.ViewStatements.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.accounts.accountDetails");
    this.view.flxHeader.isVisible=true;
    this.view.flxMainWrapper.isVisible=true;
    this.view.flxMain.isVisible=true;
    this.view.flxEditRule.isVisible=false;
    this.view.flxCheckImage.isVisible=false;
    //this.view.flxLoading.isVisible=false;
    this.view.flxLogout.isVisible=true;
    this.view.flxAccountTypesAndInfo.isVisible=false;
    this.view.flxAccountSummaryAndActions.isVisible=false;
    //this.view.flxDisputedTransactionDetail.isVisible=false;
    this.view.flxTransactions.isVisible=false;
    this.view.flxFooter.isVisible=true;
    this.view.accountInfo.isVisible=false;
    this.view.accountTypes.isVisible=false;
    this.view.moreActions.isVisible=false;
    this.view.moreActionsDup.isVisible=false;
    this.view.breadcrumb.isVisible=true;
    this.view.flxskncontainer.isVisible=false;
    this.view.flxViewStatements.isVisible=true;
    this.view.flxViewDocuments.isVisible=false;
    this.view.flxDownloadTransaction.isVisible=false;
    this.view.downloadTransction.lblHeader.text=kony.i18n.getLocalizedString("i18n.accounts.viewStatments_Small");
    this.view.downloadTransction.lblPickDateRange.isVisible=true;
    this.view.downloadTransction.lblTo.isVisible=true;
    this.view.downloadTransction.flxFromDate.isVisible=true;
    this.view.downloadTransction.flxToDate.isVisible=true;
    this.view.ViewStatements.SegStatementList.setData([]);
    this.view.forceLayout();
    this.updateProgressBarState(true);
    this.presenter.fetchAccounts(this.initViewStatements);
   },
    backToAccountDetailsstat:function(){
      var selectedKey=this.view.ViewStatements.lstSelectAccount.selectedKey;
      this.backToAccountDetailsCallback(accountstat);
	  accountstat = [];
    },
    showAccountDetailsScreen: function() {
    var scopeObj = this;
    this.view.flxHeader.isVisible = true;
    this.view.flxMainWrapper.isVisible = true;
    this.view.flxMain.isVisible = true;
    this.view.flxEditRule.isVisible = false;
    this.view.flxCheckImage.isVisible = false;
    this.view.flxDownloadTransaction.isVisible = false;
    //this.view.flxLoading.isVisible = false;
    var text1=kony.i18n.getLocalizedString("i18n.topmenu.accounts");
    var text2=kony.i18n.getLocalizedString("i18n.transfers.accountDetails");
    this.view.breadcrumb.setBreadcrumbData([{text:text1}, {text:text2,  callback:scopeObj.backToAccountDetails }])
    this.view.flxLogout.isVisible = true;
    this.view.flxAccountTypesAndInfo.isVisible = true;
    this.view.flxAccountSummaryAndActions.isVisible = true;
    //this.view.flxDisputedTransactionDetail.isVisible = false;
    this.view.flxViewStatements.isVisible = false;
    this.view.flxViewDocuments.isVisible = false;
    this.view.flxTransactions.isVisible = true;
    this.view.flxFooter.isVisible = true;
    this.view.accountInfo.isVisible = false;
    this.view.accountTypes.isVisible = false;
    this.view.moreActions.isVisible = false;
    this.view.moreActionsDup.isVisible = false;
    this.view.breadcrumb.isVisible = true;
    this.view.flxskncontainer.isVisible = true;
    this.view.forceLayout();
  },
  hideAll: function() {
     this.view.flxAccountTypesAndInfo.setVisibility(false);
     this.view.flxAccountSummaryAndActions.setVisibility(false);
     this.view.flxDisputedTransactionDetail.setVisibility(false);
     this.view.flxMyRequestsTabs.setVisibility(false);
     this.view.flxViewStatements.setVisibility(false);
     this.view.flxViewDocuments.setVisibility(false);
     this.view.flxTransactions.setVisibility(false);
     this.view.flexCategorizedMonthlySpending.setVisibility(false);
     this.view.flxPFMContainers.setVisibility(false);
     this.view.flxUncategorizedTransactions.setVisibility(false);
     this.view.forceLayout();
  },
  setRightSideActions : function(viewModel){
    var scopeObj = this;
    var dataItem , j;
    viewModel = viewModel || [];
    // Right action buttonas
    scopeObj.rightActionButtons = [
      scopeObj.view.btnScheduledTransfer,
      scopeObj.view.btnMakeTransfer,
      scopeObj.view.btnPayABill
    ];
    // Right action saparators
    scopeObj.rightActionSeparator = [
      scopeObj.view.flxSeparatorPrimaryActions,
      scopeObj.view.flxSeparatorPrimaryActionsTwo
    ];
    scopeObj.resetRightSideActions(); //Reset Actions
    if(viewModel.length === 0) {
      scopeObj.view.flxPrimaryActions.setVisibility(false);
    }
    else {
      scopeObj.view.flxPrimaryActions.setVisibility(true);
      for (var i = 0; i < 2 && viewModel.length >= 1; i++) {
        dataItem = viewModel[i];
        scopeObj.rightActionButtons[i].text = dataItem.displayName.toUpperCase();
        scopeObj.rightActionButtons[i].setVisibility(true);
        scopeObj.rightActionButtons[i].onClick = dataItem.action;
        scopeObj.rightActionButtons[i].toolTip = dataItem.displayName;
        j = i-1;
        if(j>=0 && j <scopeObj.rightActionSeparator.length) {
          scopeObj.rightActionSeparator[j].setVisibility(true);
        }
      }
    }
  },
       userCanSearch: function (formData) {
    function checkIfEmpty (value) {
      if (value === null || value === "") {
        return true;
      }
      return false;
    }
    if (!checkIfEmpty(formData.keyword)) {
      return true;
    }
    //for occu
   // else if (formData.transactionTypeSelected !== OLBConstants.BOTH) {
    else if (formData.transactionTypeSelected !== null) {
      return true;
    }
    //else if (formData.timePeriodSelected !== OLBConstants.ANY_DATE) {
    else if (formData.timePeriodSelected !== null) {
      return true;
    }
    else if (!checkIfEmpty(formData.fromCheckNumber) && !checkIfEmpty(formData.toCheckNumber)) {
      return true;
    }
    else if (!checkIfEmpty(formData.fromAmount) && !checkIfEmpty(formData.toAmount)) {
      return true;
    }
    else {
      return false;
    }
  },
      accountSummaryDetailsNew: function(){
      
      if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1== null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1 == undefined){
				this.view.accountSummary.flxInfoNew.isVisible = false;
				this.view.accountSummary.lblCurrentBalanceTitleNew.text = "";
				}else{
                this.view.accountSummary.lblCurrentBalanceTitleNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1;
				}
        if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1!=null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1 == undefined){
        this.view.accountSummary.lblCurrentBalanceValueNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1;
        }else{
          this.view.accountSummary.lblCurrentBalanceValueNew.text = "";
        }
		
       if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2==null ||this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2 == undefined){
				this.view.accountSummary.flxInfoNew.isVisible = false;
				this.view.accountSummary.lblAvailableBalanceTitle.text = "";
				}else{
                this.view.accountSummary.lblAvailableBalanceTitle.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2;
				}
        if(this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2!=null || this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2 == undefined){
        this.view.accountSummary.lblAvailableBalanceValue.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2;
        }else{
          this.view.accountSummary.lblAvailableBalanceValue.text = "";
        }
      
      
    //this.view.accountSummary.lblCurrentBalanceTitleNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType1;
    //this.view.accountSummary.lblCurrentBalanceValueNew.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance1;
     // this.view.accountSummary.lblAvailableBalanceTitle.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balanceType2;
     // this.view.accountSummary.lblAvailableBalanceValue.text = this.presenter.viewModel.transactionsViewModel.recentTransactions[0].transaction.balance2;
    },

  updateAccountList: function (accountModel) {
    var data = this.createAccountListSegmentsModel(accountModel);
    this.view.accountTypes.segAccountTypes.setData(data);
    this.view.forceLayout();
  },
  createAccountListSegmentsModel: function (accounts) {
    var myAccountsMainSegData = [];
    var myAccountsrowData = [];
    var accountNumhdval = "";
    if(accounts !== null && accounts.length > 0)
    {
		for(var i=0; i<accounts.length; i++){
					var account = accounts[i];
                    var accountobj = { "lblUsers": {
                          "text" : account.accountshareID,
                          "toolTip" : account.accountshareID
                        },
                        "lblSeparator": "Separator",
                        "flxAccountTypes": {
                        "onClick": account.onAccountSelection
                        }
                    }
                   var tempaccountheading = account.accountOwner;
                    if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
                        var accountHeader = {
                            "lblHeader": accountNumhdval
                        };
                        myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
                        myAccountsrowData = [];
                        myAccountsrowData.push(accountobj);
                    } else {
                        myAccountsrowData.push(accountobj);
                    }
                    accountNumhdval =account.accountOwner;
                    if (i === accounts.length - 1) {
                        var accountfinHeader = {
                            "lblHeader": accountNumhdval
                        };
                        myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
                        kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
                    }
         }
    }
    
    return myAccountsMainSegData;
  },
   }
});