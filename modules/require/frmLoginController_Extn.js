define(['CommonUtilities', 'OLBConstants','CommonUtilitiesOCCU'], function (CommonUtilities, OLBConstants,CommonUtilitiesOCCU) {
   return{
     
     
     
    //// overiding post show function from frmLoginController/////
     
     
  onPostShow : function () {
    
    //this.setFlowActions();
    this.setmainflexheight();
    this.loginConstructor();
    this.onLoadChangePointer();
    this.view.letsverify.lbxMonth.onSelection=this.showDates;
    this.view.letsverify.lbxYear.onSelection=this.showDates;
    this.view.lblAppVersionValue.text=appConfig.appVersion; // newly added
    this.view.main.imgRememberMe.src='checked_box.png';
    this.view.CustomFeedbackPopup.btnYes.toolTip = "TAKE SURVEY";
    this.view.CustomFeedbackPopup.btnNo.toolTip = "MAYBE LATER";
    this.disableButton(this.view.main.btnLogin);
    this.disableButton(this.view.letsverify.btnProceed);
    this.disableButton(this.view.resetusingOTPEnterOTP.btnNext);
    this.disableButton(this.view.ResetOrEnroll.btnNext);
    this.disableButton(this.view.newpasswordsetting.btnNext);
    this.view.newpasswordsetting.tbxNewPassword.onBeginEditing = this.passwordEditing;
    this.view.newpasswordsetting.tbxNewPassword.onKeyUp = this.newPwdKeyUp;
    this.view.newpasswordsetting.tbxMatchPassword.onKeyUp = this.matchPwdKeyUp;
  },
  
      
      };
});