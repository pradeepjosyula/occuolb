define(function () {
   
    return {
        
        frmPayAPersonPreShow: function () {
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    /// start
    this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
    this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
    this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
    //// End
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
	this.view.customheader.forceCloseHamburger();

    this.view.tableView.Search.isVisible = false;
    this.view.p2pReport.isVisible = false;
    this.view.p2pActivity.isVisible = false;
    this.view.btnAddRecipient.tooltip=kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient");
    this.view.btnSendMoneyNewRecipient.tooltip=kony.i18n.getLocalizedString("i18n.PayAPerson.SendMoneyToNewRecipient");
   
    this.initActions();
    this.view.forceLayout();
  }
      
    };
});