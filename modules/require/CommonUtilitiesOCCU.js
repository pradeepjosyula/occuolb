define(['formatCurrency', 'formatDate' , 'ErrHandler','OLBConstants', 'DateUtils', 'CommonUtilities'], function (formatCurrency, formatDate, ErrHandler, OLBConstants, DateUtils, CommonUtilities) {

    
  /**
     * _getAccountTypeKey - Returns a short key for account type
     * @member of {CommonUtilitiesOCCU}
     * @param {object} accountType - AccountType object.
     * @returns {String} short key denoting the account type
     * @throws {}
     */
    var _getAccountTypeKey = function (accountType) {
      var _accountTypeWithKey = {
        "Savings": "S",
        "Loan": "L",
        "CreditCard": "CC",
        "Mortgage": "M",
        "Checking": "C"
      };
       return _accountTypeWithKey[accountType];
    };
  
  var applySkinThemeToApp = function(){
    function onsuccesscallback() 
     {
        // alert("successfully set the theme to app")
     }
    function onerrorcallback() 
     {
        // alert("Skin does not exist")
     }
    kony.theme.setCurrentTheme("OCCUTheme",onsuccesscallback,onerrorcallback);      
  }

    
    return {
        getAccountTypeKey: _getAccountTypeKey,
        formatCurrency: formatCurrency,
        formatDate: formatDate,
        ErrorHandler: ErrHandler,
        DateUtils: DateUtils,
        applySkinThemeToApp:applySkinThemeToApp
    };
});