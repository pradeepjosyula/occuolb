define(['CommonUtilities'],function (CommonUtilities) {
   
    return {
    initiateAddPayee:function(){
    this.hideCancelPopup();
    this.view.flxAddPayee.setVisibility(true);
    this.view.flxonetimepayment.setVisibility(false);
    this.view.breadcrumb.setBreadcrumbData([{text:"BILL PAY"}, {text:"ADD PAYEE"}]);
	this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.billPay.BillPay");
	this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.billPay.addPayee");
   
    this.view.lblAddPayee.text = "ADD PAYEE";    
    this.view.flxMainWrapper.isVisible=true;
    this.view.flxVerifyPayeeInformation.isVisible=false;
    this.view.flxAddBillerDetails.isVisible=false;
    this.view.flxAddPayeeAck.isVisible=false;  
    this.initializePayeeInformation();
    this.registerWidgetActions();
    this.showFlxSearchPayee();
    this.accountNumberAvailable = true;

    this.view.customheader.topmenu.flxMenu.skin="slFbox";
    this.view.customheader.topmenu.flxaccounts.skin = "sknFlx4f2683Occu";
    this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
    this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
    this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
    this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
    this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    //this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
    this.view.flxAdditionError.isVisible = false;
    this.setLogoutEvents();
    this.view.btnDetailsBack.onClick = this.showMainWrapper;
    this.view.btnModify.onClick = this.showMainWrapper;
  },
      
        checkIfAllSearchFieldsAreFilled:function(){
      if (this.view.flxonetimepayment.isVisible) {
          if (this.view.oneTimePay.tbxName.text && this.view.oneTimePay.txtZipCode.text && this.view.oneTimePay.txtAccountNumber.text && this.view.oneTimePay.txtAccountNumberAgain.text &&
            ((this.view.oneTimePay.txtmobilenumber.isVisible && this.view.oneTimePay.txtmobilenumber.text) || !this.view.oneTimePay.txtmobilenumber.isVisible)) {
            this.enableButton(this.view.oneTimePay.btnNext);
          } else {
            this.disableButton(this.view.oneTimePay.btnNext);
          }
    } else { // need to change the conditions
      if (this.view.tbxThree.isVisible === true) {
        if (this.view.tbxCustomerName.text && this.view.tbxZipcode.text && this.view.tbxOne.text && this.view.tbxTwo.text && this.view.tbxThree.text) {
          this.enableButton(this.view.btnNext2);
        }
        else {
          this.disableButton(this.view.btnNext2);
        }
      }
      else {
        if (this.view.tbxCustomerName.text && this.view.tbxOne.text && this.view.tbxTwo.text) {
          this.enableButton(this.view.btnNext2);
        }
        else {
          this.disableButton(this.view.btnNext2);
        }
      }
    }

  },
    validateSearchedPayeeDetails:function(){
    var name = this.view.tbxCustomerName.text;
    var zipcode = this.view.tbxZipcode.text;
    var identityNumber = this.view.tbxOne.text;
    var duplicateIdentityNumber = this.view.tbxTwo.text ;
    this.view.tbxOne.info = {"fieldHolds":"AccountNumber"};
    if(this.view.tbxOne.info === undefined){
      return 'NO_BILLER_SELECTED_FROM_LIST';
    }
    else{
      if(identityNumber === duplicateIdentityNumber){
        return 'VALIDATION_SUCCESS';
      }
      else{
        return 'IDENTITY_NUMBER_MISMATCH';
      }            
    }


  },
      
      
      
      
    };
});