define([], function () {
    var widgetsMap = [{
            id: "ACCOUNTS",
            menu: "flxAccountsMenu",
            subMenu: {
                parent: "flxAccountsSubMenu",
                children: [{
                    widget: "flxMyAccounts",
                    id: "MY ACCOUNTS"
                },{
                    widget: "flxStatements",
                    id: "Statements"
                }]

            },
            image: "imgCollapseAccounts"
        },
        {
            id: "TRANSFERS",
            menu: "flxTransfers",
            subMenu: {
                parent: "flxTransfersSubMenu",
                children: [{
                    widget: "flxTransfersMoney",
                    id: "Transfer Money"
                }, {
                    widget: "flxTransferHistory",
                    id: "Transfer history"
                }, {
                    widget: "flxExternalAccounts",
                    id: "External Accounts"
                }, {
                    widget: "flxAddKonyAccounts",
                    id: "Add Kony Accounts"
                }, {
                    widget: "flxAddNonKonyAccounts",
                    id: "Add Non Kony Accounts"
                }]
            },
            image: "imgCollapseTransfers"
        },
        {
            menu: "flxBillPay",
            id: "BILL PAY",
            subMenu: {
                parent: "flxBillPaySubMenu",
                children: [{
                    widget: "flxPayABill",
                    id: "Pay a Bill"
                }, {
                    widget: "flxMyAccounts",
                    id: "Bill Pay History"
                }, {
                    widget: "flxMyPayeeList",
                    id: "My Payee List"
                }, {
                    widget: "flxAddPayee",
                    id: "Add Payee"
                }]
            },
            image: "imgBillPayCollapse"
        },
        {
            menu: "flxAlertsAndMessages",
            id: "ALERTS & MESSAGES",
            subMenu: {
                parent: "flxAlertsAndMessagesSubMenu",
                children: [{
                    widget: "flxAlerts",
                    id: "Alerts"
                }, {
                    widget: "flxMessages",
                    id: "Messages"
                }, {
                    widget: "flxMessages",
                    id: "New Message"
                }]
            },
            image: "imgAlertsAndMessagesCollapse"
        },
        {
            menu: "flxPayAPerson",
            id: "PAY A PERSON",
            subMenu: {
                parent: "flxPayAPersonSubMenu",
                children: [{
                    widget: "flxSendRequest",
                    id: "Send/Request Money"
                }, {
                    widget: "flxMyRequests",
                    id: "My Requests"
                }, {
                    widget: "flxHistory",
                    id: "Payment History"
                }, {
                    widget: "flxMyRecipients",
                    id: "Recipients"
                }, {
                    widget: "flxAddRecipients",
                    id: "Add Recipient"
                }]
            },
            image: "imgPayAPersonCollapse"
        },
        {
            menu: "flxProfileManagement",
            id: "SETTINGS",
            subMenu: {
                parent: "flxProfileManagementSubMenu",
                children: [{
                    widget: "flxProfileSettings",
                    id: "Profile Settings"
                }, {
                    widget: "flxSecuritySettings",
                    id: "Security Settings"
                }, {
                    widget: "flxAccountSettings",
                    id: "Account Settings"
                }]
            },
            image: "imgProfileManagementCollapse"
        },
        {
            menu: "flxWireTransfer",
            id: "WIRE TRANSFER",
            subMenu: {
                parent: "flxWireTransferSubMenu",
                children: [{
                    widget: "flxWireTransferMakeTransfer",
                    id: "Make Transfer"
                }, {
                    widget: "flxWireTransferHistory",
                    id: "History"
                }, {
                    widget: "flxWireTransferMyRecipients",
                    id: "My Recipients"
                }, {
                    widget: "flxWireTransfersAddRecipient",
                    id: "Add Recipient"
                }]
            },
            image: "imgWireTransferCollapse"
        },
        {
            menu: "flxAboutUs",
            id: "ABOUT US",
            subMenu: {
                parent: "flxAboutUsSubMenu",
                children: [{
                    widget: "flxTerms",
                    id: "Terms and Conditions"
                }, {
                    widget: "flxPrivacyPolicy",
                    id: "Privacy Policy"
                }, {
                    widget: "flxContactUs",
                    id: "Contact Us"
                }, {
                    widget: "flxLocateUs",
                    id: "Locate Us"
                }, {
                    widget: "flxFaq",
                    id: "FAQs"
                }]
            },
            image: "imgAboutUsCollapse"
        },
{
            id: "PayAPerson",
            menu: "flxPayAPerson",
            subMenu: {
                parent: "flxPayAPersonSubMenu",
                children: [{
                    widget: "flxSendRequest",
                    id: "SendOrRequest"
                }, {
                    widget: "flxMyRequests",
                    id: "MyRequests"
                }, {
                    widget: "flxHistory",
                    id: "SentTransactions"
                }, {
                    widget: "flxMyRecipients",
                    id: "ManageRecipients"
                }, {
                    widget: "flxAddRecipients",
                    id: "AddRecipient"
                }]
            },
            image: "imgPayAPersonCollapse"
        }
    ];

    return widgetsMap;
})