define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities, OLBConstants) {
   return{
   
   createTransactionSegmentModel: function (transaction) {
    var scopeObj = this;
    return {
      btnAction: {
        text: kony.i18n.getLocalizedString("i18n.accounts.Modify"),
        onClick: function () {
          //kony.print('Not Implemented');
          scopeObj.onModifyTransaction(transaction.transaction);
        }
      },
      imgDropdown: {
        "src": OLBConstants.IMAGES.ARRAOW_DOWN,
      },
      lblAmount: transaction.amount,
      lblDate: transaction.date,
      lblDescription: transaction.description,
      lblSeparator2: " ",
      lblType: transaction.type,
      lblToTitle: kony.i18n.getLocalizedString("i18n.common.To"),
      lblNotesTitle: kony.i18n.getLocalizedString("i18n.accounts.Note"),
      lblToValue: transaction.nickName,
      lblNotesValue: transaction.notes,
      lblSeperator: " ",
      template: "flxScheduledTransactions"
    };
  },
  onModifyTransaction : function(data) {
    var scopeObj = this;
    scopeObj.presenter.ScheduledTransactions.onModifyTransaction(data);
  }
 
   }
});