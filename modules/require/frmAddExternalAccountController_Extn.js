define(['commonUtilities'],function(commonUtilities) {
   
    return {
        
      
          preshowFrmAddAccount: function() {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      if (kony.onlineBanking.configurations.getConfiguration("addExternalAccount") === "false") {
        this.view.externalAccount.imgchecked.src = "unchecked_box.png";
        this.view.externalAccount.imgcheckboxchecked.src = "unchecked_box.png";
        this.view.externalAccount.flxCheckboxKA.setVisibility(false);
        this.view.externalAccount.flxcheckboxinternational.setVisibility(false);
        this.view.externalAccount.flxIntSwiftCodeKA.top = "-40dp";
      }
      this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Add Kony Accounts");
      this.view.customheader.topmenu.flxMenu.skin="slFbox";
      this.view.customheader.topmenu.flxaccounts.skin="sknFlx4f2683Occu";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      this.view.customheader.topmenu.flxaccounts.skin="sknHoverTopmenu7f7f7pointer"; 
      this.view.forceLayout();
      this.view.customheader.headermenu.btnLogout.onClick = function() {
        kony.print("btn logout pressed");
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopup.btnYes.onClick = function() {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance()
          .getModuleManager()
          .getModule("AuthModule");
        context = { action: "Logout" };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function() {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function() {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
    },
      
      addDomesticAccount: function() {
      var scopeObj = this;
      var data = { routingNumber: this.view.externalAccount.tbxRoutingNumberKA.text, bankName: this.view.externalAccount.tbxBankNameKA.text, accountType: this.view.externalAccount.lbxAccountTypeKA.selectedKeyValue[1], accountNumber: this.view.externalAccount.tbxAccountNumberKA.text, reAccountNumber: this.view.externalAccount.tbxAccountNumberAgainKA.text, beneficiaryName: commonUtilities.changedataCase(this.view.externalAccount.tbxBeneficiaryNameKA.text), nickName: commonUtilities.changedataCase(this.view.externalAccount.tbxAccountNickNameKA.text), ownerImage: this.view.externalAccount.imgchecked.src };

      if ((data.accountNumber !== null || data.accountNumber !== "") && (data.routingNumber !== null || data.routingNumber !== "") && (data.reAccountNumber !== null || data.reAccountNumber !== ""))//data.nickName = data.beneficiaryName;
      if (!(data.accountNumber === data.reAccountNumber)) {
        this.view.externalAccount.tbxAccountNumberKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.externalAccount.tbxAccountNumberAgainKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.externalAccount.btnAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.setEnabled(false);
        errMsg = kony.i18n.getLocalizedString("i18n.transfers.accNoDoNotMatch");
        this.errorDomestic({ errorDomestic: errMsg });
      } else {
        this.view.externalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
        this.view.externalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.externalAccount.btnAddAccountKA.skin = "sknBtnNormalLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.setEnabled(true);
        this.view.externalAccount.lblWarning.setVisibility(false);
        this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
        this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
        this.presenter.addDomesticAccount(this, data);
      }
    },
      
      setDomesticAccounts: function(data) {
      var self = this;
      this.resetExternalAccount();
      this.view.breadcrumb.setBreadcrumbData([
        { text: kony.i18n.getLocalizedString("i18n.transfers.transfer") },
        {
          text: kony.i18n.getLocalizedString(
            "i18n.transfers.addNonKonyBankAccountDomestic"
          )
        }
      ]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountDomestic");
      this.view.externalAccount.flxDomesticDetailsKA.setVisibility(true);
      this.view.externalAccount.flxInternationalDetailsKA.setVisibility(false);
      this.view.externalAccount.btnInternationalAccountKA.skin = "sknBtnAccountSummaryUnselected";
      this.view.externalAccount.btnDomesticAccountKA.skin = "sknBtnAccountSummarySelected";
      this.view.previouslyAddedList.segkonyaccounts.widgetDataMap = { CopyLabel0a25bb187ff174b: "CopyLabel0a25bb187ff174b", CopyLabel0f686ea9ea96c4e: "CopyLabel0f686ea9ea96c4e", Label0j5951129f89142: "Label0j5951129f89142", btnviewdetails: "btnviewdetails", flxsegment: "flxsegment", flxsegmentseperator: "flxsegmentseperator", lblAccount: "lblAccount", lblbank: "lblbank", btnMakeTransfer: "btnMakeTransfer" };
      var segData = [];
      function getMapping(context) {
        if (context.btnMakeTransfer.isVerified == "true") {
          return { text: "MAKE TRANSFER", onClick: function() {
              self.onBtnMakeTransfer();
            } };
        } else {
          return { text: "PENDING", onClick: function() {}, skin: "sknlblLato5daf0b15px" };
        }
      }
      for (var i = 0; i < data.length; i++) {
        if (data[i] !== undefined && data[i].isInternationalAccount === "false" && data[i].isSameBankAccount === "false") {
          segData.push({
            btnviewdetails: {
              text: "View Details",
              onClick: function() {
                self.onBtnViewDetails();
              }
            },
            btnMakeTransfer: getMapping({ btnMakeTransfer: data[i] }),
            lblAccount: data[i].nickName,
            lblbank: data[i].bankName,
            CopyLabel0a25bb187ff174b: {
              text: data[i].accountNumber,
              isVisible: true
            },
            CopyLabel0f686ea9ea96c4e: {
              text: data[i].accountType,
              isVisible: true
            },
            Label0j5951129f89142: {
              text: "wwq",
              isVisible: true
            }
          });
        }
      }
      var len = segData.length;

      this.view.externalAccount.btnAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
      this.view.externalAccount.btnAddAccountKA.setEnabled(false);
      if (len !== 0) {
        this.view.previouslyAddedList.flxheader.setVisibility(true);
        this.view.previouslyAddedList.flxseperator.setVisibility(true);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
         //hiding visiblity of separator for last account
        segData[len - 1].Label0j5951129f89142.isVisible = false;
        segData[len - 1].CopyLabel0f686ea9ea96c4e.isVisible = false;
        segData[len - 1].CopyLabel0a25bb187ff174b.isVisible = false;
        
        this.view.previouslyAddedList.segkonyaccounts.setData(segData);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "20px";
      } else {
        this.view.previouslyAddedList.flxheader.setVisibility(true);
        this.view.previouslyAddedList.flxseperator.setVisibility(true);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "0px";
      }
      this.view.forceLayout();
    },
      
      
      
      
      
      validateDomesticFields: function() {
      var errMsg;
      var data = { checkBox: this.view.externalAccount.flxcheckboximg.src, routingNumber: this.view.externalAccount.tbxRoutingNumberKA.text, bankName: this.view.externalAccount.tbxBankNameKA.text, accountNumber: this.view.externalAccount.tbxAccountNumberKA.text, reAccountNumber: this.view.externalAccount.tbxAccountNumberAgainKA.text, beneficiaryName: this.view.externalAccount.tbxBeneficiaryNameKA.text, nickName: this.view.externalAccount.tbxAccountNickNameKA.text, ownerImage: this.view.externalAccount.imgchecked.src };

      if (data.accountNumber === null || data.routingNumber === null || data.accountNumber === "" || data.routingNumber === "") {
        this.view.externalAccount.btnAddAccountKA.skin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.setEnabled(false);
      } else {
        this.view.externalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
        this.view.externalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.externalAccount.btnAddAccountKA.skin = "sknBtnNormalLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
        this.view.externalAccount.btnAddAccountKA.setEnabled(true);
      }
    }
      
    };
});