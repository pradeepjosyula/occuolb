define(function () {

  return {


    willUpdateUI: function(viewModel){
      var self=this;
      if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
      if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
      if(viewModel.invalidCredential) this.errorWhileValidating();
      if(viewModel.verifyAccounts) this.showVerifyAccountScreen(viewModel.verifyAccounts);
      if (viewModel.validateByCredential) this.showAcknowledgment();
      if(viewModel[1].lblBankName) this.showVerifyAccountScreen(viewModel);
      for(var i=0;i<viewModel.length;i++){
        if (viewModel[i].sideMenu) this.updateHamburgerMenu(viewModel[i].sideMenu,viewModel);
        if (viewModel[i].topBar) this.updateTopBar(viewModel[i].topBar);        
        if(viewModel[i].internalAccount){    //To populate the infromation into the verifyAccount Form

          var Verifydata= viewModel[i].internalAccount;
          this.view.flxBankName.setVisibility(false);
          this.view.flxAccountType.setVisibility(false);
          this.view.lblAccountNumber .text = kony.i18n.getLocalizedString('i18n.AddInternal.lblAccountNumberAgainKA');
          this.view.lblBeneficiaryName.text = kony.i18n.getLocalizedString('i18n.AddInternal.lblBeneficiaryNameKA');
          this.view.lblAccountNickNameKey.text = kony.i18n.getLocalizedString('i18n.AddInternal.lblAccountNickNameKA');
          // this.view.lblBillerValue.text = Verifydata.bankName;
          //  this.view.lblAccountTypeValue.text = Verifydata.accountType;
          this.view.lblAccountNumberValue.text = Verifydata.reAccountNumber;
          this.view.lblBeneficiaryNameValue.text =Verifydata.beneficiaryName;
          this.view.lblAccountNickNameValue.text = Verifydata.nickName ;
          this.view.rtxBillerAddress.isVisible=false;
          this.view.lblBankAddress.isVisible=false;
          this.view.flxsemicolon.isVisible=false;

          this.internalAccountAcknowledgement(Verifydata.beneficiaryName);//Call to set an Acknowledgement 
          //this.view.acknowledgment.lblRefrenceNumberValue.text = Verifydata.referenceNo; //set the reference no which was retrieved from service
          if (Verifydata.successFlag === false || Verifydata.successFlag === "false") { //conditionally set the success/failure image.
            this.view.acknowledgmentVer.ImgAcknowledged.src = "close_red.png";
            this.view.acknowledgmentVer.lblRefrenceNumberValue.text = Verifydata.errMsg;
            this.view.acknowledgmentVer.lblRefrenceNumberValue.isVisible = true;
          }else{
            this.view.acknowledgmentVer.ImgAcknowledged.src = "success_green.png";
            this.view.acknowledgmentVer.lblRefrenceNumberValue.isVisible = true;
            this.view.acknowledgmentVer.lblRefrenceNumberValue.text = "Recipient has been added.";
          }
          this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountOCCU');
          this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addKonyBankAccountAcknowledge')}]); 
          this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
          this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountAcknowledge");

          this.view.forceLayout();

        }
        if(viewModel[i].domesticAccount){    //To populate the infromation into the verifyAccount Form
          var Verifydata= viewModel[i].domesticAccount;
          this.view.flxBankName.setVisibility(true);
          this.view.flxAccountType.setVisibility(true);
          this.view.lblAccountNumber.text = kony.i18n.getLocalizedString('i18n.transfers.accountNumber');
          this.view.lblBeneficiaryName.text = kony.i18n.getLocalizedString('i18n.accounts.routingNumber');
          this.view.lblAccountNickNameKey.text = kony.i18n.getLocalizedString('i18n.transfers.accountNickName');
          this.view.lblBillerValue.text = Verifydata.bankName;
          this.view.lblAccountTypeValue.text = Verifydata.accountType;
          this.view.lblAccountNumberValue.text = Verifydata.accountNumber;
         //this.view.lblBeneficiaryNameValue.text =Verifydata.beneficiaryName;
          this.view.lblBeneficiaryNameValue.text =Verifydata.routingNumber;
          this.view.lblAccountNickNameValue.text = Verifydata.nickName ;
          this.view.rtxBillerAddress.isVisible=false;
          this.view.lblBankAddress.isVisible=false;
          this.view.flxsemicolon.isVisible=false;
          this.view.btnConfirm.onClick = function () {
            self.checkVerificationOptions();
            //this.microDepositVerifyAccount();
          }
          this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount'); 
         // if(Verifydata.ownerImage == "unchecked_box.png"){
            this.internalAccountAcknowledgement(Verifydata.beneficiaryName);//Call to set an Acknowledgement 
           /* this.view.acknowledgment.lblRefrenceNumberValue.text =Verifydata.referenceNo; //set the reference no which was retrieved from service
            this.view.acknowledgment.lblRefrenceNumber.setVisibility(true);
            this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(true);*/
            if (Verifydata.success === false || Verifydata.success === "false") { //conditionally set the success/failure image.
            this.view.acknowledgmentVer.ImgAcknowledged.src = "close_red.png";
            this.view.acknowledgmentVer.lblRefrenceNumberValue.text = Verifydata.errMsg;
            this.view.acknowledgmentVer.lblRefrenceNumberValue.isVisible = true;
          }else{
            this.view.acknowledgmentVer.lblRefrenceNumberValue.isVisible = true;
            this.view.acknowledgmentVer.ImgAcknowledged.src = "success_green.png";
            this.view.acknowledgmentVer.lblRefrenceNumberValue.text = "Recipient has been added.";
            this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.nonKonyBankAcknowledge')}]); 
            this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
            this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.nonKonyBankAcknowledge");
            this.view.forceLayout();
          }
        /*  }
          else{
            this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
            this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
            this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");

            if(kony.application.getCurrentForm().id === "frmConfirmAccount")
              this.showVerificationOptions();

            this.view.forceLayout();
          }*/


        }
        if(viewModel[i].internationalAccount){    //To populate the infromation into the verifyAccount Form
          var Verifydata= viewModel[i].internationalAccount;

          this.view.lblBillerValue.text = Verifydata.bankName;
          this.view.lblAccountTypeValue.text = Verifydata.accountType;
          this.view.lblAccountNumberValue.text = Verifydata.accountNumber;
          this.view.lblBeneficiaryNameValue.text =Verifydata.beneficiaryName;
          this.view.lblAccountNickNameValue.text = Verifydata.nickName ;
          this.view.rtxBillerAddress.isVisible=false;
          this.view.lblBankAddress.isVisible=false;
          this.view.flxsemicolon.isVisible=false;
          this.view.btnConfirm.onClick = function () {
            self.checkVerificationOptions();
          }
          this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount');
          if(Verifydata.ownerImage == "unchecked_box.png"){
            this.internalAccountAcknowledgement(Verifydata.beneficiaryName);//Call to set an Acknowledgement 
            this.view.acknowledgment.lblRefrenceNumberValue.text =Verifydata.referenceNo; //set the reference no which was retrieved from service
            this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.nonKonyBankAcknowledge')}]); 
            this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
            this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.nonKonyBankAcknowledge");

            this.view.forceLayout();          
          }
          else{
            this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")},{text:kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccountVerify')}]); 
            this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
            this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountVerify");

            if(kony.application.getCurrentForm().id === "frmConfirmAccount")
              this.showVerificationOptions();
            this.view.forceLayout();            
          }

        }
      }
      kony.olb.utils.hideProgressBar(this.view);
      this.AdjustScreen();
      this.view.forceLayout();
    },
    
      makeNewTransfer: function(){
    if(this.view.btnMakeTransfer.text === kony.i18n.getLocalizedString("i18n.transfers.make_transfer"))
    	//this.presenter.showTransferScreen({accountTo:this.view.lblAccountNumberValue.text});
        this.presenter.showTransferScreen({accountType:this.view.lblAccountType.text });
    else
        this.presenter.showTransferScreen();
  },
    
    
    // function overided for showing verifying screen
    
    showVerifyAccountScreen: function(selectedRow) {
    this.hideAll();
    this.view.flxAcknowledgeActions.setVisibility(false);
    this.view.acknowledgmentVer.setVisibility(false);
    this.view.flxVerifyActions.setVisibility(true);
    var self = this;
    //Setting blocked skin for confirm button for first visit
    this.blockedSkin();
    if(this.view.verifyByCredential.CheckBoxGroup0cb331bee901341.selectedKeyValues){
      this.view.verifyByCredential.CheckBoxGroup0cb331bee901341.selectedKeyValues.length=0;
    }


    this.view.verifyByCredential.tbxUsername.onKeyUp = function() {
      self.validateDepositInput();
    };
    this.view.verifyByCredential.tbxPassword.onKeyUp = function() {
      self.validateDepositInput();
    };
    this.view.verifyByCredential.CheckBoxGroup0cb331bee901341.onSelection = function() {
      self.validateDepositInput();
    };
    this.view.verifyByCredential.setVisibility(true);
    this.view.lblBillerValue.text = selectedRow[1].lblAccountHolderValue;
    this.view.lblAccountTypeValue.text = selectedRow[1].lblAccountTypeValue;
    this.view.lblAccountNumberValue.text = selectedRow[1].lblBankName;
    this.view.lblBeneficiaryNameValue.text = selectedRow[1].lblSeparatorActions;
    this.view.lblAccountNickNameValue.text = selectedRow[1].lblAccountNumberTitleUpper;
    this.view.verifyByCredential.tbxPassword.secureTextEntry = false;
    this.view.verifyByCredential.lblUsername.text = kony.i18n.getLocalizedString("i18n.transfers.depositAmount1");
    this.view.verifyByCredential.lblPassword.text = kony.i18n.getLocalizedString("i18n.transfers.depositAmount2");
    this.view.verifyByCredential.tbxUsername.placeholder = kony.i18n.getLocalizedString("i18n.transfers.EnterAmountHere");
    this.view.verifyByCredential.tbxPassword.placeholder = kony.i18n.getLocalizedString("i18n.transfers.EnterAmountHere");
    this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.transfers.verifyAccountLC");
    this.view.verifyByCredential.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.verifyByProvidingDeposits");
    this.view.verifyByCredential.Label0f96e5b8496f040.text = selectedRow[1].lblAccountHolderValue;
    this.view.breadcrumb.setBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
    }, {
      text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts"),
      callback: function() {
        self.presenter.getExternalAccounts()
      }
    }, {
      text: kony.i18n.getLocalizedString('i18n.transfers.verifyAccount')
    }]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.transfers.transfer");
    this.view.breadcrumb.btnBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.external_accounts");
    this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.verifyAccount");
    this.view.verifyByCredential.tbxUsername.left = 0;
    this.view.verifyByCredential.tbxUsername.left = "40%";
    this.view.verifyByCredential.tbxPassword.left = 0;
    this.view.verifyByCredential.tbxPassword.left = "40%";
    this.view.lblHeadingVerify.text = kony.i18n.getLocalizedString("i18n.transfers.YourTransactionDetails");
    this.view.verifyByCredential.lblUsername.skin = "sknlbl727272LatoReg15px";
    this.view.verifyByCredential.lblPassword.skin = "sknlbl727272LatoReg15px";
    this.view.breadcrumb.btnBreadcrumb2.skin = "sknBtnLato3343A813PxBg0";
    this.view.btnConfirm.onClick = function() {
      kony.print("btnConfirm pressed from showVerifyAccountScreen");
      self.onBtnConfirmVerifyAndAdd({
        "accountNumber": selectedRow[1].lblBankName,
        "firstDeposit": self.view.verifyByCredential.tbxUsername.text,
        "secondDeposit": self.view.verifyByCredential.tbxPassword.text,
        "nickName": selectedRow.lblAccountName,
        "accountType": selectedRow.lblAccountTypeValue
      });
    };
  },
    


 /*   microDepositVerifyAccount: function () {
      var scopeObj = this;
      this.view.verifyByCredential.setVisibility(true);
      this.view.acknowledgmentVer.setVisibility(false);
     
    },*/
    preshowfrmVerifyAccount: function () {
      var scopeObj = this;

      this.view.verifyByCredential.flxWarning.setVisibility(false);
      this.view.verifyByCredential.tbxUsername.text="";
      this.view.verifyByCredential.tbxPassword.text="";
      this.view.verifyByCredential.CheckBoxGroup0cb331bee901341.selectedKeys = null;
      this.normalSkin();
      this.view.verifyBankAccount.rbtVerifyByDeposits.selectedKey = null;
      this.view.verifyBankAccount.rbtVerifyBankCredential.selectedKey = "rbg1";
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin="sknFlx4f2683Occu";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      /* this.view.forceLayout();   
    this.hideAll();
    this.view.verifyBankAccount.setVisibility(true);

    this.view.acknowledgment.lblRefrenceNumber.setVisibility(false);
    this.view.acknowledgment.lblRefrenceNumberValue.setVisibility(false);
    this.view.btnConfirm.onClick = this.nextStep.bind(this);
    this.setActions();
    this.view.acknowledgment.confirmHeaders.lblHeading.skin = "sknLato42424215Px";
    if (this.context.accountType == 'internal') {
      this.internalAccountAcknowledgement();
    }
    else {
      this.externalAccount(); 
    } */
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        kony.print("btn logout pressed");
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMainContainer.frame.height;
        scopeObj.view.flxLogout.height = height + "dp";
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.btnConfirm.skin="sknBtnNormalLatoFFFFFF15Px";
      this.view.btnConfirm.hoverSkin="sknBtnHoverLatoFFFFFF15Px";
      this.view.btnConfirm.focusSkin="sknBtnFocusLatoFFFFFF15Px";
      this.view.verifyByCredential.CheckBoxGroup0cb331bee901341.selectedKeyValues=null;
      this.view.btnConfirm.setEnabled(true);
      if(kony.application.getCurrentForm().id=="frmConfirmAccount")
        this.normalSkin();

    },

  };
});