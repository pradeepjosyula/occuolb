define(function () {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    //For OCCU menu options will come from account object
    fetchQuickActions = function (account, success, error) {
        kony.print("account is "+JSON.stringify(account));
        //modified for OCCU project
        var qckactions = [];
            if(account.opttransfer == "true") {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"));  
            }
			if(account.optpayBill == "true") {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.PayBill"));   
            }
			if(account.optdebitCard == "true") {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.DebitCard"));  
            }
			if(account.optviewDetails == "true") {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.ViewDetails")); 
            }
			if(account.favouriteStatus && account.favouriteStatus === '1') {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.UntagasFavorite"));
            } else{
			  qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.TagasFavorite"));
			}
			if(account.optdepositCheck == "true") {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"));  
            }
			if(account.optsendMoney == "true") {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.SendMoney"));   
            }
			if(account.optstopCheck == "true") {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.StopCheck"));  
            }
			if(account.optmakePayment == "true") {
              qckactions.push(kony.i18n.getLocalizedString("i18n.accounts.MakePayment"));  //payDueAmountDisplayName
            }
            success({
                account: account,
                actions: qckactions
            });
    };
  
   //For OCCU menu options will come from account object so all the possible action are defined here
   getQuickActionsViewModel = function (account, actions) {
       kony.print("in getQuickActionsViewModel");
        var scopeObj = this,
            accountType = account.accountType,
            validActions,
            finalActionsViewModel = [];        
        /**
         * Method to handle Cancel quick action.
         */
        var onCancel = function () {
            scopeObj.showAccountDetails(account);
        };

        if (accountType) {
            if (actions.length) {
                validActions = _getValidActions(actions, scopeObj.isValidAction, account);
                finalActionsViewModel = validActions.map(function (action) {  //get action object.
                    //modified for OCCU project
                    var quickActions = _getQuickActions({
                        onCancel: onCancel,
                        payABillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.PayBill"),
                        sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.accounts.SendMoney"),
                        makeATransferDisplayName: kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"),
                        viewStatementsDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewStatements"),
                        markasfavourite: kony.i18n.getLocalizedString("i18n.accounts.TagasFavorite"),
						unmarkasfavourite:kony.i18n.getLocalizedString("i18n.accounts.UntagasFavorite"),
                        debitcard: kony.i18n.getLocalizedString("i18n.accounts.DebitCard"),
                        viewdetails: kony.i18n.getLocalizedString("i18n.accounts.ViewDetails"),
                        depositcheck:  kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"),
                        stopcheck: kony.i18n.getLocalizedString("i18n.accounts.StopCheck"),
                        payDueAmountDisplayName: kony.i18n.getLocalizedString("i18n.accounts.MakePayment")
                    })
                    return scopeObj.getAction(quickActions, action, account);
                });
            }
        }
        return finalActionsViewModel;
    };
  
  //For OCCU we have toggle the Favorite and all accounts text headings.
   onShowFavouriteAccounts = function (accounts) {
        var scopeObj = this;
        scopeObj.viewModel.accountSummaryViewModel.toggleAccounts = function(){
            scopeObj.updateLoadingForCompletePage({
                isLoading: true,
                serviceViewModels: ['accountsSummary']
            });
            scopeObj.showAllAccounts();
        };
        //modified for OCCU project
        var favaccounts = accounts.filter(scopeObj.isFavourite);
		if(favaccounts !== null && favaccounts.length >0){
         scopeObj.viewModel.accountSummaryViewModel.showFavouriteText = true;
         scopeObj.viewModel.accountSummaryViewModel.toggleAccountsText = kony.i18n.getLocalizedString("i18n.accounts.showAllAccounts");
         scopeObj.viewModel.accountSummaryViewModel.toggleFavAccountsText =  kony.i18n.getLocalizedString("i18n.accounts.MyFavoriteAccounts");
         scopeObj.refreshData = scopeObj.showFavouriteAccounts.bind(scopeObj);
         scopeObj.showAccounts(accounts.filter(scopeObj.isFavourite));
		} else {
         scopeObj.viewModel.accountSummaryViewModel.showFavouriteText = false;
         scopeObj.viewModel.accountSummaryViewModel.toggleAccountsText = "";
         scopeObj.viewModel.accountSummaryViewModel.toggleFavAccountsText =  kony.i18n.getLocalizedString("i18n.accounts.MyFavoriteAccounts");
         scopeObj.refreshData = scopeObj.showFavouriteAccounts.bind(scopeObj);
         scopeObj.onShowAllAccounts(accounts);
		}
    };
  
    //For OCCU we have toggle the Favorite and all accounts text headings.
    onShowAllAccounts = function (accounts) {
        var scopeObj = this;
        scopeObj.viewModel.accountSummaryViewModel.toggleAccounts = function(){
            scopeObj.updateLoadingForCompletePage({
                isLoading : true,
                serviceViewModels : ['accountsSummary']
            });
            scopeObj.showFavouriteAccounts(accounts);
        };
        //modified for OCCU project
        scopeObj.viewModel.accountSummaryViewModel.toggleAccountsText = kony.i18n.getLocalizedString("i18n.accounts.showFavouriteAccounts");
        scopeObj.viewModel.accountSummaryViewModel.toggleFavAccountsText =  kony.i18n.getLocalizedString("i18n.accounts.AllAccounts");  
        scopeObj.refreshData = scopeObj.showAllAccounts.bind(scopeObj);
        scopeObj.showAccounts(accounts);
    };
  
    //in Viewmodel new fields are added for accounts object
    createAccountSummaryViewModal = function (account) {
        var scopeObj = this;
        return {
            accountName: account.accountName,
            nickName: account.nickName,
            accountNumber: account.accountID,
            type: account.accountType,
            availableBalance: scopeObj.formatCurrency(account.availableBalance),
            currentBalance: scopeObj.formatCurrency(account.currentBalance),
			outstandingBalance: scopeObj.formatCurrency(account.outstandingBalance),
            isFavourite: scopeObj.isFavourite(account),
           /*added for OCCU project */
            shareId:account.shareId,
            balanceType:account.balanceType,
            opttransfer:account.opttransfer,
            optpayBill:account.optpayBill,
            optdebitCard:account.optdebitCard,
            optviewDetails:account.optviewDetails,
            optisFavorite:account.optisFavorite,
            optdepositCheck:account.optdepositCheck,
            optsendMoney:account.optsendMoney,
            optstopCheck:account.optstopCheck,
            optmakePayment:account.optmakePayment,
            toggleFavourite: function () {
                scopeObj.updateLoadingForCompletePage({
                    isLoading : true,
                    serviceViewModels : ['accountsSummary']
                })
                scopeObj.changeAccountFavouriteStatus(account, scopeObj.refreshData.bind(scopeObj), scopeObj.onServerError.bind(scopeObj));
            },
            openQuickActions: function () {
                scopeObj.onQuickActionsMenu(account);
            },
            onAccountSelection: function () {
                return scopeObj.navigateToAccountDetails(account);
            }
        };
    };
  
    //new fields are added ina ccounts object, also secondary and primary actions will come from account object
    getAccountDetailsViewModel = function (account) {
        var scopeObj = this; 
        return {
            accountType: account.accountType,
            accountSummary: {
                availableBalance: scopeObj.formatCurrency(account.availableBalance),
                eStatementEnable: account.currentAmountDueeStatementEnable,
                currentBalance: scopeObj.formatCurrency(account.currentBalance),
                pendingDeposit: scopeObj.formatCurrency(account.pendingDeposit),
                pendingWithdrawals: scopeObj.formatCurrency(account.pendingWithdrawal),
                currentDueAmount: scopeObj.formatCurrency(account.currentAmountDue),
                creditLimit: scopeObj.formatCurrency(account.creditLimit),
                principalBalance: scopeObj.formatCurrency(account.principalBalance),
                principalAmount: scopeObj.formatCurrency(account.principalValue),
                payOffLoan: scopeObj.formatCurrency(account.principalBalance),
              	productBalance: scopeObj.formatCurrency(account.productBalance),
              	productBalanceType: account.productBalanceType,
              	balanceType: account.balanceType,
                maturityDate: CommonUtilities.formatDate(account.maturityDate),
                maturityOption: outputDataCheck(account.maturityOption),
                asOfDate: kony.i18n.getLocalizedString("i18n.accounts.AsOf") + CommonUtilities.formatDate(new Date().toISOString()),
                availableCredit: scopeObj.formatCurrency(account.availableCredit),
                interestEarned: scopeObj.formatCurrency(account.interestEarned),
                paymentTerm: outputDataCheck(account.paymentTerm)
            },
            balanceAndOtherDetails: {
                totalCredit: scopeObj.formatCurrency(account.totalCreditMonths),
                totalDebits: scopeObj.formatCurrency(account.totalDebitsMonth),
                bondInterestLastYear: displayInterest(account.bondInterestLastYear),
                dividendPaidLastYear: scopeObj.formatCurrency(account.dividendPaidLastYear),
                dividendRate: displayInterest(account.dividendRate),
                dividendPaidYTD: scopeObj.formatCurrency(account.dividendPaidYTD),
                lastDividendPaid: scopeObj.formatCurrency(account.lastDividendPaidAmount),
                paidOn: CommonUtilities.formatDate(account.lastPaymentDate),
                minimumDueAmount: scopeObj.formatCurrency(account.minimumDue),
                paymentDueDate: CommonUtilities.formatDate(account.dueDate),
                lastStatementBalance: scopeObj.formatCurrency(account.lastStatementBalance),
                lastPaymentDate: CommonUtilities.formatDate(account.lastPaymentDate),
                rewardsBalance: outputDataCheck(account.availableCredit),
                interestRate: displayInterest(account.interestRate),
                interestPaidYTd: scopeObj.formatCurrency(account.interestPaidYTD),
                interestPaidLastYear: scopeObj.formatCurrency(account.interestPaidLastYear),
                lastPaymentAmount: scopeObj.formatCurrency(account.lastPaymentAmount),
                originalAmount: scopeObj.formatCurrency(account.originalAmount),
                originalDate: CommonUtilities.formatDate(account.openingDate),
                payOffCharge: scopeObj.formatCurrency(account.payOffCharge),
                closingDate: CommonUtilities.formatDate(account.closingDate),
                loanClosingDate: account.closingDate,
                outstandingBalance: scopeObj.formatCurrency(account.outstandingBalance),
                dividendLastPaidAmount: scopeObj.formatCurrency(account.dividendLastPaidAmount),
                dividendLastPaidDate: CommonUtilities.formatDate(account.dividendLastPaidDate),
                previousYearsDividends: scopeObj.formatCurrency(account.previousYearsDividends),
                maturityAmount: scopeObj.formatCurrency(account.maturityAmount),
                regularPaymentAmount: scopeObj.formatCurrency(account.regularPaymentAmount)
            },
            accountInfo: {
                //modiufied for OCCU
                accountName: account.accountName,
                accountNumber: displayAccountNumber(account.accountID),
                accountID: account.accountID,
                accountType: outputDataCheck(account.accountType),
                routingNumber: outputDataCheck(account.routingNumber),
                swiftCode: outputDataCheck(account.swiftCode),
                primaryAccountHolder: outputDataCheck(account.accountHolder),
                jointHolder: outputDataCheck(account.jointHolders),
                creditIssueDate: CommonUtilities.formatDate(account.openingDate),
                creditcardNumber: outputDataCheck(account.creditCardNumber),
                accountHolder:account.accountHolder,
                shareId:account.shareId
            },
            rightSideActions: (function (account) {
                var onCancel = function () {
                    scopeObj.showAccountDetails(account);
                };
                //Right side action object array
                var RightSideActions = _getQuickActions({
                    onCancel: onCancel,
                    showScheduledTransactionsForm: scopeObj.showScheduledTransactionsForm.bind(scopeObj),
                    payABillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payABill"),
                    makeATransferDisplayName: kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"),
                    payDueAmountDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payDueAmount"),
                    payoffLoanDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan_Caps"),
                    viewStatementsDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewStatements"),
                    viewBillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewBill"),
                    markasfavourite: kony.i18n.getLocalizedString("i18n.accounts.TagasFavorite"),
     				//code added for OCCU PrimaryActions
                  	unmarkasfavourite: kony.i18n.getLocalizedString("i18n.accounts.UntagasFavorite"),
                    sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.accounts.SendMoney"),
                    debitcard: kony.i18n.getLocalizedString("i18n.accounts.DebitCard"),
                    viewdetails: kony.i18n.getLocalizedString("i18n.accounts.ViewDetails"),
                    depositcheck:  kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"),
                    stopcheck: kony.i18n.getLocalizedString("i18n.accounts.StopCheck")
                });
                var accountType = account.accountType,
                    validActions,
                    finalActions = [];
                if (accountType) {
                  var actions;                  
                  scopeObj.fetchQuickActions(account, function success(response){
                        actions = response.actions;
                  },CommonUtilities.ErrorHandler.onError);                  
                 if (actions.length) {
                    validActions = _getValidActions(actions, scopeObj.isValidAction, account);
                    finalActions = validActions.map(function (action) {  //get action object.
                      return scopeObj.getAction(RightSideActions, action, account);
                    });
                  }
                    
                }
                return finalActions;

            })(account),

            secondaryActions: (function (account) { //What else do you want dropdown
                var onCancel = function () {
                    scopeObj.showAccountDetails(account);
                };
                //Secondary action object array
                var SecondaryActions = _getQuickActions({
                    onCancel: onCancel,
                  //Code For SecondaryActions
                   	showScheduledTransactionsForm: scopeObj.showScheduledTransactionsForm.bind(scopeObj),
                    payABillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payABill"),
                    makeATransferDisplayName: kony.i18n.getLocalizedString("i18n.accounts.makeATransfer"),
                    payDueAmountDisplayName: kony.i18n.getLocalizedString("i18n.accounts.payDueAmount"),
                    payoffLoanDisplayName: kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.payoffLoan_Caps"),
                    viewStatementsDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewStatements"),
                    viewBillDisplayName: kony.i18n.getLocalizedString("i18n.accounts.viewBill"),
                    markasfavourite: kony.i18n.getLocalizedString("i18n.accounts.TagasFavorite"),
                    unmarkasfavourite: kony.i18n.getLocalizedString("i18n.accounts.UntagasFavorite"),
                    sendMoneyDisplayName: kony.i18n.getLocalizedString("i18n.accounts.SendMoney"),
                    debitcard: kony.i18n.getLocalizedString("i18n.accounts.DebitCard"),
                    viewdetails: kony.i18n.getLocalizedString("i18n.accounts.ViewDetails"),
                    depositcheck:  kony.i18n.getLocalizedString("i18n.accounts.DepositCheck"),
                    stopcheck: kony.i18n.getLocalizedString("i18n.accounts.StopCheck")
                });
                var accountType = account.accountType,
                    validActions,
                    finalActions = [];
                if (accountType) {
                  var actions;
                  scopeObj.fetchQuickActions(account, function success(response){
                        actions = response.actions;
                  },CommonUtilities.ErrorHandler.onError);
                  
                    if (actions.length) {
                        validActions = _getValidActions(actions, scopeObj.isValidAction, account);
                        finalActions = validActions.map(function (action) {  //get action object.
                            return scopeObj.getAction(SecondaryActions, action, account);
                        });
                    }
                }
                return finalActions;
            })(account)
        };
    };
  
});