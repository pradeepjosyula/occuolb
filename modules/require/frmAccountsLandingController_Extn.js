define(['CommonUtilities', 'OLBConstants'], function (CommonUtilities, OLBConstants) {
   return{
      shouldUpdateUI: function (viewModel) {
        return typeof viewModel !== 'undefined' && viewModel !== null;
      },

      willUpdateUI: function (viewModel) {
        if (viewModel.progressBar !== undefined){
          this.updateProgressBarState(viewModel.progressBar);
        } else if (viewModel.serverError)  {
          this.setServerError(viewModel.serverError);
        } else {
          if (viewModel.accountsSummary) this.updateAccountList(viewModel.accountsSummary);
          if (viewModel.welcomeBanner) this.updateProfileBanner(viewModel.welcomeBanner);
          if(viewModel.sideMenu){
            this.updateHamburgerMenu(viewModel.sideMenu); 
          }

          if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
          if (viewModel.accountQuickActions) this.updateAccountQuickActions(viewModel.accountQuickActions);
          if( viewModel.UpcomingTransactionsWidget) this.showUpcomingTransactionsWidget(viewModel.UpcomingTransactionsWidget);
          if(viewModel.messagesWidget) this.showMessagesWidget(viewModel.messagesWidget);
          if(viewModel.PFMDisabled) this.disablePFMWidget();  
          if(viewModel.unreadCount) this.updateAlertIcon(viewModel.unreadCount);
        }
        //var abc = CommonUtilities.getAccountDisplayName("312321312312321");
        this.AdjustScreen();
      },
     
         createAccountSegmentsModel: function (accounts) {
      var accountTypeConfig = {
        'Savings': {
          sideImage: 'accounts_sidebar_turquoise.png',
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        },
        'Checking': {
          sideImage: 'accounts_sidebar_purple.png',
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        },
        'CreditCard': {
          sideImage: 'accounts_sidebar_yellow.png',
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
        },
        'Deposit': {
          sideImage: 'accounts_sidebar_blue.png',
          balanceKey: 'currentBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.currentBalance'),
        },
        'Mortgage': {
          sideImage: 'accounts_sidebar_brown.png',
          balanceKey: 'outstandingBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.outstandingBalance'),
        },
        'Loan':{
          sideImage: 'accounts_sidebar_brown.png',
          balanceKey: 'outstandingBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.outstandingBalance'),
        },
        'Default': {
          sideImage: 'accounts_sidebar_turquoise.png',
          balanceKey: 'availableBalance',
          balanceTitle: kony.i18n.getLocalizedString('i18n.accounts.availableBalance'),
        }
      };
      //Modified account number mask for OCCU project
      var accountNumberMask = function (accountNumber) {
        var stringAccNum = '' + accountNumber;
        /*var isLast4Digits = function (index) {
          return index > (stringAccNum.length - 5);
        };
        return stringAccNum.split('').map(function (c, i) {
          return isLast4Digits(i) ? c : 'X';
        }).join('');*/
        var index = stringAccNum.length - 4;
        var accountNum = stringAccNum.substring(index,stringAccNum.length);
        accountNum = 'X'+accountNum;
        return accountNum;
      };
      var getConfigFor = function(accountType){
        if(accountTypeConfig[accountType]){
          return accountTypeConfig[accountType];
        }else{
          kony.print(accountType+' is not configured for display. Using Default configuration.');
          return accountTypeConfig.Default;
        }
      };
      kony.print("accounts is "+JSON.stringify(accounts));
      var accountTypes = [];
      accounts.map(function(account){
        return account.type;
      }).forEach(function(type){
        if(accountTypes.indexOf(type) === -1){
          accountTypes.push(type);
        }
      });
      var self = this;
      //modified for OCCU project
      var myAccountsMainSegData = [];
            var myAccountsrowData = [];
            var accountNumhdval = "";
            var accountNumhdvalMask = "";
            var count = 0;
      //for occu color code
      		
             if(accounts !== null && accounts.length > 0);{
			     for(var i=0; i<accounts.length; i++){
					var account = accounts[i];
                   var hexcolor = account.hexcolor;
      		hexcolor =hexcolor.substring(1,hexcolor.length);
               	var hexImage = "accounts_sidebar_blue.png";
            if(hexcolor == "004B98"){
                hexImage = "imghexone.png";
                }else if(hexcolor == "4F8B89"){
                hexImage = "imghextwo.png";
                }else if(hexcolor == "5C4F79"){
                hexImage = "imghexnine.png";
                }else if(hexcolor == "006AA7"){
                hexImage = "imghexthree.png";
                }else if(hexcolor == "7E57C6"){
                hexImage = "imghexfour.png";
                }else if(hexcolor == "8C829E"){
                hexImage = "imghexfive.png";
                }else if(hexcolor == "62A07B"){
                hexImage = "imghexsix.png";
                }else if(hexcolor == "87D400"){
                hexImage = "imghexseven.png";
                }else if(hexcolor == "53638D"){
                hexImage = "imghexeight.png";
                }else if(hexcolor == "CEDF00"){
                hexImage = "imghexten.png";
                }
                    var accountobj = {
                       // "imgIdentifier": getConfigFor(account.type).sideImage,
                      	"imgIdentifier": {
                            "src": hexImage,
                            "isVisible":true
                         },
                        "lblIdentifier": "A",
                        "imgFavourite": account.optisFavorite ? "filled_star.png" : "noimg.png",
                        "lblAccountName": account.accountName,
                        "lblAccountNumber": account.type +" "+account.shareId,
                        "lblAccountType": account.type,
                        "lblAvailableBalanceValue": account.availableBalance,
                        "lblAvailableBalanceTitle": account.balanceType, //getConfigFor(account.type).balanceTitle,
                        "imgMenu": "contextual_menu.png",
                        "toggleFavourite": account.toggleFavourite,
                        "onAccountClick": account.onAccountSelection,
                        "onQuickActions": account.openQuickActions,
                        "flxMenu": {
                          //modified for occu color code
                           // "skin": self.getSkinForAccount(account.type)
                          "skin":"skin"+hexcolor
                        },
                      	"imgThreeDotIcon":{
                            "text" : "O"
                        }
                    };
                    var accountNumber = account.accountNumber;
                    kony.print("accountNumber is " + accountNumber);
                    var tempaccountheading = account.nickName + " | " + accountNumber;
                    if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
                        var accountHeader = {
                            "lblHeader": accountNumhdvalMask
                        };
                        myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
                        myAccountsrowData = [];
                        myAccountsrowData.push(accountobj);
                    } else {
                        myAccountsrowData.push(accountobj);
                    }
                    accountNumhdval = account.nickName + " | " + accountNumber;
                    accountNumhdvalMask = account.nickName + " | " + accountNumberMask(accountNumber);
                    var fullAccountsLen = accounts.length;
                    if (count === fullAccountsLen - 1) {
                        var accountfinHeader = {
                            "lblHeader": accountNumhdvalMask
                        };
                        myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
                        kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
                    }
                    count++;
                }
            }
         return myAccountsMainSegData;
    },
      showUpcomingTransactionsWidget: function(transactions) {
        var self = this;
        this.view.upcomingTransactions.lblHeader.text = kony.i18n.getLocalizedString("i18n.Accounts.upComingTransactions");
        var formattedTransactions = [];
      	if (transactions !== undefined && transactions.length > 0){
        //show only 3 transactions
        transactions = transactions.slice(0,3);
        transactions.map(function(transaction) {
          //for OCCU
          var transactionType ="";
          var scheduledDate = "01/08/2012";
          if(transaction.scheduledDate != "--/--/----"){
            scheduledDate = transaction.scheduledDate;
          }else{
            scheduledDate = "";
            //self.view.upcomingTransactions.lblDate.setVisibility(false);
          }
          if(transaction.transactionType!=null){
                transactionType = transaction.transactionType;
              }else{
                transactionType = " ";
              }
            formattedTransactions.push({
                "lblDate": CommonUtilities.getFrontendDateString(scheduledDate, CommonUtilities.getConfiguration("frontendDateFormat")),
                "lblTo": kony.i18n.getLocalizedString("i18n.transfers.lblTo"),
                "lblToValue": transaction.toAccountName,
              	"lblFormToNavigate": transactionType,
                "lblAmount": self.presenter.formatCurrency(transaction.amount),
                "lblSeparator": "lblSeparator"
            });
        });
        }
        if (formattedTransactions.length == 0) {
            this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(false);
            this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(true);
            this.view.upcomingTransactions.flxNoTransactions.setVisibility(true);
            this.view.upcomingTransactions.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.Accounts.NoUpcomingTransactions");
        } else {
            this.view.upcomingTransactions.flxUpcomingTransactionWrapper.setVisibility(true);
            this.view.upcomingTransactions.flxNoTransactionWrapper.setVisibility(false);
            this.view.upcomingTransactions.segMessages.setData(formattedTransactions);
        }
        this.view.forceLayout();
    },
     
         updateAccountList: function (accountModel) {
      this.view.accountList.btnShowAllAccounts.onClick = accountModel.toggleAccounts;
      if(accountModel.toggleAccountsText)this.view.accountList.btnShowAllAccounts.text = accountModel.toggleAccountsText;
      //modified for OCCU project - toggle text for all accounts and favorite accounts
      if(accountModel.toggleFavAccountsText)this.view.accountList.lblAccountsHeader.text = accountModel.toggleFavAccountsText;    
      if (accountModel.showFavouriteText) {
        this.view.accountList.btnShowAllAccounts.setVisibility(true);
      } else {
        this.view.accountList.btnShowAllAccounts.setVisibility(false);
      }
      this.view.accountList.segAccounts.setData(this.createAccountSegmentsModel(accountModel.accounts));
      kony.print("final is  "+JSON.stringify(this.createAccountSegmentsModel(accountModel.accounts)));
      this.view.forceLayout();
    }
     
    }
});