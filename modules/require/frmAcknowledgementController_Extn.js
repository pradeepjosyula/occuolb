define(['commonUtilities','OLBConstants'],function(commonUtilities,OLBConstants){
   
    return {
        
    updateTransferAcknowledgeUI: function (viewModel) {
    this.customizeUIForTransferAcknowledege();
    if (this.presenter.MakeTransfer.isFutureDate(viewModel.transferData.sendOnDateComponents) || viewModel.transferData.selectedTransferToType=="OTHER_EXTERNAL_ACCOUNT" || (viewModel.transferData.frequencyKey !== "Now" && viewModel.transferData.accountTo !== kony.i18n.getLocalizedString('i18n.transfers.newrecipient'))) {
      this.showTransferAcknowledgeForScheduledTransaction(viewModel);
    }
    else{
      this.showTransferAcknowledgeForRecentTransaction(viewModel);
    }
      
    this.view.confirmDialogAccounts.keyValueBankName.lblValue.text =  viewModel.transferData.accFromKey;
     this.view.lblFromAckBal.text= "$"+viewModel.transferData.FromExistingBalance;
      //viewModel.transferData.getAccountName(viewModel.transferData.accountFrom) + '....'+ viewModel.transferData.getLastFourDigit(viewModel.transferData.accountFrom.accountID);
    this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text = viewModel.transferData.accToKey; //viewModel.transferData.accountTo.beneficiaryName ? viewModel.transferData.accountTo.nickName : (viewModel.transferData.getAccountName(viewModel.transferData.accountTo) + '....'+ viewModel.transferData.getLastFourDigit(viewModel.transferData.accountTo.accountID)) ;
    this.view.lblToAckBal.text="$"+viewModel.transferData.ToBalance;
      this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text = this.presenter.formatCurrency(viewModel.transferData.amount, true);
       if ((viewModel.transferData.ToProductType === "L"|| viewModel.transferData.ToProductType === "M") && viewModel.transferData.selectedDate == "Payment Date"){
				this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = viewModel.transferData.ToPaymentDate;
			}else{
				this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = viewModel.transferData.sendOnDate;
			}
    // this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text = viewModel.transferData.sendOnDate;
    this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text =  kony.i18n.getLocalizedString(viewModel.transferData.frequency);
    this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text = ""; //viewModel.transferData.notes;
    this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text = "";
    this.view.confirmDialogAccounts.keyValueAccountNickName.lblColon.text = "";
    this.view.btnMakeTransfer.onClick = function () {
      this.presenter.showTransferScreen();
    }.bind(this);
    this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString('i18n.transfers.make_transfer');
    this.view.btnAddAnotherAccount.setEnabled(true);
    if(viewModel.transferData.accountTo !== null && viewModel.transferData.accountTo !== undefined && viewModel.transferData.accountTo === kony.i18n.getLocalizedString('i18n.transfers.newrecipient')){
      this.view.btnAddAnotherAccount.onClick = function() {
       this.presenter.showSameRecBankAccounts(viewModel.transferData);
        kony.print("call new recipient save");
      }.bind(this);
      this.view.btnAddAnotherAccount.text = kony.i18n.getLocalizedString ('i18n.transfers.saverecipient');
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString('i18n.transfers.saverecipient');
    } else {
      this.view.btnAddAnotherAccount.onClick = function () {
        this.presenter.showTransferScreen({initialView: 'recent'});
      }.bind(this);
      this.view.btnAddAnotherAccount.setEnabled(false);
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString('i18n.transfers.addAnotherAccount');
    }
       if (viewModel.transferData.successFlag === false || viewModel.transferData.successFlag === "false"){//conditionally set the success/failure image.          
          this.view.acknowledgment.lblRefrenceNumberValue.text = ""; //viewModel.transferData.errorFlag;
          this.view.acknowledgmentModify.lblRefrenceNumberValue.text = ""; //viewModel.transferData.errorFlag;
          this.view.acknowledgment.lblRefrenceNumber.text = ""; //viewModel.transferData.errorFlag;
          this.view.acknowledgmentModify.lblRefrenceNumber.text = "";
          this.view.acknowledgment.ImgAcknowledged.src = "close_red.png";
          this.view.acknowledgmentModify.ImgAcknowledged.src = "close_red.png";
          //this.view.Balance.setVisibility(false);    
          this.view.acknowledgment.lblTransactionMessage.text = ""; //viewModel.transferData.errorFlag;
          this.view.acknowledgmentModify.lblTransactionMessage.text = "";
          this.view.acknowledgmentModify.lblRefrenceNumberValue.text = viewModel.transferData.errorFlag;
          this.view.acknowledgmentModify.lblTransactionDate.text = "";
          this.view.confirmDialogAccounts.lblRegDLimit.setVisibility(false);
       } else {
          this.view.acknowledgment.ImgAcknowledged.src = "success_green.png";
          this.view.acknowledgmentModify.ImgAcknowledged.src = "success_green.png";
          this.view.acknowledgment.lblRefrenceNumberValue.text ="";
          this.view.acknowledgmentModify.lblRefrenceNumberValue.text = "";
          this.view.acknowledgment.lblRefrenceNumber.text = "";
          this.view.acknowledgmentModify.lblRefrenceNumber.text = "";
          if(viewModel.transferData.regDCount !== null && viewModel.transferData.regDCount !== undefined && viewModel.transferData.regDCount !== "null")
            {
          this.view.confirmDialogAccounts.lblRegDLimit.text = "You have " + viewModel.transferData.regDCount+" Reg D limits left";
          this.view.confirmDialogAccounts.lblRegDLimit.setVisibility(true);
            } else {
           this.view.confirmDialogAccounts.lblRegDLimit.setVisibility(false);    
            }
       }
  },
   showTransferAcknowledgeForRecentTransaction (viewModel) {
    this.view.acknowledgment.setVisibility(true);  
    this.view.Balance.setVisibility(true);
    this.view.acknowledgmentModify.setVisibility(false);    
    //this.view.acknowledgment.lblRefrenceNumberValue.text = ""; //viewModel.transferData.referenceId;
    this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);    
    this.view.Balance.lblSavingsAccount.text = viewModel.transferData.accountFrom;
    this.view.Balance.lblBalanceValue.text =  viewModel.transferData.servAvaibal; //this.presenter.formatCurrency(viewModel.transferData.accountFrom.availableBalance); 
  },
  showTransferAcknowledgeForScheduledTransaction (viewModel) {
    function getDateFromDateComponents (dateComponents) {
      var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
      return   [dateComponents[0], monthNames[dateComponents[1] - 1], dateComponents[2]].join(" ");      
    }
    this.view.acknowledgmentModify.setVisibility(true);
    this.view.acknowledgment.setVisibility(false);     
    this.view.Balance.setVisibility(false);
    this.view.acknowledgmentModify.btnModifyTransfer.text = kony.i18n.getLocalizedString('i18n.transfer.MODIFYTRANSACTION');
    //viewModel.transferData.transactionType = "InternalTransfer";
    this.view.acknowledgmentModify.btnModifyTransfer.onClick = this.modifyTransfer.bind(this, viewModel.transferData);
    this.view.acknowledgmentModify.btnModifyTransfer.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');    
    //this.view.acknowledgmentModify.lblRefrenceNumberValue.text = ""; // viewModel.transferData.referenceId;
    if (viewModel.transferData.successFlag === true || viewModel.transferData.successFlag === "true"){
    this.view.acknowledgmentModify.lblTransactionMessage.text = kony.i18n.getLocalizedString("i18n.Transfers.YourTransactionHasBeenScheduledfor");
    this.view.acknowledgmentModify.lblTransactionDate.text = getDateFromDateComponents(viewModel.transferData.sendOnDateComponents);
    }
    if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.frequencyKey !== "Now" && (viewModel.indefinetly === true || viewModel.indefinetly === "true")) {
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblIndefinetly");
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = kony.i18n.getLocalizedString("i18n.transfers.lblIndefinetlyValue");
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);    
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.startDate");  
    }
    else if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.frequencyKey !== "Now") {
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblendingon");
      this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text = viewModel.transferData.endOnDate;
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);      
      this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.transfers.startDate");
    } else {
    this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);    
    this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text = kony.i18n.getLocalizedString("i18n.billPay.PaymentDate");
    }
  },
  modifyTransfer: function (transferData) {
    this.presenter.showMakeTransferForEditTransaction({
      transactionType: "InternalTransfer", //this.presenter.MakeTransfer.getTransferType(transferData.accountTo),
      toAccountNumber: transferData.accToKey,
      fromAccountNumber: transferData.accFromKey, 
      ExternalAccountNumber: transferData.accToKey,
      amount: transferData.amount,
      frequencyType: transferData.frequencyKey,
      noOfRecurrences: transferData.noOfRecurrences,
      howLongKey: transferData.howLongKey,
      notes: transferData.notes,
      scheduledDate: transferData.sendOnDate,
      frequencyEndDate: transferData.endOnDate,
      frequencyStartDate: transferData.sendOnDate,
      isScheduled: "1",
      transactionDate: transferData.sendOnDate,
      transactionId: transferData.referenceId,
      selectedTransferToType: transferData.selectedTransferToType,
      accToKey:transferData.accToKey,
      accFromKey:transferData.accFromKey,
      indefinetly: transferData.indefinetly
    });
  },
  initActions: function () {
    var scopeObj = this;
    this.view.customheader.topmenu.flxMenu.skin = "slFbox";
    this.view.customheader.topmenu.flxaccounts.skin="sknFlx4f2683Occu";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png"; 
    this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
    this.view.customheader.topmenu.flxSeperator3.setVisibility(false);
    this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
    // this.view.btnMakeAnotherPayment.onClick = function () {
    //   var navObj = new kony.mvc.Navigation("frmBillPay");
    //   navObj.navigate();
    // };
    this.view.customheader.headermenu.btnLogout.onClick = function () {
      kony.print("btn logout pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
      scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
      scopeObj.view.flxLogout.left = "0%";
    };
    this.view.CustomPopup.btnYes.onClick = function () {
      kony.print("btn yes pressed");
      var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      context = {
        "action": "Logout"
      };
      authModule.presentationController.doLogout(context);
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.btnNo.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    this.view.CustomPopup.flxCross.onClick = function () {
      kony.print("btn no pressed");
      scopeObj.view.flxLogout.left = "-100%";
    };
    // this.view.btnViewPaymentActivity.onClick = function () {
    //   var obj1 = {
    //     "tabname": "history"
    //   };
    //   var navObj = new kony.mvc.Navigation("frmBillPay");
    //   navObj.navigate(obj1);
    // };
   this.view.btnSavePayee.onClick = this.onClickSavePayee;
   if(commonUtilities.getConfiguration("printingTransactionDetails")==="true"){
    this.view.imgPrintBulk.setVisibility(true);
    this.view.CopyimgPrint0cb1a69de676e48.setVisibility(true);     
    this.view.imgPrintBulk.onTouchStart = this.onClickPrintBulk;
    this.view.CopyimgPrint0cb1a69de676e48.onTouchStart = this.onClickPrint;
   }else{
    this.view.imgPrintBulk.setVisibility(false);
    this.view.CopyimgPrint0cb1a69de676e48.setVisibility(false);
   }
  },
   updateBulkPayUIAcknowledge: function(viewModel) {
            var self = this;
			var strresponseBilldata = viewModel.responseBilldata;
			viewModel = viewModel.bulkPay;
            var totalSum = 0;
            this.view.breadcrumb.setBreadcrumbData([{
                text: "BILL PAY"
            }, {
                text: "ACKNOWLEDGEMENT"
            }]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.billPayAcknowledgement");
            this.view.flxContainer.isVisible = false;
            this.view.flxMainBulkPay.isVisible = true;
            bulkPayAckWidgetDataMap = {
                "lblRefernceNumber": "lblRefernceNumber",
                "lblPayee": "lblPayee",
                //"lblPayeeAddress": "lblPayeeAddress",
                "lblPaymentAccount": "lblPaymentAccount",
                "lblPaymentAccountType": "lblPaymentAccountType",
                //"lblEndingBalanceAccount": "lblEndingBalanceAccount",
                "lblSendOn": "lblSendOn",
                "lblDeliverBy": "lblDeliverBy",
                "lblAmount": "lblAmount",
                "imgAcknowledgement": "imgAcknowledgement"
            };
            viewModel = viewModel.map(function(dataItem) {
                var ackImage;
                var billpRefNumber = "";
                var deliverBy = "";
                var scheduledPaymentId = "";
				var payeeId = dataItem.payeeId;
				var responseBilldata = [];
				if(strresponseBilldata !== null && strresponseBilldata !== undefined && strresponseBilldata !== "" && strresponseBilldata !== "null"){
				responseBilldata = JSON.parse(strresponseBilldata);
                if (responseBilldata !== null && responseBilldata !== undefined) {
					for (var key in responseBilldata) {
					  if (responseBilldata.hasOwnProperty(key)) {
						var value = responseBilldata[key];
						var PayeeIDVal = value.payeeId+"";
						if(PayeeIDVal === payeeId){
							billpRefNumber = value["confirmationNumber"];
							deliverBy = value["deliveryDate"];
							if(deliverBy !== null && deliverBy !== ""  && deliverBy !== undefined){
								var deliveryArr = deliverBy.split("T");
								var deliveryDateArr = deliveryArr[0].split("-");
								deliverBy = deliveryDateArr[1]+"/"+deliveryDateArr[2]+"/"+deliveryDateArr[0];
							}
							scheduledPaymentId = value["scheduledPaymentId"];
							var lblamount = value["amount"]+"";
							var amount = lblamount.replace("$", "");
							amount = amount.replace(/,/g, "");
							totalSum = totalSum + parseFloat(amount);
						}
					};
                }
				}
				}
                if (billpRefNumber !== undefined && billpRefNumber !== null && billpRefNumber !== "" && billpRefNumber !== "null") {
                    ackImage = "bulk_billpay_success.png";                   
                } else {
                    billpRefNumber = "";
                    ackImage = "bulk_pay_unsuccessfull.png";
                }
                return {
                    "lblRefernceNumber": billpRefNumber,
                    "lblPayee": dataItem.lblPayee,
                    "lblPayeeAddress": "Address: " + dataItem.payeeAddressLine1,
                    "lblPaymentAccount": dataItem.lblPaymentAccount,
                    "lblPaymentAccountType": dataItem.lblPaymentAccountType,
                    //"lblEndingBalanceAccount": "Ending Balance: " + self.presenter.formatCurrency(dataItem.lblPaymentAccount),
                    "lblSendOn": self.presenter.getDateFromDateString(dataItem.lblSendOn),
                    "lblDeliverBy": deliverBy,
                    "lblAmount": self.presenter.formatCurrency(dataItem.lblAmount),
                    "imgAcknowledgement": ackImage,
                };
            });
            this.view.lblAmountValue.text = this.view.lblAmountValue.text = self.presenter.formatCurrency(totalSum);
            this.view.segBill.widgetDataMap = bulkPayAckWidgetDataMap;
            this.view.segBill.setData(viewModel);
            this.view.btnViewPaymentActivity.onClick = this.donothing; //this.onClickPaymentActivity;
            this.view.btnMakeAnotherPayment.onClick = this.onClickMakePayment;
            this.view.flxSuccessMessage.setVisibility(false);
            this.view.forceLayout();
        },
       willUpdateUI: function (viewModel) {
         if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
        if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
        if(viewModel.transferAcknowledge) {this.updateTransferAcknowledgeUI(viewModel.transferAcknowledge);}
        if(viewModel.ackPayABill)
          {
            this.updateBillPayAcknowledgeUI(viewModel.ackPayABill);
          }
          if (viewModel.bulkPay) {
            this.updateBulkPayUIAcknowledge(viewModel);
          }
        this.AdjustScreen();
       },
       donothing : function(){
         kony.print("functionality not included now");
       }
    };
});