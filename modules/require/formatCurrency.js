define([],function () {
    /**
     * @param {String} value numeric value of money from backend
     * @param {any} currencySymbol currency Symbol to use
     * @returns {String} money formated for UI
     */
    var formatCurrency = function (value, currencySymbol) {
        if (typeof value === 'string' && !isNaN(Number(value))) {
            if (Number(value) >= 0) {
                return currencySymbol + value;
            } else {
                return '- ' + currencySymbol + (-1 * Number(value));
            }
        } else {
            return null;
        }
    };

    return formatCurrency;
});