define(function () {
   
    return {
      
      preShowCustomerFeedback: function() {
      this.feedbackRating=0;
      var feedback=this.view.Feedback;
      this.view.flxFeedbackAcknowledgement.setVisibility(false);
      this.view.flxAcknowledgementContainer.setVisibility(false);
      this.view.flxFeedbackContainer.setVisibility(true);
      this.view.flxFeedback.setVisibility(true);
      feedback.imgRating1.src = "feedback_icon.png";
      feedback.imgRating2.src = "feedback_icon.png";
      feedback.imgRating3.src = "feedback_icon.png";
      feedback.imgRating4.src = "feedback_icon.png";
      feedback.imgRating5.src = "feedback_icon.png";
      /////// start
    //this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlx4f2683Occu";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxFeedback.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblFeedback.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgFeedback.src = "feedback_purple.png";
      ///// End
      feedback.flxAddFeatureRequest.setVisibility(true);
      feedback.flxUserFeedback.setVisibility(false);
      feedback.LblAddFeatureRequest.toolTip = "ADD FEATURE REQUEST";
      feedback.confirmButtons.btnModify.toolTip = "CANCEL";
      feedback.confirmButtons.btnConfirm.toolTip = "SUBMIT";	  
      feedback.txtareaUserComments.text="";
      feedback.txtareaUserAdditionalComments.text="";
      this.view.forceLayout();
      this.showActions();
    }
      
    };
});