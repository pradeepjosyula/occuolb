define([], function () {
    var toDateFormat = function (DateObj, format) {
        var dd = DateObj.getDate();
        var mm = DateObj.getMonth() + 1; //January is 0!
        var yyyy = DateObj.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        if(format === "dd/mm/yyyy") {
            return dd + '/' + mm + '/' + yyyy;
        }else if(format === "mm/dd/yyyy"){
          return mm + '/' + dd + '/' + yyyy;
        }
        else {
            return yyyy + '-' + mm + '-' + dd;
        }
     
    };

    var getDate = function (accountModelDate) {
        return new Date(accountModelDate);
    };
    /**
     * @param {String} value string value of date from backend
     * @param {String} dateformat excpected format Ex: dd/mm/yyyy
     * @returns {String} date formated for UI
     */
    var formatDate = function (value, format) {
        if (typeof value === 'string') {
            var dateObject = getDate(value);
            if ( !isNaN(dateObject.getTime())){
                return toDateFormat(dateObject, format);
            }else{
                return 'Invalid Date';
            }
        } else {
            return null;
        }
    };

     /**
     * @param {String} value string value of date from backend
     * @param {String} dateformat excpected format Ex: dd/mm/yyyy
     * @returns {String} date formated for Backend dd-mm-yyyy
     */

    var uiFormatToBackend = function (dateString) {
        var components = dateString.split('/');
        var dateObj = new Date();
        dateObj.setDate(components[0]);
        dateObj.setMonth(components[1] - 1);
        dateObj.setFullYear(components[2]);
        if ( !isNaN(dateObj.getTime())){
            return toDateFormat(dateObj, 'dd-mm-yyyy');
        }
        else {
            return 'Invalid Date';
        }
    }

    var getCurrentDateString = function () {
        var currDate = new Date();
        var month = currDate.getMonth() + 1;
        return (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/" + (month < 10 ? '0' + month : month) + "/" + currDate.getFullYear();
      } 

    return {
        toDateFormat: toDateFormat,
        formatDate: formatDate,
        uiFormatToBackend: uiFormatToBackend,
        getCurrentDateString: getCurrentDateString
    }
});