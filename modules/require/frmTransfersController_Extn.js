define(['CommonUtilities', 'OLBConstants','CommonUtilitiesOCCU'], function (CommonUtilities, OLBConstants,CommonUtilitiesOCCU) {
  var selectedtransferType = "";
  gblrecipientAccID = "";
  selectedData = {};
  return{
    setSelectedData : function(selectedRowData){
      selectedData = selectedRowData;
    },
    getSelectedData : function(){
      return selectedData;
    },
    updateMakeTransferForm: function(makeTransferViewModel,recipientAccID,recipientsAccType,selectedRowData) {
      var scopeObj = this;
      // if(selectedRowData !==undefined ){
      //   this.setSelectedData(selectedRowData);
      // }

      // // this.showMainTransferWindow();
      // var scopeObj = this;
      // // CommonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calSendOn);
      // // CommonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calStart);
      // // CommonUtilities.disableOldDaySelection(this.view.transfermain.maketransfer.calEndingOn);
      // if (makeTransferViewModel.showProgressBar) {
      //   this.initialLoadingDone = false;
      //   CommonUtilities.showProgressBar(this.view);
      // } else {
      //   this.initialLoadingDone = true;
      //   CommonUtilities.hideProgressBar(this.view);
      // }
      // if (gblrecipientAccID !== "") {
      //   recipientAccID = gblrecipientAccID;
      // }
      // if (makeTransferViewModel.selectedTransferToType == null ) {
      //   if(selectedData == undefined ||selectedData == "undefined" || selectedData == null || selectedData == {}){
      //     scopeObj.getSelectedData();
      //   }
      //   if(recipientsAccType!==null && recipientsAccType !== ""&& recipientsAccType!== undefined){
      //     var type;
      //     type=recipientsAccType;
      //     gblrecipientAccID = recipientAccID;
      //     makeTransferViewModel.transferToTypeListener(type);
      //   } else {
      //     this.configureTransferGateway(makeTransferViewModel);
      //     this.showTransfersGateway();
      //   }				
      // } else {
      //   if(selectedData == undefined ||selectedData == "undefined" || selectedData == null || selectedData == {}){
      //     scopeObj.getSelectedData();
      //   }

        this.showMakeTransfer();
        var fromaccounts = makeTransferViewModel.accountsFrom;
        this.view.flxMyPaymentsAccount.isVisible = true;
        // Change Transfer Type functions ---->
        selectedtransferType = makeTransferViewModel.selectedTransferToType;
        if (makeTransferViewModel.selectedTransferToType == "OWN_INTERNAL_ACCOUNTS") {
          this.view.mypaymentAccounts.lblOCCU.isVisible=true;
          this.view.mypaymentAccounts.lblCreditBank.isVisible=true;
          this.view.mypaymentAccounts.lblAccounts.isVisible=false;

        } else if (makeTransferViewModel.selectedTransferToType == "OTHER_INTERNAL_MEMBER") {
          this.view.mypaymentAccounts.lblOCCU.isVisible=false;
          this.view.mypaymentAccounts.lblCreditBank.isVisible=true;
          this.view.mypaymentAccounts.lblAccounts.isVisible=true;
        } else if(makeTransferViewModel.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT"){

          this.view.mypaymentAccounts.lblOCCU.isVisible=true;
          this.view.mypaymentAccounts.lblCreditBank.isVisible=false;
          this.view.mypaymentAccounts.lblAccounts.isVisible=true;
          //this.configureTransferGateway(makeTransferViewModel);
        }
        else{
          this.view.mypaymentAccounts.lblOCCU.isVisible=true;
          this.view.mypaymentAccounts.lblCreditBank.isVisible=true;
          this.view.mypaymentAccounts.lblAccounts.isVisible=true;

        }
        if (makeTransferViewModel.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT") {
          if(selectedData.check == true){
            this.displayExternalAccountsSegFromToAccont(fromaccounts, makeTransferViewModel, recipientAccID, selectedData.externalTransferOption);
            selectedData.check = false;
          }else{

            this.displayExternalAccountsSeg(fromaccounts, makeTransferViewModel,recipientAccID);
          }
          gblrecipientAccID = recipientAccID;
        } else {
          this.displayAccountsSegmentsModel(fromaccounts, makeTransferViewModel);
        }
        this.view.transfermain.maketransfer.accountslist.lblFromAccountName = makeTransferViewModel.accountFromKey;
        var toaccounts = makeTransferViewModel.accountsTo;
        //var toaccountsdata = this.displayAccountsSegmentsModel(toaccounts,"toaccounts");
        //this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.setData(toaccountsdata);
        this.view.transfermain.maketransfer.accountsTolist.lblFromAccountName = makeTransferViewModel.accountToKey;
        this.view.forceLayout();
        // this.view.transfermain.maketransfer.lbxFromAccount.masterData = makeTransferViewModel.accountsFrom;
      
        // this.filterAccountsTo(makeTransferViewModel);
        // this.filterFromAccount(makeTransferViewModel);
        this.view.transfermain.maketransfer.lbxFrequency.masterData = this.presenter.getFrequencies();

        //set all data for loan 
        this.view.transfermain.maketransfer.txtBxOtherAmount.text = makeTransferViewModel.otherAmount;
        this.view.transfermain.maketransfer.RbPaymentAmount.selectedKey = makeTransferViewModel.paymentAmountkey;
        this.onSelectionPaymentDue(makeTransferViewModel);
        this.view.transfermain.maketransfer.RbNowLoan.selectedKey = makeTransferViewModel.RbNowLoan;
        this.view.transfermain.maketransfer.RbOneTimeLoan.selectedKey = makeTransferViewModel.RbOneTimeLoanKey;
        if (makeTransferViewModel.ToProductType === "L" || makeTransferViewModel.ToProductType === "M") {
          this.onSelectionMotgagePayment(makeTransferViewModel);
        }
        this.view.transfermain.maketransfer.flexswitchOff.onClick = this.onClickIndefinetly.bind(this, makeTransferViewModel);
        this.view.transfermain.maketransfer.flexswitchOn.onClick = this.onClickIndefinetly.bind(this, makeTransferViewModel);
        // Work around for calendar issue                
        // Work around for calendar issue
        this.checkValidityMakeTransferForm();
        this.onFrequencyChanged();
        if(makeTransferViewModel.frequencyKey !== "Once" && makeTransferViewModel.frequencyKey !== "Now") {
          if(makeTransferViewModel.indefinetly === true || makeTransferViewModel.indefinetly === "true"){
            this.view.transfermain.maketransfer.flexswitchOff.left = "6.4%";
            this.view.transfermain.maketransfer.flexswitchOn.skin = "slFboxSwitchOn";
            this.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
            this.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
          } else {
            this.view.transfermain.maketransfer.flexswitchOff.left = "3.77%";
            this.view.transfermain.maketransfer.flexswitchOn.skin = "slFboxSwitchGreyOff";
            this.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(true);
            this.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(true);
          }
        }
        if (makeTransferViewModel.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT" && fromaccounts !== null && fromaccounts !== undefined && fromaccounts.length>1) {
          var processingDate = fromaccounts[0].nextProcessingDate;
          if (makeTransferViewModel.processingDate !== null && makeTransferViewModel.processingDate !== "" && makeTransferViewModel.processingDate !== undefined) {
            this.view.transfermain.maketransfer.calSendOn.date = makeTransferViewModel.processingDate;
            this.view.transfermain.maketransfer.calStart.date = makeTransferViewModel.processingDate;
          } else {
            this.view.transfermain.maketransfer.calSendOn.date = processingDate;
            this.view.transfermain.maketransfer.calStart.date = processingDate;
          }
        } else {
          this.view.transfermain.maketransfer.calSendOn.date = makeTransferViewModel.sendOnDate;
          this.view.transfermain.maketransfer.calStart.date = makeTransferViewModel.sendOnDate;
        }

        this.view.transfermain.maketransfer.calSendOn.dateComponents = this.view.transfermain.maketransfer.calSendOn.dateComponents;
        this.view.transfermain.maketransfer.calStart.dateComponents = this.view.transfermain.maketransfer.calStart.dateComponents;
        this.view.transfermain.maketransfer.btnConfirm.onClick = function() {
          var formData = scopeObj.getFormData(makeTransferViewModel);      
          // formData.accountFrom = 
          // formData.accountTo = this.getToAccount(makeTransferViewModel, formData.accountToKey);
         
              scopeObj.presenter.confirmTransfer(makeTransferViewModel,formData);        
          
        }.bind(this);
        this.view.transfermain.maketransfer.btnModify.onClick = function() {
          makeTransferViewModel.cancelTransaction(makeTransferViewModel);
        }
        this.view.transfermain.maketransfer.RbNowLoan.onSelection = this.onSelectionLoanPayment.bind(this, makeTransferViewModel);
        this.view.transfermain.maketransfer.RbOneTimeLoan.onSelection = this.onSelectionMotgagePayment.bind(this, makeTransferViewModel);
        this.view.transfermain.maketransfer.RbPaymentAmount.onSelection = this.onSelectionPaymentDue.bind(this, makeTransferViewModel);
        if (makeTransferViewModel.paymentAmountkey !== null && makeTransferViewModel.paymentAmountkey !== undefined && makeTransferViewModel.paymentAmountkey === "Other Amount") {
          scopeObj.view.transfermain.maketransfer.txtBxOtherAmount.setEnabled(true);
          makeTransferViewModel.otherPaymentAmount = false;
        } else {
          scopeObj.view.transfermain.maketransfer.txtBxOtherAmount.setEnabled(false);
          makeTransferViewModel.otherPaymentAmount = true;
        }
        this.view.transfermain.maketransfer.RbNow.onSelection = this.onSelectionCCPayment.bind(this, makeTransferViewModel);
        this.view.transfermain.maketransfer.txtOtherAmount.setEnabled(false);
        if (makeTransferViewModel.selectedTransferToType === "OTHER_EXTERNAL_ACCOUNT") {
          this.view.transfermain.maketransfer.accountslist.segAccountTypes.onRowClick = this.clickExternalFromAccountsSeg.bind(this, makeTransferViewModel);
          this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.onRowClick = this.clickExternalToAccountsSeg.bind(this, makeTransferViewModel);
        } else {
          this.view.transfermain.maketransfer.accountslist.segAccountTypes.onRowClick = this.clickFromAccountsSeg.bind(this, makeTransferViewModel);
          this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.onRowClick = this.clickToAccountsSeg.bind(this, makeTransferViewModel);
        }
        this.view.transfermain.maketransfer.lbxFromAccount.onSelection = this.filterAccountsTo.bind(this, makeTransferViewModel);
        this.view.transfermain.maketransfer.accountslist.FlexAccountname.onClick = this.onClickFromAccountName.bind(this, makeTransferViewModel);
        this.view.transfermain.maketransfer.accountsTolist.FlexAccountname.onClick = this.onClickToAccountName.bind(this, makeTransferViewModel);
        if (makeTransferViewModel.error) {
          this.view.transfermain.maketransfer.lblWarning.setVisibility(true);
          this.view.transfermain.maketransfer.lblWarning.text = kony.i18n.getLocalizedString(makeTransferViewModel.error);
        } else {
          this.view.transfermain.maketransfer.lblWarning.setVisibility(false);
        }
        if (makeTransferViewModel.serverError) {
          this.view.flxMakeTransferError.setVisibility(true);
          this.view.rtxMakeTransferError.text = makeTransferViewModel.serverError;
        } else {
          this.view.flxMakeTransferError.setVisibility(false);
        }
    },


    willUpdateUI: function (viewModel) {
      if (viewModel == undefined) { }
      else if (viewModel.serverError) {
        this.showServerError(viewModel.serverError);
      } else {
        if(viewModel.isLoading!==undefined)this.changeProgressBarState(viewModel.isLoading);
        if (viewModel.showRecentTransfers) this.resetFormForRecentTransfers();
        if (viewModel.sideMenu) this.updateHamburgerMenu(viewModel.sideMenu);
        if (viewModel.topBar) this.updateTopBar(viewModel.topBar);
        if (viewModel.accounts) this.updateTransferAccountList(viewModel);
        if (viewModel.gateway) this.updateGateWay(viewModel.gateway);
        if (viewModel.makeTransfer){
          this.updateMakeTransferForm(viewModel.makeTransfer);
          forRecipients=viewModel.makeTransfer;

        }
        if (viewModel.recentTransfers) this.showRecentsData(viewModel.recentTransfers, viewModel.config);
        if (viewModel.scheduledTransfers) this.showScheduledData(viewModel.scheduledTransfers, viewModel.config);
        if(viewModel.externalAccounts) this.showExternalAccounts(viewModel.externalAccounts);
        if(viewModel.viewSelectedExternalAccount) this.showSelectedExternalAccount(viewModel.viewSelectedExternalAccount);
        if(viewModel.viewExternalAccountTransactionActivity) this.showExternalAccountTransactionActivity(viewModel.viewExternalAccountTransactionActivity);
        if(viewModel.searchTransferPayees) {
          this.showSearchTransferPayees(viewModel.searchTransferPayees);
        }
        if (this.initialLoadingDone) {
          CommonUtilities.hideProgressBar(this.view);    
        }
      }
      this.AdjustScreen();
      this.view.forceLayout();

    },




    getLastFourDigit:function (numberStr) {
      if (numberStr.length < 4) return numberStr;
      return numberStr.slice(-4);
    },
    displayAccountsSegmentsModel: function(fromaccountsobj, makeTransferViewModel) {
      var scopeObj = this;
      var myAccountsMainSegData = [];
      var myAccountsrowData = [];
      var accountNumhdval = "";
      var mainaccountNumhdval = "";
      var accountfromselval = "";
      var selSecHeader = "";
      var selProuductID = "";
      var fromaccounts = fromaccountsobj.from;
      if (fromaccounts !== null && fromaccounts !== undefined && fromaccounts.length > 0) {
        for (var i = 0; i < fromaccounts.length; i++) {
          var account = fromaccounts[i];
          var subaccountobj = account.subAcct;
          if(subaccountobj !== null && subaccountobj !== undefined && subaccountobj !== "" && subaccountobj.length >0){
            for(var k=0; k< subaccountobj.length; k++)
            {
              if (subaccountobj !== null && subaccountobj !== undefined && subaccountobj !== "") {
                var subaccount = subaccountobj[k].fromDescription;
                var accountobj = {
                  "lblUsers": {
                    "text": subaccountobj[k].fromDescription + " ( " + subaccountobj[k].fromProductType + " " + subaccountobj[k].fromProductId + ")",
                    "toolTip": subaccountobj[k].fromDescription + " ( " + subaccountobj[k].fromProductType + " " + subaccountobj[k].fromProductId + ")" //accountshareID
                  },
                  "lblBalance":{
                    "text":CommonUtilities.formatCurrencyWithCommas(subaccountobj[k].fromBalance),
                    "toolTip":CommonUtilities.formatCurrencyWithCommas(subaccountobj[k].fromBalance)     //// avail balance
                  },
                  "lblSeparator": "Separator",
                  "flxAccountTypesTransfers": {
                    "onClick": account.onAccountSelection
                  },
                  "FromaccountID": account.fromAccountNumber,
                  "FromProductID": subaccountobj[k].fromProductId,
                  "FromProductType": subaccountobj[k].fromProductType,
                  "FromExistingBalance": subaccountobj[k].fromBalance,
                  //"accountdisplay": subaccountobj[k].fromDescription + " | " + subaccountobj[k].fromProductType + " " + subaccountobj[k].fromProductId + "  |  " + account.fromOwner + " | " + ' X' + this.getLastFourDigit(account.fromAccountNumber)+ "  | Avl Bal: "+CommonUtilities.formatCurrencyWithCommas(subaccountobj[k].fromBalance),
                  "accountdisplay": subaccountobj[k].fromDescription + " ( " + subaccountobj[k].fromProductType + " " + subaccountobj[k].fromProductId + ")" 
                }
                var tempaccountheading = account.fromOwner + " | " + account.fromAccountNumber;
                if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
                  var accountHeader = {
                    "lblHeader": mainaccountNumhdval
                  };
                  myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
                  myAccountsrowData = [];
                  myAccountsrowData.push(accountobj);
                } else {
                  myAccountsrowData.push(accountobj);
                }
                accountNumhdval = account.fromOwner + " | " + account.fromAccountNumber;
                mainaccountNumhdval = account.fromOwner + " | " + ' X' + this.getLastFourDigit(account.fromAccountNumber);
                if (i === 0 && k === 0 && makeTransferViewModel.accFromKey === "") {
                  accountfromselval = subaccountobj[k].fromDescription + " ( " + subaccountobj[k].fromProductType + " " + subaccountobj[k].fromProductId + ")";
                  selSecHeader = mainaccountNumhdval;
                  selProuductID = subaccountobj[k].fromProductId;
                  makeTransferViewModel.accFromKey = accountfromselval;
                  makeTransferViewModel.FromaccountID = account.fromAccountNumber;
                  makeTransferViewModel.FromProductID = subaccountobj[k].fromProductId;
                  makeTransferViewModel.FromProductType = subaccountobj[k].fromProductType;
                  makeTransferViewModel.FromExistingBalance = subaccountobj[k].fromBalance;
                  makeTransferViewModel.secHeader = mainaccountNumhdval;
                }
                if (makeTransferViewModel.accountFromKey !== null && makeTransferViewModel.accountFromKey !== undefined && makeTransferViewModel.accountFromKey === account.fromAccountNumber) {
                  accountfromselval = subaccountobj[k].fromDescription + " ( " + subaccountobj[k].fromProductType + " " + subaccountobj[k].fromProductId + ")";
                  selSecHeader = mainaccountNumhdval;
                  selProuductID = subaccountobj[k].fromProductId;
                  makeTransferViewModel.accFromKey = accountfromselval;
                  makeTransferViewModel.FromaccountID = account.fromAccountNumber;
                  makeTransferViewModel.FromProductID = subaccountobj[k].fromProductId;
                  makeTransferViewModel.FromProductType = subaccountobj[k].fromProductType;
                  makeTransferViewModel.FromExistingBalance = subaccountobj[k].fromBalance;
                  makeTransferViewModel.secHeader = mainaccountNumhdval;
                }
              }
            }
          }
          if (i === fromaccounts.length - 1) {
            var accountfinHeader = {
              "lblHeader": mainaccountNumhdval
            };
            myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
            kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
          }
        }
      }
      this.view.transfermain.maketransfer.accountslist.segAccountTypes.setData(myAccountsMainSegData);
      this.view.transfermain.maketransfer.accountslist.FlexAccountname.lblFromAccountName.text = makeTransferViewModel.accFromKey;
      this.view.transfermain.maketransfer.accountslist.FlexAccountname.lblFromBalance.text = "$"+ makeTransferViewModel.FromExistingBalance;
      this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.setData([]);
      this.displayToAccountsSegmentsModel(fromaccountsobj, makeTransferViewModel.secHeader, makeTransferViewModel,makeTransferViewModel.FromProductID);
    },
    displayToAccountsSegmentsModel: function(fromaccountsobj, selAccountNumber, makeTransferViewModel, selProuductID) {
      var scopeObj = this;
      var myAccountsMainSegData = [];
      var myAccountsrowData = [];
      var accountNumhdval = "";
      var mainaccountNumhdval = "";
      var accountdisplaytext = "";
      var regDCount = "";
      var fromaccounts = fromaccountsobj.from;
      if (fromaccounts !== null && fromaccounts !== undefined && fromaccounts.length > 0) {
        if (makeTransferViewModel.selectedTransferToType === "OTHER_INTERNAL_MEMBER") {
          var accountRecHeader = {
            "lblHeader": ""
          };
          myAccountsrowData = [];
          var accountobjnew = {
            "lblUsers": {
              "text": kony.i18n.getLocalizedString('i18n.transfers.newrecipient'), //accountshareID,
              "toolTip": kony.i18n.getLocalizedString('i18n.transfers.newrecipient') //accountshareID
            },
            "lblSeparator": "Separator"
          }
          myAccountsrowData.push(accountobjnew);
          myAccountsMainSegData.push([accountRecHeader, myAccountsrowData]);
          myAccountsrowData = [];
        }
        for (var i = 0; i < fromaccounts.length; i++) {
          var accountsfrm = fromaccounts[i];
          var fromccountheading = accountsfrm.fromOwner + " | " + ' X' + this.getLastFourDigit(accountsfrm.fromAccountNumber);
          if (selAccountNumber === fromccountheading) {
            var subaccountobj = accountsfrm.subAcct;
            if(subaccountobj !== null && subaccountobj !== undefined && subaccountobj.length >0){
              for (var k = 0; k < subaccountobj.length; k++) {
                var fromproID = subaccountobj[k].fromProductId;
                if (selProuductID === fromproID) {
                  regDCount = subaccountobj[k].regD;
                  var toaccounts = accountsfrm.subAcct[k].to;
                  if (toaccounts !== null && toaccounts !== undefined && toaccounts !== "") {
                    for (var j = 0; j < toaccounts.length; j++) {
                      var account = toaccounts[j];
                      var toaccountNameOwner = account.toOwner;
                      if (makeTransferViewModel.selectedTransferToType === "OTHER_INTERNAL_MEMBER") {
                        toaccountNameOwner = account.accountLastName;
                      }
                      var accountdisplaytxt = "";
                      var accountdisplaysegtxt = "";
                      if(account.toBalance !== null && account.toBalance !== undefined && account.toBalance !== "null"){
                        accountdisplaytxt = " | Avl Bal: "+ CommonUtilities.formatCurrencyWithCommas(account.toBalance);
                        accountdisplaysegtxt = " )";
                      }
                      if(account.productType !== "S" || makeTransferViewModel.selectedTransferToType !== "OWN_INTERNAL_ACCOUNTS"){
                        accountdisplaytxt = "";
                        accountdisplaysegtxt= "";
                      }
                      var accountobj = {
                        "lblUsers": {
                          "text": account.description + " ( " + account.productType + " " + account.productId + accountdisplaysegtxt,
                          "toolTip": account.description + " ( " + account.productType + " " + account.productId + accountdisplaysegtxt //accountshareID
                        },
                        "lblBalance":{
                          "text":CommonUtilities.formatCurrencyWithCommas(account.toBalance),
                          "toolTip":CommonUtilities.formatCurrencyWithCommas(account.toBalance)      //// avail balance
                        },
                        "lblSeparator": "Separator",
                        "flxAccountTypesTransfers": {
                          "onClick": account.onAccountSelection
                        },
                        "ToaccountID": account.accountNumber,
                        "ToProductID": account.productId,
                        "ToProductType": account.productType,
                        "ToPaymentDue": account.toDue,
                        "ToPaymentDate": account.toDate,//!=undefined?account.toDate:"",
                        "ToCCPaymentDate":account.pymntDue,
                        "ToCCStatementBal":account.statementBal,
                        "ToCCMinimumBal":account.minimumBal,
                        "ToCCCurrentBal":account.currentBal,
                        "ToAccountName": toaccountNameOwner,
                        "accountdisplay":  account.description + " | " + account.productType + " " + account.productId + " | "  + toaccountNameOwner + " | " + ' X' + this.getLastFourDigit(account.accountNumber) + accountdisplaytxt
                      }
                      var tempaccountheading = toaccountNameOwner + " | " + account.accountNumber;
                      if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
                        var accountHeader = {
                          "lblHeader": mainaccountNumhdval
                        };
                        myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
                        myAccountsrowData = [];
                        myAccountsrowData.push(accountobj);
                      } else {
                        myAccountsrowData.push(accountobj);
                      }
                      accountNumhdval = toaccountNameOwner + " | " + account.accountNumber;
                      mainaccountNumhdval = toaccountNameOwner + " | " + ' X' + this.getLastFourDigit(account.accountNumber);
                      if (j === 0 && makeTransferViewModel.accToKey === "") {
                        if (makeTransferViewModel.selectedTransferToType === "OTHER_INTERNAL_MEMBER") {
                          accountdisplaytext = kony.i18n.getLocalizedString('i18n.transfers.newrecipient');
                          makeTransferViewModel.accToKey = accountdisplaytext;
                        } else {
                          //accountdisplaytext =  account.description + " | " + account.productType + " " + account.productId + " | "  + toaccountNameOwner + " | " + ' X' + this.getLastFourDigit(account.accountNumber) + accountdisplaytxt;
                          accountdisplaytext =  account.description + " ( " + account.productType + " " + account.productId + " ) ";
                          makeTransferViewModel.accToKey = accountdisplaytext;
                          makeTransferViewModel.ToaccountID = account.accountNumber;
                          makeTransferViewModel.ToProductID = account.productId;
                          makeTransferViewModel.ToProductType = account.productType;
                          makeTransferViewModel.ToPaymentDate = account.toDate;
                          makeTransferViewModel.ToPaymentDue = account.toDue;
                          makeTransferViewModel.ToBalance = account.toBalance;
                        }
                        if (makeTransferViewModel.accountToKey !== null && makeTransferViewModel.accountToKey !== undefined && makeTransferViewModel.accountToKey === account.accountNumber) {
                          accountdisplaytext =  account.description + " ( " + account.productType + " " + account.productId + " ) ";
                          makeTransferViewModel.accToKey = accountdisplaytext;
                          makeTransferViewModel.ToaccountID = account.accountNumber;
                          makeTransferViewModel.ToProductID = account.productId;
                          makeTransferViewModel.ToProductType = account.productType;
                          makeTransferViewModel.ToPaymentDate = account.toDate;
                          makeTransferViewModel.ToPaymentDue = account.toDue;
                          makeTransferViewModel.ToBalance = account.toBalance;
                        }
                      }
                    }
                  } else {
                    if (makeTransferViewModel.accToKey === "") {
                      if (makeTransferViewModel.selectedTransferToType === "OTHER_INTERNAL_MEMBER") {
                        accountdisplaytext = kony.i18n.getLocalizedString('i18n.transfers.newrecipient');
                        makeTransferViewModel.accToKey = accountdisplaytext;
                      } 
                    }
                  }
                }
              }
            }						
          }                       
          if (i === fromaccounts.length - 1) {
            var accountfinHeader = {
              "lblHeader": mainaccountNumhdval
            };
            myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
            kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
          }
        }
      }
      this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.removeAll();
      this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.setData(myAccountsMainSegData);
      this.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromAccountName.text = makeTransferViewModel.accToKey;
      this.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromBalance.text = "$"+ makeTransferViewModel.ToBalance;

      this.FromAccountsRegD(regDCount,makeTransferViewModel);
      this.onSelectNewRecipirnt(makeTransferViewModel.accToKey,makeTransferViewModel);
    }, 

    FromAccountsRegD: function(regDCount,makeTransferViewModel) {
      if (makeTransferViewModel.selectedTransferToType === "OTHER_INTERNAL_MEMBER") {
        this.view.transfermain.maketransfer.lblRegDLimit.text = "";
      } else {

        if (regDCount !== undefined && regDCount !== "" && regDCount !== null ){
          var a = Number(regDCount);
          var z = "You have " + a +" Reg D limits left";         
          if (a === 0) {

            this.view.transfermain.maketransfer.btnConfirm.setEnabled(false);
            makeTransferViewModel.regDCount = 0;
          } else {
            makeTransferViewModel.regDCount = a-1;
            this.view.transfermain.maketransfer.btnConfirm.setEnabled(true);
          }
          this.view.transfermain.maketransfer.lblRegDLimit.text = z;
        }
        else {        
          this.view.transfermain.maketransfer.lblRegDLimit.text = "";
        }
      }    
    },

    clickFromAccountsSeg: function(makeTransferViewModel) {
      var scopeObj = this;
      var index = scopeObj.view.transfermain.maketransfer.accountslist.segAccountTypes.selectedIndex[1];
      var secindex = scopeObj.view.transfermain.maketransfer.accountslist.segAccountTypes.selectedIndex[0];
      var segmentData = scopeObj.view.transfermain.maketransfer.accountslist.segAccountTypes.data[secindex][1][index];
      var segmentHeaderText = scopeObj.view.transfermain.maketransfer.accountslist.segAccountTypes.data[secindex][0].lblHeader;
      var selectedAccountNum = segmentData.lblUsers.text;
      scopeObj.view.transfermain.maketransfer.accountslist.FlexAccountname.lblFromAccountName.text = segmentData.accountdisplay;
      scopeObj.view.transfermain.maketransfer.accountslist.FlexAccountname.lblFromBalance.text = segmentData.FromExistingBalance;
      makeTransferViewModel.accFromKey = segmentData.accountdisplay;
      makeTransferViewModel.FromaccountID = segmentData.FromaccountID;
      makeTransferViewModel.FromProductID = segmentData.FromProductID;
      makeTransferViewModel.FromProductType = segmentData.FromProductType;
      makeTransferViewModel.FromExistingBalance = segmentData.FromExistingBalance;
      makeTransferViewModel.secHeader = segmentHeaderText;
      var fromaccounts = makeTransferViewModel.accountsFrom;
      makeTransferViewModel.accToKey = "";
      var segToData = this.displayToAccountsSegmentsModel(fromaccounts, segmentHeaderText,makeTransferViewModel, segmentData.FromProductID);
      this.onClickFromAccountName(makeTransferViewModel);
      this.view.forceLayout();
    },
    clickToAccountsSeg: function(makeTransferViewModel) {
      var scopeObj = this;
      var index =  scopeObj.view.transfermain.maketransfer.accountsTolist.segAccountTypes.selectedIndex[1]; 
      var secindex = scopeObj.view.transfermain.maketransfer.accountsTolist.segAccountTypes.selectedIndex[0];
      var segmentData = scopeObj.view.transfermain.maketransfer.accountsTolist.segAccountTypes.data[secindex][1][index];
      var segmentHeaderText = scopeObj.view.transfermain.maketransfer.accountsTolist.segAccountTypes.data[secindex][0].lblHeader;
      var selectedAccountNum = segmentData.lblUsers.text;
      if(selectedAccountNum === kony.i18n.getLocalizedString('i18n.transfers.newrecipient')){
        this.onSelectNewRecipirnt(selectedAccountNum,makeTransferViewModel);
        scopeObj.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromAccountName.text = selectedAccountNum;
      } else {
        makeTransferViewModel.ToProductType = segmentData.ToProductType;
        makeTransferViewModel.ToPaymentDate = segmentData.ToPaymentDate;
        makeTransferViewModel.ToPaymentDue = segmentData.ToPaymentDue;
        makeTransferViewModel.ToCCPaymentDate = segmentData.ToCCPaymentDate;
        makeTransferViewModel.ToCCStatementBal = segmentData.ToCCStatementBal;
        makeTransferViewModel.ToCCMinimumBal = segmentData.ToCCMinimumBal;
        makeTransferViewModel.ToCCCurrentBal = segmentData.ToCCCurrentBal;
        this.onSelectNewRecipirnt(selectedAccountNum,makeTransferViewModel);
        makeTransferViewModel.accToKey = selectedAccountNum;
        makeTransferViewModel.ToaccountID = segmentData.ToaccountID;
        makeTransferViewModel.ToProductID = segmentData.ToProductID;
        makeTransferViewModel.ToAccountName = segmentData.ToAccountName;
        scopeObj.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromAccountName.text = selectedAccountNum;
        scopeObj.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromBalance.text = segmentData.lblBalance.text;
      }
      this.onClickToAccountName(makeTransferViewModel);
      scopeObj.view.transfermain.maketransfer.lbxForHowLong.selectedKey = "DATE_RANGE";
      scopeObj.view.transfermain.maketransfer.flexswitchOff.left = "3.77%";
      scopeObj.view.transfermain.maketransfer.flexswitchOn.skin = "slFboxSwitchGreyOff";
      scopeObj.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
      scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
      scopeObj.onHowLongChange();
      scopeObj.view.transfermain.maketransfer.lbxFrequency.selectedKey = "Now";
      scopeObj.onFrequencyChanged(); 

    },
    onSelectNewRecipirnt: function(selectedAccountNum,makeTransferViewModel){
      var scopeObj = this;
      this.view.transfermain.maketransfer.lbxFrequency.masterData = makeTransferViewModel.frequencies;
      scopeObj.view.transfermain.maketransfer.lbxForHowLong.setEnabled(true);

      scopeObj.view.transfermain.maketransfer.lblSendOn.text = kony.i18n.getLocalizedString('i18n.transfers.send_on')
      if(selectedAccountNum === kony.i18n.getLocalizedString('i18n.transfers.newrecipient')){
        kony.print("new recipient");
        scopeObj.view.transfermain.maketransfer.flxAmount.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblAmount.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblForhowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxForHowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOff.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.tbxNoOfRecurrences.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxCalSendOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblSendOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexNewRecipient.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.flxWhenLoan.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxMainCreditCard.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.tbxLastName.text = makeTransferViewModel.recLastName || "";
        scopeObj.view.transfermain.maketransfer.tbxMembershipID.text = makeTransferViewModel.recMemshpID;
        scopeObj.view.transfermain.maketransfer.tbxShareID.text = makeTransferViewModel.recShareID;
      } else {        
        scopeObj.view.transfermain.maketransfer.flxAmount.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.lblAmount.setVisibility(true);
        if (makeTransferViewModel.selectedTransferToType === "OTHER_INTERNAL_MEMBER") {
          scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(false);
          scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(false);
          scopeObj.view.transfermain.maketransfer.flexNewRecipient.setVisibility(false);
          scopeObj.view.transfermain.maketransfer.flxWhenLoan.setVisibility(false);
          scopeObj.view.transfermain.maketransfer.flxMainCreditCard.setVisibility(false);
        } else {              
          if(makeTransferViewModel.ToProductType === "L" || makeTransferViewModel.ToProductType === "M") {
            scopeObj.view.transfermain.maketransfer.lblDueDateLoan.text = "(Payment Date: "+makeTransferViewModel.ToPaymentDate+")";
            scopeObj.view.transfermain.maketransfer.lblPAymentAmount.text = makeTransferViewModel.ToPaymentDue;
            scopeObj.view.transfermain.maketransfer.flxAmount.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lblAmount.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lblForhowLong.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lbxForHowLong.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.flexswitchOff.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.flexswitchOn.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.flxCalSendOn.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lblSendOn.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lblSendOn.text = "Pay On"
            scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.tbxNoOfRecurrences.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.flxStartDate1.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.lblStart.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.flexNewRecipient.setVisibility(false);
            scopeObj.view.transfermain.maketransfer.flxWhenLoan.setVisibility(true);
            scopeObj.view.forceLayout();
          }else {              
            if(makeTransferViewModel.ToProductType === "CC") {
              scopeObj.view.transfermain.maketransfer.lblCCDueDate.text = makeTransferViewModel.ToCCPaymentDate;
              scopeObj.view.transfermain.maketransfer.lblCurrentBalance.text = makeTransferViewModel.ToCCCurrentBal;
              scopeObj.view.transfermain.maketransfer.lblStatementBalance.text = makeTransferViewModel.ToCCStatementBal;
              scopeObj.view.transfermain.maketransfer.lblMinimumBalance.text = makeTransferViewModel.ToCCMinimumBal;
              scopeObj.view.transfermain.maketransfer.flxAmount.setVisibility(false);
              scopeObj.view.transfermain.maketransfer.lblAmount.setVisibility(false);
              scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(false);
              scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(false);
              scopeObj.view.transfermain.maketransfer.flexNewRecipient.setVisibility(false);
              scopeObj.view.transfermain.maketransfer.flxMainCreditCard.setVisibility(true);
              scopeObj.view.forceLayout();
            }
            else {
              scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(true);
              scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(true);
              scopeObj.view.transfermain.maketransfer.flexNewRecipient.setVisibility(false);
              scopeObj.view.transfermain.maketransfer.flxWhenLoan.setVisibility(false);
              scopeObj.view.transfermain.maketransfer.flxMainCreditCard.setVisibility(false);
            }        
          }
        }
      }
      this.AdjustScreen()
    },

    onClickFromAccountName: function(makeTransferViewModel) {
      var scopeObj = this;
      if(scopeObj.view.transfermain.maketransfer.accountslist.flxAccountTypesSegment.isVisible === true){
        scopeObj.view.transfermain.maketransfer.accountslist.flxAccountTypesSegment.setVisibility(false);
      } else{
        scopeObj.view.transfermain.maketransfer.accountslist.flxAccountTypesSegment.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.accountsTolist.flxAccountTypesSegment.setVisibility(false);
      }
      this.view.forceLayout();
    },
    onClickToAccountName: function(makeTransferViewModel) {
      var scopeObj = this;
      if(scopeObj.view.transfermain.maketransfer.accountsTolist.flxAccountTypesSegment.isVisible === true){
        scopeObj.view.transfermain.maketransfer.accountsTolist.flxAccountTypesSegment.setVisibility(false);
      } else{
        scopeObj.view.transfermain.maketransfer.accountsTolist.flxAccountTypesSegment.setVisibility(true);
      }
      this.view.forceLayout();
    },
    onAccountSelection: function () {
      kony.print("account object is "+account);
      kony.print("account object is step two");
    },
    filterFromAccount: function (makeTransferViewModel) {
      var scopeObj = this;
      if (makeTransferViewModel.accountsTo.length === 1 && makeTransferViewModel.accountsFrom.length > 1) {
        var fromAccountMasterData = makeTransferViewModel.accountsFrom.slice().filter(function (fromAccountPair) {
          return  !scopeObj.presenter.MakeTransfer.isSameAccount(fromAccountPair[0], makeTransferViewModel.accountsTo[0][0])
        })
        if (fromAccountMasterData.length < makeTransferViewModel.accountsFrom.length) {
          scopeObj.view.transfermain.maketransfer.lbxFromAccount.masterData = fromAccountMasterData;      
        }
      }
    },
    filterAccountsTo: function (makeTransferViewModel) {
      var scopeObj = this;
      var fromSelectedIndex = scopeObj.view.transfermain.maketransfer.lbxFromAccount.selectedKey;
      if (makeTransferViewModel.accountsTo.length > 1) {
        var toAccountMasterData = makeTransferViewModel.accountsTo.slice().filter(function (toAccountPair) {
          return  !scopeObj.presenter.MakeTransfer.isSameAccount(fromSelectedIndex, toAccountPair[0])
        })
        if (toAccountMasterData.length < makeTransferViewModel.accountsTo.length)
          scopeObj.view.transfermain.maketransfer.lbxToAccount.masterData = toAccountMasterData;
      }
    },
    configureTransferGateway: function(makeTransferViewModel) {
      var transferGatewayConfig = [  
        {  
        key: OLBConstants.TRANSFER_TYPES.OWN_INTERNAL_ACCOUNTS,
        info:["i18n.transfers.MemToMemTrans","i18n.transfers.transferLimit"], 
        title:"i18n.transfers.toMyKonyBankAccounts",
        image: "to_konybank_acc.png",
        },

        {
          key: OLBConstants.TRANSFER_TYPES.OTHER_INTERNAL_MEMBER,
          info:["i18n.transfers.OrangeToOthers","i18n.transfers.transferLimit"], 
          title:"i18n.transfers.toOtherKonyBankAccounts",
          image: "to_otherkonybank_acc.png"
        },
        {key: OLBConstants.TRANSFER_TYPES.OTHER_EXTERNAL_ACCOUNT,
          info:["i18n.transfers.OtherDomesticBank","i18n.transfers.transferLimit"], 
          title:"i18n.transfers.toOtherBankAccounts",
          image: "to_otherbank_acc.png",
          external: true,
      },  
      ]
      var self = this;
      var transferToViews = this.view.transfermain.flxTransfersGateway.widgets();			
      this.view.WhatisThisCustomPopup.btnYes.onClick = this.onClickYesWhatisThisPopup.bind(this, makeTransferViewModel);
      this.view.WhatisThisCustomPopup.flxCross.onClick = this.onClickYesWhatisThisPopup.bind(this, makeTransferViewModel);

      function createCallback(transferType, visible) {
        gblrecipientAccID = "";
        // Blocking Wire Transfer not part of the scope
        if(transferType.key === 'WIRE_TRANSFER') {
          return function () {
          }
        }
        return function () {
          self.presenter.loadAccountsByTransferType(transferType.key);
        }
      }

      var visibleCounter = 0;
      for (var index = 0; index < transferGatewayConfig.length; index++) {
        var transferToView = transferToViews[visibleCounter+1];
        var gatewayConfig = transferGatewayConfig[index];
          transferToView.setVisibility(true);
          var infoWidgets = transferToView.widgets()[0].widgets();
          infoWidgets[0].src = gatewayConfig.image;
          infoWidgets[1].text = kony.i18n.getLocalizedString(gatewayConfig.title);
          infoWidgets[2].text = this.getDisplayDescription(gatewayConfig.displayName,gatewayConfig.key);
          var buttonProceed = transferToView.widgets()[1].widgets()[0];
          buttonProceed.onClick = createCallback(gatewayConfig);
          visibleCounter++;
      }
  
      for (var i = visibleCounter + 1; i < transferToViews.length ; i++) {
        var view = transferToViews[i];
        view.setVisibility(false);
      }
    },
    onClickWhatisThisBtn: function(makeTransferViewModel,index) {
      var scobj = this;
      var id = scobj.view.transfermain.flxTransfersGateway.id;
      if(scobj.view.flxPopup.isVisible === false){
        if(index === 0){
          this.view.WhatisThisCustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.transfers.MyAccountsDesc");
        } else if(index === 1) {
          this.view.WhatisThisCustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.transfers.OCCUAccountsDesc"); 
        } else {
          this.view.WhatisThisCustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.transfers.OtherAccountsDesc");
        }
        scobj.view.flxPopup.setVisibility(true);
      } else {
        scobj.view.flxPopup.setVisibility(false);
      }
    },
    onClickYesWhatisThisPopup: function(makeTransferViewModel) {
      var scobj = this;
      scobj.view.flxPopup.setVisibility(false);
    },

    resetTransfersForm: function () {
      this.view.transfermain.maketransfer.lbxFromAccount.masterData = [];
      this.view.transfermain.maketransfer.lbxToAccount.masterData = [];
      // this.filterAccountsTo(makeTransferViewModel);
      // this.filterFromAccount(makeTransferViewModel);
      this.view.transfermain.maketransfer.lbxFrequency.masterData = this.presenter.getFrequencies();
      this.view.transfermain.maketransfer.calSendOn.date = commonUtils.DateUtils.toDateFormat(new Date(), kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'));
      this.view.transfermain.maketransfer.calEndingOn.date = commonUtils.DateUtils.toDateFormat(new Date(), kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'));
      this.view.transfermain.maketransfer.lbxForHowLong.masterData = this.presenter.getForHowLong();
      this.view.transfermain.maketransfer.calStart.date = commonUtils.DateUtils.toDateFormat(new Date(), kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'));
      this.view.transfermain.maketransfer.tbxLastName.text = "";
      this.view.transfermain.maketransfer.calEndingOn.date = commonUtils.DateUtils.toDateFormat(new Date(), kony.onlineBanking.configurations.getConfiguration('frontendDateFormat'));
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.text = "";
      this.view.transfermain.maketransfer.txtNotes.text = "";
      this.view.transfermain.maketransfer.tbxShareID.text = "";
      this.view.transfermain.maketransfer.tbxMembershipID.text = "";
      this.view.transfermain.maketransfer.tbxAmountRec.text = "";
      this.view.transfermain.maketransfer.txtBxOtherAmount.text = "";
      this.view.transfermain.maketransfer.txtOtherAmount.text = "";
      this.view.transfermain.maketransfer.RbPaymentAmount.masterData = [];
      this.view.transfermain.maketransfer.RbOneTimeLoan.masterData = [];
      this.view.transfermain.maketransfer.RbNowLoan.masterData = [];
      this.view.transfermain.maketransfer.setAmount("");
      this.view.transfermain.maketransfer.formatAndShow();
      this.checkValidityMakeTransferForm();
      this.onFrequencyChanged();
    },

    getFormData: function (viewMoel) {
      var viewModel = {};
      viewModel.accountFromKey = this.view.transfermain.maketransfer.lbxFromAccount.selectedKey;
      viewModel.accountToKey =  this.view.transfermain.maketransfer.lbxToAccount.selectedKey;
      viewModel.amount= this.view.transfermain.maketransfer.tbxAmount.text.replace(/,/g, "");
      viewModel.frequencyKey= this.view.transfermain.maketransfer.lbxFrequency.selectedKey;
      viewModel.howLongKey= this.view.transfermain.maketransfer.lbxForHowLong.selectedKey;
      viewModel.sendOnDate= this.view.transfermain.maketransfer.calSendOn.date;
      viewModel.sendOnDateone= this.view.transfermain.maketransfer.calStart.date;  
      viewModel.sendOnDateComponentsone= this.view.transfermain.maketransfer.calStart.dateComponents;///added newly
      viewModel.sendOnDateComponents= this.view.transfermain.maketransfer.calSendOn.dateComponents;
      viewModel.endOnDate= this.view.transfermain.maketransfer.calEndingOn.date;
      viewModel.endOnDateComponents= this.view.transfermain.maketransfer.calEndingOn.dateComponents;
      viewModel.noOfRecurrences= this.view.transfermain.maketransfer.tbxNoOfRecurrences.text.trim();
      viewModel.notes= this.view.transfermain.maketransfer.txtNotes.text.trim();
      viewModel.recLastName = this.view.transfermain.maketransfer.tbxLastName.text.trim();
      viewModel.recMemshpID = this.view.transfermain.maketransfer.tbxMembershipID.text.trim();
      viewModel.recShareID = this.view.transfermain.maketransfer.tbxShareID.text.trim();
      viewModel.recamount= this.view.transfermain.maketransfer.tbxAmountRec.text;
      viewModel.ToPaymentDue= this.view.transfermain.maketransfer.lblPAymentAmount.text;
      viewModel.otherAmount= this.view.transfermain.maketransfer.txtBxOtherAmount.text;
      viewModel.ccOtherAmount= this.view.transfermain.maketransfer.txtOtherAmount.text;
      viewModel.paymentAmountkey = this.view.transfermain.maketransfer.RbPaymentAmount.selectedKey;
      viewModel.RbOneTimeLoanKey = this.view.transfermain.maketransfer.RbOneTimeLoan.selectedKey;
      viewModel.RbNowLoan = this.view.transfermain.maketransfer.RbNowLoan.selectedKey;
      return viewModel;
    },     
    checkValidityMakeTransferForm: function () {

      var disableCreateTransferButton = function () {
        this.view.transfermain.maketransfer.btnConfirm.setEnabled(false);
        this.view.transfermain.maketransfer.btnConfirm.skin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.transfermain.maketransfer.btnConfirm.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";  
        this.view.transfermain.maketransfer.btnConfirm.focusSkin="sknBtnBlockedLatoFFFFFF15Px";        
      }.bind(this);

      var formData = this.getFormData({});
      if (formData.amount === null || formData.amount === "") {
        disableCreateTransferButton();
        return;
      }
      if(selectedtransferType === "OWN_INTERNAL_ACCOUNTS"){
        if (formData.frequencyKey === "Once"){
          var currDate = new Date();
          currDate.setDate(currDate.getDate() + 1);
          var month = currDate.getMonth() + 1;
          var nextdateVal = "";
          if (CommonUtilities.getConfiguration("frontendDateFormat") === "mm/dd/yyyy") {
            nextdateVal = (month < 10 ? '0' + month : month) + "/" +  (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/"  + currDate.getFullYear();
          }
          else {
            nextdateVal = (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/" + (month < 10 ? '0' + month : month) + "/" + currDate.getFullYear();        
          }
          this.view.transfermain.maketransfer.calSendOn.date = nextdateVal;
          this.view.transfermain.maketransfer.calSendOn.dateComponents = this.view.transfermain.maketransfer.calSendOn.dateComponents;
          //return;
        } else {
          var currDate = new Date();
          var month = currDate.getMonth() + 1;
          var curdateval = "";
          if (CommonUtilities.getConfiguration("frontendDateFormat") === "mm/dd/yyyy") {
            curdateval = (month < 10 ? '0' + month : month) + "/" +  (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/"  + currDate.getFullYear()
          }
          else {
            curdateval =  (currDate.getDate() < 10 ? '0' + currDate.getDate() : currDate.getDate()) + "/" + (month < 10 ? '0' + month : month) + "/" + currDate.getFullYear();        
          }
          this.view.transfermain.maketransfer.calSendOn.date = curdateval;
          this.view.transfermain.maketransfer.calSendOn.dateComponents = this.view.transfermain.maketransfer.calSendOn.dateComponents;
          //return;
        }
      }
      /*if(formData.frequencyKey !== "Once" && formData.frequencyKey !== "Now" && formData.howLongKey === "NO_OF_RECURRENCES" && formData.noOfRecurrences === "") {
        disableCreateTransferButton();
        return;
      }  */ 
      this.view.transfermain.maketransfer.btnConfirm.skin = "sknbtnLatoffffff15px";
      this.view.transfermain.maketransfer.btnConfirm.setEnabled(true);
      this.view.transfermain.maketransfer.btnConfirm.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
      this.view.transfermain.maketransfer.btnConfirm.focusSkin="sknBtnFocusLatoFFFFFF15Px";
    },

    // iniTabActions 
    initTabsActions: function () {
      var scopeObj = this;
      // this.view.transfermain.maketransfer.flxNewTransferForm.setVisibility(true);
      this.view.transfermain.Search.txtSearch.width = "100%";
      this.view.transfermain.imgSearch.src = "search_blue.png";
      this.setSearchFlexVisibility(false);
      this.view.customheader.customhamburger.activateMenu("Transfers", "Transfer Money");
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      /// start
      this.view.customheader.topmenu.flxaccounts.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
      //// End
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      this.view.flxTransferViewReport.isVisible = false;
      this.fixBreadcrumb();
      this.view.transfermain.btnMakeTransfer.onClick = function () {this.presenter.showTransferScreen()}.bind(this);
      this.view.transfermain.btnExternalAccounts.onClick=function(){this.presenter.showExternalAccounts()}.bind(this);
      this.view.transfermain.btnExternalAccounts.toolTip = kony.i18n.getLocalizedString('i18n.Transfers.Recipients');
      this.view.transfermain.btnRecent.onClick=function(){this.getUserRecentTransactions()}.bind(this);
      this.view.transfermain.btnScheduled.onClick=function(){this.getUserScheduledTransactions()}.bind(this);
      this.setHeaderActions();

      this.view.transfermain.maketransfer.lbxForHowLong.onSelection = this.onHowLongChange.bind(this);
      this.view.transfermain.maketransfer.lbxFrequency.onSelection = this.onFrequencyChanged.bind(this);
      // this.view.transfermain.maketransfer.tbxAmount.onKeyUp = this.onAmountKeyUp.bind(this);
      this.view.transfermain.maketransfer.tbxAmount.onTextChange = this.onAmountChanged.bind(this);    
      this.view.transfermain.maketransfer.tbxNoOfRecurrences.onKeyUp = this.checkValidityMakeTransferForm.bind(this);
      this.view.transfermain.maketransfer.btnModify.text = "Cancel";
      this.view.transfermain.maketransfer.flxOption1.setVisibility(false);
      this.view.transfermain.maketransfer.flxOption2.setVisibility(false);
      this.view.transfermain.maketransfer.flxOption3.setVisibility(false);
      this.view.transfermain.maketransfer.flxOption4.setVisibility(false);
      this.view.transfermain.maketransfer.lblDueDate.setVisibility(false);
    },

    updateGateWay: function (gateway) {
      this.showTransfersGateway();
      this.configureTransferGateway(gateway.overrideFromAccount);
    },






    //Modification for delete recipient

    onExternalAccountDelete:function(param){
      var scopeObj=this;
      kony.print("btn Delete pressed");
      scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount");

      if(param=="E") {

        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccountMsgForExternal");
      } 

      else {
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccountMsg");
      }

      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMain.frame.height;
      scopeObj.view.flxLogout.height = height + "dp";
      scopeObj.view.flxLogout.left = "0%";

      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      //alert("data"+JSON.stringify(data));
      var accountNumber=data[index].lblIdentifier;
      var lastName=data[index].lblAccountNumberValue;
      var accountType=data[index].lblStatus;
      //var accType=CommonUtilitiesOCCU.getAccountTypeKey(accountType);

      var shareId=data[index].lblRoutingNumberValue; 
      var description=" ";
      this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString('i18n.common.deleteTheAccount');
      this.view.CustomPopup.btnYes.onClick = function () {
        kony.print("btn yes pressed");
        //function
        scopeObj.presenter.deleteExternalAccount(accountNumber,lastName,accountType,shareId,description);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString('i18n.common.noDontDelete');
      this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };  
      this.AdjustScreen();
    },


    hideAll: function () {
      this.view.transfermain.btnMakeTransfer.skin = "sknBtnAccountSummaryUnselected";
      this.view.transfermain.btnRecent.skin = "sknBtnAccountSummaryUnselected";
      this.view.transfermain.btnScheduled.skin = "sknBtnAccountSummaryUnselected";
      this.view.transfermain.btnExternalAccounts.skin = "sknBtnAccountSummaryUnselected";
      this.view.flxMakeTransferError.setVisibility(false);
      this.view.transfermain.flxSort.setVisibility(false);
      this.view.transfermain.flxMakeTransferForm.setVisibility(false);
      this.view.transfermain.segmentTransfers.setVisibility(false);
      this.view.transfermain.flxNoTransactions.setVisibility(false);
      this.view.transfermain.tablePagination.setVisibility(false);
      this.view.transfermain.flxTransfersGateway.setVisibility(false);
      this.view.transfermain.flxSortExternal.setVisibility(false);
      this.view.transferactivity.isVisible=false;
      // this.view.flxMyPaymentsAccount.isVisible=false;   ////newly added
    },    


    showTransfersGateway: function () {
      this.hideAll();
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfers')}, {text:kony.i18n.getLocalizedString("i18n.transfers.make_transfer")}]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.make_transfer");
      this.view.transfermain.btnMakeTransfer.skin = "sknBtnAccountSummarySelected";
      this.view.transfermain.flxTransfersGateway.setVisibility(true);
      this.view.transfermain.flxSearchImage.isVisible=false;
      this.setSearchFlexVisibility(false);
      this.view.transfermain.flxRowSeperator.setVisibility(false);
      this.view.forceLayout();
    },

    //////....> for Scheduled Data Mapping 




    showScheduledData: function (data, config) {
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString('i18n.transfers.transfers')}, {text:kony.i18n.getLocalizedString("i18n.transfers.scheduled")}]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.scheduled");
      if ((data === undefined) || ((data instanceof Array) && data.length === 0)||(data.opstatus===0)) {
        this.showNoTransactions();
      }
      else {

        this.showSegment();
        this.sortFlex("Recent");        
        this.view.transfermain.btnScheduled.skin = "sknBtnAccountSummarySelected";
        this.view.transfermain.tablePagination.flxPaginationPrevious.onClick = function () {};
        this.view.transfermain.tablePagination.flxPaginationNext.onClick = function () {};
        this.view.transfermain.flxRowSeperator.setVisibility(true);
        this.getNewTransfersData(data, this.getUserScheduledTransactions);
        this.view.mypaymentAccounts.lblAccounts.setVisibility(true);
        this.view.mypaymentAccounts.lblOCCU.setVisibility(true);
        this.view.mypaymentAccounts.lblCreditBank.setVisibility(true);
        this.view.flxMyPaymentsAccount.setVisibility(true);
        this.view.transfermain.imgSortType.setVisibility(true);
        this.setScheduledTransactionsPagination();
        this.view.transfermain.lblActions.setVisibility(true);
        this.view.transfermain.flxSortAmount.setVisibility(true);

        this.view.transfermain.flxSortExternal.setVisibility(false);
        this.view.transfermain.flxSort.setVisibility(true);
        this.setSearchFlexVisibility(false);
        var scheduledDataMap = {
          "btnRepeat": "btnScheduledDelete",
          "btnAction": "btnScheduledRepeat",
          "btnEdit":"btnEdit",
          "flxDropdown": "flxDropdown",
          "imgDropdown": "imgDropdown",
          "lblAmount": "lblAmount",
          "template": "scheduledTemplate",
          "lblDate": "lblDate",
          "lblFromAccountTitle": "lblFromAccountTitle",
          "lblFromAccountValue": "lblFromAccountValue",
          "lblIdentifier": "lblIdentifier",
          "lblNoteTitle": "lblNoteTitle",
          "lblNoteValue": "lblNoteValue",
          "lblRecurrenceTitle": "lblRecurrenceTitle",
          "lblRecurrenceValue": "lblRecurrenceValue",
          "lblReferenceNumberTitle": "lblReferenceNumberTitle",
          "lblReferenceNumberValue": "lblReferenceNumberValue",
          "lblSendTo": "lblSendTo",
          "lblSeparator": "lblSeparator",
          "lblStatusTitle": "lblStatusTitle",
          "lblStatusValue": "lblStatusValue",
        };
      }
      this.view.transfermain.segmentTransfers.widgetDataMap = scheduledDataMap;
      this.view.transfermain.segmentTransfers.setData(this.transfersViewModel.transactionsData);
      CommonUtilities.Sorting.setSortingHandlers(this.recentAndScheduledSortMap, this.onScheduledSortClickHandler, this);
      CommonUtilities.Sorting.updateSortFlex(this.recentAndScheduledSortMap, config);
      CommonUtilities.hideProgressBar(this.view);
      this.view.forceLayout();
    },


    ///function to convert 

    changeAccType: function(test) {
      var type;
      if (test == "S" || test == "s") {
        type = "Savings"
      } else if (test == "L" || test == "l") {
        type = "Loan";
      } else if (test == "CC" || test == "cc") {
        type = "CreditCard";
      } else if (test == "M" || test == "m") {
        type = "Mortgage";
      } else if (test == "C" || test == "c") {
        type = "Checking";
      }
      return type;
    },


    // functions for navigating to Verify accounts 

    goToVerifyAccount : function(accID,data){


      var tfrModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
      tfrModule.presentationController.navigateToVerifyAccount(accID,data);


    },


    calculateIndex: function(){

      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      var rowData=data[index];
      var accID=data[index].lblRoutingNumberValue;
      this.goToVerifyAccount(accID,rowData);
    },

    /// delete scheduled transfer for internal/External


    delete:function(value){
      var typeAccount=value;
      if(value==null|| value=="null"){
        accType="internal";
        this.deleteTransaction(accType);
      }
      else{
        accType="external";
        this.deleteTransaction(accType);
      }

    },

    //edit schedule transfer overiding


    editScheduledTransaction: function(transaction, onCancelCreateTransfer){
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      var selectedData = data[index];
      if(transaction.scheduledTransferId!==null && transaction.scheduledTransferId !=="null"){
        accType="OTHER_EXTERNAL_ACCOUNT";
      } 
      else{
        accType="OWN_INTERNAL_ACCOUNTS";
      }
      var record = {
        "amount": transaction.amount,
        "frequencyEndDate": transaction.frequencyEndDate,
        "frequencyStartDate": selectedData.frequencyStartDate,
        "frequencyType": selectedData.frequencyType,
        "fromAccountNumber": selectedData.fromAccountNumber,
        "numberOfRecurrences": selectedData.lblRecurrenceValue,
        "scheduledDate": transaction.nextTransfer,
        "toAccountNumber": selectedData.toAccountNumber,
        "transactionDate": selectedData.lblDate,
        "ExternalAccountNumber": selectedData.externalAccountNumber,
        "transactionId": transaction.scheduledTransferId,
        "transactionsNotes": selectedData.lblNoteValue ,
        "transactionType": selectedData.transactionType,
        "category": accType,
        "selectedTransferToType":accType
      };
      this.presenter.showMakeTransferForEditTransaction(record, onCancelCreateTransfer);
      this.AdjustScreen();
    },


    //for scheduled data overiding createNewTransferData

    createNewTransfersData: function (onCancelCreateTransfer,transaction) {
      var scopeObj = this;
      if(transaction.transactionsNotes===undefined || transaction.transactionsNotes===null)
        transaction.transactionsNotes="None";
      if(transaction.opstatus===0){
        this.view.transfermain.flxNoTransactions.setVisibility(true);
        this.view.transfermain.segmentTransfers.isVisible = false;
        this.view.flxTrasfersWindow.isVisible = true;
        this.view.transfermain.flxTransfersGateway.isVisible = false;
        this.view.transfermain.lblScheduleAPayment.isVisible = false;
        this.view.transfermain.flxSortExternal.isVisible = false;
        this.view.transferactivity.isVisible = false;
        //this.sortFlex("ExternalAcc");
        //this.showSegment();
        this.view.transfermain.flxRowSeperator.setVisibility(true);
        this.view.transfermain.btnExternalAccounts.skin = "sknBtnAccountSummarySelected";

      }

      isDeleteIstrue=true;
      if(transaction.edit === "false"|| transaction.edit===false){
        isDeleteIstrue=false; 
      }


      return {
        btnAction: {
          text: kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
          toolTip:kony.i18n.getLocalizedString("i18n.transfers.viewReport"),
          onClick: this.viewTransactionReport,
        },
        btnRecentRepeat: {
          text: kony.i18n.getLocalizedString("i18n.transfers.repeat"),
          toolTip:kony.i18n.getLocalizedString("i18n.transfers.repeat"),
          onClick: this.repeatTransaction.bind(this, onCancelCreateTransfer),
        },
        btnScheduledRepeat: {
          text: kony.i18n.getLocalizedString("i18n.accounts.scheduledDetails"),       //---------->changed edit to details
          toolTip: kony.i18n.getLocalizedString("i18n.accounts.scheduledDetails"),	//---------->changed edit to details
          //onClick: this.editScheduledTransaction.bind(this, transaction, onCancelCreateTransfer),
          //onClick:this.showUnselectedRow();
        },
        btnScheduledDelete: {
          text:kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
          toolTip:kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
          onClick: this.delete.bind(this,transaction.scheduledTransferId),
          isVisible:isDeleteIstrue
        },

        btnEdit: {
          text:"EDIT",
          //toolTip:kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
          onClick: this.editScheduledTransaction.bind(this, transaction, onCancelCreateTransfer),
        },
        btnRepeat: {
          text: kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
          toolTip: kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
          //onClick: this.deleteTransaction

        },

        "flxDropdown": "flxDropdown",
        "imgDropdown": "arrow_down.png",
        "recentTemplate": "flxRecentTransfers",
        "scheduledTemplate": "flxScheduledTransfers",
        "lblFromAccountTitle": "Frequency",
        "lblRecurrenceTitle": kony.i18n.getLocalizedString("i18n.transfers.lblRecurrences"),
        "lblReferenceNumberTitle": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"), //accountNumber moved in place of reference number
        "lblSeparator": transaction.fromAccount,
        "lblStatusTitle": kony.i18n.getLocalizedString("i18n.Transfers.Statustitle"),
        "lblNoteTitle": kony.i18n.getLocalizedString("i18n.transfers.note"),
        "category": transaction.scheduledTransferId,
        "frequencyType": transaction.frequency,
        "transactionType": transaction.transactionType,
        "fromAccountNumber": transaction.fromAccount.split("-")[0],
        "toAccountNumber": transaction.toAccount,
        "lblAmount":transaction.amount,
        "externalAccountNumber": transaction.ExternalAccountNumber,
        "lblSendTo": transaction.description,
        "lblLatestScheduledTransaction": transaction.nextTransfer,
        "newRecurrenceValue": (transaction.recurrenceDesc || ' '),
        "lblDate": transaction.nextTransfer,
        "scheduledDate": transaction.nextTransfer,						///transfers.Transfer.toAccount.split("-")[0]
        "lblStatusValue": transaction.statusDescription,
        "lblReferenceNumberValue": transaction.fromAccount.split("-")[0],
        "lblRecurrenceValue": transaction.numberOfRecurrences,
        "lblFromAccountValue": transaction.frequency,
        "lblNoteValue": transaction.transactionsNotes,
      };
    },



    //////// ..............> overided for Delete Operation


    deleteTransaction: function(param) {
      kony.print("delete transaction");
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      var selectedData = data[index];
      var TransactionsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository('Transfer');
      if(param=="internal"){

        var transactionObject={

          "fromAccountNumber":selectedData.fromAccountNumber,
          "toAccountNumber":selectedData.toAccountNumber.split("-")[0],
          "frequency":selectedData.frequencyType,
          "nextTransfer":selectedData.scheduledDate,
          "toProductType":selectedData.toAccountNumber.split("-")[1],
          "toProductId":selectedData.toAccountNumber.split("-")[2],
          "amount":selectedData.lblAmount,
          "fromProductType":selectedData.lblSeparator.split("-")[1],
          "fromProductId":selectedData.lblSeparator.split("-")[2],
          "username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
          "type":param,
          "edit":"",
          "startDate": "",
          "endDate": ""
        };

      }
      else{
        var transactionObject={
          "username":kony.mvc.MDAApplication.getSharedInstance().appContext.usernameOCCU,
          "scheduledTransferId":selectedData.category,
          "type":param

        };
      }



      var scopeObj = this;
      scopeObj.view.deletePopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
      scopeObj.view.deletePopup.lblPopupMessage.text = kony.i18n.getLocalizedString("I18n.billPay.QuitTransactionMsg");
      var height = scopeObj.view.flxHeader.frame.height + scopeObj.view.flxMain.frame.height;
      scopeObj.view.flxDelete.height = height + "dp";
      scopeObj.view.flxDelete.left = "0%";
      this.view.deletePopup.btnYes.onClick = function() {
        scopeObj.view.flxDelete.left = "-100%";
        scopeObj.presenter.deleteTransfer(this, transactionObject);
      }
      this.view.deletePopup.btnNo.onClick = function() {
        kony.print("btn no pressed");
        scopeObj.view.flxDelete.left = "-100%";
      }
      this.view.deletePopup.flxCross.onClick = function() {
        kony.print("btn no pressed");
        scopeObj.view.flxDelete.left = "-100%";
      }
      this.AdjustScreen();
    },






    /////  for all the recipients internal and external

    showExternalAccounts:function(viewModel, hidePagination){

      var Internal = [];
      var External = [];
      var moreOptions=[];
      var newArray1=[];
      var newArray2=[];
      var newArray3=[];

      if(viewModel=="errorExternalAccounts"){
        this.hideAll();
        this.showServerError();
        this.view.transfermain.flxNoTransactions.setVisibility(true);
        this.view.transfermain.rtxNoPaymentMessage.i18n_text= kony.i18n.getLocalizedString("i18n.transfers.errorExternalAccounts");
        this.view.transfermain.lblScheduleAPayment.isVisible=false;
        this.view.forceLayout();
      }else if ((viewModel === undefined) || ((viewModel instanceof Array) && viewModel.length < 2)||viewModel.opstatus===0) {
        this.view.transfermain.btnExternalAccounts.skin = "sknBtnAccountSummarySelected";
        this.showNoTransactions("EXTERNAL ACCOUNT");
      }

      else{
        var scopeObj = this;
        this.view.flxMakeTransferError.setVisibility(false);
        this.view.flxTrasfersWindow.isVisible=true;
        this.view.transferactivity.isVisible=false;
        this.sortFlex("ExternalAcc");
        this.showSegment();
        this.view.transfermain.flxRowSeperator.setVisibility(true);
        this.view.mypaymentAccounts.lblAccounts.setVisibility(true);
        this.view.mypaymentAccounts.lblOCCU.setVisibility(true);
        this.view.mypaymentAccounts.lblCreditBank.setVisibility(true);
        this.view.flxMyPaymentsAccount.setVisibility(true);
        this.view.transfermain.btnExternalAccounts.skin = "sknBtnAccountSummarySelected";
        this.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.transfers.transfer")
        }, {
          text: kony.i18n.getLocalizedString("i18n.transfers.external_accounts")
        }]);
        this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
        this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.transfers.external_accounts");

        this.view.transfermain.lblSortDate.text = "Account Name";
        this.view.transfermain.lblSortDateExternal.text="Recipient Name";

        this.view.transfermain.lblSortDescription.text = "Bank Name";
        this.view.transfermain.lblSortDescriptionExternal.text="Account Number";
        this.view.transfermain.lblSortType.text = "Amount";
        this.view.transfermain.flxSortExternal.setVisibility(true);
        this.view.transfermain.flxSort.setVisibility(false);
        this.view.forceLayout();
        var externalAccountDataMap = {
          "btnBankDetails": "btnBankDetails",
          "btnDelete": "btnDelete",
          "btnEdit": "btnEdit",
          "btnViewActivity": "btnViewActivity",
          "btnAction": "btnAction",
          "btnCancel": "btnCancel",
          "btnTransferFrom": "btnTransferFrom",
          "btnMakeTransfer": "btnMakeTransfer",
          "btnSave": "btnSave",
          "flxDropdown": "flxDropdown",
          "imgDropdown": "imgDropdown",
          "lblAccountHolderTitle": "lblAccountHolderTitle",
          "lblAccountHolderValue": "lblAccountHolderValue",
          "lblAccountName": "lblAccountName",
          "lblAccountNumberTitle": "lblAccountNumberTitle",
          "lblAccountNumberTitleUpper": "lblAccountNumberTitleUpper",
          "lblAccountNameUpper": "lblAccountNameUpper",
          "lblAccountNumberValue": "lblAccountNumberValue",
          "lblAccountTypeTitle": "lblAccountTypeTitle",
          "lblAccountTypeValue": "lblAccountTypeValue",
          "lblAddedOnTitle": "lblAddedOnTitle",
          "lblAddedOnValue": "lblAddedOnValue",
          "lblBankDetailsTitle": "lblBankDetailsTitle",
          "lblBankName": "lblBankName",
          "lblIdentifier": "lblIdentifier",
          "lblRoutingNumberTitle": "lblRoutingNumberTitle",
          "lblRoutingNumberValue": "lblRoutingNumberValue",
          "lblSeparator": "lblSeparator",
          "lblSeparatorActions": "lblSeparatorActions",
          "lblStatus": "lblStatus",
          "txtAccountName": "txtAccountName",
          "txtAccountNumber": "txtAccountNumber",
          "txtAccountType": "txtAccountType"
        };
        var len=viewModel.length;
        function getMappings(context){
          if(context.routingNumberDetails){
            context=context.routingNumberDetails;
            if(context.lblRoutingNumberTitle){
              if(context.lblRoutingNumberTitle.isInternationalAccount=="true"){
                return kony.i18n.getLocalizedString("i18n.accounts.swiftCode");
              }else{
                return kony.i18n.getLocalizedString("i18n.accounts.routingNumber");
              }
            }if(context.lblRoutingNumberValue){
              if(context.lblRoutingNumberValue.isInternationalAccount=="true"){
                return context.lblRoutingNumberValue.swiftCode;
              }else if(context.lblRoutingNumberValue.isSameBankAccount=="false"){
                return context.lblRoutingNumberValue.routingNumber;
              }else{
                return "NA";
              }
            }
          }
          if(context.accountStatus){
            if(context.accountStatus.isVerified=="true"){
              return kony.i18n.getLocalizedString("i18n.transfers.verified");
            }
            else{
              return kony.i18n.getLocalizedString("i18n.transfers.pending");
            }
          }
          if(context.viewActivity){
            if(CommonUtilities.getConfiguration("externalTransactionHistory")=='true'&&context.viewActivity.isVerified=="true"){
              return  {
                "text":kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                "onClick":function(){
                  kony.print("viewActivity clicked");
                  scopeObj.onBtnViewActivity();
                }
              }  
            }else{
              return {
                "isVisible": false
              }
            }  
          }
          if(context.btnActivityAccountStatus){
            if(context.btnActivityAccountStatus.isVerified=="true"){
              return {
                "text":kony.i18n.getLocalizedString("i18n.transfers.make_transfer"),
                "toolTip":kony.i18n.getLocalizedString("i18n.common.transferToThatPayee"),

                "onClick":function(){
                  scopeObj.onBtnMakeTransfer();
                }
              };
            }
            else{
              return {
                "text":kony.i18n.getLocalizedString("i18n.transfers.verifyAccount"),
                "toolTip":kony.i18n.getLocalizedString("i18n.transfers.verifyAccount"),

                "onClick":function(){
                  scopeObj.onBtnVerifyAccount();
                }
              };
            }
          }
          if (context.congigkeyPlug) {
            if (CommonUtilities.getConfiguration('addExternalAccount') == 'true') {
              return true;
            } else if (CommonUtilities.getConfiguration('addExternalAccount') == 'false' && context.congigkeyPlug.isVerified == "true") {
              return true;
            } else {
              return false;
            }
          }
          if(context.addedOn){
            var date=context.addedOn.slice(0,10);
            return date.slice(8,10)+"/"+date.slice(5,7)+"/"+date.slice(0,4);
          }
        }
        if(!hidePagination) {
          this.setExternalAccountsPagination(viewModel,len-1);          //new lOC                
        }

        var data = [];

        var accType;
        var type;

        for (var i = 0; i < len-1; i++) {
          if (viewModel[i].recipientType==="external"||viewModel[i].recipientType==="External") {
            accountTypeforRec="OTHER_EXTERNAL_ACCOUNT"
            External.push({
              "btnDelete": {
                "text": "VIEW ACTIVITY",
                "onClick": function() {
                  kony.print("external accounts DELETE pressed");
                  //scopeObj.onExternalAccountDelete("E");
                  scopeObj.onBtnViewActivity();
                }
              },
              "btnTransferFrom": {
                "text": "DELETE",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  scopeObj.onExternalAccountDelete("E");
                  //scopeObj.onBtnViewActivity();
                }
              },

              "btnAction": {
                "text": "DETAILS",

              },
              "imgDropdown": "arrow_down.png",
              "lblAccountHolderTitle": "Account Holder",
              "lblAccountHolderValue": "Individual Account",
              "lblAccountName": viewModel[i].accountName,
              "lblAccountNumberTitle": "Recipient NickName",
              "lblAccountNumberValue": viewModel[i].accountName,
              "lblAccountTypeTitle": "Account Type",
              "lblAccountTypeValue": viewModel[i].accountType,
              "lblAddedOnTitle": "Added On",
              "lblAddedOnValue": "",
              "lblBankDetailsTitle": "Bank Details",
              "lblAccountNameUpper":viewModel[i].accountName,
              "lblAccountNumberTitleUpper":viewModel[i].accountNumber,
              "lblBankName": viewModel[i].externalAccountId,
              "lblIdentifier": "lblIdentifier",
              "lblRoutingNumberTitle": "",    //new lOC
              "lblRoutingNumberValue":viewModel[i].id,    //new lOC
              "lblSeparator": viewModel[i].accountNumber,
              "lblSeparatorActions": "lblSeparatorActions",
              "lblStatus": viewModel[i].status,
              "template": "flxExternalAccountsTransfersUnselected",
              "txtAccountName": {
                "text": viewModel[i].accountNumber,
                "placeholder": ""
              },
              "txtAccountNumber": {
                "text": viewModel[i].accountNumber,
                "placeholder": ""
              },
              "txtAccountType": {
                "text": viewModel[i].accountType,
                "placeholder": ""
              }

            });
          }
          else{
            accType=viewModel[i].accountType;
            type = this.changeAccType(accType);

            accountTypeforRec="OWN_INTERNAL_ACCOUNT";
            Internal.push({
              "btnDelete": {
                "text": "VIEW ACTIVITY",
                "onClick": function() {
                  kony.print("external accounts DELETE pressed");
                  //scopeObj.onExternalAccountDelete("E");
                  scopeObj.onBtnViewActivity();
                }
              },

              "btnViewActivity": {
                "text": "MAKE TRANSFER",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  scopeObj.updateMakeTransferForm(forRecipients,"", accountTypeforRec);
                }
              },

              "btnAction": {
                "text": "DETAILS",

              },

              "btnTransferFrom": {
                "text": "DELETE",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  scopeObj.onExternalAccountDelete("I");
                  //scopeObj.onBtnViewActivity();
                }
              },


              "imgDropdown": "arrow_down.png",
              "lblAccountHolderTitle": "Account Holder",
              "lblAccountHolderValue": viewModel[i].bankName,
              "lblAccountName": viewModel[i].accountName,
              "lblAccountNameUpper": viewModel[i].accountName,
              "lblAccountNumberTitle": "Recipient's NickName",
              "lblAccountNumberValue": viewModel[i].accountName,
              "lblAccountNumberTitleUpper": viewModel[i].accountNumber,
              "lblAccountTypeTitle": "Account Type",
              "lblAccountTypeValue": type,
              //"lblAccountTypeValue": accountTypeValue,
              "lblAddedOnTitle": "Added On",
              "lblAddedOnValue": "add",
              "lblBankDetailsTitle": "Bank Details",
              //"lblBankName": viewModel[i].bankName,
              "lblBankName": viewModel[i].accountNumber,
              "lblIdentifier": viewModel[i].accountNumber,
              //"lblIdentifier": accountTypeValue,
              "lblRoutingNumberTitle": "",
              "lblRoutingNumberValue": viewModel[i].id,
              "lblSeparator": viewModel[i].accountName,
              "lblSeparatorActions": "lblSeparatorActions",
              //"lblStatus": getMappings({accountStatus:viewModel[i]}),
              "lblStatus": viewModel[i].accountType,
              "template": "flxExternalAccountsTransfersUnselected",
              "txtAccountName": {
                "text": viewModel[i].accountName,
                "placeholder": ""
              },
              "txtAccountNumber": {
                "text": viewModel[i].accountName,
                "placeholder": ""
              },
              "txtAccountType": {
                "text": type,
                "placeholder": ""
              }
            });

          }
        }

        var data = [];
        var finalExtSeg = [];
        for (var i = 0; i < External.length; i++) {
          if (External[i].lblStatus === "Active" || External[i].lblStatus === "active") {
            newArray1.push({
              "btnDelete": {
                "text": "VIEW ACTIVITY",
                "onClick": function() {
                  kony.print("external accounts DELETE pressed");
                  //scopeObj.onExternalAccountDelete("E");
                  scopeObj.onBtnViewActivity();
                }
              },
              "btnAction": {
                "text": "DETAILS",
              },
              "btnViewActivity": {
                "text": "TRANSFER FROM",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  //scopeObj.updateMakeTransferFormForRecipients(forRecipients,fromAccountNumber)
                  scopeObj.calIndValForRecipients(scopeObj, accountTypeforRec);
                }
              },
              "btnTransferFrom": {
                "text": "DELETE",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  scopeObj.onExternalAccountDelete("E");
                  //scopeObj.onBtnViewActivity();
                }
              },
              "btnEdit": {
                "text": "TRANSFER TO",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  //scopeObj.updateMakeTransferFormForRecipients(forRecipients,fromAccountNumber);
                  scopeObj.calTransferToRecipients(scopeObj, accountTypeforRec,viewModel);
                }
              },
              "imgDropdown": "arrow_down.png",
              "lblAccountHolderTitle": "Account Holder",
              "lblAccountHolderValue": "Individual Account",
              "lblAccountName": External[i].lblAccountName,
              "lblAccountNumberTitle": "Recipient NickName",
              "lblAccountNumberValue": External[i].lblAccountNumberValue,
              "lblAccountTypeTitle": "Account Type",
              "lblAccountTypeValue": External[i].lblAccountTypeValue,
              "lblAddedOnTitle": {
                "text": "Activated",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  //scopeObj.calculateIndex();
                  //scopeObj.onBtnViewActivity();
                }
              },
              "lblAddedOnValue": "",
              "lblBankDetailsTitle": "Bank Details",
              "lblAccountNameUpper": External[i].lblAccountName,
              "lblAccountNumberTitleUpper": External[i].lblAccountNumberTitleUpper,
              "lblBankName": {
                skin: "sknLblLato42424215pxBold",
                text: External[i].lblBankName
              },
              "lblIdentifier": "lblIdentifier",
              "lblRoutingNumberTitle": "", //new lOC
              "lblRoutingNumberValue": "", //new lOC
              "lblSeparator": External[i].lblAccountNumberTitleUpper,
              "lblSeparatorActions": "lblSeparatorActions",
              "lblStatus": "",
              "template": "flxExternalAccountsTransfersUnselected",
              "txtAccountName": {
                "text": External[i].lblAccountNumberTitleUpper,
                "placeholder": ""
              },
              "txtAccountNumber": {
                "text": External[i].lblAccountNumberTitleUpper,
                "placeholder": ""
              },
              "txtAccountType": {
                "text": External[i].lblAccountTypeValue,
                "placeholder": ""
              }
            });
          } else if (External[i].lblStatus === "Pending Activation") {
            newArray2.push({
              "btnDelete": {
                "text": "VIEW ACTIVITY",
                "onClick": function() {
                  kony.print("external accounts DELETE pressed");
                  //scopeObj.onExternalAccountDelete("E");
                  scopeObj.onBtnViewActivity();
                }
              },
              "btnAction": {
                "text": "DETAILS",
              },
              "btnTransferFrom": {
                "text": "DELETE",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  scopeObj.onExternalAccountDelete("E");
                  //scopeObj.onBtnViewActivity();
                }
              },
              "imgDropdown": "arrow_down.png",
              "lblAccountHolderTitle": "Account Holder",
              "lblAccountHolderValue": "Individual Account",
              "lblAccountName": External[i].lblAccountName,
              "lblAccountNumberTitle": "Recipient NickName",
              "lblAccountNumberValue": External[i].lblAccountNumberValue,
              "lblAccountTypeTitle": "Account Type",
              "lblAccountTypeValue": External[i].lblAccountTypeValue,
              "lblAddedOnTitle": {
                "text": "VERIFY",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  scopeObj.calculateIndex();
                  //scopeObj.onBtnViewActivity();
                }
              },
              "lblAddedOnValue": "",
              "lblBankDetailsTitle": "Bank Details",
              "lblAccountNameUpper": External[i].lblAccountName,
              "lblAccountNumberTitleUpper": External[i].lblAccountNumberTitleUpper,
              "lblBankName": {
                skin: "sknLblLato42424215pxBold",
                text: External[i].lblBankName
              },
              "lblIdentifier": "lblIdentifier",
              "lblRoutingNumberTitle": "", //new lOC
              "lblRoutingNumberValue": "", //new lOC
              "lblSeparator": External[i].accountNumber,
              "lblSeparatorActions": "lblSeparatorActions",
              "lblStatus": "",
              "template": "flxExternalAccountsTransfersUnselected",
              "txtAccountName": {
                "text": External[i].lblAccountNumberValue,
                "placeholder": ""
              },
              "txtAccountNumber": {
                "text": External[i].lblAccountNumberValue,
                "placeholder": ""
              },
              "txtAccountType": {
                "text": External[i].lblAccountTypeValue,
                "placeholder": ""
              }
            });
          }
          else{
            newArray3.push({
              "btnDelete": {
                "text": "VIEW ACTIVITY",
                "onClick": function() {
                  kony.print("external accounts DELETE pressed");
                  //scopeObj.onExternalAccountDelete("E");
                  scopeObj.onBtnViewActivity();
                }
              },
              "btnAction": {
                "text": "DETAILS",
              },
              "btnTransferFrom": {
                "text": "DELETE",
                "onClick": function() {
                  kony.print("viewActivity clicked");
                  scopeObj.onExternalAccountDelete("E");
                  //scopeObj.onBtnViewActivity();
                }
              },
              "imgDropdown": "arrow_down.png",
              "lblAccountHolderTitle": "Account Holder",
              "lblAccountHolderValue": "Individual Account",
              "lblAccountName": External[i].lblAccountName,
              "lblAccountNumberTitle": "Recipient NickName",
              "lblAccountNumberValue": External[i].lblAccountNumberValue,
              "lblAccountTypeTitle": "Account Type",
              "lblAccountTypeValue": External[i].lblAccountTypeValue,
              "lblAddedOnTitle": {
                "text": "In Process",

              },
              "lblAddedOnValue": "",
              "lblBankDetailsTitle": "Bank Details",
              "lblAccountNameUpper": External[i].lblAccountName,
              "lblAccountNumberTitleUpper": External[i].lblAccountNumberTitleUpper,
              "lblBankName": {
                skin: "sknLblLato42424215pxBold",
                text: External[i].lblBankName
              },
              "lblIdentifier": "lblIdentifier",
              "lblRoutingNumberTitle": "", //new lOC
              "lblRoutingNumberValue": "", //new lOC
              "lblSeparator": External[i].accountNumber,
              "lblSeparatorActions": "lblSeparatorActions",
              "lblStatus": "",
              "template": "flxExternalAccountsTransfersUnselected",
              "txtAccountName": {
                "text": External[i].lblAccountNumberValue,
                "placeholder": ""
              },
              "txtAccountNumber": {
                "text": External[i].lblAccountNumberValue,
                "placeholder": ""
              },
              "txtAccountType": {
                "text": External[i].lblAccountTypeValue,
                "placeholder": ""
              }
            });

          }
        }

        finalExtSeg=newArray1.concat(newArray2);
        var processArray=finalExtSeg.concat(newArray3);
        //External=External.concat(moreOptions);
        data = Internal.concat(processArray);
        this.view.transfermain.segmentTransfers.widgetDataMap = externalAccountDataMap;
        this.view.transfermain.segmentTransfers.setData(data);
        CommonUtilities.Sorting.updateSortFlex(this.externalAccountsSortMap, viewModel[len-1]);
      }
    },
    ///for Recipients TransferTo 
    calTransferToRecipients : function(scopeObj,accountTypeforRec,toViewModel){

      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      var rowData=data[index];
      var externalTransferOption = toViewModel[index].externalTransferOption;
      var selectedRowData = {
        "externalTransferOption" : externalTransferOption,
        "check" : true
      }
      var toAccountNumber=rowData.lblBankName.text;
      var accType=accountTypeforRec;
      forRecipients.accFromKey="";
      forRecipients.accToKey="";
      scopeObj.updateMakeTransferForm(forRecipients,toAccountNumber,accType,selectedRowData);
    },



    ////// for recipients calculating index value 

    calIndValForRecipients : function(scopeObj,accountTypeforRec){
      var index = this.view.transfermain.segmentTransfers.selectedIndex[1];
      var data = this.view.transfermain.segmentTransfers.data;
      var rowData=data[index];
      var fromAccountNumber=rowData.lblBankName.text;
      var accType=accountTypeforRec;
      scopeObj.updateMakeTransferForm(forRecipients,fromAccountNumber,accType);
    },


    showSegment: function () {
      this.hideAll();
      this.view.transfermain.flxSort.setVisibility(true);
      this.view.transfermain.segmentTransfers.setVisibility(true);
      this.view.transfermain.tablePagination.setVisibility(true);/////newly added
      this.view.forceLayout();
    },
    onSelectionLoanPayment : function(makeTransferViewModel){
      var scopeObj = this;
      var selpaymntLoanBtn = scopeObj.view.transfermain.maketransfer.RbNowLoan.selectedKey;
      if(selpaymntLoanBtn === "Now" || selpaymntLoanBtn === "Payment Date"){
        scopeObj.view.transfermain.maketransfer.flxOptions2.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblSendOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxCalSendOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblForhowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxForHowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOff.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.tbxNoOfRecurrences.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxStartDate1.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblStart.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxFrequency.selectedKey = "Now";
        scopeObj.onFrequencyChanged();
      }
      else{
        scopeObj.view.transfermain.maketransfer.flxOptions2.setVisibility(true);
        scopeObj.onSelectionMotgagePayment(makeTransferViewModel);
      }
    },

    onClickIndefinetly: function(makeTransferViewModel){
      var scopeObj = this;
      var left = scopeObj.view.transfermain.maketransfer.flexswitchOff.left;
      if (left === "6.4%") {
        makeTransferViewModel.indefinetly = false;
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.flexswitchOff.left = "3.77%";
        scopeObj.view.transfermain.maketransfer.flexswitchOn.skin = "slFboxSwitchGreyOff";
      } else {
        makeTransferViewModel.indefinetly = true;
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOff.left = "6.4%";
        scopeObj.view.transfermain.maketransfer.flexswitchOn.skin = "slFboxSwitchOn";
      }     
    },

    // ONE-TIME Tranfer Option Calender Future Day Selection
    onFutureDateOneTime : function(){
      var scopeObj = this;        
      scopeObj.view.transfermain.maketransfer.flxCalSendOn.calSendOn.clear();
      var current_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);   
      var day = current_date.getDate();
      var month = current_date.getMonth()+1;
      var year = current_date.getFullYear();     
      scopeObj.view.transfermain.maketransfer.flxCalSendOn.calSendOn.validStartDate = [day,month,year];
      scopeObj.view.transfermain.maketransfer.flxCalSendOn.calSendOn.dateComponents = [day,month,year,0,0,0];

    },

    onSelectionMotgagePayment : function(makeTransferViewModel){
      var scopeObj = this;
      var selpaymntMotgageBtn = scopeObj.view.transfermain.maketransfer.RbOneTimeLoan.selectedKey;
      if(selpaymntMotgageBtn === "One Time"){
        scopeObj.onFutureDateOneTime();
        scopeObj.view.transfermain.maketransfer.lblSendOn.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.flxCalSendOn.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblForhowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxForHowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOff.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.tbxNoOfRecurrences.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxStartDate1.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblStart.setVisibility(false);
      }
      else{
        if (selpaymntMotgageBtn == "Recurring") {
          /*var temp = [];
                    for (var key in makeTransferViewModel.frequencies) {
                        temp.push(key);
                    }
                    for (var i = 2; i < temp.length; i++) {
                        this.view.transfermain.maketransfer.lbxFrequency.masterData = temp[i];
                        scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(true);
                        scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(true);
                    }*/

          var list = [];
          var count = 0;
          for (var key in this.presenter.getFrequencies()) {
            if (makeTransferViewModel.frequencies.hasOwnProperty(key)) {
              if(makeTransferViewModel.frequencies[key][0] !== "Now" && makeTransferViewModel.frequencies[key][0] !== "Once") {
                list.push([makeTransferViewModel.frequencies[key][1], makeTransferViewModel.frequencies[key][1]]);
                count++;
              }
            }
          }
          this.view.transfermain.maketransfer.lbxFrequency.masterData = list;
          this.view.transfermain.maketransfer.lbxFrequency.selectedKey = "Weekly";
          scopeObj.onFrequencyChanged();
          scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(true);
          scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(true);
          scopeObj.view.transfermain.maketransfer.lbxForHowLong.setEnabled(false);
          scopeObj.view.transfermain.maketransfer.lbxForHowLong.selectedKey = "NO_OF_RECURRENCES";
          scopeObj.onHowLongChange();
        }
        // scopeObj.view.transfermain.maketransfer.flxlblFrequency.setVisibility(true);
        /* scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(true);
                scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(true);
 */ // this.onFrequencyChanged();
      }
    },

    onSelectionPaymentDue : function(makeTransferViewModel){
      var scopeObj = this;
      var selpaymntDueBtn = scopeObj.view.transfermain.maketransfer.RbPaymentAmount.selectedKey;
      if(selpaymntDueBtn === "Payment Amount"){
        scopeObj.view.transfermain.maketransfer.txtBxOtherAmount.setEnabled(false);
        makeTransferViewModel.otherPaymentAmount = true;
      }
      else{
        scopeObj.view.transfermain.maketransfer.txtBxOtherAmount.setEnabled(true);
        makeTransferViewModel.otherPaymentAmount = false;
      }
    },

    onSelectionCCPayment : function(makeTransferViewModel){
      var scopeObj = this;
      var selpaymntCCBtn = scopeObj.view.transfermain.maketransfer.RbNow.selectedKey;
      if(selpaymntCCBtn === "Now" || selpaymntCCBtn === "Payment Date"){
        //scopeObj.view.transfermain.maketransfer.flxOptions2.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblSendOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxCalSendOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblForhowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxForHowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOff.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.tbxNoOfRecurrences.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxStartDate1.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblStart.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxFrequency.selectedKey = "Now";
        scopeObj.onFrequencyChanged();
      }
      else{

        scopeObj.view.transfermain.maketransfer.lblSendOn.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.flxCalSendOn.setVisibility(true);
        scopeObj.view.transfermain.maketransfer.lblFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxFrequency.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblForhowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lbxForHowLong.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOff.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flexswitchOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxCalEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblNoOfRecOrEndingOn.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.tbxNoOfRecurrences.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.flxStartDate1.setVisibility(false);
        scopeObj.view.transfermain.maketransfer.lblStart.setVisibility(false);
        //scopeObj.view.transfermain.maketransfer.flxOptions2.setVisibility(true);
        //scopeObj.onSelectionMotgagePayment(makeTransferViewModel);
      }

    },

    onSelectionCCPaymentDue : function(makeTransferViewModel){
      var scopeObj = this;
      var selpaymntCCDueBtn = scopeObj.view.transfermain.maketransfer.RbCurrentBalance.selectedKey;
      if(selpaymntCCDueBtn === "Current Balance"){
        scopeObj.view.transfermain.maketransfer.txtOtherAmount.setEnabled(false);
        makeTransferViewModel.amount = scopeObj.view.transfermain.maketransfer.lblCurrentBalance.text;
        makeTransferViewModel.enteredccAmount = false;
      } else if(selpaymntCCDueBtn === "Statement Balance"){
        scopeObj.view.transfermain.maketransfer.txtOtherAmount.setEnabled(false);
        makeTransferViewModel.amount = scopeObj.view.transfermain.maketransfer.lblStatementBalance.text;
        makeTransferViewModel.enteredccAmount = false;
      } else if(selpaymntCCDueBtn === "Minimum Due Balance"){
        scopeObj.view.transfermain.maketransfer.txtOtherAmount.setEnabled(false);
        makeTransferViewModel.amount = scopeObj.view.transfermain.maketransfer.lblMinimumBalance.text;
        makeTransferViewModel.enteredccAmount = false;
      }
      else{
        scopeObj.view.transfermain.maketransfer.txtOtherAmount.setEnabled(true);
        makeTransferViewModel.enteredccAmount = true;
      }
    },
    checkNullAndGetEmptyText : function(inputstring){
      var newinputstring = inputstring;
      if(inputstring !== null && inputstring !== "null" && inputstring !== undefined && inputstring !== "undefined"){
        newinputstring= inputstring;
      } else {
        newinputstring = "";
      }
      return newinputstring;
    },
   displayExternalAccountsSeg: function(fromaccountsobj, makeTransferViewModel, recipientAcctID) {
      var scopeObj = this;
      var myAccountsMainSegData = [];
      var myAccountsrowData = [];
      var accountNumhdval = "";
      var mainaccountNumhdval = "";
      var accountfromselval = "";
      var selSecHeader = "";
      var selProuductID = "";
      //var externalaccounts = CommonUtilitiesOCCU.getExternalOccunts();
      var fromaccounts = fromaccountsobj;
      if (fromaccounts !== null && fromaccounts.length > 0) {
        var count = 0;
        for (var i = 0; i < fromaccounts.length; i++) {
          var account = fromaccounts[i];
          var productCode = this.checkNullAndGetEmptyText(account.productCode);
          var accountID = this.checkNullAndGetEmptyText(account.accountID);
          if (account.external === true || account.external === "true") {
            accountID = this.checkNullAndGetEmptyText(account.externalAccountId);
          }
          var accountName = this.checkNullAndGetEmptyText(account.accountName);
          var shareID = this.checkNullAndGetEmptyText(account.shareId);
          var availablebal = this.checkNullAndGetEmptyText(account.availableBalance);
          var bankName = this.checkNullAndGetEmptyText(account.bankName);
          var accountobj = {
            "lblUsers": {
              "text": accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " " + shareID : "") + ")",
              "toolTip": accountName + (productCode !== "" ? " | " + productCode : "") + (shareID !== "" ? " " + shareID : "") + (availablebal !== "" ? "  | Available Balance: " + CommonUtilities.formatCurrencyWithCommas(account.availableBalance) : "")
            },
            "lblBalance":{
                          "text":CommonUtilities.formatCurrencyWithCommas(account.availableBalance),
                          "toolTip":CommonUtilities.formatCurrencyWithCommas(account.availableBalance)      //// avail balance
                        },
            "lblSeparator": "Separator",
            "flxAccountTypesTransfers": {
              "onClick": account.onAccountSelection
            },
            "FromaccountID": accountID,
            "FromProductID": shareID,
            "FromProductType": productCode,
            "FromExistingBalance": availablebal,
            "FromIsExternal": account.external,
            "FromRegD": account.regD,
            "FromExternalAccountId": account.externalAccountId,
            "accountdisplay": accountName + (productCode !== "" ? " | " + account.productCode : "") + (shareID !== "" ? " " + shareID : "") + (bankName !== "" ? "  |  " + bankName : "") + (accountID !== "" ? " | " + ' X' + this.getLastFourDigit(accountID) : "") + (availablebal !== "" ? "  | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "")
          }
          var tempaccountheading = account.bankName + " | " + accountID;
          if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
            var accountHeader = {
              "lblHeader": mainaccountNumhdval
            };
            myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
            myAccountsrowData = [];
            myAccountsrowData.push(accountobj);
          } else {
            myAccountsrowData.push(accountobj);
          }
          accountNumhdval = account.bankName + " | " + accountID;
          mainaccountNumhdval = account.bankName + " | " + ' X' + this.getLastFourDigit(accountID);
          if (count === 0 && makeTransferViewModel.accFromKey === "") {
            //accountfromselval = accountName + (productCode !== "" ? " | " + account.productCode : "") + (shareID !== "" ? " " + shareID : "") + (bankName !== "" ? "  |  " + bankName : "") + (accountID !== "" ? " | " + ' X' + this.getLastFourDigit(accountID) : "") + (availablebal !== "" ? "  | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "");
            accountfromselval =accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " " + shareID : "") + ")";
            selSecHeader = mainaccountNumhdval;
            selProuductID = account.shareId;
            makeTransferViewModel.accFromKey = accountfromselval;
            makeTransferViewModel.FromaccountID = accountID;
            makeTransferViewModel.FromProductID = account.shareId;
            makeTransferViewModel.FromProductType = account.productCode;
            makeTransferViewModel.FromExistingBalance = account.availableBalance;
            makeTransferViewModel.FromExistingBalance = account.availableBalance;
            makeTransferViewModel.FromIsExternal = account.external;
            makeTransferViewModel.FromExternalAccountId = account.externalAccountId;
            makeTransferViewModel.secHeader = mainaccountNumhdval;
            this.view.transfermain.maketransfer.lblRegDLimit.text = "";
            makeTransferViewModel.accToKey = "";
          }
          if (recipientAcctID !== null && recipientAcctID !== undefined && (recipientAcctID === accountID || recipientAcctID === account.externalAccountId)) {
            //accountfromselval = accountName + (productCode !== "" ? " | " + account.productCode : "") + (shareID !== "" ? " " + shareID : "") + (bankName !== "" ? "  |  " + bankName : "") + (accountID !== "" ? " | " + ' X' + this.getLastFourDigit(accountID) : "") + (availablebal !== "" ? "  | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "");
             accountfromselval =accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " " + shareID : "") + ")";
            selSecHeader = mainaccountNumhdval;
            selProuductID = account.shareId;
            makeTransferViewModel.accFromKey = accountfromselval;
            makeTransferViewModel.FromaccountID = accountID;
            makeTransferViewModel.FromProductID = account.shareId;
            makeTransferViewModel.FromProductType = account.productCode;
            makeTransferViewModel.FromExistingBalance = account.availableBalance;
            makeTransferViewModel.FromExistingBalance = account.availableBalance;
            makeTransferViewModel.FromIsExternal = account.external;
            makeTransferViewModel.FromExternalAccountId = account.externalAccountId;
            makeTransferViewModel.secHeader = mainaccountNumhdval;
            makeTransferViewModel.accToKey = "";
          }
          if (makeTransferViewModel.accountFromKey !== null && makeTransferViewModel.accountFromKey !== undefined && (makeTransferViewModel.accountFromKey === accountID || makeTransferViewModel.accountFromKey === account.externalAccountId)) {
            //accountfromselval = accountName + (productCode !== "" ? " | " + account.productCode : "") + (shareID !== "" ? " " + shareID : "") + (bankName !== "" ? "  |  " + bankName : "") + (accountID !== "" ? " | " + ' X' + this.getLastFourDigit(accountID) : "") + (availablebal !== "" ? "  | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "");
             accountfromselval =accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " " + shareID : "") + ")";
            selSecHeader = mainaccountNumhdval;
            selProuductID = account.shareId;
            makeTransferViewModel.accFromKey = accountfromselval;
            makeTransferViewModel.FromaccountID = accountID;
            makeTransferViewModel.FromProductID = account.shareId;
            makeTransferViewModel.FromProductType = account.productCode;
            makeTransferViewModel.FromExistingBalance = account.availableBalance;
            makeTransferViewModel.FromExistingBalance = account.availableBalance;
            makeTransferViewModel.FromIsExternal = account.external;
            makeTransferViewModel.FromExternalAccountId = account.externalAccountId;
            makeTransferViewModel.secHeader = mainaccountNumhdval;
            makeTransferViewModel.accToKey = "";
          }
          count++;                
          if (i === fromaccounts.length - 1) {
            var accountfinHeader = {
              "lblHeader": mainaccountNumhdval
            };
            myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
            kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
          }
        }
      }
      this.view.transfermain.maketransfer.accountslist.segAccountTypes.setData(myAccountsMainSegData);
      this.view.transfermain.maketransfer.accountslist.FlexAccountname.lblFromAccountName.text = makeTransferViewModel.accFromKey;
      this.view.transfermain.maketransfer.accountslist.FlexAccountname.lblFromBalance.text = "$"+ makeTransferViewModel.FromExistingBalance;
      this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.setData([]);
      this.displayToExternalAccountsSeg(fromaccountsobj, makeTransferViewModel.secHeader, makeTransferViewModel, makeTransferViewModel.FromProductID);
      this.view.transfermain.maketransfer.flxAmount.setVisibility(true);
      this.view.transfermain.maketransfer.lblAmount.setVisibility(true);
      this.view.transfermain.maketransfer.lblFrequency.setVisibility(true);
      this.view.transfermain.maketransfer.lbxFrequency.setVisibility(true);
      this.view.transfermain.maketransfer.flexNewRecipient.setVisibility(false);
      this.view.transfermain.maketransfer.flxWhenLoan.setVisibility(false);
      this.view.transfermain.maketransfer.flxMainCreditCard.setVisibility(false);
    },         
    displayToExternalAccountsSeg: function(fromaccountsobj, selAccountNumber, makeTransferViewModel, selProuductID) {
      var scopeObj = this;
      var myAccountsMainSegData = [];
      var myAccountsrowData = [];
      var accountNumhdval = "";
      var mainaccountNumhdval = "";
      var accountdisplaytext = "";
      var regDCount = "";
      //var externalaccounts = CommonUtilitiesOCCU.getExternalOccunts();
      var fromaccounts = fromaccountsobj;
      var count = 0;
      if (fromaccounts !== null && fromaccounts.length > 0) {                                                               
        for (var i = 0; i < fromaccounts.length; i++) {
          var account = fromaccounts[i];
          var productCode = this.checkNullAndGetEmptyText(account.productCode);
          var accountID = this.checkNullAndGetEmptyText(account.accountID);
          var accountName = this.checkNullAndGetEmptyText(account.accountName);
          var shareID = this.checkNullAndGetEmptyText(account.shareId);
          var availablebal = this.checkNullAndGetEmptyText(account.availableBalance);
          var bankName = this.checkNullAndGetEmptyText(account.bankName);                          
          if(makeTransferViewModel.FromIsExternal !== "" && makeTransferViewModel.FromIsExternal === account.external){
            kony.print("Same extrernal account type");
          } else {               
            if(account.external === true || account.external === "true"){
              accountID = this.checkNullAndGetEmptyText(account.externalAccountId);
            }
            var toaccountNameOwner = account.bankName;                                                                                                                                
            var accountdisplaytxt = "";
            var accountdisplaysegtxt = "";
            if(account.availableBalance !== null && account.availableBalance !== null && account.availableBalance !== "null"){
              accountdisplaytxt = CommonUtilities.formatCurrencyWithCommas(availablebal);
              accountdisplaysegtxt = CommonUtilities.formatCurrencyWithCommas(availablebal);
            }
            if(account.productCode !== "S"){
              accountdisplaytxt = "";
              accountdisplaysegtxt= "";
            }
            var accountobj = {
              "lblUsers": {
                "text": accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " "+shareID : ""),
                "toolTip": accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " "+shareID : "")
              },
              "lblBalance":{
                          "text":accountdisplaysegtxt,
                          "toolTip":accountdisplaysegtxt      //// avail balance
                        },
              "lblSeparator": "Separator",
              "flxAccountTypesTransfers": {
                "onClick": account.onAccountSelection
              },
              "ToaccountID":accountID,
              "ToProductID": shareID,
              "ToProductType": productCode,
              "ToAccountName": bankName,
              "ToIsExternal": account.external,
              "ToExternalAccountId": account.externalAccountId,
              "accountdisplay": accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " "+shareID : "")
            }
            var tempaccountheading = toaccountNameOwner + " | " + accountID;
            if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
              var accountHeader = {
                "lblHeader": mainaccountNumhdval
              };
              myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
              myAccountsrowData = [];
              myAccountsrowData.push(accountobj);
            } else {
              myAccountsrowData.push(accountobj);
            }
            accountNumhdval = toaccountNameOwner + " | " + accountID;
            mainaccountNumhdval = toaccountNameOwner + " | " + ' X' + this.getLastFourDigit(accountID);
            if (count === 0 && makeTransferViewModel.accToKey === "") {
              accountdisplaytext =  accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " "+shareID : "");
              makeTransferViewModel.accToKey = accountdisplaytext;
              makeTransferViewModel.ToaccountID = accountID;
              makeTransferViewModel.ToProductID = account.shareId;
              makeTransferViewModel.ToProductType = account.productCode;
              makeTransferViewModel.ToIsExternal = account.external;
              makeTransferViewModel.ToExternalAccountId = account.externalAccountId;
              makeTransferViewModel.toBalance=accountdisplaysegtxt;
            } 
            if (makeTransferViewModel.accountToKey !== null && makeTransferViewModel.accountToKey !== undefined && (makeTransferViewModel.accountToKey === accountID || makeTransferViewModel.accountToKey === account.externalAccountId)) {
              accountdisplaytext =  accountName + (productCode !== "" ? " ( " + productCode : "") + (shareID !== "" ? " "+shareID : "");
              makeTransferViewModel.accToKey = accountdisplaytext;
              makeTransferViewModel.ToaccountID = accountID;
              makeTransferViewModel.ToProductID = account.shareId;
              makeTransferViewModel.ToProductType = account.productCode;
              makeTransferViewModel.ToIsExternal = account.external;
              makeTransferViewModel.ToExternalAccountId = account.externalAccountId;
              makeTransferViewModel.toBalance=accountdisplaysegtxt;
            } 
            count++;
          }

          if (i === fromaccounts.length - 1) {
            var accountfinHeader = {
              "lblHeader": mainaccountNumhdval
            };
            myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
            kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
          }

        }
      }
      this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.removeAll();
      this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.setData(myAccountsMainSegData);
      this.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromAccountName.text = makeTransferViewModel.accToKey;
      this.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromBalance.text = makeTransferViewModel.toBalance;
    },
        displayExternalAccountsSegFromToAccont: function(fromaccountsobj, makeTransferViewModel, recipientAcctID,externalTransferOption) {
            var scopeObj = this;
            var myAccountsMainSegData = [];
            var myAccountsrowData = [];
            var accountNumhdval = "";
            var mainaccountNumhdval = "";
            var accountfromselval = "";
            var selSecHeader = "";
            var selProuductID = "";
            //var externalaccounts = CommonUtilitiesOCCU.getExternalOccunts();
            var fromaccounts = fromaccountsobj;
            if (fromaccounts !== null && fromaccounts.length > 0) {
                var count = 0;
                for (var i = 0; i < fromaccounts.length; i++) {
                    var account = fromaccounts[i];
                    var productCode = this.checkNullAndGetEmptyText(account.productCode);
                    var accountID = this.checkNullAndGetEmptyText(account.accountID);
                                                                                if (((externalTransferOption == true) && (account.external === false || account.external === "false")) || ((externalTransferOption == false) && (account.external === true || account.external === "true"))) {
                   
                        accountID = this.checkNullAndGetEmptyText(account.externalAccountId);
                    
                    var accountName = this.checkNullAndGetEmptyText(account.accountName);
                    var shareID = this.checkNullAndGetEmptyText(account.shareId);
                    var availablebal = this.checkNullAndGetEmptyText(account.availableBalance);
                    var bankName = this.checkNullAndGetEmptyText(account.bankName);
                    var accountobj = {
                        "lblUsers": {
                            "text": accountName + (productCode !== "" ? " | " + productCode : "") + (shareID !== "" ? " " + shareID : "") + (availablebal !== "" ? "  | Available Balance: " + CommonUtilities.formatCurrencyWithCommas(account.availableBalance) : ""),
                            "toolTip": accountName + (productCode !== "" ? " | " + productCode : "") + (shareID !== "" ? " " + shareID : "") + (availablebal !== "" ? "  | Available Balance: " + CommonUtilities.formatCurrencyWithCommas(account.availableBalance) : "")
                        },
                        "lblSeparator": "Separator",
                        "flxAccountTypesTransfers": {
                            "onClick": account.onAccountSelection
                        },
                        "FromaccountID": accountID,
                        "FromProductID": shareID,
                        "FromProductType": productCode,
                        "FromExistingBalance": availablebal,
                        "FromIsExternal": account.external,
                        "FromRegD": account.regD,
                        "FromExternalAccountId": account.externalAccountId,
                        "accountdisplay": accountName + (productCode !== "" ? " | " + account.productCode : "") + (shareID !== "" ? " " + shareID : "") + (bankName !== "" ? "  |  " + bankName : "") + (accountID !== "" ? " | " + ' X' + this.getLastFourDigit(accountID) : "") + (availablebal !== "" ? "  | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "")
                    }
                    var tempaccountheading = account.bankName + " | " + accountID;
                    if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
                        var accountHeader = {
                            "lblHeader": mainaccountNumhdval
                        };
                        myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
                        myAccountsrowData = [];
                        myAccountsrowData.push(accountobj);
                    } else {
                        myAccountsrowData.push(accountobj);
                    }
                    accountNumhdval = account.bankName + " | " + accountID;
                    mainaccountNumhdval = account.bankName + " | " + ' X' + this.getLastFourDigit(accountID);
                    if (count === 0 && makeTransferViewModel.accFromKey === "") {
                        accountfromselval = accountName + (productCode !== "" ? " | " + account.productCode : "") + (shareID !== "" ? " " + shareID : "") + (bankName !== "" ? "  |  " + bankName : "") + (accountID !== "" ? " | " + ' X' + this.getLastFourDigit(accountID) : "") + (availablebal !== "" ? "  | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "");
                        selSecHeader = mainaccountNumhdval;
                        selProuductID = account.shareId;
                        makeTransferViewModel.accFromKey = accountfromselval;
                        makeTransferViewModel.FromaccountID = accountID;
                        makeTransferViewModel.FromProductID = account.shareId;
                        makeTransferViewModel.FromProductType = account.productCode;
                        makeTransferViewModel.FromExistingBalance = account.availableBalance;
                        makeTransferViewModel.FromExistingBalance = account.availableBalance;
                        makeTransferViewModel.FromIsExternal = account.external;
                        makeTransferViewModel.FromExternalAccountId = account.externalAccountId;
                        makeTransferViewModel.secHeader = mainaccountNumhdval;
                        this.view.transfermain.maketransfer.lblRegDLimit.text = "";
                        makeTransferViewModel.accToKey = "";
                    }
                    if (recipientAcctID !== null && recipientAcctID !== undefined && (recipientAcctID === accountID || recipientAcctID === account.externalAccountId)) {
                        accountfromselval = accountName + (productCode !== "" ? " | " + account.productCode : "") + (shareID !== "" ? " " + shareID : "") + (bankName !== "" ? "  |  " + bankName : "") + (accountID !== "" ? " | " + ' X' + this.getLastFourDigit(accountID) : "") + (availablebal !== "" ? "  | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "");
                        selSecHeader = mainaccountNumhdval;
                        selProuductID = account.shareId;
                        makeTransferViewModel.accFromKey = accountfromselval;
                        makeTransferViewModel.FromaccountID = accountID;
                        makeTransferViewModel.FromProductID = account.shareId;
                        makeTransferViewModel.FromProductType = account.productCode;
                        makeTransferViewModel.FromExistingBalance = account.availableBalance;
                        makeTransferViewModel.FromExistingBalance = account.availableBalance;
                        makeTransferViewModel.FromIsExternal = account.external;
                        makeTransferViewModel.FromExternalAccountId = account.externalAccountId;
                        makeTransferViewModel.secHeader = mainaccountNumhdval;
                        makeTransferViewModel.accToKey = "";
                    }
                  if (makeTransferViewModel.accountFromKey !== null && makeTransferViewModel.accountFromKey !== undefined && (makeTransferViewModel.accountFromKey === accountID || makeTransferViewModel.accountFromKey === account.externalAccountId)) {
                        accountfromselval = accountName + (productCode !== "" ? " | " + account.productCode : "") + (shareID !== "" ? " " + shareID : "") + (bankName !== "" ? "  |  " + bankName : "") + (accountID !== "" ? " | " + ' X' + this.getLastFourDigit(accountID) : "") + (availablebal !== "" ? "  | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "");
                        selSecHeader = mainaccountNumhdval;
                        selProuductID = account.shareId;
                        makeTransferViewModel.accFromKey = accountfromselval;
                        makeTransferViewModel.FromaccountID = accountID;
                        makeTransferViewModel.FromProductID = account.shareId;
                        makeTransferViewModel.FromProductType = account.productCode;
                        makeTransferViewModel.FromExistingBalance = account.availableBalance;
                        makeTransferViewModel.FromExistingBalance = account.availableBalance;
                        makeTransferViewModel.FromIsExternal = account.external;
                        makeTransferViewModel.FromExternalAccountId = account.externalAccountId;
                        makeTransferViewModel.secHeader = mainaccountNumhdval;
                        makeTransferViewModel.accToKey = "";
                    }
                  count++;                
                if (i === fromaccounts.length - 1) {
                    var accountfinHeader = {
                        "lblHeader": mainaccountNumhdval
                    };
                    myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
                    kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
                }
                }
            }
                                                }
            this.view.transfermain.maketransfer.accountslist.segAccountTypes.setData(myAccountsMainSegData);
            this.view.transfermain.maketransfer.accountslist.FlexAccountname.lblFromAccountName.text = makeTransferViewModel.accFromKey;
            this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.setData([]);
            this.displayToExternalAccountsSegFromToAccount(fromaccountsobj, makeTransferViewModel.secHeader, makeTransferViewModel, makeTransferViewModel.FromProductID,externalTransferOption);
            this.view.transfermain.maketransfer.flxAmount.setVisibility(true);
            this.view.transfermain.maketransfer.lblAmount.setVisibility(true);
            this.view.transfermain.maketransfer.lblFrequency.setVisibility(true);
            this.view.transfermain.maketransfer.lbxFrequency.setVisibility(true);
            this.view.transfermain.maketransfer.flexNewRecipient.setVisibility(false);
            this.view.transfermain.maketransfer.flxWhenLoan.setVisibility(false);
            this.view.transfermain.maketransfer.flxMainCreditCard.setVisibility(false);
                                                
        },
    displayToExternalAccountsSegFromToAccount: function(fromaccountsobj, selAccountNumber, makeTransferViewModel, selProuductID,externalTransferOption) {
      var scopeObj = this;
      var myAccountsMainSegData = [];
      var myAccountsrowData = [];
      var accountNumhdval = "";
      var mainaccountNumhdval = "";
      var accountdisplaytext = "";
      var regDCount = "";
      //var externalaccounts = CommonUtilitiesOCCU.getExternalOccunts();
      var fromaccounts = fromaccountsobj;
      var count = 0;
      if (fromaccounts !== null && fromaccounts.length > 0) {                                                               
        for (var i = 0; i < fromaccounts.length; i++) {
          var account = fromaccounts[i];
          var productCode = this.checkNullAndGetEmptyText(account.productCode);
          var accountID = this.checkNullAndGetEmptyText(account.accountID);
          var accountName = this.checkNullAndGetEmptyText(account.accountName);
          var shareID = this.checkNullAndGetEmptyText(account.shareId);
          var availablebal = this.checkNullAndGetEmptyText(account.availableBalance);
          var bankName = this.checkNullAndGetEmptyText(account.bankName);                          
          if(makeTransferViewModel.FromIsExternal !== "" && makeTransferViewModel.FromIsExternal === account.external){
            kony.print("Same extrernal account type");
          } else {    
                                                if((externalTransferOption == true && account.external === false || account.external === "false") || (externalTransferOption == false && 
                                                                                account.external === true || account.external === "true")){
            //if(account.external === true || account.external === "true"){
              accountID = this.checkNullAndGetEmptyText(account.externalAccountId);
            }
            var toaccountNameOwner = account.bankName;                                                                                                                                
            var accountdisplaytxt = "";
            var accountdisplaysegtxt = "";
            if(account.availableBalance !== null && account.availableBalance !== null && account.availableBalance !== "null"){
              accountdisplaytxt = availablebal !== "" ? " | Avl Bal: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "";
              accountdisplaysegtxt = availablebal !== "" ? " | Available Balance: " + CommonUtilities.formatCurrencyWithCommas(availablebal) : "";
            }
            if(account.productCode !== "S"){
              accountdisplaytxt = "";
              accountdisplaysegtxt= "";
            }
            var accountobj = {
              "lblUsers": {
                "text": accountName + (productCode !== "" ? " | " + productCode : "") + (shareID !== "" ? " "+shareID : "") + accountdisplaysegtxt,
                "toolTip": accountName + (productCode !== "" ? " | " + productCode : "") + (shareID !== "" ? " "+shareID : "") + accountdisplaysegtxt
              },
              "lblSeparator": "Separator",
              "flxAccountTypesTransfers": {
                "onClick": account.onAccountSelection
              },
              "ToaccountID":accountID,
              "ToProductID": shareID,
              "ToProductType": productCode,
              "ToAccountName": bankName,
              "ToIsExternal": account.external,
              "ToExternalAccountId": account.externalAccountId,
              "accountdisplay": accountName + (productCode!==""? " | "+productCode:"") + (shareID!==""? " "+shareID:"") + (bankName!==""? " | " + bankName:"") + (accountID!==""? " | " + ' X' + this.getLastFourDigit(accountID):"") + accountdisplaytxt
            }
            var tempaccountheading = toaccountNameOwner + " | " + accountID;
            if (accountNumhdval !== "" && accountNumhdval !== tempaccountheading) {
              var accountHeader = {
                "lblHeader": mainaccountNumhdval
              };
              myAccountsMainSegData.push([accountHeader, myAccountsrowData]);
              myAccountsrowData = [];
              myAccountsrowData.push(accountobj);
            } else {
              myAccountsrowData.push(accountobj);
            }
            accountNumhdval = toaccountNameOwner + " | " + accountID;
            mainaccountNumhdval = toaccountNameOwner + " | " + ' X' + this.getLastFourDigit(accountID);
            if (count === 0 && makeTransferViewModel.accToKey === "") {
              accountdisplaytext =  accountName + (productCode!==""? " | "+productCode:"") + (shareID!==""? " "+shareID:"") + (bankName!==""? " | " + bankName:"") + (accountID!==""? " | " + ' X' + this.getLastFourDigit(accountID):"") + accountdisplaytxt;
              makeTransferViewModel.accToKey = accountdisplaytext;
              makeTransferViewModel.ToaccountID = accountID;
              makeTransferViewModel.ToProductID = account.shareId;
              makeTransferViewModel.ToProductType = account.productCode;
              makeTransferViewModel.ToIsExternal = account.external;
              makeTransferViewModel.ToExternalAccountId = account.externalAccountId;
            } 
            if (makeTransferViewModel.accountToKey !== null && makeTransferViewModel.accountToKey !== undefined && (makeTransferViewModel.accountToKey === accountID || makeTransferViewModel.accountToKey === account.externalAccountId)) {
              accountdisplaytext =  accountName + (productCode!==""? " | "+productCode:"") + (shareID!==""? " "+shareID:"") + (bankName!==""? " | " + bankName:"") + (accountID!==""? " | " + ' X' + this.getLastFourDigit(accountID):"") + accountdisplaytxt;
              makeTransferViewModel.accToKey = accountdisplaytext;
              makeTransferViewModel.ToaccountID = accountID;
              makeTransferViewModel.ToProductID = account.shareId;
              makeTransferViewModel.ToProductType = account.productCode;
              makeTransferViewModel.ToIsExternal = account.external;
              makeTransferViewModel.ToExternalAccountId = account.externalAccountId;
            } 
            count++;
          }

          if (i === fromaccounts.length - 1) {
            var accountfinHeader = {
              "lblHeader": mainaccountNumhdval
            };
            myAccountsMainSegData.push([accountfinHeader, myAccountsrowData]);
            kony.print("myAccountsMainSegData after push is " + JSON.stringify(myAccountsMainSegData));
          }

        }
      }
                  
      this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.removeAll();
      this.view.transfermain.maketransfer.accountsTolist.segAccountTypes.setData(myAccountsMainSegData);
      this.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromAccountName.text = makeTransferViewModel.accToKey;
    },
    clickExternalFromAccountsSeg: function(makeTransferViewModel) {
      var scopeObj = this;
      var index = scopeObj.view.transfermain.maketransfer.accountslist.segAccountTypes.selectedIndex[1];
      var secindex = scopeObj.view.transfermain.maketransfer.accountslist.segAccountTypes.selectedIndex[0];
      var segmentData = scopeObj.view.transfermain.maketransfer.accountslist.segAccountTypes.data[secindex][1][index];
      var segmentHeaderText = scopeObj.view.transfermain.maketransfer.accountslist.segAccountTypes.data[secindex][0].lblHeader;
      var selectedAccountNum = segmentData.lblUsers.text;
      var fromaccounts = makeTransferViewModel.accountsFrom;
      scopeObj.view.transfermain.maketransfer.accountslist.FlexAccountname.lblFromAccountName.text = segmentData.accountdisplay;
      makeTransferViewModel.accFromKey = segmentData.accountdisplay;
      makeTransferViewModel.FromaccountID = segmentData.FromaccountID;
      makeTransferViewModel.FromProductID = segmentData.FromProductID;
      makeTransferViewModel.FromProductType = segmentData.FromProductType;
      makeTransferViewModel.FromExistingBalance = segmentData.FromExistingBalance;
      makeTransferViewModel.FromIsExternal = segmentData.FromIsExternal;
      makeTransferViewModel.FromExternalAccountId = segmentData.FromExternalAccountId;
      makeTransferViewModel.secHeader = segmentHeaderText;  
      makeTransferViewModel.accToKey = "";
      var segToData = this.displayToExternalAccountsSeg(fromaccounts, segmentHeaderText, makeTransferViewModel, segmentData.FromProductID);
      this.onClickFromAccountName(makeTransferViewModel);
      this.FromAccountsRegD(segmentData.FromRegD,makeTransferViewModel);
      this.view.forceLayout();
    },
    clickExternalToAccountsSeg: function(makeTransferViewModel) {
      var scopeObj = this;
      var index = scopeObj.view.transfermain.maketransfer.accountsTolist.segAccountTypes.selectedIndex[1];
      var secindex = scopeObj.view.transfermain.maketransfer.accountsTolist.segAccountTypes.selectedIndex[0];
      var segmentData = scopeObj.view.transfermain.maketransfer.accountsTolist.segAccountTypes.data[secindex][1][index];
      var segmentHeaderText = scopeObj.view.transfermain.maketransfer.accountsTolist.segAccountTypes.data[secindex][0].lblHeader;
      var selectedAccountNum = segmentData.lblUsers.text;
      makeTransferViewModel.ToProductType = segmentData.ToProductType;
      makeTransferViewModel.accToKey = segmentData.accountdisplay;
      makeTransferViewModel.ToaccountID = segmentData.ToaccountID;
      makeTransferViewModel.ToProductID = segmentData.ToProductID;
      makeTransferViewModel.ToAccountName = segmentData.ToAccountName;
      makeTransferViewModel.ToIsExternal = segmentData.ToIsExternal;
      makeTransferViewModel.ToExternalAccountId = segmentData.ToExternalAccountId;
      scopeObj.view.transfermain.maketransfer.accountsTolist.FlexAccountname.lblFromAccountName.text = segmentData.accountdisplay;
      var fromaccounts = makeTransferViewModel.accountsFrom;
      this.displayExternalAccountsSeg(fromaccounts,makeTransferViewModel);
      this.onClickToAccountName(makeTransferViewModel);
    },

   createMarginAmongTheRadioButtonGroup : function()
    {
      for(i=0;i<frmTransfers_transfermain_maketransfer_RbPaymentAmount.children.length;i++)
     frmTransfers_transfermain_maketransfer_RbPaymentAmount.children[i].style.marginBottom ="20px";
    }

  };
}); 

