define(function() {
    /**
     * Method to receive deeplinking URL saved in AppContext
     * @memberof {Deeplinking}
     * @param {void}  -  None
     * @returns {String} - DeeplinkingUrl
     * @throws {}
     */
    var _deeplinkUrl = function() {
        return kony.mvc.MDAApplication.getSharedInstance().appContext.deeplinkUrl.deeplinkpath;
    };

    /**
     * Method to get StartupForm if provided by AppUrl
     * @memberof {Deeplinking}
     * @param {void}  -  None
     * @returns {String} - Startup form name
     * @throws {}
     */
    var _formId = function() {
        return kony.mvc.MDAApplication.getSharedInstance().appContext.deeplinkUrl.formID;
    };
    /**
     * Method to check if url contains session token
     * @memberof {Deeplinking}
     * @param {string}  -  stringURL,value
     * @returns {boolean} - true, if url has session token
     * @throws {}
     */
    var hastoken = function(stringURL, value) {
        var returnValue = false;
        var position = stringURL.indexOf(value);
        if (position >= 0) {
            returnValue = true;
        }
        return returnValue;
    };
    /**
     * Method to get Session Token if provided by AppUrl
     *@memberof {Deeplinking}
     * @param {void} - None
     * @returns {String} - session token
     * throws{}
     */
    var _sessionToken = function() {
        var formId = kony.mvc.MDAApplication.getSharedInstance().appContext.deeplinkUrl.formID;
        if (formId) {
            if (hastoken(formId, "?")) {
                var keyValue = formId.split("?");
                keyValue = keyValue[1];
                var value = keyValue.split("=");
                var sessionToken = value[1];
                return sessionToken;
            }
        }
        return null;
    };
    /**
     * Method to get StartupForm
     * @memberof {Deeplinking}
     * @param {void}  -  None
     * @returns {String} startUpPage- Startup form name
     * @throws {}
     */
    var _startUpPage = function() {
        var startUpPage = _formId();
        if (!startUpPage) {
            var deeplinkUrl = _deeplinkUrl();
            startUpPage = deeplinkUrl.slice(deeplinkUrl.indexOf("kdw") + 5);
        }
        return startUpPage;
    };

    /**
     * Method to get set navigation for deeplinked page
     * @memberof {Deeplinking}
     * @param {void}  -  None
     * @returns {void} - None
     * @throws {}
     */
    var _deeplinking = function() {
        var sessiontoken = _sessionToken();
        //if redirected url has session token , then set value of isCSR_Assist_Mode to true
        if (sessiontoken) {
            kony.mvc.MDAApplication.getSharedInstance().appContext.isCSR_Assist_Mode = true;
        } else {
            kony.mvc.MDAApplication.getSharedInstance().appContext.isCSR_Assist_Mode = false;
        }
        var formName = _startUpPage();
        var newSession = kony.mvc.MDAApplication.getSharedInstance().appContext.deeplinkUrl.isNewSession;
        if (newSession) {
            kony.mvc.MDAApplication.getSharedInstance().appContext.deeplinkUrl.isNewSession = false;
        }
        if (formName === "" || formName === "frmLogin" && !newSession) {
            return;
        }
        switch (formName) {
            case "frmEnrollNow":
                {
                    var context = {
                        action: "Navigate to Enroll"
                    };
                    //Loading enroll module
                    var enrollModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("EnrollModule");
                    enrollModule.presentationController.showEnrollPage(context);
                    return;
                }
                // other cases
            default:
                {
                    return sessiontoken;
                }
        }
    };

    return {
        deeplinking: _deeplinking
    };

});