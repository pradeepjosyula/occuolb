define(['formatCurrency', 'formatDate' , 'ErrHandler','OLBConstants', 'DateUtils'], function (formatCurrency, formatDate, ErrHandler, OLBConstants, DateUtils) {

  /**
         * getAccountDisplayName - Returns a combination of account name and account number for displaying.
         * @member of {CommonUtilities}
         * @param {object} account - Account object.
         * @returns {String} Account name concatenated with account number
         * @throws {}
         */
  var _getAccountDisplayName = function (account) {
    return _getAccountName(account) + " ...." + _getLastFourDigit(account.accountID);
  };

  var _getAccountDisplayNameWithBalance = function (account) {
    return _getAccountDisplayName(account) + " " + _getDisplayBalance(account);
  };

  /**
         * getAccountName - Returns the account nick name for an account. If nickname doesn't exist, returns account name.
         * @member of {CommonUtilities}
         * @param {object} account - Account object.
         * @returns {String} Account nickname/name 
         * @throws {}
         */
  var _getAccountName = function (account) {
    if (account.nickName && account.nickName.length > 0) {
      return account.nickName;
    } else {
      return account.accountName;
    }
  };
  /**
        * __mergeAccountNameNumber - when we do not have access to account object but we want to concatenate account name with account number
        * @member of {CommonUtilities}
        * @param {string, integer} account name and account number
        * @returns {String} - concatenated string
        * @throws {}
        */
  var _mergeAccountNameNumber=function(accountName,accountNumber) {
    return accountName + " ...." + (accountNumber % 10000);

  };	

  /**
         * getDisplayBalance - Returns account balance string.
         * @member of {CommonUtilities}  
         * @param {object} account - account object.
         * @returns {String} Available balance string.
         * @throws {}
         */
  var _getDisplayBalance = function (account) {
    return "(" + kony.i18n.getLocalizedString('i18n.common.available') + ":" + _formatCurrencyWithCommas(account.availableBalance) + ")";
  };

  /**
         * getLastFourDigit - Returns last four characters of a given string.
         * @member of {CommonUtilities}
         * @param {String} numberStr - String whose last four digits should be returned.
         * @returns {String} last four characters of the given string
         * @throws {}
         */
  var _getLastFourDigit = function (numberStr) {
    if (numberStr.length < 4) return numberStr;
    return numberStr.slice(-4);
  };

  /**
         * Downloads file from a given url.
         * @member of {CommonUtilities}
         * @param {object} data - object containing url and filename.
         * @returns {}
         * @throws {String} - Error message
         */
  var _downloadFile = function (data) {
    if (data) {
      if (data.url) {
        var element = document.createElement('a');
        element.setAttribute('href', data.url);
        element.setAttribute('download', data.filename || 'download');
        element.setAttribute('target', '_blank'); //Tmp fix : Chrome blocked cross orgin download- so in chrome we are opening the file in new window.
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
      }
      else{
        return "Url is Invalid : " + data.url;
      }
    }
  };

  /**
         * _validateCurrency - Returns true if amount is a valid,else returns false
         * @member of {CommonUtilities}
         * @param {String} amount - Amount.
         * @returns {boolean} 
         * @throws {}
         */
  var _validateCurrency = function(amount){
    if(amount){
      amount=amount+"";
      var regularexp= /^\$?[0-9][0-9\,]*(\.\d*)?$|^\$?[\.]([\d][\d]?)$/;
      if(amount.match(regularexp))
        return true;
    }
    return false;
  };

  /**
         * _addZeroesToAmount- Adds decimals to given amount string. (Do not access this method outside CommonUtilities)
         * @member of {CommonUtilities}
         * @param {String} amount - Amount.
         * @returns {String} Amount string with decimals
         * @throws {}
         */
  var _addZeroesToAmount = function(amount){
    amount = amount + "";
    if(amount.indexOf(".")>=0 && (amount.length-amount.indexOf(".")>2)){  
      amount = amount.slice(0, (amount.indexOf(".")) + 3);
      amount = Number(amount);
      amount = amount.toFixed(2);
      amount = amount+"";
    } else {
      var value = Number(amount);
      value = value.toFixed(2);
      amount = value.toString();
    }
    return amount;
  };

  /**
         * _addCommasToAmountString - Adds commas after every 3 digits to given amount string. This method should be modified based on the region. A comma after 3 digits is the standard for USA. (Do not access this method outside CommonUtilities)
         * @member of {CommonUtilities}
         * @param {String} amount - Amount.
         * @returns {String} Amount string with commas. 
         * @throws {}
         */
  var _addCommasToAmountString = function(str) {
    var parts = str.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  };

  /**
         * _formatCurrencyWithCommas - Returns formatted amount string. Adds commas and currency symbol to a given amount string.
         * @member of {CommonUtilities}
         * @param {String} amount - Amount.
         * @param {boolean} currenySymbolNotRequired - Flag to determine whether currency symbol should be appended.
         * @returns {String} Formatted amount string
         * @throws {}
         */
  var _formatCurrencyWithCommas = function(amount, currencySymbolNotRequired){
    if(amount === undefined || amount === null)
      return;
    if(amount[0] === kony.i18n.getLocalizedString("i18n.common.currencySymbol")){
      amount = amount.slice(1);
    }
    if(typeof(amount) == 'string')
      amount = amount.replace(/,/g,"");
    if(isNaN(amount))
      return;
    if (currencySymbolNotRequired) {
      return _addCommasToAmountString(_addZeroesToAmount(amount));        
    }
    amount = kony.i18n.getLocalizedString("i18n.common.currencySymbol") + _addCommasToAmountString(_addZeroesToAmount(amount));
    if(amount.match(/-/)){
      amount ="-"+amount.replace(/-/,"");
    }
    return amount;
  };

  var _getDisplayCurrencyFormat = function(amount){
    amount = this.formatCurrencyWithCommas(amount);
    if(amount){
      if(amount.match(/-/)){
        amount ="-"+amount.replace(/-/,"");
      }
    }
    return amount;
  };

  /**
         * Sorting Configuration object.
         * @member of {CommonUtilities}
         * @param {object} dataInputs - sorting inputs (offset, limit, sortBy, order)
         * @param {object} sortingConfigInputs - default page configuration (offset, limit, sortBy, order, defaultSortBy, defaultOrder) 
         * @returns {object} - sorting configuration object (offset, limit, sortBy, order)
         * @throws {}
         */
  var _getSortConfigObject = function (dataInputs, sortingConfigInputs) {

    try {

      dataInputs = dataInputs || {};

      if (sortingConfigInputs === null || typeof sortingConfigInputs !== "object") {
        ErrHandler.onError('Invalid Inputs');
        return;
      }

      var sortObj = {};

      if (dataInputs.resetSorting) {
        sortObj.offset = OLBConstants.DEFAULT_OFFSET;
        sortObj.limit = OLBConstants.PAGING_ROWS_LIMIT;
        sortObj.sortBy = sortingConfigInputs.defaultSortBy;
        sortObj.order = sortingConfigInputs.defaultOrder;
      } else {
        if(typeof dataInputs.offset === "number") {
          sortObj.offset =  dataInputs.offset;
        } else {
          if(typeof sortingConfigInputs.offset === "number") {
            sortObj.offset = sortingConfigInputs.offset;
          } else {
            sortObj.offset = OLBConstants.DEFAULT_OFFSET;
          }
        }
        if(typeof dataInputs.limit === "number") {
          sortObj.limit =  dataInputs.limit;
        } else {
          if(typeof sortingConfigInputs.limit === "number") {
            sortObj.limit = sortingConfigInputs.limit;
          } else {
            sortObj.limit = OLBConstants.PAGING_ROWS_LIMIT;
          }
        }
        sortObj.sortBy = dataInputs.sortBy || sortingConfigInputs.sortBy || sortingConfigInputs.defaultSortBy;
        sortObj.order = dataInputs.order || sortingConfigInputs.order || sortingConfigInputs.defaultOrder;

        if (dataInputs.sortBy && !dataInputs.order) {
          if (dataInputs.sortBy === sortingConfigInputs.sortBy) {
            sortObj.order = sortingConfigInputs.order === OLBConstants.ASCENDING_KEY ? OLBConstants.DESCENDING_KEY : OLBConstants.ASCENDING_KEY;
          } else {
            sortObj.order = OLBConstants.ASCENDING_KEY;
          }
        }
      }
      // re-assign to sortConfig Object to persist.
      sortingConfigInputs.sortBy = sortObj.sortBy;
      sortingConfigInputs.order = sortObj.order;
      sortingConfigInputs.limit = sortObj.limit;
      sortingConfigInputs.offset = sortObj.offset;

      return sortObj;

    } catch (exception) {
      ErrHandler.onError(exception);
    }

  };

  /**
        * Update Sort Flex - Method to update sorting icons in headers with given sortMap.
        * @member of {CommonUtilities}
        * @param {object} sortMap - sorting map (name, imageFlx, clickContainer)  
        * @param {object} viewModel - target column and order(asc/desc).
        * @returns {}
        * @throws {}
        */
  var _updateSortFlex = function (sortMap, viewModel) {
    viewModel = viewModel || {};
    if (sortMap && sortMap.length && viewModel) {
      sortMap.forEach(function (item) {
        if (viewModel.sortBy === item.name) {
          item.imageFlx.src = viewModel.order === OLBConstants.DESCENDING_KEY ? OLBConstants.IMAGES.SORTING_NEXT : OLBConstants.IMAGES.SORTING_PREVIOUS;
        } else {
          item.imageFlx.src = OLBConstants.IMAGES.SORTING;
        }
      });
    }
  };

  /**
        * _setSortingHandlers - Method to attach sort Handlers
        * @member of {CommonUtilities}
        * @param {object} sortMap - sorting map (name, imageFlx, clickContainer)
        * @param {function} clickHandler - on sort click handler
        * @param {object} scope - click handler scope.
        * @returns {}
        * @throws {}
        */
  var _setSortingHandlers = function (sortMap, clickHandler, scope) {
    var scopeObj = this;
    sortMap.map(function (_item) {
      _item.clickContainer.onClick = clickHandler.bind(scope || scopeObj, event, {
        'sortBy': _item.name,
        'offset' : OLBConstants.DEFAULT_OFFSET
      });
    });
  };

  /**
         * Get Configuration key value.
         * @member of {CommonUtilities}
         * @param {String} key - configuration key available in Configuration object.
         * @returns {String} Whatever value is associated with the given key.
         * @throws {}
         */
  var _getConfiguration = function (key) {
    if (key === undefined && key === null) {
      ErrHandler.onError('Invalid Configuraion Key.');
      return null;
    }
    return kony.onlineBanking.configurations.getConfiguration(key);
  };

  /**
         * Display loading indicator.
         * @member of {CommonUtilities}
         * @param {object} view : view object of the form .
         * @returns {}
         * @throws {}
         */
  var _showProgressBar = function(view){
    kony.olb.utils.showProgressBar(view);
  };

  /**
         * Hide loading indicator.
         * @member of {CommonUtilities}
         * @param {object} view : view object of the form. 
         * @returns {}
         * @throws {}
         */
  var _hideProgressBar = function(view){
    kony.olb.utils.hideProgressBar(view);
  };

  /**
         * _getFrontendDateString - Formats a given date string into a given date format.
         * @member of {CommonUtilities}
         * @param {String} dateString - The date string to be formatted.
         * @param {String} dateFormat - Format into which the given date string should be converted.
         * @returns {String} Formatted date string.
         * @throws {}
         */
  var _getFrontendDateString = function(dateString, dateFormat) {
    if (!dateString) return;
    if(isNaN(dateString)){
      if (dateString.indexOf('T')>=0) {
        var date = dateString.split('T');
        if (date[0].indexOf('-')>=0) date = date[0].split('-');
        else if (date[0].indexOf('/')>=0) date = date[0].split('/');
        if (dateFormat === undefined || dateFormat === null || dateFormat === 'mm/dd/yyyy') return date[1] + '/' + date[2] + '/' + date[0];
        else if (dateFormat === 'dd/mm/yyyy') return date[2] + '/' + date[1] + '/' + date[0];
        else if (dateFormat === 'mm/dd/yyyy') return date[1] + '/' + date[2] + '/' + date[0];
        else if (dateFormat === 'yyyy/dd/mm') return date[0] + '/' + date[2] + '/' + date[1];
        else if (dateFormat === 'yyyy/mm/dd') return date[0] + '/' + date[1] + '/' + date[2];
      }
    }else{
      if (dateFormat === 'dd/mm/yyyy') return ((dateString.getDate()<10 ? '0' :'')+dateString.getDate()) + '/' +(dateString.getMonth()+1<10?'0':'')+(dateString.getMonth()+1)+'/' + dateString.getYear();
      if (dateFormat === 'mm/dd/yyyy') return ((dateString.getMonth()+1<10?'0':'')+(dateString.getMonth()+1)) + '/' +((dateString.getDate()<10 ? '0' :'')+dateString.getDate())+'/' + dateString.getFullYear();
      if (dateFormat === 'yyyy/dd/mm') return dateString.getFullYear()+ '/' +((dateString.getDate()<10 ? '0' :'')+dateString.getDate())+'/' + ((dateString.getMonth()+1<10?'0':'')+(dateString.getMonth()+1));        
      if (dateFormat === 'yyyy/mm/dd') return dateString.getFullYear()+ '/' +(dateString.getMonth()+1<10?'0':'')+(dateString.getMonth()+1)+'/' + ((dateString.getDate()<10 ? '0' :'')+dateString.getDate());        
    }
    return dateString;
  };

  /**
         * _getBackendDateFormat - Formats a given date string into a given date format.
         * @member of {CommonUtilities}
         * @param {String} dateString - The date string to be formatted.
         * @param {String} dateFormat - Format into which the given date string should be converted.
         * @returns {String} - Formatted date string
         * @throws {}
         */
  var _getBackendDateFormat = function(dateString, dateFormat){
    if(!dateFormat || !dateString )
      return;
    var date = dateString.split('/');
    if(dateFormat === 'dd/mm/yyyy')
      return date[2]+'-'+date[1]+'-'+date[0];
    else if(dateFormat === 'mm/dd/yyyy')
      return date[2]+'-'+date[0]+'-'+date[1];
    else if(dateFormat === 'yyyy/mm/dd')
      return date[0]+'-'+date[1]+'-'+date[2];
    else if(dateFormat === 'yyyy/dd/mm')
      return date[0]+'-'+date[2]+'-'+date[1];
  };

  /**
         * _disableOldDaySelection - Disables the selection of past dates and sets the date range for a given calendar widget.
         * @member of {CommonUtilities}
         * @param {String} widgetId - ID of the calendar widget.
         * @param {String} backendDate - If a date is passed, the calendar widget's selection will be enabled from this date.
         * @returns {}
         * @throws {}  
         */
  var _disableOldDaySelection = function(widgetId, backendDate) {
    var dd, mm, yy;
    var maxDate, maxMonth, maxYear;
    var dateFormate = kony.onlineBanking.configurations.getConfiguration("frontendDateFormat");
    widgetId.dateFormat=dateFormate;
    var numberOfYearsAllowed = OLBConstants.CALENDAR_ALLOWED_FUTURE_YEARS; 
    if (backendDate) {
      backendDate = backendDate.split("/");
      mm = backendDate[0];
      dd = backendDate[1];
      yy = backendDate[2];
      maxDate = dd;
      maxMonth = mm;
      maxYear = (Number(yy) + numberOfYearsAllowed);  
      if (dateFormate === "dd/mm/yyyy") {
        widgetId.enableRangeOfDates([dd, mm, yy], [maxDate, maxMonth, maxYear], "skn", true);
        widgetId.date = dd + "/" + mm + "/" + yy;
      } else {
        widgetId.enableRangeOfDates([dd, mm, yy], [maxDate, maxMonth, maxYear], "skn", true);
        widgetId.date = mm + "/" + dd + "/" + yy;
      }
    } else {
      var date = new Date();
      dd = date.getDate();
      mm = date.getMonth() + 1;
      yy = date.getFullYear();
      maxDate = dd;
      maxMonth = mm;
      maxYear = (Number(yy) + numberOfYearsAllowed);  
      if (dateFormate === "dd/mm/yyyy") {
        widgetId.enableRangeOfDates([dd, mm, yy], [maxDate, maxMonth, maxYear], "skn", true);
        widgetId.date = dd + "/" + mm + "/" + yy;
      } else {
        widgetId.enableRangeOfDates([dd, mm, yy], [maxDate, maxMonth, maxYear], "skn", true);
        widgetId.date = mm + "/" + dd + "/" + yy;
      }
    }
  };

  /**
       * _sendDateToBackend - Converts a given date string from frontend format to backend format.
       * @member of {CommonUtilities}
         * @param {String} dateString - The date string which should be formatted.
       * @param {String} frontendDateFormat - Front end date format.
       * @param {String} backendDateFormat - Backend date format.
       * @returns {String} Formatted date string
       * @throws {}
         */
  var _sendDateToBackend=function(dateString,frontendDateFormat,backendDateFormat){
    if( !dateString ) {
      return;            
    }
    var date=dateString.split('/');
    var day;
    var month;
    var year;
    if(frontendDateFormat === "mm/dd/yyyy" || frontendDateFormat === undefined || frontendDateFormat === null){
      day = date[1];
      month = date[0];
      year = date[2];
    }	
    if(frontendDateFormat === "dd/mm/yyyy" || frontendDateFormat === undefined || frontendDateFormat === null){
      day = date[0];
      month = date[1];
      year = date[2];
    }
    if(backendDateFormat === "yyyy-mm-dd" || backendDateFormat === undefined || backendDateFormat === null)		
      return year + '-' + month + '-' + day;

    //other conditions as required
  };	

  /**
        * _changedataCase - Changes the case of the first letter of each word in the string to uppercase and returns it.
        * @member of {CommonUtilities}
        * @param {String} str - String whose case should be changed.
        * @returns {String} - The given string with each word's first character changed to uppercase
        * @throws {}
        */
  var _changedataCase=function(str) {
    return str.replace(/\w\S*/g, function(word) {
      return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    });
  };	


  /**
       * _getDateAndTime - Formats a given date string into a given date format (including hour and minute fields.)
       * @member of {CommonUtilities}
       * @param {String} dateString - The date string to be formatted.
       * @param {String} dateFormat - Format into which the given date string should be converted.
       * @returns {String} - Formatted date string including hours and minutes.
       * @throws {}
       */
  var _getDateAndTime=function(dateString,dateFormat){
    if (!dateString) {
      return;
    }
    if (dateString.indexOf(' ')>=0) {
      var date = dateString.split(' ');
      var time=date[1];
      time=time.split(":");
      if (date[0].indexOf('-')>=0) date = date[0].split('-');
      else if (date[0].indexOf('/')>=0) date = date[0].split('/');
      var hour = time[0];
      var minute = time[1]; 
      if (dateFormat === undefined || dateFormat === null || dateFormat === 'mm/dd/yyyy') return date[1] + '/' + date[2] + '/' + date[0] + ' ' + hour + ':' + minute;
      else if (dateFormat === 'dd/mm/yyyy') return date[2] + '/' + date[1] + '/' + date[0] + ' ' + hour + ':' + minute;
      else if (dateFormat === 'mm/dd/yyyy') return date[1] + '/' + date[2] + '/' + date[0] + ' ' + hour + ':' + minute;
      else if (dateFormat === 'yyyy/dd/mm') return date[0] + '/' + date[2] + '/' + date[1] + ' ' + hour + ':' + minute;
      else if (dateFormat === 'yyyy/mm/dd') return date[0] + '/' + date[1] + '/' + date[2] + ' ' + hour + ':' + minute;
    }
  };


  /**
         * _toggleCheckbox - Toggle The check box state on a image widget. Assuming the image widget passed is being used as checkbox.
         * @member of {CommonUtilities}
         * @param {object} imageWidget - The image widget of the check box which should be toggled. 
         * @returns {}
         * @throws {}
         */
  var _toggleCheckbox = function (imageWidget) {
    imageWidget.src = imageWidget.src === OLBConstants.IMAGES.CHECKED_IMAGE ?  OLBConstants.IMAGES.UNCHECKED_IMAGE : OLBConstants.IMAGES.CHECKED_IMAGE; 
  };

  /**
         * _isChecked - Returns the state of Checkbox (Image Widget)
         * @member of {CommonUtilities}
         * @param {object} imageWidget - The image widget of the check box of which state is needed. 
         * @returns {boolean} - returns true if checkbox is checked and false if checkbox is not checked
         * @throws {}
         */

  var _isChecked = function (imageWidget) {
    return imageWidget.src === OLBConstants.IMAGES.CHECKED_IMAGE;
  };

  /**
         * _setCheckboxState - Sets the state of Checkbox (Image Widget)
         * @member of {CommonUtilities}
         * @param {boolean} state - The state of checkbox which needs to be set.
         * @returns {void} - None
         * @throws {}
         */

  var _setCheckboxState = function (state, imageWidget) {
    if (state) {
      imageWidget.src = OLBConstants.IMAGES.CHECKED_IMAGE;
    }
    else {
      imageWidget.src = OLBConstants.IMAGES.UNCHECKED_IMAGE;
    }
  };

  /**
         * Method to update App Level configuration key
         * @param {String} key to be updated
         * @param {String} value of the key
         * @returns {}
         * @throws {}
         */
  var _updateAppLevelConfiguration = function(key, value) {
    if (key) {
      return kony.onlineBanking.configurations.updateAppLevelConfigurationKey(key, value);
    } else {
      ErrHandler.onError("Invalid Key");
      return null;
    }
  };

  /**
         * Method to update User Level configuration key
         * @param {String} key to be updated
         * @param {String} value of the key
         * @returns {}
         * @throws {}
         */
  var _updateUserLevelConfiguration = function(key, value) {
    if (key) {
      return kony.onlineBanking.configurations.updateUserLevelConfigurationKey(key, value);
    } else {
      ErrHandler.onError("Invalid Key");
      return null;
    }
  };

  /**
          *Method to get Months
          *key:get Months with reange of key.
          *value:value of the key
        */

  var _returnMonths = function (monthId) {
    var months = {
      1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June",
      7: "July", 8: "August", 9: "September", 10: "October", 11: "November", 12: "December"
    };
    if (monthId) {
      var pastMonths = {};
      while (monthId !== 0) {
        pastMonths[monthId] = months[monthId];
        monthId--;
      }
      return pastMonths;
    } else {
      return months;
    }
  };

  /**
         *Method to determine if the app is logged in CSR mode
         *@params: None
         *@return:true if it is logged in CSR mode else returns false
         *@throw:{}
        */
  var _isCSRMode=function(){
    return kony.mvc.MDAApplication.getSharedInstance().appContext.isCSR_Assist_Mode;
  };

  /**
         * _isRadioBtnSelected - Returns the state of Radio (Image Widget)
         * @member of {CommonUtilities}
         * @param {object} imageWidget - The image widget of the check box of which state is needed. 
         * @returns {boolean} - returns true if checkbox is checked and false if checkbox is not checked
         * @throws {}
         */

  var _isRadioBtnSelected = function (imageWidget) {
    return imageWidget.src === OLBConstants.IMAGES.RADIOBTN_ACTIVE_SMALL;
  };

  /**
         * disableButton : Method to disable button status
         * @member of {frmStopPaymentsController}
         * @param {object} button, widget button object 
         * @return {}
         * @throws {} 
         */
  var _disableButton= function (button) {
    button.setEnabled(false);
    button.skin = "sknBtnBlockedLatoFFFFFF15Px";
    button.hoverSkin = "sknBtnBlockedLatoFFFFFF15Px";
    button.focusSkin = "sknBtnBlockedLatoFFFFFF15Px";
  };

  /**
         * enableButton : Method to enable button status
         * @member of {frmStopPaymentsController}
         * @param {object} button, widget button object
         * @return {}
         * @throws {} 
         */
  var _enableButton= function (button) {
    button.setEnabled(true);
    button.skin = "sknbtnLatoffffff15px";
    button.hoverSkin = "sknBtnFocusLatoFFFFFF15Px";
    button.focusSkin = "sknBtnHoverLatoFFFFFF15Px";
  };

  /**
         * showServerDownScreen : Method to show the common Server down page
         * @member of {CommonUtilities}
         * @param {} - NONE
         * @return {VOID}
         * @throws {} 
         */
  var _showServerDownScreen = function(){
    var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    //Presenting the Login form without a viewmodel first as a work around for preShow of form being called after willUpdateUI.
    authModule.presentationController.presentUserInterface('frmLogin', {});
    authModule.presentationController.navigateToServerDownScreen.call(authModule.presentationController);
  };
  /**
         * disableButtonActionForCSRMode : Method to disable Segment buttons and buttons in csr mode
         * @member of {CommonUtilities}
         * @param {} 
         * @return {}
         * @throws {} 
         */
  var _disableButtonActionForCSRMode = function () {
    kony.print("This action is not valid for CSR");
  };
  /**
         * disableButtonSkinForCSRMode : Method to set skin to buttons in csr mode
         * @member of {CommonUtilities}
         * @param {} 
         * @return {string} skin - skin to applied to buttons in csr mode
         * @throws {} 
         */
  var _disableButtonSkinForCSRMode = function () {
    var skin = "sknBtnBlockedLatoFFFFFF15Px" ;
    return skin;
  };
  /**
         * disableSegmentButtonSkinForCSRMode : Method to set skin to Segment buttons in csr mode
         * @member of {CommonUtilities}
         * @param {size} : size of the font in button
         * @return {string} skin - skin to be applied to buttons in csr mode
         * @throws {} 
         */
  var _disableSegmentButtonSkinForCSRMode = function (size) {
    if(size==13)
      return "sknBtnLato3343A813PxBg0CSR";
    if(size==15)
      return "sknBtnLato3343A815PxBg0CSR";
    if(size==17)
      return "sknBtnLato3343A817PxBg0CSR";
  };

  /**
         * _isValidAmount : Method to validat amount field
         * @member of {CommonUtilities}
         * @param {string} amount, amount string
         * @return {boolean} is valid amount value or not
         * @throws {} 
         */
  var _isValidAmount = function(amount) {
    return amount !== undefined && amount !== null && !isNaN(amount) && amount !== "";
  };

  /**
         * _isEmptyString : Method to validate Empty String
         * @member of {CommonUtilities}
         * @param {string} str, string to validate
         * @return {boolean} is valid Empty string or not
         * @throws {} 
         */
  var _isEmptyString = function(str) {
    return str !== undefined && str !== null && str.toString().trim().length === 0;
  };

  /**
         * getMaskedAccountNumber : Method to mask Account Number
         * @member of {CommonUtilities}
         * @param {string} accountNumber,
         * @return {string} masked account Number
         * @throws {} 
         */
  var _getMaskedAccountNumber = function (accountNumber) {
    var stringAccNum = accountNumber;
    var isLast4Digits = function isLast4Digits(index) {
      return index > stringAccNum.length - 5;
    };
    return stringAccNum.split('').map(function (c, i) {
      return isLast4Digits(i) ? c : 'X';
    }).join('');
  };
  /**
         * Method to check if given string contains required character
         * @memberof {CommonUtilities}
         * @param {string}  -  string,value
         * @returns {boolean} - true if string contains the required character
         * @throws {}
         */
  var  _substituteforIncludeMethod=function(string, value) {
    var returnValue = false;
    var position = string.indexOf(value);
    if (position >= 0) {
      returnValue = true;
    }
    return returnValue;
  };

  /**
         * Function to Find difference between current date and a date
         * @member of  FormatUtilManager
         * date format is yyyymmddhhmmss
         */
  var _getTimeDiferenceOfDate = function (date) {
    var yyyy = date.substring(0, 4);
    var mon = date.substring(4, 6);
    var dd = date.substring(6, 8);
    var hh = date.substring(8, 10);
    var mm = date.substring(10, 12);
    var ss = date.substring(12);
    var date1 = new Date(yyyy, parseInt(mon, 10) - 1, dd, hh, mm, ss);
    var date1_ms = date1.getTime();
    var dateTemp = new Date();
    var utcDate = dateTemp.getUTCDate();
    var utcMonth = dateTemp.getUTCMonth();
    var utcYear = dateTemp.getUTCFullYear();
    var utcHour = dateTemp.getUTCHours();
    var utcMins = dateTemp.getUTCMinutes();
    var utcSecs = dateTemp.getUTCSeconds();
    var date2 = new Date(utcYear, utcMonth, utcDate, utcHour, utcMins, utcSecs);
    var date2_ms = date2.getTime();
    var difference_ms = date2_ms - date1_ms;

    var one_year = 365 * 1000 * 60 * 60 * 24;
    var one_month = 30 * 1000 * 60 * 60 * 24;
    var one_day = 1000 * 60 * 60 * 24;
    var one_hour = 1000 * 60 * 60;
    var one_min = 1000 * 60;
    var one_sec = 1000;

    var timeDiff = "just now";

    var yearDiff = difference_ms / one_year;
    var monthDiff = difference_ms / one_month;
    var daysDiff = difference_ms / one_day;
    var hoursDiff = difference_ms / one_hour;
    var minutesDiff = difference_ms / one_min;
    var secondsDiff = difference_ms / one_sec;

    if (Math.floor(yearDiff) > 0) {

      if (Math.floor(yearDiff) == 1) {
        timeDiff = "year";
      } else {
        timeDiff = "years";
      }

      return Math.floor(yearDiff) + " " + timeDiff + " ago";
    } else if (Math.floor(monthDiff) > 0) {

      if (Math.floor(monthDiff) == 1) {
        timeDiff = "month";
      } else {
        timeDiff = "months";
      }

      return Math.floor(monthDiff) + " " + timeDiff + " ago";
    } else if (Math.floor(daysDiff) > 0) {

      if (Math.floor(daysDiff) == 1) {
        timeDiff = "day";
      } else {
        timeDiff = "days";
      }

      return Math.floor(daysDiff) + " " + timeDiff + " ago";
    } else if (Math.floor(hoursDiff) > 0) {

      if (Math.floor(hoursDiff) == 1) {
        timeDiff = "hour";
      } else {
        timeDiff = "hours";
      }

      return Math.floor(hoursDiff) + " " + timeDiff + " ago";
    } else if (Math.floor(minutesDiff) > 0) {

      if (Math.floor(minutesDiff) == 1) {
        timeDiff = "minute";
      } else {
        timeDiff = "minutes";
      }

      return Math.floor(minutesDiff) + " " + timeDiff + " ago";
    } else if (Math.floor(secondsDiff) > 0) {

      if (Math.floor(secondsDiff) == 1) {
        timeDiff = "second";
      } else {
        timeDiff = "seconds";
      }

      return Math.floor(secondsDiff) + " " + timeDiff + " " + " ago";
    }

    return timeDiff;
  };

  /**
         * getPrimaryContact: Returns the contact for which the isPrimary flag is true. Expects only one of the contacts to be primary. If there are more than one, returns the last.
         * @member of {CommonUtilities}
         * @param {Array} - Array of contacts.  
         * @return {}
         * @throws {}
            */
  var _getPrimaryContact = function(contacts){
    var primaryContact = "";
    contacts.forEach(function(item){
      if(item.isPrimary === "true"){
        primaryContact = item.Value;
      }
    });
    return primaryContact;
  };

  /**
         * _deFormatAmount: Removes the formatting (commas) from the amount. 
         * @member of {CommonUtilities}
         * @param {String} - amountString with commas.  
         * @return {}
         * @throws {}
            */
  var _deFormatAmount = function(amountString){
    return String(amountString).replace(/,/g, "");
  };

  /**
         * validateAndFormatAmount: Validates the amount in the given amount field reference and then formats it if the amount is valid. 
         * @member of {CommonUtilities}
         * @param {Object} - Widget Reference.  
         * @return {}
         * @throws {}
            */
  var _validateAndFormatAmount = function(widgetId){
    var amount = widgetId.text;
    if(!this.isValidAmount(amount)){
      return false;
    }else{
      widgetId.text = this.formatCurrencyWithCommas(amount, true);
      return true;
    }
  };

  /**
         * _validateAmountFieldKeyPress: Doesn't allow the user to enter a 3rd decimal. 
         * @member of {CommonUtilities}
         * @param {Object} - Widget Reference.  
         * @return {}
         * @throws {}
            */
  var _validateAmountFieldKeyPress = function(widgetId){
    var amount = widgetId.text;
    if(amount.indexOf('.') < 0)
      return;
    else{
      var arr = amount.split('.');
      if(arr[1].length <= 2)
        return;
      widgetId.text = (arr[0] + "." + arr[1].slice(0, 2));
      return true;
    }
  };

  /**
         * removeDelimitersForAmount: Removes the formatting (commas) for the amount in the given widget. 
         * @member of {CommonUtilities}
         * @param {Object} - Widget Reference.  
         * @return {}
         * @throws {}
            */
  var _removeDelimitersForAmount = function(widgetId){
    var amount = widgetId.text;
    widgetId.text = amount.replace(/,/g, "");
  };

  /**
         * bindEventsForAmountField: Binds the Key Up, End Editing and Begin Editing events for an amount field.
         * @member of {CommonUtilities}
         * @param {Object} - Widget Reference.  
         * @return {}
         * @throws {}
            */
  var _bindEventsForAmountField = function(widgetId) {
    widgetId.onKeyUp = this.validateAmountFieldKeyPress.bind(this, widgetId);
    widgetId.onEndEditing = this.validateAndFormatAmount.bind(this, widgetId);
    widgetId.onBeginEditing = this.removeDelimitersForAmount.bind(this, widgetId);
  };

  var _blockFutureDate = function(widgetId,startDate){
    var dateFormate = kony.onlineBanking.configurations.getConfiguration("frontendDateFormat");
    var finalDate;
    widgetId.dateFormat=dateFormate;
    if (startDate) {
      startDate = startDate.split("/");
      var mm = startDate[0];
      var dd = startDate[1];
      var yy = startDate[2];
      finalDate = new Date(yy,mm,dd);
      finalDate.setDate(finalDate.getDate()+60);
      var maxDate = finalDate.getDay();
      var maxMonth = finalDate.getMonth();
      var maxYear = finalDate.getFullYear();
      if (dateFormate === "dd/mm/yyyy") {
        widgetId.enableRangeOfDates([dd, mm, yy], [maxDate, maxMonth, maxYear], "skn", true);
        widgetId.date = dd + "/" + mm + "/" + yy;
      } else {
        widgetId.enableRangeOfDates([dd, mm, yy], [maxDate, maxMonth, maxYear], "skn", true);
        widgetId.date = mm + "/" + dd + "/" + yy;
      }
    }
  };

  var _getNumberOfDays = function(fromDate, toDate) {
    var startDate,endDate,diffDays;
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    if(kony.onlineBanking.configurations.getConfiguration("frontendDateFormat")==="mm/dd/yyyy"){
      fromDate = fromDate.split("/");
      toDate = toDate.split("/");
      startDate = new Date(fromDate[2],fromDate[0],fromDate[1]);
      endDate = new Date(toDate[2],toDate[0],toDate[1]);
    }
    //other conditions depending on date formate
    if(startDate && endDate){
      var utc1 = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
      var utc2 = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
      diffDays = Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }
    return diffDays;
  };
  return {
    getMaskedAccountNumber : _getMaskedAccountNumber,
    getAccountDisplayName: _getAccountDisplayName,
    getAccountName: _getAccountName,
    getDisplayBalance: _getDisplayBalance,
    getLastFourDigit: _getLastFourDigit,
    downloadFile: _downloadFile,
    formatCurrency: formatCurrency,
    formatDate: formatDate,
    ErrorHandler: ErrHandler,
    formatCurrencyWithCommas: _formatCurrencyWithCommas,
    getDisplayCurrencyFormat : _getDisplayCurrencyFormat,
    DateUtils: DateUtils,
    getConfiguration: _getConfiguration,
    Sorting: {
      getSortConfigObject: _getSortConfigObject,
      setSortingHandlers: _setSortingHandlers,
      updateSortFlex: _updateSortFlex
    },
    getAccountDisplayNameWithBalance: _getAccountDisplayNameWithBalance,
    showProgressBar: _showProgressBar,
    hideProgressBar: _hideProgressBar,
    getFrontendDateString: _getFrontendDateString,
    getBackendDateFormat: _getBackendDateFormat,
    disableOldDaySelection:_disableOldDaySelection,
    changedataCase:_changedataCase,
    getDateAndTime:_getDateAndTime,
    toggleCheckBox: _toggleCheckbox,
    isChecked: _isChecked,
    sendDateToBackend:_sendDateToBackend,
    setCheckboxState: _setCheckboxState,
    updateAppLevelConfiguration:_updateAppLevelConfiguration,
    updateUserLevelConfiguration:_updateUserLevelConfiguration,
    returnMonths : _returnMonths,
    isCSRMode:_isCSRMode,
    isRadioBtnSelected: _isRadioBtnSelected,
    enableButton: _enableButton,
    mergeAccountNameNumber: _mergeAccountNameNumber,
    disableButton: _disableButton,
    showServerDownScreen: _showServerDownScreen,
    disableButtonActionForCSRMode:_disableButtonActionForCSRMode,
    disableButtonSkinForCSRMode:_disableButtonSkinForCSRMode,
    disableSegmentButtonSkinForCSRMode:_disableSegmentButtonSkinForCSRMode,
    isValidAmount: _isValidAmount,
    isEmptyString: _isEmptyString,
    substituteforIncludeMethod:_substituteforIncludeMethod,
    getTimeDiferenceOfDate: _getTimeDiferenceOfDate,
    validateCurrency : _validateCurrency,
    getPrimaryContact: _getPrimaryContact,
    validateAmountFieldKeyPress: _validateAmountFieldKeyPress,
    removeDelimitersForAmount: _removeDelimitersForAmount,
    validateAndFormatAmount: _validateAndFormatAmount,
    bindEventsForAmountField: _bindEventsForAmountField,
    deFormatAmount: _deFormatAmount,
    blockFutureDate:_blockFutureDate,
    getNumberOfDays:_getNumberOfDays
  };
});
