define(['commonUtilities'],function(commonUtilities){

  return {

    willUpdateUI:function(viewModel){
      this.view.internalAccount.btnAddAccountKA.skin="sknBtnNormalLatoFFFFFF15Px";
      this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnHoverLatoFFFFFF15Px";
      this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnFocusLatoFFFFFF15Px";
      this.view.internalAccount.btnAddAccountKA.setEnabled(true);
      if(viewModel==undefined){
        this.resetInternalAccount();
      }
      else{
        for(var i=0;i<viewModel.length;i++){
          if (viewModel[i].sideMenu) this.updateHamburgerMenu(viewModel[i].sideMenu);
          if (viewModel[i].topBar) this.updateTopBar(viewModel[i].topBar);
          if (viewModel[i].internalAccount){
            this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
          }
          if(viewModel[i].sameAccounts){
            this.resetInternalAccount();
            this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
            this.setSameBankAccounts(viewModel[i].sameAccounts);
          }
          if(viewModel[i].serverError){
            this.resetInternalAccount();
            this.view.internalAccount.rtxDowntimeWarning.text = viewModel[i].serverError;
            this.view.internalAccount.flxDowntimeWarning.setVisibility(true);
          }
          if (viewModel[i].newrecipient) {
            this.view.internalAccount.tbxBeneficiaryNameKA.text = viewModel[i].newrecipient.recShareID;
            this.view.internalAccount.tbxAccountNickNameKA.text = viewModel[i].newrecipient.recLastName;
            this.view.internalAccount.tbxAccountNumberAgainKA.text = viewModel[i].newrecipient.recMemshpID;
            this.view.internalAccount.btnAddAccountKA.skin = "sknBtnNormalLatoFFFFFF15Px";
            this.view.internalAccount.btnAddAccountKA.hoverSkin = "sknBtnHoverLatoFFFFFF15Px";
            this.view.internalAccount.btnAddAccountKA.focusSkin = "sknBtnFocusLatoFFFFFF15Px";
            this.view.internalAccount.btnAddAccountKA.setEnabled(true);
          }
        }
        commonUtilities.hideProgressBar(this.view);
      }
      this.AdjustScreen();
    },


    setSameBankAccounts:function(data){
      var self=this;
      this.view.previouslyAddedList.segkonyaccounts.widgetDataMap={
        "CopyLabel0a25bb187ff174b": "CopyLabel0a25bb187ff174b",
        "CopyLabel0f686ea9ea96c4e": "CopyLabel0f686ea9ea96c4e",
        "Label0j5951129f89142": "Label0j5951129f89142",
        "btnviewdetails": "btnviewdetails",
        "flxsegment": "flxsegment",
        "flxsegmentseperator": "flxsegmentseperator",
        "lblAccount": "lblAccount",
        "lblbank": "lblbank",
        "btnMakeTransfer": "btnMakeTransfer"
      };
      var segData=[];
      function getMapping(context){
        if(context.btnMakeTransfer.isVerified=="true"){
          return {
            "text":"MAKE TRANSFER",
            "onClick":function () {
              self.onBtnMakeTransfer(); 
            }
          };
        }else{
          return {
            "text":"PENDING",
            "onClick":function () {},
            "skin":"sknlblLato5daf0b15px",
          };
        }
      }
      for(var i=0;i<(data.length);i++){
        if((data[i]!==undefined)&&(data[i].isSameBankAccount==="true")){
          segData.push({
            "btnviewdetails": {
              "text":"View Details",
              "onClick":function () {
                self.onBtnViewDetails();
              }  
            },
            "btnMakeTransfer": getMapping({"btnMakeTransfer":data[i]}),
            "lblAccount":data[i].nickName,
            "lblbank":data[i].bankName,
            "CopyLabel0a25bb187ff174b": {
              text: data[i].accountNumber,
              isVisible: true
            },
            "CopyLabel0f686ea9ea96c4e": {
              text: data[i].accountType,
              isVisible: true
            },
            "Label0j5951129f89142": {
              text: "wwq",
              isVisible: true
            }
          });
        }
      }
      var len = segData.length;

      this.view.internalAccount.btnAddAccountKA.skin="sknBtnBlockedLatoFFFFFF15Px";
      this.view.internalAccount.btnAddAccountKA.setEnabled(false);
      if (len !== 0) {
        this.view.previouslyAddedList.flxheader.setVisibility(true);
        this.view.previouslyAddedList.flxseperator.setVisibility(true);
        //  this.view.previouslyAddedList.segkonyaccounts.setVisibility(true);
        //hiding visiblity of separator for last account
        segData[len - 1].Label0j5951129f89142.isVisible = false;
        segData[len - 1].CopyLabel0f686ea9ea96c4e.isVisible = false;
        segData[len - 1].CopyLabel0a25bb187ff174b.isVisible = false;

        this.view.previouslyAddedList.segkonyaccounts.setData(segData);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "0px";

      } else {
        this.view.previouslyAddedList.flxheader.setVisibility(true);
        this.view.previouslyAddedList.flxseperator.setVisibility(true);
        this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
        this.view.previouslyAddedList.flxaddkonyaccnt.top = "0px";
      }
      this.view.forceLayout();
    },

    preshowFrmAddAccount: function () {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.internalAccount.lblBankNameValue.text = kony.mvc.MDAApplication.getSharedInstance().appContext.bankName;
      this.view.internalAccount.flxInternationalDetailsKA.setVisibility(false);
      this.view.customheader.topmenu.flxMenu.skin="slFbox";
      this.view.customheader.topmenu.flxaccounts.skin="sknFlx4f2683Occu";
      this.view.customheader.topmenu.lblAccounts.skin = "sknLblGtmffffffOccu";
      this.view.customheader.topmenu.imgAccounts.src = "accounts_white.png";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.lblTransferAndPay.skin = "sknLblGtm4f268316pxOccu";
      this.view.customheader.topmenu.imgTransfers.src = "sendmoney_purple.png";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxSeperator3.setVisibility(true);
      this.view.customheader.topmenu.flxaccounts.skin="sknHoverTopmenu7f7f7pointer"; 
      this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
      this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
      this.view.customheader.customhamburger.activateMenu("Transfers", "Add Other bank and credit union Member account");
      this.view.breadcrumb.setBreadcrumbData([{text:kony.i18n.getLocalizedString("i18n.transfers.transfer")}, 
                                              {text:kony.i18n.getLocalizedString("i18n.AddExternalAccnt.Add_Kony_Bank_Beneficiar")}]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip= kony.i18n.getLocalizedString("i18n.transfers.transfer");
      this.view.breadcrumb.lblBreadcrumb2.toolTip=kony.i18n.getLocalizedString("i18n.AddExternalAccnt.Add_Kony_Bank_Beneficiar");											
      this.view.forceLayout();
      this.view.customheader.headermenu.btnLogout.onClick = function () {
        kony.print("btn logout pressed");
        scopeObj.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        scopeObj.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        scopeObj.view.flxLogout.left = "0%";
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        kony.print("btn yes pressed");
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        context = {
          "action": "Logout"
        };
        authModule.presentationController.doLogout(context);
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        kony.print("btn no pressed");
        scopeObj.view.flxLogout.left = "-100%";
      };

    },


    addInternalAccount:function(){
      var scopeObj=this;  
      var data= { 
        "bankName": this.view.internalAccount.lblBankNameValue.text,
        "accountType": this.view.internalAccount.lbxAccountTypeKA.selectedKeyValue[1],
        "accountNumber": this.view.internalAccount.tbxAccountNumberKA.text,
        "reAccountNumber":this.view.internalAccount.tbxAccountNumberAgainKA.text,
        "beneficiaryName":commonUtilities.changedataCase(this.view.internalAccount.tbxBeneficiaryNameKA.text),
        "nickName":commonUtilities.changedataCase(this.view.internalAccount.tbxAccountNickNameKA.text)
      }; 
      var errMsg; 
      if((data.nickName === null || data.nickName === "") && (data.beneficiaryName !== null || data.beneficiaryName !== ""))
        data.nickName = data.reAccountNumber;

      //if(!(data.accountNumber === data.reAccountNumber)){
      if(!(data.reAccountNumber !==null) && (data.beneficiaryName !==null) && (data.nickName !==null)){

        //alert("Hiiii"); 
        this.view.internalAccount.tbxAccountNumberKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.internalAccount.tbxAccountNumberAgainKA.skin = "skntxtLato424242BorderFF0000Op100Radius2px";
        this.view.internalAccount.btnAddAccountKA.skin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.setEnabled(false);
        errMsg = kony.i18n.getLocalizedString("i18n.transfers.accNoDoNotMatch");
        this.errorInternal({"errorInternal":errMsg});
      }
      else{
        this.view.internalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
        this.view.internalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.internalAccount.btnAddAccountKA.skin="sknBtnNormalLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnHoverLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnFocusLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.setEnabled(true);
        this.view.internalAccount.lblWarning.setVisibility(false);
        this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
        this.presenter.addInternalAccount(this,data);
      }
    },

    validateInternalError:function(){
      var errMsg;
      var data= { 
        "bankName": this.view.internalAccount.lblBankNameValue.text,
        "accountNumber": this.view.internalAccount.tbxAccountNumberKA.text,
        "reAccountNumber":this.view.internalAccount.tbxAccountNumberAgainKA.text,
        "beneficiaryName": this.view.internalAccount.tbxBeneficiaryNameKA.text,
        "nickName":this.view.internalAccount.tbxAccountNickNameKA.text
      }; 

      if (data.beneficiaryName === "" || data.reAccountNumber === "" || data.nickName === "") {
        this.view.internalAccount.btnAddAccountKA.skin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnBlockedLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.setEnabled(false);
      }
      else{
        this.view.internalAccount.tbxAccountNumberKA.skin = "sknTextBox72";
        this.view.internalAccount.tbxAccountNumberAgainKA.skin = "sknTextBox72";
        this.view.internalAccount.btnAddAccountKA.skin="sknBtnNormalLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.hoverSkin="sknBtnHoverLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.focusSkin="sknBtnFocusLatoFFFFFF15Px";
        this.view.internalAccount.btnAddAccountKA.setEnabled(true);
      }
    },  


  };
});